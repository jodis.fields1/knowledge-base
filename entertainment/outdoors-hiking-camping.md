# Outdoors, hiking, camping

**Liability waivers** \- 2014-08: common for adventure sports, note that these are enforceable (see Donahue v. Ledgends Inc Alaska 2014)

**Gear/Clothing**

See wardrobe as well, but shopped 2014-06 and found   [polyester vs. nylon bottoms](http://www.backpackinglight.com/cgi-bin/backpackinglight/forums/thread_display.html?forum_thread_id=68186 "http://www.backpackinglight.com/cgi-bin/backpackinglight/forums/thread_display.html?forum_thread_id=68186") which makes it seem like a wash

Get the Biolite Campstove

Sleeping bags: reviewed printout from REI which showed fill volume, EN13537 rating (European standard) , water-repellent etc. Ended up with mid-priced Radiant 650-fill duck down fill volume 6.3 oz. good down to 19 degrees based on long underwear layer

**Kayaks**
Oru Kayaks are cool! from Shark Tank.

**Synthetic** ****convertible** **pants**   
Kuhl - way too large even at 30x32 for liberator convertible   
The North Face - M Active Fit at 30/short quite baggy   
REI Endeavor Convertible - excellent fit with little bagginess! But spendy at $109   
Columbia Silver Ridge - hint of bagginess at bottom is a bit annoying but otherwise great; affordable at $60

Knots

2014-12: dad showed me the bowline ( [wiki](https://en.wikipedia.org/wiki/Bowline "https://en.wikipedia.org/wiki/Bowline")) and trucker's hitch ( [ wiki](https://en.wikipedia.org/wiki/Trucker%27s_hitch "https://en.m.wikipedia.org/wiki/Trucker%27s_hitch")); I  need to review them!

See  [ animatedknots.com](http://www.animatedknots.com/ "http://www.animatedknots.com/") - need to read up on this site and learn some of these

## Fishing

### Gear and setup

Terminology includes the arbor of the reel (basically the wheel/disc) to which the line is attached using an "arbor knot" ( [wiki](https://en.wikipedia.org/wiki/Arbor_knot "https://en.wikipedia.org/wiki/Arbor_knot")); per for fly rods a large arbor is recommended to pull up faster

Lures ( [wiki](https://en.wikipedia.org/wiki/Fishing_lure "https://en.wikipedia.org/wiki/Fishing_lure")): fishingnoob page is  [good overview](http://fishingnoob.com/68/types-of-fishing-lures/ "http://fishingnoob.com/68/types-of-fishing-lures/") and I have lots of soft plastic bait; confused by my spinnerbait tho

Pixee by Blue Fox is a famous #1 lure, bought a couple 2014-07-19 at David's suggestion

### Trout fishing in Juneau  - also see Juneau note   

[Juneau Fishing Reports](http://www.alaskaflyfishinggoods.com/fishing-reports/juneau.html "http://www.alaskaflyfishinggoods.com/fishing-reports/juneau.html") by Alaska Fly Fishing Goods **

** Basic tactics:  **

** http://www.dnr.state.mn.us/areas/fisheries/crystalsprings_hatchery/basic_tactics.html \- few anglers catch most fish   
Trout Fishing in SE AK (Juneau) - forum overview; Cowee and Petersen crks are the popular spots. Petersen is short, small, and crowded. Cowee is way out the road and tougher to fish, but a much nicer experience IMO. Each have small runs of 200 or less; also F &G weir reports for more info

**Water**   
Drinking Water Treatment Methods for Backcountry and Travel Use - from CDC and says that boiling is only guarantee; chlorine doesn't kill all protozoa   
MSR - MiniWorks EX at $90 is good for filtering water

Camping

If you really think it will rain, bring a tent for above and below, plus a pavilion for the cooking area! In addition be sure to tuck the tarp under in so it doesn't capture water!

**Survivalist**   
"Prepper" movement in the United States discussion includes Subculture of Americans prepares for civilization's collapse

Also shtfplan.com stands for shit hits the fan   

Thinking about getting into metalworking and forging which is related to this; see for example SHTF Blacksmithing : What you need to know to get started? orGet Medieval: How to Build a Metal Forge   
See Personal->Home section for some stuff and Cooking for food storage tips   

Medication storage->not a lot of studies  [per straightdope forum](http://boards.straightdope.com/sdmb/showthread.php?p=14944861) but they should be OK

[ Survival Tabs ](http://www.sohlius.com/purchase-tabs)- inspired by Lifecaps scam on Shark Tank, but these actually have calories

## Boy Scouts
So I became a Life scout and my 3 brothers are Eagle; controversy over pedophilia hit Oregon particularly bad for some reason with a $20m jury verdict in 2010 for Kerry Lewis; in 2012 Oregon Supreme Court ordered release of the secret files on pedophilia
