# Travel ins

http://www.washingtonpost.com/lifestyle/travel/the-navigator-travel-insurance-claims-can-hinge-on-the-tiniest-details/2012/05/17/gIQA9VASYU_story.html by Christopher Elliot is the best article I've seen on this topic in a while; he has a website dedicated to travel and started a travelers nonprofit http://www.elliott.org/ and his overview is at http://elliott.org/live-2/when-should-i-buy-travel-insurance/


**Regional differences**

Insureandgo.com is a biggie in Europe but there universal healthcare doesn't cover foreign healthcare

**Companies**

USTIA has a whole list!

Seven Corners - pure play; RoundTrip brand underwritten by Nationwide or United States Fire; led by Krampen and Tysdal; looks like they are heavily involved in the trade association per http://www.sevencorners.com/companyinfo/leadership/

Worldnomads.com - uses National Union; recommended a lot on travel.stackexchange.com

**Lit review**

Particularly on the medical side, I imagined there might be adverse selection and medical scholarly research. Not much on adverse selection.

The Law and the Travel Industry by Anolik http://www.travellaw.com/page/alexander-anolik might be good

  


Travel Health Insurance: Indicator of Serious Travel Health Risks http://onlinelibrary.wiley.com/doi/10.2310/7060.2003.35770/abstract \- first one I read; note that it says Swiss insurance generally covers travel

Travel Insurance Claims Made by Travelers from Australia http://onlinelibrary.wiley.com/doi/10.2310/7060.2002.21444/abstract \- includes number which were accepted (2/3s)

Travel insurance and medical evacuation: view from the far side https://www.mja.com.au/journal/2004/180/1/travel-insurance-and-medical-evacuation-view-far-side \- paints a dim picture of medical evacuation insurance

Illness and injury presenting to a Norwegian travel insurance company's helpline http://www.sciencedirect.com/science/article/pii/S1477893906001037 \- suggests a central database for this information

  


_Informal_

The Basics Of Travel Insurance

  


**Associations**

United States Travel Insurance Association USTIA

American Society of Travel Agents

Travel Health Insurance Association of Canada (thiaonline.com/)

International Association for Medical Assistance to Travellers http://www.iamat.org/blog/index.cfm is mainly educational

  


**Casual publications**

http://www.thestar.com/specialsections/snowbirds/article/1301146--protect-yourself-with-a-travel-health-insurance-policy \- Canadia bias

  


http://www.ncbi.nlm.nih.gov/pubmed/9718838 - Travel brochures need to carry better health advice; medical risks

  


**Journals and websites**

http://www.squaremouth.com/ - best travel insurance provider by far

http://www.travelinsurancereview.net/reviews/ - nowhere near as good

http://www.tripbase.com - cool website! will give you ideas about where to go

  


Journal of Travel Medicine

Travel Medicine and Infectious Disease

  


http://www.travelinsurancereview.net/travel-medical-insurance/
