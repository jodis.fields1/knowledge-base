NOTE: see life-log/logging/travel-data.md & life-log/gadgets

### parking
See also items_home/icebox/auto

Apps:
* SpotAngels
* SpotHero

### gear
[Everyday Carry](http://everydaycarry.com/)

http://tynan.com/gear2017

[How to Build a Men's Capsule Wardrobe for Traveling](https://web.archive.org/web/20160417021247/https://www.fredperrotta.com/travel-capsule-wardrobe/)

#### Light jacket
Patagonia Men's Down Sweater Jacket:
    * found at https://theworldpursuit.com/packable-down-jacket/
    * seems promising
    * compresses to 6"x7"x3.5" per https://www.youtube.com/watch?v=BrgjkxqHxmU

Tynan recommends Montbell Plasma 1000:
    * Negative: I want pockets
    * Positive: packs down into a pocket?

James Clear recommends Patagonia Nano Puff Hoody:
    * negative - does not pack down small enough

EMS Feather Pack Hooded Jacket
    * says it has fancy water repellent

!UNIMPORTANT

#### luggage
* CURRENT: FILL THIS IN
* MAYBE: [Biaggi Luggage Contempo Foldable Spinner Carry On, 22-Inch](https://www.amazon.com/Biaggi-Luggage-Contempo-Foldable-Spinner/dp/B0090LGZH2) - innovative folding


## Tools
https://maps.roadtrippers.com
TripSee - cool idea, helps you actually book flights and hotels
Tripit - more for after you booked your flight

## 2017
https://nomadlist.com/regions
TODO: plan Southeast Asia trip: Chiang Mai, Thailand or Ubud, Bali or m

1. Book flight: skyscanner and hipmunk are best, looks like it'll be $600 roundtrip min
2. Find 

http://thewirecutter.com/reviews/travel-guide/
http://herpackinglist.com/2011/06/essential-items-hand-washing-clothes/ - sink plug, pack towels to help dry faster
clothes - Outlier Slim Dungarees or perhaps travel jeans? http://snarkynomad.com/travel-jeans-my-imaginary-best-friend/ - but I like merino wool


## Digital nomad
https://github.com/engineerapart/TheRemoteFreelancer

https://www.reddit.com/r/digitalnomad/comments/6fhg5c/best_bank_accounts_for_digital_nomads/ - address is tricky, note the regs cited
https://www.reddit.com/r/churning/

## Old (pre-2015)
Christopher Elliot has good advice

**Packing**
[https://www.minimus.biz](https://www.minimus.biz) - travel sized items
[The NOMATIC Travel Bag](https://www.indiegogo.com/projects/the-nomatic-travel-bag-backpack#/) - 

Review http://www.onebag.com/rsrc.html and buy some travel clothing next time! for example Magellan's or Ex-Officio

See  [Tips and Hacks for Everyday Life: What are the best travel hacks?](http://www.quora.com/Tips-and-Hacks-for-Everyday-Life/What-are-the-best-travel-hacks/answer/Thomas-Snerdley) from Quora, particularly two-bag system

learn  [the art of packing](http://www.quora.com/Tips-and-Hacks-for-Everyday-Life/What-is-something-useful-I-can-learn-right-now-in-10-minutes-that-would-be-useful-for-the-rest-of-my-life/answer/Carlos-Amezquita?srid=tnlV&share=1) by Carlos Amezquita at Quora

**Industry trends**

Growth in travel generally; also among youths per Youth travel on the rise for reasons other than leisure

Kuoni Travel Trends Report - identifies top destinations and stuff; Maldives is hot whereas Thailand, Sri Lanka, Switzerland for singles

Environmental impact -  [huge increased travel](https://en.wikipedia.org/wiki/Aviation_and_the_environment#Future_emission_levels:_improved_efficiencies_vs._the_trend_in_increased_travel_and_freight) offsets efficiency improvements; also see airline wiki article

*Airlines*- see stock notes

faredetective.com - seemed like its charts might be broken

farereport.com - somewhat basic

[Basic Measurements in the Airline Business](http://www.aa.com/i18n/amrcorp/corporateInformation/facts/measurements.jsp) - saw this in researching Hawaiian 2014

findthebest has prices for all airplanes!

Forecasting aircraft values: An appraiser's perspective by Avitas - interesting article

[How Airline Ticket Prices Fell 50% in 30 Years (and Why Nobody Noticed)](http://www.theatlantic.com/business/archive/2013/02/how-airline-ticket-prices-fell-50-in-30-years-and-why-nobody-noticed/273506/) - could they decline more?

[Airline fare analysis: comparing cost per mile](http://blog.rome2rio.com/2013/01/02/170779446/) - widely varying costs

Samoa Air says charging passengers by weight has been successful (2013) - great!

[Low-cost air fares: How ticket prices fall and rise](http://www.bbc.com/news/business-22882559) - cool graphs on rise of price as it gets nearer

**Mapping**

**Google is awesome but it's not showing the Wikipedia layer anymore**

OpenStreetMap  [has no satellite view](https://help.openstreetmap.org/questions/6849/how-can-i-see-the-aerial-imagery-without-editing-the-map), which is a weakness

Per Perfect Vision From 383 Miles (2014), DigitalGlobe creates the images, perhaps created by Ball Corp which makes aluminum cans

**Food and vendors**

GlutenFreeTravelSite.com - competitor, good cust. service

Items

Insurance

Airlines

Fewer Flights and Higher Fares: Is This the Future of Air Travel? (2013) - good article, notes decline in hubs

Checklist

Soap: looking for  [travel-sized container]()
Accessories: belt, backup glasses
Tech: retractable usb cable
*Sleeping in the airport*
[IRS regulations prevent sleeping pods at Anchorage airport](http://www.adn.com/article/20140914/irs-regulations-prevent-sleeping-pods-anchorage-airport) (2014) - need to get Murkowski/Begich on this

Sleepbox - little room to sleep in

## Losing stuff
Lost ID once - now I have Safedome

## Programs
http://www.flyertalk.com/forum/online-travel-booking-bidding-agencies/1341415-priceline-bidding-primer.html suggests priceline can be awesome
gtfo - Get the Flight Out - originally a hopper thing but they seem to have dropped it?
    * maybe http://www.gtfoflights.com/
    * Scott's Flights https://app.scottscheapflights.com
hoteltonight - can get a hotel for very cheap at the last minute
hotwire - somewhat similar but does not reveal the name of the hotel (owned by Expedia) in an opaque model
routehappy.com - seems to be pretty good
kayak - first check because of user interface, bought by Priceline
getgoing.com - better than hotels.com since no annual restriction, also has opaque with pick two, get one trip
priceline - name your own price is an opaque option
travelocity - has an opaque option 

Room 77 - pick the room you want

trivago - Expedia bought it, European focus

Loyalty

confiscation of points