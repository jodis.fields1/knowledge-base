# Language learning

See http://www.getrichslowly.org/blog/2011/08/22/how-to-learn-a-foreign-language-without-spending-a-cent/ for an interesting take by Benny Lewis, the Irish polyglot

  


http://www.fluentin3months.com/passive-learning/ - skepticism

Benny's survey http://www.fluentin3months.com/best-course/

Look at the Language Hacking Guide

Use LiveMocha and Busuu to talk to native speakers; not sure about LingQ

HB 2.0 is Benny's favorite - which is really just people

http://language-learning-programs.findthebest.com/ - list

  


Timothy Ferris says focus on commonly-used words http://www.fourhourworkweek.com/blog/2009/01/20/learning-language/

  


_Free_

skritter.com - characters?

duolingo.com - supposed to be good for Spanish and such

http://fsi-language-courses.org/Content.php

  


_Edufire_

http://edufire.com/classes/mandarin really seems to just be a connection to a tutor

  


_Mango_

Might as well try it as the library has it

  


_Rosetta Stone_

Trying to get this for free but having issues; note that the lack of English words is a turnoff to me

Benny's review http://www.fluentin3months.com/rosetta-stone-review/

  


_Pimsleur_

Old, focuses on saying words in foreign and then native

http://www.fluentin3months.com/pimsleur/

  


_Assimil_

discussion http://www.fluentin3months.com/forum/general-discussion/assimil/
