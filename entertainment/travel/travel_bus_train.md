# Travel bus or tain

This can theoretically reduce carbon emissions, but it's so much slower...

## train
https://www.universalhub.com/2019/how-amtrak-sabotages-even-minimal-mbta-interest | How Amtrak sabotages even minimal MBTA interest in electrifying the Providence Line | Universal Hub
https://www.cnet.com/news/are-us-trains-really-that-bad-its-complicated/ | Are US trains really that bad? It's complicated - CNET

## bus
https://shop.flixbus.com/ - electric

https://www.wanderu.com/en-us/train/us-ca/san-francisco/us-or/portland/ | Train from San Francisco to Portland - Tickets from $92 | WANDERU
https://www.wanderu.com/en-us/bus/us-ca/san-francisco/us-or/portland/ | Bus from San Francisco to Portland - Tickets from $75 | WANDERU
