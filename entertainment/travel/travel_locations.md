https://nomadlist.com/ - best source

Airbnb Experiences?

## 2017 research
### South America
Medellin, Colombia - San Francisco weather ("City of Eternal Spring"), detailed breakdown at http://ecomvoyager.com/a-digital-nomads-guide-to-medellin/
Cancun, 
Playa Del Carma - quite touristry, see http://ecomvoyager.com/a-digital-nomads-guide-to-playa-del-carmen/#more-377
maybe even Puerto Vallarta https://www.reddit.com/r/digitalnomad/comments/2viykr/has_anyone_here_set_up_shop_in_puerto_vallarta/

Aruba / Curacao - touristy islands off Venezuela
Panama City - bit on the expensive side

Argentina - ??
Brazil - ??
Chile - ??

Cuenca, Ecuador - 700k people, most European, safer, more expats
Quito, Ecuador - 3m people, crime problem historically; few beautiful women?, but see https://medium.com/@Hmaailm/all-you-need-is-ecuador-this-is-for-real-e13be206a5e2
Guayaquil, Ecuador - 3.5m largest
La Paz, Bolivia - poorest country in SA, high altitude (sun and weather issues), spotty internet, disease...
Lima, Peru - 10m people, sounds a bit rough / expensive
Costa Rica - no cafe / coworking culture, Tamarindo, is beach spot, bit on the expensive side with spotty internet I hear
Antigua, Gutamela - not much English, as a city bit more expensive but still cheap < $1000
San Salvador, EL Salvador - again, English isn't great
Nicaragua - negative scores
Belize - apparently expensive and not super safe per https://nomadlist.com/forum/t/which-city-in-belize-should-we-go-to/6605/2
Venezuela - unsafe

### South Korea
Seoul (obv)
Busan - (second largest city) ? https://medium.com/@novikresna/10-ways-busan-is-the-next-chiang-mai-as-digital-nomad-hub-b701a7864384

### Bali
not safe for women https://www.reddit.com/r/digitalnomad/comments/2viykr/has_anyone_here_set_up_shop_in_puerto_vallarta/coih5iu/

## Flights to Asia
https://www.cleverlayover.com
https://travel.stackexchange.com/questions/73678/direct-flights-between-se-e-asia-and-south-america
Seems that Hong Kong is the main hub to fly from SF
Seoul is another major hub
Tokyo flights are oddly expensive
Direct flights to Bangkok planned for 2017 but delayed due to FAA Category 2 safety rating