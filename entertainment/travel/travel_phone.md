# Travel - phone

switchboard - https://goswitchboard.com/features/

https://www.lifewire.com/parking-how-to-hold-cell-number-577582

* https://www.mymoneyblog.com/list-of-cheap-basic-prepaid-cell-phone-plans-under-10-a-month.html
* https://www.myrateplan.com
* [Cell Phone Networks and Frequencies Explained: 5 Things To Know](https://www.whistleout.com/CellPhones/Guides/cell-phone-networks-and-frequencies-explained)

Two goals:
1. good international phone
   1. Google Fi
2. [Seeking advice on emergency phone+service to leave in a car glove box](https://www.reddit.com/r/NoContract/comments/77bfvv/seeking_advice_on_emergency_phoneservice_to_leave/)
   1. lots of options, including freedompop
   2. https://www.mymoneyblog.com/list-of-cheap-basic-prepaid-cell-phone-plans-under-10-a-month.html
      1. tello is prolly best, and ting

## 2020-01 again
LG v20 has a SIM 89014103272569391606 which according to https://www.numberingplans.com/?page=analysis&sub=simnr is from Cingular / AT&T

Samsung S5 has a SIM 89014103270232188912


## 2018-10 again

### unlocking
after googling, found AT&T has a site: https://www.att.com/deviceunlock/?#/

pulled the trigger on https://support.freedompop.com/app/answers/detail/a_id/3265/

TODO: wipe and put in generic ROM https://www.reddit.com/r/freedompop/comments/5j34o1/got_a_samsung_s4_but_about_half_the_time_whenever/dbihj6t/


## 2018-07 next look
figuring out more, frontrunners:

t-mobile $3/month - suggested on reddit

### OneSim
OneSim - says expires after 2 years?
* http://www.onesimcard.com/international-sim-card/universal/

Caution: Estonian number https://www.amazon.com/gp/customer-reviews/RTX9G4ME7RRZN/ref=cm_cr_dp_d_rvw_ttl?ie=UTF8&ASIN=B002ZJ5EDC

### ting


### tello
Tello - Samsung S4 not compatible

## truphone
Truphone - got a bad feel from navigating their site
    * [Anyone using Truphone as daily driver?](https://www.reddit.com/r/NoContract/comments/4nc153/anyone_using_truphone_as_daily_driver/)
    * "Expiration is a little foggy. I know of no one who has had a phone expire with a credit balance. Tradition says use it every 90 days" - https://www.howardforums.com/showthread.php/1896286-2017-Truphone-Thread-2017

### freedompop
FreedomPop - VoIP, seems a bit sketchy?
    * "loses it's phone number. Also, every week it needs to update it's software" per https://www.reddit.com/r/NoContract/comments/77bfvv/seeking_advice_on_emergency_phoneservice_to_leave/dokstrz/

## general
mostly from 2017
http://tynan.com/tmobile - in 2014 the the master said T-Mobile

https://www.skyroam.com/wifi-pricing

https://www.pagepluscellular.com/ | Page Plus Cellular - Prepaid Cell Phone Cards | Prepaid National Cell Phones | Page Plus Cellular
https://www.desktodirtbag.com/cheapest-international-phone-plan-world-travel/ | 
https://www.reddit.com/r/digitalnomad/comments/53uam6/from_what_country_and_provider_can_i_get_the_best/ | From what country and provider can I get the best International Roaming Data Plans to the most countries? : digitalnomad
http://prepaid-data-sim-card.wikia.com/wiki/Prepaid_SIM_with_data | Prepaid Data SIM Card Wiki | FANDOM powered by Wikia
https://www.cable.co.uk/guides/payg-sims-no-monthly-top-up-long-expiry/ | PAYG SIMs with no monthly top-up & long expiry | Best deals from O2, ASDA Mobile, giffgaff and Vodafone
