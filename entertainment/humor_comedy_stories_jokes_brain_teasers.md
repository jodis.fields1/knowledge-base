
## TODO 2021
Louis long-distance https://www.reddit.com/r/louisck/comments/ho8mll/an_awfully_long_email_from_louis_ck/

## toasts
* "Never lie, steal, cheat, or drink. But if you must lie, lie in the arms of the one you love. If you must steal, steal away from bad company. If you must cheat, cheat death. And if you must drink, drink in the moments that take your breath away"
  * from Hitch but apparently an old Irish blessing

## newish (starting 2017)

### dilbert
2018-06: started from 1989-04-16; current: [1989-04-21](http://dilbert.com/strip/1989-04-21)
TODO: set this up on my landing page

### innocent
[40 Dumb Jokes Based On Smart Wordplay That Will Make You Laugh Out Loud](https://thoughtcatalog.com/melanie-berliet/2015/09/40-dumb-jokes-based-on-smart-wordplay-that-will-make-you-laugh-out-loud/)

Where do orcas go hear music?
  * the orchestra
  * lots more at animal jokes from https://web.archive.org/web/20171117044734/http://www.funology.com/animal-jokes/

What do trees say? "Leave" - "oh I didn't think of that one" - "what did you come up with" - stumped

I googled for how to start a large fire...
  * 52,000 matches

Mitch Hedberg jokes:
[A Complete Ranking Of (Almost) Every Single Mitch Hedberg Joke](https://www.buzzfeed.com/mrloganrhoades/a-complete-ranking-of-almost-every-single-mitch-hedberg-joke?utm_term=.rv9O85P5B3#.mo1eqy1yXr)
  * I don't have a girlfriend. I just know a girl who would get really mad if she heard me say that

### coding
[45 Jokes Only Programmers Will Get](https://web.archive.org/web/20180313031306/https://www.hongkiat.com/blog/programming-jokes/):

Where do programmers like to hang out?
  * The foo bar


Why did the programmer quit his job?
  * Because he didn't get arrays

What's the object-oriented way to get rich?
  * Inheritance

### not innocent
Dating is a lot like fishing...
  * https://www.reddit.com/r/Jokes/comments/6z0qvc/dating_is_a_lot_like_fishing/

How do you get an apple pregnant?
  * You cum in cider

## old (pre-2015)
See Pinterest for funny ecards and stuff

### Jokes
[http://existentialcrisisfactory.tumblr.com/post/245629162/pun-itive-sentences](http://existentialcrisisfactory.tumblr.com/post/245629162/pun-itive-sentences)
[Supplies joke](http://www.thatsjustracist.com/content.php?id=88) - Asian joke that Collin told me
[Two perfectly rational, perfectly informed individuals walk into a bar](http://pikdit.com/i/two-perfectly-rational-perfectly-informed-individuals-walk-into-a-bar/) - 

### My quips and quotes
Upon hearing a joke that as a child a lady wanted to be a comedian: "and the funny thing about that joke is that it is the best you've got."
Upon hearing that someone doesn't like people: "that's just because you don't like yourself"
"My mind is inside my head".
"You need to buy a smart brain".
"Women can be trained".
"My life is a steady stream of soon forgotten epiphanies."
"The only way to become closer to God is to become a god."

### Sites
Total frat move - recommended by TJ
McSweeney's Internet Tendency
New Yorker cartoons

## Jokes from Kyle
jalapeno - holl up in ya business! (from Kyle Owen)

## Stories about rebellious childhood/youth
Crush on Christa Boyer in kindergarten  
Back of the bus, "faster faster" or "harder harder" jokes  
Playing doctor with Kelsey  
Pot as a child
Jaywalking - if the topic of the law comes up
Bible - circumcision story from Old Testament if it comes up
Kicked out of dorm - pothead issues  

## Appropriate

[How the fight started...(Found on Facebook)](https://m.reddit.com/r/funny/comments/1mylpg/how_the_fight_startedfound_on_facebook/) 

A tourist got off a boat and asked a boy if Ketchikan always  rains this much. The boy said "I dunno. I'm only 12."
A father and his son are in a car accident. The father is  killed and the son is seriously injured. The son is taken to the  hospital where the surgeon says, " I cannot operate, because this  boy is my son."

Why was 6 afraid of 7? Because 7 was registered six offender. Or 7 ate 9

## Phrases*
Genitals in the shredder 
Pillars of intransigence

## Jokes
Contest for most inappropriate name - Pat McGroin from Mad  Men  

Discussing design flaws with Harley Davidson in women versus  men and God said, "I dunno. A lot more people seem to be riding  mine than yours."

*Witty* - someecards.com; I wonder if    [this is  Ashley Kelly](http://www.someecards.com/users/profile/Ash3451270)

  [A  Complete Ranking Of (Almost) Every Single Mitch Hedberg  Joke](http://www.buzzfeed.com/mrloganrhoades/a-complete-ranking-of-almost-every-single-mitch-hedberg-joke) - inspired by seeing  [Hippie  Martian Zen Genius](http://www.mcsweeneys.net/articles/mitch-hedberg-hippie-martian-zen-genius) on r/humor in 2014

  [  Glossary of cricket terms](http://en.wikipedia.org/wiki/Glossary_of_cricket_terms) - fodder for weird  phrases
  [Hilarious  and fantastically funny French phrases](http://frenchdistrict.com/new-york-english/french-translations-english-funny-phrases-idioms-weird-meaning/) - elevator wit,  carrots, etc are kinda funny
  [17  Euphemisms for Sex From the 1800s](http://mentalfloss.com/article/12399/17-euphemisms-sex-1800s)  - blanket  hornpipe, amorous congress

Schnitzel-something - from Gilmore Girls

**Inappropriate**
*Had a friend who liked to say these; don't really like them*.
Why hard to fool an aborted fetus? Wasn't born  yesterday
Why do lesbians hate sports? Because they hate Dick's  (sporting good store)
What's black, white, and red in a telephone booth? A nun  with a javelin through her back
Uncle Matt: so I went to this church and found out that this  lady was healing people by laying hands on them. I said well I've  got this itch on my penis...
Three kids are walking down the road with some chickenwire  and pass an old man on the porch of his house. He asks what  they're doing and they say "we're gonna get some chickens". He  says "you ain't gonna get any chickens with that chickenwire".  Kids walk back with a bunch of chickens. Ditto for duck tape and  ducks. Finally they go out with pussywillow and the man says  "wait, let me get my hat".

*Chuck Norris* - [  Top 50 Chuck Norris Facts & Jokes](http://www.chucknorrisfacts.com/chuck-norris-top-50-facts) - 
*Insurance*  - [Insurance  Jokes](http://www.einsuranceprofessional.com/jokes.html) - decent list

*So-so* 
I found out why I'm fat. Head & Shoulders runs down my  back. It increases body and volume!
The Israeli Ambassador's Speech,    [see  Snopes](http://www.snopes.com/politics/humor/landdispute.asp) - Matt's joke about UN, Israeli speech,  Moses washing, and Palestinians stole clothes. Palestinian said  "we weren't even there yet"! 

**Math tricks** 
Game: take a number, add 10, mulltiply by 3, subtract 30, and  give me your answer. Questioner takes number and divides by 3 to  get original number due to distributive property  (3(a+10) 

Summary of calculus (potential blog post): calculus 1 is  just about slopes of lines and basically 2-dimensional, find the  minimums and maximums to maximize efficiency; calculus 2 is  finding the area under the lines; calculus three gets more  three-dimensional

  **Brain-teasers** 
  You are rushing to a wedding and come to a fork in the road, but  don't know which direction to take. A man stands there and you  recognize him as one of twin brothers, one of whom always lies  and other who always tells the truth. The man knows the right  direction. You can ask one question. What question will tell you  the right road? - answer: what direction would your brother say;  go the opposite.

Ran across these in Mathematics for the Nonmathematician:  
  [Fox,  goose and bag of beans puzzle](https://en.wikipedia.org/wiki/Fox,_goose_and_bag_of_beans_puzzle)- various permutations of it  

  [Jugs of Water](http://256.com/gray/teasers/)- two jugs  (3 and 5 pints) and a pool; fill the 5 pint, pour into 3 pint,  pour out 3 pint, pour 2 pints into 3 pint, fill 5 pint, and fill  3 pint thereby drawing out a pint 

  Two husband
