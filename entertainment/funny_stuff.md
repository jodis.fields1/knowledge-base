where is my funny stuff??

See Anki -> my_fun

Reeve versus reeve: noticed after meeting a man named Silas Reeves that one is a bandit and another is a law enforcement official 

### comedians
bess kalb - but she ended up becoming more political than funny

### puns
https://web.archive.org/web/20170710023816/http://existentialcrisisfactory.tumblr.com/post/245629162/pun-itive-sentences

### my quips and quotes
Upon hearing a joke that as a child a lady wanted to be a comedian: "and the funny thing about that joke is that it is the best you've got."
Upon hearing that someone doesn't like people: "that's just because you don't like yourself"
"My mind is inside my head".
"You need to buy a smart brain".
"My life is a steady stream of soon forgotten epiphanies."
"The only way to become closer to God is to become a god."

### sites
Total frat move - recommended by TJ
McSweeney's Internet Tendency
New Yorker cartoons