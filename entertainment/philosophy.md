# Philosophy

"Whereof one does not know, one cannot speak" - inspired by this Wittgenstein quote I read Was Wittgenstein Right? (2014) and Of Flies and Philosophers: Wittgenstein and Philosophy in NYT; surprisingly Horwich says most philosophers don't like Wittgenstein; sounds mostly like Kant anyway

## Personal view

I laid out my personal view on philosophy in my Love of Wisdom essay from college; I found later that Barry Schwartz wrote a book called Practical Wisdom which has a similar thesis altho less criticism on traditional philosophy

## Community
[http://philpapers.org/browse/philosophy-of-law](http://philpapers.org/browse/philosophy-of-law) \- this is where all the philosophy looks like

## Free will & determinism

See  [ http://www.3ammagazine.com/3am/the-4million-dollar-philosopher/](http://www.3ammagazine.com/3am/the-4million-dollar-philosopher/) for a recent project - however his flattering citation to Robert Kane, who I recognized for relating quantum mechanics and free will, left me cold.

My view is strongly reductionist (see wiki page)

## Morality or justice
Michael Sandel is apparently a "rockstar" of moral philosophy, per [http://www.guardian.co.uk/books/2012/may/27/michael-sandel-reason-values-bodies](http://www.guardian.co.uk/books/2012/may/27/michael-sandel-reason-values-bodies)

Morality shifting in the context of intergroup violence - explains some of the behavior at the ASEA Health Trust, possibly

## Linguistics
[ www.skyscrapercity.com/showthread.php?t=544682](http://www.skyscrapercity.com/showthread.php?t=544682) gives examples of the oddities in American pronunciation; I recall that Thailand is also strangely pronounced versus some other uses of the "ai" sound but can't remember the details

## Books

Trimmed  out of wishlists:

Steps to an Ecology of Mind: Collected Essays in Anthropology, Psychiatry, Evolution, and Epistemology - sounds a little pseudosciency   
Science and sanity; an introduction to non-Aristotelian systems and general semantics - another overrated, rambling book

Godel, Escher, Bach - similar to above