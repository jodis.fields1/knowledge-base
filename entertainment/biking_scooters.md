Biking
===

Flat-folding bike helmet called Morpher https://www.morpherhelmet.com/

[Polyanswer Head/Skull Protective Cap](https://www.amazon.com/gp/product/B07G7G33N4)

Folding bike such as the Brompton but also see Amazon for cheaper ones 

## tutorial in SF
https://sf.curbed.com/2017/9/19/16330162/cycling-sf-how-to-bikes-guide | Biking in San Francisco: A beginner’s guide to cycling - Curbed SF
https://www.walkuplawoffice.com/2018/10/12/most-dangerous-areas-for-san-francisco-bicyclists-new-study/ | Most Dangerous Areas for San Francisco Bicyclists [New study] | Walkup, Melodia, Kelly & Schoenberger
https://sfbike.org/events/category/education/adult-classes/scc-1/ | Upcoming Events
https://sfbike.org/events/category/education-2/adult-classes/ | Upcoming Events
https://data.sfgov.org/Transportation/Map-of-Speed-Limits/ttcm-fwt2 | Map of Speed Limits | DataSF | City and County of San Francisco

## rental in SF
http://thebikehut.org/ | The San Francisco Bike Hut | bikes bikes and more

## gears & fundamentals
biking gear higher speeds - Google Search
* https://www.purecycles.com/blogs/bicycle-news/96200007-gears-and-shifting-101-3-speed-vs-8-speed | Gears and Shifting 101: 3-Speed vs. 8-Speed – Pure Cycles
* https://www.rei.com/learn/expert-advice/bike-gears-and-shifting.html | Bike Gears: The Basics of Shifting | REI Expert Advice

## bike models
breezer liberty nike - Google Search
* rented this model 2019-07
* shimano 8 and 3 speed - Google Search

## laws
https://bayareabicyclelaw.com/safety-laws/scooters-sidewalks/ | Are Electric Scooters Allowed on Sidewalks in California? - Bay Area Bicycle Law

## pickup by uber
https://www.reddit.com/r/uber/comments/6fdkgp/uber_and_bicycles/ - try UberXL
UberPedal - discontinued; see http://rideordriveuber.com/uber-pedal/
