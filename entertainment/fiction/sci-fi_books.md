
https://www.barnesandnoble.com/blog/sci-fi-fantasy/best-science-fiction-fantasy-books-2018/
* ended up w/ Vita Nostra; when over, read http://www.scifipulse.net/in-review-vita-nostra/
* Digital, or Brevis Est is second & Migrant, or Brevi Finietur is third per https://scifiportal.eu/sergey-and-marina-dyachenkos-vita-nostra-english-translation/

Uprooted - accidentally got Spinning Silver

[The Dispossessed](https://en.wikipedia.org/wiki/The_Dispossessed)

## new
http://www.isfdb.org/ | The Internet Speculative Fiction Database
https://www.google.com/search?hl=en&q=The%20Stars%20My%20Destination | The Stars My Destination - Google Search
http://www.isfdb.org/cgi-bin/stats.cgi?12 | Top 100 Novels as Voted by ISFDB Users
https://classicsofsciencefiction.com/ | Classics of Science Fiction – Discover the most remembered science fiction stories
https://en.wikipedia.org/wiki/The_Year%27s_Best_Fantasy_Stories_(series) | The Year's Best Fantasy Stories (series) - Wikipedia
https://en.wikipedia.org/wiki/Ballantine_Adult_Fantasy_series | Ballantine Adult Fantasy series - Wikipedia
https://en.wikipedia.org/wiki/The_Worm_Ouroboros | The Worm Ouroboros - Wikipedia
https://en.wikipedia.org/wiki/Titus_Groan | Titus Groan - Wikipedia
https://en.wikipedia.org/wiki/A_Voyage_to_Arcturus | A Voyage to Arcturus - Wikipedia
https://www.reddit.com/r/audiobooks/comments/2xt4pj/whats_the_best_narrated_audiobook_youve_listened/?utm_source=amp&utm_medium=comment_list | What's the best narrated audiobook you've listened to? : audiobooks

### old
Source of ideas:

https://en.wikipedia.org/wiki/Category:Science_fiction_novels_by_decade
https://www.goodreads.com/list/show/75182.Science_Fiction_2010_2019_[https://en.wikipedia.org/wiki/Damon_Knight_Memorial_Grand_Master_Award](https://en.wikipedia.org/wiki/Damon_Knight_Memorial_Grand_Master_Award)
http://en.wikipedia.org/wiki/Science_Fiction_and_Fantasy_Writers_of_America - has a Nebula Award
http://en.wikipedia.org/wiki/World_Science_Fiction_Society - Hugo Award
[Cyberpunk Reading List](http://www.goodreads.com/list/show/11277.Cyberpunk_Reading_List#1133491)

### next novels - move these librarything wishlist
https://en.wikipedia.org/wiki/A_Fire_Upon_the_Deep
https://en.wikipedia.org/wiki/Consider_Phlebas
https://en.wikipedia.org/wiki/Vorkosigan_Saga

### authors / series
[https://en.wikipedia.org/wiki/Commonwealth_Saga](https://en.wikipedia.org/wiki/Commonwealth_Saga) 
Jack Finney
Walter M. Miller
Herman Hasse
Algis Brudy - Rogue Moon
Theodore Sturgeon - To Here and the Easel
Aristo - Orlando Furioso
Robert S. Richardson
James Gunn
Stanley Weinbaum
The Cyberiad by Stanislaw Lem
 - see in particular A._E._van_Vogt (book: Slan), L. Sprague de Camp (book: Lest Darkness Fall alternate history), Philip José Farmer (book/series: World of Tiers)

The Stars My Destination - 


#### rejected
Bruce Sterling - passing based on https://www.amazon.com/gp/customer-reviews/R1AJ9XW5AILSID/ref=cm_cr_dp_d_rvw_ttl?ie=UTF8&ASIN=0441030955
