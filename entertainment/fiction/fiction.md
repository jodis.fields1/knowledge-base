[https://en.wikipedia.org/wiki/Nevil_Shute](https://en.wikipedia.org/wiki/Nevil_Shute) - a town like Alice

Finish Gene Wolfe books

## Sources
http://www.free-ebooks.net is good for free stuff
http://librivox.org/ is good overview
http://www.audiobooktreasury.com is where I got Sense and Sensibility (librivox recording)
http://theaudiobookbay.com - signed up for this to get The Book of Swords audiobook
various torrents sites - I used bitsnoop.com for paksenarrion

## Finance
http://www.bus.lsu.edu/academics/finance/faculty/dchance/miscprof/financenovels.htm
Theodore Drieser is king at this
Horatio Alger, Jr. - rags-to-riches stories are sorta similar

https://www.goodreads.com/book/show/26292.The_Chairman

## Ideas
https://en.wikipedia.org/wiki/List_of_best-selling_books
Poul Anderson - The People of the Wind

Possibly http://en.wikipedia.org/wiki/The_Demolished_Man an old classic

http://en.wikipedia.org/wiki/List_of_joint_winners_of_the_Hugo_and_Nebula_awards

## Read

Coldfire Trilogy - one of my favorite series!

Old Man's War - not really related

http://en.wikipedia.org/wiki/Lord_Dunsany - famous early writer

**Fantasy**

Apparently Robert E. Howard (creator of Conan, friend/contemp of Lovecraft) was the father of sword and sorcery genre

Lovecraft - read the  [plot summary of his story The Call of Cthulu](https://en.wikipedia.org/wiki/The_Call_of_Cthulhu#Plot_summary) on wiki

Obviously Ursula K. L. Guin

Quite into the 9 Princes of Amber by Zelazny

See http://bestfantasybooks.com/top25-fantasy-books-for-women.html for an interesting list

*World of Darkness*
White Wolf - see https://www.reddit.com/r/WhiteWolfRPG/
For a long time I was very into this, and to be honest I still am. Particularly interested in the Mage part and have read several of the novels; Tower of Babel (ISBN: 1565048539  [on Amazon](http://www.amazon.com/gp/product/1565048539/)) by John H. Steel now Gherbod Fleming is on my to-read list

**Classic**

Classic actually can be fun. Major example is John Update whose Rabbit and Bech series are fun and quick reads altho cynical about modern life.

## finance
Theodore Dreiser