## mine
Trimmed The Sounds of Poetry: A Brief Guide out of my wishlist as I don't have time for it now! 

Haikus - inspired by Audrey Benson

Pithy Ode to Rain
Rain! Rain! Please Don't Go!
I Love You So
You'll Seldom Come Another Day

Alternate
Rain! Rain! Love the rain!
When you go, you'll miss it so
...OK, maybe it is a pain

It's a bird, it's a plane, it's insurance!
In the gray cube farm
When catastrophe strikes, insurance is near!
Unless you're not here...

## others
Edna St. Vincent Millay
    * discovered thru Fitzgerald TV show
    * famous for [Renascence](https://en.wikipedia.org/wiki/Renascence_(poem))