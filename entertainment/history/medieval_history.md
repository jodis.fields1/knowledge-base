# Medieval history with British focus

**  
**

**  
**

**

**Medieval weapons**

Got into reading up on about fashion with   [17th Century Europe](http://www.costumes.org/classes/fashiondress/17thCent.htm "http://www.costumes.org/classes/fashiondress/17thCent.htm")

Also see https://en.wikipedia.org/wiki/Classification_of_swords where I learned about the smallsword which became the rapier as a dress sword; learned more about swords such as hand-and-half swords using a hand on the blade (with glove of course)

Cinquedea was a type of dress sword up to 28", also read up on the estoc which was a thrusting sword against plate

See mention of swordsmen and Richard Francis Burton below

Thibault's treatise is the famous 1628 rapier manual Academie de l'Espée

Longsword vs Katana, Rapier vs Scimitar, CUTTING VS THRUSTING?  - epic thread with all sorts of info including Youtube clip; also referred to Katana vs. Rapier: Another Fantasy Worth Considering

There Is No "Best Sword": If there were no firearms, they'd still be designing new swords - particularly interesting as he mentions that Vikings did leg cuts to win, with 70% of corposes showing them

The Medieval European Knight vs. The Feudal Japanese Samurai? - discusses the varieties of medieval warriors

  


**Continental Europe from Franks to France and Germany**

So the Carolingian Empire split in the 900s and France and the Holy Roman Empire arose

  


_Germany_

2014-05: started by searching 1500s and finding the  [Battle of Hemmingstedt](https://en.wikipedia.org/wiki/Battle_of_Hemmingstedt "https://en.wikipedia.org/wiki/Battle_of_Hemmingstedt") peasant rebellion focus on Germany->the Holy Roman Empire was empowered by Otto the Great (early emperor) but became split with prince-electors with special rights who voted on the Emperor but didn't have that much power over them, and sold off land to appease them. Various kingdoms arose. Prince-electors: Bohemia (Prague was seat in 1300s), Brandenburg, Rhine ( [Electoral Palatinate](https://en.wikipedia.org/wiki/Electorate_of_the_Palatinate "https://en.wikipedia.org/wiki/Electorate_of_the_Palatinate")), and Saxony. Infighting happened for example see  [War of the Succession of Landshut](https://en.wikipedia.org/wiki/War_of_the_Succession_of_Landshut "https://en.wikipedia.org/wiki/War_of_the_Succession_of_Landshut") but as noted in  [this r/askhistorians thread](http://www.reddit.com/r/AskHistorians/comments/1tpka4/the_holy_roman_empire_just_how_imperial_was_it/ "http://www.reddit.com/r/AskHistorians/comments/1tpka4/the_holy_roman_empire_just_how_imperial_was_it/") it was not super-common like Italy

Holy Roman Emperor sometimes grew up in Italy or something and were more catholic (see  [ House of Habsburg](https://en.wikipedia.org/wiki/House_of_Habsburg "https://en.wikipedia.org/wiki/House_of_Habsburg") which had title from 1438-1740); also see  [Thirty Years War](https://en.wikipedia.org/wiki/Thirty_Years%27_War "https://en.wikipedia.org/wiki/Thirty_Years%27_War") and  [France–Habsburg rivalry](https://en.wikipedia.org/wiki/France%E2%80%93Habsburg_rivalry "https://en.wikipedia.org/wiki/France%E2%80%93Habsburg_rivalry") and also rise of Capet-Bourbon dynasty (French); note that the House of Hanover arose from earlier Roman House of Este ( [ wiki](https://en.wikipedia.org/wiki/House_of_Este "https://en.wikipedia.org/wiki/House_of_Este")) and was also important

By 1740 Prussia (developed out of Brandenburg) and Austria division became stark (Austria owned more by the Holy Roman Emperor titular figure but exclusively southeast such as Austria, Hungry, etc while Prussia had real power of German land)

German Confederation in 1814 followed by German unification in 1871 (start of short-lived but powerful German Empire) which led to hubris and WWI

[ Junkers](https://en.wikipedia.org/wiki/Junker "https://en.wikipedia.org/wiki/Junker") (young nobles) started pretty much conquering the area

  


_France_

Hugh Capet took over after the Carolingian ended beginning the  [ Capetian dynasty](https://en.wikipedia.org/wiki/Capetian_dynasty "https://en.wikipedia.org/wiki/Capetian_dynasty")

**

**  
**

**England** _  
_

Barry Lyndon is a great movie about this time; from it I learned about The Hellfire Club (an 18th century 'immoral society') and I also noticed that a proposed hard cider tax was controversial, confirming that hard cider was a popular drink back then

Vikings TV show might be pretty interesting but Wikipedia says it is very inaccurate; pretends Vikings are ignorant of England!

Fleet Prison is also interesting, and includes a predecessor of Marx Charles Hall

  


The invention of the career ladder (2013) - fun BBC news picture of white-collar work in 1850s

  


_Demography_

Medieval demography ( [ wiki](https://en.wikipedia.org/wiki/Medieval_demography "https://en.wikipedia.org/wiki/Medieval_demography")) puts the place into context, not sure how reliable this chart  [Historical Population of United Kingdom, 43 AD to Present](http://chartsbin.com/view/28k "http://chartsbin.com/view/28k") is

  


_Late medieval_

History of gentleman is fascinating  [ http://en.wikipedia.org/wiki/Gentleman](http://en.wikipedia.org/wiki/Gentleman "http://en.wikipedia.org/wiki/Gentleman")note that England lacked a "nobilary prefix" like the rest of Europe; also funny that there was a gentleman of the bedchamber  [ http://en.wikipedia.org/wiki/Gentleman_of_the_bedchamber](http://en.wikipedia.org/wiki/Gentleman_of_the_bedchamber "http://en.wikipedia.org/wiki/Gentleman_of_the_bedchamber")

Some family names are particularly famous, such as http://en.wikipedia.org/wiki/Strickland_%28surname%29

major electoral reform in  [ http://en.wikipedia.org/wiki/Reform_Act_1832](http://en.wikipedia.org/wiki/Reform_Act_1832 "http://en.wikipedia.org/wiki/Reform_Act_1832")which gave voters to those with assets over 10 pounds, which was a lot back then

Modern gentleman:  [ http://en.wikipedia.org/wiki/Sigma_Alpha_Epsilon](http://en.wikipedia.org/wiki/Sigma_Alpha_Epsilon#The_True_Gentleman "http://en.wikipedia.org/wiki/Sigma_Alpha_Epsilon#The_True_Gentleman")#The_True_Gentleman is pretty good

Reading  [Name some famous European swordsmen, please](http://boards.straightdope.com/sdmb/showthread.php?t=322856 "http://boards.straightdope.com/sdmb/showthread.php?t=322856") was enlightening, lead me to badass  Richard Francis Burton

The gentlemen insisted on search warrants but really these were handled through a writ of assistance http://en.wikipedia.org/wiki/Writ_of_assistance

  


_Early medieval_

Mass invasions - possible by Vikings and others but wiki's  [English people](https://en.wikipedia.org/wiki/English_people "https://en.wikipedia.org/wiki/English_people") cites genetic evidence saying no 

Regarding King Arthur ( [questionably real, but Riothamus is most interesting](https://en.wikipedia.org/wiki/Historical_basis_for_King_Arthur#Riothamus "https://en.wikipedia.org/wiki/Historical_basis_for_King_Arthur#Riothamus")), look at the list of King of Britons ( [ wiki](https://en.wikipedia.org/wiki/King_of_the_Britons "https://en.wikipedia.org/wiki/King_of_the_Britons"))

Starting from  [ http://en.wikipedia.org/wiki/Rhys_ap_Gruffydd](http://en.wikipedia.org/wiki/Rhys_ap_Gruffydd "http://en.wikipedia.org/wiki/Rhys_ap_Gruffydd") I went on to read about Henry I (a scholar!) son of William the Conqueror who took over England from the Anglo-Saxon House of Wessex in the Battle of Hastings which demonstrated cavalry superiority until the Battle of Crecy in 1346, although note that Harold was severely damaged after destroying a Norwegian army and the last great Viking Harald Hardrada, 

Henry II who laid the foundation of common law, and followed by Richard the Lionheart!; note that they seemed adopt some  [ http://en.wikipedia.org/wiki/Anglo-Saxon](http://en.wikipedia.org/wiki/Anglo-Saxon "http://en.wikipedia.org/wiki/Anglo-Saxon") legal systems

The English army was housecarls professional infrantry

The story of Henry I allowing his granddaughters to be mutilated per [ http://en.wikipedia.org/wiki/Henry_I_of_England](http://en.wikipedia.org/wiki/Henry_I_of_England#Activities_as_a_king "http://en.wikipedia.org/wiki/Henry_I_of_England#Activities_as_a_king")#Activities_as_a_king is fascinating

another landmark event is  [ http://en.wikipedia.org/wiki/Provisions_of_Oxford](http://en.wikipedia.org/wiki/Provisions_of_Oxford "http://en.wikipedia.org/wiki/Provisions_of_Oxford") forced on Henry III

  


_Genetics_

Genetic history of the British Isles ( [wiki](https://en.wikipedia.org/wiki/Genetic_history_of_the_British_Isles "https://en.wikipedia.org/wiki/Genetic_history_of_the_British_Isles")), also found this thread  [Genetics of the British and Irish people](http://www.eupedia.com/forum/threads/24907-Genetics-of-the-British-and-Irish-people "http://www.eupedia.com/forum/threads/24907-Genetics-of-the-British-and-Irish-people") with lots of discussion

Speculation at A genetic condition could explain much of Irish history (2012), also see Culture–gene coevolution of individualism–collectivism and the serotonin transporter gene (2009) which was covered in Why the British are free-thinking and the Chinese love conformity: It's all in the genes claim scientists (2012) 

  


**Medieval courts**

[The Tudors](http://www.historyextra.com/feature/tudors-time-it%E2%80%99s-political "http://www.historyextra.com/feature/tudors-time-it%E2%80%99s-political")\- apparently somewhat accurate? after watching it I believe that to be the case    


  
Historical Dictionary of Late Medieval England: 1272-1485 - p134 has an overview of the institution and its history, with the name appearing 1470, served as residence, seat of govt, treasury, and military center; two groups include the domus (staff and administrators) and familia regis (nobility); Edward II's staff wrote up an Ordinance splitting the domus into five units: steward keeper (chief), controller of the household (domestic), chamberlain (King's chamber), and cofferor (financial); Order of the Garter founded with 26 knights in 1340; in 1466 jousting regulations were issued; 

  
[The County Courts of Medieval England, 1150-1350](http://press.princeton.edu/titles/1675.html "http://press.princeton.edu/titles/1675.html")\- Palmer's treatise reviewed in  [The Medieval English County Court](http://www.jstor.org/discover/10.2307/1288421 "http://www.jstor.org/discover/10.2307/1288421");    
  
The Making of a Court Society: Kings and Nobles in Late Medieval Portugal - page 1 discusses the contribution of Norbert Elias with 1969 book    
  
The Princely Court: Medieval Courts and Culture in North-West Europe, 1270-1380 - discusses on p17 critiques of Elias    
  
The Fortunes of the Courtier - book about Castiglione's Cortegiano, or Book of the Courtier which was published 1528 in Italy as a guide for a courtier

  


[Islam?s Medieval Underworld](http://blogs.smithsonianmag.com/history/2013/07/islams-medieval-underworld/ "http://blogs.smithsonianmag.com/history/2013/07/islams-medieval-underworld/") - cool setting, name of the gang was the Banu Sasan, refers to Chaucer's Pardoner as noting the English equivalent, five separate categories of thug, from the housebreaker to out-and-out killers such as the sahib ba?j, the ?disemboweler and ripper-open of bellies,? and the sahib radkh, the ?crusher and pounder? who accompanies lone travelers on their journeys and then, when his victim has prostrated himself in prayer, ?creeps up and hits him simultaneously over the head with two smooth stones.?

  

