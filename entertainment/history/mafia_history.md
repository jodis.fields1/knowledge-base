# Mafia history

Inspired by   [Mob mentality](http://www.theguardian.com/film/2006/apr/22/mafia "http://www.theguardian.com/film/2006/apr/22/mafia") (2013) by John Patterson 

[Apalachin Meeting](https://en.wikipedia.org/wiki/Apalachin_Meeting "https://en.wikipedia.org/wiki/Apalachin_Meeting") - good detailed overview of a landmark mafia event, mentions Vito Genovese and Carlo Gambino   

[Meyer Lansky](https://en.wikipedia.org/wiki/Meyer_Lansky "https://en.wikipedia.org/wiki/Meyer_Lansky") - the real Hyman Roth of the mafia; lost his fortune when Cuba nationalized it

[ Kiss of death (mafia)](https://en.wikipedia.org/wiki/Kiss_of_death_%28mafia%29 "https://en.wikipedia.org/wiki/Kiss_of_death_%28mafia%29") - seeing this got me curious about kissing customs, see  [cheek kissing](https://en.wikipedia.org/wiki/Cheek_kissing "https://en.wikipedia.org/wiki/Cheek_kissing") and  [Customs and etiquette in Italy](https://en.wikipedia.org/wiki/Customs_and_etiquette_in_Italy "https://en.wikipedia.org/wiki/Customs_and_etiquette_in_Italy")

[John Gotti](https://en.wikipedia.org/wiki/John_Gotti "https://en.wikipedia.org/wiki/John_Gotti") - three mistrials and acquittals! wow  
