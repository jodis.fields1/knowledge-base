# History

The Rise of the West: A History of the Human Community; with a Retrospective Essay - this would be a good book

Wallchart of World History (Revised): From Earliest Times to the Present - wallchart is interesting

The Sources of Social Power: Volume 1, A History of Power from the Beginning to AD 1760 - sounds fascinating

The Lessons of History by the Durants - a summary of the huge and celebrated The Story of Civilization

Business history around the world - interesting subset which I do not have time for right now

  


**Universe and Earth history**

[The history of our world in 18 minutes | David Christian](https://www.youtube.com/watch?v=yqc9zX04DXs)

Chronology of the Universe ( [ wiki](https://en.wikipedia.org/wiki/Chronology_of_the_universe "https://en.wikipedia.org/wiki/Chronology_of_the_universe")) - started around 14t years ago

History of the Earth ( [ wiki](https://en.wikipedia.org/wiki/History_of_the_Earth "https://en.wikipedia.org/wiki/History_of_the_Earth")) - note Cambrian explosion about 500m years ago, also Great Oxygenation Event around 3b years ago

**  
**

**Wells and environmental health**

"Poisoning the well" originated with allegations against Jews in the 13th century per various sources (e.g. p763 of Antisemitism: A Historical Encyclopedia and p267 of Medieval Jewish Civilization)

Per Genesis 29:1-14 as noted in The IVP Bible Background Commentary: Genesis--Deuteronomy there were often stones over wells to guard against contamination and perhaps even hide a well

  


**Business association history**

Meir Kohn is the best in this area; see [Merchant Associations in Pre-Industrial Europe (2003)](https://www.ubernote.com/webnote/client/papers.ssrn.com/sol3/papers.cfm?abstract_id=427763 "https://www.ubernote.com/webnote/client/papers.ssrn.com/sol3/papers.cfm?abstract_id=427763")for a decent overview or better yet [The Origins of Western Economic Success: Commerce, Finance, and Government in Preindustrial Europe](http://www.dartmouth.edu/%7Emkohn/orgins.html "http://www.dartmouth.edu/%7Emkohn/orgins.html")and [How and Why Economies Develop and Grow: Lessons from Preindustrial Europe and China](http://www.dartmouth.edu/%7Emkohn/how.html "http://www.dartmouth.edu/%7Emkohn/how.html")from his homepage at [ http://www.dartmouth.edu/~mkohn/](http://www.dartmouth.edu/%7Emkohn/ "http://www.dartmouth.edu/%7Emkohn/");

  


**United States history**

Strauss–Howe generational theory - quirky horoscopish theory about generations and their characteristics

_  
_

_Reconstruction_

After the Civil War was mysterious to me; reading up on Andrew Johnson on wiki in particular I learned a lot; also looked at the Dred Scott case which precipitated the Civil War; the Freedmen's Bureau and historically black colleges; how amendments passed over the south state objections (answer: duress)

  


_Vietnam War_

See http://en.wikipedia.org/wiki/Conscription_in_the_United_States#Vietnam_War for an overview of how many people were drafted

  


1960s: Kennedy elected in 1960 against Nixon in tight race with allegations of fraud particularly around Daley in Chicago (perhaps made Nixon more cynical); Johnson took over next and won against Goldwater in landslide

  


**Conspiracies or intrigue**

Back in the day I read Shadows of Power: The Council on Foreign Relations and American Decline by James Perloff which got me started in that area; John Birch Society book is the king; think I read No One Dare Call It a Conspiracy by Gary Allen (1972); Georgia Tech apparently discusses conspiracy theories per [ http://conspiracytheories.lmc.gatech.edu/index.php/Conspiracies_of_the_John_Birch_Society](http://conspiracytheories.lmc.gatech.edu/index.php/Conspiracies_of_the_John_Birch_Society "http://conspiracytheories.lmc.gatech.edu/index.php/Conspiracies_of_the_John_Birch_Society"); <http://rationalwiki.org/wiki/Bilderberg_Group>is a good overview

  


the story of Vince Foster at [ http://www.nytimes.com/1993/08/22/us/life-undone-special-report-portrait-white-house-aide-ensnared-his-perfectionism.html?pagewanted=all&src=pm](http://www.nytimes.com/1993/08/22/us/life-undone-special-report-portrait-white-house-aide-ensnared-his-perfectionism.html?pagewanted=all&src=pm "http://www.nytimes.com/1993/08/22/us/life-undone-special-report-portrait-white-house-aide-ensnared-his-perfectionism.html?pagewanted=all&src=pm")is sad and interesting

**   
**

**Intellectual history** \- inspired by math, see math history note

[ Proto-writing](https://en.m.wikipedia.org/wiki/Proto-writing "https://en.m.wikipedia.org/wiki/Proto-writing") - it appears that there really wasn't much happening in Northern Europe besides Phoenician influences

[Literacy, reading, and writing in the medieval West](http://www.heuristiek.ugent.be/sites/default/files/historiographical%20essay.pdf "http://www.heuristiek.ugent.be/sites/default/files/historiographical%20essay.pdf") (2000) - skimmed this but didn't seem very definitive; Normans used writing out of necessity in England but generally warriors thought it beneath them perhaps

[Dark Ages (historiography)](https://en.m.wikipedia.org/wiki/Dark_Ages_%28historiography%29 "https://en.m.wikipedia.org/wiki/Dark_Ages_%28historiography%29") - couple neat graphs and tables in history section; wonder how the production of manuscripts was calculated; major source of literature in   [Patrologia Latina](https://en.m.wikipedia.org/wiki/Patrologia_Latina "Patrologia Latina")

[Greek scholars in the Renaissance](https://en.m.wikipedia.org/wiki/Greek_scholars_in_the_Renaissance "https://en.m.wikipedia.org/wiki/Greek_scholars_in_the_Renaissance") - when Constantinople was sacked by Crusaders in 1453 apparently intellectuals fled to Western Europe and sowed the seeds of the Renaissance

  


**Ancient**   
[ http://en.wikipedia.org/wiki/Bronze_Age_collapse](http://en.wikipedia.org/wiki/Bronze_Age_collapse "http://en.wikipedia.org/wiki/Bronze_Age_collapse")is an interesting story from around 1200 BC

[ http://en.wikipedia.org/wiki/Jewish_history](http://en.wikipedia.org/wiki/Jewish_history "http://en.wikipedia.org/wiki/Jewish_history") what a rough history, but so amazing that they stuck together in some ways

  


_Decline of Rome_ \- 2014-05 read  [Decline of the Roman Empire](https://en.m.wikipedia.org/wiki/Decline_of_the_Roman_Empire "https://en.m.wikipedia.org/wiki/Decline_of_the_Roman_Empire") and  [Late Antiquity](https://en.m.wikipedia.org/wiki/Late_Antiquity "Late Antiquity")

Plague seems most likely (?), lead and funny accounting are also contributing factors. One article says Rome dropped from nearly a million people to 30,000 people in a few hundred years!
