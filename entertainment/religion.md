# Religion

2014-07: looked into megachurches, inspired by glancing at AlaskaDispatch article  [Chris Thompson: Try something different for church this weekend](http://www.adn.com/article/20140726/chris-thompson-try-something-different-church-weekend "http://www.adn.com/article/20140726/chris-thompson-try-something-different-church-weekend") (2014) summarizing churches in AK

## Megachurches (US Protestant)

Defined as 2,000+ attendance (not including Catholic parishes) and supposedly there are around 1,700 in US with around 10% of Christian churchgoers (?); apparently they are still growing with major ones including Saddleback Church (Rick Warren), Lakewood Church (Joel Osteen), Elevation Church (Steven Furtick), Experience Life Church (Chris Galanos), and Hillsong NYC (Carl Lentz)

Leadership Network produes a report and posted  [How Many Megachurches](http://leadnet.org/blog/post/how_many_megachurches); see also  [Database of Megachurches in the U.S.](http://hirr.hartsem.edu/megachurch/database.html) by Hartford Institute for Religion Research

Highly personality-driven so a couple have failed; see for example Crystal Cathedral and Cathedral of Tomorrow

### Alaska

ChangePoint - 3,300 in Anchorage

Anchorage Baptist Temple - around 2,200 in Anchorage

Juneau Christian Center - ??; has sermons online altho no financial information up

Crossroads Church - Juneau church which meets at Harborview

  

