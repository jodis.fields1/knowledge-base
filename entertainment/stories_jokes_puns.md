from https://thoughtcatalog.com/melanie-berliet/2015/09/40-dumb-jokes-based-on-smart-wordplay-that-will-make-you-laugh-out-loud/

1. What’s the difference between snowmen and snowwomen?
Snowballs.

2. Why can’t you trust an atom?
Because they make up literally everything.

3. How do fish get high?
Seaweed.

4. Did you hear about the kidnapping at school?
Everything’s fine. He woke up.

5. What does a grape say after it’s stepped on?
Nothing. It just lets out a little wine.

6. Why don’t teddy bears ever order dessert.
Because they’re always stuffed.

7. What do you call a bear with no teeth?
A gummy bear.

8. What happens when a frog’s car breaks down?
It gets toad away.

9. I never wanted to believe that my Dad was stealing from his job as a road worker.
But when I got home, all the signs were there.

10. What’s the difference between roast beef and pea soup?
Anyone can roast beef but nobody can pee soup!

11. Did you hear about the guy who broke both his left arm and left leg?
He’s all right now.

12. What do you call a midget psychic who just escaped from prison?
A small medium at large.

13. What does a cannibal do after he dumps his girlfriend?
Wipes his ass.

14. Why does Humpty Dumpty love autumn?
Because he had a great fall.

15. People wonder why I call my toilet “the Jim” instead of “the John.”
I do it so I can say “I go to the Jim first thing every morning.”

16. I was wondering why the ball kept getting bigger and bigger…
And then it hit me.

17. I went to the bank the other day and asked the teller to check my balance.
The bitch pushed me, but I couldn’t really blame her.

18. What do computers snack on?
Microchips.

19. How come oysters never donate to charity?
Because they’re shellfish.

20. What did the big chimney say to the little chimney?
You’re too young to smoke.

21. What do you call two Mexicans playing basketball?
A Juan on Juan.

22. What’s the tallest building in the world?
The library, cause it has the most stories.

23. How do trees get online?
They log in.

24. Why did the scarecrow keep getting promoted?

Because he was outstanding in his field.

25. What car does Jesus drive?
A Christler.

26. Money doesn’t grow on trees, right?
So why does every bank have so many branches?

27. Why did the pig leave the party early?
Because everyone thought he was a boar.

28. Why shouldn’t you write with a broken pencil?
There’s no point.

29. Two men broke into a drugstore and stole all the Viagra.
The police better be on the lookout for two hardened criminals.

30. Why are barns so noisy?
Because all the cows have horns.

31. What do you call someone wearing a belt with a watch attached to it?
A waist of time.

32. What’s the difference between a teacher and a train?
One says, “Spit out your gum” and the other says, “Choo choo choo.”

33. What did the janitor yell after he jumped out of the closet?
“Supplies!”

34. How can you get four suits for a dollar?
Buy a deck of cards.

35. What’s so great about being a hitman?
They all kill it.

36. Why didn’t the melons get married?
Because they cantaloupe.

37. What do you say to a drunk who walks into a bar with jumper cables around his neck?
“You can stay. Just don’t try to start anything.”

38. A man got hit in the head with a can of Coke.
Thank goodness it was a soft drink.

39. What’s the difference between a cat and a complex sentence?
A cat has claws at the end of its paws. A complex sentence has a pause at the end of its clause.

40. If April showers bring May flowers, what comes next in June?
Pilgrims.