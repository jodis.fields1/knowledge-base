[Goog](Goog) Hacks 100 Industrial-Strength Tips & Tools

## taxonomies
[Melvil Decimal System](https://www.librarything.com/mds/6)

## similar sites
bibsonomy - 

aboutus.org

[http://www.similarsites.com/](http://www.similarsites.com/)

[sitejabber.com](http://sitejabber.com)

sitereapture.com (signed up for topalternative.com); dozens of other ones are also around

similarsitesearch.com plus

note that SimilarGroup runs apparently a few of these websites see http://www.crunchbase.com/company/similarweb and http://en.wikipedia.org/wiki/SimilarGroup

## Search

http://www.findouter.com/ is the ultimate directory

Learned a fair amount about these things in the Not Just Googlewebinar by Katie Fearer: meta-engines include dogpile, surfwax, clusty, graball

Collecta - ?

Rollyo - custom search engine

Thumbshots - how different ones work

Wolfram Alpha and Exalead

*Engines*

Google- fix the tracking as noted at  [Googlecopy link location annoyance?](http://webapps.stackexchange.com/questions/31028/google-copy-link-location-annoyance) by installing the greasemonkey script 

Bing, DuckDuckGo

http://www. google.com/cse/manage/all - GoogleCustom searchlets you add certain websites to searchthrough only

*Directories*

The Arizona University Research Guides is pretty cool http://libguides.asu.edu/index.php?gid=3074

Open graph protocol  -  interface with Facebook. I skimmed  What You Need to Know About Open Graph Meta Tags for Total Facebook and Twitter Mastery

## Google News meta tags

http://www.noratol.nl/webdesign/html/step19.php on meta tags

[http://en.wikipedia.org/wiki/Meta_element](http://en.wikipedia.org/wiki/Meta_element)

In Nov 2010 Google announced new meta tags for "original-source" and "syndication-source" http://googlenewsblog.blogspot.com/2010/11/credit-where-credit-is-due.html - syndication-source is like canonical but not preferred

Sometime in 2011 the "standout" tag was introduced http://support.google.com/news/publisher/bin/answer.py?hl=en&answer=191283; could not find articles on Google News which use this

Read the following background:

http://www.searchenginechronicle.com/source-syndication-meta-tag-review/ - no comment section, not real interesting

http://blog.sfgate.com/abraham/2010/11/22/google-news-new-meta-tags-kill-news-partnership-deals/ - cynical of the original-source tag and thinks it is too harsh; I think he's wrong
