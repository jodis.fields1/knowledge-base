See http://www.dailyfinance.com/2011/12/08/how-durbins-debit-card-fee-cut-backfired-on-small-merchants/ and http://m.startribune.com/business/?id=136189108&c=y to see discussion on elimination of discounts on small transactions; rather, the full 21 cents is charged.

http://www.cardfellow.com/blog/issuing-bank/ is a good overview; great website

Industry

http://en.wikipedia.org/wiki/Payment_card_industry is an overview

Card associations (http://en.wikipedia.org/wiki/Card_association) which are also called "bankcard networks" are at the heart - see http://www.paymentsapi.com/2010/07/payments-101-card-association/ and http://www.investopedia.com/terms/b/bank-card-association.asp

Main trade association is Electronic Transactions Association; NACHA - The Electronic Payments Association is the ACH org; http://www.greensheet.com/gs_archive.php?issue_number=040301&story=13 mentions a couple others

http://en.wikipedia.org/wiki/Verifone is the only point of sale company I know of atm; from there you can find it is crowded

Class action lawsuit

In 2012 a big class-action lawsuit came down - see e.g. http://www.nrf.com/modules.php?name=News&op=viewlive&sp_id=1439 (2012 comment)

In 2002 there was a big one; see http://www.justice.gov/atr/cases/f201200/201283.htm (US v. Visa & Mastercard); has overview of the industry

Cash discount or credit surcharge

The article at nerdwallet is rather fascinating http://www.nerdwallet.com/blog/2011/durbin-amendment-explained/ but it says that the retailer can have a surcharge. On the other hand, http://www.cardfellow.com/blog/how-the-dodd-frank-wall-street-reform-and-consumer-protection-act-financial-reform-affects-your-business/ says that only a discount can be offered; also notes that competition will mean that credit unions are not really protected. Specific discussion of this at http://www.cleveland.com/business/index.ssf/2011/05/can_stores_offer_a_discount_if.html. HBR discusses this issue: http://blogs.hbr.org/cs/2011/07/should_you_offer_different_pri.html and bankrate says cash discounts are pretty common http://www.bankrate.com/finance/personal-finance/pay-cash-and-ask-for-a-discount-1.aspx

http://www.merchantcouncil.org/merchant-account/operation/pass-fee-customer.php which may be dated says all the credit card contracts prohibit surcharges; not clear if the Durbin Amendment changed that

Processors keeping the money

This point is interesting: http://www.cardpaymentoptions.com/credit-card-processing-news/two-processors-disclose-durbin-amendment-savings-tips/. Also, while the savings are not required to go to merchants, the competition means they should eventually - see http://www.cardpaymentoptions.com/credit-card-processing-news/two-processors-disclose-durbin-amendment-savings-tips/

Credit union

Also, http://www.cardfellow.com/blog/how-the-dodd-frank-wall-street-reform-and-consumer-protection-act-financial-reform-affects-your-business/ says that because of exchange competition, credit unions are not really protected.

Durbin amendment impact

2011 credit union magazine article: http://www.cutimes.com/2011/12/18/in-a-first-interchange-gained-federal-regulation?t=debit-atm-shared-branching

2011 Durbin says it is being passed on http://www.legalnewsline.com/news/230757-durbin-c