See also investing/finance-legal/

## Advice
Ramit Sethi - learned thru Gabe, famous guy who wrote IWillTeachYouToBeRich.com, got a bunch of his  [blueprint pdfs](http://earn1k.com/privatelist/i-will-teach-you-to-be-rich-blueprints/ "http://earn1k.com/privatelist/i-will-teach-you-to-be-rich-blueprints/") 2014-10 and ended up at his private list

# investment apps
* TODO: PortfolioAnalyst from InteractiveBrokers
  * also maybe https://www.fundvisualizer.com/ and https://www.portfoliovisualizer.com/

## aggregators
* Fidelity - eMoney Advisor
* Personal Capital - looks like it might be better; has no InteractiveBrokerssupport unfortunately - Bill Harris former CEO of Intuit/Quicken is founder per http://investorjunkie.com/13093/personal-capital-review/
* NextCapital - got a free premium account, pretty basic
  * 2019: did a $30m series C; still decent
* FutureAdvisor - https://www.futureadvisor.com/ - looks like they support a few brokerages
  * checked 2019, doesn't let you do much without deposit
* Morningstar - unfortunately:
  *  no automatic sync; plus they have issues according to recent rants http://socialize.morningstar.com/NewSocialize/forums/p/308779/3276837.aspx#3276837;
  *  as of 2009 Morningstar X-Ray was free at T. Rowe Price website http://www.bogleheads.org/forum/viewtopic.php?t=47897

## potfolio analysis
See beancount portfolio analysis tool

https://www.portfolio-performance.info/portfolio/ | Portfolio performance
https://github.com/moneymanagerex/moneymanagerex | moneymanagerex/moneymanagerex: Money Manager Ex is an easy to use, money management application built with wxWidgets
https://github.com/OpenBankProject/OBP-API/issues/1089 | which banks/services are supported · Issue #1089 · OpenBankProject/OBP-API
https://github.com/omdv/robinhood-portfolio | omdv/robinhood-portfolio: Portfolio analyzer for Robinhood accounts
https://www.msolife.com/portfolio-analysis-with-python/ | Portfolio Analysis with Python: Move Over Excel » MSoLife
https://www.reddit.com/user/Schwab_Official/comments/9ywffe/hey_reddit_charles_schwab_team_here_with_a/ | Hey Reddit, Charles Schwab team here with a question. What's your approach to tracking gains and losses in your trading history and learning from your past? : Schwab_Official
https://money.stackexchange.com/questions/7247/where-can-i-find-open-source-portfolio-management-software | Page not found - Personal Finance & Money Stack Exchange
https://github.com/topics/portfolio-management | portfolio-management · GitHub Topics
http://www.sastibe.de/2018/05/2018-05-11-emacs-org-mode-rest-apis-stocks/ | Use Emacs Org Mode and REST APIs for an up-to-date Stock Portfolio
https://github.com/quantopian/pyfolio | quantopian/pyfolio: Portfolio and risk analytics in Python


## other
http://www.quotetracker.com/ - lots of people like it

Fund Manager Software - automatic sync with a long list per http://www.fundmanagersoftware.com/tintrtv.html

http://www.icarra2.com/ - old-fashioned desktop app

https://www.hedgeable.com/products/ - not really the same; not a lot of flexibility; however, has an alternative to M*'s fund rating system per http://www.fa-mag.com/fa-news/5699-upstart-unveils-alternative-to-morningstar-fund-rating-system.html
* shutdown, pivoted to https://www.hydrogenplatform.com/
* qplum by https://www.linkedin.com/in/mansisinghal also shut down; https://www.linkedin.com/in/marisa-palestino-586ba069/ was my relations contact