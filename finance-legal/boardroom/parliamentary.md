# Parliamentary

The Alaska State Library has an electronic copy of  Webster's New World Robert's Rules of Order Simplified and Applied, although it is apparently broken and downloads PDFs each time (at least in Firefox; can't even get as far in Internet Explorer).  The Downtown Juneau library has a copy.

  


Free 1876 version at http://www.gutenberg.org/ebooks/9097 \- read this entire thing, pretty interesting

  


Turns out Robert's Rules of Order, Revised refers to the 1915 version - which is in the public domain. See attached (also on Google Books but cannot be rotated in some key spots) - also see attached RobertsRules101.pdf for list of editions and a brief summary of differences.

  


The Standard Code of Parliamentary Procedure is a more modern version (see wiki for comparison); the 4th edition (published 1988) is available at UAS Egan.

  
  
  


  

