# Corporate law

See also Retail shareholder and activist investing has a lot on this as well

Another landmine: if <300 holders of record, a company can deregister from the SEC reporting

http://www.law.yale.edu/documents/pdf/two_systems_for_corporate_governance_v04.00.pdf

http://www.mwe.com/info/news/wp0604a.pdf

Red Flags for Fraud

## lawsuits
Securities Class Action ClearingHouse (SCAC) - ??

## Holding company and groups
See corporate group and holding company ( [wiki](https://en.wikipedia.org/wiki/Holding_company))

## Shareholder rights
https://www.investopedia.com/investing/know-your-shareholder-rights/

State law:
* [2009 California Corporations Code - Section 200-213 :: Chapter 2. Organization And Bylaws](https://law.justia.com/codes/california/2009/corp/200-213.html)

## Trade association
Professional Liability Underwriting Society's annual D&O Symposium

## Subsidiaries and firewalls

Of firewalls and subsidiaries: The right stuff for expanded bank activities - read this, focused on banks (universal bank versus BHC versus op-sub, favoring op-sub - looks like it didn't work out like that)

Bankruptcy in the Core and Periphery of Financial Groups: The Case of the Property-Casualty Insurance Industry - more applicable to my job; found counterintuitive results suggesting that claimaints against in non-core or non-flagship companies, with less explicit support, fared better than those in flagship companies.

Should those charged with corporate governance care about auditor offshoring? - interesting journal International Journal of Disclosure and Governance -  did not read

## Delaware law

http://en.wikipedia.org/wiki/Smith_v._Van_Gorkom was exceptionally important as it led to the explosion in D&O liability insurance and reduced liability; see http://www.insideinvestorrelations.com/articles/disclosure-regulation/17354/delaware-goes/;

## NYSE

NYSE's Listed Company Manual http://nysemanual.nyse.com/lcm/ - 303A has the rules; majority-owned companies ("controlled") can be exempt from certain rules such as 303A.1, 4, and 5 (majority independent directors) but they have to disclose that; revision proposal was just to refer to must comply with the disclosure requirements in Instruction 1 to Item 407(a) of Regulation S-K.

Proposal: 303A.3NYSE clarifies that a listed company must disclose a method for allinterested parties, not only shareholders, to communicate their concerns regarding the listed company to the presiding director or the non-management or independent directors as a group.

## NASDAQ
## Research articles
The Production of Corporate Law (1997) by William J. Carney - tried reading this a while back; seems to have some tables but not in the version I got

Corporate Governance Reforms in Continental Europe (2007) - take away is that there's self-dealing from those have control but not majority ownership

Handbook of Law and Economics, Volume 2, CORPORATE LAW AND GOVERNANCE by Becht, Bolton, and Roell - not so into activism, flawed

Questioning Authority: Why Boards Do Not Control Managers and How a Better Board Process Can Help (2012) - points out the obvious; good point about 70% of large company boards have combined CEO/Chairman roles