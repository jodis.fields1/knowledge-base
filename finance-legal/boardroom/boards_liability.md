# Boards and liability

**  
**

The Fountainhead on boards: "boards are one or two personalities amid a bunch of ballast" (paraphrasing) - very true

**  
**

**D &O**

**Nonprofits: Nonprofits Insurance Alliance Group which started around 1989 in California offers insurance solely for nonprofits, not available in Alaska**

**  
**

**Coverage risks: imputations are a big one; read D &O Liability Insurance: Coverage Risks for Innocent Directors and Officers points to Cutter & Buck, Inc. v. Genesis Ins. Co., 306 F. Supp. 2d 988, 1011 (W.D. Wash. 2004) where the innocent coinsureds lost coverage due to imputation of signer's knowledge to them**

**Look for Enhanced Side-A Only DIC to avoid insured-v-insured exclusions and better severability**

**  
**

**Follow Limiting Corporate Directors' Liability: Delaware's Section 102 (b)(7) and the Erosion of the Directors' Duty of Care to see a lot of articles**

**  
**

**Directors and Offi cers: Side A Only Coverage Working Through the Hype (2007) - pretty good article**

**  
**

**AS 10.20.151(d) notes that "the articles of incorporation may also contain a provision eliminating or limiting the personal liability of a director to the corporation for monetary damages for the breach of fiduciary duty as a director"; general code has a similar clause**

**  
**

**Dissent?**

**Why Board Members Need to Dissent - 2008, doesn't discuss letting it out publicly[Board Confidentiality – What Happens in the Board Room Stays in the Board Room](http://charitylawyerblog.com/2012/05/31/board-confidentiality-what-happens-in-the-board-room-stays-in-the-board-room/ "http://charitylawyerblog.com/2012/05/31/board-confidentiality-what-happens-in-the-board-room-stays-in-the-board-room/")\- 2013 article which argues that the duty of confidentiality ( <http://en.wikipedia.org/wiki/Duty_of_confidentiality>) is wrapped up in the duty of loyalty; [Top Ten Non-profit Governance Mistakes (From a Lawyer’s Perspective) mentions this as number 9](http://www.avvo.com/legal-guides/ugc/top-ten-non-profit-governance-mistakes-from-a-lawyers-perspective "http://www.avvo.com/legal-guides/ugc/top-ten-non-profit-governance-mistakes-from-a-lawyers-perspective") ; and this article [adds 5](http://charitylawyerblog.com/2009/10/05/top-15-non-profit-board-governance-mistakes-from-a-legal-perspective/ "http://charitylawyerblog.com/2009/10/05/top-15-non-profit-board-governance-mistakes-from-a-legal-perspective/"). [](http://business.uc.edu/content/dam/business/departments/finance/docs/Cassandra%20D.%20Marshall%20Are%20Dissenting%20Directors%20Rewarded.pdf "http://business.uc.edu/content/dam/business/departments/finance/docs/Cassandra%20D.%20Marshall%20Are%20Dissenting%20Directors%20Rewarded.pdf") [](http://business.uc.edu/content/dam/business/departments/finance/docs/Cassandra%20D.%20Marshall%20Are%20Dissenting%20Directors%20Rewarded.pdf "http://business.uc.edu/content/dam/business/departments/finance/docs/Cassandra%20D.%20Marshall%20Are%20Dissenting%20Directors%20Rewarded.pdf") [](http://business.uc.edu/content/dam/business/departments/finance/docs/Cassandra%20D.%20Marshall%20Are%20Dissenting%20Directors%20Rewarded.pdf "http://business.uc.edu/content/dam/business/departments/finance/docs/Cassandra%20D.%20Marshall%20Are%20Dissenting%20Directors%20Rewarded.pdf") [](http://business.uc.edu/content/dam/business/departments/finance/docs/Cassandra%20D.%20Marshall%20Are%20Dissenting%20Directors%20Rewarded.pdf "http://business.uc.edu/content/dam/business/departments/finance/docs/Cassandra%20D.%20Marshall%20Are%20Dissenting%20Directors%20Rewarded.pdf")** [](http://business.uc.edu/content/dam/business/departments/finance/docs/Cassandra%20D.%20Marshall%20Are%20Dissenting%20Directors%20Rewarded.pdf "http://business.uc.edu/content/dam/business/departments/finance/docs/Cassandra%20D.%20Marshall%20Are%20Dissenting%20Directors%20Rewarded.pdf") [](http://business.uc.edu/content/dam/business/departments/finance/docs/Cassandra%20D.%20Marshall%20Are%20Dissenting%20Directors%20Rewarded.pdf "http://business.uc.edu/content/dam/business/departments/finance/docs/Cassandra%20D.%20Marshall%20Are%20Dissenting%20Directors%20Rewarded.pdf") [](http://business.uc.edu/content/dam/business/departments/finance/docs/Cassandra%20D.%20Marshall%20Are%20Dissenting%20Directors%20Rewarded.pdf "http://business.uc.edu/content/dam/business/departments/finance/docs/Cassandra%20D.%20Marshall%20Are%20Dissenting%20Directors%20Rewarded.pdf")

[](http://business.uc.edu/content/dam/business/departments/finance/docs/Cassandra%20D.%20Marshall%20Are%20Dissenting%20Directors%20Rewarded.pdf "http://business.uc.edu/content/dam/business/departments/finance/docs/Cassandra%20D.%20Marshall%20Are%20Dissenting%20Directors%20Rewarded.pdf") [](http://business.uc.edu/content/dam/business/departments/finance/docs/Cassandra%20D.%20Marshall%20Are%20Dissenting%20Directors%20Rewarded.pdf "http://business.uc.edu/content/dam/business/departments/finance/docs/Cassandra%20D.%20Marshall%20Are%20Dissenting%20Directors%20Rewarded.pdf") [](http://business.uc.edu/content/dam/business/departments/finance/docs/Cassandra%20D.%20Marshall%20Are%20Dissenting%20Directors%20Rewarded.pdf "http://business.uc.edu/content/dam/business/departments/finance/docs/Cassandra%20D.%20Marshall%20Are%20Dissenting%20Directors%20Rewarded.pdf")

  


[](http://business.uc.edu/content/dam/business/departments/finance/docs/Cassandra%20D.%20Marshall%20Are%20Dissenting%20Directors%20Rewarded.pdf "http://business.uc.edu/content/dam/business/departments/finance/docs/Cassandra%20D.%20Marshall%20Are%20Dissenting%20Directors%20Rewarded.pdf") [Are Dissenting Directors Rewarded?](http://business.uc.edu/content/dam/business/departments/finance/docs/Cassandra%20D.%20Marshall%20Are%20Dissenting%20Directors%20Rewarded.pdf "http://business.uc.edu/content/dam/business/departments/finance/docs/Cassandra%20D.%20Marshall%20Are%20Dissenting%20Directors%20Rewarded.pdf")\- 2010; finds that they are the opposite of rewarded; mentions the 2004 change to Item 5.02 on the 8-K which requires disclosure of reasons for a board members resignation even if the director didn't request the letter (which were expanded in 2007) which led me to [Director Resignations: Agreeing to Differ](http://www3.gmiratings.com/wp-content/uploads/2012/10/GMIRatings_DirResignations_102012.pdf "http://www3.gmiratings.com/wp-content/uploads/2012/10/GMIRatings_DirResignations_102012.pdf")(2012); also see [Final Rule: Additional Form 8-K Disclosure Requirements](http://www.sec.gov/rules/final/33-8400.htm "http://www.sec.gov/rules/final/33-8400.htm"); [](http://www.jstor.org/stable/40686452 "http://www.jstor.org/stable/40686452") [](http://www.jstor.org/stable/40686452 "http://www.jstor.org/stable/40686452")

[](http://www.jstor.org/stable/40686452 "http://www.jstor.org/stable/40686452") [](http://www.jstor.org/stable/40686452 "http://www.jstor.org/stable/40686452") [](http://www.jstor.org/stable/40686452 "http://www.jstor.org/stable/40686452")

  


[](http://www.jstor.org/stable/40686452 "http://www.jstor.org/stable/40686452") [The Duty of Confidentiality and Disclosing Corporate Misconduct](http://www.jstor.org/stable/40686452 "http://www.jstor.org/stable/40686452")(1981) - did not read but on JSTOR; more recent topics include <http://blogs.law.harvard.edu/corpgov/2010/01/23/maintaining-board-confidentiality/>and [Practical Tips on Board Confidentiality](http://www.mofo.com/files/Uploads/Images/130312-Practical-Tips-on-Board-Confidentiality.pdf "http://www.mofo.com/files/Uploads/Images/130312-Practical-Tips-on-Board-Confidentiality.pdf")(2013)

  


**Inspecting books and records**

Siegler (2000) Cooperatives and Condominiums discusses Cohen v. Cocoline Products, Inc. in New York Law General. The only limit on the word “absolute” comes when you’re clearly trying to dive quite deep, like personnel records of lower-level staff or voting records of members (see  [this article from Davis-Stirling on limits](https://www.davis-stirling.com/tabid/1581/Default.aspx "https://www.davis-stirling.com/tabid/1581/Default.aspx")).  [Another article](http://www.berding-weil.net/articles/when-absolute-is-not.php "http://www.berding-weil.net/articles/when-absolute-is-not.php") notes that if you’re suing for monetary damages from the corporation while you’re a director, in California you’re probably not going to get the attorney’s defense memos on the case.
