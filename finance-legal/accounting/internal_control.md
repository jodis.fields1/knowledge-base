# Internal control
See for summaries of the small organization internal control guidance
* http://www.journalofaccountancy.com/Issues/2007/Mar/InternalControlGuidanceNotJustASmallMatter.htm
* http://www.accountingweb.com/blogs/cpapastr/today039s-world-audits/six-components-good-internal-control-systems-smaller-entities 
   http://www.coso.org/guidance.htm for the most notable publications by COSO; of the "paid" documents as of now, I have the following:

## Documents

Internal Control — Integrated Framework (1992) - plus 1994 addendum - the original authoritative document; attached.

Internal Control Issues in Derivatives Usage (1996) - do not have this.

Enterprise Risk Management — Integrated Framework (2004) - attached.

Internal Control over Financial Reporting — Guidance for Smaller Public Companies (2006) - have this altho the PDFs are secured.

Guidance on Monitoring Internal Control Systems (2009) - have discussion drafts: Vol2 is dated 2007 and Vol3 is dated 2008.

## Auditing

AU Section 380: The Auditor’s Communication With Those Charged With Governance - read this briefly