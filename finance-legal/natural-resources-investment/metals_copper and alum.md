# Metals (copper and alum)

2013-07: research originally motivated by comment about wind energy's copper requirement and Pebble Mine

## Copper versus aluminum substitution

Aluminium Is No Lightweight (2011) by Freas - good overview of substitition in various areas: "On a per pound basis, aluminium is actually more conductive than copper because it is about one third the weight. The challenge is that the diametre of the metal needs to increase in order to make aluminium work"; for a specific company, see General Cable's Stabiloy aluminum which can replace copper; CommScope (acquired by PE) sells GoundSmart (copper clad steel or CCS) for wind farm use in Bishop Hill;  Helukabel is also involved with a profile in AltEnergyMag; note that even with the high price of copper, other concerns and prices might dominate; 

Copper or aluminium? Which one to use and when? - says there's only a small subset of areas where either can be used

## Aluminum

http://en.wikipedia.org/wiki/Aluminum_wire

Southwire in particular is a big proponent: Aluminum Building Wire 40 Years Later, How to Convert From Copper to Aluminum Conductors, 

HSB has a couple articles: Fundamentals of Aluminum Conductors and Part 2  Part 2: Installation and Maintenance; mentions millions of fires from poor wiring

## Copper

From Copper Use for Wind Power, Infrastructure Seen Aiding Demand by Bloomberg I found Bernd Langner who wrote Understanding Copper, says substition is down 5 to 10 years

[From SolarPowerWorld](http://www.solarpowerworldonline.com/2012/07/study-says-solar-uses-more-copper-than-fossil-fuels/): Renewable energy sources such as wind, solar and geothermal typically use four to six times more copper than fossil fuels, according to a new market study. The study, Current and Projected Wind and Solar Renewable Electric Generating Capacity and Resulting Copper Demand, was conducted by BBF Associates & Konrad J.A. Kundig, Ph.D. and commissioned by the Copper Development Association (CDA)