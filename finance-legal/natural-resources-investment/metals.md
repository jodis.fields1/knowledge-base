# Metals

Lots about cleaning. Basically motivated by the question  [How can brown stains be removed from pots and pans?](http://cooking.stackexchange.com/questions/2880/how-can-brown-stains-be-removed-from-pots-and-pans) (at stackexchange)

## Science

[Learn about Metals](http://www.hometrainingtools.com/learn-about-metals-science-teaching-tip/a/1262/) - science tools website overview

_History_ 
with a regular campfire you can only get about 800 degrees; bloomers allowed higher, and then blast furnaces in the late middle ages per A Brief History of Iron and Steel Production by Joseph S. Spoerl

Tricks to distinguish:

[How to Tell Stainless Steel from Aluminum](http://www.warmtips.com/20051210.htm) - use the water test

How Can I Tell Whether a Pot is Stainless Steel or Aluminum? - aluminum sounds duller, slightly warmer to the touch, scratches more

## Finishing
Buffer - use a high-grade abrasive [as described at howstuffworks](http://home.howstuffworks.com/polish-metal-sandpaper.htm) with silicon carbic for soft metals and aluminum oxide for hard

## Stainless

Various types with funny names like austenitic 

For food grade, 304 also known as 18/8 has 18% chromium and 8% nickel and is most common and not magnetic; nickel could leak; in 400 series (cheapest) there is no nickel and it is magnetic; I've heard that in the 200 series lacking nickel (magnetic) manganese is substituted which can affect taste and is also magnetic. Also 300 is resistant to corrisoin from acids while 400 just resists corrosion from oxygen in the air and water per  [Learning About and Caring for Stainless Steel](http://www.julien.ca/en/home_refinements/additional_information/the_abcs_of_stainless_steel/index.html) (pretty good reference)


_Rust remover_  - Spotless Stainless Rust Remover and Protector

_Polish_ \- can use olive oil, flour, or professional polish per How to Polish Stainless Steel

_Stain remover_ - How to Clean Stainless Steel (Without Breaking the Bank) suggests Flitz, isopropyl, Barkeeper's, Weiman, etc. Citric acid is recommended for really tough stains. White vinegar is also acidic.

Specialty Steel Industry of North America (SSINA) - has a guide called the The Care and Cleaning of Stainless Steel: Designer Handbook

## Aluminum
 Wagner & Griswold is focused on this http://www.wag-society.org/cleaning.php

http://www.howtocleanstuff.net/how-to-clean-and-polish-aluminum/ \- suggestions

For removing the anodization, use sodium hydroxide (lye)  [as described at instructables](http://www.instructables.com/id/Removing-Anodizing-From-Aluminum-Quickly-and-Easil/?ALLSTEPS) lk

For cleaning, wirebrush attached to drill but also perhaps citric acid altho there is differing opinion

For dents, see http://www.ehow.com/how-does_4796759_removing-dents-sheet-metal.html

How to make Aluminum look new...? - good forum tips such as wirewheel and SOS pad; Best way to clean aluminum? suggests sandblasting first

One suggestion: "boil water and add one part vinegar to two parts of water. Then immerse the aluminium for a half hour to an hour"

http://www.widman.biz/uploads/Polishing_Stainless.pdf - good example of buffing and getting out dents