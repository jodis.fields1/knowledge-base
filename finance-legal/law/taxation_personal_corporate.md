# Taxation (including corporate)

https://www.irs.gov/e-file-providers/list-of-available-free-file-fillable-forms

## Overview

PRESENT LAW AND BACKGROUND RELATING TO TAXTREATMENT OF BUSINESS DEBT dated July 2011 JCX-41-11 https://www.jct.gov/publications.html?func=showdown&id=3803 - answered my question about deducting principal payments on debt; however, since the deductions for depreciation and interest reduce taxable net income and generate a lot of cash flow which could be used to pay down principal, it's not entirely clear if principal payments are really taxed?

Information Related to the Scope and Complexity of the Federal TaxSystem (2001) - GAO-01-301R study which identifies the number of forms in p39; 

## Personal finance

### IRAs
https://www.reddit.com/r/financialindependence/comments/gu3s72/purposely_taking_10_penalty_for_401k_early/

Individual 401(k) - 2020-02 spoke to an IRA retirement agent

Cannot do both traditional and Roth really

Traditional IRA - $5,500 or so deductible from income

Roth IRA - $5,000 or so not deductible
* https://www.reddit.com/r/personalfinance/comments/f5w8gy/ally_invest_is_refusing_to_make_a_same_year/fiayn6z/
* 1099-R, Form 8606 Part 3

529- per CNN Money, state-administered with possibly less flexibility and lifetime limits perhaps around $100k; savingforcollege.com seems to be the best resource which I found thru WSJ article What Is a 529 Expense? Take Our Test.

Coverdell ESA

HSA - requires a high-deductible plan, weakened by guidance per  [How Obamacare Will Make Health Savings Accounts More Costly](http://www.forbes.com/sites/theapothecary/2012/04/27/how-obamacare-will-make-health-savings-accounts-more-costly/ "http://www.forbes.com/sites/theapothecary/2012/04/27/how-obamacare-will-make-health-savings-accounts-more-costly/") where only employer contributions count

## Forgiven debt as income

Watch out - IRS can go after you http://www.nolo.com/legal-encyclopedia/ tax-consequences-settled-forgiven-debt-29792.html

## Wealth tax

Popular idea: In reading about this, I saw the article  [Why (and how) to taxthe super-rich](http://articles.latimes.com/2011/sep/20/opinion/la-oe-ackerman-wealth-tax-20110920) which says the proposed taxwould generate $70 billion, which seemed small and almost contacted the authors. This is a 2% taxon wealth in excess of $7.2 million. So clearly if that is correct the wealth taxcan't fix our problems... perhaps I should consider finding out more; authors are bruce.ackerman@yale.edu, anne.alstott@yale.edu

## Why prefer debt

Preference for debt is listed by http://www.fdcpa.com/debt.equity.htm as:

* interest expense is generally deductible, dividends are not;

* there is no U.S. withholding taxon principal repayment, and,

* the U.S. withholding taxon interest is generally less than that of dividends (for treaty partner creditors).

http://dealbook.nytimes.com/2012/02/28/for-corporations-u-s- tax-code-adds-to-debts-appeal/ discusses the taxpreference for debt from a higher-level


## Corrupt loopholes

Dems write tax reform wish list (2013) - McCain signed onto to deducting stock options

See http://www.villagevoice.com/2012-10-10/news/the-10-most-corrupt- tax-loopholes/full/

Accidental Tax Break Saves Wealthiest Americans $100 Billion - Walton grantor retained annuity trust, or GRAT

_Subsidies for oil_

See MLPs; note 2013 Bloomberg Businessweek had a story about this; originated with Kinder Morgan founder (?)

_Tax evasion and avoidance_

Long-Run Corporate TaxAvoidance (2005) - actually 2008 but this made me feel better as most companies do pay taxes

A Hedge Fund TaxDodge Uses Bermuda Reinsurers - http://www.businessweek.com/articles/2013-02-21/a-hedge-fund- tax-dodge-uses-bermuda-reinsurers and explains how to get around the “passive foreign investment companies" restriction

United States v. Woods (2013) - really liked this Supreme Court case as they hit an evader with a penalty due to their misstatement using cobra (current options bring rewards)

Tax haven - see wikipedia article for the investigation by a researcher

Bloomberg "Gimme Shelter" in 2013 says Delaware is the main domicile because subsidiaries are not taxed there; however, for personal accounts South Dakota is big per  [Tax-Friendly South Dakota Shelters Out-of-State Billions](http://www.bloomberg.com/infographics/2013-12-27/billionaires-find-on-shore-tax-haven-in-south-dakota.html) by Bloomberg \- Little Tax Haven on the Praire goes into more details on the indefinite dynasty trust

What's the difference between secret Swiss bank accounts and the Cayman Islands?

Cayman Islands - Romney moved a few million there

Belize - apparently the most secretive place according to PanamaLaw.org

Cyprus, Jersey, Luxembourg - less well-known

Norway, Netherlands - Dutch sandwich

## History

Obviously 1986 Act is the biggest

History of the US TaxSystem - from policyalmanac and perused

Internal Revenue Service Restructuring and Reform Act of 1998 - also known as taxpayer bill of rights

Treasury has various documents from 1992 and 2005

## Industry specific

_Insurance_ 
see Federal Income Taxation of Insurance Companies (2nd Edition)

## International

Offshore Accounts: No Place to Hide? - mentions deducting investment fees, Medicare 3.8% investment tax, etc but I've never heard of them

See http://en.wikipedia.org/wiki/International_taxation

http://en.wikipedia.org/wiki/Tax_haven is the main page; estimates of around 10 to 30% of all capital is in taxhavens; read Shopping for Anonymous Shell Companies: An Audit Study of Anonymity and Crime in the International Financial System

TaxJustice Network is active against this; runs http://www.tackletaxhavens.com

http://en.wikipedia.org/wiki/Foreign_Account_Tax_Compliance_Act significantly reforms the system

In 2013 NYTimes did an article http://www.nytimes.com/2013/02/12/business/crossing-the-line-between- tax-avoidance-and-evasion.html?pagewanted=all&_r=0

### Income

Expatriates in China are taxed on their income in China for US taxes; however there is an exclusion per http://www.taxmeless.com/IRS593Publication.htm TaxHighlights for U.S. Citizens and Residents Living Abroad which was $91,000 in 2009; also can get credits for foreign taxand for housing

U.S. is a worldwide income taxcountry for corporations too whereas domestic-only taxation countries are territorial such as Ireland

### Corporate

See http://en.wikipedia.org/wiki/International_Business_Companies_Act which discusses the 1999 initiative and repeal of a taxhaven law

Transfer pricing a big controversy and thin capitalization is a controversy too

CPCU530 book 15.33 says foreign earnings go into nine categories ("baskets") with IRC Section 163(j) restricting earnings-stripping; repatriation and SubPart F codifies anti-deferral (which taxes non-repatriated income); also there is an export-related taxbenefit for foreign sales corporations (FSC) designed to neutralize EU border taxadjustments arising from VAT but it was replaced by the Extraterritorial Income (ETI) arrangement in 2000 after being struck down by the WTO; WTO also struck down ETI