Real estate - investment
===

TODO: move this https://millionairemob.com/personal-financial-ratios/ | 19 Personal Financial Ratios You Need to Know | Millionaire Mob

## REITs versus direct
Generally REITs are better than these "direct" alternative arrangements per [REITs Vs. Real Estate Crowdfunding](https://seekingalpha.com/article/4051897-reits-vs-real-estate-crowdfunding) 

### REITS
* [What I Wish I Knew Before Investing In REITs | Seeking Alpha](https://seekingalpha.com/article/4242000-wish-knew-investing-reits)


### crowdfunding direct proxy
first looked into this from [DiversyFund scam](https://www.therealestatecrowdfundingreview.com/diversyfund-review-and-ranking)  on Facebook

https://506investorgroup.com/benefit-of-the-5006-investor-group-avoiding-realty-shares-investments/ | Benefit of the 506 Investor Group – Avoiding Realty Shares Investments – 506 Investor Group
https://millionairemob.com/real-estate-crowdfunding-for-non-accredited-investors/ | Best Real Estate Crowdfunding for Non-Accredited Investors
https://www.peerstreet.com/about?ps_src=header | About Us | PeerStreet

https://wallethacks.com/best-crowdfunding-real-estate-investing-sites/ | 2019 Ultimate Guide to Crowdfunding Real Estate Investing Sites
https://papers.ssrn.com/sol3/papers.cfm?abstract_id=3317678 | Equity Crowdfunding and Governance: Toward an Integrative Model and Research Agenda by Douglas J. Cumming, Tom R. Vanacker, Shaker A. Zahra :: SSRN
https://www.propertyshark.com/mason/ | PropertyShark - Real Estate Search and Property Information

## management
https://latchel.com/ | Latchel

## advice
https://www.biggerpockets.com/ | BiggerPockets: The Real Estate Investing Social Network
http://profitablepropertymanagement.com/podcast/max-nussenbaum-castle/ | The Rise and Fall of Castle Property 

Management with Max Nussenbaum - Profitable Property Management PodcastProfitable Property Management Podcast

