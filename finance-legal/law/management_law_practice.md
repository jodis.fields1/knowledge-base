# Management law and practice

## Employee rights

Weingarten rights [wiki](https://en.wikipedia.org/wiki/Weingarten_Rights) - right to have third-party present 

[EE wants to tape record everything](http://www.hrlaws.com/forum/showthread.php?t=50291)\- EE = employee, ER = employer; interesting discussion 

[BEACH v. HANDFORTH KOME](http://caselaw.findlaw.com/ak-supreme-court/1651579.html) No. S?14811 - case which displays that Alaska recognizes an implied covenant of good faith and fair dealing in employment relations 

## Operations

Avoid the  [telephone game](http://www.intelligentutility.com/magazine/article/telephone-game) - require direct communication rather than intermediaries

## Management strategy
Skimmed  [wiki Strategic management](https://en.wikipedia.org/wiki/Competitive_strategy) after noticing the history article on wiki  

Read Michael Porter's classic Competitive Strategy in 2010  
SWOT analysis,  "what-if" analysis; "but for" (causation analysis)  
This Is The Internal Grading System Google Uses For Its Employees - OKR modified version of OGSM
