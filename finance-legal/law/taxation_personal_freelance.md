# Taxation -> personal -> freelancing
https://www.investopedia.com/articles/trading/09/incorporate-active-trading.asp

https://gusto.com/blog/payroll/payroll-taxes-out-of-state-employees

https://www.nolo.com/legal-encyclopedia/how-sole-proprietors-are-taxed-30292.html

LLC: 
* https://www.bizfilings.com/toolkit/research-topics/managing-your-taxes/federal-taxes/llc-electing-s-corp-statusthe-best-of-both-worlds
* https://www.hg.org/legal-articles/independent-contractors-should-they-form-an-llc-or-s-corp-50404
* https://www.irs.gov/businesses/small-businesses-self-employed/single-member-limited-liability-companies

S-corp: 
* https://www.upcounsel.com/s-corp-how-to-pay-yourself

## bookkeeping
https://www.fundera.com/blog/self-employed-accounting-software | The 5 Best Self-Employed Accounting Softwares in 2020
https://www.google.com/search?hl=en&q=self%2Demployed%20deductions | self-employed deductions - Google Search
https://central.xero.com/s/article/Convert-to-Xero-from-QuickBooks#CheckwecanconvertyourQuickBooksfile | Convert to Xero from QuickBooks
https://www.fundera.com/business-accounting/quickbooks-self-employed-review | 2020 QuickBooks Self-Employed Review: Pros and Cons | Fundera
