Land law
===

## Beaches
Most states have public beaches to the mean high tide line. See Beachapedia  [Beach Access](http://www.beachapedia.org/Beach_Access "http://www.beachapedia.org/Beach_Access") and Alaska-specific  [State of the Beach/State Reports/AK/Beach Access](http://www.beachapedia.org/State_of_the_Beach/State_Reports/AK/Beach_Access "http://www.beachapedia.org/State_of_the_Beach/State_Reports/AK/Beach_Access")
  
## Alaska
Most are state-owned; see  [Land Ownership in Alaska](http://dnr.alaska.gov/mlw/factsht/land_own.pdf) from DNR for details

Some are municipal but still technically state-owned. See  [Alaska Municipal Land Management Handbook](http://commerce.alaska.gov/dnn/dcra/PlanningLandManagement/AlaskaMunicipalLandManagementHandbook.aspx)

[ALASKA LAND OWNERSHIP](http://nrm.salrm.uaf.edu/~stodd/AlaskaPlanningDirectory/landOwnership.html "http://nrm.salrm.uaf.edu/~stodd/AlaskaPlanningDirectory/landOwnership.html") tabular data from UAF Planning Alaska's: Public Lands: The Alaska Planning Directory

## Juneau
Trying to understand how land is divvied up and how the state and local municipalities share public  

http://www.commerce.state.ak.us/dca/planning/AKLM/pub/AK_Land_Management_Handbook.pdf is the best start - don't have time to review  

lots of other publications at http://www.commerce.state.ak.us/dca/StaffDir/GetPubl.cfm from Division of Community and Regional Affair (DCRA)

http://www.juneau.org/cddftp/maps/zonemap.php - zoning maps, but didn't see an index