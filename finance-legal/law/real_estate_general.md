Real estate
===

## Investing through actual property
See  http://www.creonline.com/getting-started.html - it says you can do it; who knows if it is true

## Regional trends
See How the Land Lies for U.S. Home Builders (2014) on constraints

## Statistics and housing stock
[AEI Special briefing on new construction, housing supply, and home prices](http://www.aei.org/publication/special-briefing-on-new-construction-housing-supply-and-home-prices/)

See http://www.census.gov/hhes/www/housing/census/historic/units.html for a breakdown of units

https://www.abodo.com/blog/2018-annual-rent-report/