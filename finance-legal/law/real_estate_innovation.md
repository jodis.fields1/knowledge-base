Real estate - innovation
===

## innovations dump

## startups
Katerra - employee reviews pretty bad

Plant Prefab -   [Amazon makes its first investment into a home builder](https://www.cnbc.com/2018/09/25/amazon-makes-its-first-investment-into-a-homebuilder.html)


indieDwell - https://indiedwell.com/multifamily 

[Colorado developers showing increasing interest in factory-built housing amid affordability crisis](https://www.denverpost.com/2018/10/09/colorado-modular-housing/)

[A Denver construction company’s tech saved it lots of time and money. Then it shared with competitors — for free – The Colorado Sun](https://coloradosun.com/2018/12/10/pcl-denver-tech-construction-colorado/)

[This housing startup designed homes that grow with their owners](https://www.curbed.com/2017/12/14/16778482/module-housing-affordable-urban-infill-startup)

## innovation discussion
[Investor momentum builds for construction tech](https://techcrunch.com/2019/02/16/investor-momentum-builds-for-construction-tech/)

[Why Autodesk Just Spent $1.15 Billion On Two Construction Tech Startups](https://www.forbes.com/sites/alexkonrad/2018/12/20/why-autodesk-just-spent-115-billion-on-two-construction-tech-startups/#480e69b332ab)

[6 Reasons Housing Construction Is Ripe For Disruption In The Fall Of 2018](https://www.forbes.com/sites/johnmcmanus/2018/09/25/6-reasons-housing-construction-is-ripe-for-disruption-in-the-fall-of-2018/#1ebe6abb7b27)

## air monitoring
[Can Smart Homes Help Prevent Mold? — ConnectM](https://connectm.com/blog/2018/2/15/can-smart-homes-help-prevent-mold)

## traditional
https://seekingalpha.com/article/4152214-lgi-homes-growth-star-late | LGI Homes - A Growth Star You Won't Be Too Late To Own - LGI Homes, Inc. (NASDAQ:LGIH) | Seeking Alpha

## amazon diy
https://people.com/home/diy-backyard-guest-house-amazon/amp/ | Amazon Is Selling a DIY Backyard Guest House | PEOPLE.com

## shortage
[Unpacking the "housing shortage" puzzle: How does housing enter and exit supply?](https://www.brookings.edu/research/unpacking-the-housing-shortage-puzzle/)


