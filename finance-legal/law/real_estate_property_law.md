Real estate property law
===

See https://en.wikipedia.org/wiki/Recording_(real_estate)

## Modifications and workout
Per A House With a Modified Loan Is a Symbol of Servicers’ Tug of War With Investors (2014), servicers should not modify below what they could get for a property thru foreclosure

## Mortgage loans
Interest is "simple interest" (don't pay interest on interest, but of course how could you) and compounds monthly per [the mortgageprofessor article on the nomenclature](http://www.mtgprofessor.com/a%20-%20simple%20interest%20mortgages/simple_interest_nomenclature.htm "http://www.mtgprofessor.com/a%20-%20simple%20interest%20mortgages/simple_interest_nomenclature.htm") and is typically paid monthly (monthly amortization). I use Trulia for mortgagecalculators but  [TimeValue.com for balloon payment calculator](http://www.timevalue.com/products/tcalc-financial-calculators/balloon-mortgage-payment-calculator.aspx "http://www.timevalue.com/products/tcalc-financial-calculators/balloon-mortgage-payment-calculator.aspx")

dotloop.com is a startup that lets you share the boilerplate documents (The Hack Heard Round the Real Estate World in Bloomberg 2013) but turns out to be not that helpful

## Law
The title is usually transferred by the deed, and actually the deed is basically the title; the deed needs to be recorded

When you pay off your loan, the lender will release the mortgage through a filing with the recorder per http://answers.yahoo.com/question/index?qid=20080518065923AAaFZ8O although there's also some talk about getting a new deed or a quitclaim deed, http://www.inman.com/buyers-sellers/columnists/bennyinmancom/must-dos-when-paying- mortgagesays there should be a "a release of the deed of trust" filed

### Deeds and title
Types of deeds include: warranty deed (general or special depending upon the state), bargain-and-sale deed, and quitclaim deed.

Bargain-and-sale deed and quitclaim deed seemed identical to me; http://en.wikipedia.org/wiki/Deed says the quitclaim deed is actually estoppel; http://www.nytimes.com/2003/06/01/realestate/q-a-quit-claim-deed-and-title-insurance.html says it can be used

http://legal-dictionary.thefreedictionary.com/Quitclaim+Deed discusses state differences

Title insurance is mainly a United States thing; see http://www.eppersonlaw.com/TES/NTESRC/NTESRC_Home.htm for countrywide overview by Oklohoma and realpropertytitlestandards.org; EntitleDirect.com is changing the game with direct-to-consumer title, http://mytitleins.com/ is a comparison shopping tool

perpetual interest in property is problem, "Uniform Marketable Title Act" tried to set a 30-year limit but like "Simplification of Land Transfers" was never adopted; Perpetual Property http://papers.ssrn.com/sol3/papers.cfm?abstract_id=1260343 is an overview which includes intellectual history

Note that not all states have all 4 varieties: Washington just seems to have 3

http://www.rockymountainlegaldirt.com/2011/07/when-it-comes-to-deeds-it-pays-to-pay-attention.html says "quitclaim deed, however, will not transfer the seller’s after acquired title in the property"

https://www.rocketlawyer.com will let you create a deed

Trust deeds probably come into play with securitized mortgagesecurities

Adverse possession allows someone to keep land if they stay there for enough time (maybe 20 years)

24 CFR 203.389 Waived title objections has a list of title issues that the FHA doesn't think are significant which I learned from skimming HUD 4155-2, Lender’s Guide to the Single Family Mortgage Insurance Process

## Price disclosure
A few states are nondisclosure states as notably documented in State and provincial ratio study practices: 2003 survey results at http://www.iaao.org/uploads/2003_Ratio_Practices_Survey.pdf with the largest being Texas but notably also including Alaska altho these states do disclose mortgages per http://www.realtor.com/home-values/faq.aspx; some of these states send out surveys (AK) or buy the MLS data when a sales contract includes a waiver (common); greatly affects accuracy of Zillow and probably Trulia per http://www.zillowblog.com/2006-05-11/chronicles-of-data-collection-ii-non-disclosure-states/ and per http://www.zillow.com/howto/DataCoverageZestimateAccuracy.htm; also discussed at length by Bankrate article http://realestate.msn.com/article.aspx?cp-documentid=13107736;

What Price Nondisclosure? The Effects of Nondisclosure of Real Estate Sales Prices (2004) by Berrens & McKee says this has negative effects on markets

## Liability disclosure
Separate issue discussed at length at http://www.zillowblog.com/2011-11-15/5-things-you-should-know-about-real-estate-disclosures/ - basically, disclose issues with your property

## Encumbrances, covenants and easements
Incorporeal interests include easements, profits a pendre, seller's restrictions (no cutting down the trees for example; may be limited by statute), and leases

In 2010-3 private transfer fees paid to developers became an issue see http://en.wikipedia.org/wiki/Private_transfer_fee; FHFA clamped down on that by not allowing Fannie to buy these mortgages in 2010 finalized in 2012

Conservation easements are somewhat controversial; see