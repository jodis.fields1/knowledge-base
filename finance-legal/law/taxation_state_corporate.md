note: saw some references to multistate corporate tax avoidance scheme...?

TODO: fix up https://en.wikipedia.org/wiki/Corporate_tax_in_the_United_States

## apportionment
question: how much state corporate income tax does, say, Google rally pay?

### quite helpful
https://www.ctj.org/the-sorry-state-of-corporate-taxes/ | The Sorry State of Corporate Taxes | Citizens for Tax Justice - Working for a fair and sustainable tax system

https://www.accountingtoday.com/opinion/technology-and-the-emerging-apportionment-riddle | Technology and the emerging apportionment riddle
http://news.cchgroup.com/2017/10/05/corporate-state-income-tax-changes/ | Market-Based Sourcing and Beyond: Lookout for New State Tax Issues in the Corporate Tax World. - Tax & Accounting Blog
https://www.calcpa.org/public-resources/ask-a-cpa/small-business-tax/corporate-tax-returns/sales-tax/when-are-services-subject-to-california-sales-tax | When are services subject to California sales tax
https://blog.taxjar.com/saas-sales-tax/ | Sales Tax by State: Is SaaS Taxable?
http://www.chapmanlawreview.com/archives/2343 | THINKING OUT CLOUD: CALIFORNIA STATE SALES AND USE TAXABILITY OF CLOUD COMPUTING TRANSACTIONS – Chapman Law Review


### news
https://www.latimes.com/business/la-fi-tn-digital-tax-20181029-story.html | U.K. targets tech giants with a digital services tax that starts in 2020 - Los Angeles Times
https://www.seattletimes.com/business/three-california-cities-ok-new-taxes-on-tech-firms-will-others-follow/ | Three California cities OK new taxes on tech firms — will others follow? | The Seattle Times

### somewhat
https://www.bloomberg.com/news/articles/2013-05-22/google-joins-apple-avoiding-taxes-with-stateless-income | Google Joins Apple Avoiding Taxes With Stateless Income - Bloomberg
https://www.outsiderclub.com/apple-aapl-google-goog-taxes-dodging-loopholes/927 | Did You Pay Your Taxes? Apple and Google Didn't
https://www.northwestregisteredagent.com/avoid-ca-franchise-tax.html | How to avoid California Franchise tax

### unrelated (mostly)
https://www.nytimes.com/2012/04/29/business/apples-tax-strategy-aims-at-low-tax-states-and-nations.html?mtrref=www.google.com&gwh=483136732416E7FF6F2D76CEECFB09EF&gwt=pay | Apple’s Tax Strategy Aims at Low-Tax States and Nations - The New York Times
https://taxfoundation.org/californias-corporate-income-tax-rate-could-rival-the-federal-rate/ | California’s Corporate Income Tax Rate Could Rival the Federal Rate | Tax Foundation
https://www.sfgate.com/bayarea/article/California-democrats-want-businesses-to-give-half-12508742.php | California Democrats want businesses to give half their tax-cut savings to state - SFGate

### publishers
https://www.stateandlocaltax.com/publications/ | Planning, Compliance & Policy Matters | Salt Shaker

### background
https://blog.taxjar.com/international-sellers-deal-sales-tax-u-s/ | Do International Sellers Have to Deal with Sales Tax in the US?
https://www.nolo.com/legal-encyclopedia/california-state-business-income-tax.html | California State Business Income Tax | Nolo.com
https://www.ftb.ca.gov/businesses/Structures/C-Corporation.shtml | C Corporation | California Franchise Tax Board
https://en.wikipedia.org/wiki/California_Franchise_Tax_Board | California Franchise Tax Board - Wikipedia