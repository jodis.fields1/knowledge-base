# Algebra tricks
See Great Courses - Secrets of Mental Math

In my limited experience with math, I've noticed that algebra tricks (pulling variables out, multiplying both sides by a variable over a variable, adding to both sides) are the key in many cases. Doing research on this:

http://www.glad2teach.co.uk/26_Math_tricks_to_learn_algebra_fast.htm \- tried emailing the guy, but it was a dead email - did not spend much time on this

Personal tricks

Try multiplying  by strange versions of 1 to change the form of an expression. For example:

(x+dx)^-2 = x^-2(1+dx/x)^-2

Now, what would I need to do to the first expression to make the x be 1?  Well, you multiply by x/x, but you can hold the numerator out while you distribute the denominator. This is (x(x/x + dx/x))^-2. Then, you distribute the -2 over its two terms to equal the expression shown above.

When multiplying multi-digit numbers, add the digits together and subtract 1 for the number of total digits.

Always remember that you can pull a multiple out and leave a 1\. For example, y + ky = kx - k^2x can become y(1-k)=kx(1-k), which can be reduced to y=kx - from Calculus Made Easy page 26.

Also from Calculus Made Easy page 26, note that (a^2 - b^2)^0.5 is difficult to work with - can't distribute the 0.5. You can instead insert k^2 = (a + b)/(a -b). I'm not sure if this is called rationalizing the square root or something.

_Exponentials and logarithms_
See  [How To Think With Exponents And Logarithms](http://betterexplained.com/articles/think-with-exponents/ "http://betterexplained.com/articles/think-with-exponents/") for good intro

Got a little hung up on what to do about a constant when you're trying to cancel a logarithm e.g. e^[5*ln(x)] - based on http://en.wikipedia.org/wiki/List_of_logarithmic_identities#Using_simpler_operations in particular "The law for powers exploits another of the laws of indices" I think this collapses down to x^5 - which is sweet. http://answers.yahoo.com/question/index?qid=20080709231016AAnn3Jt had a good explanation for solving for an exponential fit.

In Oct 2013 reviewed this again: http://www.purplemath.com/modules/logrules3.htm and also looked at  [ Numbers and their Application - Lesson 17](http://www.andrews.edu/~calkins/math/webtexts/numb17.htm "http://www.andrews.edu/~calkins/math/webtexts/numb17.htm")