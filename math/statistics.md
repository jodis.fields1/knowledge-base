

Jared Knowles has some good information http://jaredknowles.com/professional-work/

Probability - see algorithms note too
N choose k is the number of potential combinations and formula is n!/(k!(n-k)!) - note that 0! is 1 which is explained by mathforum at  [Why does 0 factorial equal 1](http://mathforum.org/library/drmath/view/57128.html)

**Tips/epiphanies**
Standard normal distribution is summarized as Z, and is really a transformation of any normal distribution but not actually seen empirically often (or ever)
Chi-squared distribution is the sum of squared random variables (of, if 1 degree of freedom, 1 variable). Often see it being "standardized" and this transformation is OK as it applies to all the variables (subtract by mean, divide by standard deviation). Note that per Cochran's theorem variance follows this distribution
When you look at a PDF, y-axis (height) may be greater than 1 as this is defined over an interval, see  [A Probability distribution value exceeding 1 is OK](http://stats.stackexchange.com/questions/4220/a-probability-distribution-value-exceeding-1-is-ok)?

Maximum likelihood ( [wiki](https://en.wikipedia.org/wiki/Maximum_likelihood)) - read the wiki article carefully, including the "draw 3 biased coins" example and I get that this can test which distribution is most appropriate

Science application
[Should we change how we look at P values?](http://membercentral.aaas.org/blogs/driving-force/should-we-change-how-we-look-p-values) - suggests lowering p-values significantly; periodic discussion started by Ionaddis PLOS Medicine: Why Most Published Research Findings Are False

Textbooks
[The Mathematical Symbols used in Statistics](http://ocw.smithw.org/csunstatreview/statisticalsymbols.pdf) - good summary, did not know pi was proportion applicable to population

[Online Statistics Education](http://onlinestatbook.com/): An Interactive Multimedia Course of Study - seems pretty good

[WWS509 Lecture Notes](http://data.princeton.edu/wws509/notes/) by German Rodriguez - these are pretty decent actually; read up to about 59/64 of Ch. 2; however it is ultimately a bit over my head


Collaborative Statistics - open and free online textbook http://cnx.org/content/col10522/1.25 per "open education"

Gotchas
You’re probably polluting your statistics more than you think (2013) - interesting article

General overview

Plotting
Stemplots, histograms - show distributions; stemplots are just quicker by hand than histograms

*Regression analysis*
Confused by multiple regression holding everything else constant;  [How exactly does one “control for other variables”?](http://stats.stackexchange.com/questions/17336/how-exactly-does-one-control-for-other-variables) at SE with what looks like Ch.3 of Introductory Econometrics: A Modern Approach by Jeffrey M. Wooldridge; apparently depends heavily on the Gauss-Markov theorem; if the fifth assumption (homoskedasticity) is included, OLS becomes "BLUE" (best in terms of the smallest variance). Read the appendix and the proof but only vaguely understood it.
"If independent variables A and B are both correlated with Y, and A and B are highly correlated with each other, only one may contribute significantly to the model, but it would be incorrect to blindly conclude that the variable that was dropped from the model has no biological importance" - quote from [Multiple Regression](http://udel.edu/~mcdonald/statmultreg.html) in Handbook of Biological Statistics which discusses reading and vertical leap, seems that the order of the variables may matter

Long series at  [PalaeoMath Home Page](http://www.palass.org/modules.php?name=palaeo_math) by Norman MacLeod which I started:
first one on Reduced Major Axis regression was a bit eye-opening; second one on [major axis regression analysis](http://www.palass.org/modules.php?name=palaeo_math&page=6) (or principal axis regression) was also eye-opening
Regression 4: Going Multivariate (Multiple Least-Squares Regression) refers to multivariate analysis as "a mathematical bestiary of different approaches, many of which have little in common with others" 

*Generalized linear models (GLiM)*
Was confused about why these are called linear when they seem nonlinear, but  [Problem understanding the logistic regression link function](https://stats.stackexchange.com/questions/80611/problem-understanding-the-logistic-regression-link-function) clarified that the linear is in the Beta parameters and not the Xs which I guess makes sense
best explanation is probably [this](http://stats.stackexchange.com/a/30909) by gung which I found from [Purpose of the link function](http://stats.stackexchange.com/questions/48594/purpose-of-the-link-function-in-generalized-linear-model) in generalized linear model
Follow the [link-function] tag on stats.stackexchange for more info

*Chi-squared variable*
Used as a test with numerous variations and Pearson's chi-squared is more popular than most
Reviewed [Tutorial: Pearsons Chi-square Test for Independence](http://www.ling.upenn.edu/~clight/chisquared.htm) (2008) and also [this link](http://math.hws.edu/javamath/ryan/ChiSquare.html) but still a bit fuzzy; basically (Observed - Expected)^2/Expected; a bit confused as to why it is divided by expected and not variance which is addressed at SE in [Why does Pearsons chi-squared test divide by the mean and not the variance?](https://math.stackexchange.com/questions/252370/why-does-pearsons-chi-squared-test-divide-by-the-mean-and-not-the-variance)
Used to test whether odds ratio = 1
Reviewed  The Chi Square Statistic which has somewhat confusing formulas for the contingency table, but the product of the totals for the row and column divided by the total makes some sense
[Use of the Chi-Square Statistic](http://math.hws.edu/javamath/ryan/ChiSquare.html) at Johns Hopkins is more clear since it calculates the proportion expected if probability of disease is the same in both groups, and therefore calculates proportion with exposure before multiplying by the number with the disease

*ANOVA*
Under the impression that this is basically the F-test, but this article [Take Away the Mystery of Regression vs. ANOVA](http://www.allanalytics.com/author.asp?section_id=1413&doc_id=252823) (2012) confused me as it talks about something else

Distributions

Normal distribution( wiki)
[What do π and e stand for in the normal distribution formula?](http://math.stackexchange.com/questions/28558/what-do-pi-and-e-stand-for-in-the-normal-distribution-formula) from SE and [Why does π appear in the density of a normal distribution?](http://www.quora.com/Why-does-%CF%80-appear-in-the-density-of-a-normal-distribution) from quora. Note: the negative exponent means that as the x value (domain) goes far below and above zero, the range becomes smaller and smaller which is expected

Binomial
Binomial distribution - kinda fundamental and is derived from a Bernoulli (binary) variable

Normal quantile plot http://en.wikipedia.org/wiki/Normal_probability_plot - this is described in Moore's Introduction as perhaps the best way to find out if a sample matches the normal distribution; however I don't see how it is as it doesn't seem to show the frequency at which values occurr; also seems to be confusion as to whether the y-axis is quantiles or just response values
Poisson- Noticed that people seemed to see that the distribution on http://blog.stephenwolfram.com/2012/03/the-personal-analytics-of-my-life/ was "obviously" Poisson - wish I could figure out how they knew. Modelling Time Series Count Data: An Autoregressive
Conditional Poisson Model was referred to as a good model

Relationships (scatterplots and regression)
Explanatory variables (x) affects response variables (y); however, sometimes there is a "confounding variable" per http://en.wikipedia.org/wiki/Confounding
Correlation is called r; equal to the covariance divided by the product of the standard deviations; covariance = sum of (x - xmean) * (y - ymean)
R^2 is correlation squared; can also be defined as variance of predicted values over the variance of observed values
Beware corelations based on averaged data (page 165 of Moore's book)
When there's a restricted range, correlation is typically lower (page 166)
Study or experiment design
Moore's book points out that lack of realism is the most serious issue (page 237)

Sampling
Population size doesn't matter ("as long as the population is at least 100 times the size of the sample")! Moore on page 267 draws the comparison to scooping out corn from a bag or a truckload. So sampling a 1 million person city and sampling the entire 300 million person country requires a similar sample size, in theory.

Capture->recapture on page 268 discusses repeated sampling; based on amount found of those released, you can estimate total amount (e.g. fish in ocean)

Central limit theorem
http://en.wikipedia.org/wiki/Illustration_of_the_central_limit_theorem#Illustration_of_the_discrete_case; the actual explanation gets into Taylor's theorem which I can't really deal with right now, as well as the key words http://en.wikipedia.org/wiki/Sample_mean_and_sample_covariance and http://en.wikipedia.org/wiki/Summation