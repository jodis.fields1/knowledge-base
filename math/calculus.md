MIT OCW - reviewed a few sessions (44, 46) of of Unit 3 of their   [Single-Variable Calculus](http://ocw.mit.edu/courses/mathematics/18-01sc-single-variable-calculus-fall-2010/index.htm) which completely online & free whereas in Multivariate requires purchase of a cheap textbook ($3 for used online)

calc101.com - for example see http://calc101.com/webMathematica/partial-fractions.jsp

Advanced calculus by Woods - recommended highly by Feynman
* years later, deeper dive in [Richard Feynman’s Integral Trick](https://medium.com/a-younger-voice/richard-feynmans-integral-trick-e7afae85e25c)

Calculus by Spivak - classic textbook 

[Elementary Calculus: An Infinitesimal Approach](http://www.math.wisc.edu/~keisler/calc.html) by Keisler - online textbook

Five Free Calculus Textbooks -   [http://books.slashdot.org/story/04/03/04/028253/five-free-calculus-textbooks](http://books.slashdot.org/story/04/03/04/028253/five-free-calculus-textbooks)

Calculus of Variations - 2000 (originally Russian) book; real analysis is a prerequisite

An Introduction To Mechanics - 1973 'gold standard' in physics
The Feynman Lectures on Physics including Feynman's Tips on Physics: The Definitive and Extended Edition - likely helpful 
What Is Calculus About? - 1962 intro book by Sawyer which gets good reviews The Hitchhiker's Guide to Calculus - 1995 intro book which covers the basics

[Understanding the Fourier transform](http://altdevblogaday.org/2011/05/17/understanding-the-fourier-transform/) - 

**Books**

Math for the Million

The Mathematical Tourist

Mathematics for the Million

The Heart of Mathematics: An Invitation to Effective Thinking

book: Algebra, Harold Jacobson

book: Courant, Differential and Integral calculus, vols I and II

vol II Courant, or ideally Spivak, Calculus on manifolds, or Fleming: Calculus of several variables, or Crowell, Williamson & Trotter

ordinary differential equations {Boyce and de Prima?]

abstract algebra (group theory) [try Artin, Algebra]

differential manifolds ( calculus on curved spaces) [begin with Spivak last chapters of Calculus on manifolds]

**Notes**

2014-10: reviewed Finan's Exam P preparation, bit confused by continuity correction with  [good explanation here](http://www.statisticshowto.com/what-is-the-continuity-correction-factor/)

2014-09: started this up again somewhat in earnest late this month with Riemann sums.

Riemann sums:

> Got hung up on inserting the interval in as the delta x.

> Summation proofs: see  [Category:Sums of Sequences at proofwiki](https://proofwiki.org/wiki/Category:Sums_of_Sequences).

> Played around with various online calculators but settled on Wolfram Alpha's "search engine" calculator w/ Android app; for example "sum x^3, x=0 to 50" could do a sum

> somiaj at #math on IRC tried to help me understand  [this stackexchange question](http://math.stackexchange.com/questions/742532/for-riemann-sums-involving-square-roots-why-do-we-let-c-i-fraci2-n) but after studying it I think I understand... the interval is like [0,3] and the i+1 is obvious to keep it incrementing
