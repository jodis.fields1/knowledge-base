https://github.com/hypotext/notation/blob/master/README.md

# tools
desmos.com

# random
2020-03-11: [The Map of Mathematics](https://www.quantamagazine.org/the-map-of-mathematics-20200213/)

2018-06-26: from Oz at Bradfield's Topological sort, read up on wiki articles "Graph theory", "Topology", and "Homeomorphism"
* "that is, they are the mappings that preserve all the topological properties of a given space"
* "An often-repeated mathematical joke is that topologists can't tell the difference between a coffee cup and a donut,[3] since a sufficiently pliable donut could be reshaped to the form of a coffee cup by creating a dimple and progressively enlarging it, while preserving the donut hole in a cup's handle."

2018-06: skimmed How To Solve It by Polya, sounds like "Mathematical Problem Solving" by Alan Schoenfeld might be better per https://www.amazon.com/gp/customer-reviews/R2PB6KB1BJPBDV/ref=cm_cr_othr_d_rvw_ttl?ie=UTF8&ASIN=069111966X

2018-03 notes:
* stumbled across https://en.wikipedia.org/wiki/Wikipedia:WikiProject_Mathematics/Popular_pages
    * read Stephen Hawking, mostly everything looks familiar
        * start from bottom to avoid popular press

# Math general
[A map of the Tricki](http://www.tricki.org/tricki/map "http://www.tricki.org/tricki/map") - found it from the Quora question about  [understanding advanced mathematics](http://www.quora.com/Mathematics/What-is-it-like-to-understand-advanced-mathematics)

The Handbook of Essential Mathematics by Sparks, Gregory & Miller - good overview; p200 has greek symbols

# Education
_General_

<http://betterexplained.com/> - always the best start

NetMath - <http://netmath.uiuc.edu/>

<http://www.artofproblemsolving.com/> \- cool wiki

[Where to Find Free Math Courses Online -- Education-Portal.com](http://education-portal.com/articles/Where_to_Find_Free_Math_Courses_Online.html) \- ?

[Math for the Layman](http://www.cs.trinity.edu/About/The_Courses/cs301/math-for-the-layman/) \- by deceased computer scientist Kenneth Iverson; skimmed through but uses open-source language J pretty heavily; my reading found mixed reviews for J

[SOCR: Statistics Online Computational Resource](http://www.socr.ucla.edu/htmls/SOCR_Distributions.html "http://www.socr.ucla.edu/htmls/SOCR_Distributions.html") \- great probabilityresources; uses Java applets

<http://www.mathpages.com/> \- miscellaneous topics

<http://ocw.mit.edu/courses/mathematics/18-098-street-fighting-mathematics-january-iap-2008/index.htm> \- Street-fighting Mathematics jumps pretty far into the deep end to begin with

Non-traditional mathematics curriculum results in higher standardized test scores, MU study finds (2013) - integrated is best

[225 study meta-analysis: Active learning (as opposed to lecture-based learning) increases student performance in science, engineering, and mathematics](http://www.reddit.com/r/science/comments/2a3wvo/225_study_metaanalysis_active_learning_as_opposed/) - duh!

More advanced than my needs: A Computational Introduction to Number Theory and Algebra by Victor Shoup has free PDF download at <http://www.shoup.net/ntb/>; also see Basic Concepts of Mathematics at <http://www.trillia.com/zakon1.html;>

## Tutoring (chatroom)

[http://gogloom.com/DIR?cat=59577&catdesc=Math](http://gogloom.com/DIR?cat=59577&catdesc=Math)

## Quantitative finance
See algotrading.md

http://www.youtube.com/user/NathanWhitehead#p/u

<http://www.nuclearphynance.com/default.aspx> \- has a good book and guide

## Calculators
Desmos - cool for demonstrating and saving calculations

Wolfram Alpha obviously <http://www.wolframalpha.com/examples/Math.html> and also an integrals calculator <http://integrals.wolfram.com/index.jsp>;

For linear algebra <http://www.gregthatcher.com/Mathematics/GaussJordan.aspx>;

calculatorsoup.com - lots of good calculators, I used the  [permutations one](http://www.calculatorsoup.com/calculators/discretemathematics/permutations.php)

_Smartphone calculators_  - See  [appcrawlr list](http://appcrawlr.com/app/search?_src=sort_&max=12&id=1008717&similarTo=1008717&device=android&_rv=1&sort=&trigger=deviceChange "http://appcrawlr.com/app/search?_src=sort_&max=12&id=1008717&similarTo=1008717&device=android&_rv=1&sort=&trigger=deviceChange"). Settled on Wolfram Alpha Calculus Course Assistant which is nice but seems to be missing a little so I'm thinking about Math Studio by Pomegranate Apps which is $20. Or maybe MathPac which is $10. TI-89 is a bit obselete when you can download similar functionality to your phone; you can even  [emulate the TI-89](http://gs3.wonderhowto.com/how-to/turn-your-samsung-galaxy-s3-into-powerful-ti-89-titanium-graphing-calculator-0148661/ "http://gs3.wonderhowto.com/how-to/turn-your-samsung-galaxy-s3-into-powerful-ti-89-titanium-graphing-calculator-0148661/") if you can get the ROM. Tried Graphing Calculator for Matlab and Integrator (nobody). Also logged a bug for the Sage Math app and looked into the difficult-looking GNU Octave app.

## Books on math  

_Survey_

Mathematics: Its Content, Methods and Meaning - probably the best survey

The Mathematical Experience - 1999 survey, probably after the above in rank  
The World of Mathematics: A Four-Volume Set - encyclopedia

Mathematics: From the Birth of Numbers - shorter but similar  

_Intro for outsiders_
Mathematics for Nonmatheticians - read this (see librarything), inspired math history note

Mathematics in 10 Lessons: The Ground Tour - bought; pretty boring and simple

The Art of Mathematics - 2006 book; the Gompers review probably nails this as garbled 

Innumeracy: Mathematical Illiteracy and Its Consequences - first in 1988; made innumeracy a household name; mixed reviews; later in 1997 wrote A Mathematican Reads the Newspaper and later in 2004 read A Mathematician Plays the Stock Market;

What Is Mathematics? An Elementary Approach to Ideas and Methods - 1941 book by Richard Courant; with 1996 revision 

_Learning calculus  - transferred to separate note_

_Stats and_ _probability_

How to Lie with Statistics - classic; gonna buy the ebook probably 

What is a p-value anyway? 34 Stories to Help You Actually Understand Statistics - by Vickers, who I recognize; ebook unavailable  

[Introduction to Probability](http://www.dartmouth.edu/~chance/teaching_aids/books_articles/probability_book/book.html) - free ebook published by the American Mathematical Society

ProbabilityTheory: The Logic of Science - big into Bayesian statistics; might be nice for Xianli; ebook for $64  
All of Statistics : A Concise Course in Statistical Inference - ebook $70 or so; lots of stats books on safari tho   

[http://www.stat.berkeley.edu/~aldous/](http://www.stat.berkeley.edu/%7Ealdous/) page by David Aldous who focuses on real-world applications

http://pages.stern.nyu.edu/~gsimon/ - NYU guy who has spent some time on insurance

_Philosophy_  
Gödel, Escher, Bach: An Eternal Golden Braid - likely overrated and too long at 700 pages

## Proofs

See math history for some of these

Also see ProofWiki - for example the  [variance is expectation of square minus square of expectation](https://proofwiki.org/wiki/Variance_as_Expectation_of_Square_minus_Square_of_Expectation) proof