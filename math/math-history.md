# Math history

  


Inspired by 2014-05 rereading of Mathematics for the Nonmathetician. Stopped at p135.

Also found  [ Branches and Roots of the Tree Mathematica](http://mathematics.gulfcoast.edu/mgf1107ll/Book.htm "http://mathematics.gulfcoast.edu/mgf1107ll/Book.htm") by Leo Lusk

  


**Numbers**

Hindus introduced positional notation; Greeks use "alpha" for 1, pi for "90", etc  [ see link](http://mathematics.gulfcoast.edu/mgf1107ll/Chap1Sec3Lesson2.htm "http://www.math.wichita.edu/history/topics/num-sys.html")

  


**Symbols**

Per p96 of the book, most symbols were accidental except for Leibniz

Prior to the 1600s or so, symbols were not used; however,  [ Diophantine equation](https://en.m.wikipedia.org/wiki/Diophantine_equation "https://en.m.wikipedia.org/wiki/Diophantine_equation") contradicts this

  


Francois Vieta - proved that "general" coefficients could be used (e.g., 5x + 3y = 0) which allowed for second-degree equations to be solved per p117; however per p119-120, Galois showed that equations above fourth degree cannot be solved algebraically

  


**Ancient figures** \- many of these were in Alexandria or perhaps Syracuse (in Crete?) or Ionia (modern Turkey)

Diophantus - major algebra figure also known for Diophantine equations

Hipparchus - founder of trigonometry but also discovered equinoxes (?)

Apollonious of Perga - mostly focused on conic sections and named ellipse, parabola, and hyperbola

Archimedes - everyone's heard the basics

Nichomachus - wrote an early music theory treatise

Ptolemy - geographer and astronomer mostly with a bit of astrology

  


_Early proofs_

Euclid's Elements (from earlier thinkers such as Thales) probably was the major first book on proofs, and used quite a few axioms

Schopenhauer's criticism of the proofs of the parallel postulate - Schopenhauer didn't like proofs; dunno if there's substance

Sum of triangle angles is 180 degrees - relies on a "theorem of parallel lines"

  


**Unproven**

Goldbach's hypothesis - every number is the sum of two primes
