See darkest repo

# new

## uploading IDs
[Launch HN: Berbix (YC S18) – Instant ID checks to fight fraud and stay compliant](https://news.ycombinator.com/item?id=21597169)

[Handling of PII regarding ID verification](https://community.monzo.com/t/handling-of-pii-regarding-id-verification/27567)

# old
simpleoptout.com - see https://twitter.com/troyd/status/1087901468907036672

[Google Has Quietly Dropped Ban on Personally Identifiable Web Tracking](https://www.propublica.org/article/google-has-quietly-dropped-ban-on-personally-identifiable-web-tracking)

Datacoup, Handshake and Meeco

## tracking, third-party cookies, etc
https://www.buzzfeednews.com/article/alexkantrowitz/heres-how-facebook-tracks-you-when-youre-not-on-facebook | Here's How Facebook Tracks You When You're Not On Facebook
https://www.wired.com/story/how-email-open-tracking-quietly-took-over-the-web/ | You Give Up a Lot of Privacy Just Opening Emails. Here's How to Stop It | WIRED
https://hunter.io/mailtracker | MailTracker - Free email tracking for Gmail • Hunter


## piracy risk
* [Just How Risky is Internet Piracy in 2017?](https://torrentfreak.com/just-how-risky-is-internet-piracy-in-2017-170715/) - not very
* https://www.yahoo.com/news/new-piracy-laws-heres-tell-youre-risk-10-year-jail-sentence-122757940.html - but possibly more in UK


## PRISM NSA program

Snowden leaked it - according to http://www.politico.com/story/2013/06/nsa-leak-keith-alexander-92971.html it prevented 50 attacks, 10 in the US

Prism and privacy: What could they know about me? (2013) - good overview from BBC, assume all your internet activity is tracked

## VPN etc
set up openVPN from https://web.archive.org/web/20171004024306/https://hackernoon.com/using-a-vpn-server-to-connect-to-your-aws-vpc-for-just-the-cost-of-an-ec2-nano-instance-3c81269c71c2

Private Internet Access (PIA) - https://news.ycombinator.com/item?id=14912044
bolehvpn

https://www.reddit.com/r/VPN/comments/53k7nv/my_university_doesnt_allow_us_to_use_a_vpn_how/

Tor - ISP can tell if you're using Tor, not so much VPN

## Tips

http://www.reddit.com/r/technology/comments/j1mit/how_to_remove_yourself_from_all_background_check/

## Anonymity rights

Looked at John Doe Is Alive and Well: Designing Pseudonym Use in American Courts which analyzed filing suit under pseudonyms

## General

The Cookies You  Can't Crumble (2014) - weird new ways of tracking

## Cellphone data

Per ACLU FOIA request, a datasheet is available at  [How long do wireless carriers keep your data?](http://www.nbcnews.com/tech/mobile/how-long-do-wireless-carriers-keep-your-data-f120367) (2011), basically not long; however see  [Are the police tracking your calls?](http://www.cnn.com/2012/05/22/opinion/crump-cellphone-privacy/) (2012) which sounds more ambiguous and  [Cops Want Wireless Providers to Record and Store Your Text Messages](http://mashable.com/2013/03/19/cops-want-text-messages-logs/) (2013) which notes that Anonmyous found differences in data retention from ACLU FOIA data. Coalition is Major Cities Chiefs Police Association, National District Attorneys' Association, the National Sheriffs' Association, and the Association of State Criminal Investigative Agencies

*Laws*

FACTA the 2003 amendment to FCRA applies to much more than just consumer credit - see https://www.privacyrights.org/fs/fs6a-facta.htm for an overview and note the mention of specialty reports with another report

Note time limits for "negative items" are probably per http://www.farmdoc.illinois.edu/finance/consumer_credit/CreditScoring.htm#q9 which says generally 10 years, 7 years for public records, and indefinitely for loans and insurance items $50k+; however https://www.privacyrights.org/fs/fs26-CLUE.htm says that information is only on CLUE for 5 years; 

Note that Electronic Communications PrivacyAct of 1986 - at first I was concerned that this prohibited me from sharing the contents of emails that are sent to me, but I'm not sure if that's true; note that [http://www.straightdope.com/columns/read/140/can-you-legally-publish-a-letter-someone-wrote-to-you](http://www.straightdope.com/columns/read/140/can-you-legally-publish-a-letter-someone-wrote-to-you) says you can keep letters but you can't publish them

[http://www.it.ojp.gov/default.aspx?area= privacy&page=1285](http://www.it.ojp.gov/default.aspx?area=privacy&page=1285) is a good list at federal Department of Justice

See [http://business.ftc.gov/documents/bus53-brief-financial- privacy-requirements-gramm-leach-bliley-act](http://business.ftc.gov/documents/bus53-brief-financial-privacy-requirements-gramm-leach-bliley-act) and [http://www.ftc.gov/opa/2009/11/glb.shtm](http://www.ftc.gov/opa/2009/11/glb.shtm) on GLB privacy- bunch of regulators came up with a model law

## Data brokers

Good overview at [http://www.propublica.org/article/everything-we-know-about-what-data-brokers-know-about-you](http://www.propublica.org/article/everything-we-know-about-what-data-brokers-know-about-you)

Pointed me to https://aboutthedata.com/ where I found out what Acxiom has on me

See  [FTC Calls for Consumer Disclosure by Data Brokers](http://www.insurancejournal.com/news/national/2014/05/28/330155.htm) (2014) where they relied on information from Acxiom Corp., Corelogic Inc., Datalogix, eBureau, ID Analytics, Intelius, PeekYou, Rapleaf, and Recorded Future

## Credit reports

"21 percent of consumers had errors in their credit reports, 13 percent had errors that affected their credit scores, and 5 percent had errors serious enough that they could be denied credit or forced to pay higher rates" - per Use Powers Federal Trade Commission Lacked

## Advocacy

PrivacyResearch Clearinghouse is a nonprofit focused on this (donated before).

PrivacyInternational (founded 1990) is across the world

DMAchoice lets you opt out of all the paper marketing spam.

SWIFT (Society for WorldWide Interbank Financial Telcommunications) is talking about setting up a system that lets people lease out their personal data per p36 of Jan 16-22 2012 issue.