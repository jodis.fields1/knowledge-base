Nevada
===

# Statewide
* no state income tax

# Las Vegas
* [Water isn’t a worry when it comes to Las Vegas growth](https://www.reviewjournal.com/business/housing/water-isnt-a-worry-when-it-comes-to-las-vegas-growth/)

## eating
Border Grill - enjoyed this place

## tech / business climate
* Tony Hsieh and community of startups
* https://vegastech.com
  *  https://vegastech.com/meet-vegas-tech-community-members/meet-nemo-chu-relocating-las-vegas/
* very few jobs in Stackoverflow
* https://www.reviewjournal.com/business/las-vegas-startup-scene-finding-its-footing-after-2012-boom/

## airbnb / hotels
note that airbnb is prolly banned https://m.lasvegassun.com/news/2018/dec/05/las-vegas-votes-to-curb-short-term-rentals-with-ne/:
"Las Vegas is the only jurisdiction in the Las Vegas Valley to permit short-term rentals. While North Las Vegas, Henderson and unincorporated Clark County do not allow the rentals, listings on Airbnb showed a combined 5,800 properties in those jurisdictions"

## housing
* TODO: email about security deposit
* https://www.apartmentratings.com/

### properties - 4.5 stars on Google Maps / Yelp
* Lofts at 7100 https://www.loftsat7100.com/
* Mirabella Apartments
  * $745/month 1br
  * bad reviews, bed bugs
* Tivoli https://lasvegasliving.com/community/floorplans/tivoli/detail/tesoro/1049
  * spoke on phone, flexible terms
  * Ovation Apartments in general seems well-run
  * also runs Altessa, found via Yelp
  * carpets :(
* Sugar Tree Apartments
  * https://www.sugartreeapartmenthomes.com
  * few availabilities, requires income
* The Oasis Apartments
  * https://www.liveoasisapts.com/Floor-plans.aspx
* Palermo Apartments
  * https://www.maxxproperties.com/palermo-apartments-las-vegas-nv/floorplans
  * not many availabilities
* Greenville Park Apartments
  * https://www.mygreenvilleparkapartments.com/floorplans.aspx
  * no roach complaints?
* https://702housing.com/rental-properties/
  * expensive, month-to-month
* Suncrest Townhomes at around $1500/month per https://www.liveatsuncresttownhomes.com/floorplans/

### properties outside downtown
* Fremont9 901 Fremont St, Las Vegas, NV 89101 - https://www.fremont9.com
  * no complaints of roaches!
* [Ridge on Charleston](https://www.rentcafe.com/apartments/nv/las-vegas/ridge-on-charleston/default.aspx) - $498 for 327 sq. ft.
  * cockroach problem, poor conditions per Google Maps
* Somerset Commons Apartments - mediocre reviews but better, roaches?
* $800 studio - ??
* [Mark One](https://hotpads.com/mark-one-las-vegas-nv-89109-ske0be/pad) - $900 
  * low reviews on apartmentratings.com, roaches
* Sterling Park - 2800 S Eastern Ave, Las Vegas, NV 89169 - low reviews on Google Maps

### properties downtown
Experience: [Micro apartment living in downtown Las Vegas: The 211 Apartments](https://mashonward.com/micro-apartment-downtown-las-vegas/)

* rents from $700 for microunits downtown to $1100 for nicer stuff
* $695 for 211 - microunits https://www.rentcafe.com/apartments/nv/las-vegas/the-2110/default.aspx
* [Spectra at 4000](https://www.rentcafe.com/apartments/nv/las-vegas/prism-at-bonanza/default.aspx) - $786 for 1br / 1 bath
  * roach infestations
  * move in date starts May


## news
* lasvegasnow.com / https://en.wikipedia.org/wiki/KLAS-TV
* Las Vegas Sun

## coworking
* https://www.workinprogress.lv/


## politics
* As of 2019, Democratic trifecta

## healthcare
See gastro-providres

[Is healthcare in Vegas THAT bad?](http://www.city-data.com/forum/las-vegas/2605770-healthcare-vegas-bad.html) - apparently