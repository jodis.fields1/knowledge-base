Texas
====

## Houston
[Houston Provides Lesson For How Fast-Growing Cities Can Relieve Congestion](https://marketurbanismreport.com/houston-provides-lesson-fast-growing-cities-can-relieve-congestion/)

## Austin

### tech / business climate
Austin - a small handful of IPOs are discussed by Hurt's  [The state of tech entrepreneurship in Austin](http://lucky7.io/post/the-state-of-tech-entrepreneurship-in-austin "http://lucky7.io/post/the-state-of-tech-entrepreneurship-in-austin")

### housing austin ??
* [LAKELINE VILLAS](https://www.udr.com/austin-apartments/cedar-park/lakeline-villas/)
  * $905 studio

### housing urban / downtown
* downtown housing is quite a bit more expensive
* Century Plaza Apartments - 4210 Red River St, Austin, TX 78751, USA - $900 for 400 sq. ft. studio seems par
  * mediocre reviews; no bugs
* [Platform Apartments](https://www.platformapartments.com/) - 2823 E Martin Luther King Jr Blvd, Austin, TX - fancy studio near Downtown for $1,4516
  * https://platformapartments.com/floorplans/e1/

### housing outskits
South:
* The Preserve at Travis Creek: 5604 Southwest Parkway, Austin, TX
  * ~$1,200 for a 1br
  * hardwood floors
* Asher: 10505 S IH 35 Frontage Rd, Austin, TX
  * starts at $975 for 556 sq ft
* The Reserve: 1016 W Stassney Ln, Austin, TX 
  * $990 for 481 sq ft

North:
* Ambrosio: 14301 N IH 35 Pflugerville, TX 78660
  * from $980 for 475 sq ft
* Sonterra Apartment Homes
  * from $897 for 692 sq ft
  * bugs, black mold in Google Maps