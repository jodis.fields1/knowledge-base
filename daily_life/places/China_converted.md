China
===

Constant fascination of mine:

## Politics
2014-07: read In China,  [Xi Jinpings anticorruption drive snares a tiger](http://www.csmonitor.com/World/Asia-Pacific/2014/0729/In-China-Xi-Jinping-s-anticorruption-drive-snares-a-tiger) (2014) about going after Zhou Yongkang which mentions that behind it is also Wang Qishan, head of the Central Commission for Discipline Inspection (CCDI); also see  [Xi risks stability with attack on high-profile ‘tiger’](http://www.ft.com/intl/cms/s/0/708c3c4e-1735-11e4-b0d7-00144feabdc0.html) (2014)

Run partly by "grey eminence" powers behind the throne; Eric X. Li gave an intriguing TED talk

Xi Jinping constitutes special team to probe top leader - notes decadal change in leadership and investigation into PetroChina exec

One year on, Xi Jinping still has everyone guessing - notes Standing COmmittee was weakened

## Overview
[101 Lectures to Learn All About China](http://www.onlinecolleges.net/2010/10/13/101-lectures-to-learn-all-about-china/) - saw from reddit thread, did not view any


### Daily life
[Chinaredditors, what are the best websites youve found for living in China?](http://www.reddit.com/r/China/comments/d6gb4/china_redditors_what_are_the_best_websites_youve/) - various helpful websites

## Government
National Reform and Development Corp is very powerful

### Media
I have an RSS feed for Caixin; saw a  [2011 reddit thread on reading](http://www.reddit.com/r/China/comments/e5eyl/where_do_you_guys_get_your_china_news_and_info/) and another on  [watching or listening](http://www.reddit.com/r/China/comments/ei2rt/what_chinese_media_do_you_listen_to_or_watch/)   

## Language (Chinese)

[2011 reddit thread on learning](http://www.reddit.com/r/China/comments/dzrkc/for_all_the_laowai_if_youre_learning_chinese_ive/ "http://www.reddit.com/r/China/comments/dzrkc/for_all_the_laowai_if_youre_learning_chinese_ive/") and  [another thread](http://www.reddit.com/r/China/comments/di9br/i_am_an_english_teacher_at_henan_university_in/ "http://www.reddit.com/r/China/comments/di9br/i_am_an_english_teacher_at_henan_university_in/") recommends chinesepod.com

Not that many people speak English; read The statistics of English in China: An analysis of the best available data from government sources (2012) which puts fluency around 2-3%

## Geography
First-Tier Cities:
* Four municipalities: Beijing, Chongqing, Shanghai, Tianjin

Cities with total retail sales of more than RMB30bn, annual per capita income of 
RMB11,000 and high per capita retail sales as proportion of income:
* 10 provincial capitals: Changchun (Jilin), Chengdu (Sichuan), Guangzhou (Guangdong),
Hangzhou (Zhejiang), Harbin (Heilongjiang), Jinan (Shandong) Nanjing, (Jiangsu),
Shenyang (Liaoning), Wuhan (Hubei), Xi’an (Shaanxi)
* Four leading cities: Dalian, Qingdao, Shenzhen, Xiamen

Second-Tier Cities
* 17 provincial capitals: Changsha (Hunan), Fuzhou (Fujian), Guiyang (Guizhou), Haikou
(Hainan), Hefei (Anhui), Hohhot (Inner Mongolia), Kunming (Yunnan), Lanzhou (Gansu),
Lhasa (Tibet), Nanchang (Jiangxi), Nanning (Guangxi), Shijiazhuang (Hebei), Taiyuan
(Shanxi), Urumqi (Xinjiang), Xining (Qinghai), Yinchuan (Ningxia), Zhengzhou (Henan)
50 prefecture-level cities, including, Ningbo, Suzhou, Wuxi, Wenzhou, Nantong,
Dongguan, Zhanjiang

15 more cities with populations of between 500,000 and 2mn

Third-Tier Cities
* Approximately 200 county-level cities

Fourth-Tier Cities
* Approximately 400 capitals of county towns


### Regions
Regionally, there's a traditional split and an economic split per  [wiki list of regions](http://en.wikipedia.org/wiki/List_of_regions_of_China "http://en.wikipedia.org/wiki/List_of_regions_of_China"):

Western China( [wiki](http://en.wikipedia.org/wiki/Western_China "http://en.wikipedia.org/wiki/Western_China")) - from west, Tibet, Sichuan, enormous Xinjiang bordered to the southeast by large Qinghai (both weak economies), which is bordered east by Gansu, which is in turn bordered east by Shaanxi (which is west of Shanxi), bordered  part of the economic split Chengdu far west and Chongqing a little east of that; in the southwest, Yunnan in the far west borders Vietnam with Guizhou bordering it on the east

East China- largest population; from southeast includes: Taiwan (not truly relevant); Fujian (36m); north comes Zhejiang (55m with Hangzhou); Jiangsu (79m with Nanjing and highest per capita GDP)  and Shanghai; north to Shandong (95m with Jinan); and west of Jiangsu to Anhui (55m with capital Hefei)

North China( [wiki](http://en.wikipedia.org/wiki/North_China "http://en.wikipedia.org/wiki/North_China")) - Beijing, Tianjin, Hebei (which these two were carved out of), Shanxi, and Inner Mongolia

Northeast China( [ wiki](http://en.wikipedia.org/wiki/Northeast_China "http://en.wikipedia.org/wiki/Northeast_China")) - Liaoning, Jilin, and Heilongjiang - note there's a bay called Yellow Sea with the Bohai Sea deeper which has some offshore oil drilling   

South Central China- Hainan in the far south island was first Special Economic Zone (?); Guangdong (104m so largest province) is major powerhouse with Shenzhen and Guangzhou plus nearby Macau and Hong Kong; Guangxi with weaker economy to the west; north of these two is Hunan which has Dongting Lake and several large 7+ million pop cities; further north is Hubei (capital Wuhan) which is north of Lake Dongting; further north is Henan "birthplace of Chinese civilization" near the North ChinaPlain and d in the center of a lot of action; 

Rivers: Yangtze River emptying in Shanghai which the Three Gorges Dam was built on and Yellow River running in the northern area; also there is also the Pearl River running south (known as the Guangdong River) from wh ich flows tributaries such as the Xi River; 
