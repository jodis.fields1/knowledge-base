Arizona
===

# Phoenix

## tech / business climate
Resources:
* https://angel.co/arizona
* https://www.bizjournals.com/phoenix/


### tech orgs
https://www.bizjournals.com/phoenix/news/2018/06/13/report-arizona-tech-industries-top-8-000-companies.html | AZ Tech Council: Over 8,000 tech companies in AZ - Phoenix Business Journal
https://wiki.hackerspaces.org/MACH | MACH - HackerspaceWiki
https://www.bizjournals.com/phoenix/news/2018/03/29/report-phoenix-tech-growthstory-shaped-by-talent.html | CBRE & GPEC report: Phoenix tech story shaped by talent, pro-business climate and quality of life - Phoenix Business Journal
https://yesphx.com/ | #yesphx – Phoenix, Arizona's Startup Community


### articles on tech
Articles:
* [Silicon Valley South West? The Arizona Tech Scene Is No Mirage](https://innotechtoday.com/silicon-valley-south-west-arizona-tech/)
  * "complete revamp of the city’s Warehouse District. Within two square miles of the area, 60 tech startups currently operate inside Galvanize’s technology campus"
* [Report: Phoenix tech growth story shaped by talent, pro-business climate and quality of life](https://www.bizjournals.com/phoenix/news/2018/03/29/report-phoenix-tech-growthstory-shaped-by-talent.html)
  * "offices in Phoenix, which has an average office asking rent of $25.49 per square foot, and where a software engineer makes an average of $95,298 and the average corporate tax rate is 4.9 percent. When comparing other major tech hubs, Phoenix is more competitive. Among the highest, San Francisco has an average office asking rent of $72.76, an average software engineer salary of $126,726 and average corporate tax rate of 8.8 percent"

## housing

### luxury downtown
* http://dtphx.org/live/for-rent/ - these all seem to be luxury

#### Camden
Camden is on the stock market

  * [Camden Copper Square](https://www.camdenliving.com/phoenix-az-apartments/camden-copper-square/apartments) - 1br 600 sq ft for $1,099/month
    * called about alternative income; she suggested just following the application and potentially putting a cosigner
    * uses RealPage - saved quote and went thru application with lazycritic / Jes Slow
    * good reviews
  * [ Camden Pecos Ranch](https://www.camdenliving.com/chandler-az-apartments/camden-pecos-ranch) - 
    * cheaper at $1000 for 1br, fewer extra fees

#### other
  * [44 Monroe](http://44monroe.com)  for luxury - A1 is prolly $1500/month or so
  * [601 FillMore](https://www.purefillmore.com) - 1br at 686 sq ft for $1350/month
    * http://dtphx.org/2016/11/16/downtown-living-take-sneak-peek-inside-alta-fillmore-apartments/
  * [Broadstone](https://broadstonerooseveltrow.com)

### non-luxury
* 1br at 620 sq ft for $850/month https://www.zumper.com/apartment-buildings/p336680/445-w-osborn-rd-phoenix-az
* 1br at 650 sq ft for $935/month https://www.zumper.com/apartment-buildings/p7403/948-e-devonshire-avenue-phoenix-az