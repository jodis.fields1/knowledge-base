Sacramento
===

California general: https://www.sacbee.com/news/databases/article235060512.html

## tech
https://startupsac.com/podcast/sacramento-startup-scene-roundup-april-2018/

## housing
https://www.rentcafe.com/luxury-apartments-for-rent/us/ca/sacramento/?OrderBy=Rent
* luxury, ascending

https://www.showmetherent.com/listings/Sacramento,_CA,_USA/beds:0

## yuba city

Internet: https://get.succeed.net/signup

### ferguson & brewer
called 530-872-1810

https://www.fergusonandbrewer.com/Apartments/module/properties/#location%3DSacramento%2C%20CA%26zoom%3D13%26lat%3D39.130894710949775%26lng%3D-121.63418539999999
* Bridge Street - good reviews https://goo.gl/maps/3kK5kNLJ7CRwqPxZ9
* Vintage Place has 1 (2 coming up)
* Rancho Carmel
  * called, opening June 15
* Sheraton has 1, Thayer has 1 coming up, 
* Oak Grove Sacramento
* Vintage Creek Sacramento

called Shadowbrook, number is broken https://www.fergusonandbrewer.com/paradise/shadowbrook-apartments - a bunch burned down in a fire!

### Valley Fair Realty Corporation
https://www.showmetherent.com/listings/companyid:94962

* 1250 Melton Dr https://www.apartments.com/1250-melton-dr-yuba-city-ca/816nzh3/
  * none available? https://www.apartments.com/pheasant-court-apartments-yuba-city-ca/smglc54/

### general
$1000 / month studios
* http://www.willowglensacramento.com/Floorplans/46146
* https://www.pacificcoastpropertiesllc.com/listings/detail/ee3c49f6-16a7-43c4-b3af-4bb0aa613f3e

Cheap apartments there on zumper/padmapper & hotpads
https://www.zumper.com/apartments-for-rent/yuba-city-ca/under-1100?property-categories=apartment&box=-121.65959358215332,39.11621020143469,-121.56466484069824,39.196076813671695
https://hotpads.com/yuba-city-ca/cheap-apartments-for-rent?beds=0-1&lat=39.0511&lon=-121.6897&orderBy=lowPrice&z=10
https://www.apartmentguide.com/apartments/California/Yuba-City/
https://www.abodo.com/yuba-city-cat
https://www.apartments.com/yuba-city-ca/?so=2

* Feather Downs Apartments
 https://www.zumper.com/apartment-buildings/p202280/feather-downs-yuba-city-ca
  * TODO
* 1170 Kenny Dr https://hotpads.com/1170-kenny-dr-yuba-city-ca-95991-ttyya2/1-40/pad?beds=0-1&lat=39.0511&lon=-121.6897&orderBy=lowPrice&z=10
  * Joshua Tree Apartments
  * negative reviews
  * https://www.kjaxproperty.com/vacancies
* 341 Del Norte https://www.abodo.com/yuba-city-ca/341-del-norte-ave
* 1575 Heather Dr Garden West One Apartments https://www.apartments.com/garden-west-one-apartments-yuba-city-ca/wbzyttk/
  * looks like a really nice place  
  * https://www.pcmliving.com/our-communities
* 1119 G St https://www.apartments.com/1119-g-st-marysville-ca/5s159bm/
  * says month to month https://www.abodo.com/marysville-ca/1119-g-street
* 720-720 1/2 14th St https://www.apartments.com/720-720-1-2-14th-st-marysville-ca/n5ywrxn/
  * TODO

## lodi
* 101 W. Locust St https://www.rentcafe.com/apartments/ca/lodi/locust-square-apartments0/default.aspx
  * murphy beds!

## fresno
Of course it's cheap

### suburbs
* [Abby Creek Apartment Homes](https://www.aspensquare.com/apartments/california/carmichael/abby-creek-apartment-homes) - 
* [Warren Properties Fulton](https://www.warrenproperties.com/content/fulton) - $875 studio for month to month!
  * Fair Oaks: no openings
* H16 https://h16midtown.com/
  * spendy, has EV charging
* https://hotpads.com/1716-capitol-ave-sacramento-ca-95811-sm1bhh/pad?lat=38.5719&lon=-121.4288&z=10
  * super-cheap
* 2215 9th Street Apartments https://www.centuryplazadowntown.com/
* 2001 Bowling Green Dr & 2244 Ethan Way Apartments https://www.zumper.com/apartment-buildings/p452931/2001-bowling-green-dr-2244-ethan-way-sacramento-ca
* Hidden Oaks https://www.hiddenoaks-citrusheights.com/
  * flexible leases, 3x income requirement, multiply by lease, $1150 12-month lease, $4k 1-month
* Agave Apartments https://www.agaveapts.com/floorplans
  * closed for renovations, no apts
* The Ridge at McClellan https://www.rentcafe.com/apartments/ca/north-highlands/the-ridge-at-mcclellan/default.aspx
  * did not pick up
* Olympus Park https://www.rentcafe.com/apartments/ca/roseville/olympus-park/default.aspx
  * available, $1100 for 12-month lease
* Shelfield Apartments https://www.rentcafe.com/apartments/ca/carmichael/shelfield-apartments/default.aspx
  * no studios, not sure about bank statements, 6-month lease available no EV; July 15
  * also manages Carmichael https://www.rentcafe.com/apartments/ca/carmichael/carmichael-apartments/default.aspx
* Olympus Park https://www.rentcafe.com/apartments/ca/roseville/olympus-park/default.aspx 
* Ashford Heights https://www.rentcafe.com/apartments/ca/sacramento/ashford-heights/default.aspx
  * $871 affordable; max $36k
* Falls at Arden https://www.rentcafe.com/apartments/ca/sacramento/falls-at-arden/default.aspx
  * did not pick up
* Country Glen Apartments https://www.rentcafe.com/apartments/ca/sacramento/country-glen-apartments-0/default.aspx
* Greenhaven Oaks https://www.greenhavenoaks.com/floorplans/#studio-250
* left a message

Stonegate https://www.apartments.com/stonegate-apartments-sacramento-ca/jsd8jq4/ - no availability

### affordable
* Adobe at Evergreen Apartments https://www.rentcafe.com/apartments/ca/davis/adobe-at-evergreen-apartments/default.aspx
  * shared apts


### AMC Apartments
* Woodlands https://www.thewoodlandsapt.com/apartments/ca/sacramento/amenities
  * income requirement via assets = 2.5 (income multiple of rent) * rent * lease term
  * air conditioning included
  * parking lot, but no EV charging
* Amber Grove http://www.ambergrove-apts.com/sacramento-ca/amenities
* Charleston https://www.liveatthecharleston.com/