Sources:
[Capitol Weekly](http://capitolweekly.net/) (Open California) - journalism nonprofit
* emailed editor john.howard@capitolweekly.net about Yang in 2019, got no response
YIMBY Congress - has [a slate](http://www.sfyimby.org/slate/)

Dislike: Alex Padilla (Secretary of State) for his super bureaucratic disallowance of email or telephone communication by voters

My votes as a district 9 Supervisor constituent:

###### San Francisco Democratic Country Central Committee

In 2016 apparently subject to some controversy around a "reform" slate, with Aaron Peskin identified as a leader. See [Airbnb, tech execs fund nasty DCCC attack piece](http://48hills.org/2016/05/12/airbnb-tech-execs-fund-nasty-attack-dccc-attack-piece/) as well as:

* [Who Is Not Running for DCCC?](http://sfbaytimes.com/who-is-not-running-for-dccc/) - by Zoe Dunning, a candidate, with an insightful overview
* [DCCC Proposal in the SF Chronicle](https://votealix.com/2016/03/24/dccc-proposal-in-the-sf-chronicle/) - reform proposal
* [S.F. Democrats battle over shades of blue](http://www.sfchronicle.com/opinion/diaz/article/S-F-Democrats-battle-over-shades-of-blue-7237926.php)
* [Reform Slate angling to oust moderates from Democratic committee](http://www.sfchronicle.com/bayarea/article/Reform-Slate-angling-to-oust-moderates-from-6858258.php) - uh-oh
* [Crowded field vies for obscure but mighty Democratic committee](http://www.sfchronicle.com/politics/article/Crowded-field-vies-for-obscure-but-mighty-7453253.php) - 
* [Another advocate doesn’t mean better advocacy](http://www.sfexaminer.com/another-advocate-doesnt-mean-better-advocacy/) - by Joel P. Engardio ([blog](http://www.engardio.com/)) who is a member and seems to be very reasonable

Coverage of 2015 activities at [Airbnb, District 3 endorsements top Democrat talks](http://www.sfexaminer.com/airbnb-district-3-endorsements-top-democrat-talks/) (2015). Also see [SF Democratic Party votes against the Mission
](http://48hills.org/2015/05/27/sf-democratic-party-votes-against-the-mission/) which shows how incumbents voted in a critical affordable housing measure.

###### Candidates for District 17:
* [John L. Burton](https://en.wikipedia.org/wiki/John_L._Burton) - sexual harassment lawsuit, plus long-term incument. No.
* London Breed ([wiki](https://en.wikipedia.org/wiki/London_Breed)) - [went off on someone at Twitter](http://sfist.com/2013/06/07/london_breed_is_the_amanda_bynes_of.php) which is funny
* Jo Elias Jackson - nothing about her, no website? No.
* Francis Tsang - has [a website](https://francistsangfordccc.com/) which lacks any substance whatsoever. No.
* Marlene Tran ([ballotpedia](https://ballotpedia.org/Marlene_Tran)) - former City Supervisor candidate, no website or substantive statement. [Appeals heavily to local Chinese](http://www.sfweekly.com/sanfrancisco/marlene-trans-success-shows-strength-of-asian-voters/Content?oid=2179486). No.
* Gladys Soto. She has [a facebook page](https://www.facebook.com/Gladys-Soto-for-San-Francisco-Democratic-County-Central-Committee-AD17-170835889621423/) but eh on details. Probably not.
* Arlo Hale Smith. [Suspended](http://members.calbar.ca.gov/fal/Member/Detail/96971) from practicing law in California historically. No.
* Melissa San Miguel. Young (29), background in youth law. Native to the Mission. [Tweeted](https://twitter.com/ml_sanmiguel) at her. Valedictorian at UC Berkeley? Maybe.
* Jill Wynns ([wikipedia](https://en.wikipedia.org/wiki/Jill_Wynns)) - very long-term board member, Wikipedia says that she is known for rooting out a corrupt Supervisor. Yes.
* Wade Woods. Long-term affordable housing advocate, part of "Stop the Manhattanization of San Francisco" movement. No.
* Cindy Wu. In her 30s. Wants to modernize Chinatown while retaining the residents. Fought a coworking space moving in. See [This Woman Is Fighting To Preserve San Francisco's Chinatown By Giving It A Sustainable Makeover](http://www.fastcoexist.com/3058076/change-generation/this-woman-is-fighting-to-preserve-san-franciscos-chinatown-by-giving-it-a) (2016). Maybe, probably not.
* Scott Wiener. Favored for State Senate. Probably a yes.
* Petra Dejesus. Long-term public interest lawyer, serves on Police Commission. Seems old-school. [Attacked by a tech group](https://www.facebook.com/petra.dejesus.7/posts/837503919715769) against affordable housing progressives. Maybe, probably not.
* Bevan Dufty ([Wikipedia](https://en.wikipedia.org/wiki/Bevan_Dufty)). Former Supervisor, involved long-term. Maybe, probably not.
* Zoe Dunning. ([Wikipedia](https://en.wikipedia.org/wiki/Zoe_Dunning)). Known for gay rights activism which took some guts and currently a consultant, but not clear what sort of innovations she wants to bring. Has a [website](http://www.zoedunning.com/), [twitter](https://twitter.com/zoedunning) which is nice. Probably yes.
* Malia Cohen ([Wikipedia](https://en.wikipedia.org/wiki/Malia_Cohen), [twitter](https://twitter.com/MaliaCohen)). Current Supervisor for 10. Sponsored some legislation ("Fair Chance", Sanctuary laws, gun control). Maybe?
* Wendy Ha Chau ([twitter](https://twitter.com/attorney_chau)) - not much on her politics, tweets mostly seem oddly commercial or self-promotional. No.
* David Campos. My Supervisor (District 9, including Mission). Need to research him. Pushed for CleanPowerSF but also reinstated Mirkarimi after domestic violence.
* Jane Kim. District 6 Supervisor. Spearheaded Twitter's tax break which was great. Yes.
* Frances Hsieh. Long-term legistlative aide. Not clear what she plans to accomplish. Maybe, probably not.
* Tom Hsieh, Sr. Old-school political guy, known as conservative.
* Rodney Hauge. No information about him. No.
* Rick Hauptman. Hard to find information about him. No.
* Shaun Haines. ([twitter](https://twitter.com/mr_sh_sf), [homepage](http://www.shaunhaines.net/)). Seems like a decent guy with lots of energy.
* Gary McCoy. No information. No.
* Rafael Mandelman ([homepage](http://www.rafaelmandelman.com/)). Seems mainly interested in saving the City College. No.
* Sophie Maxwell ([Wikipedia](https://en.wikipedia.org/wiki/Sophie_Maxwell)). No homepage. No.
* Alysabeth Alexander. Part of the Progressive Reform movement. No.
* Tom Ammiano. Father of gay comedy. Former state legislator. No.
* Joshua Arce. Major clean power advocate, moderate. Yes.
* Jon Golinger. Reform slate guy with a good public profile. No.
* Pratima Gupta. Reform slate doctor. I like that she works with St. James Infirmary which helps sex workers. Maybe.
* David Giesen. [Innovative idea of taxing cars](http://www.thecommonssf.org/dave_giesen_4_dccc). Maybe.
* Michael E. Grafton. No information. No.
* Aaron Peskin. The leader of the reform movement.
* Nicholas Pasquariello. No information. No.
* Leah Pimentel. Trained by Emerge California per [Leah Pimentel — a third-generation political activist from Bayview](http://thewesternedition.com/?c=117&a=2198) (2012).
* Rebecca Prozan ([homepage](http://www.rebeccaprozan.com/about)) - policy leader at Google with a lot of energy. Yes.
* Alix Rosenthal ([homepage](https://votealix.com/) - Incumbent. Government affairs at Sidebar. Yes.

###### Senate

Dianne Feinstein's seat opened up. Between Kamala Harris and Loretta Sanchez, really, with Tom Del Beccaro on the Republican side. Selected Kamala Harris.

###### House
12th Congressional district.
Nancy Pelosi has this locked up, but I voted for Barry Hermanson anyway. Was Preston Picus on the ballot for me? Can't see why he's bothering to run against Pelosi.

###### State assembly
17th district.
I voted for my acquaintance Matthew Del Carlo, an SF Republican with no chance that I met at the Startuphouse, to encourage engagement and diversity.

###### Ballot provisions

I voted for every provision but the affordable housing one. While I'm already starting to regret that, I'm inclined to think that we just need to increase the housing stock in the city.