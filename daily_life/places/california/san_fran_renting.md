For specifics on vendors, see life-log/renting_san_fran.md

### renting
* [Why Was California’s Radical Housing Bill so Unpopular?](https://slate.com/business/2018/04/why-sb-827-californias-radical-affordable-housing-bill-was-so-unpopular.html) (2018-04) - SB 827
    * [Why is liberal California the poverty capital of America?](http://www.latimes.com/opinion/op-ed/la-oe-jackson-california-poverty-20180114-story.html)

### housing development
https://www.google.com/search?hl=en&q=california%20underutilized%20land | california underutilized land - Google Search
https://m.sfgate.com/business/article/Study-pushes-infill-housing-Underutilized-land-2538372.php | Study pushes infill housing / Underutilized land in Bay Area could have 350,000 units - SFGate
https://www.acrevalue.com/plat-map/CA/?lat=37.421469&lng=-119.272005&zoom=7 | California Plat Map - Property Lines, Land Ownership | AcreValue
https://www.fs.usda.gov/Internet/FSE_DOCUMENTS/stelprdb5392277.pdf | Map of the national forests and other public lands of California
https://data.oaklandnet.com/Property/Alameda-County-Parcel-Boundaries/dnp4-5zvt | Alameda County Parcel Boundaries | Open Data Portal