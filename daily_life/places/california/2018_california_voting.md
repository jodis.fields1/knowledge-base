### governor
#### general
* Gavin Newsom - regretfully

#### primary
* Deanne Eastin - cool, but pie in the sky promises; tweeted her about it
* Jon Chiang - detailed numbers in platform, so prolly vote for him
* Michael Shellenberger - cool dude
* Gavin Newsom - no

### lieutenant governor
Convinced by https://www.sacbee.com/opinion/election-endorsements/article219030860.html:
* Eleni Kounalakis - my pick, housing developer
* Ed Hernandez - seems good in general but not as good

### Controller
* Betty T. Yee - my pick; sadly, challenger seems like a a bit of a crackpot

### Treasurer
* Fiona Ma - sonds like a real reformer per [Chronicle Recommends: Fiona Ma for state treasurer
](https://www.sfchronicle.com/opinion/editorials/article/Chronicle-Recommends-Fiona-Ma-for-state-treasurer-12870656.php)

### Attorney General
* Steven C. Bailey - my pick, since Xavier Becerra did not care enough to provide information to votersedge (emailed him)

### state representative
* Alejandro Fernandez - my pick, seems very engaged
    * emailed David Chiu about lack of votersedge information

### Secretary of State
* Alex Padilla - my pick, altho I recall seeing that he was against digitizing some stuff in the past :(

### federal senate
* Kevin de Leon - my pick
* Dianne Feinstein - too comfortable

### federal representative
* Lisa remmer - my pick
* Nancy Pelosi - too comfortable

### reps
* My supervisor district: 9
* State Assembly: District 17
* State Senate: District 17
* Federal House: District 12

#### SF Supervisor district 6
not my district, but interesting...
Jason Lee Jones - interesting fellow, SJW, "barista", tweeted him
Matt Haney - young and rising star, endorsed by the establishment per [Progressive Matt Haney Declares Candidacy For District 6 Supervisor](http://sfist.com/2017/09/20/matt_haney_declares_for_jane_kims_d.php)
Sonja Trauss - endorsed by Scott Wiener per [In SF politics, the action’s in District 6, a bellwether for rest of city](http://www.sfchronicle.com/bayarea/article/Mix-of-residents-issues-makes-District-6-a-11883042.php)

#### propositions
Used votersedge.org for advice:

Statewide:
* Proposition 1 - bonds for affordable housing -> yes, hesistantly
* Proposition 2 - more housing bonds -> yes, hesistantly
* Proposition 3 - water etc bonds -> yes
* Proposition 4 - childrens hospitals -> yes
* Proposition 5 - property taxes -> no because it contains constitutional amendment, also opposed by mostly everyone
* Proposition 6 - eliminate road repair and transport funding -> no, gas taxes are good
* Proposition 7 - legislative statute for DST -> yes
* Proposition 8 - price fixing dialysis centers -> no, price fixing does not work
* Proposition 9 - pulled by California Supreme Court
* Proposition 10 - allow more rent control -> no, rent control does not work
* Proposition 11 - on-call ambulance -> yes
* Proposition 12 - standards for farm animal confinment -> yes

Citywide:
* Proposition A - seall -> yes
* Proposition B - privacy policy -> no, saw online that this might interfere with public records transparency
* Proposition C - homeless shelter funding -> yes, after a bit of reading
* Proposition D - cannabis tax -> yes
* Proposition E - distributing funds for cultural purposes -> yes
