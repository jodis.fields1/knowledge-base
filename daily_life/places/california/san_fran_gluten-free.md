San Fran gluten-free
===

See also: 
* knowledge/daily_life/cooking/restaurants.md
* knowledge/daily_life/places/california/san_fran_bay_area.md

## Health inspections
[https://101g-xnet.sfdph.org:8443/ords/f?p=132:1](https://101g-xnet.sfdph.org:8443/ords/f?p=132:1)

### Bare hand law
[In California, Chefs Fight for Bare-Hand Contact](http://abcnews.go.com/Health/wireStory/california-chefs-fight-bare-hand-contact-23030612) (2014) - apparently a pretty common law

## Gluten-free search
[http://6sensorlabs.com/blog/2015/9/9/food-testing-cross-contamination](http://6sensorlabs.com/blog/2015/9/9/food-testing-cross-contamination)

findmeglutenfree.com - best

http://glutenfreetravelsite.com has National Foundation for Celiac Awareness’s GREAT Kitchens Program

gluten-free passport requires money

Kitchens With Confidence - emerging in 2019 per http://www.restaurantnewsrelease.com/kitchens-with-confidence-certified-four-kitchens-across-the-country-in-june/85120631/

## grocery stores
* Bi-Rite market has gluten-free sushi under their own barnd as does Sprouts under Hissho brand

## Bay Area
See fearlessdining.com and particularly  [Restaurant Database](http://www.fearlessdining.com/restaurant-database/)