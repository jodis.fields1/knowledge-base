# 350 SF housing discussions

## Community Housing Act
https://docs.google.com/document/d/1WQtcPuRuSPNFlxPrm6hPUFF4LFSi_mmgBbx-GgvdrQw/edit#heading=h.idbumwnu2jrr

https://twitter.com/ShahidForChange/status/1235728720838537217

* Concerned by financialization of our economy, so public financing w/o middlemen seems reasonable
* Background in government leaves me w/ mixed experiences
* More money for dense housing w/ clean energy = good thing
* 410,000,000/1500 = $273k? slide 12
* No mention of density in the proposal? Zoning P - is this fleshed out more? Missed it?
* There is some indexing for inflation
  * Does it apply to rent payments?
* BoS flexibility to change the law without requiring another initiative?
* Open-source code and third-party verification of lottery?
* No ownership interest in any other property - how to verify? Will residents submit to detailed investigation of their finances?
  * enforcement & punishment in case of violation?
* Green tie-in - demand-side response & batteries e.g. laundry during cheap electricity time (day)
* SEC. 110.8. PROGRAM GOALS AND PLANS
  * "Should the MOHCD fail to achieve its goal of 1500 units acquired or constructed during a Goal Period, the MOHCD shall carry forward the unfulfilled number of units from that Goal Period and add that unfulfilled number to the following Goal Period’s goal."
  * accumulating backlog?