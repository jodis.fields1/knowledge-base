# 2020 - California voting
[San Francisco Department of Elections Certifies March 3, 2020, Consolidated Presidential Primary Election](https://sfelections.sfgov.org/article/san-francisco-department-elections-certifies-march-3-2020-consolidated-presidential-primary)
* Dominion Voting Systems - led me to [Official site of the San Francisco Open Source Voting System Technical Advisory Committee (OSVTAC)](https://osvtac.github.io/recommendations/single-page)
  * https://twitter.com/SFOpenVoting

## local
### judicial
Progressives sweeped judicial races w/ [Final, final results: Progressives win three judges, DCCC](https://48hills.org/2020/03/final-final-results-progressives-win-three-judges-dccc/)

### DCCC
[WINNERS AND LOSERS IN THE SF DCCC RACE](https://brokeassstuart.com/2020/03/05/winners-and-losers-in-the-sf-dccc-race/)
* "Social Justice Democrats slate are still the huge winners, with 21 of 24 contests going their way over the rival Mobilize Organize slate"

I voted for YIMBY Action, but they mostly failed
* Austin Hunter