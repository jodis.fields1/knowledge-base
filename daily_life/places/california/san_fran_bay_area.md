
Burlesque show?

## news
https://twitter.com/kron4news

https://www.nbcbayarea.com/news/local/
* https://twitter.com/nbcbayarea

https://sanfrancisco.cbslocal.com/
* KPIX https://twitter.com/KPIXtv

https://www.kqed.org/

## housing
https://www.skylineheights.com/
* cheap at $1800, EV charging, Daly City

http://www.the-arbors-apts.com/
* $1800, Walnut Creek

## bars
* Bon Voyage in Mission has mocktails http://bonvoyagebar.com/#menus
* Oddjob - had some good mocktails
* Slate in Mission - had mocktails
* Velvet Cantina at 3349 23rd St, San Francisco, CA 94110
  * why?? oh yeah watermelon drink?
* The Willows - only a bit north, cool small arcade
* Devil's Acre - good non-alcoholic options
* GF friendly Thai place nearby?
speakeasy place?
* https://mariannessf.com/ - heard from https://www.sfgate.com/insidescoop/amp/Sarah-Ferguson-Duchess-of-York-mariannes-airbnb-13521805.php
Rum&Sugar - saw on Valentine's Day ad

## cafe
* Rise'n Grind - wifi password canvas3356baked

## Magic show
Paul Nathan - did Zane's show, http://www.firemagic.com/, also does Dark Kabaret https://www.facebook.com/DarkKabaret/

## comedy
punchline - ?
cobbs - famous people?
the setup - a couple times
cheaper than therapy - couple times

## dating
[OH! Poly Happy Hour - Oakland & San Francisco](https://www.facebook.com/groups/1619795184916731/)
OneTaste - orgasmic meditation - not technically dating but tangental; accused of being a cult

* Your Fucked Up Relationship - Organized by Endgames Improv

## restaurants
[Baby Blues BBQ](http://babybluesbbq.com/) - think it has gluten-free
Alba Rays - 
    * asked about GF options https://www.yelp.com/questions/alba-rays-are-there-gluten-free-options-anything-safe-for-someone-with-celiac-disease/2dvQfx-sCrtBhqRFIvmYzA

[The 7×7 Challenge: 50 Gluten-free San Francisco Dishes Get #nimatested](https://nimasensor.com/2017/05/16/7x7-challenge-50-gluten-free-san-francisco/?utm_source=googletext&gclid=CKPjk5OZlNQCFQihaAod-S8F-g)

## Downtown sights
The View - 
Yerba Buena Gardens - ?
Sea lions - ?
Ferry across the Bay
    * Alcatraz
    * Angel Island
    * Treasure Island?
* Hotel Zetta - playroom, Exit Reality VR

## crime
Most shootings along the 24th street corridor are Norte vs. Surreno stuff still. Think there's been at least one every few weeks. - 2017

## POPOS and tall places?
apparently the Transbay Transit project will have a huge POPOS
the bar in that hotel
http://sf-planning.org/privately-owned-public-open-space-and-public-art-popos
http://www.spur.org/sites/default/files/migrated/anchors/popos-guide.pdf

## Hiking
Roy's Redwoods with Ben Rodigas?
I did Mount Davidson http://www.7x7.com/san-franciscos-best-pre-brunch-hikes-1786978812.html

## north of the bay
2018-04: Sugarloaf State Park, stopped in Boyes Hot Springs on way back
    * Fairmont Hot Springs, Agua Caliente pool, Calistoga Spa is recommended

## Transport
1 California - major bus
49 - Mission to the Marina
14 - up Mission to downtown

## Critique
http://twilightoftheidols.org/san-francisco-sucks/

## advertising stuff
* SF Chronicle podcasts
* http://folklawradio.com/home/ by https://twitter.com/FolkLawMedia


## Local government
[YIMBY Action in the Press](https://yimbyaction.org/media/)

Ross Mirkarimi - interesting character, found thru  SFChronicle coverage of15th San Francisco City Survey which happens every other year

[Three charged in San Francisco public corruption case](http://www.mercurynews.com/crime-courts/ci_29420982/three-charged-san-francisco-public-corruption-case?source=infinite-up) (2016) -      