San Francisco Fitness
===


YMCA Embarcadero - my place

Active Sports Club - Union Square w/pool - decent alternative
## swimming
### city pools
TODO: Coffman Pool, 1700 Visitacion Ave.

Mine:
Garfield Pool, 1271 Treat Ave.
    * TODO: go here

Mission Pool, 1 Linda St.
    * why open so rarely? outside

Hamilton Pool and Recreation Center, 1900 Geary Blvd.
Martin Luther King Pool, 5701 3rd St.
North Beach Pool, 661 Lombard St.
Rossi Pool, 600 Arguello Blvd. 

### unorganized
https://sfrecpark.org/wp-content/uploads/Coffman-Summer-2019.pdf | Coffman-Summer-2019.pdf
https://sfrecpark.org/destination/herz-playground/coffman-pool/ | Coffman Pool | San Francisco Recreation and Park
https://sfrecpark.org/destination/mission-playground/mission-community-pool/ | Mission Community Pool | San Francisco Recreation and Park
https://sf.curbed.com/maps/swimming-bay-area-holes-pools-public-cheap-outdoor | Where to swim in the Bay Area for under $10
https://uforiastudios.com/instructors/megan-metcalfe/ | Megan Metcalfe - uforia Studios
https://sfist.com/2015/06/10/our_favorite_sf_swimming_pools/ | The Six Best Swimming Pools In San Francisco: SFist
https://sf.racked.com/2016/1/10/10746340/top-chain-gyms-san-francisco | San Francisco's Best Chain Gyms: A Complete Rundown of Cost and Amenities - Racked SF
