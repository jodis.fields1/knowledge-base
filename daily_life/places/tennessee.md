Tennessee
===

[Nashville And Memphis: An Economic Growth Disparity](https://marketurbanismreport.com/nashville-and-memphis-an-economic-growth-disparity/) (2018) - ??

## Nashville metro
Music City, capital,  in middle. "Nashville sits in a fast-growing region with cities like Atlanta, Raleigh and Charlotte, making it part of a professional-class, urbanizing New South"

Much better off than Memphis

Notable healthcare: https://en.wikipedia.org/wiki/Vanderbilt_University_Medical_Center

## Memphis
"The states that Memphis is directly adjacent to—Arkansas and Mississippi—continue to be poor, agrarian economies with minimal development. In this sense, the metro is culturally identifiable with the Old South". 63% black, "overall poverty rate of 27 percent"

[America's Progressive Developers, Memphis Edition: Crosstown Concourse](https://marketurbanismreport.com/americas-progressive-developers-memphis-edition-crosstown-concourse/) (2019) - cool

### housing
Very affordable

https://www.reddit.com/r/memphis/comments/7op847/why_the_hell_are_houses_so_cheap_in_memphis/dsbpfd3/ | liz_dexia comments on Why the hell are houses so cheap in Memphis? !

#### apartments
https://www.zumper.com/apartments-for-rent/memphis-tn | 1,319 Apartments for Rent in Memphis, TN - Zumper

#### auction
https://www.auction.com/details/791-e-waldorf-ave-memphis-tn-38106-2481769-e_12233/?utm_source=zillow&utm_medium=feed&utm_campaign=trustee&utm_name=partner | 791 EAST WALDORF AVENUE, MEMPHIS, TN 38106, Shelby County
https://www.zillow.com/homedetails/791-E-Waldorf-Ave-Memphis-TN-38106/42146822_zpid/ | 791 E Waldorf Ave, Memphis, TN 38106 | Zillow
https://www.zillow.com/homedetails/3117-Faxon-Ave-Memphis-TN-38112/42179332_zpid/ | 3117 Faxon Ave, Memphis, TN 38112 | Zillow


## job market
anecdotally bad

### healthcare
[IBD One](https://www.gastro1.com/services) started 2017

Healthcare includes University of Tennessee Health Science Center

Le Bonheur (Cerner Health) is main hospital system, plus Regional One (Cerner)
