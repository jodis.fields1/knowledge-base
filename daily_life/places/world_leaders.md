# World leaders

## Countries
http://en.wikipedia.org/wiki/List_of_countries_by_land_area

http://en.wikipedia.org/wiki/List_of_sovereign_states

https://en.wikipedia.org/wiki/List_of_proposed_state_mergers

http://en.wikipedia.org/wiki/Socialist_International - far more countries than I would expect

## Europe and Australia
Dark lands: the grim truth behind the 'Scandinavian miracle' (2014) - quite amusing overview

_Britain_ - British people among world's ugliest, according to BeautifulPeople.com

_EU_
The return of yesterday’s men: The European Commission presidency needs better candidates to regain the trust of the people (2014) - mentions some of the major figures, altho of course Germany's prime minister is super-powerful

_Austrialia_  - kinda freaky that Dr Chau Chak Wing of Kingogold donated so much money to both political parties there!

## Neighbors

_Canada_ - Liberal party has been right of New Democratic and left of Conservatives, made a power grab by forcing an election in 2011 and got decimated (Ignatieff the supposed "philosopher-king" was involved around then), but has risen way up again in 2013 after Justin Trudeau (young guy) became the leader altho next election is not until 2015 ( [the 42nd](http://en.wikipedia.org/wiki/42nd_Canadian_federal_election)); Stephen Harper has pissed a lot of people off; Tom Mulcair is the main guy for New Democrats (tho Jack Layton of Toronto was former and died). Chretien, Pierre Trudeau, and Michael Ignatieff are prominent people

Scraps most of its environmental staff per  Canada Chops Environmental Reviews, Fires Scientists, Responders; more background at How about we have strong environmental laws and job creation? at WCEL

## India

_Language and culture_ \- hundreds or thousands of languages; English not as common as I thought (5% fluent, 20% OK); Bhimrao Ramji Ambedkar was prominent "untouchable" (Dalit) and constitutional architect!

_Politics_

National Democratic Alliance - coalition which got Narendra Modi in 2014, pro-business

United Progressive Alliance - opposition party with faceman of Rahul Gandhi, was absolutely devasted in 2014

_Industry_

[India Decides Software is Not Enough](http://resourcecenter.businessweek.com/reviews/india-decides-software-is-not-enough) (2014) - electronics third import after oil and gold

See http://en.wikipedia.org/wiki/List_of_scandals_in_India for a good list

Also there was some aggressive population control measures, Bihar was the straggler; see population control note

_States_ - See  <http://en.wikipedia.org/wiki/List_of_Indian_states_by_GDP> for a good overview

Maharashtra is major

Uttar Pradesh - borders Nepal in northeast; very large (most populous with 1st in pop, 4th in area) but very poor

Gujarat - in northwest; arose as an economic powerhouse; looks like their governor Narenda Modi is riding that success to PM as of 2014

http://en.wikipedia.org/wiki/Family_planning_in_India is also interesting

## South America
Guyana - east of Venezuela, like Belize has a lot of English (official) altho 40% East Indian with a lot of Hinduism perhaps due to importing them after the end of slavery in late 1800s; unfortunately little tourism cause of bad beaches, corrupt govt, triple murder of US

## Asia
Japanese culture: help or hindrance for business growth? (2013) - long history of complaints about timidity and groupthink and this summarizes it OK

Read up on Oda Nobunaga who almost unified Japan in the 16th century

## Africa
See  <http://en.wikipedia.org/wiki/List_of_sovereign_states_and_dependent_territories_in_Africa> to get a good visualization

led by  <http://en.wikipedia.org/wiki/African_Union> and also  [ http://en.wikipedia.org/wiki/African_Economic_Community](http://en.wikipedia.org/wiki/African_Economic_Community "http://en.wikipedia.org/wiki/African_Economic_Community")

<http://en.wikipedia.org/wiki/Sahara_Desert> is obviously a big chunk; inhabited by <http://en.wikipedia.org/wiki/Berber_people> e.g. Tuareg

<http://en.wikipedia.org/wiki/Lake_Victoria> enormous lake; source of the Nile

Looked up Civil war to read about the Reconstruction but instead read up on how ex-colonies were created; notes how changes in norms prevent consolidation which might be theoretically necessary

Contrary to war how about merger see  http://en.wikipedia.org/wiki/East_African_Federation and Pan-Africanism

_Angola_

2013: read Forbes article Daddy's Girl: How An African 'Princess' Banked $3 Billion In A Country Living On $2 A Day about leader dos Santos family

_Nigeria_
Good Luck Jonathan seemed like a nice guy, but after the gasoline fiasco, not so great

_Mali_ - in 2012 Mali was having a civil war with Al Quaeda

_Cote d'Ivoire_
Roman Catholics and Muslims often spar; http://en.wikipedia.org/wiki/Laurent_Gbagbo (Christian) forcefully unseated in 2012 with support of international community - end of dictatorships in Africa?

_Morocco_ - should not be confused with Monaco! (on the French Riveria on the Mediterranean Coast); only non-member of the AU

_Uganda_ - on the Lake Victoria

http://en.wikipedia.org/wiki/Yoweri_Museveni has been around for a while; one of the http://en.wikipedia.org/wiki/New_generation_of_African_leaders which didn't work out quite so well

Uganda’s endless cycle of graft - interesting

_Sudan_

Dictator [ http://en.wikipedia.org/wiki/Omar_al-Bashir is the first sitting head to be indicted by ICC - woohoo

Long historical relationship with Egypt

_Zimbabwe_

http://en.wikipedia.org/wiki/Robert_Mugabe of the Zanu-PF party is particularly recognizable in Zimbabwe - seizes land, particularly of whites (colonials?); hyperinflation, etc; quote from Wikipedia: "When Zimbabwe gained independence, 46.5% of the country's arable land was owned by around 6,000 commercial farmers 70% of the best farming land"; 1979 first black PM altho only 4% white population;

## Dictators
http://en.wikipedia.org/wiki/Dictators

http://en.wikipedia.org/wiki/Islam_Karimov in Uzbekistan is somehow loved? or not, who knows

http://en.wikipedia.org/wiki/Alexander_Lukashenko in Belarus; sometimes called the list European dictator

## Middle East

Known for having relatively benevolent and stable dictators;

see http://en.wikipedia.org/wiki/Liberal_movements_within_Islam; note that many of the odder practices are based on hadith (stories); http://en.wikipedia.org/wiki/Quranism is loose; see [ http://en.wikipedia.org/wiki/Ghulam_Ahmed_Pervez for a description of Pakistan's flavor

Still unclear about difference between Sunni and Shia Islam; Iran was actually forcefully converted to Shia (who are only 10-20% of population); sounds like Iraq was originally a Shia stronghold from the http://en.wikipedia.org/wiki/Battle_of_Karbala

Al-Quaeda is typically considered Suuni; hilarious discussion of this at http://www.nytimes.com/2006/10/17/opinion/17stein.html?pagewanted=all&_r=0 Can You Tell a Sunni From a Shiite? with more detail at http://en.wikipedia.org/wiki/Shi%27a%E2%80%93Sunni_relations

### Countries

Quatar has a high GDP but also an absolute monarchy - http://en.wikipedia.org/wiki/Hamad_bin_Khalifa_Al_Thani is the top dog and seems to be relatively good

UAE - tops Quatar in the HDI and a fair bit bigger http://en.wikipedia.org/wiki/United_Arab_Emirates

Oman - somewhat famous for its leader http://en.wikipedia.org/wiki/Qaboos_bin_Said_al_Said who is cited as a benevolent dictator; unmarried