# United States overview
http://247wallst.com/2013/02/28/americas-happiest-and-most-miserable-states/5/ inspired this note and used Gallup-? for its data; Alaska is #31  

## Metrics
Just 54.3% of people indicated their supervisor treated them more like a partner than a boss, less than the 56.5% across the country indicated the same thing" - interesting metric; two of the six categories for well-being "work environment" and "access to basic necessities";

Republicans vote to abolish American Community Survey (ACS) - http://www.washingtoncitypaper.com/blogs/citydesk/2012/05/11/wonks-house-plan-to-get-rid-of-american-community-survey-absolutely-terrible/ - hope this doesn't follow through in Senate

## Future climate and demography
If I'm buying property, I should buy where people are going, right? What about climate change?

[Will climate change make Seattle the new L.A.?](http://today.ucla.edu/portal/ut/will-climate-change-make-seattle-170877.aspx) - heat could burn off the clouds

[Why do people move to Pheonix? (Phoenix, Tucson: to buy, school, wages)](http://www.city-data.com/forum/phoenix-area/521450-why-do-people-move-pheonix-2.html) - city-data thread and it comes down to balminess in winter months; found after  [Could Phoenix Soon Become Uninhabitable?](http://www.thenation.com/article/173346/could-phoenix-soon-become-uninhabitable)

### Migration and population growth
[The Great California Exodus: A Closer Look](http://www.manhattan-institute.org/html/cr_71.htm)  (2012) by Manhattan Institute - interesting; hopeful that this will change politics in other states, also see  [How California Is Turning The Rest Of The West Blue](http://www.npr.org/blogs/itsallpolitics/2013/08/29/216150644/how-california-is-turning-the-rest-of-the-west-blue) (2013) 

The biggest U.S. metro areas in 2025 - Bizjournals predictions include growth for N. Carolina

## States overview and list
http://en.wikipedia.org/wiki/List_of_U.S._states_and_territories_by_population is fascinating

http://www.pewstates.org/ is a good resource; I read their 2008 report; South Dakota, New Hampshire, New Jersey, and Arkansas received failing grades

See  [What is the Most Screwed Up Thing About Your State? Check This Chart](http://www.policymic.com/articles/64665/what-is-the-most-screwed-up-thing-about-your-state-check-this-chart)   

## Specific states
Mississipi - the worst of the worst, 50th in most categories; however, it does have about a 30% black population to make it a little more liberal   

Arkansas - like Mississipi, but with less of a cotton history (southeast was cotton) and only 15% blacks, so probably more conservative; Bill Clinton came from there   œ

Alabama - like Mississipi but slightly better

Georgia - original colony; powerful state with about 9m people; insurance division was headed by controversial http://en.wikipedia.org/wiki/John_Oxendine   

Virginia - fairly blue; after Georgia in population with about 8m people; very wealthy particularly near Fairfax County and DC; 20% black; 9% Hispanic;    

Maryland - a lot like Virginia and benefits from the federal government;

West Virginia - fairly blue but less so than Virginia; poll in 2005 showed that 53 percent of West Virginia voters are pro-life, the seventh highest; like AK, has no intermediate appeals court so  [ http://en.wikipedia.org/wiki/West_Virginia_Supreme_Court_of_Appeals](http://en.wikipedia.org/wiki/West_Virginia_Supreme_Court_of_Appeals) is very busy

Louisiana - originally French; headed by Bobby Jindal; lots of crawfish; seems a little less crazy but still crazy   

South Carolina - original colony; John Locke wrote the original constitution for  [ http://en.wikipedia.org/wiki/Province_of_Carolina](http://en.wikipedia.org/wiki/Province_of_Carolina "http://en.wikipedia.org/wiki/Province_of_Carolina"); first state to secede with Fort Sumter; as of 2012 lowest percent of women in legislature; 28% black; largest city is Columbia with 130k and Charleston similar; insurance commissioner appointed;

North Carolina - Research Triangle (including the Park) in 1950s was KEY and that area has since growth very quickly nearly 10m people and a purple state; much bigger than S. Carolina with nearly double population and 1.5 more area; 21% black and 9% Hispanic; Charlotte is largest with 750k followed by Raleigh with 415k; ranked high as a good state for business in 2012 (2nd or 3rd); insurance commissioner elected since 1908

Alaska - add http://www.alaskadispatch.com/article/20130508/economist-warns-alaskas-fiscal-crunch-mismanagement-and-apathy to performance review