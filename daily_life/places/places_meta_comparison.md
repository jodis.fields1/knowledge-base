note: lots of notes archived in Gmail

## rental communities

### REITs - Residential
https://finviz.com/screener.ashx?v=111&f=ind_reitresidential&o=-marketcap

https://seekingalpha.com/article/4285003-apartment-reits-roaring-rents

[Wall Street Private Equity Landlords Snapping Up Apartment Buildings](https://web.archive.org/web/20200509040411/https://ourfinancialsecurity.org/wp-content/uploads/2019/05/Private-Equity-Apartment-Bldng-Owners-4-19.pdf)
* 1 million private units, double publicly-traded

By market-cap...
* Equity Residential (EQR) - purely expensive big-city; one $2,300 studio in SF
* AvalonBay - lots but mostly big-city, nothing in SF or Oregon
  * from http://investors.avalonbay.com/CorporateProfile see http://investors.avalonbay.com/CustomPage/Index?KeyGenPage=202668
* Essex - exclusively in CA and Seattle?
* Mid-America - mostly South, but as far West as Phoenix, Las Vegas, and Denver
  * Denver location: https://www.maac.com/colorado/denver/post-river-north/ mentions EV charging
* Equity LifeStyle - properties across coast per https://www.equitylifestyleproperties.com/our-portfolio/mh-portfolio/ prolly focused on retirement communities
  * Oregon: Eugene, Fairview, Clackamas
* UDR - Portland, Austin and Nashville plus the usual suspects
  * hit them up about EV charging in Portland properties: Hunt Club - cheap parking ("unassigned parking for $8 and covered parking for $35") but no EV; Tualatin Heights - "Unassigned parking is $25. Garages are available at $120 per month for residents"; The Arbory - "assigned parking in our shared parking garage for $80. We also have assigned covered parking for $35 and unassigned covered parking for $10"
  * offers short-term leases
  * Nashville surprisingly expensive starting at around $830+
* Sun Communities - focused on elderly; not much in OR, NV, AZ but CO has stuff
* Camden Property Trust - Phoenix & Denver, los of CA, TX
* American Homes 4 Rent - single-family home place
* Apartment Investment and Management Company - CA, CO, Seattle, but no OR

Portland: see city_comparisons.md
* BroadStone Anthem - has EV charging

### remote
Tulsa - tulsaremote.com
Vermont - has some sort of incentive

### comparison
compare to other cities at https://teleport.org/cities/san-francisco-bay-area/

### charter cities
https://en.wikipedia.org/wiki/Charter_city

Singapore -
Hong Kong - 