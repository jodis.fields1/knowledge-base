# Eastern Europe and Asia

See World leaders

  


As of 2014 everyone is talking about Crimea. Background below.   


  


**Post Soviet**

Commonwealth of Independent States - Belarus, Turkmenistan, and Uzbekistan resist reform while Armena, Kazakhstan, Kyrgyzstan, Russia, Ukraine etc adopted reform per per 2006 Pugel international economics textbook

  


**Comecon (OECD Soviet equivalent)**

Austria - somewhat unique in Eastern Europe for a high GDP and advancement; Vienna is landmark city

Czech Republic - joined Soviets but rebelled in 1968 altho then its neighbors invaded per Warsaw Pact; Prague is its landmark city; on the way to development

Hungary - pretty weak really

  


**Pakistan**

Has a bunch of huge cities: Karachi, Lahore, and Islamabad

  


[The Most Dangerous Place: Pakistan’s Past, Pakistan’s Future](http://www.worldaffairsjournal.org/article/most-dangerous-place-pakistan%E2%80%99s-past-pakistan%E2%80%99s-future "http://www.worldaffairsjournal.org/article/most-dangerous-place-pakistan%E2%80%99s-past-pakistan%E2%80%99s-future") (2011) - interesting discussion of its founding and past; has a close connection to terrorism

  


[ Measuring extremism](http://tribune.com.pk/story/730798/measuring-extremism/ "http://tribune.com.pk/story/730798/measuring-extremism/") (2014) - points to  [Pew research Concerns about Islamic Extremism on the Rise in Middle East](http://www.pewglobal.org/2014/07/01/concerns-about-islamic-extremism-on-the-rise-in-middle-east/ "http://www.pewglobal.org/2014/07/01/concerns-about-islamic-extremism-on-the-rise-in-middle-east/") (2014) which shows most don't like extremism; however also see  [Pakistani Public Opinion Ever More Critical of U.S.](http://www.pewglobal.org/2012/06/27/pakistani-public-opinion-ever-more-critical-of-u-s/ "http://www.pewglobal.org/2012/06/27/pakistani-public-opinion-ever-more-critical-of-u-s/") (2012)

  


**Medieval Ottoman Empire**

Janissary - elite soldiers who were groomed and became politically potent
