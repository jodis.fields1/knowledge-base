

## Daily life
https://www.reddit.com/r/China/comments/d6gb4/china_redditors_what_are_the_best_websites_youve/

## Government  
National Reform and Development Corp is very powerful

### Politics  
2014-07: read In China,    Xi   Jinpings anticorruption drive snares a tiger (2014)   about going after Zhou Yongkang which mentions that behind   it is also Wang Qishan, head of the Central Commission   for Discipline Inspection (CCDI); also see    Xi   risks stability with attack on high-profile   ‘tiger’ (2014)
Run partly by "grey eminence" powers behind the throne; Eric   X. Li gave an intriguing TED talk
Xi Jinping constitutes special team to probe top leader -   notes decadal change in leadership and investigation into   PetroChina exec
One year on, Xi Jinping still has everyone guessing - notes   Standing COmmittee was weakened

## media
https://www.reddit.com/r/China/comments/ei2rt/what_chinese_media_do_you_listen_to_or_watch/
https://www.reddit.com/r/China/comments/e5eyl/where_do_you_guys_get_your_china_news_and_info/

## Language (Chinese)  
[2011 reddit thread on learning](https://www.reddit.com/r/China/comments/dzrkc/for_all_the_laowai_if_youre_learning_chinese_ive/) and [another thread](https://www.reddit.com/r/China/comments/di9br/i_am_an_english_teacher_at_henan_university_in/) recommends chinesepod.com
Not that many people speak English; read The statistics   of English in China: An analysis of the best available data from   government sources (2012) which puts fluency around 2-3%

## Geography  
Regionally, there's a traditional split and an economic split per [wiki list of regions](https://en.wikipedia.org/wiki/List_of_regions_of_China):

### Western China
Western China ([wiki](https://en.wikipedia.org/wiki/Western_China)) - 
    from west, Tibet, Sichuan, enormous   Xinjiang bordered to the southeast by large Qinghai (both weak   economies), which is bordered east by Gansu, which is in turn   bordered east by Shaanxi (which is west of Shanxi), bordered    part of the economic split Chengdu far west and Chongqing a   little east of that; in the southwest, Yunnan in the far west   borders Vietnam with Guizhou bordering it on the east

East China -  largest population; from southeast includes:   Taiwan (not truly relevant); Fujian (36m); north comes Zhejiang   (55m with Hangzhou); Jiangsu (79m with Nanjing and highest per   capita GDP)  and Shanghai; north to Shandong (95m with   Jinan); and west of Jiangsu to Anhui (55m with capital   Hefei)
 
North China (wiki) - Beijing, Tianjin, Hebei (which these two   were carved out of), Shanxi, and Inner Mongolia

Northeast China (wiki) - Liaoning, Jilin, and Heilongjiang - note there's a   bay called Yellow Sea with the Bohai Sea deeper which has some   offshore oil drilling    
 
South Central China - Hainan in the far south island was   first Special Economic Zone (?); Guangdong (104m so largest   province) is major powerhouse with Shenzhen and Guangzhou plus   nearby Macau and Hong Kong; Guangxi with weaker economy to the   west; north of these two is Hunan which has Dongting Lake and   several large 7+ million pop cities; further north is Hubei   (capital Wuhan) which is north of Lake Dongting; further north is Henan "birthplace of Chinese civilization" near the North   ChinaPlain and d in the center of a lot of action; 
 
Rivers: Yangtze River emptying in Shanghai which the   Three Gorges Dam was built on and Yellow River running in   the northern area; also there is also the Pearl River running   south (known as the Guangdong River) from wh ich flows   tributaries such as the Xi River; 