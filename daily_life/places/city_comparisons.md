# City comparisons
See places_meta_comparison.md

## general
"Atlanta is designated a market with Very High-Tech Labor Quality while also having only Moderate Tech Labor Costs. Similar markets are Denver, Raleigh-Durham, Portland and Chicago" per https://saportareport.com/housing-costs-barely-a-blip-for-atlantas-well-paid-20-something-it-pros-cbre-report/

[Philadelphia: Next Stop In America's Housing Crisis?](https://marketurbanismreport.com/philadelphia-housing-crisis/) (2018) - ??

[Houston Or Portland: Which City Is Doing Urban Density Better?](https://marketurbanismreport.com/houston-or-portland-which-city-is-doing-urban-density-better/) (2018) - Houston is surprisingly dense and urban

[U.S. Cities Have Drastically Different Urban Talent Sheds](https://marketurbanismreport.com/u-s-cities-drastically-different-urban-talent-sheds/)

[Atlanta's Housing Market: Still Tough For The Working Class](https://marketurbanismreport.com/atlantas-housing-market-tough-working-class/) (2018) - ??

## murder rates
https://www.thetrace.org/2018/04/highest-murder-rates-us-cities-list/
* Austin, TX is high

# City comparison resources
* https://www.niche.com
* https://www.reddit.com/r/financialindependence/comments/6gg3x3/where_do_you_live/

https://www.expatistan.com/cost-of-living | Expatistan :: International Cost of Living comparisons
https://www.numbeo.com/cost-of-living/ | Cost of Living
https://www.numbeo.com/property-investment/rankings_current.jsp | Current Property Prices Index by City
https://www.zillow.com/rent-vs-buy-calculator/ | Rent vs Buy Calculator | Zillow


## Dating and singles outlook
[A singles map of the United States of America](http://www.boston.com/bostonglobe/ideas/articles/2008/03/30/a_singles_map_of_the_united_states_of_america/?page=full) (Boston Globe) - noticed this on foreveralone

## Sites for climate
https://weatherspark.com/y/2142/Average-Weather-in-Boise-Idaho-United-States-Year-Round - best place

City-data - good but it doesn't have summary stats, just graphs

http://www.bestplaces.net/climate - lots of stats; two different spots which aren't linked: (1) compare climates w/climate profile and (2) the regular one which is more summarized

Weather underground - doesn't really have it

## Comparing cities - as of 2014

OK, per bestplaces.net San Fran has 259 days of sunshine versus 154 in Portland - pretty big difference. Ketchikan has 100, Juneau has 85

Cost of living - San Fran 199, Portland 116

**Engineering communities** \- competition to Silicon Valley
Chattoonaga, TN - https://mobile.twitter.com/AustenAllred/status/1026895622845022208?s=19 - says $120k

### Boulder / Denver
Boulder / Denver, CO - read [https://blog.g2crowd.com/blog/technology-research/the-state-of-b2b-tech-denver-2018/](https://blog.g2crowd.com/blog/technology-research/the-state-of-b2b-tech-denver-2018/) and the wiki articles; Boulder seeme expensive and small, tons of govt research institutes,

### Portland
https://en.wikipedia.org/wiki/Charbonneau,_Oregon

Portland Area - has  [Silicon Forest](http://en.wikipedia.org/wiki/Silicon_Forest) w/ 17,000 Intel employees; also see  [Why this founder left the Bay Area for Portland](http://www.geekwire.com/2013/founder-leaving-bay-area-portland/) (2013) which mentions Oregon overtaking Washington for venture funding first since 1993, much much cheaper per above

[The Best Cities For Tech Jobs](http://www.forbes.com/sites/joelkotkin/2012/05/17/the-best-cities-for-tech-jobs/) (2012) - profiles the fast-growing communities, w/ Seattle at top and Portland oddly not mentioned; Salt Lake was no. 4; 

[Move over Silicon Valley, here comes Silicon Forest](http://www.usatoday.com/story/news/nation/2013/10/24/top-cities-for-start-ups/3181641/) (2013) - says top are Los Angeles, Seattle, New York, Boston and Chicago


### Austin
Austin - a small handful of IPOs are discussed by Hurt's  [The state of tech entrepreneurship in Austin](http://lucky7.io/post/the-state-of-tech-entrepreneurship-in-austin)