Parenting child education
===

## Toys

Spielgaben - World Best Educational Toy

[Haynes Build Your Own Internal Combustion Engine](http://www.amazon.com/dp/B006H4JEQO/ 'http://www.amazon.com/dp/B006H4JEQO/') -

I might like this more than the kid!

## Pregnancy
Exercise improves child's brain per 2013 reddit thread research by Ellemberg and Curnier at Montreal

## Teenagers
Teen Night Owls likely to perform work academically - 2013 study from UC Berkeley

## Gifted children  
### Have not examined

<http://news.bbc.co.uk/2/hi/uk_news/education/7107798.stm>\- start children reading later? look up this woman's research

<http://www.readingrockets.org/article/356/>and <http://www.bookclubuniverse.com/Small-Children.php>

### Avoid websites

http://giftedkids.about.com/gi/pages/poll.htm?linkback=http://giftedkids.about.com/b/a/7356.htm&poll_id=9161307718 - unimpressive website

## Growth
Probiotics increased growth see  [Augustina et al 2013](http://jn.nutrition.org/content/143/7/1184.short?rss=1 'http://jn.nutrition.org/content/143/7/1184.short?rss=1') about Probiotics Lactobacillus reuteri DSM 17938 and Lactobacillus

Boys with a Simple Delayed Puberty Reach Their Target Height (2008) by Cools BLM et al

Skeletal growth and peak bone strength (2008) The Scientifically Proven Approach to Nutrition (nd) by Jeffrey R. Stout - from Peakhealth.com

Predicting human body height from DNA (2013) - Human Genetics article about Scandinavian tallness genetics

[Idiopathic Short Stature: A Clinical Review](http://jama.jamanetwork.com/article.aspx?articleID=1866124 'http://jama.jamanetwork.com/article.aspx?articleID=1866124') (2014) - says about 2 inches on average gained from growth hormone

## Childhood education

See <http://en.wikipedia.org/wiki/Common_Core_State_Standards_Initiative>and <http://www.corestandards.org/>funded by Bill gates - good tohear about standardizing US education; Alaska objections noted at <http://news.heartland.org/newspaper-article/2010/03/25/alaska-texas-reject-common-core-standards>altho Anchorage is implementing them <http://www.ktva.com/home/outbound-xml-feeds/ASD-Implements-Common-Core-Standards-Right-Away-144316925.html>

### Teachers and unions
[The Rubber Room](http://www.newyorker.com/reporting/2009/08/31/090831fa_fact_brill 'http://www.newyorker.com/reporting/2009/08/31/090831fa_fact_brill') (2009) - eye-opening story emphasizing the problem of unions by Steven Brill who has written a lot on the topic since; see  [Can Teachers Alone Overcome Poverty? Steven Brill Thinks So](http://www.thenation.com/article/162695/can-teachers-alone-overcome-poverty-steven-brill-thinks-so 'http://www.thenation.com/article/162695/can-teachers-alone-overcome-poverty-steven-brill-thinks-so') (2011)

### Standardized testing
Alaska is moving to make 50% of a teacher's evaluation based on test scores and I heard about Finland's high international scores, which prompted research:

great overview at <http://standardizedtests.procon.org/>

National Assessment of Educational Progress (NAEP) - national assessments since 1965 or so; got down tothe district level recently

Programme for International Student Assessment (PISA) - every 3 years international exam; Finland is famous for scoring high, much higher than neighbors; Trends in International Mathematics and Science Study - 8th grade equivalent tothe PISA

Racial discrimination argument -for schools tobe "in good standing in Virginia only 45% of black students in each school must pass standardized math tests while 68% of whites, and 82% of Asians must do the same" per [http://ideas.time.com/2012/10/11/why-its-time- to-get-rid-of-standardized-tests/#ixzz2N6cbxHM0](http://ideas.time.com/2012/10/11/why-its-time-to-get-rid-of-standardized-tests/#ixzz2N6cbxHM0 'http://ideas.time.com/2012/10/11/why-its-time-to-get-rid-of-standardized-tests/#ixzz2N6cbxHM0')

Some complaints about Alice Rivlin as a founder

### Federal law

The Elementary and Secondary Education Act of 1965 (see  [CRS report R4316](http://www.fas.org/sgp/crs/misc/R43146.pdf 'http://www.fas.org/sgp/crs/misc/R43146.pdf')) is the main law (also HIgher Education Act that same year), amended by the No Child Left Behind Act; the ARRA also passed the Race to the Top Act (R2T) for \$4.4b; while not a law, Common Core is a private and state initiative aided by R2T

History is summarized at ed.gov's  [The Federal Role in Education](http://www2.ed.gov/about/overview/fed/role.html 'http://www2.ed.gov/about/overview/fed/role.html') which mentions first DoEd in 1867; Morrill Acts of 1890 (land-grant colleges); Lanham Act of 1941  [provided daycare per source](http://american-education.org/1172-lanham-community-facilities-act.html 'http://american-education.org/1172-lanham-community-facilities-act.html') (Community Facilities Act of 1941, not other Lanham Act);

[Tossing out No Child Left Behind, Alaska educators adapt to new standards](http://www.alaskadispatch.com/article/20130816/tossing-out-no-child-left-behind-alaska-educators-adapt-new-standards 'http://www.alaskadispatch.com/article/20130816/tossing-out-no-child-left-behind-alaska-educators-adapt-new-standards') (2013) - Alaska has a special tweak on it

## Potty-training

See Research study: Whistle away the need for diapers and Vietnamese Mothers´ Experiences with Potty Training Procedure for Children from Birth to2 Years of Age was published in Journal of Pediatric Urology

Head Start 2012 research project http://www.acf.hhs.gov/programs/opre/research/project/head-start-impact-study-and-follow-up-2000-2012 per http://www.americanthinker.com/2012/02/head_start_can_a_failed_program_ever_be_killed.html
