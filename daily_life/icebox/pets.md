---
title: Pets
access: private
---

Pet and pets so it shows up on a search!

## Dogs
[A woman lost her hands and legs to an infection from puppy kisses](https://www.cnn.com/2019/08/02/health/amputation-dog-lick-ohio-woman/index.html)

For one, tail wagging isn't always happy  [per reddit](http://www.reddit.com/r/science/comments/1pnduz/tail_wagging_means_a_lot_more_to_a_dog_than_im/): if the tail wags to your left, the dog is anxious and maybe unhappy

## Bears
So I remember reading about some sort of Big Ben, but it turns out that was just fiction. However, Brutus ( [ wiki](https://en.wikipedia.org/wiki/Brutus_%28bear%29 "https://en.wikipedia.org/wiki/Brutus_%28bear%29")), adopted by Casey Anderson, is a pet bear!

## smart pets esp ravens 2018-08
['Intelligent' crows to pick up litter at French theme park](https://www.expatica.com/fr/news/country-news/France-tourism-birds-offbeat_2031808.html) - let's get this in SF

possibly a bit motivated by a raven familiar named Erlang in DnD

https://en.wikipedia.org/wiki/Bird_intelligence
https://en.wikipedia.org/wiki/Paraves | Paraves - Wikipedia
https://en.wikipedia.org/wiki/Avialae | Avialae - Wikipedia
http://www.penguinthemagpie.com/ | Penguin the Magpie – Penguin the Magpie
https://www.quora.com/Can-you-have-a-Magpie-as-a-pet | Can you have a Magpie as a pet? - Quora
https://www.youtube.com/watch?v=AYunv_fzFL4 | Gizmo Tells Mrs Eagle to Bug Off - YouTube
https://www.youtube.com/watch?v=dwXe_bzVZ0k | A Visit with Freedom - A rainy day in Arlington - YouTube
https://www.youtube.com/watch?v=3e5u9gX1fWM | After 15 Years Of Friendship, This Tiger And Bear Just Said A Final Farewell To Their Lion Brother - YouTube
https://www.youtube.com/watch?v=0fiAoqwsc9g | Crows, smarter than you think | John Marzluff | TEDxRainier - YouTube
https://www.youtube.com/watch?v=lhWbo135Efc | Try Not To Laugh Challenge - Funny bird videos awesome compilation 2017 - YouTube
https://www.youtube.com/watch?v=ozgcKw4MyvY | Einstein vs Griffin | The World's Smartest Parrots | Extraordinary Animals | Earth - YouTube
https://www.pbs.org/newshour/science/which-are-smarter-cats-or-dogs-we-asked-a-scientist | Which are smarter, cats or dogs? We asked a scientist | PBS NewsHour
https://www.washingtonpost.com/news/animalia/wp/2017/04/03/shocker-some-cats-like-people-more-than-food-or-toys/?utm_term=.e7ca344884cd | Shocker: Some cats like people more than food or toys - The Washington Post
