this applies to personal and business, so does not really belong here, maybe daily life

See also sectors and industries and transportation under govt in investing spreadsheet

### groups
Sustainable Packaging Coalition advocates for sustainable packaging

## moving
https://ride.guru/content/newsroom/uber-for-moving

https://www.moveline.com/moving-long-distance

### convenience - new
* put everything in ziplock bags for cohesion
  
winner:
* https://remoovit.com/ | Remoov | Goodbye clutter. The easiest way to sell, donate and recycle your unwanted items in the San Francisco Bay Area

Maybe: 
* https://www.1800gotjunk.com/us_en | Junk Removal | 1-800-GOT-JUNK? USA

Other:
* http://www.primepackandship.com/home/ebay-sales/ | eBay Sales - Prime Pack & Ship
* https://www.shipsmart.com/packing-shipping?cpid=1&gclid=EAIaIQobChMI4uzn-96W4QIVEh-tBh1MjgyNEAMYAiAAEgKUPfD_BwE | Packing and Shipping Solutions - Ship Smart
* 


### convenience - old
Shyp alternatives:
* ShipBob?

Doorman / last-mile alternatives:
* Amazon Key - best case TODO: hit up landlady about getting gate keycode
* Deliv, Bringg, etc
What happened to Doorman?
* alternatives - https://www.producthunt.com/alternatives/doorman-2-0
* Kapil Israni CTO / cofounder; Zander Adell CEO / cofounder moved on to Garden - followed on twitter
* Adell talks about his personal CRM (MonicaHQ competitor) at https://www.producthunt.com/posts/garden
* https://www.affinity.co/ is mentioned but its more professionally focused
* engineers include  Kevin Nguy, David Singer (https://twitter.com/ramaboo)
Shyp and Doorman (delivery) make for a good combination
uship for large items
Shyp cut down to SF Bay in 2017
Doorman shut down in 2017 :(

### moving
uship.com - the best for professional moving
BungoBoxes - you can rent boxes (??), saw on TV somewhere, lots of competitors

U-Haul and Budget control 70% of the DIY moving market and were cited for possible collusion per http://www.ftc.gov/opa/2010/06/uhaul.shtml; Penske is probably close behind (not in Juneau) and Hertz (which recently bought Dollar Thrifty) might be in the running too; Enterprise seems to be out; United Rentals is a separate publicly-traded companies

## packaging
### packaging industry
Styrofoam (polystyrne) - $20 billion polystyrene market - disgusts me been hearing about Ecovative Design's fungi-based solution for years. Read Wired's "Ecovative Design: wiping out polystyrene with fungus and farm waste" (2012); apparently partnered with Sealed Air (NYSE:SEE - inventor of bubble wrap in 1950s and notable provider of Amazon packaging) in June 2012 http://www.ecovativedesign.com/uncategorized/press-release-sealed-air-and-ecovative-complete-agreement-to-accelerate-commercialization-of-new-sustainable-packaging-material/ but prior to that biggest customers were Steelcase (NYSE:SCS - major office furniture company - note their Details brand has some adjustable furniture including my adjustable desk at work) and Dell.


#### mushroom packaging innovation?
2017 update: Ecovative is firing people and focusing on bioengineering, seems like their solution wasn't economical / scalable
been hearing about Ecovative Design's fungi-based solution for years. Read Wired's "Ecovative Design: wiping out polystyrene with fungus and farm waste" (2012); apparently partnered with Sealed Air (NYSE:SEE - inventor of bubble wrap in 1950s and notable provider of Amazon packaging) in June 2012 http://www.ecovativedesign.com/uncategorized/press-release-sealed-air-and-ecovative-complete-agreement-to-accelerate-commercialization-of-new-sustainable-packaging-material/ but prior to that biggest customers were Steelcase (NYSE:SCS - major office furniture company - note their Details brand has some adjustable furniture including my adjustable desk at work) and Dell.

As a result of the deal Ecovative is opening a new 25k sq ft Green Island plant and plans to build 2 more to supply Sealed Air

Alternatives include Claudio Bastros CBPAK http://www.cbpak.com.br/

Incumbent competitors (losers) include probably Dow Chemical, Owens Corning (former VP Jerry Weinstein is on Ecovative's board), and other chemical companies... - note that Owens Corning-> Owens-Illinois | Corning; known for its asbestos liability (interestingly, Dow Corning created GreenEarth Cleaning for drycleaning

Bemis Company and Sonoco Products compete with Sealed Air; Bemis won several awards http://www.bemis.com/news/prod/article/id/344/


Groups

Sustainable Packaging Coalition advocates for sustainable packaging

### packaging mechanics
#### adhesives
2014-08: on a chemistry bender, got back into adhesives after thinking about alcohol and acetone solvents (both are polar and nonpolar solvents!)
Packing tape always smells really bad and toxic; http://www.ehow.com/facts_6864271_chemicals-used-packing-tape.html discusses the chemicals in them; http://theboard.byu.edu/questions/15986/ remarks on it as does http://www.answerbag.com/q_view/2117153; Scotch SureStart http://www.buzzillions.com/reviews/scotch-sure-start-shipping-tape-clear-1-2-core-2x25-yds-6-rolls-pack-reviews#readReviews has reviews which say it doesn't have that stink (official page at http://www.scotchbrand.com/wps/portal/3M/en_US/ScotchBrand/Scotch/Products/ProductCatalog/?PC_7_RJH9U52300LM30I87QR3ES18H7000000_nid=3Q9VLGK4S5gsV9FWH5FFP6glFTXPM5VVWDbl); http://www.ecopackstore.com/products/products/eco-friendly-cellulose-tape-2.html is another alternative

Google Books is the only place that I found much discussion on the toxicity of adhesives; see e.g. Adhesive Bonding: Materials, Applications and Technology (2009) for a long overview; http://www.treehugger.com/clean-technology/biodegradable-packaging-tape-developed.html discusses Bio-Flex 219F a biodegradable packing tape


