# Home ins

## new
https://www.lemonade.com/policy-two - https://github.com/lemonade-hq/policy-2.0

## old
See http://www.homeinsurance.org/ home-insurance-guide/ for a website focused on homeinsurance but it seems wacky, for example "foundations are not typically excluded from coverage on a standard homeinsurance policy"

Best resport I could find: Buying Homeowners Insurance—The Full Story (From CHECKBOOK, Spring/Summer 2011) - emailed 

Kipliner's ran a special report: see http://www.kiplinger.com/fronts/special-report/home-insurance/

How to get your homeowners insurance claim paid: Find out which insurance company to choose (2014) - Consumers Report overview

Watched the Florida 2013 working group - I liked what Reginald Garcia had to say at the end about regulator courage. Also the  [Florida homeownersrights](https://web.archive.org/web/20130810145527/http://www.myfloridacfo.com/ica/docs/ConsumerRightsPropertyInsurance.pdf) is an interesting document.  [Florida Lawmakers Pass Homeowners’ Rights Bill With Credit Restriction](http://www.insurancejournal.com/news/southeast/2014/05/01/328063.htm) (2014) notes that SB 708 was passed.

Renter's negligence in fire

From CPCU530, the insurer will typically waive its subrogation interest, lessee is an additional insured, or lessee has an additional policy protecting against liability or separate fire policy

Allocating Tenant Tort Liability Through the Fire Insurance Policy (1958)

Landord/Tenant Subrogation - 50-state survey http://www.mwl-law.com/CM/Resources/Landlord_Tenant%20Subrogation%20in%20All%2050%20States.pdf

Tenant as Implied Coinsured on Landlordâs Fire Policy - Exoneration from Negligence or Rational Â Public Policy? - by Neylon, discusses AK at length http://www.neylonlaw.com/tenants-coinsured/

## Resources and websites
see insurance orgs

http://www.insuranceclaimhelp.org/

http://www.uclaim.com/

http://www.adjustersinternational.com/AdjustingToday/

RoughNotes

## Insurers

See Top 30 Homeowners Insurers Q2 2013: SNL Financial at insurancejournal for list

Also see Aon Benfield: U.S. Homeowners Insurance Offers Growth Opportunities

State Farm on top

Allstate second

Safeco probably third

Farmer's in top

Fireman's Fund might be around fourth

Country Mutual is in the running

American Family is big and bought Homesite sometime

[ANE, Hyundai Marine & Fire Launch New Homeowners Insurance Program](http://www.insurancejournal.com/news/national/2014/05/08/328658.htm) (2014) - novel coverages provided 


## Fire Protection and Fire Departments
http://blogs.courant.com/connecticut_insurance/2010/10/ homeowners-are-less-satisfied.html - brief summary

http://www.firehouse.com/forums/t33115/ - firefighter discussion

http://my.firefighternation.com/group/isodepartmentratingsandevaluations/forum/topics/iso-is-not-what-it-used-to-be is part of a group focused on this

Insurers, Government Grapple with Costs of Growth in Wildland-Urban Interface - lots of homes being built in wildfire areas

Keep a fire extinguisher around - small one would be MAX Professional 7102 Fire Gone Portable Extinguisher, ABC Rated, FG-007-102 (16 oz)

### Insurance

The NYTimes article mentioned below profiles Daniel Schwarcz. He was consumer representative that Sarah mentioned a while back who discussed how the insurance policies werenât available to consumers prior to purchase and how policies have numerous differences which agents arenât aware of and companies do not like to disclose. His full report is available here (and attached). I do think heâs onto something and I hope that there is progress on making these variations more obvious. He points out that Texas is the only area where the regulators have provided a way to compare policies (see http://www.opic.state.tx.us/hoic.php - generates a table with the provisions) and thatâs driven by a quasi-regulator called the âOffice of Public Insurance Counsel which advocates for consumers to the Texas Division. I actually think varying contract conditions will be a good thing eventually. Heâs pretty critical of SERFF, which I hope spurs reform of that terribly unstructured system

I read through some of it and there are some things that sound familiar, although Schwarcz is not naming any companies. For example, the NYTimes article mentions that some companies have added âsudden and accidental to the direct physical loss of the policy insuring agreement. Fidelity National has that. After first withdrawing their file & use form because of an incomplete answer to why they had this âsudden and accidental language in 80789, I authorized it in 81525. I discussed it with Joanne and felt hesitant about doing so, but ultimately I guess I thought it shouldnât change coverage much â in retrospect I donât think it should have been authorized.

I don't agree with Schwarcz on everything. He highlights the addition of obvious and visible marks of breakdown as a condition for theft coverage as a bad condition which was rejected by courts when it was applied to commercial policies. I have seen this in renters policies. He also says an exclusion like mysterious disappearance of an items is bad. I've seen these in renters policies and I dont think they are bad. These conditions significantly reduce the potential for fraud. Theft is not the most important coverage in a homeownerspolicy, and insuring small losses is generally not a great deal for consumers. With the addition of fraud, itâs even worse for consumers. Itâs also notable that Alaska has less authority than many states to disapprove language like this. Our state says deceptive or misleading to the risk many other states have additional against public policy language in their similar statutes.

I don't think there's nearly as much variation in Alaska. Schwarcz noted that many states could not even produce complete policies for the top ten writers in their states. Iâve found the major ones. State Farm has a fairly traditional policy which hasnât changed since the 1990s. Iâve looked at Allstate, Country Mutual, and Safecos policies recently and none of them seemed to have many differences from ISO's HO-3, but I'll look again to make sure. I know there seems to be more variation in rentersâ insurance policies. Mike helped me review what seemed to be a restrictive and likely deceptive policy from National Casualty, but it was withdrawn. The company was coming from the surplus lines, however (I believe they were showing a fair amount of Alaska surplus lines business too).

## Home Insurance Monograph
* By Ben Creasy, created 2011-12-05

### Direct-to-consumer
Only companies seem to be Homesite (bought by American Family), Electric Insurance, and perhaps Western Mutual

Agencies: http://www.troyinsurancegroup.com/homeowner-and-auto-insurance-in-texas/homeowner-insurance/ brags that it can provide Foundation Coverage and âContinual Seepage and Leakage Coverage; as far as foundation coverage, in 2012 we did get Praetorian taking over Balboa's Mortgage CatastropheProgram and renaming it the HomeownersProtection Program; form Co Tr Num was PL_11-7275 and rates PL_11-7276 but in WA is 12-0365-WA-HO-QBEF-RR; it seems a bit different in WA though

Website coverage:
* http://money.cnn.com/magazines/moneymag/money101/lesson19/index.htm
* http://uphelp.org/news/learn-seven-things- homeowners-should-know-about-their- homeowners-insurance-policies/2012-12-11
* http://uphelp.org/news/ homeowners-can-challenge-insurers-estimates/2012-12-09
* Multiple Challenges Make Expanding Private Coverage Difficult GAO-14-179 - kinda basic
* Replacement cost calculators

See separate replacement cost note

AccuCoverage - from Marshall/Swift Boeck; cost $8 per run; probably controversial from underpricing California homes

Xactimate 27 - from Verisk analytics

Valued policies - available in 19 states per Fires reignite debate over valued policies in Colorado Fires reignite debate over valued policies in Colorado - The Denver Post altho in Cali it requires an appraisal

Alaska-specific coded in green.

Guarantees - http://www.deadbeef.com/does_guaranteed_replacement_cost_even_ex/ discusses the lack of guarantees, which I noticed as well; note that repairs are also not guaranteed unless the insurer selects the contractor

Price trend - PPI's Residential Construction index might be the closest fit

### Losses associated with each peril

According to Allstateâs filing 82630 (Co Tr Num R21849), fire 58.5%, liability 4.6%, wind/cat 9.5%; water 20.5%; all other 4.1%.CPCU 520 textbook seems to say that similar information in NAICâs statistical handbook.

Page 37 of Allstateâs 2011 annual report shows that catastropheis about 8.5% of the losses; 86% of catastrophes < $50 million and about 86% caused by wind/hail. Not a lot of discussion about this except that âAllstate is subject to assessments from assigned risk plans, reinsurance facilities and joint underwriting associations providing insurance for wind related property lossesâ. In 2011, Allstate got hit by a lot of tornado losses (7 events causing 32% of cat losses, or $1.23 billion.

AK #68137 shows the State Farm's loss ratio attributed to collapse 2002-2006 in Alaska.

Hail losses have been big at about $1b across US; in 2012 Insurance Institute for Business & HomeSafety (IBHS) did a test with artificial hail with Chester Co.;Â Number of Hail Damage Insurance Claims Up 84% Since 2010 (2013) in insurancejournal says around 500k claims/year withÂ top five states generating hail damage claims were Texas (320,823), Missouri (138,857), Kansas (126,490), Colorado (118,118) and Oklahoma (114,168)

### ISO
#### Section I (Property)
Fire causes most of the losses. To understand electricalfires, see: Identifying Causes for Certain Types of Electrically Initiated Fires in Residential Circuits (article by Eaton Corp member John J. Shea)

Per Is it safe to leave your crockpot on all day while you are out? crockpots and toasters can sometimes cause fires

#### Coverage A (Dwelling)

Per page 5.18 on CPCU540, ice damming is covered as it is not excluded, at least insofar as it causes water to back up thru shingles; we also saw American Bankers trying to exclude collapse coverage related to ice damming

#### Perils Insured Against

This section notably contains mainly perils which are not insured against starting at Section I â Perils Insured Against A.2.(c). See Fleming v. USAA: The Tale of the Coverage Grant That Ate the Exclusions for discussion on this oddness or http://www.iso.com/Newsletters/International/A-Capitol-Effort-Maintaining-Insurance-Policy-Forms-and-Endorsements-When-the-Laws-Change.html and the OR legislature changed the per http://www.leg.state.or.us/comm/sms/sms01/sb0440ahbca03-20-2001.pdf to let ISO get away with it; Oregon Law firm covered it http://www.jordanramis.com/articles/article0074.html; when I asked about this in Fidelity National's AKHOFO2011(state number is 95612) they appeared knowledgeable of it

Although the Special Provisions â Alaska (HO 01 54 09 11) shows that this section is changed, it is modified only slightly. The eight exclusions in A.2.c.(6) are broken out, meaning the section ends at A.2.c.(13). The difference is in A.2.c.(11), which adds caused by resulting from human or animal forces or any act of natureâ.

The 'Ensuing Loss' Clause in Insurance Policies: The Forgotten and Misunderstood Antidote to Anti-Concurrent Causation Exclusions (2012) - I don't think this language is as common as he says

Wildfires- see https://en.wikipedia.org/wiki/List_of_wildfires

Particularly significant is 1991 Oakland Hills firestorm and 2003 Cedar Fire in San Diego

In 2012-2013 Colorado experienced a few leading to a major ins reform in 2013

Wondered whether it was global warming, but invasive species may be a major cause perÂ Invasive plant species increase wildfire risk (2010) such as cheatgrass (Invasive Cheatgrass Triggers Frequent Wildfires in Great Basin). Looked atÂ Wildland fire in ecosystems:Â fire and nonnative invasive plants but that discusses invasive species coming in after fires

#### Pipes bursting

A Reasonable Investigation of a Water Loss Requires Using Tools to Find the Damage (2014) 
* use infrared!

"Failure to close foundation vents is probably the single greatest cause of frozen pipes" - from[CBJ winterizing tips](http://www.juneau.org/water/winter.php)

Farmers HomeMut. Ins. Co. v. Fiscus, 725 P. 2d 234 - Nev: Supreme Court 1986: water damage from pipes bursting destroyed personal property; Farmers denied based on âseepage or leakage of water a over a period of time but this was contradicted by the personal property Coverage C section which said âwe insure for direct loss to the Personal property described in Coverage Property C caused by: . . . . 13. Accidental discharge or overflow of water or steam from within a plumbing, heating or air conditioning system or from within a household applianceâ; trial court found for insured and appeals court affirmed; bad faith action.

Finn v. Continental Ins. Co., 218 Cal. App. 3d 69 - Cal: Court of Appeal, 1st Appellate Dist., 5th Div. 1990: proximate cause rule and coverage for sudden break in pipe does not invalidate the exclusion for water damage which occurs over a long period of time; affirms summary judgment for insurer.

http://www.propertyinsurancecoveragelaw.com/2009/06/articles/insurance/is-the-state-farm-policy-really-worth-anything/ has a discussion on the water damage question

Moisture Mapper International is a vendor to control moisture damage

#### Earth movement

Earthquake typically not required by a lender; see[Earthquake Risk Shakes Mortgage Industry](http://www.freddiemac.com/finance/smm/may96/pdfs/quake.pdf) (1996)

See [ Are Damages Caused By Blasting or Other Man-Made Earth Movement Covered Under Your Insurance Policy?](https://web.archive.org/web/20180206081207/https://www.propertyinsurancecoveragelaw.com/2013/04/articles/insurance/are-damages-caused-by-blasting-or-other-manmade-earth-movement-covered-under-your-insurance-policy/) which refers to FC&S for a good overview.

Lead-in clause excluding all events makes a big difference (see[2009 discussion from Chip](https://web.archive.org/web/20200426041605/https://www.propertyinsurancecoveragelaw.com/2009/06/articles/state-farm/the-dirty-secret-of-exclusions-some-major-insurance-companies-like-state-farm-allstate-nationwide-and-even-usaa-do-not-want-you-to-think-about/) or West v. Umialik in AK)

Landslides: USGS Landslides program researches at $3.5m/year or so

Sinkhole issues are a big deal as aquifers are drained; in Florida coverage is required; in 2013 a big 60-foot sinkholeopened up in Florida's disneyland ;Â Q &A: Why Sinkholes Form in Florida mentions that there is a sinkhole alley: "made up of porous carbonate rocks such as limestone that store and help move groundwater ... over time, these rocks can dissolve from an acid created from oxygen in water"

#### Water and foundation
West v. Umialik Ins. Co., 8 P. 3d 1135 - Alaska: Supreme Court 2000: burst plumbing lead to a portion of the house settling; appeals court reverses summary judgment for insurer to summary judgment for insured as settling is grouped with wear and tear, implying that it is not a result of some other major cause; note that in Alaska, ISO (Umialikâs filer) revised their form to split out these exclusions and adds more force to the settling exclusion (see above).

Chase v. State Farm Fire and Cas. Co., 780 A. 2d 1123 - DC: Court of Appeals 2001: another case of the âwater damaging foundationâ circumstance; noted additional coverageâ extension of $10,000 for restabilizing land; refer and adopt reasonable expectations doctrine.

Alf v. State Farm Fire and Cas. Co., 850 P. 2d 1272 - Utah: Supreme Court 1993: Alfs had burst pipes which caused foundation damage; excluded based on earth movement; trial court found for insurer and appeals court affirmed; Utah explicitly opts out of âreasonable expectations doctrine.

Cantanucci v. Reliance Ins. Co., 43 AD 2d 622 - NY: Appellate Div., 3rd Dept. 1973: similarly, the water damaged the foundation; in this case, the water originated from a burst sewer line; since the exclusion did not specify whether the water was natural or artificial, it is assumed natural as otherwise would contradict coverage for pipes bursting; judgment reversed to favor the insured.

See http://uphelp.org/news/consumerwatch-allstate-denies-sf- homeowners%E2%80%99-claims-water-main-break/2013-03-07 for Allstate denying settling of homes due to a water main break

#### Latent defect
Chadwick v. Fire Ins. Exchange, 17 Cal. App. 4th 1112 - Cal: Court of Appeal, 1st Appellate Dist., 3rd Div. 1993: Very complex ruling based on âlatent defectâ in wall construction and whether or not that is dependent on whether the defect was obvious or not; court remanded back in favor of insured.

Yellow brass pipe fittings, which have more zinc, can deteriorate more rapidly; historically polybutylene water lines (blue pipe) are bad

#### Court Cases
Deskbook Encyclopedia of Insurance Law - probably the best overview I've taken a look at

#### Section II (Liability)****
Insurance Contracts and Judicial Discord
* following "Insurance Contracts and Judicial Discord" leads to a bunch of empirical contract law cases

http://digitalcommons.wcl.american.edu/cgi/viewcontent.cgi?article=1396 &context=aulr discusses declaratory judgments and coverage for immoral and intentionalacts and mentions rules such as the complaint-potentiality rule of Iowa and Pennsylvania versus Arizona, Alaska, Indiana, and Nebraska with the complaint-analysis of facts rule, and 13 states instruct to review the policy rather than the complaint and there's also a "fairly debatable" rule plus there are various rules on when to start defending and then they can do so under reservation of rights (or waiver, but not easy to get that signature), other controversies include mixed-claims; related to the Uniform Declaratory Judgments Act of 1922; also the Federal Declaratory Judgments Act of 1934

Wisconsin Supreme Court Narrows Definition of Occurrence (2013) - a modern version of this where someone hosted a party where someone was assaulted, contested decision found no coverage

AP article State Farm pays $109 million for dog bite claims discusses the dog-bite liability http://www.usatoday.com/news/nation/story/2012-05-17/dog-bite-insurance-claims/55037444/1 - note that per http://dogbitelaw.com/criminal-penalties-for-dog-bites/criminal-penalties-for-a-dog-bite.html people can also go to jail; http://dogbitevictim.wordpress.com/2010/12/18/adopting-out-a-known-dangerous-dog-should-be-a-crime/#comments has someone logging

See AK filing 93830 (AAIS-2012-53LC) for estimates on dog bite frequency as well as trampolines and wood stoves

###### Off-premises
Coverage has been offered in plenty of cases for example, Maine Mut. Fire v. Amer. Int'l Underwriters, 677 A. 2d 1073 - Me: Supreme Judicial Court 1996 where a man left his dog outside and it bit a child

###### Motor vehicle exclusion

Generally speaking, the motor vehicle exclusion applies. See for example Allstate Ins. Co. v. Keillor, 450 Mich. 412 - Mich: Supreme Court 1995. This can also be applied due to âconcurrent causationâ as per Newton v. Nicholas, 20 Kan. App. 2d 335 - Kan: Court of Appeals 1995, where a water tank fell off a truck and homeownersinsurance did not apply.**

###### Criminal and/or intentional acts exclusion
Home Liability Coverage: Does The Criminal Acts Exclusion Work Where The "expected Or Intended" Exclusion Failed? (1999) - article (available in OneNote) by Country Mutual and State Farm counsel about new exclusion

Note that in California, the first-party innocent coinsured exclusion is removed per not lining up with standard fire policy.

In NJ, apparently they've reformed/removed this requirement per[Innocent Coinsured Doctrine in New Jersey](http://www.propertyinsurancecoveragelaw.com/2014/05/articles/insurance/innocent-coinsured-doctrine-in-new-jersey/?utm_source=feedburner&utm_medium=feed&utm_campaign=Feed%3A+propertyinsurancecoveragelaw%2FYZft+%28Property+Insurance+Coverage+Law+Blog%29)


CP ex rel. ML v. Allstate Ins. Co., 996 P. 2d 1216 - Alaska: Supreme Court 2000: adult son of insureds sexually assaulted a child; plaintiff sued parents for negligence; Allstate denied based on criminal and also intentionalacts exclusion; suit went to United States District Court for the District of Alaska, who asked Alaska Supreme Court to answer questions on state law; court found coverage for the insureds after complex reasoning.

Applies particularly to sexual abuse (Cuervo v. Cincinnati Ins. Co., 76 Ohio St. 3d 41 - Ohio: Supreme Court 1996; Teti v. Huron Ins. Co., 914 F. Supp. 1132 - Dist. Court, ED Pennsylvania 1996)

Allstate Ins. Co. v. Roelfs, 698 F. Supp. 815 - Dist. Court, D. Alaska 1987: minor son of parents sexually molested two young girls while they were staying at the home; summary judgment denying coverage upheld by judge on the basis of the criminal acts exclusion (referenced in Eidsmoe & Edwards 1998).

Cary v. Allstate Ins. Co., 922 P. 2d 1335 - Wash: Supreme Court 1996: appeal of judgment for insurer denying coverage for criminal act committed by a homeownerswhile insane; affirmed.

Allstate Ins. Co. v. Burrough, 120 F. 3d 834 - Court of Appeals, 8th Circuit 1997: Arkansas case which precisely addresses the question of whether or not the âregardless of whether such insured person is actually charged with, or convicted of, a crimeâ language commonly used in umbrella  policies holds up when the insured defendant commits a crime but is not charged or convicted; court affirms judgment in favor of the insurer.

Allstate Ins. Co. v. McCarn, 683 NW 2d 656 - Mich: Supreme Court 2004: cited by the Burrough case above, it reverses to cover for accidental discharge of a presumed unloaded gun; has an interesting discussion on the public policy of insurance and how it benefits its victims.

######  Alaska Statutes
Prohibited denial of claim for causation (AS 21.36.096): passed in SB177 of 21st Legislature; Senate Letter of Intent (see 04-29-2000 Senate Journal 3596) notes that this was to reverse State Farm Fire and Cas. Co. v. Bongen, 925 P. 2d 1042 - Alaska: Supreme Court 1996.

###### Water exclusion
ISO described its water exclusion revision which changed it to reinforce that water damage is excluded regardless of whether it is caused by an act of nature or not in 75056.

###### Irrelevant
Brownstein v. Travelers Cos., 235 AD 2d 811 - NY: Appellate Div., 3rd Dept. 1997: Guy didnt get the policy he wanted. Boo-hoo.


###### Underwriting guidelines
In 08-38 (Alaska complaint number) Allstate non-renewed due to wildfire bushes 200 feet from dwelling.

ALLSTATE IS NON RENEWING POLICY BECAUSE OF WILDFIRE DANAGER. COMPANY RESPONSE EXPECTED ON 02/24/2008/ALLSTATE INSURANCE COMPANY RESPONDED THAT THEIR UNDERWRITING GUIDELINES REQUIRE BRUSH TO BE REMOVED 200 FEET FROM YOUR DWELLING. Â EVEN THOUGH YOUR PROPERTY DOES NOT EXTEND 200 FEET IN EACH DIRECTION, THEY WERE UNABLE TO CONTINUE COVERAGE.Al