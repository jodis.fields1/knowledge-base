# Ins orgs (associations, research)

## Consumer

* Consumer Federation of America
* UnitedPolicyholders - perhaps more focused on insurance than CFA; see  [United Policyholders has been with us through three disasters: Lean on me](http://uphelp.org/news/united-policyholders-has-been-us-through-three-disasters-lean-me/2014-05-07) which has a bio on ED Amy Bach
* [Policyholders of Florida](http://www.policyholdersofflorida.com/) - FL alternative
* Insurance Advocate http://www.insurance-advocate.com - seems to have pretty good articles
* Consumer Action http://www.consumer-action.org/about/ - don't know much about them
* Center for Insurance Research - new organization; half-alive; Brendan Bridgeland founded?
* Center for Economic Justice - outdated since 1999 but actually Birnbaum their founder is still active
* ConsumerWatchDog - California focused; founder Rosenfield is a major force there and wrote Proposition 103

### Blogs and personalities

* Insurance Law Hawaii - noticed interesting mold case  [Exclusion Bars Coverage for Mold, Fungus](http://www.insurancelawhawaii.com/insurance_law_hawaii/2012/10/exclusion-bars-coverage-for-mold-fungus.html) (2012) 
* [Property Insurance Coverage Law Blog](http://www.propertyinsurancecoveragelaw.com/) - Merlin Group and leading commentator
* Insurance Coverage Law Blog - run by David Rossmiller at Dunn Carney in Portland, OR
* Insurance Policyholder Advocate - run by Jones Day
* [Insurance Class Actions Insider](http://www.propertyinsurancecoveragelaw.com/2013/11/articles/insurance/insurance-policy-tearout-provision-may-require-insurance-carrier-to-pay-for-costs-to-repair-plumbing/) - run by Robinson & Cole LLP
* [Zalma on Insurance](http://zalma.com/blog/) - California fraud and ins expert 
* [California Insurance Litigation Blog](http://www.californiainsurancelitigation.com/) - noticed homeowners coverage
* Jack Hungelman - good consumer advocate and personal umbrella expert

## Legal

See law firms note under law

## Research

FORC - Federation of Regulatory Counsel

Connecticut Insurance Law Journal http://insurancejournal.org/ - perhaps the best insurance law journal out there

NAIC - Journal of Insurance Regulation; new library at http://naic.softlinkliberty.net/liberty/libraryHome.do

Insurance library association of Boston - note there's a library at Wharton too

R Street - free market association on insurance - not sure how much industry funding; try to encourage them to keep an open mind and think about increasing disclosure of how much money consumers lose when they buy insurance.

III - Insurance Information Institute

General Reinsurance http://www.genre.com/page/0,2964,ref%253DPublicationSeries-en,00.html - publishes a dozen or two columns; Policy Wording Matters caught my eye

## Professional
* AICPCU - runs the CPCU designation
* CAS/SOA - actuarial designations
* RIMS - risk management
* IRM - Institute of Risk Management; may be a bit fringe
* SOFE - Society of Financial Examiners

## Trade associations

SeaBright has a long list at http://www.sbic.com/about/tradeassoc.html; long list at http://www.ita.doc.gov/td/finance/8.html as well

PCI - Property Casualty Insurers Association of America (created by 2004 merger); larger than the two others probably at about 40% of P&C market; more of a stock focus and more active than AIA; lots of member-restricted content; runs ISS; publishes Executive Compensation Survey and Insurance Industry Survey; however if you go the Media Center and PCI Speaks or PCI At Work, you can find stuff for example http://www.pciaa.net/LegTrack/web/NAIIPublications.nsf/lookupwebcontent/D2734B5818DDD8D9862579880068EED?opendocument state priorities in 2012

Professional Liability Underwriting Society - has a D&O association

Inland Marine Underwriters Association (IMUA) - ?

NAMIC - has stock companies as nonvoting members; biased in favor of state regulation; more small organizations; seems a little more active

AIA - seems relatively inactive but has the largest insurers; has only 300-400 members; recently participated in the founding of GFIA the Global Federation of Insurance Associations http://www.gfiainsurance.org/

GFIA - no website up at http://www.gfiainsurance.org/ yet

ACLI - American Council of Life Insurers

AHIP - health side

## Advisory/statistical
* ISO
* AAIS
* NCCI
* SFAA
* ISS - ? subsidiary of PCI

## News

Insurance Journal - http://www.insurancejournal.com perhaps most notable

Adjusting International http://www.adjustersinternational.com/AdjustingToday/

IRMI - best website by far

Rough Notes magazine

National Underwriter