# Ins regulatory agencies

## Federal
Looked through TREAS-DO-2011-0010 at http://www.regulations.gov/#!docketDetail;rpp=250;po=0;D=TREAS-DO-2011-0010; nothing too outstanding altho TREAS-DO-2011-0010-0129 (and TREAS-DO-2011-0010-0117) which was the consumer reps under Kochenburger's name was interesting; NAIC comment just summarized history and current practice; Ohio Dept raised a weak defense of the state system; American Consumer Institute at TREAS-DO-2011-0010-0040 sounded like an astroturfing org;

## Dodd-Frank

2010 Dodd-Frank per Milliman  [The Dodd-Frank Act and the insurance industry](http://us.milliman.com/insight/insurance/The-Dodd-Frank-Act-and-the-insurance-industry-Strategic-considerations-of-U_S_-financial-reform/)

Surplus lines: focuses around state of insured for premium taxes; reduces declinations requirement for exempt commercial purchasers but declinations may still be necessary.  [GAO study](http://www.gao.gov/products/GAO-14-136) shows overview of surplus lines; eligibility criteria based around domicile state of surplus lines insurer

Some reinsurance impact thru reinsurance credit for ceding being recognized easier

## NAIC and national comparison
Julie Fritz - head honcho on the IT program

NAIC shuts down transparency, governance debate at national meeting - exclusive fight between progressive commissioners and the old guard

Transparency and Readability of Consumer Information (C) Working Group

Listed at http://www.naic.org/store_model_laws.htm

Personal Lines Property and Casualty Insurance Policy Simplification Model Regulation (1993) - does not speak on my pet peeve about duplicate and contradictory language

Kansas regs listed at https://www.ksinsurance.org/legal/regs_list.htm

## Reform proposals
Prior Approval of Auto Rates Does Not Benefit Consumers: Study (2013)

Also see NCOIL study from a bit back

## Specific states
Nebraska (NE) - publishes a newsletter; see http://www.doi.ne.gov/newslttr/letter.htm - note that this is headed by Bruce Ramge an agency and regulatory veteran; appointed by Dave Heineman a super-Republican

North Carolina (NC) - As of March 2012, has a decent website and even a newsletter advertising all the savings. Run by Wayne Goodwin, who is elected. However, website changes may happen too often. Excerpt from NC170-M75:
> It is the Department’s practice to cite companies in apparent violation of a statute or rule when the results of a sample show errors/noncompliance at or above the following levels: 0 percent for consumer complaints, sales and advertising, producers who were not appointed and/or licensed and the use of forms and rates/rules that were neither filed with nor approved by the Department; 7 percent for claims; and 10 percent for all other areas reviewed. When errors are detected in a sample, but the error rate is below the applicable threshold for citing an apparent violation, the Department issues a reminder to the company.  
>    
>  Consumer savings  
>  Per the Winter 2012 Eye on DOI Newsletter:Department of Insurance saved or recovered more than $42.2 million for consumers last year. This is a big accomplishment and took the work of several divisions. Consumer Services recovered more than $16.4 million in benefits from insurance companies. Health Insurance Smart NC and Health Care Review returned $1.1 million. SHIIP, the Seniors’ Health Insurance Information Program, saved an estimated $12 million forMedicare beneficiaries. Our Criminal Investigations Division posted $10.4 million in restitution and recoveries. Agent Services recovered more than $50,000 for consumers. Market Regulation ordered $2.1 million returned to policyholders. I am impressed by the work that my staff has done to help foster a thriving and well-regulated insurance marketplace in North Carolina. 
* Building code - Department has taken a particular interest in the building code; commissioner also serves as fire marshal

Massachusetts (MA) - reports at http://www.mass.gov/ocabr/business/insurance/insurance-companies/market-conduct-exam-reports/ however no reports past 2009

Hartford, Connecticut - reputation as the old headquarters for the industry per http://www.propertycasualty360.com/2012/02/21/connecticut-seeks-to-recapture-role-as-insurance-c

Louisiana - has a reputation for being difficult according to filers

California (CA) - 
* Proposition 103 made it commissioner elected; election to replace Steve Poizner, who tried to become governor but lost in the Republican primary to eBay’s Meg Whitman. Republican Assemblyman Mike Villines is facing off against Democratic Assemblyman Dave Jones to replace Poizner; in 2012 their FCPR Fair Claims Practices Act got basically struck down; in their ratemaking, they require excessive exec compensation and political lobbying expenses to be netted out; also includes leverage ratios from A.M. Best for each line (ranging from .2 for Products to 1.9 for Allied LInes)
* noticed that Jerome Tu is an analyst who has been around for a while and seems to have some influence
* 2014-03: noticed Geff Greenfield is a rate analyst who's been around a while, also Rachel Hemphill has a PhD in math and is casualty actuary for them
* 2014-05: reviewed their homeowners checklist and code and noticed that they protect innocent coinsureds for arson, apparently based on substantial equivalence to standard fire policy per Century-National Insurance Co. v. Jesus Garcia, et al., Case No. S179252 (see t [his coverage](http://www.sdma.com/california-supreme-court-holds-that-innocent-insured-is-not-subject-to-intentional-act-or-criminal-conduct-exclusion-in-fire-insurance-policy-02-17-2011/)

Georgia - "John Oxendine, first elected Georgia insurance commissioner in 1994; veteran regulator tried to move up the political ladder by running for the GOP nomination for governor but fell off on the first rung in the primary. His successor as regulator will be either Republican Ralph Hudgens, Democrat Mary Squires or Libertarian Shane Bruce".

Florida - has specific return on equity files; as originally noticed in FORC ("FLORIDA LEGISATIVE SESSION TO BE REMEMBERED FOR WHAT PASSED... AND WHAT DIDN'T") they significantly rolled back regulation in commercial lines with SB 468 ([Fast-track coverage changes for commercial property insurers to continue](http://www.thefloridacurrent.com/article.cfm?id=33207581) due partly to backlog altho this started with Order 130176-12 which also extends to personal lines; looks like rate filings may have been reduced as well; SB1770 in 2013 reforming Citizens has also drawn a lot of attention

Maryland - Maryland Insurance Administration Issues First-Ever Civil Fraud Orders (2013) discusses new authority, two individuals sued

New Hampshire - reading a ppt by Wallenius it said that under RSA 417:19 if the agency finds no violation, no lawsuit can be filed (a gatekeeper law) which I'd never heard of, found Agencies as Litigation Gatekeepers (2013) but no discussion of insurance gatekeeper law, 

North Dakota - Adam Hamm was President of the NAIC in 2014

Pennsylvania - commercial lines highly deregulated due to 1995 Bulletin Vol 25, Number 35; does not seem to have market conduct reports;

Arizona - commercial lines not required to be filed since date ??

Illinois - commercial lines highly deregulated

Iowa - listened to Nick Gerhart who seemed intelligent but comes from lobbying for annuity companies on SEC regulation

Indiana - Robertson is a badass per 2014 E-Reg

Virgina (VG) - commissioner is appointed by an independent commission

Washington (WA) - elections; see http://en.wikipedia.org/wiki/Washington_Referendum_67 which allows triple damages for bad faith; not capped by settlement agreement per Miller v. Kenny where Safeco ended up with a $21m bad faith lawsuit (ouch!)

Oklahoma (OK) - elections

Oregon (OR) - passed a major new environmental pollution law called SB814 in 2013

Kansas (KS) - I think there are elections; per P&C division honored for review milestone (2012) they are heavily-focused on average review period rather than quality; met Martin Hazen thru mortgage ins (see note); said new P&C director (Jim Newins?) was interested in PMI profit margins

Utah - listened to Todd E. Kiser who said agents should be the best they can be like everyone (duh)

## Public filings databases
WA https://fortress.wa.gov/oic/onlinefilingsearch/

CA http://insurance.ca.gov/0250-insurers/0800-rate-filings/0050-viewing-room/

CT  <https://filingaccess.serff.com/sfa/home/CT>

FL http://www.floir.com/edms/

AR http://www.insurance.arkansas.gov/PandC/Filings.htm

SC https://online.doi.sc.gov/Eng/Public/Queries/FlngSrch.aspx

NY https://myportal.dfs.ny.gov/web/guest-applications/serff-property-foil

NV http://doi.nv.gov/scs/Homeowners.aspx

MO adopted SERFF Filing Acess early in 2014

ME adopted SERFF Filing Access 2014

### Private
SNL Financial - has the NAIC contract I believe

http://fyionly.net/ - Quadrant

https://www.ratefilings.com/ - Perr&Knight

### Related databases

http://www.opic.state.tx.us/policy-comparisons/homeowners-2 - OPIC homeowners comparison tool

### SERFF

See  [iRate from Arkansas](http://www.afmc.org/iRATE.aspx "http://www.afmc.org/iRATE.aspx") for an advanced way to handle rate filings

See Board Minutes for main information

SERFF Liberation: The System for Electronic Rate and Form Filing Needs Competition By Eli Lehrer

### General notes

Lots if information is available from the NAIC's Insurance Department Resources Report (IDRR) at http://www.naic.org/documents/members_state_commissioners_elected_appointed.pdf

Heartland Institute used to do annual reports; now this is done by the R Street (rstreet.org)

## Elections versus appointments

According to Klein's 2008 overview an Overview of the Insurance Industry and Its Regulation, 12 commissioners/superintendents/directors are elected while the balance are appointed - however http://www.insurancejournal.com/news/national/2010/11/01/114513.htm says 11.