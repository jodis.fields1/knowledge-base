# Surety and fidelity

See http://www.jwsuretybonds.com/blog/page/107

Civil Court Bonds - http://www.thebarplan.com/courtbonds/court-bond-type/civil-court-bond/ \- this company even puts its  [commission online](http://www.thebarplan.com/courtbonds/court-bond-type/producer-information/)

http://www.thefreelibrary.com/The+unsure+world+of+ surety+bonds.+%28Cover+Story%29.-a086060306 The unsure world of suretybonds - role in the financial 

## Contractors and license bonds
Source of recovery - but also see the right to repair laws passed in several states; not to be confused with the auto Motor Vehicle Owners’ Right to Repair Act of 2011

## Industry
Excellent overview at http://business.highbeam.com/industry-reports/finance/ surety-insurance

See the 2012 McConnell State of Suretyreport by Vertex for a good overview; attached.

Amwest SuretyInsurance Company went bankrupt around 2001 - notably a pure-play per http://www.thefreelibrary.com/Amwest+Insurance+Group+Inc.+Announces+Bankruptcy+Filing.-a076739832 not sure what happened

## Federal law
See Title 31, Subtitle VI, Ch. 93 for some suretylaws

## Rating
Note that "per M" is often used - meaning per $1,000

Credit & SuretyPricing and the Effects of Financial Market Convergence www.casact.org/pubs/forum/03wforum/03wf139.pdf - good 2003 article

Is notary E&O suretyor fidelity? In Alaska fidelity is a violation of public trust; see http://www.streetdirectory.com/travel_guide/13951/legal_matters/why_do_i_need_a_notary_bond.html for argument that notary bond covers violation of public trust

In 1993 William & Mary Law Journal had an issue on suretylaw (see Volume 34 | Issue 4), particularly given the Restatement (Third) of Suretyship coming out then. Articles include:

History and Background of the Restatement of Suretyship - 36 pages; skimmed; best part is 1017 (30) where it discusses how outdated suretylaw is and the lack of statutes (California being an exception)

Symposium Introduction: The Restatement of Suretyship - 5 pages; had an interesting note about anomalies; for example statute of limitations for contracts longer than torts - but what's the suretyanomaly?

 Reconsidering Consideration in the Restatement (Third) of Suretyship - 34 pages; 1st page notes that three-way transactions are more complex, next few pages noting the importance of contract law and, surprisingly for me, consideration (which is noted as disfavored in modern contract law); page 5 has a criticism of of the Restatement; skimmed the rest - very obscure

 Suretyship on the Fringe: Suretyship by Operation of Law and by Analogy - 39 pages; skimmed 4 pages - seemed pretty basic

Miscellaneous SuretyBonds and the Restatement - 30 pages; more relevant to the work at the time (reviewing filing 90088); notes court bonds, license & permit, sometimes public official bonds, custom, trustee, lost instrument, market agency, livestock, and packers.

 The Construction Contract Suretyand Some Suretyship Defenses - 25 pages; skimmed

Striking the Balance: The Evolving Nature of Suretyship Defenses - ?

## Other sources
An Economic Analysis of the Guaranty Contract (1999) by Katz - 70 pages; skimmed first 10 or so: basically asks "why guaranty" and says that sureties are more specialized in monitoring or something, otherwise they would just do loans themselves; my first impression is that he's wrong and that banks want to diversify their credit risk - risk of default of the debtor and creditor is much lower if they are independent; maybe he discusses that later on

Fidelity - similar to suretyin that the insurer should get some restitution from the employee

See http://www.fidelitylaw.org/index.php/fidelity-law-journal for a lot of good information; this potentially gets deep into the area of criminal financial fraud - see white collar crime note