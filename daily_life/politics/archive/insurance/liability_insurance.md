# Liability insurance

Travelers organizes their liability around LIA-3001 Ed. 01-09 - all the others (in Alaska filing 79190) piggyback off this.

Risk retention groups really affect this, but the group members should be in a similar industry I guess

Generally need to analyze the commercial contract in addition to the insurance contract per Survey Says: The Feud Over Insurance and Indemnity Provisions in Bus iness Contracts Indemnity and Insurance Provisions in Commercial Contracts http://www.americanbar.org/content/dam/aba/administrative/litigation/materials/2013_insurance_coveragelitigationcommittee/b_13_indemnity_insurance_provisions.authcheckdam.pdf;

## Lawsuits in general

On the decline I believe, and found  [FACTS ABOUT DECLINING CIVIL LITIGATION IN THE UNITED STATES](http://news.lawreader.com/2007/01/10/facts-about-declining-civil-litigation-in-the-united-states/) (2007) which somewhat confirms; in Hawaii as of 2012 they have only around a dozen trials per  [Civil lawsuits decline as fewer cases go to trial](http://www.bizjournals.com/pacific/print-edition/2012/02/17/civil-lawsuits-decline-as-fewer-cases.html?page=all) (2012)

On the other hand the TRAC  [Civil Filings for February 2014](http://trac.syr.edu/tracreports/bulletins/overall/monthlymar14/cfil/) shows pretty even trend with personal injury product liability top and up 21% from five years ago

WebRecon is a business that helps identify litigants particularly wrt to Fair Debt Collection Practices Act (trend seems to be somewhat down per  [FDCPA Lawsuits Decline, While FCRA and TCPA Filings Increase](http://www.acainternational.org/news-fdcpa-lawsuits-decline-while-fcra-and-tcpa-filings-increase-31303.aspx) (2014))

## Key coverage language

Claims-made versus occurrence - the difference is that you can stop paying for insurance and still be covered with occurrence

An insured versus the insured - watch out for severalibility and voiding

Moral versus immoral and criminal conduct - see OneNote for details, but there is also Rice's Insurance Contracts and Judicial Discord over Whether Liability Insurers Must Defend Insureds' Allegedly Intentional and Immoral Conduct: A Historical and Empirical Review of Federal and State Courts' Declaratory Judgments--1900-1997 (pdf)

## Laws

See direct-action statute, covered in How States Should Resolve Conflicts Problems Under the Direct Action Statute-An Approach (1955) which notes that it overcomes the interspousal exclusion and may also encourage juries to award greater damages; historically in Louisiana and also Wisconsin but more recently in Alabama, Arkansas, Minnesota, New York, Pennsylvania; literature since then includes Direct-Action Statutes by Mark Mese in the CGL Reporter by IRMI

## General liability

Based on a post at NW PolicyHolders (" ["Bresee" Decision Applied by Oregon Federal Courts](http://nwpolicyholder.blogspot.com/2013/07/we-recently-obtained-victory-for-one-of.html)) where coverage was negated by owned property and products-completed exclusions, I looked at  [Common Liability Exclusions | The Good, The Bad and The Ugly](http://www.thompsoncoe.com/NewsPublications/Publications?find=26675) which was an excellent overview of the issues

Protecting a General Contractor from Liability Caused by a Subcontractor (2008) - ABA overview by Andrew Reidy and Seth Lamden. Also note that in AK filing #86414 (F) (with 86434 rule companion) an exclusion was revised to address this exposure.

[Georgia Supreme Court Expands Coverage for Faulty Construction](http://www.insurancejournal.com/news/southeast/2013/08/12/301434.htm) - this is sweeping the country, in this case affecting a lot of homeowners and caused by a high water-to-cement ratio in foundation

### Dramshop
basically liquor liability laws, see  [Center on Alcohol Marketing and Youth (CAMY) 2013 report](http://www.camy.org/action/commercial-host-liability/). Alaska case example is KALENKA v. JADON INC II which involved a fatal stabbing at Chilkoot Charlie's, summary judgment for bar denied

ISO GL April 2013: changes so that insurance is provided 'only to the extent permitted by law', which excludes coverage when there is an anti-indeminification statute in some cases, as noted in CLIENT ADVISORY Additional Insured – ISO Changes by AmWINS

## Professional liability

See http://en.wikipedia.org/wiki/Professional_liability_insurance

APLS is the big player in legal malpractice

Medical malpracticeis a really big deal; there's some caps on noneconomic damage

_Medical malpractice_

The Tort of MedicalMalpractice: Is It Time for Law Reform in North Carolina? - http://www.johnlocke.org/acrobat/policyReports/tort_of_medical_malpractice.pdf shows the huge difference in premiums between ob/gyn + surgeons and family practice

Richard Epstein, LLB University of Chicago, MedicalMalpractice: The Case for Contract (1976) - advocates using contract law

Newman-Toker D, et al  "25-Year summary of US malpracticeclaims for diagnostic errors 1986-2010: an analysis from the National Practitioner Data Bank" - diagnostic errors are the most costly error; read about it at http://www.medpagetoday.com/HospitalBasedMedicine/GeneralHospitalPractice/38620;

In Alaska, judges may appoint an expert panel; Miche Supp. 1997 allows the State to be sued; prejudgment interest at "legal rate" which was 10.5% at one point per  [1998 summary by McCullough Campbell & Lane](http://www.mcandl.com/alaska.html) which has a  [summary on every state](http://www.mcandl.com/states.html); 

In Pennsylvania, New Legislation Allows Doctors to Apologize \- 2013 PA allows doctors to apologize without it being used against them

_D &O_
see also governance->boards for more info

Grilled Travelers in 2012-06-0078 when they raised rates 10% or so with no losses for private D&O - private D&O is apparently much different from public D&O, but I'm not so sure I believe them now that I'm reading more stuff; read the D&O report by Andreini http://www.andreini.com/blog/2011/06/06/why-do-privately-held-firms-purchase-directors-officers-liability-coverage/ which is an excellent overview sourced to Liability of Corporate Officers and Directors by Knepper and Bailey; two good blogs are http://www.dandoeandomonitor.com/ and http://www.dandodiary.com/; also FDIC is now posting settlements on D&O litigation; http://www.muchshelist.com/knowledge-center/article/devil-details-part-i-assessing-legal-consequences-terms-commonly-found-mana has some details on coverage terms

## Product liability insurance

### Databases?
Doesn't seem to be any

2009, lots of lawsuits per http://www.bloomberg.com/apps/news?pid=newsarchive&sid=awwb48rn7sQQ \- note that Supreme Court allowed more drug lawsuits per a case

_Notable cases_

http://www.castlerockagency.com/business-insurance-news/products-liability-insurance/6-largest-products-liability-insurance-suits.html
* phen-fen
* _Montrose Chemical Corporation_ had a somewhat disproportionate impact in terms of stacking policy periods over time

### Players

CPSC - Consumer Product Safety Commission; has a database on products at http://www.saferproducts.gov/Search/default.aspx

ATRA - American Tort Reform Association http://www.atra.org/

Public Citizen

Product Safety Letter - http://www.productsafetyletter.com/default.aspx for executives

Drug Recall Lawyer Blog - http://www.drugrecalllawyerblog.com/2010/01/product_liability_statistics_t.html has some stats:

2005 state courts, there were 346 product liability trials, 28 of which were for drugs or medicaldevices. Excluding asbestos cases, plaintiffs won 19.6% of product liability trials in 2005. Also interesting, is that in 2005, 10.2% of all tort cases in Philadelphia were product cases (this is a trend that likely continues, in large part because of the hormone therapy cases). The only other county with a higher percentage was San Francisco, at 12.7%.

_Law firms_

Saiontz & Kirk, P.A. per http://www.youhavealawyer.com/product-liability/

_Outdated_

http://www.verdictslaska.com/hnplr.html - last report is from 2006