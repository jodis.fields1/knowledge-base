# Ins claims

## Diminished value
also diminution of value (see  [wiki](https://en.wikipedia.org/wiki/Diminution_in_value))

Diminished Value-What it is and How to Make an Insurance Claim for a Cash Insurance Settlement - can't really get it in first-party but probably in third-party; also see  [Diminished Value: The biggest secret in the insurance industry](http://www.autoloss.com/Services/Online-Auto-Appraisals/Diminished-Value-The-biggest-secret-in-the-insurance-industry.html#.Uwpth0Ma4Q0) at autoloss.com

[Diminished value car insurance claims get the wrecking ball](http://www.insure.com/car-insurance/diminished-car-value.html) - insure.com reviews court cases for third-party; says these claims are near impossible

## Arbitration - see dedicated note

Nationwide Inter-Company Arbitration program from 1944 helps the industry settle issues, now handled by Arbitration Forums, Inc.

## Time limits on filing claims
 see also ins contract language

Estes principle applies in Alaska - insurer must show prejudice per 1989 Estes case, which is based on 1984 Weaver Bros. case

In Iowa, noticed in CPCU book Insurance Contract Analysis that Bruns v. Hartford (1987) applies where insureds had to show carrier not prejudiced! weird

_Principles of Risk Management and Insurance_ 1981 second edition summarizes on p250-1 how various states handle prejudice

[Ninth Circuit Certifies Notice-Prejudice Question to Montana Supremes](http://nwpolicyholder.blogspot.com/2014/07/ninth-circuit-certifies-notice.html?m=1) (2014) - keeps cropping up

## Medical examinations 
kind of an issue which came up with Brandon's claim, seems homeowners policies typically have a provision saying that the insurer can select the physician to do the examination

## Bad faith

[Fifty Percent Chance Insurer Will Fight Its Policyholder - Why?](https://web.archive.org/web/20160318131019/http://www.propertyinsurancecoveragelaw.com/2014/05/articles/insurance/fifty-percent-chance-insurer-will-fight-its-policyholder-why/) - per Consumer Reports; good discussion

Bad faith tort: survey of Northeast states available at http://www.chartwelllaw.com/links_articles/documents/SurveyofBadFaithInsuranceLaw.NortheastUS.pdf; 

https://web.archive.org/web/20111206185323/https://www.policyholdersofamerica.org/
* https://web.archive.org/web/20120119152116/http://www.policyholdersofamerica.org/does_your_state_punish_bad_faith.pdf

Does the Threat of Insurer Liability for “Bad Faith” Affect Insurance Settlements? by Danial Asmat and Sharon Tennyson (2014) - "presence of tort liability for insurer bad faith increases settlement amounts and reduces the likelihood that a claim is underpaid"

The Appalling State of the Law Involving First Party Bad Faith in New York - blog post

## Post-claim underwriting (rescission, concealment)

* Generally if it's not on the application, very limited in rescinding per Concealment in Applying for Insurance: Overview of Insurer's Rights and Insured's Responsibilities which notes "In assessing whether or not a fact is material, a key indicator is whether the insurer thought the information was significant enough to ask specific questions regarding it at the time of underwriting. See, Maryland Casualty Co. v. National American Insurance Co. of California, 48 Cal.App.4th 1822 (1996); Farmers Automobile Inter-Insurance Exchange v. Calkins, 39 Cal.App.2d 390 (1940) [“the failure to inquire into [a] subject indicates an entire lack of interest in it”]; Reserve Insurance Co. v. Apps, 85 Cal.App.3d 228 (1978) See, Reserve Insurance Co. v. Apps, 85 Cal.App.3d at 231; Ashley v. American Mutual Liability Insurance Co., 167 F.Supp. 124, 130 (N.D. Cal. 1958) [“failure to inquire into a subject, with specificity, is held to indicate an entire lack of interest in it.”]; E.A. Boyd Co. v. U.S. Fidelity and Guaranty Co., 35 Cal.App.2d 171, 181-182 (1939)"

The Doctrine of Concealment: a Remnant in the Law of Insurance - really old article noting the difference betwen marine and non-marine in America, where fraudulent intent or badfaithis required

## Nonrenewal for number

See http://www.pissedconsumer.com/reviews-by-company/state-farm-insurance/non-renewal-of-home-owners-insurance-20121029355321/2.html for a bunch of complaints; note Hartford apparently sells a guaranteed renewal endorsement

Per FACTA, these claims go on the CLUE database but not forever, maybe five years - see https://www.privacyrights.org/fs/fs6b-SpecReports.htm for information on these specialty reports

_Rate impact_  - see One home insurance claim may raise your premium by up to 21% (Study) from insurancequotes.com

## Managed repair

Crawford & Company (NYSE:CRD.B) is the major company after purchasing The PRISM Network which was first in 1994; see https://www.contractorconnection.com/ for the list

## Loss settlement and depreciation

Actual Cash Value Depreciation Deduction and the Broad Evidence Rule - Adjusting Today article which cites a few surprising court cases, including Goorland v New York Property Insurance Underwriting Association Supreme (2011) which rejected depreciation for ACV since it wasn't specified, and the relationship to the Standard Fire Policy. Note also that Lazaroff v. Northwestern National is the leading New York case which found no depreciation for partial losses and McAnarney v. Newark Fire Insurance Company which developed the Broad Evidence Rule which 23 state shave adopted and 11 use fair market value such as California according to Munich Reinsurance adjusters guide

Stated amount - per Subject:  [Stated Amount vs. Agreed Value in PAP and BAP in Ask Mike](http://www.iiaba.net/webfolder/la/ask%20mike/2012/ask%20mike%20%2312-03.pdf) this is really to limit exposure for old classic cars, as opposed to agreed amount

## General

See http://money.cnn.com/magazines/moneymag/moneymag_archive/2007/03/01/8400877/index.htm

Efficient or Plain Malicious: Are computerized claim adjusting programs designed only to cheat? (2006) - UP article, gets good in the background section down a ways, reminds me of a Bloomberg article

Allstate / McKinsey attorney economics

http://www.propertyinsurancecoveragelaw.com/tags/mckinsey-documents/ covers a common issue when I encountered through court case 3AN-11-11124 CI and "attorney economics"; discussed in somewhat more detail at http://www.huffingtonpost.com/2011/12/13/insurance-claim-delays-industry-profits-allstate-mckinsey-company_n_1139102.html

Major court case is Young v. Allstate in Hawaii (2008) see http://scholar.google.com/scholar_case?case=8704700719643766767

Maryland market conduct examination (1998 but finalized 2002) required that they use a different attorney script

_Covenant settlement agreements_  - basically once the insurer denies coverage, the insured can settle with the claimant in exchange for assigning right to sue insurer; see OneNote and Great Divide v. Carpenter for details and The Consent Judgment Quandary Of Insurance Law (2013) 

## Reservation of rights

Note Boise v. St. Paul Mercury (1941) as landmark case where insured can reject reservation of rights tho this has eroded

Found Gregory S. Munro (Montana Law professor)'s website which has http://www.umt.edu/law/faculty/munro/ Reservation%20of%20Rights%20Letter.pdf (THE INSURER’S RESERVATIONOF RIGHTS LETTER AND THE DUTY TO DEFEND) published in Trial Trends (Summer 2001). Note that Munro's website (http://www.umt.edu/law/faculty/munro.htm) has a lot of good information. Note that Trial Trends is a journal for the Montana Trial Lawyers Association (http://www.monttla.com/MT/)

There's also a wiki article on the reservationof rights letter.

Insured response: McLaughlin Company has an article discussing how to respond to the letter: "How should you react to a Reservationof Rights Letter?" (http://mclaughlin-online.com/librarytmc/whitepapers/ reservation-rights.html)

Insurer drafting: Jul2008 Claims Journal " ReservationOf Rights Letters" http://www.claimsjournal.com/news/national/2008/07/28/92273.htm

Let the Insurer Beware: How Courts Are Expanding the Obligations of Insurers Who Provide a Defense Under Reservation of Rights (2013) - ACIC presentation with a lot of details