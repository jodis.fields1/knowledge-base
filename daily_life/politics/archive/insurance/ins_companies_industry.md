# Ins companies and industry

## Startups

http://articles.businessinsider.com/2012-01-10/tech/30610666_1_health-insurance-insurance-rates-stik

http://www.claudepenland.com/category/insurance-startups-insurer-startup/ lists startups altho most seem to be just new ventures

http://lunatractor.com/2011/11/15/luna-case-study-a-health-insurance-startup/ Luna healthcare in Australia

WeatherBill - agriculture crop insurance http://www.reuters.com/article/2011/02/28/us-google-cleantech-idUSTRE71R7WF20110228

### Online shopping

CoverHound - price comparison; tried this out and it is interesting but still a bit normal agency

Bizinsure - gives you the docs and prices without talking to an agent

Bankrate Current Rates - http://www.bankrate.com/funnel/insurance/insurance-rate-quote.aspx just traditional but with a detailed form system

Insurance Zebra - start-up not ready et

Insurance Panda - don't get why this is decent, standard bullshit

## Analysts

Adam Klauber at William Blair is somewhat recommended per comment by David Merkel at Tower Group International: A Falling Knife in Action

## Cycle

[Great (And Not so Great) Expectations: An Endogenous Economic Explication of Insurance Cycles and Liability Crises](http://www.jstor.org/stable/253853) (2000) - skimmed thru this article in 2011, don't remember anything about it

## Groups

State Farm - largest;  [Is the State Farm Policy Really Worth Anything?](http://www.propertyinsurancecoveragelaw.com/2009/06/articles/insurance/is-the-state-farm-policy-really-worth-anything/) blog post generated a lot of complaints 

22-company Standard & Poor’s 500 Insurance Index

2013 Ward’s 50 P&C Insurers Announced - these are particularly profitable

Q1 Top 10 Auto and Homeowners Insurers by Direct Written Premiums - SNL ranking

Sentry Mutual - fair-sized group with $10b in premium and $3b in surplus

Talanx Group - looked into this under equipment breakdown note; eyeing IPO in 2012

HCC - lots of health; looks kinda like Travelers otherwise; came to my attention because of a surety rating plan

Mapfe - biggest in Spain and biggest non-life in Latin America

Assicurazioni Generali S.p.A. - biggest in Europe after AXA

BB Seguridade SA - huge Brazilian insurer per Brazil Insurer Continues Outperforming After World’s Largest IPO

Burns & Wilcox - family-owned; buddy works there; probably owns Atain Insurance Group so it's not just a broker

State National (group code 93)- small player with $600m in premium but a bit big in collateral protection; cheap D&O rates I think under National Specialty brand

Warranty Group - owns Virginia Surety; was a bit of a mystery since not public but in 2014 TPG bought it from Onex

## Auto insurers

21st Century

AAA

AIG

Allstate

American Family

Amica

Auto Owners

Erie Insurance

Esurance

Farm Bureau

Farmers/Zurich

GEICO

Hartford

Liberty Mutual

Mercury

Metropolitan

Nationwide

Progressive

Safe Auto

SAFECO

State Farm

Travelers

USAA

Unitrin

Foremost

Boat US

## Business fundamentals

Taking AIG from "SUcks" to "Rocks" (2014) - Hancock using "RAP" (risk-adjusted profitability)

Did some research on MGA (Managing General Agents); http://en.wikipedia.org/wiki/Managing_general_agent notes they have become less significant over time due to technology; mainly in Excess & Surplus Lines (E&S) due to the randomness of geographics; can look them up at https://events.aamga.org/custom/directory/; http://specialtyinsurance.typepad.com/specialty_insurance_blog/2009/02/index.html comments on program business and notes a book called Partners in Profits: The Managing General Agent System Under Fire; [Markel to buy Canadian managing general agent](http://www.businessinsurance.com/article/20090901/NEWS/909019989) is an example of one doing a lot of specialty stuff