# Mutuals (demutualization included)

(State Farm attachment included)

## General
Liberty Mutual - in 2012 proxy ballot (which passed), the number of members necessary to nominate someone to board was increased 10-fold; noticed an interesting notice in a newspaper at http://news.google.com/newspapers?nid=336&dat=20010909&id=6i8jAAAAIBAJ&sjid=fewDAAAAIBAJ&pg=3983,5155259
* see ./mutuals_LibertyMutual2012Proxy.pdf

State Farm Mutual - 5,000 needed per their articles of incorporation (attached)

http://articles.boston.com/2012-05-23/business/31828298_1_john-cusolito-mutual-companies-executive-compensation shows MA Senators interested: Senator Brian A. Joyce; Mark C. Montigny; and Patricia D. Jehlen

Premera - not a mutual but a nonprofit - what's its corporate governance? Pulled its articles of incorporation and can't tell.

## Demutualization
2011 Harleysville purchase by Nationwide over Liberty Mutual objections

Harleysville Mutual merger with Nationwide Mutual gives a lot of investors and nothing to policyholders even tho the mutual company owns 54% of the publicly-traded company. See http://www.wohlfruchter.com/cases/class-action-behalf-policyholders-members-harleysville-mutual-insurance-company as well as http://online.wsj.com/article/BT-CO-20120305-709495.html and http://www.insurancejournal.com/news/east/2012/01/25/232610.htm

Schiff of the insuranceobserver.com commented at http://messages.finance.yahoo.com/Stocks_%28A_to_Z%29/Stocks_H/threadview?m=te&bn=8490&tid=1426&mid=1426&tof=6&frt=2#1426

http://www.philly.com/philly/blogs/inq-phillydeals/147807825.html discusses how Tom Consedine approved the deal on April 16 through Order ID-RC-12-05; mutual policyowners authorized it by a huge margin per http://sec.gov/Archives/edgar/data/792013/000110465912028046/a12-10447_18k.htm

http://www.statefarm.com/about/media/media_archive/auto_dividends.asp discusses a $3 billion dollar dividend

http://www.insure.com/articles/generalinsurance/insurance-dividends.html relatively good article on insurance dividends