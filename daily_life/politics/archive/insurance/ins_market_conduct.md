# Ins market conduct

## Exams countrywide

Top 10 Market Conduct Complaints by Regulators Against P/C Insurers - Wolters Kluwer report

## General

Trial Lawyers Association has Ten Worst Insurance Companies document

## ID theft insurance rates
I found they had 99% profit margins

### force-placed

per http://www.americanbanker.com/issues/177_51/force-placed-insurance-premiums-california-dave-jones-1047535-1.html - note that the CFPB will be regulating this; Forced-Place Insurance Rises On Fed's Radar http://www.propertycasualty360.com/2012/03/09/forced-place-insurance-rises-on-feds-radar has a discussion

Possible loophole with master policies issued to commercial groups, but standards requiring those commercial groups to issue cancellation notices in-line with personal standards when cancelling "certificates"? Even though personal lines standards are less than commercial, there may be a void here...

## Colossus software
as discussed in http://www.legalnewsline.com/spotlight/235935-judge-in-ark.-colossus-class-action-did-not-play

In Alaska, require that liability losses in the state be reported and pooled for risk analysis to allow state-specific rates, particularly since we have this Alaska Rule of Civil Procedure 82

Colossus and other similar computerized claim payment systems http://www.consumerfed.org/news/536 (June 4, 2012 "Report: Insurers Can Manipulate Computer Systems to Broadly Underpay Injury Claim") should be reviewed more carefully

## worthless insurance
Rental car companies should not be allowed to sell you worthless first-party coverage if your credit card covers it

## Media

See The Insurance Hoax by Bloomberg David Dietz and Darrell Preston from 2007

## History
see OneNote from work but basically study from Missouri in 2007 says this started in 1970s after a McKinsey report

## States

According to Institutes Ins Reg textbook (p5.6 1st edition) only one-fifth of states have dedicated market conduct staff

See http://www.naic.org/committees_d_mawg.htm for a link to a list of states that publish their market conduct exams online.

Market Systems Participation over at NAIC I-Site Utilities (https://i-site.naic.org/common/util) is a good way to see how states are doing. Doesn't exactly track population; California has a lot (38M people), but so does Missouri (6M); while TX with ($20.8M) has less.

Klein's 2008 draft paper is attached (final paper in Ch2 of Brookings Institute's Future of Insurance Regulation http://www.worldcat.org/title/future-of-insurance-regulation-in-the-united-states/oclc/294886454&referer=brief_results (not to be confused with the 2011 book

The Future of Insurance Regulation and Supervision: A Global Perspective) and shows the budgets and staff members for departments; NY is $300k+ per employee while CA if $150k, AK is $115k and Florida is all the way down at $50k. Not sure how it compares to population or premium written per state.

Note that Insurers Denying Information To Regulators (August 4, 1997) raised an important issue.

## IRES

2012-08: attended IRES in Hollywood FL; Hurricane Isaac was happening; Leslie Krier kinda dominated the discussion; didn't really learn much but did learn that Rob Stroup out of Ohio was credited with starting MCAS with an Access database; I mentioned XBRL at a discussion at the end; also asked a question about UCR

## Model law

NCOIL created MARKET CONDUCT SURVEILLANCE MODEL LAW adopted November 11, 2006 which I read through; noticed that the conflict of interest provisions are extremely strict going back 5 years; otherwise it looks pretty basic

## Other documents

Insurance Regulation: Common Standards and Improved Coordination Needed to Strengthen Market Regulation. No. GAO-03-433, September 30, 2003.

Market Regulation Handbook - good guide

## Market conduct exams

### Alaska

MCE 04-01 Unum provident12/20/2004 - first on the website; found report at http://www.maine.gov/pfr/insurance/unum/unum_exam_settlement.htm (report at http://www.maine.gov/pfr/insurance/unum/Unum_Multistate_ExamReport.htm) - pretty interesting; even expanded the board of directors for the parent to include 2 persons with regulatory experience - Wikipedia has some background at http://en.wikipedia.org/w/index.php?title=Unum&oldid=467807644#Controversy

MCE 05-01 Seabright Insurance Company 12/29/2005 - schedule rating review

MCE 05-02 Mega Life & Health Ins. Co., Mid-National Life Ins. Co. of TN, Chesapeake Life Ins. Co. (d/b/a HealthMarkets) 04/07/2009 - very long, detailed report of health insurer by Lead States Alaska and Washington; report seems to be dated Dec 20, 2007 - so why is the effective date for 2009. Note that there's a 05-02A addendum to this as well.

MCE P-05-03 Willis of Alaska 03/25/2006 - broker review began because of bid rigging and steering allegations; 3 findings - two in favor of the broker and 1 about not correcting overcharges; 4 recommended actions. $1.3 million in contingency fees for the period; all contingency arrangements included loss ratio requirements.

MCEP-05-04B Marsh USA Inc, Marsh USA Inc ( Alaska) 04/27/2007 - Two unusual contracts were found which violated broker fee agreements (page 5) discussed in detail. Did not read carefully.

MCE-P-05-05 Alaska Premier Underwriters, Inc., Lisa Wallis 05/04/2006 - 40-50% surplus lines insurer; apparently did not have good procedures in place

MCE05-06 Waddell & Reed Inc, W & R Insurance Agency, Inc 10/26/2005 - consent order related to annuity insurance issues found by Kansas and Minnesota

MCE05-09 Western and Southern Life Insurance Company 12/30/2005 - consent order related to a "Race Based Pricing Activities With Respect to the Life Insurance Business ..." report.

MCEP-05-10 Barbara E Kardys, Adjusting Specialty Group Inc., Arctic Adjusters 10/09/2006 - workers comp adjuster; examinee did not respond; report says lack of cooperation, "many instances of voiolations" under AS 21.36.125; "significant problems with record maintenance"; and so on.