# Insurance accounting

## Surplus adjustments to SAP

The main adjustments are adding back non-admitted assets (duh) and adding back the acquisition expenses from the unearned premium reserve, and this makes sense given that they would get a refund for the commission I guess this makes sense as they capitalize these marketing expenses and gain revenue from them over time (also glanced at p677 of Foundations of Insurance Economics: Readings in Economics and Finance)

## 2013 FASB  

Available at the  [FASB Exposure Drafts page](http://www.fasb.org/cs/ContentServer?c=Page&pagename=FASB%2FPage%2FSectionPage&cid=1176157086783) with  [direct link to the Topic 834 draft](http://www.fasb.org/cs/ContentServer?c=Document_C&pagename=FASB%2FDocument_C%2FDocumentPage&cid=1176163028066); 

[NYT Norris](http://www.nytimes.com/2013/06/27/business/new-rules-expected-for-insurance-accounting-may-lead-to-erratic-earnings.html?_r=1&) coverage notes broader scope and affect on life insurance of timing; for p&c, reserves would change from the most likely result to an average of possible results and in addition would be e subject to discounting

AccountingToday [FASB Proposes Major Changes in Insurance Accounting](http://www.accountingtoday.com/news/FASB-Proposes-Major-Changes-Insurance-Accounting-67265-1.html) - somewhat light but defines fullfillment cash flows as the "net of the expected cash inflows and cash outflows" which is "based on the unbiased probability weighted estimate (expected value) that incorporates all relevant information and considers all features of the contract, including guarantees and options" and also includes a margin for the expected profitability

Insurance journal [P/C Insurers Question Need for Insurance Accounting Overhaul](http://www.insurancejournal.com/news/national/2013/06/28/297062.htm)

## IFRS insurance project page
[overview](https://web.archive.org/web/20180420005542/https://www.ifrs.org/projects/2017/insurance-contracts/); they have examples (!) in the exposure draft page

Decent [20 June 2013 iasplus overview](https://web.archive.org/web/20181213144005/https://www.iasplus.com/en/news/2013/06/iasb-insurance-ed); includes definition of contractual service margin ("initial recognition, the contractual service margin is calculated at an amount equal and opposite to the sum of:  (a) the amount of the fulfilment cash flows for the insurance contract at initial recognition; and (b) any cash flows that are paid (received) before the insurance contract is initially recognised" and "subsequently, the contractual service margin will be released to profit or loss over the coverage period as the insurer fulfils its obligations" but when the contract becomes onerous the change becomes effective immediately

South Africa's org has  [surprisingly detailed overview for Insurance Contracts (Topic 834)](https://web.archive.org/web/20200426035401/https://www.fasb.org/jsp/FASB/Document_C/DocumentPage?cid=1176163028066&acceptedDisclaimer=true)

## Revenue and expenses of contracts

Lots of jargon, contractual service revenue, etc but two main approaches:

Premium Allocation Approach (PAA): IASB version was criticized in 2010 by the Group of North American Insurance Enterprises (GNAIE), Jerry de St. Paer recommended Alternative Measurement Paradigm for short-term p&c, approach used for nearly a century and similar to FASB; FASB 2013 exposure draft has a decent explanation; E&Y has a couple articles (" [Boards make decisions on the premium allocation approach](https://web.archive.org/web/20140708010424/https://www.ey.com/Publication/vwLUAssets/Insurance:_decisions_on_premium_allocation_approach/$FILE/Insurance%20Alert%20March%202012.pdf) and [Boards debate premium allocation approach](https://web.archive.org/web/20140708010424/https://www.ey.com/Publication/vwLUAssets/Insurance:_decisions_on_premium_allocation_approach/$FILE/Insurance%20Alert%20March%202012.pdf)

Building Block Approach: alternately called the general form  of the PAA or an entirely different model

_Research papers_ -  [Critical Study of the IASB Insurance AccountingProject](http://w-arc.jp/OP20130418%28Hane%29English.pdf) by Keisuke Hane argues that IASB is trying to depart from the traditional matching to solve the probelm of matching assets and liabilities while turning its back on revenue-expense matching

[Insurance Accounting](http://www.iii-insurancematters.org/white_papers/accounting-international-standards-and-solvency.html) - decent overview by III