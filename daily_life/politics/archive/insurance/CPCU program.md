# CPCU program

## CPCU500 Foundations of Risk Management and Insurance
This was mostly review. Highlights include discussion of COSO, ISO 31000, Federation of European Risk Management Associations (FERMA), COPE (construction, occupancy, protection, and exposure), DICE (Declarations, Insuring Agreement, Conditions, and Exclusions). Also learned more about insurable interest, particularly factual expectancy. Also discussed depreciation schedules briefly, including the “broad evidence rule” whereby all relevant factors should be used in determining actual cash value – not just market value. I also learned some things about tenancy law, which I put into my Anki law deck.

Mnenomic for extended coverages for Basic Group I and II: WHARVES - wind, hail, arson, r?, v?, e?, s?

## CPCU520
Most of this was pretty basic. Chapter 5, the property underwriting chapter was interesting in that it noted a lot about fire protection and discussed National Fire Protection Association standards and what causes most insurance losses. Page 5.31 had the tornadoes by state. Chapter 8 on claims was good, and the appendix lists some organizations such as the National Association of Independent Insurance Adjusters (NAIIA), which has a depreciation guide linked at the claimspages.com. There's also two bureaus of the Alliance of American Insurers - the Property Loss Research Bureau and the Liability Insurance Research Bureau. Note also that there is Claims Advisor Magazine and Adjusting International for claims handling resources. National Insurance Crime Bureau's page about Arson (Exhibit 9-1 in 9.14 in edition 1) is interesting altho common sense when you think about it.

Contains numerous fascinating facts about the industry from Insurance Information Institute's Fact Book glanced online at iiky.org/documents/Insurance_Factbook_2012.pdf

9.19 of the CPCU520 textbook (first edition) notes that deductibles applied before coinsurance benefits the insured more; this should be applied unless the policy states differently (commercial building and personal property BPP form states differently).

Chapter 3 Insurance Marketing discusses agency bill (item, statement, account current) and direct bill. Agency bill I imagine has a much higher fraud risk; direct bill is much more common for personal lines.

Normal Loss Expectancy, Probable maximum loss (PML), maximum probable loss, maximum possible loss (MPL) and maximum foreseeable (MFL)  loss assessments for property insurance - see "Developing Fire Risk Tolerance Profiles" and fireriskforum.com

Underwriting Liability Insurance

Underwriting General Liability discusses the difficulties in underwriting subcontrators. Completed operations is more service-oriented than products.

Underwriting Workers Comp mentions the difficulty of maritime - notes USLH&WCA and Jones Act which generally offers more coverage. Notes gray areas for occasional acts. Relative premium size is used in determining whether to accept (e.g., not more than 50% of account's premium). Sept 9/11 was a workers comp disaster.

Residual markets - looked this up to find that "assigned risk" automobile insurance plan (AIP perhaps run by AIPSO) is by far most common; Joint Underwriting Association (JUA) comes next; reinsurance facilities only in North Carolina and perhaps Vermont - functionally similar and described http://www.wikinvest.com/stock/The_Hanover_Insurance_Group_%28THG%29/Reinsurance_Facilities_Pools all insurers have to purchase the reinsurance

Claim Handling Process -
* reservation of rights is important to avoid implicit waiver and estoppel; Exhibit 9-2 in edition 1 shows an example
* six common reserve methods: individual case, roundtable, average value, formula system, loss ratio; when reading the book I learned about the tabular method (sort of a formula system; used in workers comp to match similar claims), also "stair-stepping" or "reserve creep" is bad, should not be adjusted randomly (more than 3 times?);; learned about the ADR methods summary jury trial and minitrial; subrogation is another element: Nationwide Inter-Company Arbitration Agreement sponsored by Arbitration Forums, Inc allows this to be handled more quickly

Property Claim Handling - four major exclusions: gradual cause of loss, ordinance or law, faulty design or material, and intentional acts. Determing actual cash value: "broad evidence rule" includes depreciation, obsolescence, and fair market value; likely to end up at Arbitration Forums if there is no agreement; fair market value should not be used unless there is a secondary market - note that certain things like antiques might be very valuable under actual cash value

Handling Specific Types of Property Claims - has a section on bailment where it discusses the bailee and owner's claims; some cover just the bailee's liability and others cover the owner regardless of liability to maintain customer goodwill (quasi first-party); with no specific agreement, bailees are liable for damage with the exception of acts of God, war, negligence of the shipper, exercise of public authority, and inherent vice of goods (also owners negligence). Liability can also be specified on the bill of lading and a released bill of lading allows owner to reduce bailee/carrier's liability.

Liability Claim Handling Process - page 11 discussed how, if a contractor negligently puts up a wall and the wall collapses and injures someone, the wall would not be covered but the injury would, as the wall would be a breach of contract. I'm guessing the contractor's surety bond would kick in or something to cover that. Comparative liability - there is a pure form and two "modified" forms (50% and 49%) where claimants which have this amount of liability cannot recover. Damages are classed into "general" and "special damages"; latter includes loss of earnings and other precise costs such as medical expenses. Discusses fighting medical expenses; also discusses collateral source rule requiring double-payment. Also discusses settlements and the possibility of rejecting a settlement only to have the judgment exceed the limits - putting the insurer at risk of a bad faith lawsuit. Ultimately settled with a "general release". Release may be specialized or include spouse; if lawsuit has been filed, claimant files notice with the court about settlement. Advance payments can be made, without requiring a release but with document specifying that amount counts toward final. Structured settlements include periodic payments; walk away settlements require no release. When an insured defends, if the allegations involve both covered and non-covered, both are defended. Reserving is also important - see claim handling process above.

  


Handling Specific Types of Liability Insurance -

  * Auto liability - vehicle insurance before driver. UM/UIM discussion was interesting: definition of underinsured vehicle varies, with some states defining it as a vehicle with liability limits less than the UIM limit ("limits trigger") and others defining it as less than the damage ("damages trigger"); the other issue is stacking, which can be interpolicy (two spouses policies, for example), intrapolicy (per vehicle, for example), and so on.
  * Premises liability - building owners own different levels of care depending upon how much they invited the person (http://en.wikipedia.org/wiki/Standard_of_care)
  * Contractors liability - discusses contractual liability for those who are working for him, _in addition_ to liability for failing to supervise
  * Products liability - warranty, express warranty, or strict liability in tort; retailer versus manufacturer relationship; warnings/labels; improper use
  * Workers Compensation - Part One is generally used for injury regardless of liability; Part Two (employers liability) is for employees who are exempt to Part One such as agricultural or have rejected the coverage, must prove negligence. Medical portion: lack of coinsurance makes the medical trend greater; not sure about UCR but course says workers comp has less influence than health insurers; utilization review and medical management. Disability portion: must go before board or commission if claimant does not agree, which can take months.
  * Professional Liability - cases tend to be litigated to a verdict rather than settled, possibly because of the insured's consent to settle requirement and the hammer clause related to that



Reinsurance Sources - includes direct reinsurers, intermediaries, and pools/syndicates; orgs include: Intermediaries and Reinsurance Underwriters Association, publishes Journal of Reinsurance; Brokers & Reinsurance Markets Association focuses on treaty reinsurance and has a contract textbook; Reinsurance Association of America does political advocacy and educuation

  


Types of Reinsurance - tons of types broadly grouped as follows: pro-rata insurance includes quota share and surplus; other side is excess of loss (non-proportional). Five types of excess of loss: (1) per risk, used mainly in property; (2) catastrophe which has an hours clause (aggregate all within 72 hours) and may not be per occurrence; (3) per policy excess of loss used mainly in liability; (4) per occurrence excess of loss used mainly in liability; (5) aggregate excess of loss also called stop loss when the attachment point is a loss ratio; often have coparticipation agreement. Note that "extra-contractual damages" (ie bad faith or excess of policy limits losses) usually not covered by reinsurance altho may be.

  


Alternatives to Traditional Reinsurance - catastrophe bond (see <http://www.artemis.bm/blog/2012/02/08/catastrophe-bonds-a-unique-asset-class-offering-sizeable-returns/> for retail investor option GAM Star Cat Bond; also Munich Re MURGY and Swiss Re); cat risk exchange (primary insurers exchange their risks); contingent surplus (allowing immediate access to capital); industry loss warranty (ILW - triggered by industry loss ratio); catastrophe option; line of credit; sidecar - SPV which meets private investors with primary insurer on a quota share agreement.

  


Reinsurance Regulation - in 2008 New York issued some "certainty" regulation in Circular 20 based on contracts not being finalized when the WTC 2001 event hit; primary insurers can get "credit for reinsurance" (credit for ceded premium and loss reserves due from the reinsurer), however these require specific clauses in the reinsurance agreement; some states require reinsurers to be licensed; reinsurers are liable for insolvency or when the intermediary does not pay; reinsurance credit can be also be acquired if there is a letter of credit or something

  


The Five Forces and SWOT method - Five Forces Model by Michael Porter; SWOT analysis (Strengths/Weaknesses, Opportunities, and Threats); note Wikipedia has a sixth force for the government <http://en.wikipedia.org/wiki/Six_Forces_Model>

  


Insurers Global Expansion - emphasizes the remarkable growth in Asia and the political risks in foreign countries; notes strategic alliances such as joint ventures as a good method to overcome bias against foreign companies

  


## CPCU530

Includes a lot about contract law, UCC, etc. Warranties and Magnuson-Moss Act - note warranties are codified in the UCC but can be waived by consumers, typically something sold "as-is" has no warranty; bill of lading, etc.

With regard to materiality, there are different standards such as "contribute to the loss" (see Understanding Insurance Law (2012) by Robert H. Jerry, p. 731)