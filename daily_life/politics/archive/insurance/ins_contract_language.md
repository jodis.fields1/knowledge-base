# Ins contract language

## Readability

Inspired by Nationwide's 2013 survey Consumers Confused, Overwhelmed by their Insurance Policies, According to Survey 

See regulatory agencies for more information on the working group

Nevada posts homeowners and auto policies online

Flesch score trend started in the 1980s but doesn't really work

_Court quotes_ \- classic "flood of darkness" quote from 1873 case Delancy v. Insurance Company; see also 1975 quote Drake v. Globe American Casualty Co

## Clauses

Sharing: Lamb-Weston rule and so on; see A Flush Beats a Straight and Excess Other Insurance Beats Pro Rata Other Insurance which discusses NJ case of first impression which led me to W9/PHC REAL ESTATE LP v. Farm Family Cas. Ins. Co., 970 A. 2d 382 and from there I went to Jones v. Medox, Inc., 430 A. 2d 488 - DC: Court of Appeals 1981 which has a cost-benefit analysis

Appraisal clause: Appraisals, Breach of Contract and Bad Faith: Is There A Connection? discusses Intermodal Equipment Logistics, LLC and Seatrain Logistics, LLC v. Hartford Accident Indemnity Company. Also see  [The Scope of Appraisal Panels Authority in Texas](http://www.propertyinsurancecoveragelaw.com/2014/05/articles/insurance/the-scope-of-appraisal-panels-authority-in-texas/) (2014) where coverage decisions can be made by the appraisal panel. Note  [The Scope of Appraisal, Part 4: Delaware](http://www.propertyinsurancecoveragelaw.com/2014/05/articles/insurance/the-scope-of-appraisal-part-4-delaware/ ) which is a federal district case from 2000

Loss payment time limits: AK filing # 94989 discusses time limits on filing claims and how American Bankers thinks it has 30 days from agreement (per 02/14/2013 response #3); noted to them AS 21.36.125(a)(6) which is part of the unfair claims settlement act; however American Bankers later contradicted itself in 95255 by saying it will make these payments promptly

Reverse competition: in consumer credit and lender-placed insurance (LPI) it is particularly big; see MDL-365 for a model law; Section 7 discusses a minimum loss ratio and Section 8 has a space for a commission cap; Birny Birnbaum of the Center for Economic Justice leads the charge see http://www.consumersunion.org/finance/testwc101.htm; sophisticated discussion at Record - Society of Actuaries, Volume 20, Issue 4, Part 1 from 1995

Concurrent causation: see attached Causation in Insurance Law by Randall Smith and Fred Simpson; note that in Alaska, the bill AS 21.36.212 arose from State Farm v. Bongen and was sponsored by former Alaska Senator Dave Donley; he contacted the Division in 2013 requesting a consumer rep in the office; see details on him at http://www.linkedin.com/pub/david-arthur-%22dave%22-donley/38/a26/403; George R. Lyle at Guess & Rudd (profile at http://www.guessrudd.com/Who-We-Are/George-R-Lyle.shtml) pointed out in a 2013 Alaska Bar Association article that "Implications for Environmental Insurance Coverage of the Alaska Statutory Prohibition On Denying Insurance Claims..."

_Insurance Contract Analysis_ (1992) first edition has good discussion on p197-200; for example Fawcett House v. Great Central found vandalism was proximate cause of water damage and later man-made causes of earth movement were proximate causes, leading to the ordinance/construction etc concurrent causation exxclusion