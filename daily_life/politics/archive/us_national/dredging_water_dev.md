# Dredging and water dev

See committee appropriation notes for complementary discussion

This is all wrapped up in the Army Corp and motivated by  [Water Resources Development Act of 2013](https://www.govtrack.us/congress/bills/113/hr3080#overview "https://www.govtrack.us/congress/bills/113/hr3080#overview") which has almost passed. See  [Committee Releases Water Resources Reform and Development Act Conference Report](http://transportation.house.gov/news/documentsingle.aspx?DocumentID=379806 "http://transportation.house.gov/news/documentsingle.aspx?DocumentID=379806") (2014)

**Overview**

Water Resources Development Act ( [wiki](https://en.wikipedia.org/wiki/Water_Resources_Development_Act "https://en.wikipedia.org/wiki/Water_Resources_Development_Act")) \- periodic bill which authorizes (no appropriation) various waterway development

**Ports**

West coast has natural deepwater ports so no dredging necessary; 2013 opens up funds for other projects so could get $25m out of the $800m annually


[Price tag to dredge Eastern ports for big ships: $5 billion](http://usatoday30.usatoday.com/money/economy/story/2012-06-21/southern-ports-expansion/55746890/1 "http://usatoday30.usatoday.com/money/economy/story/2012-06-21/southern-ports-expansion/55746890/1") (2012) - uhh, that's a lot!

[Dredging Up More Money for Maintenance](http://public.cq.com/docs/weeklyreport/weeklyreport-000004107500.html "http://public.cq.com/docs/weeklyreport/weeklyreport-000004107500.html") (2012) - behind the scenes discussion on politics

Norfolk - ready for the biggest ships as of 2012

New York - has to raise the bridge for biggest ships

Baltimore - also pretty major

Boston - likely getting $310m for a 5-year project 

  


Harbor Maintenance Tax, which averages a little more than $100 per container imported through U.S. ports - per  [this](http://www.thenewstribune.com/2014/05/15/3197086/tacoma-and-seattle-ports-could.html "http://www.thenewstribune.com/2014/05/15/3197086/tacoma-and-seattle-ports-could.html") makes Puget sound less competitive

**2013 version**

[First Take on Water Resources Conference Report: Lacks Necessary Reforms, Hikes Spending](http://blog.heritage.org/2014/05/17/first-take-water-resources-conference-report-lacks-necessary-reforms-hikes-spending/ "http://blog.heritage.org/2014/05/17/first-take-water-resources-conference-report-lacks-necessary-reforms-hikes-spending/") (2014) - interesting critique, backlog sounds bad


**Research**
** [ Dredging Coastal Ports: An Assessment of the Issues ](http://www.nap.edu/catalog.php?record_id=608) [ ](http://www.nap.edu/catalog.php?record_id=608) [ ( 1985 ) ](http://www.nap.edu/catalog.php?record_id=608) \- notes appropriately that reducing maintenance dredging is key, which is caused by sediment building up **

[ Sedimentation Control to Reduce Maintenance Dredging of Navigational Facilities in Estuaries](http://books.nap.edu/catalog.php?record_id=1023 "http://books.nap.edu/catalog.php?record_id=1023") (1987) - doesn't talk much about vegetation at all but huge variety; Asia has a bigger issue

[Dredging rivers wont stop floods. It will make them worse](http://www.theguardian.com/commentisfree/2014/jan/30/dredging-rivers-floods-somerset-levels-david-cameron-farmers "http://www.theguardian.com/commentisfree/2014/jan/30/dredging-rivers-floods-somerset-levels-david-cameron-farmers") (2014) \- just an oped by Monbiot but intriguing
