# US Congress and legislators

## contacting
https://democracy.io/ - code at https://github.com/sinak/democracy.io/issues but also see https://github.com/unitedstates/contact-congress/

http://www.tweetcongress.org/home

## Current news and updates (old)

Note related pages on US federal govt and various politics stuff

Been reading up on the schedule, and noted that the session begins in Jan 3 and extends throughout the year, basically. There used to be a "short" and a "long" session but that changed into two long sessions, an "even" and an "odd" per the 20th amendment to the Constitution (see http://www.thegreenpapers.com/Hx/SessionsExplanation.html). After two sessions, another term of Congress begins and all laws have to be resubmitted

http://en.wikipedia.org/wiki/Congressional_Record is a good source; has a Daily Digest; however not available in RSS feed

followed Politico for several months but switched over 2014-01 for The Hill (OTCM:News Communications, Inc. - deregistered 2005), might try the National Journal (Atlantic Media) next, CQ Roll Call, Washington Post are other ideas

## Schedule and recess

Been trying to figure out how much time Congress spends in recess; I guess it all depends on who's leading. For example, in 2011 Eric Cantor set a schedule where the House is 2 weeks on, 1 week off (http://www.csmonitor.com/Commentary/Editorial-Board-Blog/2011/0104/Will-the-112th-Congress-work-hard-or-hardly-work). CS Monitor notes that for 2011, "members will be in session 123 days, compared to an average of 126 days that first sessions of Congress worked between 1993 and 2010"; it also notes that states have widely varying sessions. Slate has also a Recess! discussion (http://www.slate.com/articles/news_and_politics/explainer/2009/07/recess.html). Typically the big break is in August - Slate notes Aug 3 to Sept 4 - plus weeklong breaks in Feb and May and a 2-week break in April.

Staying in session can also involve loopholes - Slates notes that "in 2008, Democrats held pro forma sessions in which a single senator would gavel in and gavel out every three days in order to keep the Senate officially in session, to prevent President Bush from making recess appointments".

See committees note for their schedule

## Transparency

Discussed Cantor's agenda for transparency below; see also http://www.opencongress.org/wiki/Republican_Transparency_Promises and much smaller list at http://www.opencongress.org/wiki/Democrat_Transparency_Promises; also see http://www.opencongress.org/wiki/Obama_Transparency_Record and http://www.opencongress.org/wiki/Project:Transparency_Hub; Republicans revealed their party rules http://sunlightfoundation.com/blog/2013/02/28/house-republican-conference-has-most-transparent-rules-in-congress/;

## Think tanks (exert influence)

American Action Forum - http://en.wikipedia.org/wiki/Douglas_Holtz-Eakin (had hopes he was impartial, but Weismann's comment in Taming Abuses, Advancing Values shows that he is biased as a 2013 Judiciary committee meeting The Obama Administration's Regulatory War on Jobs, the Economy, and America's Global Competitiveness described the 2012 cost of regulations when 3/4 was due to auto fuel-efficiency standards which return $1.7 trillion); per http://thinkprogress.org/climate/2012/07/10/513568/climate-republicans-global-warming-initiativ/?mobile=nc they are in favor of global warming regulations

Campaign for Liberty - founded in 2008 and fights for libertarian ideals led by John Tate; instrumental in the Amash amendment

NDD United - stands for nondefense discretionary, coalition of trade groups mostly, emerged in 2013 due to sequestration, oddly includes the defense industry trade group

## Laws plus institutions and procedures

Obviously the Constitution and then a great overview is CRS report Introduction to the Legislative Process in the U.S. Congress

Senate and House are different in several ways: one major way is that "the House has a very structured debate process. Each member who wishes to speak only has a few minutes, and the number and kind of amendments are usually limited. In the Senate, debate on most bills is unlimited" per White House; there are legislative days which for the Senate can last a long time per http://thomas.loc.gov/home/definitions/legisday.html; http://thomas.loc.gov/home/ds/index.html shows session days and http://www.c-spanvideo.org/congress is best for seeing what's happening

Cosponsoring bills - doesn't really matter  [per Anthony Clark at Salon](http://www.salon.com/2013/08/02/congress_favorite_time_wasting_scam_co_sponsoring_bills/)

Veto - turns out there is some controversy over the pocket veto which can act as an absolute veto, see wiki and Robert Spitzer's analysis for details

Introduction to the Legislative and Regulatory Process - decent general overview from AACN

### House procedure

http://en.wikipedia.org/wiki/Procedures_of_the_United_States_House_of_Representatives

read over CRS Report The Legislative Process on the House Floor: An Introduction when I noticed a major house bill with zero amendments (which the clerk summarized at http://clerk.house.gov/floorsummary/floor.aspx as "Considered as privileged matter. H. Res. 295 — "Providing for consideration of the bill (H.R. 2642)"); CRS notes that the "House considers most legislation by motions to suspend the rules, with limited debate and no floor amendments, with the support of at least two-thirds of the Members voting";

an appropriations for energy and water bill passed shortly before had a lot of (failed) amendments so amendments do happen

"previous question" requires only a majority in House (opposed to two-thirds in Senate)

p8 of the CRS rule mentions that the "closed rule" is controversial; The Closed Rule is a 2010 article on it by Michael Doran in Emory Law Journal 

More details in The Amending Process in the House of Representatives 

"Republican tradition known as the Hastert rule. The informal tenet, named after former House Speaker Dennis Hastert, says that the House speaker does not introduce legislation unless a majority of Republicans say they will vote for it first"

News analysis: Return of the chairmen? - Politico article noting the dynamics of the House power

Open versus closed rule:

> More details on rules for debate come from the example of H.R. 2397 which was heard under structured rule (as it was in 2012) per H. Res. 312 with details in H. Rept. 113-170 where 100 amendments were permitted by the Rules Committee (versus the 16 in 2012) which generally allowed any amendment with limits on amendments on Egypt, Syria, and NSA, but they did allow the Justin Amash amendment (Amdt. 100 or also known as H.Amdt. 413 (Amash) to H.R. 2397). The entire process is summarized in the  [2013-07-23 Congressional Record](http://beta.congress.gov/congressional-record/2013/07/23/house-section/) on page  [H4866-5](http://beta.congress.gov/congressional-record/2013/07/23/house-section/article/H4866-5) with debate over Syria particularly heavy, but keep in mind that an appropriations bill is supposed to be limited in the changes it makes. The Rules committee website has a  [nice page for filtering legislation](http://rules.house.gov/legislation?term=&rule_type=All&committee=All&congress=All). Note that the resolution committee report will have the list of the amendment

General Legislative Reorganization Act of 1946 which increased staffers, required lobbyist registration and reduced standing committees; also the Legislative Reorganization Act of 1970  Tenure makes a big influence ie "ranking member"; chairman ship also makes a big influence; 1970 reorganization law reduced that influence  Robert Byrd's orientation speech at http://www.senate.gov/artandhistory/history/common/generic/Feature_Homepage_ByrdOrientation.htm notes that  "almost from its earliest years the Senate has insisted upon its members’ right to virtually unlimited debate. When the Senate reluctantly adopted a cloture rule in 1917, it made the closing of debate very difficult to achieve by requiring a super majority and by permitting extended post-cloture debate. This deference to minority views sharply distinguishes the Senate..."  tends to be loosely-populated because Senators are hanging out elsewhere, but they are linked in especially from their office - see http://www.senate.gov/visiting/common/generic/Senate_in_session.htm also typically junior members fill in as presiding officers; noticed Begish was there; legislative clerk does the roll call

Budget process: http://en.wikipedia.org/wiki/United_States_budget_process   In the Senate, the Budget Committee "sets out a broad blueprint for the Congress with respect to the total levels of revenues and spending for the government as a whole" while the Finance Committee deals with entitlements and the Appropriations Committee deals with other program funding; doesn't seem to deal with any legislation but does seem to oversee the OMB  http://thomas.loc.gov/home/approp/app13.html is an interesting app on appropriations bills  Misconduct See http://en.wikipedia.org/wiki/Abscam and  http://en.wikipedia.org/wiki/Censure_in_the_United_States; also see http://en.wikipedia.org/wiki/Gang_of_Seven which exposed some scandals in the early 1990s  Federal legislators Murkowski - voted yes for S. 1003: A bill to amend the Higher Education Act of 1965 to reset interest rates for new student loans which was terrible for students; turns out she's way into that

Young-   [voted for the Farm Bill which Volokh notes is ](http://beta.congress.gov/congressional-record/2013/07/23/house-section/article/H4866-5) [](http://beta.congress.gov/congressional-record/2013/07/23/house-section/article/H4866-5) [ full of pork ](http://www.volokh.com/2013/07/17/why-its-so-hard-to-trust-republicans-to-limit-government-spending/ "http://www.volokh.com/2013/07/17/why-its-so-hard-to-trust-republicans-to-limit-government-spending/") with no means-testing (see Cato analysis), also in 2014 he voted against the anti-tax inversion appropriations bill

Contact forms for Murkowski and Begichare huge - what a hassle! OpenCongress form is unreliable. Doesn't seem that Popvox has a contact.

Jack Reed - Democrat who remains in House; voted against Iraq War; proposed increased penalties for financial violations

Chuck Grassley - Republican bipartisan sponsor with Jack Reed

Tom Coburn - decently honest Republican House member

Russ Feingold - tragically ousted from Senate position

Carl Levin - around since 1979; chaired the Armed Services committee forever

Waxman - Democrat house since 1975; ranking on Energy and Commerce committee

Kirsten Gillibrand - new NY senator; cute; doesn't like Spitzer who spurned her a while back

Alan Grayson - Rep from Florida who can talk shit but also gets things done according to Slate profile

Justin Amash - Republican Rep from Michigan who fights the Republican leadership

## Caucuses
Center Aisle Caucus - similar to the Gang of 6 (or 8?) http://en.wikipedia.org/wiki/Gang_of_Six and Gang of 14

International Conservation Caucus - very large?

Liberty Caucus - head of the Tea Party group

Life Insurance Caucus - sorta interesting

## Third-party websites

CongressMerge has an excellent schedule (http://www.congressmerge.com/onlinedb/cgi-bin/schedule.cgi?site=congressmerge) for both the House and the Senate, as well as committees; notably, the Senate has the future dates for all sorts of committee meetings while the House does not.

Popvox and OpenCongress are other sources; Govtrack for legislation

## History general

http://en.wikipedia.org/wiki/History_of_the_United_States_House_of_Representatives is the overview 

http://en.wikipedia.org/wiki/Party_divisions_of_United_States_Congresses is interesting

  


**Laws or acts**

See http://www.aaas.org/gr/stc/legtracker/legtracker113.shtml for AAAS legislation tracker for science

  


http://en.wikipedia.org/wiki/List_of_United_States_federal_legislation

  


_History_

See http://lcweb2.loc.gov/ammem/amlaw/lawhome.html A Century of Lawmaking for a New Nation which I found at http://law-library.rutgers.edu/ilg/usfedlaw.php

  


**Lobbying**

See Economists calculate true value of 'who' you know, rather than 'what' in US politics
