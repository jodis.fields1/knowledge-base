National defense
===

## Politics

[Leaders of Tea Party groups split on defense spending cuts](http://dailycaller.com/2011/02/04/tea-party-leaders-defense-spending-%E2%80%98sacred-cow%E2%80%99/) - nice overview of the groups; AEI (extreme support) and Heritage emerge as major defense supporters

## Extremism
* https://www.arabbarometer.org/2019/12/arabs-are-losing-faith-in-religious-parties-and-leaders/


https://www.reddit.com/r/islam/
https://www.reddit.com/r/exmuslim/
https://www.reddit.com/r/progressive_islam/
* https://www.reddit.com/r/progressive_islam/comments/heo9xa/what_do_you_believe_is_the_solution_to/

https://www.youtube.com/user/YasirQadhi/videos

Is Muslim extremism slowly declining? http://www.islam-watch.org/index.php?option=com_content&task=view&id=876&Itemid=58 says is it increasing; REVISITING THE ARAB STREET RESEARCH FROM WITHIN says arab extremists are not the same as people think; http://romove.radio.cz/en/article/23559 discusses far-right and says government action has diminished it 

## Appropriations and funding

See the appropriations committees for details; also world->US history note has some information

See http://www.usgovernmentspending.com/spending_chart_gallery and http://www.usgovernmentspending.com/defense_spending (under spending analysis) - also America’s staggering defense budget, in charts (2013) by WashPost

Subcommitte on Defense 

> read up on the House 2013 appropations report 113-113 ($600b total including "scorekeeping adjustments" and overseas contingency operations OCO); Iron Dome was interesting; the $1b for National Guard helicopters irked me (do they even see combat?); comments about Navy using civilian contractors on their ships; mostly not too interesting otherwise; Navy comments about fleet size of 306 per p165; got to page 253 which is research; on page pdf p349 I found votes on proposed amendments; final views on pdf-p376; appears to be disputes over to Rosa L. DeLauro's attempt to join the 2012 Labor-HHS bill with it similar to what   [was attempted in 2007 per Novak article](http://www.realclearpolitics.com/articles/2007/11/a_failed_congressional_ploy.html "http://www.realclearpolitics.com/articles/2007/11/a_failed_congressional_ploy.html"); this may come up under "structured rule"   [per TheHill](http://GOP mulls limiting Defense bill changes "http://GOP mulls limiting Defense bill changes") which is similar to closed rule;

> Glanced at S. 1429 with report 113-85 which is about $590b versus nearly $600b last year

> Difference between the two is $5b more in the House OCO, but I think that got struck out on the floor

> On 2 Aug I dug a little into the F-35 program. Bowles-Simpson (NCFCR 2010) recommended canceling it. which some are saying could cost $1t in O&M.   [Lies, Damn Lies, and the Trillion-Dollar F-35](http://www.airforcemag.com/MagazineArchive/Pages/2011/July%202011/0711edit.aspx "http://www.airforcemag.com/MagazineArchive/Pages/2011/July%202011/0711edit.aspx") is an interesting overview and notes it should replace A-10s, AV-8s, F-16s, and F-18 (but not F-22, the other 5th gen, whose combat   [effectiveness is criticized](http://www.businessinsider.com/f-22-wont-win-a-dogfight-thrust-vectoring-raptor-typhoon-eurofighter-2013-2 "http://www.businessinsider.com/f-22-wont-win-a-dogfight-thrust-vectoring-raptor-typhoon-eurofighter-2013-2")). According to  [wiki F-35 procurement section](http://en.wikipedia.org/wiki/F-35#Procurement_and_international_participation "http://en.wikipedia.org/wiki/F-35#Procurement_and_international_participation") dev costs were $40b and to purchase 2,000 is another $200b. Selling it abroad per   [Struggling in US, F-35 fighter pushes sales abroad](http://www.foxnews.com/politics/2012/01/27/struggling-in-us-f-35-fighter-pushes-sales-abroad/ "http://www.foxnews.com/politics/2012/01/27/struggling-in-us-f-35-fighter-pushes-sales-abroad/").

> In reading about a lot of the aircraft such as the Harrier (short-takeoff came out in 1969, replaced by the AV-8B Harrier), F-14 ("turkey" first flew in 1970 and retired in 2006), F-15 Eagle (first flight 1972, active, no losses in dogfights), F-16 Falcon (?), or the F/A-18 Hornet (first flight 1978) they were hardly used in really critical combat (see   [dogfights](http://en.wikipedia.org/wiki/Dogfight "http://en.wikipedia.org/wiki/Dogfight") for a list); retired   [fighter jets are thoroughly destroyed](http://www.impactlab.net/2007/07/02/this-is-what-happens-to-old-fighter-jets/ "http://www.impactlab.net/2007/07/02/this-is-what-happens-to-old-fighter-jets/") to avoid foreign countries from buying parts;  [commercial aircraft are sold or put in the desert](http://www.slate.com/articles/news_and_politics/explainer/2008/06/jumbo_jet_for_sale.html "http://www.slate.com/articles/news_and_politics/explainer/2008/06/jumbo_jet_for_sale.html"); lots of   [Russian military jets for sale](http://www.military-heat.com/27/military-jets-sale-civilian-market/ "http://www.military-heat.com/27/military-jets-sale-civilian-market/"); 

> Those cool spy planes F-117 were   [retired in 2008](http://usatoday30.usatoday.com/news/washington/2008-03-11-stealth_N.htm "http://usatoday30.usatoday.com/news/washington/2008-03-11-stealth_N.htm") (replaced by F-22).

> [Fleets fade away with Pentagon budget cuts](http://www.washingtontimes.com/news/2012/feb/5/fleets-fade-away-with-pentagon-budget-cuts/?page=all "http://www.washingtontimes.com/news/2012/feb/5/fleets-fade-away-with-pentagon-budget-cuts/?page=all") (2012) is a good WashPo overview

> [US Navy to Use Low-Emission Switchgrass Fuel to Power Fighter Jets](http://inhabitat.com/us-navy-to-use-low-emission-switchgrass-fuel-to-power-fighter-jets/ "http://inhabitat.com/us-navy-to-use-low-emission-switchgrass-fuel-to-power-fighter-jets/") (2013) sounds good

> [Budget cuts will ground fighter jets, Pentagon announces](http://www.csmonitor.com/USA/Latest-News-Wires/2013/0409/Budget-cuts-will-ground-fighter-jets-Pentagon-announces "http://www.csmonitor.com/USA/Latest-News-Wires/2013/0409/Budget-cuts-will-ground-fighter-jets-Pentagon-announces") - good news
