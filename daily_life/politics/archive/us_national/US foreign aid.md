# US foreign aid

## Military aid
see  [http://en.wikipedia.org/wiki/United_States_military_aid](http://en.wikipedia.org/wiki/United_States_military_aid)

Surprised to hear that Egypt got $1.3b per year in military aid, I found that it has been going back since the early 1980s but  [in 2012](http://www.nationaljournal.com/nationalsecurity/report-clinton-to-allow-egypt-aid-despite-human-rights-concerns-20120315) or so it was made contingent on human rights and democracy

Subcommittee on State, ForeignOperations, and Related Programs - see separate page on US foreign aid
> Read 113-081 on S.1372 and it covers military as well as economic aidat around $50b. Tracks the   [ US foreignaidwiki page](https://en.wikipedia.org/wiki/United_States_foreign_aid "https://en.wikipedia.org/wiki/United_States_foreign_aid"). USAID (Agency for Internatioanl Development) manages a few billion. There's some also some multilateral aid($5.5bn for UN and some for the IMF) as well as bilateral economic assistance aid, which is often done in exchange for favors as I found out in reading The Political Economy of Bilateral ForeignAid(2012) by Werker

> Glanced at 113-185 on HR 2855 which cuts 20% from 2013, cuts climage change stuff, $500m in disaster aid, narcotics and drug, foreignmilitary financing, prohibits funding UN Population Fund (roll call 2) - note that Young voted with Republicans 100%; see page 128 of pdf for minority views

Read  [Myths About Aidby Giving What We Can](http://www.givingwhatwecan.org/why-give/myths-about-aid#1) - quotes: "Rumsfeld, announced that the Defense Department was so bad at keeping track of its funds that there were $2.3 trillion that it could not account for" and "For the same amount of money as training a single guide dog, we could completely cure enough people of Trachoma-induced blindness to prevent a total of 2,600 years of blindness"

The Scandal That Is ForeignAid- Breitbart.com perspective, mentions that the inspector general has had some criticism

U.S. ForeignAssistance to Sub-Saharan Africa: The FY2012 Request  - CRS report, p22 shows a table of the expenditures, which on p16 shows $5.6 billion for HIV/AIDS under the Global Healthcare (GHCS) initiative; Governing Justly and Democratically has $370m; split among USAID, State (?), DA (Developmental Assistance), and Economic Support Fund (ESF)

Millennium Challenge Corporation - 2004 initiative

AIDMonitor.com - watchdog