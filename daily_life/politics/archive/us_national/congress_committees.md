Committees of Congress
===

## Committees

Most of the action happens here; also a lot of action happens in subcommitttees; can be found at https://law.lexisnexis.com/infopro/zimmermans/disp.aspx?z=1324 but mostly no notice

See http://www.opencongress.org/wiki/Tracking_Committee_Website_Transparency for complete metrics

2013 reorganization in House subcommittees split Energy and Environment, not sure what else was switched

### Assignment
CRS study at Committee assignment process in the US senate: Democratic and Republican party procedures; http://en.wikipedia.org/wiki/James_Traficant was the first in a long, long time to have no committee assignments

### Attendance
notice a lot of poor attendance at information hearings; this was highlighted in the 2012 McMahon v. Murphy campaign which found Murphy missed 75% of hearings, sometimes Senate wasn't in session, he points to making 97% of votes, defense by former U.S. Rep Rob Simmons per points out that attendance isn't always taken, there are sometimes conflicts in schedules, and most people multitask at these meetings;

## History
reviewed Partisanship and Power: House Committees and the Powers of the Speaker, 1789-1801 which says that parties arose from committees  as majorities completely excluded minorities from committees, Committee of the Whole is the full body but relaxed so there's more speaking; 1993 Joint Committee on the Organization of Congress may have had an impact (see http://democrats.rules.house.gov/archives/jcoc2e.htm);http://en.wikipedia.org/wiki/Robert_M._La_Follette,_Sr. is considered to be one of the most important senators

## Schedules
House monthly schedule at http://www.house.gov/legislative/ but probably inaccurate

Schedules for the day at http://housepressgallery.house.gov/committee-data/committee-schedules and http://www.senate.gov/pagelayout/committees/b_three_sections_with_teasers/committee_hearings.htm but no future plan

## Chairs and hearings
Transparency, minutes, and schedules? Most hearings are public, but how about votes to move out of committee?

See http://en.wikipedia.org/wiki/United_States_congressional_committee and http://en.wikipedia.org/wiki/United_States_congressional_hearing - however these pages don't say too much about the actual process

 6-year term limits set up by 1994 Contract with America reinstated in 2012; hearings often about spin per http://congress.indiana.edu/congressional-hearings-are-too-often-about-spin
 Committee chairs are free to set up the hearings any way they want, as long as a majority of committee members are willing to go along; also mentions statistics and says: "1960s and 1970s the average Congress had some 5,400 hearings; in the 1980s and 1990s, the average dropped slightly to 4,800. After that, though, the number plummeted; in the last Congress, it was 2,100" per Thomas Mann and Norman Ornstein; Lee H. Hamilton runs this Indiana center and is a big expert

can get hearings at http://www.gpo.gov/fdsys/browse/collection.action?collectionCode=CHRG

Reviewed http://www.loc.gov/law/about/faq-research.php#q3 which tells you were to find stuff

## Secrecy and reports
Committees - Secrecy and Delayed Transparency(http://freegovinfo.info/node/1405). Note that the Republicans began an initiative in 2012 for publishing committee meetings and a 72-hour rule summarized at http://www.opensecrets.org/news/2010/12/house-majority-leader-eric-cantor-pushes.html with Cantor's letter at http://bearingdrift.com/2010/11/04/eric-cantors-letter-to-incoming-congressional-republicans/;, so far so good with even appropriations committee hearings online in 2012!

Committee reports are shown with session #-iterating number; for example, I found 112-203 which is the report on the 2012 farm bill; sometimes these reports seem to hard to find

http://www.senate.gov/artandhistory/history/common/briefing/Committees.htm is an excellent overview; it says " In 1946, and again in the 1970s, the Senate moved to democratize committee procedures" - looks like these are based upon rules such as http://rules.senate.gov/public/index.cfm?p=RuleXXVI

http://help.lexisnexis.com/tabula-rasa/congressional/stage4_ref-reference?lbu=US&locale=en_US&audience=all is a pretty good overview

http://www.lexisnexis.com/help/CU/The_Legislative_Process/Stage_3.htm is a good source

## Electronic formats
ran into some trouble with .asx files; based on http://askubuntu.com/questions/88279/how-can-i-watch-asx-videos-with-firefox; open them with text and grab the stream then open that with VLC

## Personal history
I watched the July 27 House Oversight & Government Reform committee meeting at http://oversight.house.gov/markup/full-committee-business-meeting-30/; everything passed by unanimous consent; oversight plans are submitted Feb 15 to House Administration and Committee on Government Reform

http://thomas.loc.gov/video/house-committee/hsap/22979710 and noticed them talking about disclosing cost of mailers which failed and then couldn't get enough votes for a roll-call; http://en.wikipedia.org/wiki/Jack_Kingston is chair of the ag subcommittee and seems smart; http://en.wikipedia.org/wiki/Jeff_Flake stands up for saving money

### Senate
Senate & House Budget - see Functional Categories of the Federal Budget by CRS; supposed to finish their Congressional Budget Resolution by April 15 per http://www.ovaconline.org/pdfs/archives/budgetqa-la2004.pdf Budget & Appropriations Q&A QUESTIONS AND ANSWERS FOR GEORGETOWN BUDGET

Senate Energy & Natural Resources - Bingaman making way for Wyden; pals with Murkowski, ranking Republican; again, lots of action here

Senate Banking - confirms the regulators; Warren is on there; Sherrod Brown (D-Ohio) only dissenter on White (fella tied in 2011 as most liberal http://www.nationaljournal.com/congress/most-liberal-members-of-congress-20110226, also pushed for the http://en.wikipedia.org/wiki/Brown%E2%80%93Kaufman_amendment)

Senate Environment and Public Works - headed by Boxer; again, an interesting committee

Senate Commerce, Science and Transportation - https://www.commerce.senate.gov; mistitles a lot of its hearing as executive session (not secret, but executive business ie treaties and nominations?); sad; seems like a lot of action happens here;


### House
House Financial Services - watched Oversight Plan hearing markup 2012-1-14 on 2012-25 and noticed Jeb Hensarling (protege of Phil Gramm) and Ranking Member Maxine Waters http://financialservices.house.gov/calendar/eventsingle.aspx?EventID=319610;

House Appropriations - fiscal year is September PY - October CY; the Congressional Appropriations Process: An Introduction by CRS (2008) is a great introduction; watched the first full committee meeting of the 113th which did rules; bit of the 2013 CBO hearing and most of the GAO hearing; kinda interesting; GAO guy was sharp; http://en.wikipedia.org/wiki/Congressional_Budget_and_Impoundment_Control_Act_of_1974 302(a) allocations restrict the discretionary spending in total (for FY13, the 302(a) allocation is $1.028 trillion) and 302(b) allocations are specific to each subcommittee; Chairmans of subcommittees are called "Cardinals"; also watched the Legislative Branch appropriations full committee

House Ways and Means - sends "views and estimates" per 301(d); taxes and some mandatory stuff such as Social Security and Medicare; Dave Camp has taken it over from Rangel and was a member of Bowles-Simpson Commission; on 2013-03-03 I saw a hearing on tax reform and charitable giving which appeared to last 7 hours!?!?, by 3.5 hours it was on the 3rd panel and members were rotating out throughout the time;

House Energy and Commerce - one of the oldest standing committees; began 1795

House Science, Space, and Technology - Lamar Smith heads it up

Subcommittee on Environment - chair Andy Harris in 2013; new committee split from Energy; saw first committee hearing "The State of Environment: Evaluating Progress and Priorities" where he talked mainly about not regulating! Bernard Goldstein is the man the other two experts were terrible; Bonamici (D-Oregon) took over for David Wu and is in favor of health; newbie Chris Stewart is a consultant for the industry; Donna Edwards is a major environmental proponent; Randy Weber (R-Texas) took over for Ron Paul

Transcripts? http://banking.senate.gov/public/index.cfm?FuseAction=CommitteeInformation.FAQs says they will be posted to the website, but I don't see any

## Bugs
on the appropriations page, the RSS feed at http://www.appropriations.senate.gov/rss-news.cfm? doesn't seem to work