# Congress races

## Polling - see politics

**Schedules**

[ http://www.politics1.com/calendar.htm](http://www.politics1.com/calendar.htm "http://www.politics1.com/calendar.htm") - early November is election day ("the Tuesday after the first Monday of November")

**Demographics**

[Nations metropolitan areas continue to surge, while rural areas shrink](http://www.dailykos.com/story/2014/03/27/1287830/-Nation-s-metropolitan-areas-continue-to-surge-while-rural-areas-shrink "http://www.dailykos.com/story/2014/03/27/1287830/-Nation-s-metropolitan-areas-continue-to-surge-while-rural-areas-shrink") - this is the story of Congress

** House  **

Race, Party, and Contested Elections to the U.S. House of Representatives (2007) - scholarly paper

[Democratic Congressional Campaign Committee](http://ballotpedia.org/Democratic_Congressional_Campaign_Committee "http://ballotpedia.org/Democratic_Congressional_Campaign_Committee") - in charge of strategizing recruiting


**2014**
[Ballotpedia United States House of Representatives elections, 2014](http://ballotpedia.org/United_States_House_of_Representatives_elections,_2014 "http://ballotpedia.org/United_States_House_of_Representatives_elections,_2014") - partisan breakdown is good and shows that Democrats have super-safe districts (ie 70/30 or whatever) which means that those votes could be shifted off into another district to make it more competitive

Competitive races according to Ballotpedia above

_Arizona -_ looked at  Arizona's 1st, Kirkpatrick where Republican got hit with a spoiler by the Libertarian receiving 6% last race

_West Virginia_ \- Rahall in the 3rd is under fire, but the other two districts skew like 60% Republican and Obama lost in the area by 13%. Ouch.

_Virginia_ \- looked at 2nd where Suzanne Patrick is contesting and needs to make up 7% from last time! Endorsed by DCCC Jumpstart program and Emily's List

_Texas_ \- only the 23rd is contested, and held by Pete Gallego

[2014 \- Where Are We At With Democratic Challengers To The GOP In The House of Representatives?](http://www.dailykos.com/story/2013/10/12/1244531/-2014-Where-Are-We-At-With-Democratic-Challengers-To-The-GOP-In-The-House-of-Representatives# "http://www.dailykos.com/story/2013/10/12/1244531/-2014-Where-Are-We-At-With-Democratic-Challengers-To-The-GOP-In-The-House-of-Representatives#") - shows a surprising lack of challengers, but I suppose the races are pretty tight