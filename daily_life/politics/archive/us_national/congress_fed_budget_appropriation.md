Federal budget and appropriation
===

https://www.usgovernmentspending.com/year_spending_2019USbn_20bc6n_2030F0F1#usgs302

See Committees for details on specific appropriations

## yang math
https://freedom-dividend.com
* https://docs.google.com/spreadsheets/d/1obvYTGmhNgtTO0VmNeOm9tYNBsqD13b4TpVdc24b8x4/edit?usp=sharing

https://www.reddit.com/r/YangForPresidentHQ/comments/cdzqvf/i_did_the_math_and_ran_into_some_concerns_on_how/etxezz5/

## Authorization and appropriation

Overview of the Authorization-Appropriations Process (November 29, 2010 -  [RS20371](https://opencrs.com/document/RS20371/ ))

Typically authorization bills will have ceilings and may say "as needed"; e.g. Child Care Development Block Grand which I looked at in Sept 2013 was changed from $1b up to 2012 or so and proposed "as needed"

Auburn University  [definition of authorization bill](http://www.auburn.edu/~johnspm/gloss/authorization_bill): "about one-half of current Federal spending is by agencies or programs subject to annual re-authorization, while the other half gets its legal basis either from longer term authorization bills or from permanent laws that provide spending authority automatically to ongoing entitlement programs like Social Security"

## white house 
https://en.wikipedia.org/wiki/White_House_Office
* annual report to Congress, e.g. 420 in 2019, report was just a list of names

## Federal deficit and treasury term structure
see macro outlook as well

[Who Is Buying U.S. Treasuries?](http://www.ritholtz.com/blog/2011/08/who-is-buying-u-s-treasuries/) (2011) - seems mysterious

Go long, according to John Cochrane's  [Debt Maturity](http://johnhcochrane.blogspot.com/2012/11/debt-maturity.html) (2012)

[A Sobering Look At The Structure Of US Treasury Debt](http://www.businessinsider.com/a-sobering-look-at-the-structure-of-us-treasury-debt-2010-10) (2010) - cashflow picture of when debts mature

[The Maturity Structure of Treasury Debt: How Costly is Mismanagement?](http://econjournal.sites.yale.edu/articles/2/maturity-structure-treasury-debt-how-costly-mismanagement) (2013) - skimmed