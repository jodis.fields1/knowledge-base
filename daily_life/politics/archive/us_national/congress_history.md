# Gov history

## Early history

Sex in the Senate: Bobby Baker's salacious secret history of Capitol Hill (2013) - article on the unwritten history

** 113th **
**_2nd session_ **

Got off with a bang with a budget appropriations bill and a farm bill (!), followed by flood insurance, kids research, etc

Noticed 5-24 a few capital market bills pass out of committee on party lines; these were discussed in Hearing entitled “ [Legislative Proposals to Enhance Capital Formation for Small and Emerging Growth Companies](http://financialservices.house.gov/calendar/eventsingle.aspx?EventID=375104 "http://financialservices.house.gov/calendar/eventsingle.aspx?EventID=375104")” where I read Coffee's critique and Hearing entitled “ [Legislative Proposals to Enhance Capital Formation for Small and Emerging Growth Companies, Part II](http://financialservices.house.gov/calendar/eventsingle.aspx?EventID=377434 "http://financialservices.house.gov/calendar/eventsingle.aspx?EventID=377434")" where I read Beatty from NASAA critique and the issues FundRise had with Regulation A offerings

**_1st session_ **
_General_

House GOP seeks welfare reform 2.0 - overview of the GOP conflict between Cantor and Ryan, where Cantor added the work requirement for mothers that even Ryan voted against and Cantor is working the SKILLS Act; Cantor also opposed cutting crop subsidies which Ryan wanted to cut; "biggest single change is a proposal to permanently do away with waivers that have allowed millions of able-bodied adults without dependents to get food stamps for more than three months — even if they are working less than 20 hours a week"

_Regulatory rules_

EPA close to tweaking pesticide rule, see Farmworker advocates press EPA to update pesticide rules

_SEACC_

H.R. 2715: Biomass Thermal Utilization Act - ??

H.R. 740: Southeast Alaska Native Land - waiting for the written report to be posted

S. 340: Southeast Alaska Native Land - waiting for written report

_Bills evaluated_
H. 3547 Consolidated Appropriations - biggest bill of the year by far was pushed thru in 3 days and  [highly disapproved by public per popvox](https://www.popvox.com/bills/us/113/hr3547/report#nation "https://www.popvox.com/bills/us/113/hr3547/report#nation"), read a few summaries such as Citizens Against Government Waste at  [Earmarks Make an Appearance in 2014 Omnibus Appropriations](http://finance.yahoo.com/news/earmarks-appearance-2014-omnibus-appropriations-202800598.html "http://finance.yahoo.com/news/earmarks-appearance-2014-omnibus-appropriations-202800598.html"),  [GOP summary at gop.gov](http://H.R. 3547 - House Amendment "http://H.R. 3547 - House Amendment"), good overview at  [theweekincongress summary](http://theweekincongress.com/2014/01/16/hr-3547-consolidated-appropriations-act-2014/ "http://theweekincongress.com/2014/01/16/hr-3547-consolidated-appropriations-act-2014/"), also see the appropriation committee summaries ( [example house](http://appropriations.house.gov/news/documentsingle.aspx?DocumentID=366721 "http://appropriations.house.gov/news/documentsingle.aspx?DocumentID=366721")) and wiki where I noted the joint explanatory report (not sure where that will end up ultimately)

Several of the appropriations bills (see federal government):

> include HR2397 (defense, with controversial NSA amdt), S1371 (govt&financial), S1372 (foreign aid), S1245 (energy and water, didn't like the hydrogen fuel cell stuff; did not review house equivalent HR2609), S1244 (agriculture) but did not review S1243 (transport and HUD or HR2610), 

H.Res. 441 amending HR 3304 - NDAA for 2014; disgusted by how nontransparent this was done, has some sexual protection provisions per House caps year with $607B defense bill | TheHill

Bipartisan Budget Act of 2013 (H.J.Res 59) - annoyed that they replaced the previous CR with this bill

HR267 Hydropower Regulatory Efficiency Act of 2013 - relaxes the FERC rules by: (1) increasing eligiblity for exemption from 5MW to 10MW, (2) adds conduit exemptions with related upping of another exemption from 15MW to 40MW; (3) pilot project report on non-hydro dams. Senate report mentions 100k megawatts and a potential to add 65k more given that only 3 percent of dams have hydropower, Murkowski sponsored the Senate version, 

HR678 Bureau of Reclamation Small Conduit Hydropower Development and Rural Jobs Act - allows for hydropower in Bureau of Reclamation man-made conduits (canals), but the House version waived the NEPA requirement which was controversial in H. Rept. 113-24; however this was fixed on the floor to use a categorical exclusion and later the Senate report clariifed that the categorical exclusion process of NEPA allows for extraordinary circumstances to trigger review

HR240 Homeowners Protection Act - glanced at it quickly and noticed it has reinsurance provisions, ironically opposed by a few people in the south per Popvox; no committee report as of 08-2013

HR527 Responisible Helium- glancing at the committee report appears influenced by a 2010 NAS report which is good; The unbearable lightness of heliumis a good overview of the broader issue; Ralph Scurlock and Art Francis say we have plenty in reality altho it could be harder and harder to get; not yet passed the Senate; /. thread Scientists Speak Out Against Wasting HeliumIn Balloons discusses using hydrogen in balloons; Usage of heliumin MRIs (physics SE) is sophisticated and mentions recycling MRI machines already available tho not in wide use; Why Is There a HeliumShortage? mentions 10,000 liters per MRI and 2,000 remaining; note that heliumis the second most common element; Helium stocks run low – and party balloons are to blame says balloons should be $100 but says there's 22 grams per cubic meter on the moon;

HR2642 Federal Agriculture Reform - controversial split of the farm bill from food stamps

HR1911 Bipartisan Student Loan Certainty Act - the winner on the student loan bills and sets using the following per the committee report: interest rates would be set using the following formula: Stafford loans (subsidized and unsubsidized): 10-year Treasury Note plus 2.5 percent, capped at 8.5 percent;PLUS loans (graduate and parent): 10-year Treasury Note plus 4.5 percent, capped at 10.5 percent; report also notes later that "borrowers can also choose to maintain a variable interest rate or lock in their interest rate for the life of the loan by taking out a consolidation loan"; historically rates were 6.4% or so until they went down to about 3.5% in 2012; current rate would be around 4.4%

Online sales tax approved by Senate but not House yet

HR 2397 defense appropriations - structured rule with 100 amendments and limited on Egypt, Syria, and NSA. I went thru the  [list of amendments](http://beta.congress.gov/bill/113th-congress/house-bill/2397/amendments/?pageSort=oldestToNewest "http://beta.congress.gov/bill/113th-congress/house-bill/2397/amendments/?pageSort=oldestToNewest") - interesting ones include  [H.Amdt.367 to H.R.2397](http://beta.congress.gov/amendment/113th-congress/house-amendment/367 "http://beta.congress.gov/amendment/113th-congress/house-amendment/367") (failed Pakistan $600m cut),  [H.Amdt.371 to H.R.2397](http://beta.congress.gov/amendment/113th-congress/house-amendment/371 "http://beta.congress.gov/amendment/113th-congress/house-amendment/371") (failed Afghan infra by $300m),  [H.Amdt.373 to H.R.2397 ](http://beta.congress.gov/amendment/113th-congress/house-amendment/373 "http://beta.congress.gov/amendment/113th-congress/house-amendment/373")(agreed reduction of Afghan security by half a billion),  [H.Amdt.374 to H.R.2397](http://beta.congress.gov/amendment/113th-congress/house-amendment/374 "http://beta.congress.gov/amendment/113th-congress/house-amendment/374") (failed reduction by $2.6b of Afghan funding),  [H.Amdt.383 to H.R.2397](http://beta.congress.gov/amendment/113th-congress/house-amendment/383 "http://beta.congress.gov/amendment/113th-congress/house-amendment/383") (failed 1% reduction overall, Young against),  [H.Amdt.392 to H.R.2397](http://beta.congress.gov/amendment/113th-congress/house-amendment/392 "http://beta.congress.gov/amendment/113th-congress/house-amendment/392") (agreed $3b reduction). The excluded rule barring any aid to Syria was controversial per  [House to vote on NSA data collection](http://www.politico.com/story/2013/07/transportation-housing-bill-senate-94644_Page2.html "http://www.politico.com/story/2013/07/transportation-housing-bill-senate-94644_Page2.html") kll

**112th**
Moving Ahead for Progress in the 21st Century Act (MAP-21) \- major previous law
