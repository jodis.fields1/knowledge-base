Federal administrative government and regulation
===

2019-07-22: revisited as I reviewed ONC regulations (RIN: 0955-AA01); protip: regulations text are at the bottom and use https://www.federalregister.gov/

## FOIA
See  [FOIAMachine](http://foiamachine.org "http://foiamachine.org")

https://foiaonline.regulations.gov - some agencies are already doing it

## Budget
See Congress and legislators for a lot more information on this; also see Introduction to the Federal Budget Process from CRS

## GAO
Publishes a high-risk report every 2 years at http://www.gao.gov/highrisk/

## Records and stuff
CRS report[Retaining and Preserving Federal Records in a Digital Environment: Background and Issues for Congress](http://www.fas.org/sgp/crs/misc/R43165.pdf "http://www.fas.org/sgp/crs/misc/R43165.pdf") notes that GAO found 45% of agencies are at risk of losing information **

### Report
See https://web.archive.org/web/20130321161708/https://www.llsdc.org/Fed-agency-reports/ for a good overview

## Periodicals
Governing Magazine is a notable one; now owned by e.Republic which has Scientology affiliations

Federal Funds Information for States: username: alaska password: alaska79!

## Background
Read about http://en.wikipedia.org/wiki/Impoundment_of_appropriated_funds which was available and is no longer feasible

http://en.wikipedia.org/wiki/Federalism_in_the_United_States is interesting; in the 1970s "new federalism" emerged

http://en.wikipedia.org/wiki/Independent_Counsel_Act was passed in response to Nixon dismissing the employees investigating him; Congress needs to hire its own investigators

Grants.gov, as of 2012-04, was a mess with outdated grants lying open

http://www.legistorm.com/pro/pricing.html has transparency information but it is pricey; maplight.org is free and also cool

Government Transparency and Secrecy: An Examination of Meaning and Its Use in the Executive Branch - CRS report which is really just federal focused; no Congress included

## Admininistrative agencies
Government Performance and Results Act - 1993 law 

## Cabinet + independent agencies
http://en.wikipedia.org/wiki/Independent_agencies_of_the_United_States_government - technically independent agencies less influenced by President; not sure in practice for example EPA term runs in-line with the President

Appointments: hard to get Congress to approve appointments so recess appts are common; however they were struck down by DC Circuit Court http://thehill.com/blogs/congress-blog/labor/283313-nlrb-isnt-dead-yet-but-gop-trying-to-kill-it

## DC Circuit 
admin agencies reviewed here; famously a bit Republican, there's an analysis at  [Is the D.C. Circuit a “Broken Circuit”](http://www.volokh.com/2013/08/18/is-the-d-c-circuit-a-broken-circuit/)?

Heard at 2013 Alaska Administrative Regulation committee review Sam Batkins mentions Indiana lookback; doesn't mention the United States see http://www.whitehouse.gov/blog/2012/08/07/smarter-approach-regulation

Citizen Cosponsor Project - https://www.cosponsor.gov/ by the Republicans

Performance.gov | Performance.gov - Obama page

## Appropriations committee reports
- first reviewed appropriations  [CRS report 97-684](http://www.senate.gov/reference/resources/pdf/97-684.pdf); fine details from 12 synced (since 110th Congress) subcommittes; OMB  [lists appropriations](http://www.whitehouse.gov/omb/113/legislative_sap_apps_2013);  [2012 RollCall summary of changes](http://www.rollcall.com/news/labor_hhs_t_hud_bills_to_bear_brunt_of_spending_cuts-214035-1.html); 

### Subcommittee on Energy and Water
reviewed S. 1245 on 2013-06; committee goes line-by-line; 

> Title I Army Corp of Engineers: most at $2.5b is in Operations & Maintenance (includes recreation) and Construction at $1.35b; 
> Title II Bureau of Reclamation: $1b for reservoir management
> Title III Department of Energy: around $2b; $100m for hydrogen is a waste; $245m for bioenergy incl $30m for algae; solar $310m; wind $110m; geothermal $60m; water power $59m; vehicle $415m;  building $224m; mention of television set-top boxes having a high vampire load and suggests Energy Star 4 specs (reviewed  [CNET on PS3 about standby v. off](http://news.cnet.com/8301-17938_105-10257956-1.html) and  [2012 report by Hittinger et al of Carnegie](http://news.cnet.com/8301-11386_3-57416152-76/gaming-consoles-consume-copious-amounts-of-energy/)); $215m for advanced manufacturing; $130m for weatherization; $150m for electricity delivery; $735m for nuclear energy which mentions recent Blue Ribbon commission; $57m for advanced fuels; $440m for fossil fuel research; around $200m for energy reserves; Energy Information Admin (EIA) gets $120m; $230m for env cleanup; then a whopper of $5b for the Office of Science, incl $1.8b in Basic Energy Sciences, $630m split between biological and climate, $500m for advanced computing, $800m for high energy physics and $570m for nuclear physics, $450m for fusion energy; $379 for ARPA-E or advanced research products energy; loan guarantees; finally Departmental Administration at $126m and $42m for the inspector general; $11b for NNSA to stop nuclear proliferation; Power Marketing Administration has scant discussion; page 120 or so of the pdf has tables; 
> Title IV Independent Agencies - Nuclear Regulatory Commission plus little regional agencies includes Denali Commission and Alaska Natural Gas Transportation project; yea and nay votes shown on page 136; 
> On the house side,  [House slogs through barrage of amendments to energy and water bill](http://www.eenews.net/stories/1059984121) from E&E Publishing (Energy and Environment) describes this as not optimistic with "Office of Energy Efficiency and Renewable Energy and the Office of Electricity Delivery and Energy Reliability, while cutting their combined budget by more than half. And the Advanced Research Projects Agency-Energy would lose more than 80 percent of its budget"

### Subcommittee for Agriculture, Rural Development
reviewed S. 1244; first had to get some background on how this relates to the farm bill so I read  [CRS 7-5700 Budget Issues Shaping a Farm Bill in 2013](http://www.fas.org/sgp/crs/misc/R42484.pdf) and found that the farm bill sets a baseline of mandatory funding at around $100b per year and appropriations bill adds around $40b to it; includes food safety and inspection and FDA; votes on p89-90 show a few nays
> Title I USDA - $1.1b for Agricultural Research Service (no line details) and $770m (line details, half for classical breeding) for National Institute of Food Research, which  [Heritage recommends merging](http://www.heritage.org/research/reports/2013/06/how-to-slash-billions-from-the-agriculture-appropriations-bill); 
> Title VI FDA - funded at $4.3b total with a bit less than half user fees; food regulation at $800m; drugs at around $450m; biologics $200m; animal drugs $132m; medical devices $300m; other about $300m; 

### Subcommittee on Transportation, Housing and Urban Development
reviewed S. 1243; somewhat less interested in this so I'm skipping it although at $55b it is a huge section

### Subcommitte on Defense 
> read up on the House 2013 appropations report 113-113 ($600b total including "scorekeeping adjustments" and overseas contingency operations OCO); Iron Dome was interesting; the $1b for National Guard helicopters irked me (do they even see combat?); comments about Navy using civilian contractors on their ships; mostly not too interesting otherwise; Navy comments about fleet size of 306 per p165; got to page 253 which is research; on page pdf p349 I found votes on proposed amendments; final views on pdf-p376; appears to be disputes over to Rosa L. DeLauro's attempt to join the 2012 Labor-HHS bill with it similar to what  [was attempted in 2007 per Novak article](http://www.realclearpolitics.com/articles/2007/11/a_failed_congressional_ploy.html); this may come up under "structured rule"  [per TheHill](http://GOP%20mulls%20limiting%20Defense%20bill%20changes) which is similar to closed rule;
> Glanced at S. 1429 with report 113-85 which is about $590b versus nearly $600b last year
> Difference between the two is $5b more in the House OCO, but I think that got struck out on the floor
> On 2 Aug I dug a little into the F-35 program. Bowles-Simpson (NCFCR 2010) recommended canceling it. which some are saying could cost $1t in O&M.  [Lies, Damn Lies, and the Trillion-Dollar F-35](http://www.airforcemag.com/MagazineArchive/Pages/2011/July%202011/0711edit.aspx) is an interesting overview and notes it should replace A-10s, AV-8s, F-16s, and F-18 (but not F-22, the other 5th gen, whose combat  [effectiveness is criticized](http://www.businessinsider.com/f-22-wont-win-a-dogfight-thrust-vectoring-raptor-typhoon-eurofighter-2013-2)). According to  [wiki F-35 procurement section](http://en.wikipedia.org/wiki/F-35#Procurement_and_international_participation) dev costs were $40b and to purchase 2,000 is another $200b. Selling it abroad per  [Struggling in US, F-35 fighter pushes sales abroad](http://www.foxnews.com/politics/2012/01/27/struggling-in-us-f-35-fighter-pushes-sales-abroad/).

> In reading about a lot of the aircraft such as the Harrier (short-takeoff came out in 1969, replaced by the AV-8B Harrier), F-14 ("turkey" first flew in 1970 and retired in 2006), F-15 Eagle (first flight 1972, active, no losses in dogfights), F-16 Falcon (?), or the F/A-18 Hornet (first flight 1978) they were hardly used in really critical combat (see  [dogfights](http://en.wikipedia.org/wiki/Dogfight "http://en.wikipedia.org/wiki/Dogfight") for a list); retired  [fighter jets are thoroughly destroyed](http://www.impactlab.net/2007/07/02/this-is-what-happens-to-old-fighter-jets/) to avoid foreign countries from buying parts;  [commercial aircraft are sold or put in the desert](http://www.slate.com/articles/news_and_politics/explainer/2008/06/jumbo_jet_for_sale.html); lots of  [Russian military jets for sale](http://www.military-heat.com/27/military-jets-sale-civilian-market/); 
> Those cool spy planes F-117 were  [retired in 2008](http://usatoday30.usatoday.com/news/washington/2008-03-11-stealth_N.htm) (replaced by F-22).
> [Fleets fade away with Pentagon budget cuts](http://www.washingtontimes.com/news/2012/feb/5/fleets-fade-away-with-pentagon-budget-cuts/?page=all) (2012) is a good WashPo overview
> [US Navy to Use Low-Emission Switchgrass Fuel to Power Fighter Jets](http://inhabitat.com/us-navy-to-use-low-emission-switchgrass-fuel-to-power-fighter-jets/) (2013) sounds good
> [Budget cuts will ground fighter jets, Pentagon announces](http://www.csmonitor.com/USA/Latest-News-Wires/2013/0409/Budget-cuts-will-ground-fighter-jets-Pentagon-announces) - good news

### Subcommittee on State, Foreign Operations, and Related Programs
 see separate page on US foreign aid

### Subcommittee on Financial Services and General Government
Read 113-080 on S. 1371 with a $44b package roughly split between mandatory (including $11b in federal employee benefits and another $9b for other retirement stuff) and discretionary. Nearly $10b is for GSA, and an additional $1.5b for the SEC. The IRS also takes up a few billion with $2.5b in services and $5.5b in enforcement. Budget for the Supreme Court and a few other courts is included as well as district judges and new district judges authorized at around around $6b. Also Office of the President is about $50m.

> Noticed in Title VI general provisions it says "Section 621 is a new provision related to electronic filing of campaign finance reports by Senators and candidates seeking election to the Senate" which upon reading 2 USC 432.621 seems to remove the requirement to make these available like 2 U.S.C. 438(a)(4) and (5) which say "available for public inspection, and copying, at the expense of the person requesting such copying" (see p141 of the report)

Subcommittee on Commerce, Justice, Science, and Related Agencies:
> Read 113-171 for HR2787 and it shows $48b among 5 titles which is a 5.6% cut from 2013
> PTO gets $3b and NOAA $5b; DOJ gets $27b including $8b for FBI, $7b for prisons, and $2b for DEA; $16.6b for NASA; $7b for NSF; around $800m for Legal Services Corp + EEOC; 

## Regulations
Independent agencies are exempt from some requirements, but Executive Order 13579 still required them to do a retrospective review of regs and publish a public plan within 120 days, SEC still hasn't gotten around to it (The SEC Overhaul has a bunch of links); 

OIRA watcher:  [CPR’s Eye on OIRA](http://www.progressivereform.org/eyeonoira.cfm)

2013 Unified Agenda coverage: note that the publication is really just a table of RIN items (use dropdown); the preamble is often very short; long-term actions is something different; starting from Leland's mention I read  [The Hill coverage](http://thehill.com/blogs/regwatch/pending-regs/309257-administration-releases-belated-regulatory-roadmap-) (noting that a rule about silica exposure of construction workers has been stalled for decades tho really became proposed in 2011) and  [foreffectivegov.org coverage](http://www.foreffectivegov.org/presidents-spring-agenda-signals-continued-delays-new-rules) criticizing OIRA's delay from an environmental slant

2013 Fall Unified Agenda Regulatory Plan: reviewed most of these around 2013-12-01 (on the plane back to Juneau) and found they varied enormously, with HUD and DOE talking only about energy efficiency, Dept of Ag having a huge list, Commerce focusing on NOAA which briefly mentioned its 70 endangered species of 1,310 in US

See http://docketwrench.sunlightfoundation.com for how to deal with duplicate comments:sat down to look at some of the comments on EPA-HQ-OAR-2011-0660 and found 2400+ comments - but all the ones I looked at were just copies of a Sierra Club form letter! Lots of literature on this problem, reviewed in Kumar & Govindarajulu 2009's Duplicate and Near Duplicate Documents Detection: A Review as well as The Case Against Mass E-mails: Perverse Incentives and Low Quality Public Participation in U.S. Federal Rulemaking

Free full-text article is Information technology and public commenting on agency regulations http://onlinelibrary.wiley.com/doi/10.1111/j.1748-5991.2007.00005.x/full

DURIAN was a tool that allows one to sift through similar comments; huiyang@cs.georgetown.edu is the creator; http://erulemaking.cs.cmu.edu/ is a research center

http://reporting.sunlightfoundation.com/2012/Most_regulations_get_little_attention/ shows number of comments; could be improved by culling out duplicates

2013-02-21: spent a while searching around for the essential health benefits final rule (FR Document: 2013-04084) and couldn't find it on the HHS website or regulations.gov - turns out it was published in the FR (duh!) and signed up for that RSS feed; also signed up for just the significant ones per EO 12866 which is described at http://www.foreffectivegov.org/node/224 (which says the principles do not apply to independent agencies like the CPSC, altho the planning does); note there is no special difference between Special Filing and Regular filing just different times of publication; also look at the Unified Agenda which is produced semiannually (twice a year) by OIRA; June 1st is one deadline for agencies to send their plan along; OIRA has 90 days to look at it; however in 2012 the Spring agenda didn't make it and the Fall agenda was published in 12/21/2012! per http://www.openmarket.org/2012/10/10/where-is-obamas-unified-agenda-of-federal-regulations/ which also points to the Report to Congress on the Benefits and Costs of Federal Regulations http://www.whitehouse.gov/omb/inforeg_regpol_reports_congress; https://www.law.upenn.edu/blogs/regblog/2013/01/30-batkins-unified-agenda.html is coverage for the 2012 version;

2013-06: found out that actually the 2011 Report to Congress on the Benefits and Costs of Federal Regulations and Unfunded Mandates on State, Local, and Tribal Entities actually lists all major rules; feds released White House Unveils First-Ever Inventory of Federal Programs - Management - GovExec.com, and glanced at Red Tape: Rising Cost of Government Regulation by the Heritage Foundation

Proposed REINS act would require Congress to approve regulations, gutting the power; see http://www.volokh.com/2013/04/16/evaluating-the-reins-act/ for links to a couple law review articles I read by Adler (supporter) and Jonathan Siegel (nonsupporter) in http://www.nyujlpp.org/issues/volume-16-issue-1/ of NYU Journal of Legislation and Public Policy

## Research strategies

http://www.reginfo.gov/public/ has a lot on the pending regulations!

http://www.llsdc.org/fed-reg-cfr/

_Statutes and the U.S. Code_
Regulations derive from statutes; http://gov20.govfresh.com/visualizing-the-size-of-the-united-states-code/ gives you an idea of what's going on there; perhaps more interesting is https://github.com/unitedstates which has the uscode

_Docket Numbers and RINs_
http://exchange.regulations.gov/exchange/node/574 and http://westreferenceattorneys.com/2011/04/anatomy-of-a-rin/ and http://westreferenceattorneys.com/2011/04/more-on-rins/ and https://www.federalregister.gov/blog/2011/04/regulation-identifier-numbers

Began April 2010 with an email from Sunstein; should be used in all documents related to a proposed rule per Executive Order 12866, Sec. 4(b)

http://www.reginfo.gov/public/jsp/eAgenda/StaticContent/201110/Preamble_8888.html - thought this might discuss RINs but it doesn't seem to

Sent an email about why a lot of regulations.gov regulations don't have RINs; either notices or prior to when they were required (sometime in 2011?).

Rules can be implemented either as interim rules which bypass public comment somewhat or go through the "proposed rule" stage

_Reform initiatives_

http://governmentreform.ideascale.com/a/dtd/Abolish-the-American-Community-Survey--Census-Bureau-/128786-13060 was run by OMB for a while