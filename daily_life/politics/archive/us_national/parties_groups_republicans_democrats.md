# Parties or groups (Republicans, Democrats, etc)

**   
**

  


**Swing states and districts after redistricting**

[2012 WashPo analysis](http://www.washingtonpost.com/blogs/the-fix/post/the-10-states-that-will-determine-control-of-the-house-in-2012/2011/11/18/gIQAXZYCZN_blog.html10 "http://www.washingtonpost.com/blogs/the-fix/post/the-10-states-that-will-determine-control-of-the-house-in-2012/2011/11/18/gIQAXZYCZN_blog.html10") points to Ohio, Pennsylvania, NH, NC, Colorado, Arizona (?), New York (?), Illinois (?), California (historically an afterthought with just 1 seat changing hands 2002-2010) and Florida

No strength in numbers - Gerald Gamm finds that urban legislators do not dominate

  


**Republicans  - s ee local->Alaska legislature for local notes **

  


While there are many articles which have a surface-level description of the convention, [Point of Order! How an Open GOP Convention Would Work](http://abcnews.go.com/Politics/OTUS/open-gop-convention-mitt-romney-delegates-nomination/story?id=16064518&singlePage=true#.T48IGdmW9pg) appears relatively detailed; essentially after the first round the convention becomes more and more open with delegates more and more free to vote as they please

  


http://en.wikipedia.org/wiki/Bradley_Foundation is a major funder, as are the Koch brothers

  


_Privatizing and modifying state law_  - Center for Media and Democracy has been hitting this hard; from jmc7337 on twitter

State Policy Network - ??

ALEC - legislative model laws

Donors Trust - not really sure what this is about

  


  


**Democrats**

?

  


**Libertarians**

Albert Kokesh is interesting and gives an idea of how much libs depart from Republicans (also see Reason magazine discussions); http://en.wikipedia.org/wiki/Adam_Kokesh read

  


  


Polling  - duplicated on Politics note

  


  


**Voters**

See  <https://en.wikipedia.org/wiki/Political_party_strength_in_U.S._states>

Look at presidential elections to get an idea, but it turns out there's a lot of cases where statewide elections (senators especially) don't follow presidential (e.g. Democrats in Lousiana, North Dakota, Montana, Alaska)

Working class voters: why America's poor are willing to vote Republican (2012) - The Guardian's analysis; most poor vote Democrat; poor turnout and ideology affect it heavily tho

  


[Dem advantage among young voters returns to pre-2008 levels](http://www.dailykos.com/story/2010/02/26/840290/-Dem-advantage-among-young-voters-returns-to-pre-2008-levels "http://www.dailykos.com/story/2010/02/26/840290/-Dem-advantage-among-young-voters-returns-to-pre-2008-levels") (2010) - how persistent? see Political Attitudes Over the Life Span: The Bennington Women After Fifty Years and note that on page 13 they summarize the supposition that people become more conservative over age, which has mixed evidence

_  
_

**Independents**

  


_Tea Party_ \- wiki says it has slipped from 1,000 to 600, note that there are different groups within it with some who are quite fringe such as Dale Robertson's teaparty.org (for sale since he went bankrupt)

_Greens - ?_

  


**Psychology**

See Study Predicts Political Beliefs With 83 Percent Accuracy which discusses how Republicans have more of a flight-or-fight response and more family and country social connection

Research shows link between states' personalities and their politics (2014) - conscientiousness linked to Republicans is odd (?)

  


**"Elites"**

See conspiracy theory notes for more details

  


_Bilderberg_ \- see http://en.wikipedia.org/wiki/Talk:Bilderberg_Group/Archive_4#Getting_beyond_the_internet_information for some sources

The Global Corporate Elite and the Transnational Policy-Planning Network, 1996-2006 - potentially good article
