Court documents
===

With focus on class actions

Todo: 
* See SEC for Rakoff challenging a settlement

Also found Judicial SettlementDatabases: Development and Uses (attached) which is in my Google Drive

Judicial Settlement Databases: Development and Uses and also various Jury Verdict and Settlement Westlaw databases exist

## Court
Websites:

https://www.oyez.org/cases/2018

<https://free.law/>

http://www.topclassactions.com/ - not too great

http://www.freecourtdockets.com/ - signed up for this; however it charges for the stuff

https://www.inforuptcy.com/ - bankruptcy focus

http://archive.recapthelaw.org/

## PACER

64 Federal Courts Now Publish Opinions on FDsys - includes district court orders! interesting  

Note: if you search for a random PACER case, make sure you are in the right court website! however you can straight to the case if you know the PACER number per https://ecf.ilnd.uscourts.gov/cgi-bin/DktRpt.pl?250791

## Recap

https://citp.princeton.edu/people/students/ has some of the people working on this e.g. Harlan Yu

https://sites.google.com/site/citpgsoc/ has some info but mainly outdated

Cases:

Looked at a student loan recovery - no mercy!

Looked at a student loan bankruptcy - 13-00123 Kristeen Aunna-May Bicchinella - probably no luck
