# Legal entities

History of the Law of Business Corporations before 1800 by Samuel Williston - from 18888 but still interesting, and free over at JSTOR<http://www.jstor.org/stable/info/1322201>

## Trust (ASEA focus)  

Note that trusts typically file in court, not at the Secretary of State or Corporations Licensing office

The trust is a 501(c)(9) — Voluntary Employee Beneficiary Associations (VEBA)

Employee Benefit Plans - Parties in Interest and Prohibited Transactions: primer from AICPA dated April 2011.

https://web.archive.org/web/20130825035343/https://www.employerlawreport.com/2011/08/articles/employee-benefitserisa/the-fiduciary-exception-to-the-attorneyclient-privilege-document-everything-is-a-best-practice-except-when-it-isnt/ from PorterWright is a good resource

It's Common, but Is It Right? The Common Law of Trusts in ERISA Fiduciary Litigation. Michael J. Collins. The Labor Lawyer. Vol. 16, No. 3 (Winter/Spring 2001), pp. 391-438

## Unincorporated versus incorporated associations

Note that the trust is in a sense the most generic form of an association, as noted with some of the very bare-bones trust statutes on the east coast

Work on <http://en.wikipedia.org/wiki/Companies_law> and <http://en.wikipedia.org/wiki/Voluntary_association> or more generally<http://en.wikipedia.org/wiki/Legal_personality> and <http://en.wikipedia.org/wiki/Person>;

### Incorporation

After reading my insurance law textbook, I looked into this - major difference, as noted here <http://info.legalzoom.com/difference-between-unincorporated-association-corporation-23751.html>, is that corporations are "incorporated" meaning registered with the State and have to get certificate of authority from other states to operate there; runaway corporation statutes and long-arm statutes can also affect these corporations

http://en.wikipedia.org/wiki/Corporate_law#History is quite fascinating altho does not mention New York's earlier limited liability law in 1811; read Limited Liability In Historical Perspective <http://scholarlycommons.law.wlu.edu/wlulr/vol54/iss2/10/> for more background which discusses the Italian commenda and the French limited partnerships followed by an Irish law and then a limited partnership act in New York 1822;

### Unincorporated
http://www.sbnonline.com/2007/11/unincorporated-organizations-take-care-when-doing-business-without-a-legal-structure/  describes these as contracts amongst each other; the association "may" file service of process information; for-profit are often treated as partnerships; going after personal assets is more difficult so you can ask the person to be personally liable; looked at California Law Revision Commission Recommendation (2003) which does not recommend adopting the 1996 Uniform Unincorporated Nonprofit Association Act; California law allows for-profit unincorporated associations and allows that the association "may" file statements with a county; http://nonprofit.about.com/od/faqsthebasics/f/unincorporationassociation.htm says under $5,000 in revenue it is tax-exempt and technically could file under 501(c)(3) or others but might as well just incorporate

Until 1992 the American Bar Association (ABA) was in this category per North Carolina Has Rewritten Its Warpy Law of Unincorporated Associations http://www.brookspierce.com/news-publications-24.html but it adopted the 1996 uniform law;
