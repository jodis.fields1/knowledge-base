Legal research
===

Note: Lawyers is another good note plus there's Alaska legal research and more

## Recent thoughts
FreeLawProject.org - by Michael Jay Lissner, looks pretty organized and active as of 2014-11

## Online or self-help
Nolo is famous. Justia.com is the modern best source. Altlaw.com is a legal search engine. Avvo.com allows you to ask lawyers questions.

[Is LegalZoom's Gain Your Loss](http://www.callawyer.com/clstory.cfm?eid=911404&ref=updates) has a history and update on LegalZoom

Google, Westlaw, LexisNexis and Open Access: How the Demand for Free Legal Research Will Change the Legal Profession seems overly optimistic

Alaska's Ten Years of Electronic Reporting - Alaska broke ground on this!

## Laws, cases, and statutes

### Statutes and regs
https://simplifiedcodes.com/?page_id=20 - simplified version of laws by law professor!

https://www.govinfo.gov/help/uscode - more of an API?

https://law.cornell.edu - good too
justia.com - seems good, but dated w/ 2017 version of US Code
https://www.public.law/ - has potential; blog in my feed (formerly weblaws.org)
www.lawserver.com/ - has Q&A section
https://codes.findlaw.com/ - annoying as it pushes you to open in westlaw

http://law.onecle.com/ - also good

[casetext.com](https://casetext.com/) - annotations

plainsite.org - has potential, run by a nonprofit (no assets/revenue), but seems somewhat dead, founded by Aaron Greenspan out of Silicon Valley

### Cases

Google Scholar is best for court cases, of course

Note that http://community.aallnet.org/digitalaccesstolegalinformationcommittee/stateonlinelegalinformation has some info on preservation and authenticity of online statutes

WestLawNext has a lot of attention; seems they do have some state trial court information; review at http://deweybstrategic.blogspot.com/2012/07/next-generation-legal-search-engines.html and Does WestlawNext Really Change Everything is another article but does not discuss content much;

### Precedent enforcement stare decisis
Searched on 'state "trial court" records'

I've wondered how well precedent is enforced when judges can't really be sanctioned, just reversed; Appellate Court Supervision in the Federal Judiciary: A Hierarchical Perspective (2003) alludes to this but does not take it to its logical conclusion

Inequitable Injunctions: The Scandal of Private Judging in the U.S. Courts - may speak to this but not available

Judicial Discretion of the Trial Court - early look at this

Surveying difficult populations: Lessons learned from a national survey of state trial court judges - surveys of judges