Lawyers
===

Includes Alaska lawyers

## Directories
http://en.wikipedia.org/wiki/Attorney%E2%80%93client_matching

Lawyers.com is the free consumer version of Martindale-Hubbell (at least run by the same company). Avvo.com is another similar website but is really focused on consumer, rather than complex, law.

Options:
* Martindale-Hubbell - has been the subject of controversy and "mutiny" per http://www.wendytech.com/articlesmartindalemutiny.htm;
* Avvo.com
* Justia
* AltLaw.org
* West Legal Directory -
* bestlawyers.com
* superlawyers.com -
* Chambers
* ACC Value Index,
* LinkedIn.

## Law without a license (unauthorized practice of law) UPL
See http://hirealawyer.findlaw.com/do-you-need-a- lawyer/what-is-legal-advice.html and http://www.law.sc.edu/library/circuit_riders/manual/unauthorized_practice_of_law.pdf Librarians and the unauthorized practice of law; also see 1995 American Bar Association Commission on Nonlawyer Practice

## Law firms
see wiki  [list of largest by revenue](https://en.wikipedia.org/wiki/List_of_100_largest_law_firms_by_revenue)

Sedgwick LLP - noticed lots of insurance publications  [here](http://www.sdma.com/publications/)

Edelson ( [wiki](https://en.wikipedia.org/wiki/Edelson)) - new (2007) famous for relaxed atmosphere, pranks, and major consumer class actions

## State comments

### Alaska
overview published by AK Journal of Commerce; see http://www.americanregistry.com/recognition/alaskas-largest-law-firms/108602 for a digital copy  
Atkinson, Conway, and Gagnon - best by far; see http://www.acglaw.com/attorneys.html; learned about them thru Neil O'Donnell who pressured REI into revealing their executive salaries per a Sitnews article; most are on Avvo; Gagnon is on Alaska Super Lawyershttp://www.superlawyers.com/alaska/toplists/Top-10-Alaska-Super- Lawyers-2009/2fe5a27cde066c0b65acb8f2c1717464  

Perkins Coie is the major mining law firm; particularly noticeable thru Bradford G. Keithley who is a noticeable force in the oil & gas world and has excellent analysis on the oil tax cut  

Read up on Louis James Menendez' application and noticed misspellings in his statement, e.g. "breadth" spelled as breath and "insure" instead of ensure, slightly questionable; also surprisingly few cases tried in his criminal practice

Juneau:  
Mark Choate  
Baxter, Bruce & Sullivan

Kevin Higgins - young guy with nice website

Listened to the argument in 3PA-08-1797CI (Costella v. Sherry Jackson); amazed at Richard L Harren's incompetence, he really doesn't like the rule against suing insurers directly, co-counsel Jeff Barber seemed mildly more competent;  [Kari Kristiansen](http://judgepedia.org/index.php/Kari_Kristiansen) seemed decently competent 

http://www.alaskainjurylawgroup.com/ - noticed this through Michael Moody who I saw on Gavel Alaska - these guys are quite accomplished

Kept hearing about Debevoise http://en.wikipedia.org/wiki/Debevoise_%26_Plimpton in the West Wing; reminds me of Anonymous Lawyer, that hilarious blog

## Poor people's lawyers
Congress set up the http://en.wikipedia.org/wiki/Legal_Services_Corporation and there's an Alaska version http://www.alsc-law.org/ which also may run http://www.alaskalawhelp.org/AK/index.cfm;

## Firing
See http://legal-malpractice. lawyers.com/When-You-Are-Unhappy-with-Your- Lawyer-FAQ.html; clerk said this is not a standard form;

## Famous
Stanley M. Chesley - famous litigator involved in numerous big class-action cases http://en.wikipedia.org/wiki/Stanley_M._Chesley;

[Fred Rodell](http://en.wikipedia.org/wiki/Fred_Rodell) - OK, not a lawyerbut rather a law professor famous for criticizing lawyers

Dickie Scruggs - Mississipi antitabacco attorney guilty of bribery

Mel Weiss - top securities litigator who with Bill erach was guilty of kickback scheme