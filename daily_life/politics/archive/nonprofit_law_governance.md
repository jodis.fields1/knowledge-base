# Nonprofit law and governance
[Why Nonprofits Have a Board Problem](https://perma.cc/2YHV-KXNR) - good summary of Chait's Governance as Leadership

[For Many Nonprofit Boards, Smaller is Better](https://perma.cc/NWK6-E3CC) - agree with this, added to board of directors article

## 2015 update

Board of Nonprofit Organizations: Puzzling through the Gaps between Law and Practice (2007) - good empirical and critical overview

[Board Meeting Minutes – Part I](http://www.nonprofitlawblog.com/board-meeting-minutes/) (2011) - good overview

### Narrative summaries about conversations
[MEETING MINUTES](http://www.davis-stirling.com/MeetingMinutes/tabid/1565/Default.aspx) - no, not even a transcript

[School board expends many minutes approving meeting minutes](http://m.fairfieldcitizenonline.com/news/article/School-board-expends-many-minutes-approving-6386706.php) \- example of controversy in action

See  [A Consideration of an LLC for a 501(c)(3) Nonprofit Organization](http://heinonlinebackup.com/hol-cgi-bin/get_pdf.cgi?handle=hein.journals/wmitch38&section=22) (2011) which discusses using LLCs

## Executive session
EXECUTIVE SESSIONS: HOW TO USE THEM REGULARLY AND WISELY (2007) - widely-cited BoardSource article  

## ED contracts
[WILLIAMSBURG AREA TRANSIT AUTHORITY EXECUTIVE DIRECTOR EMPLOYMENT CONTRACT](http://www.jamescitycountyva.gov/pdf/wat/031909wata/Employment%20Contract_Executive%20Director.pdf)\- not very good, six months max severance earned per year

[Walter Norris Ypsilanti Housing](http://www.annarbor.com/norriscontract.pdf)\- also six months of severance but worded as a six-month notice  

## Governance
BoardSource - nonprofit which focuses on this; partnered with Guidestar to work on data around governance practices per  BoardSource and GuideStar Partner to Create Transparency Around Nonprofit Governance (2014)

Foraker Group - Alaska consulting group

See  [Good Governance Practices for 501(c)(3) Organizations](http://www.nacua.org/documents/GoodGovernancePractices.pdf) by the IRS

See Reiser: Enron. org: Why Sarbanes-Oxley Will Not Ensure Comprehensive NonprofitAccountability

Also see The Causes and Consequences of Internal Control Problems in Nonprofit Organizations - The Accounting Review 2011

Category A and B material weaknesses: http://www.cfo.com/article.cfm/3396587?f=related

On fraud and auditors requirements: Corporate Governance Challenges: When Auditors Must Push Back

STEALTH PREEMPTION: THE IRS'S NONPROFITCORPORATE GOVERNANCE INITIATIVE

Nonprofit Board Standard of Care, Risk Management, and Audit Committee Responsibility (Updated May 19, 2011) - by David Tate

Should Staff Contact with the Board Be Restricted? (2011) - good discussion http://www.blueavocado.org/content/should-staff-contact-board-be-restricted;

Sarbanes-Oxley: Ten Years Later (2012) - several states passed laws requireing SOX compliance

Five Internal Controls for the Very Small Nonprofit (2010) - good advice

Best Practices for Executive Directors and of Nonprofit Organizations Boards - Whatcom Council ideas but they are somewhat basic  

## IRS guidance

IRS Publication 557 has more details; see http://www.irs.gov/charities/article/0,,id=96774,00.html and click on the publications from there

Publication 4221-PC and Publication 4221-PF discuss the different types of 501(c)(3)

Glanced through 2012 Advisory Committee on Tax annual report; discussed lots of internal guidance to ensure uniformity; 1023 forms for creating 501(c)(3); 12 pages w/ 14 pages of schedules; 1.5m nonprofits total; 55k applications/yr and only 200 denied but 5,400 no determination

See Revenue Act of 1950

## IRS actions

IRS Kills Tax Exemption Of Foundation Pushing Eternal Life - LEF sold too much merchandise, unclear grantmaking

Hawaii nonprofit penalized for lobbying

Dad's SSRAA paid taxes

## IRS Form 990
wrote Guide to reading a Form 990 and sent it out to SEACC board 2014-04-13

First Form 990 filed for tax year 1941

Typically find these on Guidestar but  [foundationcenter.org/findfunders/990finder/](http://foundationcenter.org/findfunders/990finder) is actually more user-friendly in some ways (found ASEA easier)

Instructions for Form 990 Return of Organization Exempt From Income Tax Form 990 ( [html](http://www.irs.gov/instructions/i990/) or  [pdf](http://www.irs.gov/pub/irs-pdf/i990.pdf) under forms and publications with a search) -  basic guidance

_Table of contents_ \- found one at the 2008 revision list called  [TY 2008 Form 990 - Forms and Instructions (December 2008)](http://www.irs.gov/Charities-&-Non-Profits/TY-2008-Form-990---Forms-and-Instructions-%28December-2008%29)

_Filing exemptions_ \- covered by Chasin et al 2003 which notes governmental affiliates, which may exempt ASEA

_Sections_  -  Section IV has a checklist of the required schedules in question form

Fundraising\- Section VIII Statement of Revenue item 1c versus 8 is confusing as both are fundraising revenue and instructions aren't super-clear, but it appears that 1c is for contributions in excess of value whereas 8 is more like sales and the main difference may be whether the difference is tax-deductible per Expansion: Part VIII (Statement of Revenue) in  [How to Read the New IRS Form 990](http://www.npccny.org/new990/new990.htm "http://www.npccny.org/new990/new990.htm") by Peter Swords, and more likely to be unrelated business income (see Column C and D "revenue excluded from tax under section 512, 513, or 514" not entirely sure what that is), also read  [What Is Fundraising? — How to Report in the 990](http://www.cliftonlarsonallen.com/Nonprofit/What-Is-Fundraising-How-to-Report-Form-990.aspx "http://www.cliftonlarsonallen.com/Nonprofit/What-Is-Fundraising-How-to-Report-Form-990.aspx") which clarifies and suggests that 8b and 8c should net out to zero mostly (Waterkeeper Alliance did it wrong)

_Lobbying_ \- don't have to fill out the detailed disclosure of Schedule C, Part II-B if you file the Form 5768 under the 501(h) election apparently, which just says you'll abide by the dollar value limit rather than the "substantial" test

_Schedules_ \- first four (Public Support, Schedule of Contributors, Political Activities, and Supplemental Financial Statements) are the biggies; Schedule O added which additional required narrative explanations per instructions; read thru Exempt Organizations-Technical Instruction Program for FY 2003, Form 990, Schedule A and Schedule B by Chasin et al which was quite informative with inside tips such as who is able to file and the 501(c)(h) expenditures test

> _Schedule D_ _Supplemental Financial Statements_ \- major differences summarized in Maher Duessel ppt presentation  [IRS Form 990 vs. Audited Financial Statements](http://www.md-cpas.com/wp-content/uploads/2012/07/6-up-990-Differences.pdf "http://www.md-cpas.com/wp-content/uploads/2012/07/6-up-990-Differences.pdf") which notes differences in: investment values (990: no unrealized gains/losses), in-kind services (990: no recognition whatsoever), grant revenue recognition, gross v. net reporting for fundraising (990: net) plus various other revenue on net, compensation (fiscal versus calendar year, includes nontaxable benefits and expense allowances etc), consolidation (990: separate)

## Public inspection
> Exempt Organizations Annual Reporting Requirements Public Disclosure and Availability of Exempt Organizations Returns and Applications:Questions about Requirements for Exempt Organizations to Disclose IRS Filings to the General Public ( [pdf](http://www.irs.gov/pub/irs-tege/eo_disclosure_faqs.pdf)) - wow that name is a mouthful!   
>  Schedule B, Schedule of Contributors has personal contributor information which can only be disclosed with redactions; see  [IRS to Protect Privacy of Contributors to Exempt Organizations](http://www.pgdc.com/pgdc/irs-protect-privacy-contributors-exempt-organizations) from 2002 by PGDC and PhilanthropyRoundable  [Donor D&A](http://www.philanthropyroundtable.org/topic/excellence_in_philanthropy/donor_q_a5) from 2002;
> 501(c)(6) labor unions potentially face a proxy tax

Got first major change since 1979 in 2008 which were effective for the 2008 form announced 2008-8-19 with  [IRS Issues Instructions for New Form 990](http://www.irs.gov/uac/IRS-Issues-Instructions-for-New-Form-990), also links to instruction page which has table of contents and schedules listed, also look at Form 990 Redesign Background Documents see  [Background Paper Forms 990, Moving From the Old to the New August 19, 2008](http://www.irs.gov/pub/irs-tege/moving_from_old_to_new.pdf) - 

2007:  [IRS Releases Discussion Draft of Redesigned Form 990 for Tax-Exempt Organizations](http://www.irs.gov/uac/IRS-Releases-Discussion-Draft-of-Redesigned-Form-990-for-Tax-Exempt-Organizations) - more details in  [Background Paper: Redesigned Draft Form 990](http://www.irs.gov/pub/irs-tege/form_990_cover_sheet.pdf) which also summarizes the major nine sections (at this point) and fifteen schedules, with most filling out Schedule D, 3 by less than 25%, and less than 10% completing 8, Form 990 schedule from 25k to 50k, 

2012: 

Form 990-EZ qualification went up to million mark in but lowerd to $200,000 per  [Unveiled: The IRS Introduces the Redesigned Form 990](https://www.guidestar.org/rxa/news/articles/2008/unveiled-the-irs-introduces-the-redesigned-form-990.aspx) (2008); 

## Lobbying
[Top Ten Myths about 501(c)(3) Lobbying and Political Activity](http://www.asaecenter.org/Resources/whitepaperdetail.cfm?ItemNumber=12202) - note this is from 2002 with laws passed in the meantime; says pursuant to 1976 law, if a 501(h) election is made there's a $1 million limit, with 20% of the first $500k; in 2013 a Hawaii Christian nonprofit paid tax because of its excessive lobbying

## Types

IRS refers to 501(c)(3) as "private foundations" and 509(a) for public charities - see p32 Ch.3 of 557 - technically 501(c)(3)s are either one or the other

Churches, schools, hospitals or medical research are automatically charities

Others which have qualifying public support as determined by either 1/3 of contributions or some other conditions; 10 percent from government or public is a minimum

501(c)(4) is shown in Ch.4 of Publication 557 "Civic Leagues and Social Welfare Organizations"

527 is for political action committees (PAC)

501(c)(9)

## Politics

Note that public charities can elect the expenditures test but private foundations cannot (see ch.3 p49 or so); use Form 5768

As [Lobbying and Political Activity by Tax-Exempt Organizations](http://www.muridae.com/nporegulation/lobbying.html) notes, only certain 501(c)(3) can lobby and cannot participate in political activities but can lobby as long as it is not substantial, as determined by either an expenditures test or a subjective substantial part test

Per the Public Citizen's Bright Line Project aims to change it from for a candidate to a broader test from 1990

[Update on Permissible Activities of 501(c)(3) Organizations](http://www.ombwatch.org/node/322) says it is OK to send out questionnaires to candidates