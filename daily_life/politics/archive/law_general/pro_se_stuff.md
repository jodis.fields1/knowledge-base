# Pro se stuff

## Collections
http://www.ajs.org/prose/pro_links.asp

## Articles and books
Pro Se Litigation: Is This Phenomenon Helping Or Hurting Our Legal System? - 2009 article

The End of Lawyers? - 2010 book by Susskind http://www.amazon.com/The-End-Lawyers-Rethinking-services/dp/0199593612/ref=sr_1_1?ie=UTF8&qid=1365200202&sr=8-1&keywords=The+End+of+Lawyers%3F; another article on law reform would be Injustice for All (1996)

Going to Trial: A Step-by-Step Guide to Trial Practice and Procedure - by ABA for lawyers who don't go to trial very often

Represent Yourself in Court: How to Prepare & Try a Winning Case - mixed Amazon reviews

The Complete Idiot's Guide to Lawsuits - pretty basic; didn't even mention the litigation privilege against defamation

Pro Se Cites & Authorities - Amazon book

Pro Se Guide to Family Court - independently published in 2011

## Court guides
Each federal court appears to have a guide; New York's appears relatively detailed http://www.nysd.uscourts.gov/file/forms/pro-se-litigants-manual;

State guides - only found one for the Supreme Court of Ohio http://www.supremecourt.ohio.gov/Publications/proSeGuide.pdf

Guides on writing up statements: see http://www.netplaces.com/paralegal/pretrial-preparation/pleadings.htm for good details

IAALS article called Fact-Based Pleading: A Solution Hidden in Plain Sight

## Defamation risk
There is a "litigation privilege" for court filings as discussed in

http://www.avvo.com/legal-answers/i-am-a-victim-of-defamation-of-character-per-a-cou-234921.html

## Lawsuit
### Statement of acts
See David Lee's Writing the Statement of Facts at http://www.davidleelaw.com/articles/statemen-fct.html;

### Motions
Typically you get 20 days or so to respond, with a possible extension, but no exceptions past that point

### Trial
See http://www.innd.uscourts.gov/judges/miller/docs/ProseTrialExplanation.pdf

http://freedom-school.com/court_tricks_and_traps.pdf is good for tips on Texas (recommends O'Conners Texas Rules, Civil Trials, 2001); for example discusses how lawyers factual unsworn statements become fact on appeal if not objected to

### Testimony
See http://boards.straightdope.com/sdmb/archive/index.php/t-11860.html \- the pro se person can ask themselves question, or they can give it in narrative; narrative raises issues about not enough time to object; http://www.persuasivelitigator.com/2010/02/never-underestimate-a-pro-se-plaintiff.html discusses how to prevent them from putting out inadmissible evidence; http://www.rosen.com/forum/viewtopic.php?p=31807 says in N. Carolina the judge asks the question

See http://www.innd.uscourts.gov/judges/miller/docs/ProseTrialExplanation.pdf for an explanation of trial process