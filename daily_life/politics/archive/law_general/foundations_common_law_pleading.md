# General/Foundations of law (common law, pleading)

See http://en.wikipedia.org/wiki/Law_of_the_United_States

## Law school

The Five-Minute Law School: Everything You Learn In Your First Year, More or Less(http://writ.news.findlaw.com/dorf/20050803.html)

HOW TO "THINK LIKE A LAWYER": Advice To New And Prospective Law Students(http://writ.news.findlaw.com/dorf/20010822.html)

Judicial training - just read http://jec.unm.edu/education/online-training from University of New Mexico Judicial Education Center

## Number of laws - too many

Read some legal article that said Alaska has much fewer statutes, which is nice

Commentaries include http://www.esquire.com/features/thousand-words-on-culture/end-of-law-in-america-0612?click=main_sr by Stephen Marchie and poked fun of by J.D. Tuccille at http://reason.com/blog/2012/06/11/esquire-writer-discovers-that-too-many-l; http://www.theatlantic.com/politics/archive/2012/03/outdated-laws-and-subsidies-how-did-it-get-so-bad/253860/ by Jim Cooper; 68% of Americans say too many laws per http://www.rasmussenreports.com/public_content/lifestyle/general_lifestyle/july_2012/68_say_there_are_too_many_unnecessary_laws_in_the_u_s

Per http://www.ajc.com/news/news/opinion/one-nation-under-too-many-laws/nQpDt/ Jimmy Carter was big into sunset laws and suggests omnibus sunset law

Book review of ONE NATION UNDER ARREST: HOW CRAZY LAWS, ROGUE PROSECUTORS, AND ACTIVIST JUDGES THREATEN YOUR LIBERTY http://www.washingtontimes.com/news/2010/aug/17/too-many-laws-to-keep-straight \- has some good info; federal criminal law one 2007 study put the figure at 4,450

Also note Competitive Enterprise Institute's 10k commands http://cei.org/10kc and Wayne Crews, brings up the REINS Act

## Civil law

See Corpus Juris Civilis and the particular chapter of the Codex Justinian  [here](http://www.uwyo.edu/lawlib/blume-justinian/) with motivation  [described at wiki article](https://en.wikipedia.org/wiki/Codex_Justinianus#English_Translations) and also see http://en.wikipedia.org/wiki/Civil_law_%28legal_system%29; also should review the various pages at http://en.wikipedia.org/wiki/Template:Law

## Standing and federal versus state law
This gets very complicated; thought I had something written up about this (??); http://en.wikipedia.org/wiki/Erie_doctrine is major; in practice state procedure may be applied (e.g. AK Rule of Civil Procedure 82)

### Sovereign immunity of states from federal courts

11th Amendment applies to limit per majority opinion in Alden however others believe it is only for diversity jurisdiction per https://en.wikipedia.org/wiki/Eleventh_Amendment_to_the_United_States_Constitution (http://legal-dictionary.thefreedictionary.com/Eleventh+Amendment is similar); not clear how this works with https://en.wikipedia.org/wiki/Abstention_doctrine; When Can a State Be Sued? by Alstyne discusses this in response to When  You Can't Sue the State (2000) by Brown-Graham but Who's Afraid of the Eleventh Amendment? The Limited Impact of the Court's Sovereign Immunity Rulings (2006) says this is much ado about nothing; see also http://en.wikipedia.org/wiki/Sovereign_immunity_in_the_United_States for more details; http://fightcps.com/2010/12/03/how-to-sue-cps-in-federal-court/ seems optimistic

### Notable articles and blogs
See http://leiterlawschool.typepad.com/leiter/2011/05/corporate-practice-commentators-top-10-corporate-securities-articles-for-2010.html for Corporate Practice Commentator's Top 10

http://taxprof.typepad.com/taxprof_blog/2011/10/law-prof-blog.html/ is a list of notable blogs; Balkinization and the Volokh ones looks promising but not the others

### Common law positions and Restatements
http://en.wikipedia.org/wiki/Restatements_of_the_Law has a good list; The Restatement of the Common Law by the American Law Institute by Arthur Corbin (1929) has a lot on the history before the Restatement - obviously, a mess as it is now

First edition: Agency, Conflict of Laws, Contracts, Judgments, Property, Restitution, Security, Torts, and Trusts

Second edition started 1952 per http://en.wikipedia.org/wiki/American_Law_Institute

Joint and several liability - has an interesting twist

Tort -  see separate Tort law note; includes lawsuits for negligent misrepresentation; note that Third requirements intent for product liability http://www.torttalk.com/2012/07/federal-middle-district-judge-jones.html

Contract - consideration details and such

Agency & employment- employmentat will; unions in early 1800s were criminal conspiracies; also read up on Alaska Human Rights Commission (law is broader than federal per AS 18.80.220); decisions such as Parish v. Rumrunners where woman received only $4,000 three years later after being raped by coworker (backpay mitigated by something, maybe unemployment); backpay limited to one year generally; reinstatement doesn't always happen; Dowler v. Knopf was $50k on the other hand;

Trust - fiduciary duty

Conflicts of law

Louis Brandeis quote: "Stare decisis is usually the wise policy, because in most matters it is more important that the applicable rule of law be settled than that it be settled right" see http://en.wikiquote.org/wiki/Louis_Brandeis;

## State laws
Uniform Law Commission and NCCSL as well as the NCOIL for insurance

Constitutions are very important; some are uniquely terrible such as California (super-long, tons of amendments) and Alabama (even longer)

Long arm jurisdiction http://en.wikipedia.org/wiki/Long_arm_jurisdiction is limited by fed per http://en.wikipedia.org/wiki/Personal_jurisdiction Shoe doctrine also saw a 50-state survey by Vedder, Price etc (2003) euro.ecom.cmu.edu/program/law/08-732/Jurisdiction/LongArmSurvey.pdf

## Later American
Roger Traynor http://en.wikipedia.org/wiki/Roger_J._Traynor was one of the most famous state court judges; known for strict liability for products

Roscoe Pound - professor of law at Harvard for a while

## Foundation
From 1982 article Morton Horwitz and the Transformation of American Legal History I learned a bit about American legal history

More recently, after reading the 2012-04-05 controversy over Obama commenting about judicial activism with the healthcare law, I read up on Lochner and what is referred to as the "Lochner era" on Wikipedia (http://en.wikipedia.org/wiki/Lochner) along with Oliver Wendell Holmes, Jr's sole dissent.

Based on some questions I had for the  Alaska Chief Justice Carpeneti, looked at http://legal-dictionary.thefreedictionary.com/Harmless+Error and http://legal-dictionary.thefreedictionary.com/Plain-Error+Rule

Colin Blackburn http://en.wikipedia.org/wiki/Colin_Blackburn,_Baron_Blackburn was very influential in the 1800s; see E Manson, Builders of our Law (1904) for more information on these types of guys

More influential is Blackstone http://en.wikipedia.org/wiki/Commentaries_on_the_Laws_of_England

Black's Law Dictionary may have been influential - what's up with all these blacks??

Randomly read Planned Parenthood of Southeastern Pa. v. Casey

At the legal dictionary in common as of 2012-01-07, noticed (italics mine):

common law n. the traditional unwritten law of England, based on custom and usage which developed over a thousand years before the founding of the United States. The best of the pre-Saxon compendiums of the Common Law was reportedly written by a woman, Queen Martia, wife of a Briton king of a small English kingdom. Together with a book on the "law of the monarchy" by a Duke of Cornwall, Queen Martia's work was translated into the emerging English language by King Alfred (849-899 A.D.). When William the Conqueror arrived in 1066, he combined the best of this Anglo-Saxon law with Norman law, which resulted in the English Common Law, much of which was by custom and precedent rather than by written code. By the 14th Century legal decisions and commentaries on the common law began providing precedents for the courts and lawyers to follow. It did not include the so-called law of equity (chancery) which came from the royal power to order or prohibit specific acts. The common law became the basic law of most states due to the Commentaries on the Laws of England, completed by Sir William Blackstone in 1769, which became every American lawyer's bible. Today almost all common law has been enacted into statutes with modern variations by all the states except Louisiana which is still influenced by the Napoleonic Code. In some states the principles of common law are so basic they are applied without reference to statute.Copyright © 1981-2005 by Gerald N. Hill and Kathleen T. Hill. All Right reserved.

Also saw from there in http://legal-dictionary.thefreedictionary.com/Pleading:

Despite criticism, common-law pleading endured in England and in the United States for several centuries. Beginning in 1848, some states replaced it by law with a new system called Code Pleading. The statutes enacting code pleading abolished the old forms of action and set out a procedure that required the plaintiff simply to state in a complaint facts that warranted legal relief. A defendant was authorized to resist the plaintiff's demand by denying the truth of the facts in the complaint or by stating new facts that defeated them. The defendant's response is called an answer.

In 1938, federal courts began using a modern system of pleading set out in the federal Rules of Civil Procedure. This system has been so effective that many states have enacted substantially the same rules of pleading. A pleading by a plaintiff or defendant under these rules is intended simply to give the other party adequate notice of the claim or defense. This notice must give the adversary enough information so that she can determine the evidence that she wants to uncover during pretrial discovery and then adequately prepare for trial. Because of this under-lying purpose, modern federal pleading is also called notice pleading. The other objectives of earlier kinds of pleading are accomplished by different procedural devices provided for in the Federal Rules of Civil Procedure.

Cross-references

[Civil Procedure](http://legal-dictionary.thefreedictionary.com/Civil+Procedure).

West's Encyclopedia of American Law, edition 2. Copyright 2008 The Gale Group, Inc. All rights reserved.

## And in code pleading

In 1848 New York enacted a new code to govern Pleading in the courts of that state. It was called the Field Code after David Dudley Field, the man who devised it. A number of other states followed the lead of New York. This pattern of pleading a  [Cause of Action](http://legal-dictionary.thefreedictionary.com/Cause+of+Action) or a response came to be called code pleading.

Common-law pleading had required reducing every case to one claim and one response. Since grievances did not always fit into common-law forms, code pleading abandoned it. All the old forms of action were abolished and the extreme formality of common-law pleading was abandoned. Under code pleading the plaintiff has only to make a statement of facts that, if true, justify legal relief. The only requirement is that those facts fit the general pattern of some established legal right and that they state a claim on which relief can be granted. Furthermore, the plaintiff can present alternative or even inconsistent sets of facts and leave it to the trier of fact to establish which are correct. This is allowed when the plaintiff does not know all the facts affecting the claim, so long as the pleading is made honestly and in  [ Good Faith](http://legal-dictionary.thefreedictionary.com/Good+Faith). More than one cause of action can be alleged but they must be stated as separate counts. For example, some states allow a simplified form of pleading of a breach of contract. The plaintiff may simply state that money is owed but has not been paid or services have been rendered but payment has not been made.

Code pleading solved many of the problems associated with common-law pleading but it also spawned a new controversy. The requirement that a plaintiff set out a claim by reciting facts justifying relief left open the question of what facts might be included. It has often been said that a plaintiff need plead  [Ultimate Facts](http://legal-dictionary.thefreedictionary.com/Ultimate+Facts), not legal conclusions. Case after case has been fought on this point. The distinction primarily concerns how much detail must be given. A plaintiff must be able to show that he or she has a legal right, the defendant breached or violated that right, and the plaintiff thereby suffered some harm.

West's Encyclopedia of American Law, edition 2. Copyright 2008 The Gale Group, Inc. All rights reserved.

And at answers.com, this excerpt from the Gale Encyclopedia of US History:

Even as conservative judges were acknowledging their ability to remake the law to bring it into line with American views of economy and society, they were careful to portray the common law as evolving slowly. They needed to guard against the image that the common law might effect rapid change, for they needed to preserve the law's majesty. Few maintained the fiction of Blackstone's era that the law had been the same from time immemorial, but many continued to believe that judges had little power in remaking the law. The dominant view of the early nineteenth century was that judges were expounders of the common law and only had the power to make incremental changes.

## Questioning the Common Law

Many outside the legal system saw the issue differently, however. Those outsiders saw the common law not as the perfection of reason but as the perfection of nonsense. In speeches and newspapers, outsiders to the legal system—usually adherents of the Democratic Party—attacked the common law as the creation of judges, who were making law to protect property against democracy. These debates occurred at a time when judges were using common-law doctrines to outlaw union organizing, to require the return of fugitive slaves, and to protect merchants and creditors at the expense of consumers and debtors. The critics of the common law ridiculed it as an arbitrary collection of abstruse rules. William Sampson's attack was among the most vitriolic. He thought Americans "had still one pagan idol to which they daily offered up much smokey incense. They called it by the mystical and cabalistic name of Common Law."