# Crime (white-collar)

You Break the Law Every Day…Without Even Knowing It - notes that the CRS can't count federal crimes and various loopy state laws, which snopes has noted are fake in some cases

## Organized crime

The story of mobster MA Whitey Bulger and his FBI informant status was interesting, apparently inspired The Departed

## Self-defense

According to Volokh, (" [Major Shift Coming in Pennsylvania Self-Defense Law?](http://www.volokh.com/2013/07/30/major-shift-coming-in-pennsylvania-self-defense-law/), "ln but either one or two states provide that, once the defense in a criminal case introduces any evidence of possible self-defense, the prosecution must disprove self-defense beyond a reasonable doubt" while the other rule is preponderance of evidence, which makes it harder; LA may have the preponderance with PA moving towards it; potentially more significant than self-defense

## Fraud

See http://cesnb.com/index.php/2012/01/10/fraud-in-the-united-states-2/ - in 2011 Computer Evidence Specialists (CES) released a report on fraud in the United States.  

Noticed this article in Bloomberg that says "Wealthy More Likely to Lie, Cheat: Researchers" based upon Piff et al 2012's "Higher social class predicts increased unethical behavior". This is an interesting finding which is also aligned with the finding that MBA candidates cheat more than other students ("A Crooked Path Through B-School?"). My last accounting professor, who was an auditor for a while, said he found a little fraud on nearly every job. At the last conference, I heard a couple scary stories about rampant fraud, complete with suicide and prison sentences.  

_Skilling v. United States_ - motivated by Mark J. Avery getting off after looting a trust (Court Throws Out Ex-Prosecutor’s Conviction for Looting Trust) I researched this case and the  [honest services fraud statute](https://en.wikipedia.org/wiki/Honest_services_fraud); for example Overcriminalization And the Fallout From ‘Skilling’ notes a 2010 hearing 'Restoring Key 

Tools to Combat Fraud and Corruption After the Supreme Court’s Skilling Decision' but also says that feds have property fraud laws for private sector self-dealing generally; also see Honest Services Fraud After Skilling v. United States at the Federalist Society; Skilling v. United States and the New Meaning of Honest Services Mail Fraud (2011) is a great overview

## Transactional Records Access Clearinghouse (TRAC)

Create wikipedia article for TRAC  [http://en.wikipedia.org/wiki/Trac](http://en.wikipedia.org/wiki/Trac) and http://en.wikipedia.org/wiki/Special:Search?search=Transactional+Records+Access+Clearinghouse&sourceid=Mozilla-search

TRAC  <http://trac.syr.edu> is a great source - for example, see overview of Justice Dept at  http://trac.syr.edu/tracreports/bulletins/overall/monthlyfeb12/gui/

Free bulletins listed at http://trac.syr.edu/tracreports/bulletins/

Government regulatory bulletin http://trac.syr.edu/tracreports/bulletins/regulation/monthlyfeb12/fil/

White collar crimeat http://trac.syr.edu/tracreports/bulletins/white_collar_crime/monthlyfeb12/gui/

Lead charge bulletins:
* http://trac.syr.edu/tracreports/bulletins/monthly_list.shtml#trac_leadcharge

## USC
http://en.wikipedia.org/wiki/Title_18_of_the_United_States_Code is a major foundation at federal level

## Burglary

Bloomberg article Miami Heist: The Brink's Money Plane Job's Messy Aftermath (2013) is a good story on how these work; last airport robbery with a payoff as large was in 1978, when the Gambino and Lucchese crimefamilies robbed $8 million in cash and jewels from a Lufthansa

## Strict criminal liability

Park Doctrine:
* allows for criminal liability (including possible jail time for executives) for public welfare offense; 
* created by United States v. Park (1975) and United States v. Dotterweich (1943) related to violations of the Food, Drug, and Cosmetic Act (FDCA) per the [Park CriminalLiability Doctrine: Is It Dead or is it Awakening?](https://web.archive.org/web/20161013115507/http://www.hpm.com/pdf/FLEDERPARK.PDF) (2009) by [fdli.org](http://fdli.org) Food and Drug Law Institute; 
* also see The Responsible Corporate Officer Doctrine: Can You Go To Jail for You Don't Know? at http://www.venable.com/files/Publication/814e9bf3-e36f-4dde-afe1-4a1a2babb6bc/Presentation/PublicationAttachment/d7047ed5-bd76-4e2f-89c9-e46a501d7234/524.Pdf

Also followed the Responsible Corporate Officer article citations and skimmed CriminalLiability and the Clean Air Act: Should We Rely on Prosecutorial Discretion? (2012) which relies on Envtl. Prot. Agency,The General Duty Clause; also Holding the Responsible Corporate Officer Responsible: Addressing the Need for Expansion of CriminalLiability for Corporate Environmental Violators which takes a more hard stance, notes that RCO is specifically noted in Clean Water Act (CWA) and Clean Air Act (CAA)

## Restitution
See Victim and Witness Protection Act (VWPA) plus the Mandatory Victim Restitutions Act (MVRA)

## Financial crisis prosecution

[William D Cohan on Wall Street whistleblowers](http://www.ft.com/intl/cms/s/2/ce216134-e6c7-11e3-9a20-00144feabdc0.html#axzz33a3uJ5Z8) (2014) - not too surprising!

[Let’s End Politico and Deal Book’s “Competition in Sycophancy”](http://www.ritholtz.com/blog/2014/02/lets-end-politico-and-deal-books-competition-in-sycophancy/) - by William K. Black, excellent article which reveals that Akerlof and Gulliver's Travels noted that fraud drives out good business

[The Rise of Corporate Impunity](http://www.propublica.org/article/the-rise-of-corporate-impunity) (2014) - the story of Chertoff, whose agression with Arthur Andersen caused a backlash that probably led to the lax enforcement in later years

Justice Department Wielding FIRREA in Financial Fraud Cases (2013) - old 1989 law revived by Leon Weidman per From Anonymity to Scourge of Wall Street (2013)

Martin Act is a hardcore NY law

[More fraud evidence tilts the crisis narrative](http://www.cjr.org/the_audit/more_fraud_evidence_tilts_the.php?page=all) - shows misrepresentation in the MBS packages

[Mortgage Fraud Task Force Stats: Did You See This? WOW](http://www.senseoncents.com/2013/08/mortgage-fraud-task-force-stats-did-you-see-this-wow/) - huge restatement

2012-09 financial crisis task force overview at [Too Big To Jail: Wall Street Executives Unlikely To Face CriminalCharges, Source Says](http://www.huffingtonpost.com/2012/09/08/ criminal-charges-wall-street_n_1857926.html?utm_hp_ref=tw)

The transcript from 60 minutes is here: http://www.cbsnews.com/2102-18560_162-57336042.html?tag=contentMain;contentBody and notes Partnoy, a securities academic, saying that Foster's case is a good start for an investigation.   
  
See http://www.cjr.org/the_audit/60_minutes_tough_piece_on_the.php - talks to Eileen Foster; reportedly the Justice Department has never talked to Foster. Detailed report on her at http://online.wsj.com/article/SB10001424053111904060604576573073620596988.html \- no mention of her in Wikipedia   
  
http://papers.ssrn.com/sol3/papers.cfm?abstract_id=1690979&rec=1&srcabs=1579719   
  
http://dealbook.nytimes.com/2011/06/07/despite-worries-serving-at-the-top-carries-little-risk/ Despite Worries, Serving at the Top Carries Little Risk   
  
Mozilo, per 60 minutes transcript:   
  
Mozilo, who admitted no wrongdoing, accepted a lifetime ban from ever serving as an officer or director of a publicly traded company, and agreed to pay a record $22 million fine, less than five percent of the compensation he received between 2000 and 2008.

Read more: https://www.cbsnews.com/news/prosecuting-wall-street/#ixzz1j1KvOfP1

## Regulators/prosecutors
Apparently some sort of New York Council with their AG and stuff

Leslie Kazon, supervisor in New York branch, dismissed Markopolis complaint about Madoff per http://www.sec.gov/news/studies/2009/oig-509/exhibit-0144.pdf] (see also David Kotz IG report)

[SEC: “A Tollbooth on the Bankster Turnpike”](http://www.ritholtz.com/blog/2014/04/jim-kidneys-sec-farewell-speech/) - Jim Kidney's farewell speech!

Judge Rakoff has been hitting the SEC pretty hard and let loose in 2013 with Judge Rakoff: Why Have No High Level Executives Been Prosecuted In Connection With The Financial Crisis?; see also SEC Lets Top Execs In Fraud Settlement Go Free   

## News articles
Former Citigroup Manager Cleared in Mortgage Securities Case - NYTimes on Brian Stoker 

http://www.nytimes.com/interactive/2011/04/14/business/20110414-prosecute.html - April 2011 great timeline

In May 2012 both Pat Robertson and Stiglitz argue for prosecutions http://www.thecenterlane.com/?tag=savings-and-loan-crisis

April 2011 detailed look http://www.nytimes.com/2011/04/14/business/14prosecute.html?pagewanted=all

Nov 2010 http://dealbook.nytimes.com/2010/11/18/a-wave-of-bank-prosecutions-is-unlikely/ has some of the statutes

## S & L crisis

After the Fall: The CriminalLawEnforcement Response to the S&L Crisis (1991) by Bruce A. Green (http:// law.fordham.edu/faculty/3910.htm) - great coverage of the statutes and some of the history of criminalizing bank "misapplication"; little coverage of actual convictions as it was too early

The savings and loan debacle, financialcrime , and the state (1997) - summarizes the fraud on a surface-level; author Calavita has other articles including The state and white- collarcrime : Saving the savings and loans (1994) and The Savings and Loan Debacle of the 1980s: White- CollarCrimeor Risky Business? (1995)

Origins and Causes of the S&L Debacle: A Blueprint for Reform (1993) by the National Commission on Financial Institution Reform, Recovery and Enforcement is not available electronically

CQ Researcher's 2012 article: Jost, K. (2012, January 20). Financial misconduct. _ CQ Researcher _ , _ 22 _ , 53-76. Retrieved from http://library.cqpress.com/cqresearcher/ is remarkably good; notes William Black as a prominent supporter of prosecutions

## Libor

Will there be criminal prosecutions? One can only hope. See   
http://www.huffingtonpost.com/2013/03/29/libor-lawsuits-claims-dismissed_n_2981513.html 

The cases are In Re: Libor-Based Financial Instruments AntitrustLitigation, U.S. District Court for the Southern District of New York, No. 11-md-2262 - note that on the topic of antitrust The Capitol Forum is an expert

## Whistleblowers

UBS Whistleblower Gets Out of Jail - the old story about the whistleblower imprisoned while everyone else gets off; saw a documentary on this where I recall the prosecutor played dumb

## Model Penal Code versus common la
The New Common Law: Courts, Culture, and the Localization of the Model Penal Code (2011) by Anders Walker is a good overview of the model penal code and the actual lawin various states

## Federal versus state
Reading Stuntz (turns out he died in 2011 at 52) The Collapse of the American Justice System and he points out on page 65 that the federal government is much less active; however, the states are not funded proportionately to their extra work; page 66 says that federal sentences can be used to extract guilty pleas under state law

source is the Sourcebook of CriminalJustice Statistics http://www.albany.edu/sourcebook/index.html which also has the stats on whether judges are nominated or elected; http://www.justiceatstake.org/issues/state_court_issues/election_vs_appointment.cfm seems to be a website for reform of the legal system

Prosecutors and Politics: A Comparative Perspective by Michael Tonry discusses the role of prosecutors across countries; volume in _ Crimeand Justice _ series

## Procedure
http://www.criminaldefenselawyer.com/resources/criminal-defense/criminal-defense-case/what-a-summary-judgment-a-criminal-trial notes that there is no summary judgment, but there is a similar process through pretrial motions to dismiss; Chris Peloso said you can't appeal these, and maybe he's right in Alaska, but MA Rule 15 specifically discusses this whereas AK rules only mention interlocutory appeal once, in passing http://courts.alaska.gov/crpro.htm; in New York a denial of this pretrial motion can be appealed, giving the criminal another chance to get out [ http://www.davidrichlaw.com/new-york-business-litigation-and-employment-attorneys-blog/2010/11/may-i-immediately-appeal-in-new-york-from-an-order-denying-my-motion-to-dismiss-a-lawsuit/  - not clear if this can be appealed by the prosecutor but I guess so


## Bill of rights

Page 67 of Stuntz's book says that the Bill of Rights originally applied only to the federal government, as it did until well into 20th century? Reference to Whitney v. California (1927); however most state constitutions have these, but they were not highly enforceable.

  

