# Court procedure

Reservation of rights - ? Note that Effect on Surety of Obligee’s Release of Principal: A Critical Look at the Rules in the Restatement discusses it with regard to surety, even going so far back as to 1851. Describes the doctrine as follows:

Under the reservation of rights doctrine, the creditor is permitted to assert claims against the guarantor without regard to the release, and the guarantor then is permitted to recover from the debtor, despite the creditor’s release of the debtor.

Alaska Dispatch says that discovery rules are more vague in federal court: http://www.alaskadispatch.com/article/senators-alaska-seek-reform-us-department-justice?page=0,1

## Reform and frivolous litigation

Could always adopt the English rule (may happen with D&O litigation)

[H.R. 2655: Lawsuit Abuse Reduction Act of 2013](https://www.govtrack.us/congress/bills/113/hr2655) - caused a bit of stir when passed house

## Admissibility
Hearsay as noted in 5898 A-10719 Andrew v. State - which cites Rusenstrom v. Rusenstrom, 981 P.2d 558, 560-61 (Alaska 1999); Bird v.

Starkey, 914 P.2d 1246, 1248 n. 1 (Alaska 1996); Christian v. State, 276 P.3d 479, 489  (Alaska App. 2012); Savely v. State, 180 P.3d 961, 962 (Alaska App. 2008)

## Service of process
Read the first few pages of CIVIL PROCEDURE: FACEBOOK FRIEND OR FOE?: THE IMPACT OF MODERN COMMUNICATION ON HISTORICAL STANDARDS FOR SERVICE OF PROCESS—SHAMROCK DEVELOPMENT V. SMITH

Started by Rule 4 http://courts.alaska.gov/civ.htm also see http://courts.alaska.gov/serve.htm also see wiki

## Quirky
Directed verdict or Judgment as a Matter of Law per http://www.law.cornell.edu/rules/frcp/rule_50 - seems odd; frequently made because of a 7th amendment quirk per http://en.wikipedia.org/wiki/Judgment_as_a_matter_of_law - saw an example of this in Alaska case Wiersum v. Harder (2013 and rehearing petition in 2014), where I agree with dissent that it was inappropriate

## Discovery
According to http://blog.simplejustice.us/2009/04/24/criminal-summary-judgment-way-too-painless.aspx discovery is way weaker in criminal law than civil law, ironically; federal forms linked at http://samihartsfield.wordpress.com/2011/11/04/what-is-a-discovery-subpoena/; subpoenas are similar but basically for non-parties

## Civil Procedure
In civil lawsuits, there are pleadings and motions; complaints or petitions are the first pleadings, followed by an answer responding; defendants can also file cross-complaints; the pleadings ask for reparation according to http://en.wikipedia.org/wiki/Ad_quod_damnum or "according to the harm"

Alaska Rule of Civil Procedure 94 is "relaxation of rules" (could not find how often it has been used); some states such as New Jersey seem to have similar rules; however, the FRCP don't seem to have a similar rule;

Evidence-Based Federal Civil Rulemaking: A New Contemporaneous Case Coding Rule - discusses the history and evolution as well as terrible system for empirical research  

## Attorney fees
Per American rule (versus English), each party bears their own but see Alaska's English Rule:

Attorney's Fee Shifting in Civil Cases for an overview of exceptions and note that bad faith claims are exceptions with an unreasonable standard; Florida is also a notable exception as noted in "Are Surplus Lines Carriers Responsible for Attorney's Fees in Florida?" (see Florida Statute 627.428).

## Adversarial versus inquisitorial
Judges can't engage in ex parte communication or do ex parte independent research; the significance of this is notable when I saw 6743 S-14692 In Re Cummings (2013) where an Alaska judge was removed for suggesting a couple MOJs; also 2381 A-10584 Vent v. State, 288 P3d 752 where Judge Ben Esch did independent research

### Civil lawsuits

Collateral source rule allows those with health insurance to double collect for their special damages which are medical costs; note that general damages are often a multiple of the healthcare costs or might just be a multiple of the special damages

## Conduct
In Alaska, http://www.ajc.state.ak.us/conduct/conduct.html is pretty detailed