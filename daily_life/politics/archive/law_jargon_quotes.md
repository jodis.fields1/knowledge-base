Law jargon and quotes
===

Michael C. Dorf has an excellent overview http://writ.news.findlaw.com/dorf/20050803.html The Five-Minute Law School: Everything You Learn In Your First Year, More or Less

## Jargon
The "comes now" is funny, see http://transblawg.eu/index.php?/archives/1726-Comes-now-the-plaintiff.html

See http://en.wikipedia.org/wiki/List_of_Latin_phrases_%28full%29 - note I went through to "n" and put them in law quotes

Article on avoiding Latinisms has some for and some against: http://www.illinoistrialpractice.com/2005/08/i_dont_speak_la.html#comment-8544752

Stipulation is the agreement to certain facts (e.g. requests for admission)

Consent decree - basically a stipulated agreement supervised by the courts

## Quotes
[**Laws** control the lesser man... Right conduct controls the greater one.](http://thinkexist.com/quotation/laws_control_the_lesser_man-right_conduct/215710.html) - Mark Twain

[Tyrants have always some slight shade of virtue; they support the **laws** before destroying them](http://thinkexist.com/quotation/tyrants_have_always_some_slight_shade_of_virtue/181595.html) ” - Voltaire

Marriage quote by William Henry Maule - see Volokh on the  [most awesomest judge](http://www.volokh.com/2013/08/08/the-most-awesomest-judge/)

> There are so many hammocks to catch you if you fall, so many laws to keep you from experience. All these cities I have been in the last few weeks make me fully understand the cozy, stifling state in which most people pass through life. I don't want to pass through life like a smooth plane ride. All you do is get to breathe and copulate and finally die. I don't want to go with the smooth skin and the calm brow. I hope I end up a blithering idiot cursing the sun - hallucinating, screaming, giving obscene and inane lectures on street corners and public parks. People will walk by and say,  "Look at that drooling idiot. What a basket case." I will turn and say to them "It is you who are the basket case. For every moment you hated your job, cursed your wife and sold yourself to a dream that you didn't even conceive. For the times your soul screamed yes and you said no. For all of that. For your self-torture, I see the glowing eyes of the sun! The air talks to me! I am at all times!" And maybe, the passers by will drop a coin into my cup.
* http://thinkexist.com/quotation/there-are-so-many-hammocks-to-catch-you-if-you/347424.html - Henry Rollins

[We have the means to change the laws we find unjust or onerous. We cannot, as citizens, pick and choose the laws we will or will not obey.](http://thinkexist.com/quotation/we_have_the_means_to_change_the_laws_we_find/337377.html) - Ronald Reagan

[The United States is a nation of laws : badly written and randomly enforced](http://thinkexist.com/quotation/the_united_states_is_a_nation_of_laws-badly/186913.html) ” - Frank Zappa

**leges sine moribus vanae** laws without morals [are] vain - From [Horace](http://en.wikipedia.org/wiki/Horace)'s Odes: the official motto of the [University of Pennsylvania](http://en.wikipedia.org/wiki/University_of_Pennsylvania).

**misera est servitus ubi jus est aut incognitum aut vagum** miserable is that state of slavery in which the law is unknown or uncertain - Quoted by [Samuel Johnson](http://en.wikipedia.org/wiki/Samuel_Johnson) in his paper for [James Boswell](http://en.wikipedia.org/wiki/James_Boswell) on [Vicious intromission](http://en.wikipedia.org/wiki/Vicious_intromission).

**nitimur in vetitum** We strive for the forbidden - From [Ovid](http://en.wikipedia.org/wiki/Ovid)'s [_Amores_](http://en.wikipedia.org/wiki/Amores_%28Ovid%29) , [III.4:17](http://en.wikisource.org/wiki/la:Amores/3.4). It means that when we are denied of something, we will eagerly pursue the denied thing. Used by [Friedrich Nietzsche](http://en.wikipedia.org/wiki/Friedrich_Nietzsche) in his _[Ecce Homo](http://en.wikipedia.org/wiki/Ecce_Homo_%28book%29)_ to indicate that his philosophy pursues what is forbidden to other [philosophers](http://en.wikipedia.org/wiki/Philosophers).

nolle prosequi](http://en.wikipedia.org/wiki/Nolle_prosequi) 
*  to be unwilling to prosecute - A [legal motion](http://en.wikipedia.org/wiki/Motion_%28legal%29) by a [prosecutor](http://en.wikipedia.org/wiki/Prosecutor) or other [plaintiff](http://en.wikipedia.org/wiki/Plaintiff) to drop legal charges, usually in exchange for a [diversion program](http://en.wikipedia.org/wiki/Diversion_program) or [out-of-court settlement](http://en.wikipedia.org/wiki/Settlement_%28litigation%29).