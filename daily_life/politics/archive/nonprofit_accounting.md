# Nonprofit accounting

Main policies are FAS 116 and FAS 117

[FAS 116](http://www.fasb.org/cs/BlobServer?blobkey=id&blobwhere=1175820922799&blobheader=application/pdf&blobcol=urldata&blobtable=MungoBlobs "http://www.fasb.org/cs/BlobServer?blobkey=id&blobwhere=1175820922799&blobheader=application/pdf&blobcol=urldata&blobtable=MungoBlobs") (official link) is controversial, chair Dennis R. Beresford dissented due to its aggressive revenue recognition;  [Rob Fleming at Clark Nuber](https://www.clarknuber.com/people/fleming-rob.php "https://www.clarknuber.com/people/fleming-rob.php") ( [profile article](https://www.wac.net/2011/08/25/2912/ "https://www.wac.net/2011/08/25/2912/")) in WA pointed this out in  [New Accounting and Reporting Standards for Nonprofits - What You Need to Know](http://www.eskimo.com/~pbarber/fasb116.html "http://www.eskimo.com/~pbarber/fasb116.html") (link should be fixed to the new  [npofaq](http://nccsdataweb.urban.org/PubApps/nonprofitfaq.php?i=224&c=28 "http://nccsdataweb.urban.org/PubApps/nonprofitfaq.php?i=224&c=28"); sent email)  [echoed at blogpost](http://nonprofiteye.blogspot.com/2007/12/from-trenches-impact-of-fasb-116-117-on.html "http://nonprofiteye.blogspot.com/2007/12/from-trenches-impact-of-fasb-116-117-on.html") but nowhere else; 

ECFA Forum 2011 by Richard F. Larkin - see email to self

**Monthly reports and tips**

Incredibly helpful to see the monthly spend breakdown as it comes in

**Audit**

Looks like CoastAlaska actually posts the letter to the board, which is cool; see  [2013 statement](http://www.coastalaska.org/wp-content/uploads/2013/12/CoastAlaska-FY13-Final-Financial-Statements.pdf "http://www.coastalaska.org/wp-content/uploads/2013/12/CoastAlaska-FY13-Final-Financial-Statements.pdf")

## Accounting IT systems
[GoDBledger](https://opensource.com/article/20/7/godbledger)

QuickBooks - installed on my PC

BlackBaud - seems to be pretty impressive

long list of stuff at  [A Few Good Accounting Packages](http://www.techsoup.org/support/articles-and-how-tos/few-good-accounting-packages "http://www.techsoup.org/support/articles-and-how-tos/few-good-accounting-packages") which notes Peachtree available thru TechSoup

Xero sounds good

PeachTree plus Crystal Reports sounds really good

Calxa is sorta intriguing

## Overhead

See email to finance committee; also look at  [Will foundations fund overhead or administrative costs for nonprofits? What is an acceptable overhead rate?](http://grantspace.org/Tools/Knowledge-Base/Funding-Research/Proposal-Writing/overhead-costs "http://grantspace.org/Tools/Knowledge-Base/Funding-Research/Proposal-Writing/overhead-costs") from there first link shows ratios ranging from 40% (American Institute of Philanthropy) to 20% (United Way); fourth link was about the  [Nonprofit Overhead Cost Study](http://nccsdataweb.urban.org/knowledgebase/index.php?category=51 "http://nccsdataweb.urban.org/knowledgebase/index.php?category=51"); 

Charity Navigator's use of these ratios is at  [Financial Ratings Tables](http://www.charitynavigator.org/index.cfm/bay/content.view/cpid/48.htm#.Ul8UI1Mb_N4 "http://www.charitynavigator.org/index.cfm/bay/content.view/cpid/48.htm#.Ul8UI1Mb_N4")

[Overhead Myth: Thoughts from a Nonprofit Attorney](http://www.nonprofitlawblog.com/home/2013/07/nonprofit-overhead-myth-thoughts-from-a-nonprofit-attorney.html "http://www.nonprofitlawblog.com/home/2013/07/nonprofit-overhead-myth-thoughts-from-a-nonprofit-attorney.html") - good thoughts from nonprofitlawblog.com

Best overview so far:  [ http://www.nonprofitaccountingbasics.org/topic/grants](http://www.nonprofitaccountingbasics.org/topic/grants "http://www.nonprofitaccountingbasics.org/topic/grants")

Best tutorial:  [ http://www.accountingcoach.com/nonprofit-accounting/index3.html](http://www.accountingcoach.com/nonprofit-accounting/index3.html "http://www.accountingcoach.com/nonprofit-accounting/index3.html") has some introduction stuff including adjusting entries

2011 article http://www.cathedralconsulting.com/files/TOPIC%20CashVsAccrual%20April%202011.pdf suggests that cashflow accounting is common; somewhat surprising

http://www.grantspace.org/Tools/Knowledge-Base/Nonprofit-Management/Accountability/Nonprofit-audits discusses audits and has a bunch of links

http://www.centerfornonprofitexcellence.org/files/Accounting%20for%20Grants%20Toolkit%20Revised%2011-2011.pdf is a good overview

Per http://www.clintonfoundation.org/main/clinton-foundation-blog.html/2013/08/16/an-open-letter-from-bill-clinton/ the Clinton Foundation also had issues with booking all its revenue when committed

## Prior research on related parties

See  Wiley Not-for-Profit GAAP 2011:  Interpretation and Application of Generally Accepted Accounting Principles and search for "related party" to see an overview of what a related party is.

The Sarbanes-Oxley Act and Implications for Nonprofit Organizations

See  [http://www.aicpa.org/Research/Standards/CodeofConduct/Pages/default.aspx](http://www.aicpa.org/Research/Standards/CodeofConduct/Pages/default.aspx) for auditor code of conduct

See  [http://www.sao.state.ut.us/localGov/newsLetters/05Febnewsletter.pdf](http://www.sao.state.ut.us/localGov/newsLetters/05Febnewsletter.pdf) for discussion on auditor independence from Utah State Auditor

http://apps.americanbar.org/buslaw/newsletter/0086/materials/pp1.pdf for discussion of codification for lawyers

CPAs’ Perceptions of Auditor Independence: An Analysis of Views Before and After the Collapse of Enron

Auditing  [ http://www.enotes.com/auditing-reference/auditing-174116](http://www.enotes.com/auditing-reference/auditing-174116)

Party-In-Interest transactions primer from AICPA Audit Quality Center: see http://www.offhauscpa.com/EBPAQC_PII_Prohibited_Transactions_Primer.pdf