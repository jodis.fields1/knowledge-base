Government general
===

http://www.granicus.com/Solutions/Solutions-Overview.aspx Granicus provides transparency to citizens; found through Python developer http://www.linkedin.com/in/chrisjerdonek

http://freegovinfo.info/about is pretty cool

## Ballotpedia
More of a politician place actually

## Technology effectiveness
[Lessons learned from my time at the CFPB](http://radar.oreilly.com/2014/01/lessons-learned-from-my-time-at-the-cfpb.html) - good article

How Tech Giants Botched Obamacare (2014) - why can't they just require the source to be open and share ??

Commercial off-the-shelf ( [wiki](https://en.wikipedia.org/wiki/Commercial_off-the-shelf)) - ran into this thru Casebook which has its prices set in a GSA schedule as it is "COTS"

## Transparency and finances
openthebooks.com - found thru OpenGov Voices: Is “sue them” the next “best disinfectant?” (2014)

## Gov databases
http://wikis.ala.org/godort/index.php/State_Agency_Databases - listing and discussing the databases inside government; GODORT is the Government Documents Round Table of the American Library Association

ALA's Alaska section is run by Daniel Cornwall http://www.linkedin.com/in/danielcornwall

## State government
See http://www.stateinnovation.org Center for State Innovation for a nonprofit focused on this
