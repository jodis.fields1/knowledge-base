# Persuasion
This is an important skill. Noticed  [ http://www.changeminds.org/](http://www.changeminds.org/ "http://www.changeminds.org/") in my bookmarks. Also have some books to review.

## General
### Deep diving
* PUA strategy per  [girlschase deep diving article](http://www.girlschase.com/content/secrets-getting-girls-art-deep-dive)
* http://en.wikipedia.org/wiki/Persuasion

### Techniques
Flattery is also important

https://web.archive.org/web/20200421230305/https://listverse.com/2013/02/03/10-psychology-tricks-you-can-use-to-influence-people/ - great overview

Repeat back what people are saying also known as reflective listening (see http://www.ncbi.nlm.nih.gov/pubmed/17760322

Don't correct people: use the Ransberger Pivot

[To change attitudes, dont argue — agree, extremely: What if the best way to change minds isnt to tell people why theyre wrong, but to tell them why theyre right?](http://www.reddit.com/r/science/comments/2blfkl/to_change_attitudes_dont_argue_agree_extremely/) - reddit link on study which took views to their logical extreme

Backfire effect - seems to originate from Nyhan and Reifler's  [When Corrections Fail: The Persistence of Political Misperceptions](http://link.springer.com/article/10.1007%2Fs11109-010-9112-2) (2010)

Eye contact may make people more resistant to persuasion - particularly those who disagree with you

How politics makes us stupid (2014) by Ezra Klein - notes Kahan's research on resistance to facts

[Study shows strongly held incorrect beliefs often cannot be changed by disputing facts. Instead, appealing to the sense of self can allow people to be more open-minded.](http://reddit.com/r/science/comments/262n45/study_shows_strongly_held_incorrect_beliefs_often/) (2014) - reddit link; "duh"

## Speeches and public speaking
Lost Art of the Great Speech - recommended book, picked up from Parenthood

For tips on speech-writing, see some of Nixon's speeches. Also see http://en.wikipedia.org/wiki/Citizenship_in_a_Republic for a Teddy Roosevelt speech

A Time For Choosing - famous Reagan speech which I watched in 2014-04

Per Herb Perone's August 2012 advice on How To Meet the Press (see:

> Learn "the bridge" to transition topics. Discussed in detail at  [Simple Techniques for Media Interviews](http://aboutpublicrelations.net/ucmillen1.htm). Also tell the "rest of the story" to reporters with "what's really important" as a leader. Have someone around who can verify on/off record.

## Body language
* Mirroring in general
* Smiling
* Nod

### Articles
* License or obligation to smile: The effect of power and sex on amount and type of smiling  - highly-cited 
* Breaking the Rules to Rise to Power How Norm Violators Gain Power in the Eyes of Others - also interesting
* Smiles, Speech, and Body Posture: How Women and Men Display Sociometric Status and Power

## Books

Influence: The Psychology of Persuasion- 1984 classic now in its 5th edition; ebook available

Conversationally speaking : tested new ways to increase your personal and social effectiveness - another classic but not so available

## Political persuasion
[Seven Movies That Changed People’s Political Views](http://www.slate.com/articles/health_and_science/climate_desk/2014/01/movies_that_influenced_political_views_all_the_president_s_men_malcolm_x.2.html) - fascinating Slate article

http://www.rawstory.com/rs/2013/04/11/science-discovers-magic-trick-that-causes-partisan-voters-to-switch-parties/
