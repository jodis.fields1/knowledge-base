# post-2018
noticed lack of state court cases in 2009, emailed Malamud about state court cases, and he suggested:
> find a jurisdiction that is not on-line properly ... xerox or harvest or otherwise get a comprehensive collection of their docs, maintain them on a web site, write summaries of their decisions, etc ... they would take you very seriously

# Courts general (incl Supreme)

See also Court Documents and various other pages which come up in a search for court

http://www.nytimes.com/2005/04/17/magazine/17CONSTITUTION.html?_r=1 is an interesting article on the "Constitution in Exile" movement began by Richard Epstein


## Supreme Court
Denies writs based in its term orders at http://www.supremecourt.gov/orders/ordersofthecourt.aspx and sometimes publishes comments; Solicitor General provides recommendations

Top reason for granting writ of certiorari is disagreement among circuits

## Statistics

Annual report http://www.uscourts.gov/Statistics/JudicialBusiness/2012/judicial-caseload-indicators.aspx (motivated by the list of tax evasion cases at http://en.wikipedia.org/wiki/Tax_evasion_in_the_United_States); more notably, Justia has a great way to browse the dockets by particular categories http://dockets.justia.com/browse/circuit-9/state-alaska/noscat-1/; also there's a Fulbright & Jaworski  9th Annual Litigation Trends Survey summarized at http://www.insurancejournal.com/news/national/2013/03/04/283224.htm;

Rice has a state supreme courts database at http://www.ruf.rice.edu/~pbrace/statecourt/;

## Districts

9th district - all of west coast, most controversial and largest by population etc

6th circuit - michigan and ohio down to tennessee is a weird bag, reputation for being reversed lately

5th circuit - TX and LA, known as conservative, good profile about chief judge Stewart at Meet the chief judge of the nation’s most divisive, controversial and conservative appeals court (2014)

## Accountability auditing and supervision
Alaska Judicial Observers - great program

Courts are relatively independent so this is a mystery; read Appellate Court Supervision in the Federal Judiciary: A Hierarchical Perspective (2003); District and circuit court judges enjoy life tenure

Every Day Is a Good Day for a Judge to Lay Down His Professional Life for Justice by Weinstein (2004) - easy for a federal judge to say

The dumbing down of America's judges - John Lott thinks so

Assignments should be random but sometimes aren't per  [Non-Random Assignment of Federal Cases and Bank of America ](http://www.theracetothebottom.org/miscellaneous/non-random-assignment-of-federal-cases-and-bank-of-america.html)(2010) which refers to an SSRI paper

### Elections

Judges are often elected or have votes for retention; see judgepedia for bios and stuff; http://www.adn.com/2010/10/28/1524477/allies-rally-around-fabe-as-justice.html has some statistics and says 3 percent removed by vote; trend to shift away from merit-based nomination to straight governor nomination with legislative approval per http://www.usatoday.com/story/opinion/2013/03/27/state-court-gridlock-brownback-column/1991337/;

Use ballotpedia as a basis but also http://www.therobingroom.com has potential

### Procrastination

In Alaska judges don't get paid (as much?) if they don't complete cases within 6 months which is cool

2013-05: Motivated by a story of late filing of a brief (http://www.volokh.com/2013/05/13/lawyers-beware/) I looked into this and found relatively little: http://www.ncjj.org/pdf/tabulletintimelycaseprocessing.pdf for juvenile justice; Indiana Trial Rule 53.1-2 which are the "lazy judge" rules; also looked at articles citing Case for Judicial Disciplinary Measures, The (1965) but none seemed very specific except for one focused on the Philippines, most highly-cited was Informal Methods of Judicial Discipline (1993);

California Superior Courts in Crisis (2013) - "stipulated civil divorce that used to take between a month and six weeks now takes up to five months to turnaround" is just crazy; did some reading on their election procedure motivated by Alarcon (see Department 36: The Judge Is a Bobble-Head, His Clerk an Unbridled Dominator in Metropolitan News-Enterprise) and found 

## Precedents

A case can be of first impression http://en.wikipedia.org/wiki/First_impression_%28law%29 meaning that it decides on new things in law

### Decisions

funny ones at http://googlescholar.blogspot.com/2010/07/entertaining-legal-opinions.html;

## Notable people advocating for modernization

Did a search for "modernizing state courts" and came up with a lot of interesting stuff

http://legal-dictionary.thefreedictionary.com/Judicial+Administration led me to NCSC

National Center for State Courts (NCSC) is the leader ncsc.org

### Judges

http://sentencing.typepad.com/sentencing_law_and_policy/2012/07/some-notable-recent-commentary-on-judging-and-modern-technology.html mentions judges citing bloggers, but nothing more

### Agitators
Carl Malamud of public.resource.org is an obvious proponent, but not very influential

### History

Grace Beatrice Brown Jenkins of Wyoming did a bunch of work back in the day thru a citizens' committee http://trib.com/opinion/columns/modernizing-wyoming-s-courts/article_abd1fcc5-fcce-5b2d-958c-b1c3ceda055b.html

In South Carolina, the "Judicial Automation Project" involved a "Strategic Technology Plan" which I reviewed http://www.judicial.state.sc.us/judauto/stratplan.cfm and it had a good survey of courts

In 2009 Pawlenty advocated for reform of courts http://minnesota.publicradio.org/display/web/2009/02/25/pawlenty_courts/

Texas Strategic Plan Fiscal Years 2013 -2017 http://www.courts.state.tx.us/oca/Strategic_plan/stratplan13-17.pdf describes an extremely splintered system

Modernizing court administration: The case of the Los Angeles superior court (1971) - doesn't have too much information

Leading the Unfinished Reform:The Future of Third Branch Administration (2005) - starts off really wishy-washy and boring, but then gets into the history which is cool; lists organizations noted below

A Quiet Revolution in the Courts: Electronic Access to State Court Records: A CDT Survey of State Activity and Comments on Privacy, Cost, Equity and Accountability (2005) - https://www.cdt.org/publications/020821courtrecords.shtml has a list of what each state does; amusing how optimistic they are

_Organizations_

American Judicature Society - started in 1913; old court reform org; known for advocating merit selection

American Bar Association Minimum Standards on Judicial Administration - sets minimum standards; started by Vanderbilt (ABA President 1937-8)

Institute of Judicial Administration - started in 1938 by Pound and Vanderbilt;

Model Act to Provide for an Administrative Office of State Courts - 1948

Conference of Chief Justices - started in 1949

National Council of Juvenile and Family Court Judges - not really as associated with what I'm interested in

## Public Access to state courts  

Searched on PACER equivalent for state courts and found http://legalresearchplus.com/2011/05/12/the-existential-exercise-of-finding-state-court-materials-online/ (cited by http://ziefbrief.typepad.com/ziefbrief/2011/05/state-court-materials-online-it-depends.html)

http://www.ncsc.org/Topics/Access-and-Fairness/Privacy-Public-Access-to-Court-Records/Resource-Guide.aspx \- click on State Links to see more specific links to states' policies

http://bulk.resource.org/courts.gov/ doesn't have anything for states yet

Electronic Access to Court Records by The Reporters Committee for Freedom of the Press (2007) - see http://en.wikipedia.org/wiki/Reporters_Committee_for_Freedom_of_the_Press

http://courts.delaware.gov/opinions/List.aspx?ag=Superior%20Court allows you to read Superior Court opinions in Delaware

A Quiet Revolution in the Courts: Electronic Access to State Court Records (2002) - notes that DE has it; also First and Second District in Florida

The Other California Public Records Law - restrictive public records law in California

## Metrics

See NCSC in general, but also http://www.courtools.org for more details

https://judicialview.com/ has a lot of news

_Number of trials_
Quick glance garnered "The Jury Trial Implosion" (2011) and "Examining Trial Trends in State Courts" plus BJS Data Collection: Court Statistics Project (CSP); decline in trials but more overall lawsuits; summary judgment and ADR make up a large portion of the change

## ADR
Court-faciliated ADR list at http://www.ncsconline.org/WC/Publications/ADR/SearchState.asp