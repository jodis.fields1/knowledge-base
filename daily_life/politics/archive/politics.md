Politics
===

GetOutOfManhattan project
* https://twitter.com/jcrben/status/1433936349891944448
* create a nextjs page for this idea?

## New
YangGang
* unsubscribed from Yang discord at https://disboard.org/server/469312502187032576 (see https://disboard.org/servers/tag/yang)

https://thegoodparty.org/

Focused on approval voting but left r/ApprovalCalifornia

Unsubscribed from r/SandersForPresident & r/Political_Revolution

## Polls

electionprojection - seems to be the best place to get details on obscure polls, for example  [2014 Alaska Gubernatorial poll](http://www.electionprojection.com/latest-polls/2014-alaska-governor-polls.php) is there

Gallup (half cellphone), Rasmussen (leans Republican), Public Policy Polling are all Ok but PPP only uses landlines

YouGov - new innovative online pollster which Stanford professor Doug Rivers helps run

2014-02: reviewed  Gallup time-series on party affiliation; also see  Poll: Do GOP Voters really want Arizona Gov. Brewer to veto anti-gay bill? where I learned about The Art of Asking Questions (Princeton University Press, 1951), pp. 7-8, cited and discussed in David W. Moore, The Superpollsters: How They Measure and Manipulate Public Opinion in America

[How Cellphones Complicate Polling](http://bits.blogs.nytimes.com/2012/11/12/how-cellphones-complicate-polling/) (2014) says "A 1996 federal law states that calls to cellphones must be hand-dialed, not generated by computer"

## Trends
Nigel Farage of Britain's ultra-right UKIP party went from 3% to 23% in one election - might say something about the future

## Comparative politics

Looked into Canada's parliamentarian political system a bunch - see that note for details, but basically it is very personality-based given how Trudeau caused a huge change in support

## Money and campaigning

527 groups a key and unlimited personal contribs were created by SpeechNow.org v. FEC and not Citizens United

Alaska limits at http://doa.alaska.gov/apoc/faqs/faq-contribution-limits.html and described at http://doa.alaska.gov/apoc/Commission/history.html

In 2014-08 IRS agreed to a settlement whereby it will police religious nonprofits (501(c)(3) orgs) for electioneering

_Citizens United_
based on discussion with Neil 2014-06 it is a little unclear; several sources e.g. national council of nonprofits says it does not affect charities (501(c)(3)) and narrowly affects 501(c)(4) (see email to Neil 2014-06-24) but opensecrets.org says that (c)(4) groups "couldn't directly involve themselves in political campaigns. And they had to identify their funders to the public" prior to _Citizens United_  I guess

_Huge donors_ 

Charles' children and found [Chase](https://en.wikipedia.org/wiki/Chase_Koch) killed someone in his car as a driver and works at the company while Elizabeth started a publishing company (see Adventures in publishing: a surprisingly whimsical project flows from the brain of a Koch daughter)

## Donor databases

FEC - of course  [use advanced](https://www.fec.gov/data/)

influenceexplorer.com - pulls federal from opensecrets.org and state from followthemoney.org

State politics - http://www.followthemoney.org/ is awesome or http://ballotpedia.org/wiki/index.php/Campaign_finance_websites for particular states


APOC Campaign Disclosure -  [search by candidate](https://aws.state.ak.us/ApocReports/CampaignDisclosure/CDForms.aspx) and then look at a filng to see names of contributors as well as expenditures

https://opensecrets.org - seems to be missing more than some others

https://followthemoney.org - good

### Outdated
http://www.campaignmoney.com - basic but seems to have a pretty good list

## Gerrymandering

Bloomberg Businessweek has run some good articles on gerrymanderingin 2013 when California implemented an independent redistricting commission; California legislature and politics wiki articles talk about it a bit for example http://ballotpedia.org/wiki/index.php/Redistricting_in_California and the Ribbon of Shame; http://en.wikipedia.org/wiki/Politics_of_Texas#Redistricting_Disputes_and_the_1990s provides a good example where Texas Democrats lost the Congressional elections but retained the legislature

Had read up on this but didn't document it; doing more looking into it, I found Is Partisan GerrymanderingUnconstitutional? (2011 ProPublica) which referred to the high standard of Davis V. Bandemer (1986) under the Burger court but I don't think this is what I saw earlier, note that the Wikipedia gerrymanderingpage doesn't even mention Davis

I think what I saw earlier was Baker v. Carr (1962) under the Warren court per http://jostonjustice.blogspot.com/2011/03/supreme-courts-feckless-surrender-to.html


## Redistricting and reapportionment rules main reapportionment 
[wiki page](http://en.wikipedia.org/wiki/United_States_congressional_apportionment) and  [ballotpedia page on 2010 change](http://ballotpedia.org/State_Legislative_and_Congressional_Redistricting_after_the_2010_Census) which happens after every census. Note  [H.R. 2758: Redistricting Reform Act of 2013](https://www.govtrack.us/congress/bills/113/hr2758) from Zoe Lofgren would be good but no chance of passing

See  [The United States of America: Reapportionment and Redistricting](http://aceproject.org/ace-en/topics/bd/bdy/bdy_us) which notes the 1930 rule "method of equal proportions" established based on 1930 NAS report 

After the 2000 census, in 2002 Republicans gained ground so some states did some redistricting a few years later like in 2003 or so, such as Texas and Colorado - Colorado's was struck down by the  [CO Supreme Court](http://www.nytimes.com/2003/12/02/national/02REDI.html) and  [Redistricting: the wars get more frequent](http://www.csmonitor.com/2003/0529/p02s02-uspo.html/%28page%29/3) covers it too

[Taking the “Re” out of Redistricting: State Constitutional Provisions on Redistricting Timing](http://georgetownlawjournal.org/files/pdf/95-4/levitt_mcdonald.pdf) (2006) also discusses it and notes it went to the Supreme Court in League of United Latin American Citizens v. Perry; p34 has a nice table showing how each state works

ACLU article on Everything You Always Wanted to Know About Redistricting But Were Afraid to Ask explains precisely how the 10% deviation works: divide population by total number of seats for ideal size, and then look at smallest and largest population to determine the deviation 

## Municipalities
http://en.wikipedia.org/wiki/Dillon_Rule#Dillon.27s_Rule Dillon's Rule is relevant - basically states tend to have ultimate power over their municipalities

## Legislature and professionalization
http://frontloading.blogspot.com/2011/01/frontloading-starts-with-state.html has calendar schedule (based on http://www.ncsl.org/legislatures-elections/legislatures/2011-legislative-session-calendar.aspx) also see http://www.ncsl.org/legislatures-elections/legislatures/full-and-part-time-legislatures.aspx for list of professional legislatures; there's a lot of research e.g. Legislative Professionalism and Influence on State Agencies: The Effects of Resources and Careerism and

Full-Time, Part-Time, and Real Time: Explaining State Legislators' Perceptions of Time on the Job surveys how much work actually goes in

California has had a professional legislature for a while; Texas retains a biennial legislature, which is the subject of  Texas Stands Against Tide in Retaining Biennial Legislature http://www.nytimes.com/2010/12/31/us/politics/31ttlegislature.html however they hold hearings year-round and get paid almost nothing
