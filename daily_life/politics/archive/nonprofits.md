# Nonprofits

Classification system: http://nccs.urban.org/classification/NTEE.cfm National Taxonomy of Exempt Entities (NTEE); 

https://en.wikipedia.org/wiki/Open_Philanthropy - interesting

## Websites
National Council of Nonprofits is a trade association; Nonprofit Resource Center is a potential resource http://www.nprcenter.org/; http://www.usa.gov/Business/Nonprofit.shtml

FoundationCenter topical resource list http://www.foundationcenter.org/getstarted/topical/foundroles.html has a lot of stuff

National Center on Nonprofit Enterprise (published financing nonprofits book below) has a good list of articles at http://www.nationalcne.org/index.cfm?fuseaction=category.display&category_id=22

## Regulation
Independent Sector is sort of a regulator - list of self-regulatory standards at http://www.independentsector.org/compendium_of_standards;

CharityNavigator, Guidestar, and Givewell de facto regulate; CharityWatch is more explicit and also evaluates 501(c)(4)s such as ACLU and Sierra Club

Bad nonprofits: Tampa Bay Times and The Center for Investigative Reporting found 50 worst and also found Copilevitz & Canter represents three-quarters of them

## Foundations and grants
[Key Facts on Community Foundation](http://foundationcenter.org/gainknowledge/research/pdf/keyfacts_comm2012.pdf) | Foundation Center (2012) - total 2010 giving was $46 billion; 10% community, $33b independent, $5b corporate, $4.3b operating

[Top 100 U.S. Foundations by Asset Size](http://foundationcenter.org/findfunders/topfunders/top100assets.html) - decent list

Statistics: see  [tracking the field report](http://www.issuelab.org/resource/tracking_the_field_volume_3_exploring_environmental_grantmaking) on environmental grant reports

Excerpt from FoundationFundamentals is good http://foundationcenter.org/getstarted/onlinebooks/ff/text.html;

[Accounting Practices Committee Library of Documents](http://www.cof.org/files/Documents/Community_Foundations/Professional_Groups/FAOG/AcctAlerts/APCManual.pdf ) - from Fiscal and Administrative Officers Group of Council on Foundations

Recasting the Relationship Between Foundations and Nonprofits (2013) - "prevailing nature of the foundation-nonprofit relationship is, in my experience, best described as the nonprofit community serving as a pool of potential contractors to the foundation community"

IRS Internal Revenue Manual (IRM) Part 7 Chapter 26 is on Private Foundations; see section 6 on private operating foundations http://www.irs.gov/irm/part7/irm_07-026-006.html;

Financing nonprofits: putting theory into practice (2007) is a good book - looked at Table 3.2 on page 49 to see grants by subject area which shows that environmental grants are not really a big part, education is around 20%, medical around 20%, human services 15%, arts and culture around 12%; page 56 mentions that around half of foundations often or sometimes do foundation-designed as opposed to unsolicited proposals; 59-60 contrasts Mellon's approach (fellowships for scholars and scholarships for students) with Ford's approach which funds organizations and oversees the projects and discusses 4 types such as general altruist, mission-driven etc;

Who’s making global civil society: philanthropy and US empire in world society - 2011 overview article

Also looked at Basic Facts about Charitable Giving OTA Paper 95 by Joulfaian http://www.treasury.gov/resource-center/tax-policy/tax-analysis/documents/ota95.pdf which was followed by http://www.philanthropy.iupui.edu/research-by-category/basic-facts-about-charitable-giving-from-the-center-on-philanthropy-panel-study;

Nonprofit Competitive Advantage in Grant Markets: Implications of Network Embeddedness (2011) - not too surprising that relationships

[The $13 Billion Mystery Angels](http://www.businessweek.com/articles/2014-05-08/three-mysterious-philanthropists-fund-fourth-largest-u-dot-s-dot-charity) (2014) - fascinating story about quant hedge fund guys, mostly skew liberal

Bernard Osher - founded one of largest savings institutions (merged with Wachovia); nearly dead but major donor

Druckenmiller - Soros partner also a major donor who also does environmental causes

## Statistics
See http://philanthropy.com/section/Facts-Figures/235/ - I read the religion and giving section (around 5% with religion versus 1% without)

Largest per history below as of 2004 were Harvard, Stanford, Yale, Howard Hughes, CommonFund, Kaiser Foundation Hospitals, etc)

## History
2014: glanced at A History of the Tax-Exempt Sector: An SOI Perspective which has stats at the end

## Donate to
Givewell - used to subscribe to their blog https://blog.givewell.org/

Sunlight Foundation- awesome work, a lot of that type of work is funded by Knight Foundation with overview at  [Sunlight Foundation: New Knight Foundation Report Reveals How Civic Tech is Funded](http://sunlightfoundation.com/blog/2013/12/04/new-knight-foundation-report-reveals-how-civic-tech-is-funded/)

Sam Adams Alliance - also does good work

## Finding
http://reic.uwcc.wisc.edu/issues/ has some fascinating data

CharityNavigator, Guidestar (which has a UK and India group) are OK for navigating nonprofits; also see http://greatnonprofits.org/ for crowdsourcing as well as crowdrise.org and http://projects.propublica.org/nonprofits/ nonprofit explorer

National Center for Charitable Statistics NCCS Database http://nccs.urban.org from the Urban Institute is likely the best source on financial data, also see  [NCCS Web Tools](http://nccsweb.urban.org/)

Also see http://www.intelligentphilanthropy.com/about which is available by subscription

Canada's Canada Revenue Agency publishes the 990 equivalent which are known as Annual Information Return (T3010) at http://www.cra-arc.gc.ca/chrts-gvng/lstngs/menu-eng.html; From Stories to Evidence: How Mining Data Can Promote Innovation in the Nonprofit Sector discusses this

UK in England and Wales has the Charity Commission per http://www.charity-commission.gov.uk/

Givewell.org evaluates nonprofits pretty well

The Global Journal has a top 100 NGOs article http://www.theglobaljournal.ch/product.php?id_product=78

Forbes has a list of largest in the United States

## Innovative
B Corporation http://www.bcorporation.net

Freelancers Union - revolutionizing health insurance

## Foundations
http://en.wikipedia.org/wiki/List_of_wealthiest_foundations - these provide some of money behind nonprofits

## Political
See dedicated note above

## Think tanks
See US Congress note

2013-04: did some research; found About Us | Think Tanks and Civil Societies Program (also see http://en.wikipedia.org/wiki/Think_Tanks_and_Civil_Societies_Program) at UPenn by Dr. James McGann which creates the Go To Think Tank Index; found out Brookings Institution is #1

Also read up on Heritage Foundation background with Needham and Heritage Action at A 31-Year-Old Is Tearing Apart the Heritage Foundation (2013)

## Charity
Malaria is a major problem; may be eliminated per 2013 report The Stability of Malaria Elimination in the journal Science

[Celebrated charities that we don’t recommend](http://blog.givewell.org/2009/12/28/celebrated-charities-that-we-dont-recommend/) (2011) - good Givewell overview, see also  [ Mega-charities](http://blog.givewell.org/2011/12/28/mega-charities/) (2011)

## Overpopulation
See population_growth_demographics.md

Look into federal U.S. money going into population

Population Services International - vary large with ~$700 million budget; not on Charity Navigator (!?); Givewell evaluated

Population Council - biggie with $45 million budget; leader is way overpaid; no Givewell analysis

United Nations Population Fund (UNFP) - big influence

Population Connection - United States-based education group

Population Action International - ??

Population Research Institute - more people! conservative group

The Population Institute - tiny and under Charity Navigator radar

Population Media Center

Family Health International - seems to have diversified somewhat

## Other charity
Love Inc - dad wants me to donate to them, Christian

Compassion International - huge adopt-a-child charity, competes with Christian Children's Fund now known as ChildFund

Save the Children - focused on children

World Vision - ditto
CARE International - donated to them for a while but still have a nebulous understanding of how effective they are

Partners In Health - low admin ratio, hires local workers, sounds good

Goodwill - emailed about finances 2009-12; said they had 166 stores and directed me to http://www.goodwill.org/about-us/newsroom/press-kit/ for Goodwill Industries International financials.

Juneau Cooperative Christian Ministry (Glory Hole)

Project Prevention - 
* I emailed them a while back about their Guidestar profile (subject: Address (CT or NC)) - in Guidestar as CHILDREN REQUIRING A CARING KOMMUNITY; 
* National Association of Pregnant Woman is a critic http://www.youtube.com/watch?v=8sbnDjj7WbU; also read some criticism at http://www.nydailynews.com/life-style/health/group-pays-drug-addicts-sterilized-receive-long-term-birth-control-sparks-criticism-article-1.1075432?pgno=1 from Soros affiliate and a medical ethicist; 
* http://blog.practicalethics.ox.ac.uk/2010/04/embrace-the-controversy-lets-offer-project-prevention-on-the-nhs/ suggests a reversible pilot program (e.g. depo shots); 
* http://articles.latimes.com/2009/oct/17/local/me-banks17 discusses specific statistics with families with 5+ children making up "2.5% of the department's caseload. But their 1,314 kids account for 8.3% of the 15,853 children in foster homes"; "The toughest easy money an addict ever earned" in the Oregonian led me to "Why caring communities must oppose CRACK/Project Prevention" (2003) which is cited by more contemporary articles

## Journalism / investigation
[Center for Pubic Integrity](https://en.wikipedia.org/wiki/Center_for_Public_Integrity) - originally launched International Consortium of Investigative Journalists (ICIJ); noticed [Accountability in Your State](https://publicintegrity.org/topics/state-politics/accountability-in-your-state/); "no other organization had shined “so many probing flashlights into so many Washington dirty-laundry baskets.” Over the years, it has churned out more than 500 major investigations, won dozens of national prizes, and served up scores of scoops" per https://archives.cjr.org/feature/something_fishy.php

Vox -

ProPublica -

## General advocacy
Alliance for Justice - Wikipedia, the free encyclopedia - relatively significant coalition

Center for Economic and Policy Research - best economics nonprofit out there; run by Dean Baker and Mark Weisbrot

Government Accountability Project (GAP) - responsive to emails; however, in 2009-07 I emailed them about their lack of financials; response was OK but seems they stay pretty behind (David Rosen davidr@whistleblower.org). In 2011 I emailed them about not posting a secret hold problem on their website after hyping it in emails, Dylan  Blaylock, comm. director, responded with thanks.

Demand Progress - believe this is affiliated with Bold Progressives; not sure.

Change.org - seems good so far; similar to Progressive Secretary (ran by Jim Harris - great guy; died in 2010 or so)

Public Citizen - lots of spam; kept telling them they have to have a good online system; in early 2010, Lindsey Pullen said it would debut in 2-3 months. In 2012 as I write this, no interactive website. Did not renew for 2012.

Alaska ACLU - could not find where the money goes; 2009-06 email was not responded to.

Participatory Politics Foundation by David Moore - formerly ran OpenCongress whose  website was a bit crowded earlier on and I sent in a lot of feedback (not sure where the email is); also sent $50; got response from David Moore at the Participatory Politics Foundation.

## Consumer
Gluten-Free Certification Organization (GFCO) - parent is Gluten Intolerance Group of North America; sent email in 2010; response from Channon Quinn (director channon.quinn@gluten.net): gluten.net is main website; gfco.org is searchable list; followed up with email about donation concerns: had trouble finding a list of products online and disclosure of parent group name was lacking. No discussion of the Scientific and Professional Board. Cynthia Kupper (exec director cynthia@gluten.net) responded very nicely. Ivy Ostenberg was also nice
* also emailed them about RSS feed issues

ConsumerReports.org (Consumers Union)

National Foundationfor CeliacAwareness - has GREAT (Gluten-Free Resource Education and Awareness Training) Kitchens Amber Designation which Domino's worked with when making its GF pizza; also runs a list of gluten-free manufacturers http://www.celiaccentral.org/shopping/gluten-free-food-manufacturers/ 

## Misc
Privacy Rights International