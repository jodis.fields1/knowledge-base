Court technology
===

## California
California's court technology is a basket-case but plans to reform per [California Aims to Modernize Court Tech Further with New Plan](https://statetechmagazine.com/article/2018/12/california-aims-modernize-court-tech-further-new-plan) which discusses https://www.courts.ca.gov/documents/jctc-Court-Technology-Strategic-Plan.pdf and also see [Tactical Plan for Technology 2019–2020](https://www.courts.ca.gov/documents/jctc-Court-Technology-Tactical-Plan.pdf)