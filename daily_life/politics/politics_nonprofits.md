Political nonprofits and other orgs
--- 

Also see archive/nonprofits.md for old notes

Sunlight Foundation - main player

Ballotpedia - 

Participatory Politics Foundation - I donated to them for OpenCongress in 2010
* lots of cool projects listed at http://www.participatorypolitics.org/projects/
* head David Moore personally responded

Project Vote Smart is from way back in 1992; has a great database; 
* has some shadiness with founder Kimball; on the decline per http://techpresident.com/news/21821/project-vote-smart-might-sell-ranch
* now sells their API at prices per http://votesmart.org/share/api/register#.URV_BfIhQZ0
* suggested they open-source in 2018-04
* https://votesmart.org/galaxy/ - a bit fancy

Center for Responsive Politics which runs OpenSecrets.org is a newer and different one

OnTheIssues is another one

## comparison tools
https://votersedge.org -
https://www.vote411.org - not very good