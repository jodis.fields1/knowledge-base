Politics - canvassing
===
 
Aarika Rhodes - phonebanking; www.aarikarhodes.com; Eli Shi

Motivated by phonebanking for Andrew Yang campaign

Turns out dialing wireless phones is legal as long as it's not robocalled

https://en.wikipedia.org/wiki/Canvassing
https://en.wikipedia.org/wiki/Predictive_dialer

Do-Not-Call list does not apply to political calls as noted by "exemptions to the Do Not Call rules. Because of the limits to FTC’s authority, the Registry does not apply to political calls or calls from non-profits and charities (but the Registry does cover telemarketers calling on behalf of charities)" https://www.ftc.gov/news-events/media-resources/do-not-call-registry

[FCC Order Increases Risks for Political Calls and Texts](https://www.wileyrein.com/newsroom-newsletters-item-5390.html)
* "know the rules before you robocall or robotext (including using autodialers)"
* also see https://www.fcc.gov/political-campaign-robocalls-robotexts
