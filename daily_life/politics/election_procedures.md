General election procedures and projects
===

## new
Fair Representation Act - multimember RCV proposal in Congress

[How Ranked-Choice Voting elects extremists - Medium](https://www.reddit.com/r/EndFPTP/comments/bzqnx8/how_rankedchoice_voting_elects_extremists_medium/)

[Monotonicity](https://www.electionscience.org/library/monotonicity/) - helped explain this vote someone higher & another person wins

### 2018
[No, Instant Runoff wouldn’t solve spoiled elections.](https://medium.com/@jameson.quinn/no-instant-runoff-wouldnt-solve-spoiled-elections-7f6136f1d0ee)
* "Extremists tend to be either first choice or last choice; but for centrists, a significant part of their true support is in the form of second-choice preferences; either side would support them against the other. Since runoff systems ignore that second-choice support, they eliminate the centrist"
FairVote has my attention after Maine victory
* overview at http://www.fairvote.org/ranked_choice_voting_expands_in_2018
* Jennifer Lawrence (from Hunger Games) got involved in Maine!

#### California
https://en.wikipedia.org/wiki/Electoral_reform_in_California

FairVote California - launched 2015 or so; not sure what they're up to
* "met with over 100 leaders and community groups to develop one-on-one relationships across the state" http://www.fairvoteca.org/a_recap_of_2017_with_fairvote_california - Santa Clara, Santa Cruz
* top-two in trouble?

## old (pre-2015)
http://rangevoting.org/BurrSummary.html - only major weakness - as I pointed out in a 2007 blog article, you could pair approval with instant runoff voting to avoid the problem

Prevent redistricting through an algorithmic method: https://groups.google.com/forum/?hl=en&fromgroups#!topic/electionscience/7hVJ0t91DS0

**Presidential election**

Romney-Biden May Be Winning Ticket in Unlikely Voting Tie(http://www.businessweek.com/news/2012-11-02/romney-biden-may-be-winning-ticket-in-unlikely-voting-tie) is fascinating; note that electors can sometimes go rogue altho 30 states bind their electors to vote for a particular person; http://en.wikipedia.org/wiki/Article_Two_of_the_United_States_Constitution#Clause_3:_Electors is the controlling law altho modified by the Twelfth Amendment

## Approval voting
See http://wiki.electorama.com/wiki/Local_organizations_for_approval_voting for a list of endorsers

[STAR voting](https://en.wikipedia.org/wiki/STAR_voting)

[Democrats long for a third-party spoiler](http://www.adn.com/article/20140712/democrats-long-third-party-spoiler "http://www.adn.com/article/20140712/democrats-long-third-party-spoiler") (2014) - good history; approval voting would work against democrats in AK

_Groups:_

http://www.electology.org/about-us

Good articles on approval voting:

http://www.electology.org/How-I-Came-To-Care-About-Voting-Systems

http://lightyears.blogs.cnn.com/2012/03/16/opinion-why-a-different-voting-system-might-be-better/

http://www.nytimes.com/2012/02/12/opinion/sunday/sunday-dialogue-rethinking-how-we-vote.html?pagewanted=all is interesting - really don't understand Richie's comment about approval voting at the end

**Primary systems**

In 2000 California's open primary was found unconstitutional; now the https://en.wikipedia.org/wiki/Nonpartisan_blanket_primary is becoming popular (Washington, California, not AK per Alaska’s Primary Election History - H42.pdf) altho there was an attempt with 2012 HB 77 and it is argued that it makes things more moderate altho FairVote argues the opposite relying on its Louisiana history ("Louisiana's Nonpartisan Primary: Model or Travesty of Reform?"); CSM cites Institute of Governmental Studies report in 2012 saying 6-7% better chance for moderates (California's nonpartisan primary: A step towards a more moderate future?)

  


**Federal congresspersons electoral methods**

Title 15 of Alaska has some criteria - for example in 1993 term limits were passed http://ballotpedia.org/wiki/index.php/Alaska_Term_Limits,_Measure_4_%281994%29

  


Term limits were struck down as unconstitutional; however, H.J.Res. 101 in 113th by Rep. Steven Palazzo [R-MS4] wants to change the constitution but it turns out this is commonly proposed per Sixteen Good, Bad, and Insane Ideas for a Twenty-Eighth Amendment to the Constitution (2013)

  


Should read http://en.wikipedia.org/wiki/Elections_in_the_United_States

  


Any laws implementing http://en.wikipedia.org/wiki/Seventeenth_Amendment_to_the_United_States_Constitution? Looking to change how we vote in senators/reps'

  


http://wiki.answers.com/Q/States_can_set_their_own_rules_for_electing_US_senators_and_representatives_as_long_as_those_rules_do_not_conflict_with_the_US_Constitution is exactly this question

  


http://www.thegreenpapers.com/Hx/ExceptionsToCongressionalElectionDates.phtml interesting I guess?

  


_Political reform project_

First get the website up and running; use the images at http://commons.wikimedia.org/wiki/Category:Washington,_D.C. (reflecting pool - http://commons.wikimedia.org/wiki/File:Bonus_army_on_Capitol_lawn_cph.3a00515.jpg looked cool too) plus another of Alaska for example the devil's thumb looks cool at http://commons.wikimedia.org/w/index.php?title=Category:Mountains_of_Alaska&fileuntil=Tracy+Arm+coast+65.jpg#mw-category-media
