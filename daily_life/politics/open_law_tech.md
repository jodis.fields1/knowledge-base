Open law and tech
===

*Priority*: because this is downstream of politicians, less than fixing election systems

Prolly overlaps stuff in ./archive/legal_research.md and elsewhere, but dedicated note

Socrata - bought buy https://en.wikipedia.org/wiki/Tyler_Technologies

## law ideas
* all agencies must have an open-source idea database and report on it
  * similar to the top 10 response by Office of Children's Services

## people
https://twitter.com/cydharrell

Luke Fretwell https://twitter.com/lukefretwell - CA focused

## orgs
Congressional Data Coalition - members are interested; also I'm in the Open House Project https://groups.google.com/forum/#!forum/openhouseproject

Code for America

https://codefordemocracy.org/

## 2019-09-09

### d3 visualization of US Code or California code
https://stackoverflow.com/questions/20849918/how-to-vary-the-size-of-piechart-in-d3-with-tree-layout | d3.js - How to vary the size of piechart in d3 with tree layout - Stack Overflow
https://www.quora.com/What-are-some-good-examples-of-data-visualization-in-legal-data | (1) What are some good examples of data visualization in legal data? - Quora
https://github.com/alexcengler?tab=repositories | alexcengler (Alex C Engler) / Repositories
https://rapidapi.com/community/api/united-states-code/details | United States Code API: How To Use the API | RapidAPI
https://twitter.com/harlanyu/status/165184504527462400 | Harlan Yu on Twitter: "Visualizing the size of the @USCode by title. 22 million words, about half are statutory notes, Title 42 is huge. #LDTC http://t.co/FBVOSo9k" / Twitter
https://computationallegalstudies.com/2009/09/14/the-structure-of-the-united-states-code/ | The Structure of the United States Code - Computational Legal Studies™
http://openseadragon.github.io/#support | OpenSeadragon

### general
http://www.openlawlab.com/ | Open Law Lab | Law By Design
https://www.upturn.org/recruiting/ | Recruiting | Upturn
https://opengovdata.io/ | Open Government Data: The Book
https://legiscan.com/CA/datasets | California Legislative Datasets | LegiScan
https://github.com/tylerpearson/california-laws-api | tylerpearson/california-laws-api: API for California's state code. Uses rails-api and Elasticsearch.
https://chhsdata.github.io/opendatahandbook/glossary/ | 6. Glossary · OPEN DATA HANDBOOK
https://github.com/openstates/openstates | openstates/openstates: source for Open States's scrapers
https://go.code.ca.gov/index.html | Code California - Coding California government
https://codecagov-playbook.readthedocs.io/en/latest/community/ | Community - Code California Playbook (ALPHA)

## court cases
free.law - nobody else seems to come close
* rejecting GDPR https://free.law/2018/09/26/responding-to-gdpr-right-to-erasure-requests/
* Juriscraper would like to upgrade to Python 3
* Founder Mike Lissner invited me to hit him up at mike@free.law if I want to get more involved