Restaurants
===

## meta
https://www.glutenfreefollowme.com

findmeglutenfree.com - best

https://foursquare.com/shannonbadiee/list/gluten-free-in-sf

## San Francisco
[Gluten-free restaurants](https://mapsengine.google.com/map/u/0/edit?mid=zTKJPetPoFjc.kLw9xJEIFUfk)

## TODO recommended
Kaffa Ethiopian - all the way in Berkeley

The Game Parlor in Sunset

Next Level Burger

### ok
Wholesome Bakery

Mariposa

Rosa Mexicano

Tropisueño - good Mexican food

Mikaku - gluten-free sushi

Gracias Madre - on Mission

### rejected
Rubio's - good Mexican food near work

Daily Grill - fairly chill

Sugar Cafe

Dosa - recommended by Mike & Trish

American Zoetrope - Francis Ford Coppola's restaurant but Bask across the street is better

### bad
Taqueria La Cumbre - very poor Yelp reviews

Comida Yucatan - heart of the ghetto at 294 Turk St

### Diners - all day breakfast!
Moz Cafe - closest to Hack Reactor

Dottie's Cafe - not actually the best; for sale

Cafe Mason - my goto, the omelet pans are washed between use and toast is separate; plus grill has separate spaces between fries and pancakes

Tried: Pinecrest Diner, Sunflower Cafe, Taylor St Coffee Shop and none catered at all to glutel