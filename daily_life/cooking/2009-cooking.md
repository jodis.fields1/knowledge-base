
| Date 	| Meal title 	| Recipe + adjustments | Notes 
* 8/25/2009, Bread from baking machine, Bette's Best Rice Bread from GF Gourment, 
    * Used 3 eggs rather than 2 large eggs. By the book on 1/2 cup sugar, 1 and 1/2 cup water. Seemed too moist and sweet. Left out molasses, dry milk powder, gelatin. Cooked quick on Zojirushi, so only 2 hours. 

* 8/28/2009, Spaghetti.,  My own.
    * Used Newman's Own, which is good, but used Hunt's Diced Tomatoes Roasted Garlic, which was not.
* 8/28/2009, Garlic bread, My own: butter, ground garlic and parsley in a bowl, then dripped on bread
    * Used bread baked 8/25; too sweet for good garlic bread
 