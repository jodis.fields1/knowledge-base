sweet potatoes
---
Technically illegal on SCD

## ibd
https://www.crohnsforum.com/showthread.php?t=31914 | Potatoes - good or bad? - Crohn's Disease Forum - Support group and forum for Crohn's Disease, Ulcerative Colitis, and other IBD

## overviews
http://www.whfoods.com/genpage.php?tname=foodspice&dbid=64 | Sweet potatoes
https://en.wikipedia.org/wiki/Sweet_potato | Sweet potato - Wikipedia

## cooking
https://cooking.stackexchange.com/questions/17452/any-way-to-make-sweet-mashed-potatoes-less-stringy?noredirect=1&lq=1 | Any way to make sweet mashed potatoes less stringy? - Seasoned Advice
https://cooking.stackexchange.com/questions/9388/should-sweet-potatoes-be-peeled-when-preparing-them/9412#9412

## varieties
https://paleoflourish.com/types-of-sweet-potatoes-with-images-and-why-you-should-eat-them | Types of Sweet Potatoes (With Images) and Why You Should Eat Each
https://www.theglobeandmail.com/life/health-and-fitness/ask-a-health-expert/yam-vs-sweet-potato-which-ones-healthier-and-whats-the-difference/article4102306/ | Yam vs. sweet potato: Which one's healthier (and what's the difference)? - The Globe and Mail


## peeling and fiber
https://cooking.stackexchange.com/questions/12556/how-deeply-should-i-peel-sweet-potatoes | peeling - How deeply should I peel sweet potatoes? - Seasoned Advice
https://spoonuniversity.com/lifestyle/can-you-eat-sweet-potato-skins-or-have-we-just-been-fooling-ourselves | Can You Eat Sweet Potato Skins Or Have We Just Been Fooling Ourselves?
https://pubs.acs.org/doi/abs/10.1021/jf9604293?journalCode=jafcau | Potato Peel Dietary Fiber Composition:  Effects of Peeling and Extrusion Cooking Processes - Journal of Agricultural and Food Chemistry (ACS Publications)
https://www.google.com/search?hl=en&q=potato%20peeling%20effect%20on%20fiber | potato peeling effect on fiber - Google Search
https://cooking.stackexchange.com/questions/64379/prevent-fiber-threads-in-sweet-potato-halwa | pudding - prevent fiber threads in sweet potato halwa - Seasoned Advice
https://pubs.acs.org/doi/abs/10.1021/jf101021s | Composition and Physicochemical Properties of Dietary Fiber Extracted from Residues of 10 Varieties of Sweet Potato by a Sieving Method - Journal of Agricultural and Food Chemistry (ACS Publications)
