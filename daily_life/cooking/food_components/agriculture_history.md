OK, ended up going back to the beginning for this...


## history of humanity
https://en.wikipedia.org/wiki/Genetic_history_of_Europe | Genetic history of Europe - Wikipedia
https://en.wikipedia.org/wiki/Genetic_history_of_the_British_Isles | Genetic history of the British Isles - Wikipedia
https://en.wikipedia.org/wiki/Recent_African_origin_of_modern_humans | Recent African origin of modern humans - Wikipedia
https://en.wikipedia.org/wiki/Behavioral_modernity#Late_Upper_Paleolithic_Model_or_%22Revolution%22 | Behavioral modernity - Wikipedia
https://en.wikipedia.org/wiki/Paleolithic | Paleolithic - Wikipedia
https://en.wikipedia.org/wiki/Ancient_DNA | Ancient DNA - Wikipedia
https://www.livescience.com/51219-jurassic-world-real-dinosaur-breeding.html | Real-Life 'Jurassic World' Dinos May Be 10 Years Off, Scientist Says
https://en.wikipedia.org/wiki/Timeline_of_human_prehistory | Timeline of human prehistory - Wikipedia
https://en.wikipedia.org/wiki/Upper_Paleolithic#Timeline | Upper Paleolithic - Wikipedia
https://en.wikipedia.org/wiki/Proto-Sinaitic_script | Proto-Sinaitic script - Wikipedia
https://en.wikipedia.org/wiki/History_of_the_world | History of the world - Wikipedia

### foods
https://en.wikipedia.org/wiki/List_of_ancient_dishes

#### millet
https://www.cam.ac.uk/research/features/archaeology-shows-theres-more-to-millet-than-birdseed | Archaeology shows there's more to millet than birdseed | University of Cambridge
https://f-origin.hypotheses.org/wp-content/blogs.dir/2082/files/2015/06/Ethnobotany-of-millet-cultivation-in-the-north-of-the-Iberian-Peninsula-2015.pdf | Ethnobotany-of-millet-cultivation-in-the-north-of-the-Iberian-Peninsula-2015.pdf
https://en.wikipedia.org/wiki/Proso_millet | Proso millet - Wikipedia
https://en.wikipedia.org/wiki/Foxtail_millet | Foxtail millet - Wikipedia
https://en.wikipedia.org/wiki/Millet | Millet - Wikipedia
https://www.google.com/search?hl=en&q=millet%20cultivation%20Europe | millet cultivation Europe - Google Search

https://en.wikipedia.org/wiki/Flatbread | Flatbread - Wikipedia

#### rice
https://www.zum.de/whkmla/sp/0910/chef/chef1.html | WHKMLA : The History of Rice Cultivation in Europe
