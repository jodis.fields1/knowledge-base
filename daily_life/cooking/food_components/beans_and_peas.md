**Cooking and soaking requirements**

[http://www.simplebites.net/a-simple-guide-to-cooking-dried-beans/](http://www.simplebites.net/a-simple-guide-to-cooking-dried-beans/) - suggests adding stuff (bacon, jalepeno, or garlic) but I've heard you need to avoid salt

See  [http://missvickie.com/howto/beans/howtobeantypes.html](http://missvickie.com/howto/beans/howtobeantypes.html) - soaking and then pressure cooker times mostly

whfoods link below discusses benefits of soaking

http://www.simplebites.net/a-simple-guide-to-cooking-dried- beans/ and http://whatscookingamerica.net/Vegetables/driedbeantip.htm says the same

http://budgetbytes.blogspot.com/2010/12/how-to-kick-can-of- beans.html a comment at the end says calcium can prevent beansfrom getting soft;

*Soaking*

Quick soak may be better - see travelerpalm at cookingforengineers forum  [cites Cooks illustrated study](http://www.cookingforengineers.com/forums/viewtopic.php?t=822) at Soaking beans(?) discussion

****

**Nutritional**

http://www.slowcarbfoodie.com/2011/07/23/what-kind-of- beans-should-i-eat-the-great-slow-carb-bean-index/ good overview

http://www.michiganbean.org/bean-classes/ - not nutritional but descriptive

http://us.soyjoy.com/nutrition/~/media/SoyJoy/Documents/SOY_infosheet_beanchart_final.ashx - white beanshave less fiber

http://beaninstitute.com/health-benefits/nutritional-value-of-dry- beans/ is a narrative focusing on black beans

[http://www.whfoods.com/genpage.php?tname=foodspice&dbid=2](http://www.whfoods.com/genpage.php?tname=foodspice&dbid=2) on how black beansare better than lentils or chickpeas

**Gas side-effect**

commenter at budgetbytes above says kidney/red beansrequire parboiling

http://digestive.niddk.nih.gov/ddiseases/pubs/gas/

**Freezing cooked beans**

http://www.thekitchn.com/cheap-convenient-better-than-canned-freezer- beans-177487 recommends adding vinegar to prevent splitting

http://www.favoritefreezerfoods.com/preparing-cooking-and-freezing-dried- beans.html - a lot of ideas for freezer food

http://www.thekitchn.com/good-question-can-i-freeze-coo-74327 - good idea

## Phytic acid

Mainly popular concern about this is rooted in fearmongering (right or wrong) by Weston A. Price

2013-04-20: ultimate conclusion is that it is probably not worth worrying so much about http://board.crossfit.com/showthread.php?t=62648 has a good discussion of the chemistry by Matthew Marturano; also read http://healthfoodlover.com/hfl/2010/10/dephytinization-lentils-legumes-cereal-grains/ An Objective Look At Phytic Acid: Actions, Sources + Dephytinization Methods; also see Phytic acid in health and disease (1995) with the modern update in Phytate in foods and significance for humans: Food sources, intake, processing, bioavailability, protective role and analysis (2009); also found some research on low-phytic acid and decorticated is where skins are removed to get rid of some

Later research includes Phytase supplementation improves blood zinc in rats fed with high phytate Iranian bread

http://www.truenutrition.com/p-1086-ip6-powder-100-grams.aspx briefly summarizes its beneficial health effects including on cancer

Application of in vitro bioaccessibility and bioavailability methods for calcium, carotenoids, folate, iron, magnesium, polyphenols, zinc, and vitamins B6, B12, D, and E - excellent 2012 overview

Protective effect of myo-inositol hexaphosphate (phytate) on bone mass loss in postmenopausal women (2013) - suggests protection against osteoporosis

Acute Effects of Navy Bean Powder, Lentil Powder and Chickpea Powder on Postprandial Glycaemic Response and Subjective Appetite in Healthy Young Men (2012) - do not reduce appetite, hooray

*Weston Price and Chris Masterjohn*(some bio at http://www.orthodoxct.org/news_120804_1.html - he got his PhD at the University of Connecticut and now is a postdoc at University of Illinois)

Got into the whole vitamin K2 thing and vegetarianism versus meat - check out http://www.thedentalessentials.com/The_role_of_vitamin_k2_on_tooth_decay_s/29.htm; http://www.westonaprice.org/fat-soluble-activators/x-factor-is-vitamin-k2; http://www.marksdailyapple.com/vitamin-k2/; http://wholehealthsource.blogspot.com/2008/06/vitamin-k2-menatetrenone-mk-4.html (I remember this Guyenet guy); http://paleohacks.com/questions/26159/how-much-k2-is-in-grass-fed-butter; FAQs: All about fermented cod liver oil (and why I don’t take fish oil); http://www.greenpasture.org/public/FAQ/index.cfm (major retailer); Separating fact from fiction on cod liver oil (Chris Kresser); http://nourishedandnurtured.blogspot.com/2013/01/why-we-stopped-taking-fermented-cod.html (negative view - good!);

The most striking finding was a systematic review with a .23 risk factor for fracture from oteoporosis but Consumerlab has a full overview with less striking results; I thought about getting a High Vitamin Butter Oil (Grass fed & Organic) 12 Oz or Life Extension Super K with Advanced K2 Complex Softgels;

China Study fight - Masterjohn critiqued the China study; author Campbell responded with http://www.vegsource.com/articles2/campbell_china_response.htm; and Masterjohn responded at http://www.cholesterol-and-health.com/Campbell-Masterjohn.html;