# carrots
https://scdlifestyle.com/2010/03/how-to-make-scd-legal-carrots-faster/ | How to Make SCD Legal Carrots Faster and More Nutritious at the Same Time!
https://www.foxnews.com/food-drink/the-truth-behind-baby-carrots | The truth behind baby carrots | Fox News

## brands
Green Giant:
* https://en.wikipedia.org/wiki/Bolthouse_Farms - 
    * known for carrot juice
    * 2018-08 for sale by Campbell Soup https://www.forbes.com/sites/andriacheng/2018/08/30/the-jury-is-still-out-on-campbell-soup/#a068dc5122da
        * former CEO Jeff Dunn may buy
    * not https://en.wikipedia.org/wiki/Green_Giant which is frozen goods, subsidiary of General Mills but now owned by B&G https://en.wikipedia.org/wiki/B%26G_Foods