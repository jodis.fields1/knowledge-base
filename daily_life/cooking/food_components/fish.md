Fish
===

## barramundi
https://www.thebetterfish.com/thecurrent/why-barramundi-is-the-future-of-fish/ | Why Barramundi is the "It" Fish | Australis Barramundi
https://www.thebetterfish.com/thecurrent/barramundi-fish-farm-aquaculture/ | So You Want to Be a Fish Farmer? | Australis Barramundi
https://thefishsite.com/articles/australis-pulls-out-of-land-based-barramundi | Australis pulls out of land-based barramundi | The Fish Site
https://www.target.com/p/lovethewild-baja-style-frozen-fish-taco-bowl-with-barramundi-9oz/-/A-54313528 | LoveTheWild Baja Style Frozen Fish Taco Bowl With Barramundi - 9oz : Target
https://www.target.com/p/smart-ones-classic-favorites-santa-fe-style-frozen-rice-38-beans-9oz/-/A-13257209 | Smart Ones Classic Favorites Santa Fe Style Frozen Rice & Beans - 9oz : Target

## Halibut

General tips:

Halibut - pan fry to avoid the dryness (see [http://www.professorshouse.com/Food-Beverage/Topics/Meat/Articles/Cooking-Halibut/](http://www.professorshouse.com/Food-Beverage/Topics/Meat/Articles/Cooking-Halibut/)). Note that it is better undercooked than overcooked. The nice [helpwithcooking.com](http://helpwithcooking.com/) says [here](http://www.helpwithcooking.com/fish-guide/baking-fish.html) that tin foil can help retain the moisture in fish(did not work for me)

Tests:

*Baking:*

Halibut was about 1.5 to 2 inches thick; not very wide but perhaps seemed wide (wideness shouldn't matter?).
Preheated the oven to 450 degrees Fahrenheit.
Tossed some butter on top of the halibut and put some lemon drops on top. Also did a bit of seasoning.

Set the timer for 10 minutes. Turned out great

*Pan-fried*

Used tips at [http://checkitoutavesta.blogspot.com/2009/03/pan-fried-halibut.html](http://checkitoutavesta.blogspot.com/2009/03/pan-fried-halibut.html) for halibut tacos (diced halibut); worked great; added McCormick's taco seasoning while raw

2013-04-15: with Stephanie I used parchment paper, lemon, leeks, thyme, spices and water to make pretty good halibut; cut a rather thick piece into 2 less thick pieces; oven had some issues getting above 400 degrees so it took longer; used [http://www.chow.com/recipes/28716-basic- fish-baked-in-parchment](http://www.chow.com/recipes/28716-basic-fish-baked-in-parchment) (recommends pouring water in); [http://recipes.howstuffworks.com/tools-and-techniques/how-to-cook-fish7.htm](http://recipes.howstuffworks.com/tools-and-techniques/how-to-cook-fish7.htm) (how to cut the parchment paper into a heart) and added leeks and lemon