avocados
===

TODO: https://realfoodrn.com/how-to-slice-and-freeze-avocados/

## quality problems
[Meet the Mexican Importer Delivering Perfect Avocados For the Instagram Age](https://remezcla.com/culture/mexican-importer-nyc-avocados/)

"There is only one variety that can be stored in the fridge, and that ALWAYS, if you open them on time, are good to eat. It is the Hass variety. This variety is the most consumed by North America and Europe, because it can be produced in warmer places, picked up and stored in refrigerated containers, while it reaches its destination they slowly ripe. A ripe Hass avocado can be consumed during a time frame of two weeks, if you keep them in the fridge, making them very practical to store." - https://www.quora.com/Why-were-so-many-avocados-I-bought-in-southern-California-of-poor-quality-this-year-And-why-don’t-the-pits-sprout

"Hass avocados, when ripe tend to have a brown-ish darker skin tone. Different varieties may remain green and have a bitter taste when unripe" - https://cooking.stackexchange.com/questions/89300/bad-tasting-avocados-recently


## ibd
https://www.ncbi.nlm.nih.gov/pmc/articles/PMC4732800/ | Onset of Ulcerative Colitis during a Low-Carbohydrate Weight-Loss Diet and Treatment with a Plant-Based Diet: A Case Report
https://www.crohnscolitiscommunity.org/post?forumId=18&threadId=13468 | Community Forum

## varieties / producers
https://en.wikipedia.org/wiki/Avocado#A_cultivars

https://www.google.com/search?hl=en&q=avocado%20lamb%20hass | avocado lamb hass - Google Search
* bravocado
    * premium brand from http://www.henryavocado.com/salesdistribution/
    * actually mostly from South America per https://www.freshplaza.com/article/2186183/new-avocado-distribution-center-prepares-for-supply-boost/
    * relationship to Shanghai brand at http://www.fruitnet.com/asiafruit/article/172422/bravocado-seeks-more-avocado-sales ??
* Cavalo'Tini
    * Calavo Growers, Santa Paula, CA 93060
        * publicly-traded

## overviews
http://www.whfoods.com/genpage.php?tname=foodspice&dbid=5 | Avocados
http://foodb.ca/foods/130 | Showing Food Avocado - FooDB

### home cultivation
https://www.hjorthjort.xyz/2016/04/25/growing-avocados-from-seeds-you-ve-been-doing-it-wrong.html | Growing avocados from seeds without using tootphicks
https://www.youtube.com/watch?v=69xzcd_IffI | Growing Avocado Trees from Seeds, Days 88-103 - YouTube
how to speed up avocado tree growth grow light - Google Search

### innovations
https://www.npr.org/2015/06/03/411801139/california-avocado-farmers-boost-yields-with-new-growing-method | California Avocado Farmers Boost Yields With New Growing Method : NPR
https://www.organicproducenetwork.com/article/404/whats-causing-organic-avocado-producersstruggle-to-meet-demand | What's Causing Organic Avocado Producer's Struggle to Meet Demand?

