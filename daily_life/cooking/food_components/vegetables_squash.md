See agriculture_controlled.md

[Squash may have anti-diabetic properties](https://www.upi.com/Squash-may-have-anti-diabetic-properties/31561352502176/)

[Don't Squander That Squash: Like Fine Wine, It Might Improve With Age](https://www.npr.org/sections/thesalt/2015/11/25/457398031/dont-squander-that-squash-like-fine-wine-it-might-improve-with-age)

## squash
2018-11:
* butternut squash - TODO
* kabocha squash - 30 minutes in instant pot
    * tested pretty sweet, digested well
* acorn squash - 30 minutes instant pot (maybe more?)
    * neutral taste, digested well
    * 2018-11-17: cooked rather orange one - taste?
        * https://cooking.stackexchange.com/questions/24130/what-happens-to-an-acorn-squash-when-its-skin-turns-orange
* spaghetti squash - ??
* dumpling squash - ??

## cooking / ripeness
https://cooking.stackexchange.com/questions/24130/what-happens-to-an-acorn-squash-when-its-skin-turns-orange