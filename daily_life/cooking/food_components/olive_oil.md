
* California Olive Ranch - traditionally good

[Olive Oil Quality Seals? Take Your Pick](https://www.oliveoiltimes.com/olive-oil-basics/olive-oil-quality-seals/34742)

[NAOOA strengthens olive oil quality standards to boost consumer confidence and sales](https://www.oliveoiltimes.com/olive-oil-basics/olive-oil-quality-seals/34742)

[The biggest fraud in olive oil](https://www.aboutoliveoil.org/olive-oil-fraud)
* says fraud is overstated

[5 myths — and facts — about olive oil](https://www.seattletimes.com/life/wellness/5-myths-and-facts-about-olive-oil/)
* says fraud is not overstated

## NAOOA
https://www.aboutoliveoil.org/certified-olive-oil-list
* Bertoli's is on the list
    * "Bertoli parent company Deoleo agreed this month to pay $7 Million to settle a class action lawsuit accusing the company of misrepresenting the contents of some of its olive oil bottles"
    * [How to make sure you're buying high quality olive oil](https://eatsiptrip.10best.com/2018/04/23/how-to-make-sure-youre-buying-high-quality-olive-oil/)