Note meat sometimes doesn't seem good to me; wonder if might be related to having 70% of people have two functional copies of a gene linked to an odor receptor called OR7D4, which detects androstenone - http://healthland.time.com/2012/05/03/does-meat-gross-you-out-it-may-be-genetic/

Beef and hamburger

If using venison, see  Venison Burgers from youhavetocookitright (egg and worcester sauce adding moisture)

Pot roast - tried Morton of Omaha's boil in a bag from Costco and think I got glutened (based on foodfacts.com entry; see 2004 discussion with  complaints about inconsistent quality). Switched to straight beef such as chuck instead. Next up is trying a couple recipes I saw such as  Dinner Recipe: Individual Pot Roasts with Thyme-Glazed Carrots,  Perfect Pot Roast,  Maries Easy Slow Cooker Pot Roast or  Classic Beef Pot Roast

Chili

How To: Make a (Basic) Ground Beef Chili | Ordinary Times - good start in 2014!

Searing

Per http://en.wikipedia.org/wiki/Searing there's a debate over this; wiki cites McGee altho http://culinaryarts.about.com/od/cookingmethods/a/sealinjuices3.htm Danilo Alfo has 

http://www.cheftalk.com/t/41420/searing-meat-does-not-lock-in-the-juices is a good discussion; key concepts are basting and resting

## Chicken

parts at http://www.recipetips.com/kitchen-tips/t--365/chicken-description-of-parts.asp and dissection at http://kriegerscience.wordpress.com/2010/10/24/how-to-dissect-a-chicken-leg/

Note that cooking chicken in water is a bit risky; couple things to make it more moist:

brining - ingredient http://allrecipes.com/recipe/simple-chicken-brine/ altho I've done it simple with just salt; http://www.smoker-cooking.com/how-to-brine-chicken.html is a bit too complicated in terms of prep but cold water is important; http://ruhlman.com/2010/10/how-to-brine-chicken-quick-brine-recipe/ is some expert commentary on brines

### whole chicken
https://melissaknorris.com/how-to-cook-a-whole-chicken-in-the-instant-pot/ | How to Cook a Whole Chicken in the Instant Pot
https://www.reddit.com/r/instantpot/comments/7vnnyn/chicken_coming_out_tough_new_to_the_ip/ | Chicken coming out tough? New to the IP : instantpot
https://amindfullmom.com/how-to-make-a-whole-chicken-in-the-instant-pot/ | Instant Pot Rotissiere Chicken | A Mind "Full" Mom
https://www.reddit.com/r/instantpot/comments/57mre3/best_whole_chicken_method_and_time/ | Best whole chicken method and time? : instantpot


### Pan-fried in general

Noticed on http://miskcooks.com/2012/03/28/how-to-pan-fry-chicken-breasts/ some tips - essentially cover the pan

### Chicken soup

http://chowhound.chow.com/topics/751794 is a poll; however, not a lot of great advice from my perspective

http://allrecipes.com/recipe/quick-chicken-soup/ is somewhat consistent with Darin's advice

suggested methods for moist chicken: http://www.thekitchn.com/how-to-cook-moist-tender-chicken-breasts-every-time-36891 and http://www.mnn.com/food/recipes/blogs/quick-moist-chicken-breasts-the-easy-way

http://busycooks.about.com/od/chickenrecipes/a/howtocookchixbr_2.htm is chicken methods


#### chicken noodle
https://www.chefsteps.com/activities/pressure-cooked-blond-chicken-stock | Pressure-Cooked Blond Chicken Stock | Recipe | ChefSteps
https://shop.pacificfoods.com/our-products/broths-and-stocks/broth?includeUnavailable | Broths: Free-Range Chicken, Vegetable, Beef and More | Pacific Foods
https://mygluten-freekitchen.com/busy-moms-slow-cooker-chicken-noodle-soup-gluten-free/ | Busy Mom's Slow Cooker Chicken Noodle Soup {Gluten-free}
https://greenhealthycooking.com/instant-pot-chicken-noodle-soup/ | Instant Pot Chicken Noodle Soup - Green Healthy Cooking


### gumbo
Gumbo StyleCreole Chicken

\1. Fascinating mixture, but it is hampered by the bland taste of the chicken. Need to really increase the salt to add flavor. The alternative is to simmer in vegetables and such (onions I suppose particularly) for a 'white stock'.

\2. Xianli made this for me; it seemed a little dry - she used corn starch as a replacement for the flour

Taco chicken

Marinate long time chicken in fridge with taco seasoning, salt, and black pepper; warm up oil at 8 heat; add chicken and stir; 3-4 minutes cooking; put butter; cook 2-3 minutes; add cheese; when cheese melts ready
