history and innovation of food production and agriculture
---
with a detour into the history of civilization:

back in the day I noted [Hydroponics Genesis Controller
](https://www.indiegogo.com/projects/hydroponics-genesis-controller/x/2086002#/)
* familiar

## 2018-11-23
### controlled environment
* this is good - more reproducibility / better quality
* https://en.wikipedia.org/wiki/Controlled-environment_agriculture - the future (see wiki)

#### academic research
* [The Future of Farming Takes Root](https://uanews.arizona.edu/story/future-farming-takes-root)

#### CEA for sale
* AeroFarms - consumer brand only NJ
* [Driscroll's](https://en.wikipedia.org/wiki/Driscoll's) - controlls $2b in berry revenue?; "Containerized production is not the same production system as hydroponics which is a water-based production system"
* OrganicaWorld - CSA in Florida
* GreenGro technologies - indoor farming stuff, caters to marijuana, publicly-traded
* Plenty, Inc - raised $200m
* Archi’s Acres, Inc. - small group in Escondido California, originally did TEDx talk https://www.patreon.com/ArchisAcres
* Radicle Farm - New Jersey
* Wholesum Harvest - no direct to consumer

#### at home hobby
* https://www.maximumyield.com - lots of resources e.g. https://www.maximumyield.com/what-is-the-best-way-to-grow-an-avocado-plant-using-an-aeroponic-system/7/3514
* https://www.growingproduce.com/vegetables/how-to-successfully-produce-vegetables-in-controlled-environments/ | How To Successfully Produce Vegetables In Controlled Environments - Growing Produce
* https://blog.extension.uconn.edu/2014/12/23/controlled-environment-agriculture/# | Controlled Environment Agriculture | Extension