See Food->vegetables and Cooking diseases->Shelf life and Kitchen->Food storage

**Gardening**- get the Click & Grow Smart Herb Garden

**Cilantro**

I use this a ton but it can be a pain. Summary of similar-tasting products at  [Mountain Valley Growers Polygonum Odoratum](http://www.mountainvalleygrowers.com/polodoratum.htm) (Vietnamese Cilantro or Rau Ram); others are Coriandrum sativum (Cilantro) and Eryngium foetidium (Culantro).

*Preparation* - make sure to soak it to get all the dirt out; use a colander, per  [wikihow article](http://www.wikihow.com/Chop-Cilantro). pick leaves off? lots of people do  [per seriouseats](http://www.seriouseats.com/talk/2012/02/do-you-pick-the-leaves-off-when-using-cilantroparsley.html) but I can't stand it!  [per yahoo answers](http://How%20do%20you%20use%20fresh%20Cilantro?) cut it close to the leaves and that's enough

*Storage tips*: 

VegetableGardener.com semiscientific tip  [How to Keep Cilantro Fresh](http://www.vegetablegardener.com/item/12215/how-to-keep-cilantro-fresh) says put in water then cover in refrigerator, not sure whether you snip the bottom off

[showmethecurry.com video](http://showmethecurry.com/tips/storing-cilantro-coriander.html) notes that you let it dry first and remove all the dead ones

*Suggestions to freeze it*

**Vegetables**

I foolishly kept buying romaine lettuce when I should have been buying iceberg lettuce for sandwiches; saw 10 varieties on a realsimple article

**

## Genetics
2013 Nature Immunology report finds connection to innate lymphoid cells by Dr Gabrielle Belz, Ms Lucie Rankin, Dr Joanna Groom per http://www.eurekalert.org/pub_releases/2013-03/waeh-gdr030313.php

## Cooking
Steaming is my preference: Asparagus - steam in microwave covered by wax paper (learned later to use another plate from Ann)

## Chopping
See  Gordon Ramsay: How to Chop an Onion but I liked  Knife Skills - How to Dice an Onion more. Google  "How to chop an onion" for  this article and also I liked  How to Slice, Dice or Chop an Onion*

### Storage
some release ethylene gas so should be stored separately; see Kitchen note for other details but generally use the Progressive Vegetable keeper;   [produce bags at Amazon](http://www.amazon.com/review/RS5IHIBIHV88/ref=cm_cr_dp_cmt?ie=UTF8&ASIN=B002UXQ7QQ&nodeID=284507&tag=&linkCode=#wasThisHelpful) with a good review comment

## Salads

See  [wiki template on dressings](https://en.wikipedia.org/wiki/Template:Salad_dressings) for more information, inspired by reading about balsamic vinegar

2014-08: read up on vinegar again (ie diluted acetic acid), see  [What are the functions of vinegar in cooking? ](http://cooking.stackexchange.com/questions/13322/what-are-the-functions-of-vinegar-in-cooking)(2011) altho that is more general