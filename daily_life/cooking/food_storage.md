Food storage
===

[Anchor Hocking TrueSeal Glass Food Storage Containers with Lids](https://www.amazon.com/gp/product/B00FRH03SK)

https://thewirecutter.com/reviews/best-food-storage-containers/

Have Glasslock, OK w/ them. Seeking sandwich container.

Options:
*  https://www.amazon.com/gp/product/B003IUDF4C - Wonder bread
*  altho https://www.amazon.com/dp/B01DV760AM - black fancy design
*  https://www.amazon.com/Sistema-Collection-Sandwich-Storage-Container/dp/B01GOE765W
*  https://www.amazon.com/gp/product/B06WGRJM4B - Komax Biokips Food Storage Sandwich Lunch Box Container 23oz. (set of 3) - did not want 3

## Freezing
Put stuff in water? https://www.favoritefreezerfoods.com/freezer-burn.html