# cooking software
https://mobile.twitter.com/benawad/status/1207676480161034244

## open-source
https://github.com/thinkle/gourmet/issues/897 - idea to rewrite on modern stack
* https://github.com/qgriffith/OpenEats
* https://github.com/nazgul26/PHPRecipebook
* https://github.com/onetsp/RecipeParser

## old
Whisk.com

food.com - switched to geniuskitchen.com

Allrecipes - leader, I used to use them esp. their Android app, tho they signed me up for a couple mags automatically (owned by Meredith Corp)

**Meal planning**
https://www.chefsteps.com/activities/balanced-meal-planning-with-jef-nelson-of-shogun-fitness

https://www.reddit.com/r/glutenfree/comments/8oc1jn/can_i_get_some_love_from_my_fellow_gf_mealpreppers/

plantoeat - best

eatthismuch - decent


Best to start with is the Food Planner  and reviewed http://lifehacker.com/5896745/plan-your-weekly-meals-stress-free for ideas but then found that Amazon's appstore was better with a cooking category and s [earch for meal planner](http://www.amazon.com/s/ref=nb_sb_noss?url=node%3D2478841011&field-keywords=meal+planner)

Contenders:

Food Planner - http://help.foodplannerapp.com/Android:Main_help is a good overview of the help

My Family Meal Planner Light - looks good!

Tried:

Ziplist - no planning really

Pepperplate - no widget and web-reliant

my Food - very basic and not much in the way of planning; no apparent widget

Cozi - meal planning on iPad but not on Android, deleted account

Recipe Pal - not a real meal planner

Not tried:

MealBoard - ??

My Menus - ??

*Articles*

50 Packable Lunch Ideas - ??

Building a Recipe Database - interesting NYTimes article

Lifehacker article noted above

How to pack a grilled cheese sandwich and soup for lunch - Momables has a lot of good stuff (see  [http://www.momables.com/sample-menu/](http://www.momables.com/sample-menu/)); also see How long is it safe to keep food hot in a thermos and also How to Pack a Safe School Lunch from sandiegohealth.org 

Top 10 Tuesdays: Top Ten Items That Do NOT Need Refrigeration - has some suggestions

[How to Pack a Safe Lunch](http://www.foodsafety.wsu.edu/consumers/factsheet8.htm) - shows dry or hard cheeses as safe to leave out (high in acid or low in moisture)

Portable healthy lunch ideas that don't require refrigeration or a microwave - straightdope has good advice

**Websites**

Supercook.com - announced on reddit for when you've got a bunch of stuff           http://www.reddit.com/r/recipes/comments/11otlc/hey_foodies_weve_relaunched_supercook_finding/

Recco - looks interesting per http://nyconvergence.com/2011/06/new-food-recommendation-app-combines-twitter-yelp-elements.html

Foodista -

foodfacts.com - looks pretty clean

http://www.reddit.com/r/recipes/comments/fv8q0/have_food_in_the_fridge_dont_know_what_to_make/ - lists recipechimp and recipepuppy