glutenings:
* 2018-08-11 - Evol Thai Style Curry Chicken
    * labeled gluten-free :(

## smoothies
TODO: try rice in smoothies per https://www.youtube.com/watch?v=hWu00m5ekls

staple: 1 cup greek yogurt (Fage, unflavored), avocado, ice, banana, honey

## Snacks
Milton Craft Bakers gluten free crackers - enjoyed these at Christine & Andy's

## oatmeal
* Bob's Red Mill, Old Fashioned Rolled Oats
    * 1 cup to 1.5 cup water, cover, microwave for 1.5 minutes
    * 2 minutes might be better? need to measure temperature

## mac & cheese
forget packaged options - just cook Barilla pasta in an instant pot and toss in parmesan + butter
* how long?

https://www.kcet.org/food-living/the-mac-cheese-recipe-kraft-doesnt-want-you-to-know

https://www.budgetbytes.com/miracle-mac-n-cheese/

## staples
Whole chicken raw
* 35 minute cook time...
* 2018-06 - went OK?
* 2018-07-4

Maybe:
* Beecher's “World's Best” Mac & Cheese http://beechershandmadecheese.com/
    * actually really good!
* Evol Gluten-free Smoked Gouda Mac & Cheese
    * far better than bacon mac & cheese version
* Evol Chicken Tikka Masala
    * Safeway near work
* Tandoor Chef Channa Masala
    * unclear gluten-free precautions

### flour
* Bob's Red Mill Pie Crust
    * quiche from Megan and Hamish was really good!

### bread
Canyon Bakehouse bread

Udi's Soft White
* harder to find lately
* mostly pb&j
* buy from Target

## packaged
Foster Farms Gluten-free Chicken Breasts
* bought 2018-01 or so?
* 2018-08: has gristle, ugh

## desserts
* Mikawaya Mochi ice cream - has rice so I can't eat it with SCD

## dietary supplements
baze.com - not open to public yet

* http://www.countrylifevitamins.com/category/vitamins-c-d-e-k
  * megadoses :(
* https://glutenproject.com/

## rejected
Ancient Harvest Supergrain Mac & Cheese - ugh, not good