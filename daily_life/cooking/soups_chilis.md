https://www.reddit.com/r/getdisciplined/comments/6ntx66/question_has_anyone_ever_actually_woke_up_and/dkdeivj/

[The Best Vegetarian Chili in the World
](ttps://www.allrecipes.com/recipe/72508/the-best-vegetarian-chili-in-the-world/)

Instant Pot- [Grandpa’s Chili With Beans](http://instantpot.com/grandpas-chili-with-beans/) -

2013-06: 12 cups (3 quarts of water), plus another 4 cups of broth, a tablespoon of better than boullion, plus a bag of small carrots, celery, potatoes, a couple cups of rice

2012-05-05: http://www.countryliving.com/recipefinder/homemade-chicken-noodle-soup-3996 by Xianli was great; 2 tablespoon in 8 cups of water (reduce to 7 cups water)

2012-01-06: cooked up a bunch of soup. I put it on 7 and it boiled fast, then lowered it to 3 and let it boil longer - too long, as it turns out. The 6-8 cups of water that I put in there boiled away to nothing quick. Also put about a tablespoon of flavoring ("Better Than Boullion") in there, but it wasn't enough - could not taste it. Tastes OK after adding water and spice.

**Slow cooker**
See http://allrecipes.com/recipes/everyday-cooking/sensational-slow-cooking/main.aspx?prop24=PN_2.4.8_SN.Slow-Cooker

http://allrecipes.com/Recipe/Slow-Cooker-Chicken-and-Noodles/Detail.aspx interesting to try

Why Brining Keeps Turkey and Other Meat So Moist - http://www.finecooking.com/articles/why-brining-keeps-meat-moist.aspx inspired by http://www.livestrong.com/article/508934-how-to-make-chicken-moist-and-tender-in-crockpot/ which did not explain the science

How can I get chicken breasts to be tender and moist? - http://answers.yahoo.com/question/index?qid=20070612131023AAlMzSf has more complete explanation including "osmosis"