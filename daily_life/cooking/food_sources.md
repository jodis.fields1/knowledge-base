Food sources
===

### Lunchbox

Always embarrassed by this and it turns out a lot of people are, but good  [adult ones at Amazon](http://www.amazon.com/gp/feature.html?ie=UTF8&docId=1001001241) such as perhaps  [Lunch Brief](http://www.amazon.com/Lunch-Brief-Notebook-Briefcase-Insulated/product-reviews/B0011MU0LU) or  [eBags Slim Lunch Box (Black)](http://www.amazon.com/gp/product/B0013KIJ0E/ref=ox_sc_act_title_6?ie=UTF8&psc=1&smid=A1MPSLFC7L5AFK) which is unfortunately unavailable; also see  [Six Pack Fitness Executive 500 Briefcase](http://breakingmuscle.com/other-gear/product-review-six-pack-fitness-executive-500-briefcase), 

[12 Manly Lunch Boxes For Men | Next Luxury](http://nextluxury.com/food-travel/manly-lunch-boxes-for-men/) - 

See  [Bentgo All-in-One Stackable Lunch/Bento Box, Blue](http://www.amazon.com/gp/product/B00B78T8U2/ref=ox_sc_act_title_5?ie=UTF8&psc=1&smid=A2IEDZAWBB03ZE) and  [Bento System 2.0 - Berry](http://secure.laptoplunches.com/itemdesc.asp?ic=K632-BERRY&eq=&Tp=)

## Shelf life

Peres - gadget on indiegogo to detect spoiled meat

## Meats- see separate note

*Storage -* Freezing chicken breasts: simplecookingblog.com recommends tightly wrapping with foil, freezer paper, or plastic wrap and even double-wrapping to avoid freezer burn. For freezing, freezing paper may be no better than wax paper (source??); freezing should have less estrogenic activity risk anyway.

## Agenda

*Morning*

Fresh fruit - apple, orange, kiwi, or some mix

Meal - fried potatoes; pancakes; rice meal; yogurt; cereal; oatmeal (also tried Ancient Harvest Quinoa Flakes which were OK)

Pancakes

Stephanie told me the key is to get it to where water pops when you flick it at the pan, which is about 370 F
Recipe
​    1 cup Pamela's Baking & Pancake Mix
​    1 large egg (or equivalent of liquid egg replacer)

​    3/4 cup water

​    1 TBSP oil

*Attempt*

2012-01-26: tried the recipe but had to add 5 tablespoons more of flour to make it thick enough for Stephanie
2012-12-23: put it at 4 and then cooked for 2 minutes; then 30 seconds on the other side; turned out ??

2012-10-14: tasted OK but mushy in the middle again; this time made with Pamel's bread flour per http://pamelasproducts.com/dairy-free-pancakes/ calls for baking soda and baking powder which is interestingl

*Advice* preventing mushy in the middle see http://cooking.stackexchange.com/questions/12098/pancakes-not-cooked-in-the-middle has some advice; as does http://answers.yahoo.com/question/index?qid=20080925195926AAENHXE

**Products**

*Breakfast bars and snacks*

Glutino - tried apple and blueberry; they are OK

Larabar - tend to go for the banana bread but there's also carrot cake, owned by General Mills I believe (thought ConAgra at first)

Frozen peanut-butter balls - mix peanut butter, bananas (ripe), chocolate whey protein

Jovial Fig Fruit Filled Cookies - I like them, from Italy which is kinda odd

*Frozen food*

Pizza - tried microwaving then pan-toasting per https://canyoumicrowave.com/frozen-pizza/ but didn't work out well

Saffron Road - yummy! $18m in 2013 by Adnan Durrani; choose Chicken Pad Thai first w/more calories but Tikka Masala has lots of sauce for extra rice

[gf overflow Frozen Meals](http://www.gfoverflow.com/results.php?q=category:+Frozen+Meals) - good list

Taquitos/Tamales - Nuevo Grille Tamales at Costco are good (from Circle Foods which was bought by Tyson in 2014); Delimex was good, but now on shared line per  [this](http://asgoodasgluten.blogspot.com/2012/05/in-my-pantry-delimex-chicken-corn.html?m=1)

*Packets*

Asian

Phad Thai (Sun-Bird brand)

> 2012-02-21: didn't like this so much. You don't cook the meat with seasoning, so the meat is bland. Used rice noodles and probably used too many.

Indian

Patak's Dopiak Curry - very mild-tasting but OK overall

Patak's Tikka Masala curry - the lemon combined with tomato just didn't do it for me; seemed sour in a bad way; an Amazon reviewer suggests Fresh and Easy

TastyBite Bengal Lentils - much better than than the Jaipur Vegetables

Madras Lentils - better than Jaipur but still lacking in spice

TastyBite Jaipur Vegetables - not spicy enough; cashew paste, paneer cheese, chilies, ginger, and garlic

*Insects*- see Etom Foods as well as the grosser World Entomophagy.

## Oil

2014-07-07: see composition of fats ( [wiki permalink](https://en.wikipedia.org/w/index.php?title=Vegetable_oil&oldid=615498032#Composition_of_fats)), based on linoleic acid (omega-6) being potentially correlated with IBD, I looked into this again; canola oil has much more omega-3s but I also found  [Is it a Good Idea to Cook With Olive Oil? A Critical Look](http://authoritynutrition.com/is-olive-oil-good-for-cooking/) (2014) and  [Are Olive Oil and Canola Oil Interchangeable?](http://www.alternet.org/story/155231/are_olive_oil_and_canola_oil_interchangeable?page=0%2C0) (2012) which points to canola oil forming trans fats on the shelf, ugh! Mary Flynn is big into olive oil per  [The science of cooking with olive oil](http://www.truthinoliveoil.com/2013/10/science-cooking-olive-oil) (2013). Also see Rapeseed oil sales soar as middle class cooks turn to it instead of olive oil because it has half the amount of saturated fat (2014) which notes that cold-pressed rapeseed (canola) oil is becoming popularl

## Gluten-free

[Gluten PPM Table, Updated August 2013](http://celiacdisease.about.com/od/PreventingCrossContamination/a/Gluten-Free-PPM-table.htm) - good source list

[Gluten-Free Foods and Facilities (Master List)](http://www.glutenfreeveganmom.com/2011/04/15/gluten-free-foods-and-facilities-master-list/) - 

[New Seasons shopping list](http://www.newseasonsmarket.com/assets/files/glutenfree-shop-list.pdf) - 

## Companies

Udi's - acquired by E&A Industries per http://www.9news.com/money/206247/344/Udis-Gluten-Free-Foods-rides-trend-secret-recipe and now under Boulder Canyon

Glutino - merged with Gluten Free Pantry which provides my favorite sandwich bread

Amy's - cross-contaminates; no gluten-free facility

Annie's - publicly-traded under BNNY and I don't think their mac & cheese is from a certified gluten-free facility

Schar - European giant which opened dedicated U.S. gluten-free facility in 2012 per Schar Opens Dedicated Gluten-Free Facility

Schwan's - biggest in delivered frozen food; haven't really eaten from them, (update: bought Sabatasso so eat their pizza)

### Manufacturing

Thai Kitchen - great rice ramen; send them kudos in 2009; Mary Lepley at McCormick responded. Looks like this got bought out.