Instant pot recipes
===

whisk.com is my main recipe app

https://instantpot.com/portfolio-item/sealing-ring-color-2-pack/

## core
beyond meat - beefy versus feisty

## rice
https://cooking.stackexchange.com/questions/9552/how-do-i-prevent-stickiness-in-a-rice-cooker

## whole chicken
https://healinggourmet.com/healthy-recipes/pressure-cooker-chicken/

## chicken noodle soup
[INSTANT POT CHICKEN NOODLE SOUP
](https://perma.cc/BVC4-E24D) 
* 2018-08-16: success!! tasted great, altho I really dislike cutting up chicken
    * 1.6 pounds of chicken seemed a bit excessive when prepping, but seem more reasonable once finished
* 2018-01-07: left it on high pressure for 30 mins versus 5, so gruel

## emaws goulash
http://www.food.com/recipe/emaws-goulash-140213?ftab=reviews

my version:
1 lb ground beef
1 medium onion, chopped
1 medium green bell pepper, chopped
2 -4 garlic cloves, diced and smashed
1 (14 1/2 ounce) can tomatoes, dice if using whole, do not drain
1 (15 1/4 ounce) can whole kernel corn
1 (15 ounce) can tomato sauce
8 ounces dreamfield elbow macaroni
1 -2 tablespoon chopped parsley
salt and pepper

## chili
https://skillet.lifehacker.com/the-first-seven-things-your-should-make-with-a-new-inst-1790730616
modification:
1 large can of diced tomatoes with liquid - replace with red bell pepper

2 pounds of ground meat, such as beef chuck
1 diced onion
2-4 chopped cloves of garlic, depending on your preference
2 cups of dry pinto beans
Seasonings, such as chili powder and cumin
About two cups of stock or broth
Your favorite chilies, depending on your spice tolerance