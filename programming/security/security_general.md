# Security general
See darkest repo

https://twofactorauth.org
* need more hardware tokens
* https://www.dongleauth.info/#

https://cheatsheetseries.owasp.org/

https://www.cvedetails.com - when coding

https://gtfobins.github.io - "GTFOBins is a curated list of Unix binaries that can be exploited by an attacker to bypass local security restrictions"

[Web Application Penetration Testing Cheat Sheet](https://news.ycombinator.com/item?id=16728207)
* https://web.archive.org/web/20180402050046/https://jdow.io/blog/2018/03/18/web-application-penetration-testing-methodology/

## yubikey fido hardware token
https://solokeys.com/

## basics for web server
[Securing a Server with Ansible](https://ryaneschinger.com/blog/securing-a-server-with-ansible/)
* see links there e.g. Bryan Kennedy's post

## automated scanning
https://github.com/golismero/golismero
https://www.owasp.org/index.php/Category:Vulnerability_Scanning_Tools

auditjs - leverages SonaType
* https://blog.sonatype.com/compare-npm-audit-versus-auditjs

## web of trust
[Malicious crossenv package on npm](https://news.ycombinator.com/item?id=14905870) - good discussion

### subkeys
TODO: get deep into this
https://security.stackexchange.com/questions/32935/migrating-gpg-master-keys-as-subkeys-to-new-master-key | key management - Migrating GPG master keys as subkeys to new master key - Information Security Stack Exchange
https://security.stackexchange.com/questions/71743/distributing-public-keys-with-or-without-a-sub-key | key management - Distributing public keys; with or without a sub-key? - Information Security Stack Exchange
https://alexcabal.com/creating-the-perfect-gpg-keypair/ | Creating the perfect GPG keypair - Alex Cabal

## ssl certificates
verification: read https://stackoverflow.com/questions/188266/how-are-ssl-certificates-verified?answertab=votes#comment84224683_188308
for the math

## general
https://github.com/FallibleInc/security-guide-for-developers
https://www.reddit.com/r/netsec/
https://news.ycombinator.com/item?id=15152957
https://security.stackexchange.com/questions/158515/is-penetration-testing-becoming-objectively-harder-as-apps-protocols-operating
https://www.quora.com/How-can-I-use-the-deep-web-to-learn-hacking
https://www.reddit.com/r/hacking/

### openssh
common vector (hit me); need `PasswordAuthentication No` and max tries per https://developer.ibm.com/articles/au-sshlocks/

"Some realistic things you can do are keep software fully patched, tune your firewall rules (do you really need to give access to the whole world?), implement an IDS/IPS on the AWS local network and route all traffic through it, and finally log all traffic and forward to a hardened logging server. 


## 2018-02 ssl link dump
[MacOS command line version of the WireGuard VPN is now available for testing](https://news.ycombinator.com/item?id=17091618) 

https://blog.jessfraz.com/post/installing-and-using-wireguard/
* tailscale


https://community.apigee.com/articles/28041/nodejs-and-self-signed-ssl-certificates.html | Node.js and Self-signed SSL Certificates - Apigee Community
https://deliciousbrains.com/ssl-certificate-authority-for-local-https-development/ | How to Create Your Own SSL Certificate Authority for Local HTTPS Development
http://gsferreira.com/archive/2014/12/overcome-the-depth_zero_self_signed_cert-on-nodejs/ | Overcome the DEPTH_ZERO_SELF_SIGNED_CERT on Node.js - Guilherme Ferreira
https://en.wikipedia.org/wiki/Round-trip_delay_time | Round-trip delay time - Wikipedia
https://istlsfastyet.com/ | Is TLS Fast Yet?
https://security.stackexchange.com/questions/13688/my-understanding-of-how-https-works-gmail-for-example | tls - My understanding of how HTTPS works (gmail for example) - Information Security Stack Exchange
https://gist.github.com/jed/6147872 | How to set up stress-free SSL on an OS X development machine
