vulnerable apps:
* https://owasp.org/www-project-vulnerable-web-applications-directory/

CORS seems useless as it can be easily spoofed, but perhaps it is useful to prevent phishing schemes in combination with some other techniques... don't quite get how it helps since anyone can break in

ANSWER: for third-party sites which are you accessing which can throw a script down
* https://stackoverflow.com/questions/21058183/whats-to-stop-malicious-code-from-spoofing-the-origin-header-to-exploit-cors#comment31686387_21058346 | javascript - What's to stop malicious code from spoofing the "Origin" header to exploit CORS? - Stack Overflow

"getting around CORS" Google search
* https://gist.github.com/jesperorb/6ca596217c8dfba237744966c2b5ab1e
    * not that great...
  * https://gist.github.com/jesperorb/6ca596217c8dfba237744966c2b5ab1e#gistcomment-2872918
    * https://github.com/balvin-perrie/Access-Control-Allow-Origin---Unblock - https://chrome.google.com/webstore/detail/cors-unblock/lfhmikememgdcahcdlaciloancbhjino?hl=en
      * more stars and reviews, prolly better
    * [Bypass CORS](https://chrishham.github.io/BypassCors/) - https://github.com/chrishham/BypassCors
* https://stackoverflow.com/questions/29954037/why-is-an-options-request-sent-and-can-i-disable-it
  * [Can't send a post request when the 'Content-Type' is set to 'application/json'](https://stackoverflow.com/a/39012388/4200039) - seems like text/plain might be the right encoding, then just convert over on the backend
  * https://developer.mozilla.org/en-US/docs/Web/HTTP/CORS

https://cheatsheetseries.owasp.org/cheatsheets/HTML5_Security_Cheat_Sheet.html#cross-origin-resource-sharing
* "Don't rely only on the Origin header for Access Control checks. Browser always sends this header in CORS requests, but may be spoofed outside the browser"
  * so what is the point of all this anyway? - see ANSWER above


## link dump
https://daveceddia.com/access-control-allow-origin-cors-errors-in-react-express/ | Access-Control-Allow-Origin: Dealing with CORS Errors in React and Express
https://infosecwriteups.com/hacking-http-cors-from-inside-out-512cb125c528 | Hacking HTTP CORS from inside out: a theory to practice approach | by Lucas Vinícius da Rosa | InfoSec Write-ups
https://stackoverflow.com/questions/4850702/is-cors-a-secure-way-to-do-cross-domain-ajax-requests | javascript - Is CORS a secure way to do cross-domain AJAX requests? - Stack Overflow
https://dev.to/nicolus/what-you-should-know-about-cors-48d6 | What you should know about CORS - DEV Community
https://stackoverflow.com/questions/1336126/does-every-web-request-send-the-browser-cookies | Does every web request send the browser cookies? - Stack Overflow
https://stackoverflow.com/questions/46288437/set-cookies-for-cross-origin-requests | authentication - Set cookies for cross origin requests - Stack Overflow
https://stackoverflow.com/questions/47932027/how-to-protect-rest-api-with-cors | How to protect REST API with CORS? - Stack Overflow
https://www.linkedin.com/pulse/cross-origin-resource-sharingcors-salesforce-apis-josu%C3%A9-nogueira/ | (2) Cross-origin resource sharing(CORS) and Salesforce APIs | LinkedIn
