
## CVE CVSS
takeaways:
* major thing to note is that that CVSS
https://en.wikipedia.org/wiki/Common_Vulnerability_Scoring_System takes into account remote versus local vulnerability, also whether data is compromised (both important to watch)

https://nvd.nist.gov/general# | NVD - General
https://www.owasp.org/images/7/72/OWASP_Top_10-2017_%28en%29.pdf.pdf | https://www.owasp.org/images/7/72/OWASP_Top_10-2017_%28en%29.pdf.pdf
https://www.openbugbounty.org/ | Open Bug Bounty | Free Bug Bounty Program & Coordinated Vulnerability Disclosure
https://www.bleepingcomputer.com/news/security/nearly-8-000-security-flaws-did-not-receive-a-cve-id-in-2017/ | Nearly 8,000 Security Flaws Did Not Receive a CVE ID in 2017
https://www.riskbasedsecurity.com/2018/04/what-harm-can-come-from-missing-59000-vulnerabilities/ | What Harm Can Come From Missing 59,000 Vulnerabilities?
https://www.riskbasedsecurity.com/2017/01/cvssv3-new-system-new-problems-file-based-attacks/ | CVSSv3: New System, New Problems (File-based Attacks)
https://securitytracker.com/archives/category/121.html | SecurityTracker  >  View Topics  >  Category  >  Application (Web Browser)
https://nvd.nist.gov/vuln/detail/CVE-2010-5312 | NVD - CVE-2010-5312
https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2010-5312 | CVE - CVE-2010-5312
https://www.first.org/cvss/v2/guide | CVSS v2 Complete Documentation
