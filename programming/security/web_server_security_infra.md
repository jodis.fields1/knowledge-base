
## trusting cloud
risk from employees mainly

https://theconversation.com/how-secure-is-your-data-when-its-stored-in-the-cloud-90000 | How secure is your data when it's stored in the cloud?
https://www.bbc.com/news/business-36151754 | Can we trust cloud providers to keep our data safe? - BBC News
https://cloud.google.com/blog/products/identity-security/trust-a-cloud-provider-that-enables-you-to-trust-them-less | It’s easier to trust a cloud provider that enables you to trust them less | Google Cloud Blog
https://www.washingtonpost.com/technology/2020/03/02/cloud-hack-problems/ | How hackers breach unlocked cloud server databases - The Washington Post
https://www.reddit.com/r/linuxadmin/comments/dw7cok/is_it_possible_the_vps_provider_to_gain_access_to/ | Is it possible the VPS provider to gain access to my server? : linuxadmin
https://serverfault.com/questions/987473/can-microsoft-employees-see-my-data-in-azure | sql server - Can Microsoft employees see my data in Azure? - Server Fault
https://aws.amazon.com/compliance/data-privacy-faq/ | Data Privacy - Amazon Web Services (AWS)
