TODO: [key stretching](https://en.wikipedia.org/wiki/Key_stretching) to slow down brute force

symmetric (passwords) is easy but insecure - use it to encrypt private keys tho

* [Is password-based AES encryption secure at all?](https://crypto.stackexchange.com/questions/42538/is-password-based-aes-encryption-secure-at-all)
* [File encryption on the command line](http://backreference.org/2014/08/15/file-encryption-on-the-command-line/)
* [How to brute-force a somewhat remembered aescrypt password?](https://security.stackexchange.com/questions/161592/how-to-brute-force-a-somewhat-remembered-aescrypt-password)

https://alternativeto.net/software/aescrypt/ | AES Crypt Alternatives and Similar Software - AlternativeTo.net
https://crypto.stackexchange.com/questions/42538/is-password-based-aes-encryption-secure-at-all | Is password-based AES encryption secure at all? - Cryptography Stack Exchange
https://www.eetimes.com/document.asp?doc_id=1279619 | How secure is AES against brute force attacks? | EE Times
