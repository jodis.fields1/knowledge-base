
https://www.google.com/search?hl=en&q=chrome%20not%20respecting%20installed%20root%20certificate | chrome not respecting installed root certificate - Google Search

## https in development
https://marmelab.com/blog/2019/01/23/https-in-development.html | HTTPS In Development: A Practical Guide

https://dblazeski.medium.com/chrome-bypass-net-err-cert-invalid-for-development-daefae43eb12 | Chrome: Bypass NET::ERR_CERT_INVALID for development | by Dejan Blazeski | Medium
https://gist.github.com/fntlnz/cf14feb5a46b2eda428e000157447309 | Self Signed Certificate with Custom Root CA
https://github.com/FiloSottile/mkcert | FiloSottile/mkcert: A simple zero-config tool to make locally trusted development certificates with any names you'd like.
https://superuser.com/questions/1192774/can-i-map-a-ip-address-and-a-port-with-etc-hosts | ubuntu 14.04 - Can I map a IP address and a port with /etc/hosts? - Super User
