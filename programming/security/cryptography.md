
TODO: move this to a crypto-play repository

### gpg
#### unattended more fun
seems this has to refer to a config file?
https://www.gnupg.org/documentation/manuals/gnupg/Unattended-GPG-key-generation.html

##### example
after this the foo.pub will be in the directory...

export GNUPGHOME="$(mktemp -d)"
cat >foo <<EOF
     %echo Generating a basic OpenPGP key
     Key-Type: DSA
     Key-Length: 1024
     Subkey-Type: ELG-E
     Subkey-Length: 1024
     Name-Real: Joe Tester
     Name-Comment: with stupid passphrase
     Name-Email: joe@foo.bar
     Expire-Date: 0
     Passphrase: abc
     # Do a commit here, so that we can later print "done" :-)
     %commit
     %echo done
EOF
gpg --batch --generate-key foo

gpg --homedir $(pwd) --generate-key --batch foo