https://jordanfinners.dev/blogs/make-one-resolution-this-year-never-write-your-own-authentication
* cool webmentions thing at the bottom
* alternative view: [Never outsource Auth](https://news.ycombinator.com/item?id=27144930) in [Three things to never build yourself: auth, notifications, payments](https://news.ycombinator.com/item?id=27144930)

[Sick of spending time on Auth, we built an open source 'Stripe for Auth'](https://news.ycombinator.com/item?id=25458033)

[Ask HN: Open-source auth0/octa alternative?](https://news.ycombinator.com/item?id=24306029)
* Keycloak
* https://news.ycombinator.com/item?id=27145992

# new
management:
* https://www.npmjs.com/package/redux-react-session
* https://www.jmfurlott.com/handling-user-session-react-context/
* https://github.com/mozilla/node-client-sessions

https://medium.com/lightrail/getting-token-authentication-right-in-a-stateless-single-page-application-57d0c6474e3 - convenience balanced versus security

https://medium.com/@jcbaey/authentication-in-spa-reactjs-and-vuejs-the-right-way-e4a9ac5cd9a3

https://hackernoon.com/the-best-way-to-securely-manage-user-sessions-91f27eeef460
* overkill, also see https://hackernoon.com/all-you-need-to-know-about-user-session-security-ee5245e6bdad


# old
[OAuth 2.0 Comparisons: The Good, The Bad, The Quirky](https://www.programmableweb.com/news/oauth-2.0-comparisons-good-bad-quirky/2013/01/29)

## 2017
https://dzone.com/articles/oauth-20-beginners-guide
https://auth0.com/blog/cookies-vs-tokens-definitive-guide/ - response at http://cryto.net/~joepie91/blog/2016/06/13/stop-using-jwt-for-sessions/

## OAuth versus OpenID
[OpenID vs. OAuth [duplicate\]](http://stackoverflow.com/questions/3376141/openid-vs-oauth) - reasonable answer

**OpenID Connect** - learn at [openid.net](http://openid.net)

## Sessions (cookies)
Cookies - domain is important; only gets sent w/ matching domain?

Cookies - http://www.htmlgoodies.com/beyond/javascript/article.php/3470821 has tips

Javascript API https://github.com/js-cookie/js-cookie: https://stackoverflow.com/questions/1458724/how-do-i-set-unset-a-cookie-with-jquery?rq=1

https://security.stackexchange.com/questions/178663/why-isnt-stealing-cookies-enough-to-authenticate
* see also cookies discussion in browsers_extensions.md
