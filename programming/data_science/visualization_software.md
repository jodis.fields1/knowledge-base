vizydrop.com - drop a file to create a chart

# Visualization and software

Tableau Software - apparently really good at this

[Blockspring for Google Sheets](https://api.blockspring.com/blog/blockspring-for-google-sheets) - 

**Settings and preferences**

Ended up finding that some settings are inside .xlsx files which can be unzipped and viewed, see calculation mode below

File->Options - which of these are workbook-specific versus workbook-wide?

[Excel 2010 settings file](http://eileenslounge.com/viewtopic.php?f=27&t=5678 "http://eileenslounge.com/viewtopic.php?f=27&t=5678") -> mostly stored in the registry, unfortunately; for example HKEY_CURRENT_USER\Software\Microsoft\Office\XX.0\Excel\Options

Note that there is an XLStart page but not clear what it does. At work it is at C:\Users\bjcreasy\AppData\Roaming\Microsoft\Excel which I found   [thru techrepublic tip](http://www.techrepublic.com/blog/microsoft-office/quick-tip-find-excels-xlstart-folder-fast/ "http://www.techrepublic.com/blog/microsoft-office/quick-tip-find-excels-xlstart-folder-fast/")

excel*.xlb (excel14.xlb for 2010) is in this folder and stores the toolbar and menu customization per   [techtarget article](http://whatis.techtarget.com/fileformat/XLB-Toolbar-file-Microsoft-Excel "http://whatis.techtarget.com/fileformat/XLB-Toolbar-file-Microsoft-Excel")

In xlstart there is both personal.xlsb and book.xltx (see   [save cell styles](http://office.microsoft.com/en-us/excel-help/save-cell-styles-to-use-in-all-new-workbooks-HP001216733.aspx "http://office.microsoft.com/en-us/excel-help/save-cell-styles-to-use-in-all-new-workbooks-HP001216733.aspx"))

[Controlling Excel Calculation](http://www.decisionmodels.com/calcsecretse.htm "http://www.decisionmodels.com/calcsecretse.htm") - lists the application-wide and workbook-specific settings

_Calculation mode_ \- under Formula; this appears to be Workbook-specific but I figured out 2014-05 that these are in the zipped Excel file under xl->workbook.xml (see   [blog post I commented on](http://www.ion.icaew.com/itcounts/post/Excel-Tip-of-the-Week--27---Excel-options "http://www.ion.icaew.com/itcounts/post/Excel-Tip-of-the-Week--27---Excel-options")). the   [suggestion from excel.tips.net](http://excel.tips.net/T001988_Forcing_Manual_Calculation_For_a_Workbook.html "http://excel.tips.net/T001988_Forcing_Manual_Calculation_For_a_Workbook.html")  (2012) actually see   [Setting the Calculation Default ](http://excelribbon.tips.net/T009310_Setting_the_Calculation_Default.html "http://excelribbon.tips.net/T009310_Setting_the_Calculation_Default.html")(2014) may not work   [per Stackoverflow](http://stackoverflow.com/questions/17106544/how-to-set-calculation-mode-to-manual-when-opening-an-excel-file "http://stackoverflow.com/questions/17106544/how-to-set-calculation-mode-to-manual-when-opening-an-excel-file"). Ran "calculation" search at mrexcel.com and excelforum.com and found a little bit more info.

**Quick tips**

Look into MZTools freeware to make VBA more powerful
[Convert Excel Column to text file seperated by commas](http://www.excelforum.com/excel-general/641984-convert-excel-column-to-text-file-seperated-by-commas.html "http://www.excelforum.com/excel-general/641984-convert-excel-column-to-text-file-seperated-by-commas.html") - basically paste into word and use find&replace

_Sheet navigation_  - these get overwhelming fast; right-click on the arrows ("Sheet Navigation"   per techrepublic), but also set some macro solutions and an   [index sheet per ozgrid](http://www.ozgrid.com/VBA/sheet-index.htm "http://www.ozgrid.com/VBA/sheet-index.htm")

**Formatting** \- super important! also note saving cell styles in book.xltx

[What are the best practices when modelling in Excel?](http://www.quora.com/What-are-the-best-practices-when-modelling-in-Excel "http://www.quora.com/What-are-the-best-practices-when-modelling-in-Excel") - Bruno Sindicic has great, great advice

**Regression analysis**

Doing a regression analysis in Excelis a bit tricky; you can show a trendline after making a a scatterplot; in Excel2007 you should "edit data" and then you can set the x-data and y-data

http://office.microsoft.com/en-us/ excel-help/perform-a-regression-analysis-HA001111963.aspx - make sure you select a big enough range for the results, use $ signs (use F4) for all the inputs; and use CTRL-SHIFT-ENTER; see the link for more clear explanation. The years (in periods) are likely the dependent variable.Note that you can make your analysis annual using quarterly data by using .25 for the periods

http://www.actuarialoutpost.com/actuarial_discussion_forum/showpost.php?p=5003501&postcount=2 is a good guide with an attached exceldocument; however, still no explanation for how to do the LOGEST version

Time series and forecasting: reviewed   [Forecasting Time Series within Excel](http://www.calstatela.edu/faculty/hwarren/a503/forecast%20time%20series%20within%20Excel.htm "http://www.calstatela.edu/faculty/hwarren/a503/forecast%20time%20series%20within%20Excel.htm") which is a class   [Quantitative Methods: ACCT 503 Syllabus](http://www.calstatela.edu/faculty/hwarren/a503/syllabusA503.htm "http://www.calstatela.edu/faculty/hwarren/a503/syllabusA503.htm")

Powerful Forecasting with Excel looks better, only   [available thru xlpert.com for $36](http://www.xlpert.com/buy-now.html "http://www.xlpert.com/buy-now.html")

**Tables** \- meaning special data type under Insert->Table

These can be very frustrating, especially with headers and filters. The key is to convert to a range. (Not really sure what this means.)

  


**Other**

_Index-Match_  - better than vlookup; I like this intro  <http://www.randomwok.com/excel/how-to-use-index-match/> but also consulted http://superuser.com/questions/480338/spreadsheet-vlookup-search-in-second-column-and-return-first; note that I did seem to find an error in the 2007 version with the "less than" option (1) as it went to the smallest rather t han largest of those less than

  


_Excel side-by-side_ - Appears there isn't real easy ways to do this; http://superuser.com/questions/273977/need-two- excel-windows-side-by-side-on-different-monitors-in-the-same-instance notably does not discuss the little box you can check

  


http://www.interworks.com/blogs/tlester/2011/02/28/how-open-two- excel-documents-side-side also discusses how to do this

  


_Canvas interface_

Apple's iWork Numbers has a different take on spreadsheets where it is a blank canvas that you can toss tables on. This really helps deal with the issues of cells being the wrong width or height. [7 ideas Excelcan pick-up from iWork Numbers](http://chandoo.org/wp/2009/09/21/ excel-vs-iwork-numbers/) doesn't pick up on that but the commenters do. CNet review has some user complaints http://download.cnet.com/Apple-iWork/3000-18483_4-55181.html

  


Per http://news.cnet.com/8301-13579_3-57522442-37/apple-sued-over-spreadsheet-technology/ Apple has been used; Data Engine Technologies v. Apple; apparently they have patent 7,954,047but it conflicts with a 1995 patent

  


Interesting discussion at http://smurfonspreadsheets.wordpress.com/2010/02/10/the-better-spreadsheet-fallacy/ on innovating with spreadsheets

  


Spreadsheets inefficient? See http://eckemoffllc.com/about/eliminate-spreadsheets/ http://flightmap.wordpress.com/2012/02/14/portfolio-management-with-a-spreadsheet/ and http://www.k2e.com/tech-update/articles/53-are-you-wasting-time-with-inefficient- excel-techniques (more on data scraping)

  


_Keyboard shortcut_ \- I think you have to use alt+num to set these up but can't remember

Outdated list from Wall Street Oasis email:

Control + C = Copy

Alt + E + S = Paste special

Shift + arrow keys = Select cells

Control + 1 = Format box

Alt + H + O + I = Autofit columns

Control + spacebar = Select an entire column

Shift + spacebar = Select an entire row

Control + ~ = Show formulas

F9 = Recalculate workbooks

Alt + = ....= Autosum

F2 = Open a cell without using your mouse

Shift + F2 = Insert a comment

  


**Spreadsheet programs**

  


http://en.wikipedia.org/wiki/Comparison_of_spreadsheet_software

http://alternativeto.net/software/numbers/

SoftMaker PlanMaker - http://www.softmaker.com/english/education_en.htm does not seem to work

Quantrix - inspired by Lotus Improv; inspired pivot tables apparently

EditGrid - ?

KOffice - doesn't look optimistic here; just one screenshot per http://www.koffice.org/kcells/screenshots/

Calligra - split from KOffice; most moved to Calligra

Gnumeric - known for accuracy in scientific work

Siag - runs on old systems

Tabulus - only $500 http://www.tabulus.com

Google Drive Fusion Tables - https://www.google.com/fusiontables

  


**Visualization and charts**

  


_Bubbles along a line_

Worked with Excelspreadsheets as part of a graph project idea I had (see attached image). It needs to convey the mean, standard deviations, and credibility (volume of data) of a bunch of different variables at the same time (it's insurance metrics which are volatile if not based on a lot of data) I've been trying to figure out how to a bubble chart with no Y axis - the bubbles just sit along a straight line. There's a question at http://stackoverflow.com/questions/5673239/1-dimensional-chart-in-flex about this; does not seem to be possible in Excel. mike suggested that I just have the y-variable as 0 - and it turns out that does work. But there's too much overlap - the main problem is that the bubbles are either too small or too big. Could transform them using logarithms.

  


I settled on an XY scatterplot with the X axis as a measure of credibility (premium) as a compromise. I set the x-axis at the mean so it was right in the middle of the page. I was able to add the standard deviation lines by adding new series and creating linear trendlines for them similar to http://processtrends.com/pg_charts_horizontal_line.htm.

  


_XY Data Labels_

The one major issue is that I couldn't add named data labels except manually (which I discovered per instructions at http://www.excelbanter.com/showthread.php?t=37058). There are add-ins to do this: [J-Walk Chart Tools Add-In](http://spreadsheetpage.com/index.php/file/j_walk_chart_tools_add_in/) does not work in Excel2007, and the [XY Chart Labeler](http://www.appspro.com/Utilities/ChartLabeler.htm) requires admin privileges to install.

  


However, after thinking about this, I think perhaps a bar chart might work just as well or better, particularly one that is like http://excelribbon.tips.net/T001188_Two-Level_Axis_Labels.html and compares the national alongside the countrywide.

  


I also learned how to show different series in the XY across years (and presumably across different x-axis labels, such as companies) from http://answers.yahoo.com/question/index?qid=20090803063556AAyzZL1. This could work for what I'm trying to show.

  


_Thermometer_

Nice trick - Harvey Tantilla sent email showing how to create but it screwed up; lots of tips online such as http://www.ehow.com/how_4865470_make-thermometer-chart-microsoft- excel.html;

  


_Visualization_

See home art and decorations for more

Saving Obamacare article on Sha Hwang calls him a design genius - exaggeration, but someone to watch out for. This might fit better in public sector technology.

Took a look at Smashing's article on modern visualization (http://www.smashingmagazine.com/2007/08/02/data-visualization-modern-approaches/) - nothing really piqued my interest except for creating timelines with Xtimeline and Circavie (did not test either), IBM's Many Eyes (http://www-958.ibm.com/software/data/cognos/manyeyes/visualizations), Prefuse for data visualization; Swivel looked good but is dead according to Crunchbase (which notes Freebase as similar - bought by Google but still existing). See also Smashing's tags for http://www.smashingmagazine.com/tag/charts/ and http://www.smashingmagazine.com/tag/data-visualization/.

  


_Formatting_

From   [It’s Always in the Details: Excel, interviewing and life](http://www.wallstreetoasis.com/blog/it%E2%80%99s-always-in-the-details-excel-interviewing-and-life?utm_source=newsletter&utm_medium=newsletterfeb21&utm_campaign=newsletter "http://www.wallstreetoasis.com/blog/it%E2%80%99s-always-in-the-details-excel-interviewing-and-life?utm_source=newsletter&utm_medium=newsletterfeb21&utm_campaign=newsletter") suggestion is colour code: assumptions in blue, calculations in black and green 

_  
_

How to get that clean look in Excel? Use zebra striping to start (read   [Zebra Striping: Does it Really Help?](http://alistapart.com/article/zebrastripingdoesithelp "http://alistapart.com/article/zebrastripingdoesithelp") which found a subjective preference); in Excel use format as table but be careful to not have fill already

Also reviewed   [How to Make Your Spreadsheets Less Lame](http://designshack.net/articles/graphics/how-to-make-your-spreadsheets-less-lame/ "http://designshack.net/articles/graphics/how-to-make-your-spreadsheets-less-lame/") which has good tips

[It Turns Out You Can Make Pretty Charts in Excel](http://priceonomics.com/it-turns-out-you-can-make-pretty-charts-in-excel/ "http://priceonomics.com/it-turns-out-you-can-make-pretty-charts-in-excel/") - pricenomics

  


**Resources**  - see   [Excel xmarks search](http://www.xmarks.com/suggest/topic/excel "http://www.xmarks.com/suggest/topic/excel")

Excelribbon.tips.net - 2007+ version of excel.tips.net with   [lots of tips](http://excelribbon.tips.net/ "http://excelribbon.tips.net/")

[Excelforum](http://www.excelforum.com/ "http://www.excelforum.com/") - one of the bigger forums, signed up to it

answers.microsoft.com - of course...

mrexcel.com - another biggie

vertex42.com - etc

spreadsheetpage.com - ditto

exinfm.com/free_spreadsheets.html - 

mvps.org/dmcritchie/excel/excel.htm - ?
