# machine learning
Sentiment analysis: http://sage.thesharps.us/2016/12/30/update-on-sentiment-analysis-of-foss-communities/
* do this for Wikipedia
* https://www.theverge.com/2019/3/14/18265851/alphabet-google-jigsaw-tune-chrome-extension

[A friendly Introduction to Backpropagation in Python](https://news.ycombinator.com/item?id=15782156)

[A visual introduction to machine learning](http://www.r2d3.us/visual-intro-to-machine-learning-part-1/) - data at https://github.com/jadeyee/r2d3-part-1-data

### natural language processing
https://venturebeat.com/2019/08/31/the-shift-toward-open-source-conversational-ai/amp/

### deep learning
https://www.zerotodeeplearning.com/
    * not sure if https://book.zerotodeeplearning.com from newline is related

### machine learning
[Which GPU(s) to Get for Deep Learning: My Experience and Advice for Using GPUs in Deep Learning](http://timdettmers.com/2017/04/09/which-gpu-for-deep-learning/) -

#### automated coding
[Turning deep-learning AI loose on software development](https://www.sciencedaily.com/releases/2018/04/180425120203.htm)
    * BAYOU
[Turning Design Mockups Into Code With Deep Learning](https://blog.floydhub.com/turning-design-mockups-into-code-with-deep-learning/) - 

## Data science
[Python Data Science Handbook](https://news.ycombinator.com/item?id=15127672) - 

[Top 10 data mining algorithms in plain English](http://rayli.net/blog/data/top-10-data-mining-algorithms-in-plain-english/) - really well-written, and nearly [2000 points on r/programming](https://www.reddit.com/r/programming/comments/36a6b5/top_10_data_mining_algorithms_in_plain_english/)

RapidMiner is a very popular tool
https://github.com/pachyderm/pachyderm - versioned data containers