https://www.reddit.com/r/programming/comments/8shzcu/yaml_probably_not_so_great_after_all/
    * always quote booleans, in case you get hit with NO -> false
    * use yaml 1.2, 
        * use https://bitbucket.org/ruamel/yaml for Python
        * avoid pyYAML per https://github.com/yaml/pyyaml/issues/116

## json
I like json5

## yaml
2018-03: read Wikipedia article, pretty good, also looked at https://web.archive.org/web/20180328145305/https://medium.com/@sidneyliebrand/the-greatnesses-and-gotchas-of-yaml-5e3377ef0c55
    * contains lots of links and resources