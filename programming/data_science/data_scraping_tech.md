Scraping technology
===

## new
https://www.knime.com/community/palladian | Palladian Nodes for KNIME (trusted extension) | KNIME
https://readypipe.com/ | Readypipe - All-in-one platform to run your web scrapers


## old
[I Don’t Need No Stinking API – Web Scraping in 2016 and Beyond](https://franciskim.co/2016/08/24/dont-need-no-stinking-api-web-scraping-2016-beyond/)
**Web Scraping 101 with Python - reddit thread with good advice **
[Ask HN: What are best tools for web scraping?](https://news.ycombinator.com/item?id=15694118) - 2018
See http://wikiposit.org/w and https://scraperwiki.com/ and http://www.programmableweb.com/ for APIs

urllib2 for python should be replaced with  [requests](http://www.python-requests.org/en/latest/) and also use  [Scrapy](http://scrapy.org/) or  [Beautiful Soup 4](http://www.crummy.com/software/BeautifulSoup/#Download); also see  [mechanize](http://wwwsearch.sourceforge.net/mechanize/)

**PDF scraping**

PDFMiner - noticed this https://groups.google.com/forum/?fromgroups=#!starred/pdfminer-users/09N_JzQf4MI discussing tutorials (there are none)

http://pdfbox.apache.org/ - PDFBox is likely better documented; uses Java

Sunlight Foundation PDF Liberation Front likely has resources listed at  [this blog](http://pdfliberation.wordpress.com/) but focused on  [Tabula](http://source.mozillaopennews.org/en-US/articles/introducing-tabula/)and [Ashima’s PDF Table Extractor](https://github.com/ashima/pdf-table-extract)

Using Adobe XI and reading  [How to copy/Paste a table from PDF to Excel using AcrobatX](http://forums.adobe.com/message/4189040), I found that you have to select the area around the table rather than the table elements themselves

**HTML parsing and regexp**

[https://en.wikipedia.org/wiki/Comparison_of_HTML_parsers](https://en.wikipedia.org/wiki/Comparison_of_HTML_parsers)

[https://github.com/cheeriojs/cheerio](https://github.com/cheeriojs/cheerio) - seems to be best

[Webscraping with CasperJS, PhantomJS, jQuery, and XPath](https://gist.github.com/subelsky/3297506) - also see

*Xpath*

[XPath is actually pretty useful once it stops being confusing](https://web.archive.org/web/20161217081146/https://genius.com/Mat-brown-xpath-is-actually-pretty-useful-once-it-stops-being-confusing-annotated) (2015) for xpath intro

[XBRL parser](https://www.reddit.com/r/finance/comments/4988uj/hi_guys_i_created_an_sec_edgar_xbrl_scraper_and/) (2016)- haven't tried it

XPath: access with [document.evaluate per MDN](https://developer.mozilla.org/en-US/docs/Web/API/Document/evaluate) and need to iterate or get the snapshop type per [this SO question](http://stackoverflow.com/questions/5587835/xpath-evaluate-not-returning-anything); however it may need to be proper XML (may be able to use HTML Tidy to convert)