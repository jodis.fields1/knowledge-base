# algorithmic algo trading

See archive_notes/automated_trading.md

For my python programmers out there, what module do you use to get your data? Why is that your choice out of everything else?
* ameritrade recommended

https://youtu.be/RQqXWrmu9G8?t=584
* Gritanni -> thinkorswim scan / study shows all stocks break high of day after 45 minutes consolidation

Testing tickers ZVZZT ZWZZT:
* https://www.nasdaqtrader.com/MicroNews.aspx?id=ERA2016-3

Penny stock trading automation:
* https://twitter.com/quantlabs/status/1212078526784036864

[Max Dama on Automated Trading](https://news.ycombinator.com/item?id=19215026)
* interesting stories on RenTech, https://meanderful.blogspot.com/2018/01/the-accidental-hft-firm.html

[A simple custom formula: Percentage of Float Traded Today](https://www.trade-ideas.com/2015/08/21/a-simple-custom-formula-percentage-of-float-traded-today/)

[Regulating robo-finance: an exposé on the SEC approach to algorithmic trading](https://medium.com/anderson-law-journal/regulating-robo-finance-an-expos%C3%A9-on-recently-ratified-sec-rule-requiring-algorithmic-trading-f27c8eb5b338)

## people
https://twitter.com/paidtofade/status/1404291571231641601

## broker
https://twitter.com/drelasher/status/1216055724733759488 - recommends TDA

https://github.com/tylerfloyd/TastyWorks/issues/10

alpaca https://alpaca.markets
* https://twitter.com/AlpacaHQ/status/1215060657483210754
* [Show HN: Stock Trading from Google Spreadsheet](https://news.ycombinator.com/item?id=18606766)

https://tradier.com/ - dunno

## follow strategies
Collective2

Mudrex - cryto, ugh

Nvstr

## testimonials
https://www.reddit.com/r/RobinHood/comments/5mbcxj/my_journey_through_algorithmic_trading_in_2016/

https://news.ycombinator.com/item?id=20720095 | I've reproduced 130 research papers about “predicting the stock market” | Hacker News

## zipline
https://www.quantopian.com/tutorials | Quantopian Tutorials
https://www.zipline.io/beginner-tutorial.html | Zipline Beginner Tutorial — Zipline 1.3.0 documentation
https://github.com/zipline-live/zipline | zipline-live/zipline: Zipline-Live, a Pythonic Algorithmic Trading Library

https://groups.google.com/forum/m/#!topic/zipline/KHKI1PZx08I | Live trading - Google Groups

## other
https://github.com/EliteQuant/EliteQuant | EliteQuant/EliteQuant: A list of online resources for quantitative modeling, trading, portfolio management
https://github.com/EliteQuant/EliteQuant_Excel | EliteQuant/EliteQuant_Excel: EliteQuant Excel for Quantitative Modeling, Trading, and Portfolio Management. It enables you to create quantitative financial models in Excel spreadsheet, in the same way how financial professionals such as traders, quants, and portfolio managers do their day to day work. You are able to create pricing tools for products across all asset classes such as interest rate or FX, and from plain vanilla to exotic instruments. You are also able to backtest and live trade from Excel, with the so-called RTD, or real-time data support.

https://github.com/foolcage/fooltrader/blob/master/README-en.md | fooltrader/README-en.md at master · foolcage/fooltrader

https://github.com/hustcer/star | hustcer/star: A STock Analysis and Research tool for terminal(cli) users. 技术控和命令行爱好者的 A 股辅助分析工具。

https://github.com/InvestmentSystems/static-frame | InvestmentSystems/static-frame: The StaticFrame library consists of the Series and Frame, immutable data structures for one- and two-dimensional calculations with self-aligning, labelled axes.

https://github.com/LongOnly/Quantitative-Notebooks | LongOnly/Quantitative-Notebooks: Educational notebooks on quantitative finance, algorithmic trading, financial modelling and investment strategy

https://github.com/nickslevine/zebras | nickslevine/zebras: Data analysis library for JavaScript built with Ramda
* https://observablehq.com/@nickslevine/introduction-to-zebras-a-data-analysis-library-for-javascr | Introduction to zebras - a data analysis library for JavaScript / nickslevine / Observable

https://news.ycombinator.com/item?id=20726842 | Stock trading strategies using machine learning | Hacker News

https://financial-hacker.com/ | The Financial Hacker – A new view on algorithmic trading

https://github.com/business-science/tidyquant | business-science/tidyquant: Bringing financial analysis to the tidyverse

## zorro
https://robotwealth.com/my-experience-dealing-with-zorros-support-team/ | My Experience Dealing with Zorro's Support Team - Robot Wealth
https://github.com/AndrewAMD/AllyInvestZorroPlugin | AndrewAMD/AllyInvestZorroPlugin: A Zorro broker API plugin for Ally Invest, written in Win32 C++.
https://opserver.de/ubb7/ubbthreads.php?ubb=showflat&Number=476126&page=1 | Zorro on Linux - lite-C Forums

## pandas
https://kdboller.github.io/2018/07/13/python-for-finance-plotly-dash.html | Python for Finance: Dash by Plotly | Data Informed Narratives
https://kdboller.github.io/2018/03/04/scaling-financial-insights-with-python.html | Scaling Financial Insights with Python | Data Informed Narratives
https://www.firstpythonnotebook.org/pandas/ | Chapter 3: Hello pandas — First Python Notebook 1.0 documentation
https://www.reddit.com/r/RobinHood/comments/7173l1/python_script_for_portfolio_tracking/ | Python script for portfolio tracking : RobinHood
https://pydata.github.io/pandas-datareader/stable/remote_data.html | Remote Data Access — pandas-datareader 0.7.0 documentation
https://github.com/pydata/pandas-datareader | pydata/pandas-datareader: Extract data from a wide range of Internet sources into a pandas DataFrame.

https://jakevdp.github.io/PythonDataScienceHandbook/02.00-introduction-to-numpy.html | Introduction to NumPy | Python Data Science Handbook

https://news.ycombinator.com/item?id=19477868&utm_term=comment | Mathigon – an interactive, personalized mathematics textbook | Hacker News
https://towardsdatascience.com/two-essential-pandas-add-ons-499c1c9b65de | Two essential Pandas add-ons - Towards Data Science
