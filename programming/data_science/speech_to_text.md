# speech-to-text

## projects
https://gitlab.com/jcrben-play-learn/google-speech-to-text

Motivation: transcribe legislators' speeches similar to https://www.digitaldemocracy.org/about-digital-democracy | About Digital Democracy | Digital Democracy

TL;DR: just read https://medium.com/cod3/convert-speech-from-an-audio-file-to-text-using-google-speech-api-b951f4032a64 | Convert speech from an audio file to text using Google Speech API

## audio file conversion
learned a bit about ffmpeg

https://github.com/srbcheema1/medipack | srbcheema1/medipack
https://superuser.com/questions/1379157/whats-the-difference-in-ffmpeg-between-using-cv-libx264-cv-copy-and-vcode | video - What's the difference in FFMPEG between using -c:v libx264, -c:v copy and -vcodec copy? - Super User
https://askubuntu.com/questions/59383/extract-part-of-a-video-with-a-one-line-command | Extract part of a video with a one-line command - Ask Ubuntu

## link dump
https://www.armedia.com/blog/transcription-services-aws-google-ibm-nuance/ | Transcription Services Showdown: AWS vs. Google vs. IBM vs. Nuance
https://cloud.google.com/speech-to-text/ | Cloud Speech-to-Text - Speech Recognition  |  Cloud Speech-to-Text  |  Google Cloud
https://github.com/GoogleCloudPlatform/golang-samples/tree/master/speech/captionasync | golang-samples/speech/captionasync at master · GoogleCloudPlatform/golang-samples
https://cloud.google.com/speech-to-text/docs/async-recognize | Transcribing long audio files  |  Cloud Speech-to-Text Documentation  |  Google Cloud
https://developers.google.com/apis-explorer/?hl=en_US#p/speech/v1/ | Google APIs Explorer
https://github.com/googleapis/nodejs-storage/blob/master/src/storage.ts | nodejs-storage/storage.ts at master · googleapis/nodejs-storage
https://github.com/googleapis/nodejs-storage/blob/master/samples/buckets.js | nodejs-storage/buckets.js at master · googleapis/nodejs-storage
https://github.com/googleapis/nodejs-speech/blob/2e647d83ddfe3e1afb5dcf7e748c6db7a0b07b75/samples/recognize.js#L251 | nodejs-speech/recognize.js at 2e647d83ddfe3e1afb5dcf7e748c6db7a0b07b75 · googleapis/nodejs-speech
https://cloud.google.com/speech-to-text/docs/samples | Sample applications  |  Cloud Speech-to-Text Documentation  |  Google Cloud

## cruft
https://www.npmjs.com/package/nodejs-docs-samples-speech | nodejs-docs-samples-speech - npm
