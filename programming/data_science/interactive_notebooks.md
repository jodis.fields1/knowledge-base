https://github.com/knsv/mermaid - "Generation of diagram and flowchart from text in a similar manner as markdown" - using this with remarkjs presentations

## jupyter notebooks
See dotfiles notes

### jupyter extensions
JavaScript:
    * [n-riesco/ijavascript](https://github.com/n-riesco/ijavascript)
    * [Nodebooks: Introducing Node.js Data Science Notebooks](https://medium.com/ibm-watson-data-lab/nodebooks-node-js-data-science-notebooks-aa140bea21ba)

[timbr-io/jupyter-react-js](https://github.com/timbr-io/jupyter-react-js) - 2016, dead

## observable notebooks
JavaScript notebooks from D3 creator & Backbone creator
[Show HN: Observable Notebooks](https://news.ycombinator.com/item?id=16274686) - 
