Keywords: data science, extraction, etc machine learning

Projects: see stock-analysis/../scraping_tools.md

Sources:
* https://quant.stackexchange.com/questions/141/what-data-sources-are-available-online
* https://en.wikipedia.org/wiki/List_of_financial_data_feeds
* Quandl https://www.quandl.com/ - seems good
* CKAN - powers a bunch of data websites
    * https://ckan.org/2018/09/20/open-letter-response/ - looks like US will be using them for a big project
* FRED - federal reserve

**Data**- note finance specific data elsewhere in financial dataand macro outlook

[http://weboob.org/](http://weboob.org/) - partnered with Cozy which is also cool!

[http://schema.org/](http://schema.org/) - foundation of metadata

also see microformats.org - not sure how they relate

[Common crawl](http://commoncrawl.org/) !! - all the web to play with

## finance
FRED for financial data

Pandas examples: 
Yahoo: $ aapl.yahoo <- read.csv("http://ichart.finance.yahoo.com/table.csv?s=AAPL", sep=",", header=1)

Google: $ aapl.google <- read.csv("http://www.google.com/finance/historical?q=NASDAQ:AAPL&authuser=0&output=csv ", sep=",", header=1)

## outdated
Pete Warden - author of  data science toolkit and master of scrapingdata http://www.datasciencetoolkit.org/ - https://github.com/petewarden/dstk