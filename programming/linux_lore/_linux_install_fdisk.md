
https://www.slant.co/improve/topics/5086/~partitioning-tools-for-linux - GParted

## fdisk / parted / etc
* prolly few of these are well unit-tested
* bunch of shit with different names
    * summarized at http://dwaves.de/2017/05/23/linux-overview-partitions-difference-fdisk-gdisk-cfdisk-parted-gparted/ | » linux overview partitions – difference fdisk gdisk cfdisk parted gparted
* https://unix.stackexchange.com/questions/104238/fdisk-vs-parted | fdisk vs parted - Unix & Linux Stack Exchange
* GPT fdisk - https://askubuntu.com/questions/340841/the-state-of-partitioning-tools | fdisk - The state of partitioning tools? - Ask Ubuntu
* GNU fdisk - modular source per http://savannah.gnu.org/forum/forum.php?forum_id=6973
    * https://www.gnu.org/software/fdisk/ | fdisk - GNU Project - Free Software Foundation (FSF)

### other
https://en.wikipedia.org/wiki/Util-linux | util-linux - Wikipedia
https://en.wikipedia.org/wiki/GNU_Parted | GNU Parted - Wikipedia
https://inconsolation.wordpress.com/2013/11/07/fdisk/ | fdisk: One that’s not from coreutils | Inconsolation
https://github.com/andreiw/mac-fdisk | andreiw/mac-fdisk: Debian/Ubuntu's mac-fdisk (pdisk) for manipulating Apple Partition Maps. For everyone else (like Arch).
https://blog.stgolabs.net/2012/09/fdisk-updates-and-gpt-support.html | fdisk updates and GPT support | Davidlohr Bueso

### cfdisk
https://unix.stackexchange.com/questions/194680/cfdisk-or-fdisk | linux - cfdisk or fdisk? - Unix & Linux Stack Exchange
https://github.com/karelzak/util-linux/blob/master/disk-utils/cfdisk.8 | util-linux/cfdisk.8 at master · karelzak/util-linux

### gpt fdisk ??
http://www.rodsbooks.com/gdisk/ | GPT fdisk Tutorial

### GNU fdisk
https://www.serverwatch.com/server-tutorials/theres-a-new-gnu-fdisk-in-town.html | There's a New GNU fdisk in Town -- ServerWatch Tip of the Trade
http://lists.gnu.org/archive/html/bug-fdisk/2017-11/msg00000.html | [bug-fdisk] Proposals for a new name

