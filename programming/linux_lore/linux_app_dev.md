TL;DR - use electron apps as much as possible

[Afraid of Makefiles? Don't be](https://news.ycombinator.com/item?id=15041986)

## kde
### Javascript
motivated by https://github.com/lingtjien/Tiling-Gaps-Kwin/blob/7d3d5e0ee302a753e1f7ec2e860337aea9b109e2/metadata.desktop
"Starting with the KDE Platform 4.7 release, the Plasma team recommends writing your Plasmoids with QtQuick technologies with the Plasma QML integration API." - https://techbase.kde.org/Development/Tutorials/Plasma4/JavaScript/API - tutorial over
at https://techbase.kde.org/Development/Tutorials/Plasma/JavaScript/ConfigDialog?

### QtQick
https://wiki.qt.io/Language_Bindings - lots
"project = QTBUG AND resolution = Unresolved AND component = "QML: Declarative and Javascript Engine" at https://bugreports.qt.io/browse/ pulls up nearly 1000
https://doc.qt.io/qt-5/qtscript-attribution-javascriptcore.html
Qt/qt-5/qtscript/src/3rdparty/javascriptcore/JavaScriptCore/ - seems pretty old
QtQuick for node.js https://github.com/BrigJS/brig
[QML vs. HTML5](https://blog.qt.io/blog/2017/07/28/qml-vs-html5/) - not all that fair

## GNOME
[Step by step tutorial to create extensions](https://wiki.gnome.org/Projects/GnomeShell/Extensions/StepByStepTutorial) - ??

### JavaScript
Philip Chimento is maintaining this for his employer Endless OS 
https://gitlab.gnome.org/GNOME/gjs/blob/master/NEWS says Spidermonkey 52

[API documentation for writing GNOME applications in JavaScript](http://devdocs.baznga.org/) -
Two intepreters per [Javascript in GNOME](https://wiki.gnome.org/JavaScript)
"Travis Reitter reports that the GNOME project has settled on JavaScript as the primary language for application development." https://lwn.net/Articles/536241/
JavaScript - https://github.com/GNOME/gjs
https://github.com/niagr/gjs-ts and https://github.com/sammydre/ts-for-gjs - typescript

lots of discussion over at [WebReflection/node-gtk](https://github.com/WebReflection/node-gtk/issues/36) on an attempt to bring the actual runtime to Linux proper rather than run thru gjs (lags) or Electron (browser)

## OS.js
https://github.com/os-js/OS.js - maybe consider typescript https://github.com/os-js/OS.js/issues/668


## IPC and dbus etc
seems like this is important?
awesome overview:
http://0pointer.net/blog/the-new-sd-bus-api-of-systemd.html | The new sd-bus API of systemd