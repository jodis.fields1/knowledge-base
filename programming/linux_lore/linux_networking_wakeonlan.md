Linux networking - wakeonlan
===

## Mac
`brew install wakeonlan` per https://www.cyberciti.biz/faq/apple-os-x-wake-on-lancommand-line-utility/

## Linux
https://wiki.archlinux.org/index.php/Wake-on-LAN

eth-wake per  https://serverfault.com/questions/191854/does-wake-on-lan-wol-depend-on-hardware-or-the-operating-system

### wireless
possibly fixed by https://bugs.launchpad.net/ubuntu/+source/linux/+bug/1742094

Does not work for my Thinkpad T460s per https://forum.snapcraft.io/t/network-manager-broken-for-desktop-ubuntu/12512/34
* also see https://forums.lenovo.com/t5/ThinkPad-T400-T500-and-newer-T/T440p-Wake-on-Wireless-LAN-WoWLAN-support/td-p/3531280 and https://forums.lenovo.com/t5/ThinkPad-X-Series-Laptops/Thinkpad-x1-Yoga-doesn-t-support-WoWLAN-wake-on-wireless-Local/td-p/3611613 and https://forums.lenovo.com/t5/Gaming-Laptops/Wake-on-Wireless-LAN-WoWLAN-legion-y530-not-working/td-p/4560943

https://www.phoronix.com/scan.php?page=news_item&px=NetworkManager-WoWLAN
* commits: https://cgit.freedesktop.org/NetworkManager/NetworkManager/log/?qt=grep&anzwix=source&q=wake-on-wlan
* might need to use snap hooks per https://docs.ubuntu.com/core/en/stacks/network/network-manager/docs/reference/snap-configuration/wowlan
* https://wireless.wiki.kernel.org/en/users/documentation/wowlan mentions using `iw`
  * discussed at https://www.cyberciti.biz/faq/configure-wireless-wake-on-lan-for-linux-wifi-wowlan-card/

```
iw list
iw phy0 wowlan show
sudo iw phy phy0 wowlan enable magic-packet
```

#### sleep states / hardware
https://askubuntu.com/questions/920495/wake-on-wireless-lan-on-ubuntu-what-sleep-states-will-it-work-from-and-how-to

https://askubuntu.com/questions/920495/wake-on-wireless-lan-on-ubuntu-what-sleep-states-will-it-work-from-and-how-to

#### See also
* https://forum.snapcraft.io/t/network-manager-broken-for-desktop-ubuntu/12512/19
* https://www.nixpal.com/commands-to-hibernate-linux-machines/
* https://www.kernel.org/doc/Documentation/power/states.txt
* https://www.mjmwired.net/kernel/Documentation/power/states.txt
* https://wiki.archlinux.org/index.php/Power_management/Suspend_and_hibernate

## troubleshooting
Prolly outdated but: https://askubuntu.com/questions/1051822/wake-on-lan-quit-working-with-latest-kernel-bionic
