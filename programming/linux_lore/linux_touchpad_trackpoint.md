


2018-07-20:
* [custom range of touchpad](https://news.ycombinator.com/item?id=17589505)
    * 
* [libinput-gestures example from reddit](https://www.reddit.com/r/unixporn/comments/8zoud9/swaywm_my_workflow_with_workspace_switching_via/e2kp2d4/)
* [The definitive guide to synclient](http://who-t.blogspot.com/2017/01/the-definitive-guide-to-synclient.html)
* [Linux touchpad like a Macbook: goal worth pursuing?](https://news.ycombinator.com/item?id=17547817) - 

## gestures
[How to Re-enable One-Finger Tap and Drag, and Menu Buttons Selection by Keyboard In OS X Yosemite](https://www.guidingtech.com/34353/re-enable-mavericks-features/) - enabled this fancy three finger resize

## 2015-05 link dump
https://www.reddit.com/r/thinkpad/comments/58c0dn/t460s_trackpoint_slow_linux/ | T460s TrackPoint slow (Linux) : thinkpad
https://github.com/ivanbrennan/nixbox/blob/master/configuration.nix#L38 | nixbox/configuration.nix at master · ivanbrennan/nixbox
https://www.reddit.com/r/thinkpad/comments/8hgjbl/t480_trackpoint_on_linux/ | T480 Trackpoint on Linux ... : thinkpad
https://www.reddit.com/r/thinkpad/comments/2iw6hj/so_whats_so_good_about_the_trackpoint/ | So... what's so good about the TrackPoint? : thinkpad
https://howchoo.com/g/mdy0ngziogm/the-perfect-almost-touchpad-settings-on-linux-2 | The Perfect (almost) Touchpad Settings on Linux - howchoo
https://www.reddit.com/r/thinkpad/comments/7ybj09/t470s_on_ubuntulinux_touchpad_very_bad/duffxiw/ | Liskni_si comments on T470s on Ubuntu/linux: touchpad very bad
https://forum.kde.org/viewtopic.php?f=66&t=142726#p383651 | How to disable touchpad completely in KDE ? • KDE Community Forums
https://major.io/2013/08/24/get-a-rock-solid-linux-touchpad-configuration-for-the-lenovo-x1-carbon/ | Get a rock-solid Linux touchpad configuration for the Lenovo X1 Carbon - major.io
https://news.ycombinator.com/item?id=13300695 | It would be so much easier to love my Thinkpad T460s if it wasn't for the touchp... | Hacker News
http://www.notebookreview.com/feature/need-know-touchpads-trackpads/ | What You Need to Know About Touchpads and Trackpads
https://unix.stackexchange.com/questions/131432/which-driver-is-handling-my-touchpad | linux - Which driver is handling my touchpad? - Unix & Linux Stack Exchange
https://unix.stackexchange.com/questions/428975/lenovo-x1-carbon-gen-6-2018-touchpad-and-trackpoint-issues-with-linux | ubuntu - Lenovo X1 Carbon Gen.6 (2018) touchpad and trackpoint issues with linux - Unix & Linux Stack Exchange
https://www.reddit.com/r/archlinux/comments/48tqj9/difference_between_libinput_and_evdev/ | Difference between libinput and evdev? : archlinux
https://github.com/NixOS/nixpkgs/issues/19560 | Synaptics touchpad enabled even though I explicitly configured it not to be · Issue #19560 · NixOS/nixpkgs
https://www.reddit.com/r/i3wm/comments/8jl8jz/hate_your_touchpad_when_in_the_shell_or_emacs/ | Hate your touchpad when in the shell or emacs? Love it when in the browser? : i3wm
https://www.reddit.com/r/kde/comments/8co4ty/how_to_get_touchpad_scrolling_coasting_working/ | How to get touchpad scrolling coasting working? : kde
https://wayland.freedesktop.org/libinput/doc/latest/touchpad_pressure.html#touchpad_pressure_hwdb | libinput: Touchpad pressure-based touch detection
https://github.com/torvalds/linux/blob/master/drivers/input/mouse/trackpoint.c | linux/trackpoint.c at master · torvalds/linux
https://help.ubuntu.com/community/SynapticsTouchpad | SynapticsTouchpad - Community Help Wiki
https://askubuntu.com/questions/776948/configuring-libinput-gestures-for-a-macbook-like-experience | 16.04 - Configuring libinput-gestures for a MacBook like experience - Ask Ubuntu
https://www.maketecheasier.com/add-multitouch-gestures-ubuntu/ | How to Add Mac-Like Multi-Touch Gestures to Ubuntu - Make Tech Easier
