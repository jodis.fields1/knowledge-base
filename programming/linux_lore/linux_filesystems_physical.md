
FreeNAS - ZFS over at https://github.com/freenas

[Device file](https://en.wikipedia.org/wiki/Device_file#Block_devices)

### tools
fdisk - primary tool, see https://wiki.archlinux.org/index.php/Fdisk
    * `fdisk -l` is like diskutil on macOS - use it to work with unmounted drives (partition etc)
    * on macOS also gpt and fdisk apply
lsbk - list blcks
dumpe2fs - bunch of filesystem / GPT stuff, see http://landoflinux.com/linux_dumpe2fs_command.html
dd
hexdump
mount = mount -a mounts everything in /etc/fstab
lshw - maybe?

### tips for memory / storage general
`free -ltm` - general memory; `cat /proc/meminfo`; https://www.cyberciti.biz/faq/linux-check-memory-usage/


### swap
"mount-point – Where the contents of the device may be accessed after mounting; for swap partitions or files, this is set to none" - per https://en.wikipedia.org/wiki/Fstab
`swapon /swapfile` for immediate activation per https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/7/html/storage_administration_guide/ch-swapspace which seems better than but should really cross-reference https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/5/html/deployment_guide/s1-swap-adding

### mounting
[Correct way to mount a hard drive](https://unix.stackexchange.com/questions/72125/correct-way-to-mount-a-hard-drive) - ?
[How to mount an ISO file?](https://askubuntu.com/questions/164227/how-to-mount-an-iso-file), see also [Why has Ubuntu moved the default mount points?](https://askubuntu.com/questions/336518/why-has-ubuntu-moved-the-default-mount-points)

Note: equivalent on OSX is quite different [Mount block file on OSX](https://serverfault.com/questions/174909/mount-block-file-on-osx)

### RAM
initrd -> initramfs

### Recommendations
[Which open file system would make the best unified cross-platform file system for large external storage?](https://www.quora.com/Which-open-file-system-would-make-the-best-unified-cross-platform-file-system-for-large-external-storage)

[File system for external hard drive: interoperable and open?](https://askubuntu.com/questions/895457/file-system-for-external-hard-drive-interoperable-and-open) - recommends exFAT which is proprietary but handled by FUSE, also see [Why should I use exFAT over NTFS on removable media?](https://superuser.com/questions/257646/why-should-i-use-exfat-over-ntfs-on-removable-media/262304#262304)

### bootloader
[Where to find the source code for a PC MBR Bootloader?](https://stackoverflow.com/questions/1278999/where-to-find-the-source-code-for-a-pc-mbr-bootloader) - `dd if=/dev/sda bs=512 count=1 | hexdump -C` just shows compiled code

#### underlying theory
https://en.wikipedia.org/wiki/Design_of_the_FAT_file_system
https://en.wikipedia.org/wiki/Disk_sector
https://en.wikipedia.org/wiki/Solid-state_drive