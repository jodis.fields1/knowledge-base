
## kinetic scrolling
libinput requires kinetic scrolling be implemented by apps, but synaptics supports it out of the box

### directly related
https://wayland.freedesktop.org/libinput/doc/latest/faqs.html#kinetic-scrolling-does-not-work | FAQs - Frequently Asked Questions — libinput 1.11.903 documentation
https://forum.manjaro.org/t/how-to-enable-kinetic-scrolling-with-libinput-driver/36488/8 | How to enable Kinetic scrolling with libinput driver? - Technical Issues and Assistance / Drivers - Manjaro Linux Forum
https://www.reddit.com/r/kde/comments/426lys/kinetic_scrolling_with_libinput/ | Kinetic scrolling with libinput? : kde
https://bugzilla.mozilla.org/show_bug.cgi?id=1213601 | 1213601 - Kinetic scrolling does not work
https://bugs.kde.org/show_bug.cgi?id=76082 | 76082 – smooth scrolling in all apps
https://www.google.com/search?hl=en&ei=RfWCW6HLF8mL0gL3wY6oCA&q=chromium+kinetic+scrolling+libinput&oq=chromium+kinetic+scrolling+libinput&gs_l=psy-ab.3..33i22i29i30k1.6750.26563.0.27068.74.46.18.10.17.0.140.4375.26j19.45.0....0...1c.1.64.psy-ab..2.70.4157...0j0i131k1j0i67k1j0i22i30k1j0i22i10i30k1j0i131i67k1j0i13k1j0i13i30k1j33i160k1j0i8i13i30k1j33i21k1.0.URYblK8R-uM | chromium kinetic scrolling libinput - Google Search
https://bugs.chromium.org/p/chromium/issues/detail?id=384970 | 384970 - Linux: Please support XInput 2.1 true smooth/high-resolution scrolling - chromium - Monorail
https://bugs.chromium.org/p/chromium/issues/detail?id=763791 | 763791 - Please support libinput kinetic scrolling. - chromium - Monorail

### loosely related
#### general
https://pavelfatin.com/scrolling-with-pleasure/ | Scrolling with pleasure
https://github.com/ariya/kinetic | ariya/kinetic: Kinetic Scrolling with JavaScript
https://en.wikipedia.org/wiki/Scrolling#Computing | Scrolling - Wikipedia
https://stackoverflow.com/questions/1810742/algorithm-to-implement-kinetic-scrolling | language agnostic - Algorithm to implement kinetic scrolling - Stack Overflow
https://ux.stackexchange.com/questions/14238/whats-the-appropriate-scrolling-deceleration-behavior | touch screen - What's the appropriate scrolling deceleration behavior? - User Experience Stack Exchange
https://dzone.com/articles/javascript-kinetic-scrolling-0 | JavaScript Kinetic Scrolling: Part 2 - DZone Java
https://askubuntu.com/questions/849714/libinput-scrolling-behaviour-config | touchpad - Libinput scrolling behaviour config - Ask Ubuntu
