list packages explicitly installed:
requested for DNF at https://bugzilla.redhat.com/show_bug.cgi?id=1473962

### Debian-based
https://askubuntu.com/questions/40011/how-to-let-dpkg-i-install-dependencies-for-me/795048#795048 - you can install `apt install *.deb` files

#### Ubuntu backport
[UbuntuBackports](https://help.ubuntu.com/community/UbuntuBackports) - see about requesting new backports

#### RPM
[Chapter 25. RPM Resources](https://docs.fedoraproject.org/en-US/Fedora_Draft_Documentation/0.1/html/RPM_Guide/ch-online-resources.html)

#### Metapackages
Note that metapackages are not entirely removed per http://www.reddit.com/r/Ubuntu/comments/oidrl/just_switched_from_ubuntu_to_kubuntu/c3hsqfb however you can use  aptitude (NOT)  

#### Overview
http://www.infodrom.org/Debian/doc/maint/Maintenance-pkgmaint.html is a good overview of debian's package management
Seems like Arch may be able to remove metapackages per pacman -Rsc will "remove a package, its dependencies and all the packages that depend on the target package" at  How do I fully remove the kde meta package?
Seems like editing APT::Never-MarkAuto-Sections per  How to ensure full package install after removing a meta-package? could work
How to determine if a package is a meta-package from the command line? - basically no certain way! grr