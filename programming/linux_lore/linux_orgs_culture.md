# Linux orgs and culture

TODO: https://en.wikipedia.org/wiki/Linuxfest_Northwest

**Funding**
See https://www.bountysource.com/

**People**
http://geekfeminism.wikia.com/wiki/List_of_women_in_FLOSS - interesting list

**Organizations and culture**
Europe's largest hacker organization is the Chaos Computer Club ( [wiki](http://en.wikipedia.org/wiki/Chaos_Computer_Club "http://en.wikipedia.org/wiki/Chaos_Computer_Club")) and from there I found out about American orgs such as the Cult of the Dead Cow ( [wiki](http://en.wikipedia.org/wiki/Cult_of_the_Dead_Cow "http://en.wikipedia.org/wiki/Cult_of_the_Dead_Cow"), started 1984, declared war on Scientology in 90s, progenitor of Anonymous?) and the HOPE ( [wiki](http://en.wikipedia.org/wiki/Hackers_on_Planet_Earth "http://en.wikipedia.org/wiki/Hackers_on_Planet_Earth")) conference (Hackers on Planet Earth) run by 2600: The Hacker Quarterly

Keir Thomas is a notable non-programmer watching the community;  http://www.pcworld.com/article/162457/linux_needs_critics.html

## Open-source culture

Learned about http://en.wikipedia.org/wiki/Bus_factor which is the number of individual developers who would need to be killed for the project to end

Rant on Linux and the constructivism in the OLPC project http://radian.org/notebook/sic-transit-gloria-laptopi - awesome

[Producing Open Source Software](http://producingoss.com/en/index.html "http://producingoss.com/en/index.html") - scholarly book which I read 2014-11

_Unix Philosophy and criticism_

See http://en.wikipedia.org/wiki/Unix_philosophy; got into the criticism of it and noticed for example http://en.wikipedia.org/wiki/The_Unix-Haters_Handbook and a discussion at http://news.ycombinator.com/item?id=3959959 about the benefits/costs of the Unix philosophy; also books such as Gancarz The Unix Philosophy or his later book Linux and the Unix Philosophy; stereotypical Linux guy https://twitter.com/1990slinuxuser

contemporary 2010 rant at http://www.mail-archive.com/mutt-users@mutt.org/msg40603.html ("Re: Unix Philosophy (was List management headers)") with a rant by Derek Martin about quick rejection of new features; my concern here was motivated by htop duplicating top when it could've been added to it, and too many packages

First article by John Rose on LispM (Lisp Machines) was amazing; see http://news.ycombinator.com/item?id=3696345 for a similar comment ("Reading the rant from John Rose in the preface to THE UNIX-HATERS Handbook [1] was pretty eye-opening"); homepage seems to be http://www.mindspring.com/~blackhart/

http://linuxfinances.info/info/unixhaters.html is too long

  


Usability criticism (modern)

In addition to the old Unix-Haters handbook, see http://news.ycombinator.com/item?id=2643671 based on http://batsov.com/articles/2011/06/11/linux-desktop-experience-killing-linux-on-the-desktop/ and http://www.freerepublic.com/focus/f-chat/2915176/posts Is usability breaking Linux adoption?

  


_Specific nondistro organizations_

Linux Foundation - runs the Filesystem Hierarchy Standard and employs Linus Torvalds; mainly corporate-funded; also runs linux.com; has a lot of money with $9 million in 2010 or so; individual membership for $99 is available which gives the right to vote and run for the board; 2 individual member reps right now; little disappointed but per http://www.itworld.com/it-managementstrategy/237829/linux-foundation-sites-almost-all-back-action they dropped the Linux Developer Network in 2011; see http://www.linuxfoundation.org/about/individual-affiliate-election for the election procedures

  


Software in the Public Interest (SPI) - runs Debian; pretty weak and loosely-organized Should Ubuntu Have Been Created?(http://www.spi-inc.org/; http://keithcu.com/wordpress/?page_id=558) discusses the effect; Mark Shuttleworth explains his reasoning over at http://raphaelhertzog.com/2011/11/17/people-behind-debian-mark-shuttleworth-ubuntus-founder/; http://larrythefreesoftwareguy.wordpress.com/2012/05/04/marks-reality-distortion-field/ is a disillusioned comment; Debian is actually pretty weak, see http://wiki.debian.org/PeopleBehindDebian which has only about 50 people much less than KDE; note that it's not as hard as I thought to get involved debugging their website (see footer for links) altho it seems to be a bit dead

> DebConf13 - I watched  [a lot of videos](http://meetings-archive.debian.net/pub/debian-meetings/2013/debconf13/archival/ "http://meetings-archive.debian.net/pub/debian-meetings/2013/debconf13/archival/") but particularly liked How other FLOSS communities mentors by Asheesh Laroia

  


Blue Systems - sponsors Kubuntu; ran by a rich German kid

  


http://linuxfund.org - provides funding through credit cards and donations

  


GNOME Foundation - 

2014-11 update:  [desktop-devel-list](https://mail.gnome.org/mailman/listinfo/desktop-devel-list "https://mail.gnome.org/mailman/listinfo/desktop-devel-list") seems to be the main spot, noticed someone giving up maintaining which raised the discussion of they are keeping track and  [Andre Klapper said, candidly, that they are not](https://mail.gnome.org/archives/desktop-devel-list/2014-September/msg00113.html "https://mail.gnome.org/archives/desktop-devel-list/2014-September/msg00113.html"), meanwhile  [someone tried to step up for gnome-panel](https://mail.gnome.org/archives/desktop-devel-list/2014-September/msg00066.html "https://mail.gnome.org/archives/desktop-devel-list/2014-September/msg00066.html") and got rejected coldly!

can see a little more into their internal dealings at http://vote.gnome.org/ and scroll down at http://www.gnome.org/foundation/ to see all sorts of other information; from http://www.gnome.org/foundation/governance/ you find reports; note that http://blogs.gnome.org/otte/2012/07/27/staring-into-the-abyss/ by Benjamin Otte paints a dim picture for GNOME; developers responded to GNOME 3 criticism at http://www.datamation.com/open-source/gnome-answers-criticisms-1.html; note Linus also criticized it http://www.zdnet.com/blog/open-source/linus-torvalds-would-like-to-see-a-gnome-fork/9347 which is also summarized in this somewhat epic article Is the Linux desktop becoming extinct? (2013)

GNOME has transparent voting in the minutes; http://stormyscorner.com/2011/08/transparent-voting-why-i-like-the-idea-even-though-i-think-it-would-be-useless.html the ED commented somewhat negatively on the idea a while back

  


_Distros_ \- obviously there's too many; see distrowatch; noticed this article Should Ubuntu Have Been Created?(http://keithcu.com/wordpress/?page_id=558) from 2005 which notes a bit of the problem

  


Arch Linux - 

2014-11: looked at some of the 35 suggestions for AUR, nobody seems to be working on them;  [this igurublog guy](http://igurublog.wordpress.com/2011/02/19/archs-dirty-little-notso-secret/ "http://igurublog.wordpress.com/2011/02/19/archs-dirty-little-notso-secret/") doesn't like the leadersip, Aaron Griffin (phrakture) is the leader and spends most of his time on r/fitness it looks like

[arch-dev-public](https://lists.archlinux.org/pipermail/arch-dev-public/ "https://lists.archlinux.org/pipermail/arch-dev-public/") is the main place they talk

[pkgstats](https://wiki.archlinux.org/index.php/pkgstats "https://wiki.archlinux.org/index.php/pkgstats") pushes stats which can be  [viewed at archlinux.de](https://www.archlinux.de/?page=Statistics "https://www.archlinux.de/?page=Statistics") and are pretty interesting

interesting: the  [/usr merge discussion Dan McGee spoke out against kernel modules](https://lists.archlinux.org/pipermail/arch-dev-public/2012-March/022641.html "https://lists.archlinux.org/pipermail/arch-dev-public/2012-March/022641.html") in /usr/lib for philosophical reasons altho he did it in an irate manner

Chakra is the KDE spinoff; co-founder Jan Mette died from myocarditis in 2010 http://chakra-project.org/bbs/viewtopic.php?id=2612&p=3 and reading the wiki article it causes up to 20% of sudden death in young people per http://annals.org/article.aspx?articleid=717984 Sudden death in young adults: a 25-year review - 86% related to exercise

  


Ubuntu - has the Unity desktop (which I  hated, but some journalists are now saying it can help you focus by limiting the number of windows); trying to commercialize

  


Linux Mint - frustrated by its GNOME focus; has no wiki but a bunch of tutorials, many of which have minor errors due to changes over time

  


Debian - see  [Debian mailing list statistics](https://lists.debian.org/stats/ "https://lists.debian.org/stats/") for membership to their various lists, debian-devel seems to be main conversation place

  


**KDE**  - see separate KDE note

  


_Features/Issues_

One issue is that it doesn't tell at first if you have two monitors; configuration can be tricky; for some reason Kmenuedit, which necessary for shortcuts, is hidden from the System Settings and has to be launched in konsole; see http://superuser.com/questions/6791/keyboard-shortcuts-in-kde-4-x

  


_Adoption_

http://en.wikipedia.org/wiki/Linux_adoption#Measuring_desktop_adoption

Linux Mint chief not carried away by success (2011) - decent little article on Clement; he deserves it; discusses adoption statistics

Linux Foundation apparently studies this; see http://www.eweek.com/c/a/Linux-and-Open-Source/Survey-Linux-Adoption-Continues-to-Grow-at-the-Expense-of-Windows-664575/

using user-agent strings is interesting and I guess W3Counter does his; however, using this for the distros can be problematic see http://doctormo.org/2011/10/20/ubuntus-adoption-curve-past-and-present/

http://askubuntu.com/questions/46657/show-all-open-windows-in-11-04ksd

  


_Licensing_

I read [LibreSSL, OpenSSL, collisions and problems](https://blog.flameeyes.eu/2015/06/libressl-openssl-collisions-and-problems) and the [peculiar libretunnel situation](http://www.tedunangst.com/flak/post/the-peculiar-libretunnel-situation) (2015); note that LibreSSL does not support FIPS
