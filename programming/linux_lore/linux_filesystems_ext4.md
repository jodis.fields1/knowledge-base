
### overview
[An introduction to Linux's EXT4 filesystem](https://opensource.com/article/17/5/introduction-ext4-filesystem) - really good overview

[LinuxFilesystemsExplained](https://web.archive.org/web/20170310030201/https://help.ubuntu.com/community/LinuxFilesystemsExplained) - learned about editing running files


#### ext4
[Ext4 Disk Layout](https://ext4.wiki.kernel.org/index.php/Ext4_Disk_Layout) - disk layout!

[e2fsprogs Definition](http://www.linfo.org/e2fsprogs.html)