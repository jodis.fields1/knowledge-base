TODO: [How to set a default desktop environment at system start?](https://superuser.com/questions/685970/how-to-set-a-default-desktop-environment-at-system-start)
    * answer how to find out what config file you're using with systemd

See cheat systemctl

systemctl status PATTERN (e.g. units)
systemctl show PATTERN (e.g. units)

systemctl mask / unmask better than enable / disable

systemd analyze critical-chain per [Is there a way to see the execution tree of systemd?](https://serverfault.com/questions/617398/is-there-a-way-to-see-the-execution-tree-of-systemd)

## alternatives
[Escape from System D, episode V](https://davmac.wordpress.com/2018/06/21/escape-from-system-d-episode-v/) - Dinit
    * mentions that "rust memory allocation failure cause termination" is dealbreaker

## 2018 desktop return
https://unix.stackexchange.com/questions/288751/why-is-a-link-removed-when-disabling-a-service-a-file-is-not | systemd - Why is a link removed when disabling a service? (a file is not) - Unix & Linux Stack Exchange
https://www.digitalocean.com/community/tutorials/how-to-use-systemctl-to-manage-systemd-services-and-units | How To Use Systemctl to Manage Systemd Services and Units | DigitalOcean
https://www.digitalocean.com/community/tutorials/understanding-systemd-units-and-unit-files | Understanding Systemd Units and Unit Files | DigitalOcean

## old
https://unix.stackexchange.com/questions/158494/the-name-org-freedesktop-policykit1-was-not-provided-by-any-service-files | centos - The name > org.freedesktop.PolicyKit1 was not provided by any .service files - Unix & Linux Stack Exchange
https://unix.stackexchange.com/questions/363016/how-to-upgrade-my-systemd-to-latest-version | debian - How to upgrade my systemd to latest version? - Unix & Linux Stack Exchange

https://suckless.org/sucks/systemd | sucks - systemd | suckless.org software that sucks less

### hacking / internals
https://kinvolk.io/blog/2015/12/testing-systemd-patches/ | Testing systemd Patches | Kinvolk


### ubuntu DNSSEC resolver
dear lord! broke networks for ubuntu 17.04
https://bugs.launchpad.net/ubuntu/+source/systemd/+bug/1650877 | Bug #1650877 “systemd-resolved: resolve call failed: DNSSEC vali...” : Bugs : systemd package : Ubuntu
https://bugs.launchpad.net/ubuntu/+source/systemd/+bug/1682499 | Bug #1682499 “Disable DNSSEC by default” : Bugs : systemd package : Ubuntu

