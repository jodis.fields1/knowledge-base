# networking
Check out Chris McKenzie's [list of cheatsheets](http://getpostdelete.com/LinuxKeyboardShortcuts.html)

instead of cut use cuts per [http://stackoverflow.com/a/24543432/4200039](http://stackoverflow.com/a/24543432/4200039)

[https://github.com/yuvadm/pyp](https://github.com/yuvadm/pyp) - the Pyed Piper

[Node Input/output Piper](https://github.com/kolodny/nip) - nip is cool

Use nip instead [https://github.com/kolodny/nip](https://github.com/kolodny/nip)

## 2018-05 update on docs and manpages
manned.org was the original documented open-source manpage browser
https://jlk.fjfi.cvut.cz/arch/manpages/ (at https://github.com/lahwaacz/archweb_manpages)

## The struggle
[The Longest Debugging—The journey towards a reliable Linux workstation](http://fortintam.com/blog/2018/02/25/journey-towards-a-reliable-linux-workstation/)
* a member of the Pur.ism team

## Setup
[Build yourself a Linux](https://news.ycombinator.com/item?id=14264189)
https://news.ycombinator.com/item?id=14264189
[Helpful Linux I/O stack diagram](https://major.io/2014/04/30/helpful-linux-io-stack-diagram/) (2014) - don't really understand it but hmm

## systemd / containers / docker
[Docker without Docker](https://news.ycombinator.com/item?id=9442254)
[Linux Containers Internals (Part I)](https://rabbitstack.github.io/operating%20systems/linux-containers-internals-part-i/)

## architecture overview
[https://github.com/0xAX/linux-insides](https://github.com/0xAX/linux-insides)

## logging
https://www.loggly.com/ultimate-guide/using-journalctl/
two major alternatives to journald:
advice at https://news.ycombinator.com/item?id=14209168

syslog-ng - on Github, corporate sponsor

rsyslog  - on Github, pure open-source

## Processes and ports
### lsof
[Linux super-duper admin tools: lsof](http://www.dedoimedo.com/computers/lsof.html) is good, also see [An lsof Primer](https://danielmiessler.com/study/lsof/); use sudo lsof -iTCP -sTCP:LISTEN -P -n to find listening daemons [per SE post](http://unix.stackexchange.com/questions/26887/lsof-and-listening-ports). also used myid=$(pgrep mysql) && lsof -i -a -P +p $myid

top and htop and iftop and [vtop](https://github.com/MrRio/vtop/issues/35) (nodejs) - 

type man hier - https://en.wikipedia.org/wiki/Unix_filesystem#Conventional_directory_layout

[Arch hierarchy](https://wiki.archlinux.org/index.php/Arch_filesystem_hierarchy) - explicitly discusses their approach 

*/Var/run->/run* - another change to continue to make things confusing,   [explained here](http://askubuntu.com/questions/57297/why-has-var-run-been-migrated-to-run)