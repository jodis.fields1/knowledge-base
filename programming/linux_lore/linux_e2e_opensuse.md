sadly, altho SUSE leads with openQA, not doing too well... video in below stats showed a steady decline

## 2018-07-01
https://build.opensuse.org/package/view_file/Apache/apache2/apache2.spec?expand=1 | File apache2.spec of Package apache2 - openSUSE Build Service
https://en.opensuse.org/openSUSE:Build_Service_Tutorial | openSUSE:Build Service Tutorial - openSUSE

### why not and growth
https://en.opensuse.org/openSUSE:Statistics | openSUSE:Statistics - openSUSE
https://twitter.com/BryanLunduke/status/996518061204127744 | Bryan Lunduke on Twitter: "Why I don't use @openSUSE... many reasons. Some having to do with my lack of confidence in the parent company. Don't read that to mean openSUSE is bad, mind you. It has *many* good points. Also the closure of SUSE Studio removed what was, in my opinion, the greatest strength.… https://t.co/kZPWww8xL2"

### reviews
https://distrowatch.com/weekly.php?issue=20180604 | DistroWatch.com: Put the fun back into computing. Use Linux, BSD.
https://www.reddit.com/r/linux/comments/6zz9ve/gnome_326_is_already_checked_in_to_opensuse/dmzfi4g/ | rbrownsuse comments on GNOME 3.26 is already checked in to openSUSE Tumbleweed

### hardware issues
https://forums.opensuse.org/showthread.php/531508-Installer-touchpad-and-trackpoint-not-working-during-install-(ThinkPad-450s)?highlight=touchpad | LEAP 15 Installer: touchpad and trackpoint not working during install (ThinkPad 450s)
https://forums.opensuse.org/showthread.php/530676-synaptics-touchpad-stopped-working-with-kernel-4-16-1?highlight=touchpad | TUMBLEWEED synaptics touchpad stopped working with kernel 4.16.1
