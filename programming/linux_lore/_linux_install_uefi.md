## uefi
2018-10: went down the rabbit hole when ubuntu install to usb broke my nixos boot...
https://unix.stackexchange.com/questions/160500/how-do-multiple-boot-loaders-work-on-an-efi-system-partition | grub - How do multiple boot loaders work on an EFI system partition - Unix & Linux Stack Exchange

## practical
https://askubuntu.com/questions/330541/what-is-vmlinuz-efi

https://unix.stackexchange.com/questions/52996/how-to-boot-efi-kernel-using-qemu-kvm | ubuntu - How to boot EFI kernel using QEMU (kvm)? - Unix & Linux Stack Exchange

https://unix.stackexchange.com/questions/tagged/uefi?page=2&sort=votes&pagesize=15 | Highest Voted 'uefi' Questions - Page 2 - Unix & Linux Stack Exchange

https://unix.stackexchange.com/questions/83669/how-to-recreate-efi-boot-partition | linux - How to recreate EFI boot partition? - Unix & Linux Stack Exchange

## fundamentals
https://wiki.osdev.org/UEFI | UEFI - OSDev Wiki
http://x86asm.net/articles/introduction-to-uefi/index.html | Introduction to UEFI
https://en.m.wikipedia.org/wiki/Bytecode | Bytecode - Wikipedia
http://vzimmer.blogspot.com/2015/08/efi-byte-code.html?m=1 | Vincent Zimmer's blog: EFI Byte Code
https://www.happyassassin.net/2014/01/25/uefi-boot-how-does-that-actually-work-then/ | UEFI boot: how does that actually work, then?
https://cs.dartmouth.edu/~bx/blog/resources/uefi.html
https://web.archive.org/web/20160101013007/http://homepage.ntlworld.com/jonathan.deboynepollard/FGA/efi-boot-process.html | FGA: The EFI boot process.
