I think NixOS makes this irrelevant?

## sd card / usb drive
https://askubuntu.com/questions/451364/how-to-enable-exfat-in-ubuntu-14-04 | partitioning - How to enable exFAT in Ubuntu 14.04 - Ask Ubuntu
https://unix.stackexchange.com/questions/321494/ubuntu-16-04-package-exfat-utils-is-not-available-but-is-referred-to-by-anothe | sd card - ubuntu 16.04 "Package exfat-utils is not available, but is referred to by another package." - Unix & Linux Stack Exchange

## 2018-05
### practical
https://askubuntu.com/questions/32499/migrate-from-a-virtual-machine-vm-to-a-physical-system/32506#32506 | virtualization - Migrate from a virtual machine (VM) to a physical system - Ask Ubuntu

https://serverfault.com/questions/33858/is-it-possible-to-convert-a-vmware-vmdk-image-file-to-physical-hardisk-drive/297335
### image files
https://en.wikipedia.org/wiki/Disk_image | Disk image - Wikipedia
https://www.turnkeylinux.org/blog/convert-vm-iso | Converting a virtual disk image: VDI or VMDK to an ISO you can distribute | TurnKey GNU/Linux Blog

https://askubuntu.com/questions/85741/how-do-i-mount-edit-and-repack-an-img-file | 11.10 - How do I mount, edit and repack an .img file? - Ask Ubuntu
https://stackoverflow.com/questions/13209258/how-to-view-img-files | image - How to view .img files? - Stack Overflow
https://askubuntu.com/questions/134124/why-do-i-get-no-root-file-system-is-defined-when-i-try-install-in-one-partitio | Why do I get "No root file system is defined" when I try install in one partition? - Ask Ubuntu

https://www.turnkeylinux.org/blog/convert-vm-iso?page=6 | Converting a virtual disk image: VDI or VMDK to an ISO you can distribute | TurnKey GNU/Linux Blog
https://buyvm.net/beware-the-moshbear/ | BuyVM - Community Chat
https://client02.chat.mibbit.com/?channel=%23Frantech&server=irc.dairc.net | Mibbit.com Webchat client
https://cloud.google.com/compute/docs/images/building-custom-os | Building Custom Operating Systems  |  Compute Engine Documentation  |  Google Cloud
https://github.com/turnkeylinux/tracker/issues | Issues · turnkeylinux/tracker
https://buyvm.net/features/ | BuyVM - Features
https://askubuntu.com/questions/1004045/apt-get-update-fails-on-live-usb-session | apt-get update fails on live USB session - Ask Ubuntu
https://www.google.com/search?hl=en&q=GRUB%20view%20img | GRUB view img - Google Search

https://askubuntu.com/questions/196125/how-can-i-resize-an-lvm-partition-i-e-physical-volume#comment1605094_196134 | partitioning - How can I resize an LVM partition? (i.e: physical volume) - Ask Ubuntu
