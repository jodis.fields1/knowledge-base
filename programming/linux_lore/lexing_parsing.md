# lexing / parsing / compiling
See common_software_fundamentals.md

## tutorials
[Understanding Compilers — For Humans](https://medium.com/@thelukaswils/understanding-compilers-for-humans-ba970e045877)
* https://www.reddit.com/r/programming/comments/8q431m/i_wrote_a_new_7_minute_article_teaching_the/

[How to implement a programming language in JavaScript](https://web.archive.org/web/20190716080127/http://lisperator.net/pltut/) - reviewed 2019-08-19

[Leveling Up One’s Parsing Game With ASTs](https://medium.com/basecs/leveling-up-ones-parsing-game-with-asts-d7a6fc2400ff)
* https://stackoverflow.com/questions/50940142/lexeme-vs-token-terminology

TODO: https://stackoverflow.com/questions/6216449/where-can-i-learn-the-basics-of-writing-a-lexer

[Introducing Flex and Bison by John Levine](https://www.safaribooksonline.com/library/view/flex-bison/9780596805418/ch01.html) introduces flex, a lexer ("scanner"), and bison, a parser.

[Rich Programmer Food](https://steve-yegge.blogspot.ca/2007/06/rich-programmer-food.html)
> The first big phase of the compilation pipeline is parsing. You need to take your input and turn it into a tree. So you go through preprocessing, lexical analysis (aka tokenization), and then syntax analysis and IR generation. Lexical analysis is usually done with regexps. Syntax analysis is usually done with grammars. You can use recursive descent (most common), or a parser generator (common for smaller languages), or with fancier algorithms that are correspondingly slower to execute. But the output of this pipeline stage is usually a parse tree of some sort.

## old
Also motivated by UglifyJS discussion, and see:

**Abstract syntax tree (AST)** - jslint and jshint have their own parsers, apparently built out of the SpiderMonkey implementation. Esprima is a major one with potential issues; Shift AST is supposed to be better - found thru ESLint's [Introducing Espree, an Esprima alternative](http://eslint.org/blog/2014/12/espree-esprima/)

https://www.blackbytes.info/2015/08/static-analysis-in-ruby/

* [UglifyJS — why not switching to SpiderMonkey AST](http://lisperator.net/blog/uglifyjs-why-not-switching-to-spidermonkey-ast/)
* [JavaScript myth: JavaScript needs a standard bytecode](http://www.2ality.com/2012/01/bytecode-myth.html)
* Ruby/Rails post