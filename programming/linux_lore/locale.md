
**Locale** - do not understand this

[setlocale() always returns "C"](https://bytes.com/topic/c/answers/828621-setlocale-always-returns-c) - same for the PHP version

[The Open Group Base Specifications Issue 7 IEEE Std 1003.1, 2013 Edition](http://pubs.opengroup.org/onlinepubs/9699919799/) - Chapter 7 covers Locale and there's a reference to the function too