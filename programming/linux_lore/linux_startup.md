
[How to switch between tty and xorg session](https://unix.stackexchange.com/questions/167386/how-to-switch-between-tty-and-xorg-session) - CTRL+ALT+F1-12 (F7 is typically the graphical session)

## set environment variables on startup
https://askubuntu.com/questions/65563/how-do-i-set-an-environment-variable-at-boot-time-via-a-script - "specify environment variables as kernel parameters"

https://unix.stackexchange.com/questions/354291/how-to-set-environment-variables-at-startup-in-archlinux | arch linux - How to set environment variables at startup in Archlinux - Unix & Linux Stack Exchange
https://unix.stackexchange.com/questions/44370/how-to-make-unix-service-see-environment-variables | init script - How to make unix service see environment variables? - Unix & Linux Stack Exchange
https://medium.com/@benmorel/creating-a-linux-service-with-systemd-611b5c8b91d6 | Creating a Linux service with systemd – Benjamin Morel – Medium
https://unix.stackexchange.com/questions/354291/how-to-set-environment-variables-at-startup-in-archlinux?noredirect=1&lq=1 | arch linux - How to set environment variables at startup in Archlinux - Unix & Linux Stack Exchange
https://wiki.archlinux.org/index.php/environment_variables | Environment variables - ArchWiki
https://askubuntu.com/questions/940679/pass-environment-variables-to-services-started-with-systemctl | systemd - pass environment variables to services started with systemctl - Ask Ubuntu
https://help.ubuntu.com/community/EnvironmentVariables | EnvironmentVariables - Community Help Wiki
https://superuser.com/questions/112235/setting-environment-variables-in-linux-for-all-processes | unix - Setting environment variables in Linux for all processes - Super User


## display manager / xinit
TODO: SDDM - document Xsetup versus Xsession
    * https://wiki.archlinux.org/index.php/Xprofile - SDDM sources /usr/share/sddm/scripts/Xsession
    * [Arch boot process](https://wiki.archlinux.org/index.php/Arch_boot_process) -

## prevent display manager (boot to shell)
https://superuser.com/questions/548646/dont-start-display-manager-when-booting#comment1915902_548653
    * note - if you map to boot you'll end up in initramfs

also: https://askubuntu.com/questions/19486/how-do-i-add-a-kernel-boot-parameter

## overview
https://en.wikipedia.org/wiki/Linux_startup_process
BIOS->
Grub-> (left shift to get into)
kernel-> tries a few hardcoded files
    !try_to_run_init_process("/sbin/init")
    https://wiki.archlinux.org/index.php/Init
    http://www.yolinux.com/TUTORIALS/LinuxTutorialInitProcess.html
Systemd-> (process id 1?)
Display Manager->
    Ex: lightdm
    1. /usr/share/lightdm/<systemd units>

debugging:
    