Linux networking
===

https://wiki.linuxfoundation.org/networking/start

[OpenSnitch](https://news.ycombinator.com/item?id=14245270)


### Resources
[Linux Advanced Routing & Traffic Control HOWTO](http://lartc.org/howto/index.html)
[Understanding Linux Network Internals: Guided Tour to Networking on Linux](https://www.amazon.com/gp/product/0596002556/) (2006) - review says it is way outdated

[x86 Linux Networking System Calls: Socketcall](http://jkukunas.blogspot.com/2010/05/x86-linux-networking-system-calls.html) - as low level as you can be

[A Guide to the Implementation and Modification of the Linux Protocol Stack](http://www.cs.unh.edu/cnrg/people/gherrin/linux-net.html) by Glenn Herrin (2000) - 

[Programming Linux sockets, Part 1: Using TCP/IP](http://www.ibm.com/developerworks/linux/tutorials/l-sock/) (2003) - lots of good resources at bottom

[Five pitfalls of Linux sockets programming](http://www.ibm.com/developerworks/library/l-sockpit/) -

[Beej's Guide to Network Programming](http://beej.us/guide/bgnet/output/html/singlepage/bgnet.html) - 

## connecting
nmcli - https://nullr0ute.com/2016/09/connect-to-a-wireless-network-using-command-line-nmcli/
nmtui per https://askubuntu.com/questions/896140/how-to-connect-to-wifi-network-with-i3wm - did not work once when I tried

## IPv6
turns out it's more of a pain in the ass than I'd expect
[Things you didn’t known about IPv6 link-local address](https://blogs.gentoo.org/eva/2010/12/17/things-you-didnt-known-about-ipv6-link-local-address/)

## utilities

## iproute2
iproute2 - https://kernel.googlesource.com/pub/scm/linux/kernel/git/shemminger/iproute2/, differences explained [here](https://web.archive.org/save/https://lists.fedoraproject.org/archives/list/devel@lists.fedoraproject.org/message/JWVHJ3MLR4O7IQKY7I2JJCBDQ5NZ23VX/), notably uses netlink rather than ioctl

## ifconfig
ifconfig - https://sourceforge.net/p/net-tools/code/ci/master/tree/ plus Github https://github.com/ecki/net-tools

## ifconfig deprecation
ifconfig is replaced by iproute2 (ip link, ip addr, etc); see  [A new years resolution: Stop using deprecated CLI utilities](https://bbs.archlinux.org/viewtopic.php?id=155770) with much more detail at  [Deprecated Linux networking commands and their replacements](http://dougvitale.wordpress.com/2011/12/21/deprecated-linux-networking-commands-and-their-replacements/)
also https://wiki.debian.org/NetToolsDeprecation


#### iproute2
https://kamalmarhubi.com/blog/2015/06/11/ifconfig-how-does-it-even/

#### iptables
see [serversforhackers excellent introduction](https://serversforhackers.com/video/firewalls-basics-of-iptables)
figure out iptables: https://jvns.ca/blog/2017/06/07/iptables-basics/ and https://www.frozentux.net/iptables-tutorial/iptables-tutorial.html