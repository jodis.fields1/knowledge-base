## link dump

https://www.linux.com/news/udev-introduction-device-management-modern-linux-system | Udev: Introduction to Device Management In Modern Linux System | Linux.com | The source for Linux information
https://www.kernel.org/doc/html/v4.14/input/input.html | 1. Introduction — The Linux Kernel documentation

### RMI
https://patchwork.kernel.org/patch/10584495/ | Input: synaptics - enable RMI on ThinkPad T560 - Patchwork
https://www.reddit.com/r/linux/comments/58l6tq/does_microsofts_precision_touchpad_system_just/ | Does Microsoft's Precision Touchpad system just use RMI4 on the backend? : linux

### synaptics
https://www.phoronix.com/scan.php?page=news_item&px=linux-4.6-input-updates | Linux 4.6 Adding Synaptics RMI4 To Better Handle Touchscreens, Touchpads - Phoronix
https://patchwork.kernel.org/patch/10512751/ | Input: synaptics - Lenovo ThinkPad T25 and T480 devices should use RMI - Patchwork

### elan
https://askubuntu.com/questions/763584/elantech-touchpad-not-working-on-ubuntu-16-04-and-arch-linux | asus - Elantech Touchpad not working on Ubuntu 16.04 and Arch Linux - Ask Ubuntu
https://www.kernel.org/doc/html/v4.16/input/devices/elantech.html | 9. Elantech Touchpad Driver — The Linux Kernel documentation
http://www.emc.com.tw/eng/st_tpn_sp.asp | ELAN Microelectronics Corp.
https://patchwork.kernel.org/patch/10638705/ | [5/5] Input: elantech/SMBus - export all capabilities from the PS/2 node - Patchwork
https://patchwork.kernel.org/patch/10589985/ | elan_i2c trackpad on T480s not reporting as clickpad - Patchwork

### microsoft precision
https://bugs.freedesktop.org/show_bug.cgi?id=106716 | 106716 – Palm detection on Precision Touchpads causes spurious taps
https://www.reddit.com/r/archlinux/comments/6432la/increase_the_precision_of_my_precision_touchpad/ | Increase the precision of my Precision touchpad in Linux : archlinux
https://www.reddit.com/r/thinkpad/comments/7azz4p/making_t470s_touchpad_better_under_linux_with/ | Making T470(s) TOUCHpad better under Linux with libinput : thinkpad
https://patchwork.kernel.org/patch/10523439/ | [v4,10/12] HID: multitouch: report MT_TOOL_PALM for non-confident touches - Patchwork
https://patchwork.kernel.org/patch/10523451/ | [v4,04/12] HID: multitouch: store a per application quirks value - Patchwork

### unorganized
https://stackoverflow.com/questions/42637985/redux-best-way-to-perform-business-logic-with-state-data-in-event-handlers | Redux: Best way to perform business logic with state data in event handlers - Stack Overflow
https://wayland.freedesktop.org/libinput/doc/latest/architecture.html | libinput’s internal architecture — libinput 1.12.3 documentation
http://who-t.blogspot.com/2018/05/x-server-pointer-acceleration-analysis-part2.html | Who-T: X server pointer acceleration analysis - part 2
https://wayland.freedesktop.org/libinput/doc/latest/api/group__base.html#gac30276a06e8b1434b959f2c8dde74f7c | libinput: Initialization and manipulation of libinput contexts
https://wayland.freedesktop.org/libinput/doc/latest/reporting-bugs.html#reporting-touchpad-bugs | Reporting bugs — libinput 1.12.3 documentation
https://github.com/torvalds/linux/blob/master/drivers/input/evdev.c | linux/evdev.c at master · torvalds/linux
https://www.businessinsider.com/macbook-touchpad-better-than-windows-10-touchpads-2015-8 | MacBook touchpad better than Windows 10 touchpads - Business Insider
https://unix.stackexchange.com/questions/131432/which-driver-is-handling-my-touchpad | linux - Which driver is handling my touchpad? - Unix & Linux Stack Exchange
https://www.collabora.com/news-and-blog/news-and-events/linux-kernel-4.19.html | Linux Kernel 4.19
https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/commit/?id=771c035372a036f83353eef46dbb829780330234 | kernel/git/torvalds/linux.git - Linux kernel source tree
https://lwn.net/Articles/763106/ | The second half of the 4.19 merge window [LWN.net]
https://github.com/getpatchwork/patchwork | getpatchwork/patchwork: Patchwork is a web-based patch tracking system designed to facilitate the contribution and management of contributions to an open-source project.
https://kernelnewbies.org/ML | ML - Linux Kernel Newbies
https://unix.stackexchange.com/questions/28736/what-does-the-i8042-nomux-1-kernel-option-do-during-booting-of-ubuntu | linux - What does the 'i8042.nomux=1' kernel option do during booting of Ubuntu? - Unix & Linux Stack Exchange
