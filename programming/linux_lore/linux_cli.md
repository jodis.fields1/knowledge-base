[The Essential Cheat Sheet for Linux Admins](https://www.loggly.com/blog/the-essential-cheat-sheet-for-linux-admins/) - should post this on a wall

### bare bones
/bin/login (http://man7.org/linux/man-pages/man1/login.1.html) is the default! per https://askubuntu.com/a/640112/457417 which shows `ps -o 'cmd=' -p $(ps -o 'ppid=' -p $$)`

also play around with x-terminal-emulator

### list explicitly installed packages
grr...

### display only user processes
`ps --ppid 2 -p 2 --deselect`

### init runlevels (deprecated)
http://www.comptechdoc.org/os/linux/howlinuxworks/ - e.g., explains runlevels https://web.archive.org/web/20170627171045/http://comptechdoc.org/os/linux/howlinuxworks/linux_hlrunlevels.html