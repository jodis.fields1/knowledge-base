See dotfiles in general esp. cheat

setuid - can upgrade permissions
* noticed over at https://www.reddit.com/r/linux/comments/96se41/the_418_kernel_is_out/e430r0g/
* https://unix.stackexchange.com/questions/91516/under-what-scenarios-would-i-want-to-set-a-suid-bit/91520#91520

## set folder when creating create file
2018-10: searched folder set owner and group when creating file - setgid on directory, no equivalent for setuid per https://unix.stackexchange.com/questions/165865/forcing-owner-on-created-files-and-folders | debian - Forcing owner on created files and folders - Unix & Linux Stack Exchange

* https://unix.stackexchange.com/questions/1314/how-to-set-default-file-permissions-for-all-folders-files-in-a-directory | How to set default file permissions for all folders/files in a directory? - Unix & Linux Stack Exchange
* https://askubuntu.com/questions/51951/set-default-group-for-user-when-they-create-new-files | permissions - Set default group for user when they create new files? - Ask Ubuntu
