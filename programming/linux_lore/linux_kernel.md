check out https://askubuntu.com/questions/tagged/kernel


[What is linux-headers-virtual?](https://www.quora.com/What-is-linux-headers-virtual)

## annotations
[A Heavily Commented Linux Kernel Source Code (PDF version)](https://www.reddit.com/r/linux/comments/asun0d/a_heavily_commented_linux_kernel_source_code_pdf/)

https://github.com/0xAX/linux-insides

[Linux Kernel Development](https://www.amazon.com/Linux-Kernel-Development-Robert-Love/dp/0672329468) - recommended by Oz at Bradfield

## load custom kernel
per https://askubuntu.com/questions/764241/how-to-remove-new-kernel-and-make-older-default-16-04
install new kernel
load GRUB (left shift), select new kernel
delete old kernel

## old introduction
[https://jvns.ca/blog/2014/01/04/4-paths-to-being-a-kernel-hacker/](https://jvns.ca/blog/2014/01/04/4-paths-to-being-a-kernel-hacker/)

[Getting into Linux Kernel Development](https://www.cyphar.com/blog/post/getting-into-linux-kernel-development) - not very concrete

[Rewrite Linux Kernel in Rust?](https://news.ycombinator.com/item?id=14479435)

[Why is the Linux kernel 15+ million lines of code? [closed]](https://unix.stackexchange.com/questions/223746/why-is-the-linux-kernel-15-million-lines-of-code/223763#223763)
    * 11m lines of driver code