Loop devices
===================
https://en.wikipedia.org/wiki/Loop_device | Loop device - Wikipedia
https://superuser.com/questions/799162/permanent-loop-device | linux - Permanent loop device? - Super User
https://wiki.osdev.org/Loopback_Device | Loopback Device - OSDev Wiki
https://unix.stackexchange.com/questions/218660/what-is-the-difference-between-loop-device-and-block-device | linux - What is the difference between loop device and block device? - Unix & Linux Stack Exchange
https://unix.stackexchange.com/questions/4535/what-is-a-loop-device-when-mounting | grep - What is a "loop device" when mounting? - Unix & Linux Stack Exchange
