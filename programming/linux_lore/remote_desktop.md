Remote desktop
===

keywords: rdp, vnc

See also: linux_networking_wakeonlan.md

## integrating w/ windows to launch just one app
https://news.ycombinator.com/item?id=22834286 | FreeRDP 2.0 – A Remote Desktop Protocol implementation | Hacker News
https://wiki.archlinux.org/index.php/rdesktop | Rdesktop - ArchWiki
https://serverfault.com/questions/25164/seamless-remote-windows-on-linux-client | rdp - Seamless Remote Windows on Linux Client - Server Fault
https://github.com/kimmknight/remoteapptool | kimmknight/remoteapptool: Create and manage RemoteApps hosted on Windows 7, 8, 10, XP and Server. Generate RDP and MSI files for clients.
https://stackoverflow.com/questions/1226772/can-rdp-clients-launch-remote-applications-and-not-desktops | Can RDP clients launch remote applications and not desktops - Stack Overflow
https://docs.microsoft.com/en-us/windows-server/remote/remote-desktop-services/clients/remote-desktop-allow-outside-access | Remote Desktop - Allow access to your PC from outside your network | Microsoft Docs


## android apps
https://docs.microsoft.com/en-us/windows-server/remote/remote-desktop-services/welcome-to-rds

Two versions: Remote Desktop 8 & 10 https://www.androidpolice.com/2020/08/03/microsoft-overhauls-its-remote-desktop-app-with-support-for-virtual-desktops/


## winners
ssh - use -L to local tunnel the port from remote to here
NoMachine - free plan, for now
TigerVNC - second place

## nixos
https://nixos.wiki/wiki/Remote_Desktop

## protips
when running laptop headless on ubuntu, use GNOME Tweaks Tool to disable shut down -> suspend per 

## options / reviews
TODO: check out 
* https://github.com/novnc/noVNC - Javascript!
* https://en.wikipedia.org/wiki/X2Go
  * smart caching of images https://wiki.x2go.org/doku.php/doc:faq:start
  * can do sound
* Remmina
* Thinlinc per https://www.reddit.com/r/sysadmin/comments/8j68pv/linux_based_rdp_server/
* Guacomole

options include xpra and vnc
 [Ask HN: What is the best setup to work remotely with GUI on Linux?](https://news.ycombinator.com/item?id=19318939)
 * TigerVNC

https://www.reddit.com/r/linux/comments/auzcp3/remote_desktop_on_linux_managing_gui_displays/
* X2Go pretty popular

## rdp
TODO: follow https://www.makeuseof.com/tag/how-to-establish-simple-remote-desktop-access-between-ubuntu-and-windows/

"RDP is also seen as far more performant than VNC, due to the semantic advantage" per https://superuser.com/questions/32495/whats-the-difference-between-rdp-vs-vnc

### clients
https://unix.stackexchange.com/questions/140047/what-are-the-differences-between-rdesktop-and-xfreerdp

for mac, use official Microsoft client https://apps.apple.com/us/app/microsoft-remote-desktop-10/id1295203466?mt=12

### servers
http://www.xrdp.org/ - https://github.com/neutrinolabs/xrdp

https://github.com/FreeRDP/FreeRDP

"FreeRDP guys(who I was a member of at the time) would not commit to a standard API. They will change it all the time with no way to detect. One of the reasons we started NeutrinoRDP." per https://github.com/neutrinolabs/xrdp/issues/1338

## TigerVNC
for my thinkpad ubuntu:
* use 1680x1050 - or maybe 1920x1080?
* toggle resize to local window option
* resize window slightly to trigger refresh after connecting (?)
* when opening firefox, use `firefox -no-remote -P` and use a new profile if already running
* `vncserver -list` to check servers; also, `~/.vnc/xstartup` has unset SESSION_MANAGER which is funky
* `vncconfig -iconic &` has to be running to copy and paste (??) per https://askubuntu.com/a/904211/457417

TigerVNC looks like the best; followed something like https://www.cyberciti.biz/faq/install-and-configure-tigervnc-server-on-ubuntu-18-04/ - packages `tigervnc-standalone-server tigervnc-xorg-extension tigervnc-viewer` packaged by https://salsa.debian.org/debian-remote-team/tigervnc

### gotchas
GOTCHA: 
* [Bug 960149 - unable to unlock gnome in ThinLinc (VNC)](https://bugzilla.redhat.com/show_bug.cgi?id=960149) reported by me to https://github.com/TigerVNC/tigervnc/issues/856
* https://superuser.com/questions/1403368/tigervnc-connection-refused-when-using-ip-address-and-accepted-with-127-0-0-1
  * `$localhost = "no";` in /etc/vnf.conf

keyboard shortcuts:
* started wiki page https://github.com/TigerVNC/tigervnc/wiki/Keyboard-shortcuts
* hit f8 to see special menu
* https://groups.google.com/forum/#!topic/tigervnc-users/Ixn62_ZsRGI

https://wiki.archlinux.org/index.php/TigerVNC

### port forwarding
On netgear (routerlogin.net), go to port forwarding, add custom service, and so on
* port forward the router per 
  * https://www.howtogeek.com/124849/how-to-remote-control-your-home-computer-from-anywhere-with-vnc/
  * https://www.ncf.ca/ncf/support/wiki/Port_forwarding#Port_forwarding_on_SmartRG_Modems
  * goto http://192.168.42.1/admin/


### slow / speed optimization
seems that plugging in ethernet to server helped a lot?
* https://superuser.com/questions/164576/remote-desktop-for-os-x-thats-better-than-vnc - tried Remotix briefly


## Xpra
Like tmux for GUI applications, might be faster than VNC and lets the applications live in their own windows

Steps:
1. Need to have an X environment, might be dependency of xpra? 
    a. installed xinit and i3 to get the baseline at least
    b. set up ~/.xinitrc and add exec i3 per https://bbs.archlinux.org/viewtopic.php?pid=1130626#p1130626
    c. xinit /full/path/to/i3
2. may have to set display e.g. `export DISPLAY=:0.0`
3. install a late version by following instructions at https://www.xpra.org/trac/wiki/Download#Linux - i.e. winswitch instructions but replacing /s/winswitch/xpra 
4. run:
    a. from server: xpra start ssh/root@104.251.213.36/100 --start-child=xterm
    b. from client: xpra attach ssh/root@104.251.213.36/100

## Lightweight window managers?
i3 seems to be best, but it's tough; $mod got mapped to alt https://fedoramagazine.org/getting-started-i3-window-manager/
