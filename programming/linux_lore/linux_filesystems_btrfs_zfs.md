MOTIVATION: [When is it safe to copy a SQLite data file that is currently open?](https://stackoverflow.com/questions/22973427/when-is-it-safe-to-copy-a-sqlite-data-file-that-is-currently-open)
TODO: figure out why CoW (copy-on-write) does not work with VMs
TODO: try out http://rockstor.com/ ?

major competitor is XFS (supported by RedHat) which has no checksumming of data or snapshots

snapshots with ex4 using lvcreate --snapshot https://unix.stackexchange.com/a/225744/108198

## general
https://en.wikipedia.org/wiki/ECC_memory - recommended, gets to core computer science reliability questions, identify with `dmidecode --type 16` per https://www.cyberciti.biz/faq/ecc-memory-modules/

[Let's discuss. Why do you use or not use BTRFS?](https://www.reddit.com/r/linux/comments/61js64/lets_discuss_why_do_you_use_or_not_use_btrfs/) - lots of interesting back and forth, points to pgbench that btrfs is definitely slower

[BTRFS disadvantages?](https://www.reddit.com/r/DataHoarder/comments/47abm6/btrfs_disadvantages/) - comparisons point to ZFS, https://www.reddit.com/r/DataHoarder/comments/47abm6/btrfs_disadvantages/d0bmrwu/ points to a discussion on COI by btrfs devs

[BTRFS, A File System for Everyone](https://www.weareconvoy.com/2016/04/btrfs-a-file-system-for-everyone/) - positive, see http://rockstor.com/

[My assessment of “btrfs”](https://nwrickert2.wordpress.com/2016/04/15/my-assessment-of-btrfs/) - informal, going with ext4

### zfs
[ZFS Root Filesystem on AWS](https://news.ycombinator.com/item?id=13278949)
supported in ubuntu - https://wiki.ubuntu.com/Kernel/Reference/ZFS
https://pthree.org/2012/04/17/install-zfs-on-debian-gnulinux/ - pointed from the Github wiki

### btrfs
[SUSE statement on the future of btrfs](https://www.reddit.com/r/linux/comments/6vof8h/suse_statement_on_the_future_of_btrfs/)
https://btrfs.wiki.kernel.org/index.php/Main_Page - skimmed main page

#### tips
`btrfs filesystem  df -h  /` instead of df per https://www.reddit.com/r/linux/comments/6vof8h/suse_statement_on_the_future_of_btrfs/dm2qum3/

#### public bugs
[32 blocking bugs of any status](https://bugzilla.kernel.org/buglist.cgi?bug_severity=blocking&bug_status=NEW&bug_status=ASSIGNED&bug_status=REOPENED&bug_status=RESOLVED&bug_status=VERIFIED&bug_status=REJECTED&bug_status=DEFERRED&bug_status=NEEDINFO&bug_status=CLOSED&component=btrfs&list_id=973329&product=File%20System&query_format=advanced)
[282 verified bugs with more than 1 comment](https://bugzilla.kernel.org/buglist.cgi?bug_status=NEW&component=btrfs&f1=longdescs.count&f2=OP&f5=OP&j6=OR&j_top=OR&limit=0&list_id=973299&o1=greaterthan&order=changeddate%20DESC%2Cbug_status%2Cpriority%2Cassigned_to%2Cbug_id&product=File%20System&query_format=advanced&resolution=---&v1=1)

Good sign, there was recent (2017) response and investigation of bug report:
[Bug 197681 - BTRFS qgroup limit problem with small files (Ubtunu 16.04 LTS)](https://bugzilla.kernel.org/show_bug.cgi?id=197681)