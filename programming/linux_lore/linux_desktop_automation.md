technically this is not just a KDE thing

#### automation complaint
2018-04 raised awareness at: https://security.stackexchange.com/questions/183885/why-has-ubuntu-18-04-moved-back-to-insecure-xorg
UPDATE: way-cooler seems more receptive with its D-Bus API - "Applications can no longer read your keystrokes unless you give them explicit permission to do so" http://way-cooler.org/

posted to https://pointieststick.wordpress.com/2018/02/17/im-on-patreon-now/:
Actually, as I explore desktop automation tools (a critical need for me), I'm learning about how xdotool is no longer supported and I'm rather disappointed. Lots of discussion about this and userland fragmentation driven by Wayland over at https://www.reddit.com/r/linux/comments/7bm9az/what_cant_be_done_on_wayland_still/ and I'm beginning to feel like this guy (https://github.com/octalmage/robotjs/issues/333#issuecomment-338424053: "And that is the main reason why I left Linux. If I can't control anything, what's the point."

I guess xdotool is disabled because it's a security risk, but I used it for years without being hacked. Driving is a safety issue, but I'm not willing to give it up.

macOS has powerful automation tools such as http://www.hammerspoon.org/ (among others) and it looks like I'll be doubling down on macOS because of it.

I did notice that the Sway folks seem to be more focused on interoperability (at least they point to https://github.com/swaywm/sway-protocols) but their maintainer says "patch Sway" if you want to do automation (https://github.com/swaywm/sway/issues/719).

Going deep down the rabbit hole, I found https://github.com/myfreeweb/evscript - but I doubt automation is going to be elegant. As the author of that notes, 'Wayland compositor authors could have agreed on a protocol like this one to allow all this useful functionality, but with secure access control, just like on macOS where this requires ticking a checkbox in accessibility settings. But no, their attitude has been "screw you, because security".'

Ultimately, I suspect that the Wayland compositor which allows for automation will gain market share, possibly forcing the others to go along with it. Or maybe Wayland will push more folks to macOS as the technical automation-minded people see what they're missing over there.

Possibly I'm misunderstanding or reading FUD (Fear, uncertainty, doubt) and the problems aren't as deep as they sound. I hope that's the case - maybe you could write a blog post about it.

##### automation complaint post research
[In blunt terms Mir has become a general compositor, capable of connecting smaller DEs](https://www.reddit.com/r/linux/comments/7bm9az/what_cant_be_done_on_wayland_still/dpkg802/) - not sure this is really true
[Keyboard emulation in Wayland](https://unix.stackexchange.com/questions/381831/keyboard-emulation-in-wayland) - see 2 other discussions in comment
[Wayland improvements since Plasma 5.8 release](https://blog.martin-graesslin.com/blog/2016/10/wayland-improvements-since-plasma-5-8-release/#comment-72083)

Looked at Wayland bugs https://bugs.freedesktop.org/describecomponents.cgi?product=Wayland:
Wayland: [Add an API for taking screenshots and recording screencasts](https://bugs.freedesktop.org/show_bug.cgi?id=98894) - stalled in 2016
Libinput [Some way to inject automated libinput events?](https://bugs.freedesktop.org/show_bug.cgi?id=99800)

[[RFC] Interface for injection of input events](https://lists.freedesktop.org/archives/wayland-devel/2017-March/033676.html) - response from xdotools maintainer
[[RFC] Interface for injection of input events](https://lists.freedesktop.org/archives/wayland-devel/2017-March/033518.html) - the security naysayer

[Actiona: X11 has not been detected](https://wiki.actiona.tools/doku.php?id=en:x11notdetected) - another automation framework broken per https://github.com/Jmgr/actiona/issues/67
