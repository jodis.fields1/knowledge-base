



### 2018-10
https://wiki.archlinux.org/index.php/HiDPI

Slight epiphany: [How the Retina Display MacBook Pro Handles Scaling](https://www.anandtech.com/show/5996/how-the-retina-display-macbook-pro-handles-scaling)
* "5.4-inch panel features a native resolution of 2880 x 1800, or exactly four times the standard 1440 x 900 resolution of a regular 15-inch MacBook Pro"
* "By default, the Retina MBP ships in a pixel doubled configuration. You get the effective desktop resolution of the standard 15-inch MacBook Pro's 1440 x 900 panel, but with four physical pixels driving every single pixel represented on the screen"


### old
[Top 3 Misconceptions about HiDPI](https://medium.com/elementaryos/top-3-misconceptions-about-hidpi-f5ef493d7bf8)

https://gitlab.com/smlx/sway-build/ - 

### Wayland start
[Core Wayland protocol and libraries](https://github.com/wayland-project/wayland) - ?

[Writing a Wayland Compositor, Part 1: Hello wlroots](https://news.ycombinator.com/item?id=16406471) - commented in the github repo, found dependencies at https://jan.newmarch.name/Wayland/ProgrammingClient/

[How input works – Keyboard input](https://blog.martin-graesslin.com/blog/2016/12/how-input-works-keyboard-input/) - also some discussion of automation here!

[Where do I start if I want to write a wayland compositor?](https://stackoverflow.com/questions/24960352/where-do-i-start-if-i-want-to-write-a-wayland-compositor)

### X11
http://openbox.org/wiki/Help:Autostart
https://ubuntuforums.org/showthread.php?t=2259345
https://raspberrypi.stackexchange.com/questions/3972/running-x11-without-window-manager
https://encrypted.google.com/search?hl=en&q=x11+without+window+manager&oq=x11+without+window+manager&gs_l=serp.3..0j0i22i30k1.8345.10887.0.11007.22.11.0.0.0.0.381.1313.0j1j2j2.5.0....0...1.1.64.serp..17.5.1312.UuJCcPievSo
https://linuxconfig.org/how-to-run-x-applications-without-a-desktop-or-a-wm
https://askubuntu.com/questions/432255/what-is-display-environment-variable
https://help.ubuntu.com/community/ServerGUI

