NOTE: cheat pages for user commands should point here

useradd, adduser, usermod, getent, /etc/passwd -
* very Unix (in the sense of awkward UX) tools
* NixOS fixes this with declarative management

## resources
https://www.quora.com/Where-can-I-get-the-source-code-of-the-useradd-command-in-Linux | Where can I get the source code of the useradd command in Linux? - Quora
https://github.com/Distrotech/shadow-utils/blob/distrotech-shadow-utils/src/useradd.c | shadow-utils/useradd.c at distrotech-shadow-utils · Distrotech/shadow-utils
https://linux.die.net/man/8/adduser | adduser(8) - Linux man page
