underscore to put it at the top - no point if you can't get past this!

[Setting up a multi-boot of 5 Linux distributions](https://medium.com/@manujarvinen/setting-up-a-multi-boot-of-5-linux-distributions-ca1fcf8d502)
    * someday...

## reinstall kernel
I had to do this: [How to restore deleted files in /boot? (vmlinuz missing, system does not boot)](https://askubuntu.com/questions/696409/how-to-restore-deleted-files-in-boot-vmlinuz-missing-system-does-not-boot)

## 2018-03 troubleshooting
[How to fix boot into initramfs prompt and “mount: can't read '/etc/fstab': No such file or directory” and “No init found”?](https://unix.stackexchange.com/questions/120198/how-to-fix-boot-into-initramfs-prompt-and-mount-cant-read-etc-fstab-no-su) - passed a kernel parameter

`break=premount` to drop into a debug shell early per https://wiki.debian.org/InitramfsDebug

also [How to scroll up after a kernel panic?](https://unix.stackexchange.com/questions/208260/how-to-scroll-up-after-a-kernel-panic) - see linux kernel debugging as well

[GRUB rescue problem after deleting Ubuntu partition! [duplicate]](https://askubuntu.com/questions/283559/how-can-i-run-disk-utility-in-terminal-via-a-comand-line)
    * rebooted again with  the Linux Mint usb ISO and it seemed to work this time...

## from usb
use etcher from resin.io
unetbootin is basically the same thing but broken?

### building on linux
lsblk
[Equivalent of LIST DISK in Linux? [duplicate]](https://unix.stackexchange.com/questions/157154/equivalent-of-list-disk-in-linux) -
https://askubuntu.com/questions/283559/how-can-i-run-disk-utility-in-terminal-via-a-comand-line

### building on macOS
GParted Live medium is a good general tool?
http://www.rodsbooks.com/gdisk/ - "GPT fdisk (consisting of the gdisk, cgdisk, sgdisk, and fixparts programs) is a set of text-mode partitioning tools for Linux, FreeBSD, Mac OS X, and Windows"
    * https://sourceforge.net/projects/gptfdisk/

may have to fiddle with the USB drive on macOS
    * 2018-03: USB drive was having issues
        * use macOS Disk Utility - kept trying to erase GPT FAT (same as FAT32), finally worked
        * CLI tools include diskutil -l fdisk and gpt
            * had to unmount drives to use fdisk - then see physical location (e.g. /dev/disk2) with diskutil -l - then `sudo gpt destroy -r /dev/disk2` but it turned out it wasn't a GPT (was an MBR instead)

### binary editing
Curiosity if 2018-05
[Add Binary Editor Feature](https://github.com/Microsoft/vscode/issues/2582)

https://unix.stackexchange.com/questions/282215/how-to-view-a-binary-file | assembly - How to view a binary file? - Unix & Linux Stack Exchange
https://github.com/ehlers/unyaffs/ | ehlers/unyaffs: Extract files from a YAFFS2 file system image
https://help.ubuntu.com/community/Grub2/Troubleshooting | Grub2/Troubleshooting - Community Help Wiki
https://www.techwalla.com/articles/how-to-view-the-contents-of-a-master-boot-record | How to View the Contents of a Master Boot Record | Techwalla.com
http://blog.hakzone.info/posts-and-articles/bios/analysing-the-master-boot-record-mbr-with-a-hex-editor-hex-workshop/ | Analysing the Master Boot Record (MBR) with a hex editor (Hex Workshop) | Dr. Haider M. al-Khateeb
https://stackoverflow.com/questions/8953369/display-file-in-binary-format | vim - Display file in binary format - Stack Overflow
https://github.com/stef-levesque/vscode-hexdump/issues/22 | Open a ~15MB file · Issue #22 · stef-levesque/vscode-hexdump
https://vi.stackexchange.com/questions/343/how-to-edit-binary-files-with-vim | filetype - How to edit binary files with Vim? - Vi and Vim Stack Exchange
