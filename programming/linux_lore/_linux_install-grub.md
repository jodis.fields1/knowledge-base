
https://www.unix-ninja.com/p/Manually_booting_the_Linux_kernel_from_GRUB - helped me

Just don't. Use reEFInd instead per [Is GRUB the best bootloading solution? Is there an easier alternative?](https://unix.stackexchange.com/questions/146784/is-grub-the-best-bootloading-solution-is-there-an-easier-alternative)

[How to get to the GRUB menu at boot-time?](https://askubuntu.com/questions/16042/how-to-get-to-the-grub-menu-at-boot-time) - Shift or Esc

## reviewed
[Our Bootloader Problem](https://npmccallum.gitlab.io/post/our-bootloader-problem/)

[Can I install grub2 on a flash drive to boot both BIOS and UEFI](https://unix.stackexchange.com/questions/273329/can-i-install-grub2-on-a-flash-drive-to-boot-both-bios-and-uefi)

[How does GRUB 2 work under multiple Linux partitions with UEFI](https://unix.stackexchange.com/questions/379441/how-does-grub-2-work-under-multiple-linux-partitions-with-uefi)
