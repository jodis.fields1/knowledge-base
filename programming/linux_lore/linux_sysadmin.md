### TODO

https://github.com/trimstray/test-your-sysadmin-skills - found at https://news.ycombinator.com/item?id=17857285

https://www.amazon.com/UNIX-Linux-System-Administration-Handbook/dp/0134277554

### openSUSE
See linux_e2e_opensuse.md

Use OpenSUSE to learn AppArmor and CentOS for SELinux?
unimpressed by https://en.opensuse.org/openSUSE:Submitting_bug_reports
* apparmor - https://medium.com/information-and-technology/so-what-is-apparmor-64d7ae211ed

### logging / debugging
TODO: try out [lnav](https://github.com/tstack/lnav) - http://lnav.org/
TODO: checkout The Art of Monitoring (artofmonitoring.com)
[Continuously monitor files opened/accessed by a process](https://superuser.com/questions/348738/continuously-monitor-files-opened-accessed-by-a-process) - also see https://unix.stackexchange.com/q/157064/108198

### systemd
see also: linux_systemd.md
https://wiki.archlinux.org/index.php/Systemd - complex
[How do I make my systemd service run via specific user and start on boot?](https://askubuntu.com/a/859583/457417) - systemctl --user, might be worth answering https://superuser.com/questions/544399/how-do-you-make-a-systemd-service-as-the-last-service-on-boot with that
https://askubuntu.com/questions/836525/why-does-an-ubuntu-server-have-graphical-target-as-the-default-systemd-target - ??

looked for in the following locations per https://fedoramagazine.org/systemd-getting-a-grip-on-units/ with higher overriding lower:
~/.config/systemd/user (if enabled by loginctl enable-linger USERNAME)
/etc/systemd/system
/run/systemd/system
/usr/lib/systemd/system

#### Tips
systemd-analyze - per https://serverfault.com/a/617864/309584 shows dependency tree

#### General overview
Series linked at https://fedoramagazine.org/systemd-unit-dependencies-and-order/ particularly https://fedoramagazine.org/systemd-getting-a-grip-on-units/

### Access control for userspace

Arch doesn't have either SELinux or AppArmor

The standard Discretionary Access Control is supplemented by  Mandatory Access Control through Linux Security Modules such as  AppArmor or SELinux; Arch has issues with AppArmor    [per  wiki talkpage](https://wiki.archlinux.org/index.php/Talk:AppArmor)

There's an ongoing debate about capabilities-based systems  but   [as this guy  notes, there are few in the wild](http://security.stackexchange.com/a/11251), also read   [the  c2 discussion](http://c2.com/cgi/wiki?CapabilitySecurityDiscussion) and see erights.org for advocate

