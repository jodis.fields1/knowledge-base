

# examining usb
https://stackoverflow.com/questions/17058134/is-there-an-equivalent-of-lsusb-for-os-x

# 2018-10 install on usb drive
## ubuntu

[Ubuntu 16.04 systemd-boot.md](https://gist.github.com/0x414A/7f1c27ac24f05a42ed43aa73946a7033) - my ultimate solution; alternatively, see https://blobfolio.com/2018/06/replace-grub2-with-systemd-boot-on-ubuntu-18-04/ or http://valleycat.org/linux/arch-usb.html?i=2 (extremely detailed):

You do not need a separate /boot partition unless you have an LVM setup (used in dm-crypt setups).

Run ubiquity -b to open the installer with the option of skipping grub installation (since we're using systemd-boot).

When you get to the screen "Ubuntu has finished installation" choose Continue testing.

Open a Terminal.

Chroot into the new system.

mount /dev/sda5 /mnt # replace /dev/sda5 with wherever it is you installed Ubuntu
mount /dev/sda1 /mnt/boot/efi # this is where Ubuntu mounts the ESP
for i in /dev /dev/pts /proc /sys /run; do sudo mount -B $i /mnt$i; done
chroot /mnt
This will set systemd-boot to be our default:

```bash
aj@aj-MacbookPro $ sudo -i
root@aj-MacBookPro:~# history
    1  cd boot
    2  ls
    3  cd efi
    4  ls
    5  cat /etc/fstab # check entries
    6  ls
    7  cd ..
    8  ls
    9  cp abi-4.4.0-22-generic efi
   10  cp config-4.4.0-22-generic efi
   11  cp initrd.img-4.4.0-22-generic efi
   12  cp System.map-4.4.0-22-generic efi
   13  cp vmlinuz-4.4.0-22-generic efi
   14  ls efi
   15  cd efi
   16  ls
   17  bootctl --help
   18  cd
   19  cd /boot/efi
   20  ls
   21  mkdir -p loader/entries
   22  ls
   23  vi loader/loader.conf
   24  vi loader/entries/ubuntu.conf
   25  tree
   26  apt install tree
   27  tree
   28  bootctl install --path=/boot/efi
   29  efibootmgr
   30  ls
   31  cd /
   32  ls
   33  cd boo
   34  cd boot
   35  ls
   36  ls -l
   37  cp initrd.img-4.4.0-22-generic efi
   38  tree efi
   39  bootctl install --path=/boot/efi
   40  cd efi
   41  ls
   42  ls EFI
   43  ls EFI/systemd/
   51  efibootmgr -c -d /dev/sda1 -p 1 -l EFI/systemd/systemd-bootx64.efi -L "Linux Boot Manager"
   52  efibootmgr -o 0000
   53  ls
   54  history
root@aj-MacBookPro:~# 
```

### afterwards
* `/etc/fstab` was wrong for the boot partition
    * fixed via proper UUID https://serverfault.com/questions/3132/how-do-i-find-the-uuid-of-a-filesystem
* kept getting Invalid input/output message (corruption): https://askubuntu.com/questions/147228/how-to-repair-a-corrupted-fat32-file-system

### create disk
Ubuntu 18.04 - install instructions using Etcher did not create bootable drive; used ddrescue instead per [How to create a bootable Ubuntu 18.04 Bionic USB stick on Linux](https://web.archive.org/web/20180622060827/https://www.tecmint.com/install-linux-os-on-usb-drive/)


### bad advice
boot-repair - is it even maintained? https://askubuntu.com/a/84143/457417
YUMI – Multiboot USB Creator - where is the code?? http://www.pendrivelinux.com/yumi-multiboot-usb-creator/
### recipes
* [Installing Ubuntu on USB and booting from Destop with UEFI](https://askubuntu.com/questions/906857/installing-ubuntu-on-usb-and-booting-from-destop-with-uefi)
* [Ubuntu installation on USB stick with pure EFI boot (Mac compatible)](https://medium.com/@mmiglier/ubuntu-installation-on-usb-stick-with-pure-efi-boot-mac-compatible-469ad33645c9)


### ubuntu uefi issues
ubuntu "ubiquity" installer refuses to use target disk partition https://bugs.launchpad.net/ubuntu/+source/ubiquity/+bug/704763

[Placing grub.cfg in /boot/efi causes avoidable problems on EFI-based systems](https://bugs.launchpad.net/ubuntu/+source/grub2/+bug/1567534)
  
#### workarounds
* [Re: USB UEFI Install results in multiple boot options.](https://ubuntuforums.org/showthread.php?t=2295795&p=13360196#post13360196)


#### gdisk partition issues
"GUID Partition Table Header signature is wrong" - 
* sudo gdisk /dev/sdb - said "invalid GPT and valid MBR" ... "Main partition table overlaps first partition by 2 blocks" ... "Secondary partition table overlaps last partition by 33 blocks".
* deleted last partition, then inside gdisk:
    * hit x for experts, then s for resize partition table, w for quit