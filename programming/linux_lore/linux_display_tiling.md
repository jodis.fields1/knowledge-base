See KDE for KDE-specific

[Why use a tiling window manager over a floating one?](https://www.reddit.com/r/linux/comments/5zl0tv/why_use_a_tiling_window_manager_over_a_floating/) - ??

### possibilities
[Awesome WM vs i3](https://www.reddit.com/r/archlinux/comments/20q0s0/awesome_wm_vs_i3/) - ??
https://github.com/stumpwm/stumpwm - rejected, cannot save layouts

[[XFCE + i3] i3DE: A tiling desktop environment](https://www.reddit.com/r/unixporn/comments/93ypxp/xfce_i3_i3de_a_tiling_desktop_environment/)

#### kde tiling
for one of these I have to do:
    * krunner -> KWin Scripts -> turn on
    
#### jazqa
TODO: [Save the layouts feature](https://github.com/Jazqa/kwin-quarter-tiling/issues/52)
jazqa - https://www.reddit.com/r/kde/comments/6ptzh6/kwin_quarter_tiling_kwin_plugin/

#### lingtjien
https://github.com/lingtjien/Tiling-Gaps-Kwin - KDE runs JavaScript? - https://www.reddit.com/r/kde/comments/7bwu2a/kwin_tilinggaps_script_now_on_github/

#### faho
faho - [No config options](https://github.com/faho/kwin-tiling/issues/79)

#### i3wm
see dotfiles & dedicated note ./linux_desktop_i3wm.md
[Saving and restoring layouts](https://www.reddit.com/r/i3wm/comments/4twwi8/saving_and_restoring_layouts/) - ??
[i3-based desktop environment](https://www.reddit.com/r/i3wm/comments/7tde4c/i3based_desktop_environment_share_your_opinions/) - need a framework, and manjaro-i3 sounds like it!

##### installation
[xinit](https://wiki.archlinux.org/index.php/Xinit) - also see https://wiki.archlinux.org/index.php/Xprofile


###### initial pains
TODO: [SDDM remains in the background after i3wm login](https://github.com/sddm/sddm/issues/830) - make sure this ain't a problem

TODO: document gotchas
* [Any good guides for a clean i3wm install on Ubuntu or Debian?](https://www.reddit.com/r/i3wm/comments/6r7m6w/any_good_guides_for_a_clean_i3wm_install_on/)

I had sddm set to autologin, so I didn't see the option to run i3wm and instead executed `startx`. Wasted a couple hours wondering why I had a blank screen:
* finally saw "another window manager seems to be running" which made me realize I couldn't have KDE running
* got errors like [/usr/lib/xorg/Xorg.wrap: Only console users are allowed to run the X server](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=863891) (addressed, sort of by, by removing xorg-server-legacy)
* [Startx: EGL_MESA_drm_image required (virtualbox arch linux)](https://www.reddit.com/r/linuxquestions/comments/7lxfg6/startx_egl_mesa_drm_image_required_virtualbox/) - another unnecessary rabbit hole

#### awesomewm (rejected too barebones)
[Guide to making custom layouts?](https://www.reddit.com/r/awesomewm/comments/2f57tg/guide_to_making_custom_layouts/) - ??
[lcpz/lain](https://github.com/lcpz/lain) - awesome-vain successor

#### xomand (rejected as too complex)
Wayland Support

## wayland

https://github.com/i3/i3/issues/2648 - i3wm, awesomewm and xmonad won't be ported to Wayland

Two major options: Sway and Way-Cooler - also there's fireplace which may not be tiling
Sway, Way-Cooler, and fireplace originally built upon wlc which died - they then diverged into separate wayland compositors:
* Sway (https://cmpwn.com/@sir) and Way-Cooler folks decided to go with a new project wlcroots and wlcroots-rs
* Fireplace guy went with Smithay on rust

### sway
[Sway and client side decorations](https://drewdevault.com/2018/01/27/Sway-and-client-side-decorations.html) - lots of drama! led to fix noted at https://www.reddit.com/r/linux/comments/7teqen/sway_and_client_side_decorations/dtbxq1x/
[Sway 1.0 is not going to support the Nvidia proprietary driver](https://news.ycombinator.com/item?id=15564611) - use AMD instead
https://github.com/swaywm/sway - copies i3, prolly the leader
sway is the modern wayland equivalent - "will not support" saving/loading layouts to disk but probably possible with i3-getmsg per https://github.com/swaywm/sway/issues/2#issuecomment-354633202

## way-cooler
Customizable Wayland compositor (window manager) - https://github.com/way-cooler/way-cooler seems most modern
people made fun of its Chromium dependence https://www.reddit.com/r/linux/comments/4xganf/announcing_way_cooler_a_tiling_window_manager_for/