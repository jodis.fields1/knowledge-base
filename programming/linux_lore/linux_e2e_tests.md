See linux_desktop_kde.md for KDE testing

[Kernel quality control, or the lack thereof](https://lwn.net/Articles/774114/)

## 2020 check'
list of testing methods: https://elinux.org/Test_Systems
* also see [Linux Kernel Testing: Where Are We ?](https://elinux.org/images/9/9f/Linux-Kernel-Testing-Where-are-we.pdf)

LTP results are posted to their mailing list: 
* example http://lists.linux.it/pipermail/ltp/2020-March/015792.html
* https://github.com/linux-test-project/ltp


KernelCI https://kernelci.org/  - why in addition to LTP?
  * discussed at https://www.zdnet.com/article/automated-testing-comes-to-the-linux-kernel-kernelci/

## Safe upgrades
since most Linux distros don't do enough testing:
[Timeshift](https://github.com/teejee2008/timeshift) - noticed per 2018 Linux mint conversation [hat is Mint's current "official" recommendation about updates?](https://forums.linuxmint.com/viewtopic.php?f=18&t=263867&p=1430587&hilit=regression&sid=95177961ca67bf699965a6af0e7751c1#p1430587)

### performance testing
[Is there a benchmark tool for Ubuntu?](https://askubuntu.com/questions/198978/is-there-a-benchmark-tool-for-ubuntu)

### GNU testing
* [How the GNU coreutils are tested (2017)](https://news.ycombinator.com/item?id=18034265)
  * surprisingly good!

### kernelci
https://linux-test-project.github.io/
[Linux Kernel Testing: Intro to kernelci.org](https://web.archive.org/web/20171022233043/https://baylibre.com/intro-kernelci/ /) - see https://kernelci.org/

### General distros
https://ci.debian.net/doc/

### openQA from openSUSE
https://www.dennogumi.org/2016/11/testing-the-untestable/
https://github.com/os-autoinst/openQA/

### openSuse tests (2018-02)
https://github.com/os-autoinst/openQA/blob/master/docs/GettingStarted.asciidoc#introduction
https://github.com/os-autoinst/os-autoinst-distri-opensuse - ??
shout-user501	hey, I see references to https://openqa.suse.de/ periodically (e.g., https://progress.opensuse.org/issues/30388)
> but I can't navigate there - are these available only to openSUSE employees?
> 09:46	DimStar	shout-user501: openqa.suse.de is indeed the internal instance, not relevant to openSUSE; openSUSE has its own openqa instance on openqa.opensuse.org; progress.opensue.org otoh is used by the team also to track issues on internal products

#### 2017 review openQa
https://openqa.fedoraproject.org/ | openQA
https://fedoraproject.org/wiki/Test_Results:Fedora_27_Branched_20171008.n.0_Installation?rd=Test_Results:Current_Installation_Test | Test Results:Fedora 27 Branched 20171008.n.0 Installation - Fedora Project Wiki
https://fedoraproject.org/wiki/QA/FC6Test2TreeTesting | QA/FC6Test2TreeTesting - Fedora Project Wiki
https://fedoraproject.org/wiki/Wikitcms | Wikitcms - Fedora Project Wiki
https://fedoraproject.org/wiki/OpenQA | OpenQA - Fedora Project Wiki

##### python tests?
https://hackweek.suse.com/projects/allow-openqa-tests-in-python | Hack Week: allow openQA tests in python
https://github.com/os-autoinst/os-autoinst/pull/364#issuecomment-335037364 | Allow to implement openQA test modules in python by bmwiedemann · Pull Request #364 · os-autoinst/os-autoinst

### debian and autopkgtest
https://lwn.net/Articles/605405/

### Linux testing project / Robot framework
https://en.wikipedia.org/wiki/Linux_Desktop_Testing_Project
https://github.com/ldtp/ldtp2
https://github.com/wywincl/LDTPLibrary