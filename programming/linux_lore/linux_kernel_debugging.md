
[Linux kernel live debugging, how it's done and what tools are used?](https://stackoverflow.com/questions/4943857/linux-kernel-live-debugging-how-its-done-and-what-tools-are-used/42316607#42316607)
### Qemu
https://unix.stackexchange.com/questions/276480/booting-a-raw-disk-image-in-qemu
http://www.unicorn-engine.org/docs/beyond_qemu.html

See: 
* https://stackoverflow.com/questions/11408041/
* how-to-debug-the-linux-kernel-with-gdb-and-qemu
http://www.cs.rochester.edu/~sandhya/csc256/assignments/qemu_linux.html
* [Use kdevelop to develop linux kernel](https://sites.google.com/site/embedded2009/weekly-small-project-list/use-kdevelop-to-develop-linux-kernel)
* http://www.cs.rpi.edu/~moorthy/Courses/os98/Pgms/socket.html