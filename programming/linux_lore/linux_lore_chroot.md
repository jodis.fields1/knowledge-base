## 2018-07 looked into this on a whim
Conclusion: not really about security, but OK for isolation from scattering files across the system
[Why do I need to use chroot](https://unix.stackexchange.com/questions/340297/why-do-i-need-to-use-chroot)
[What chroot() is really for](https://lwn.net/Articles/252794/) (2007)

Tutorials:
[Linux / Unix: chroot Command Examples](https://www.cyberciti.biz/faq/unix-linux-chroot-command-examples-usage-syntax/)
[Arch Linux: Change root](https://wiki.archlinux.org/index.php/change_root)
