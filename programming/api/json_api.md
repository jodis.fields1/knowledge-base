
https://github.com/holidayextras/jsonapi-server - "A config driven NodeJS framework implementing json:api and GraphQL. You define the resources, it provides the api"

[https://github.com/json-api/json-api/issues/406](Clarify the versioning strategy for APIs)

### json-api sideposting
https://jsonapi-suite.github.io/jsonapi_suite/tutorial

(https://github.com/json-api/json-api/issues/795#issuecomment-343171311) - since 2015
"Honestly, those looking for a solution: Just use new resources. There's no need for a special type of specification. Resources can be whatever you want them to be, including a list of operations."
    * - https://github.com/json-api/json-api/issues/795#issuecomment-181576452
    * https://github.com/json-api/json-api/issues/795#issuecomment-343171311 exmaple of why
        * commenter overcame with https://jsonapi-suite.github.io/jsonapi_suite/ruby/writes/nested-writes
[Support for multiple operations in a single request]

https://github.com/e0ipso/subrequests


### Api development tooling
https://stackoverflow.com/questions/39384321/how-to-fetch-and-reuse-the-csrf-token-using-postman-rest-client
https://stackoverflow.com/questions/27182701/how-do-i-send-spring-csrf-token-from-postman-rest-client

## old
[Ghost discuss: Standard spec for JSON REST API? - JSON API, OData, etc with discussion at](https://github.com/TryGhost/Ghost/issues/2362):
* https://news.ycombinator.com/item?id=9280058
*  https://discourse.wicg.io/t/json-api-v1-0-review/207
*  http://nordicapis.com/top-specification-formats-for-rest-apis/

### library support
Library support:
* React Redux - see https://github.com/cohitre/redux-json-api
* http://jsonapi.org/implementations/#server-libraries-node-js - I like postlight/lux and ethanresnick/json-api also has a koa strategy
    * https://github.com/ErisDS/express-bookshelf-jsonapi
    * sadly json-server does not seem compatible but see https://github.com/holidayextras/jsonapi-server instead?
    * Django https://github.com/django-json-api/django-rest-framework-json-api
    * PHP Doctrine https://github.com/oligus/jad | oligus/jad: JSON Api to Doctrine