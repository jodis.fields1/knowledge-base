## rest
note that REST can be a bit overrated; see api_atomic_transactions.md

* [The RESTed NARWHL](https://web.archive.org/web/20181005090710/https://www.narwhl.com/)

Other discussion:
* https://apihandyman.io/do-you-really-know-why-you-prefer-rest-over-rpc/
* [What RESTful actually means](https://codewords.recurse.com/issues/five/what-restful-actually-means)
* http://martinfowler.com/articles/richardsonMaturityModel.html

[How a RESTful API server reacts to requests](https://www.oreilly.com/ideas/how-a-restful-api-server-reacts-to-requests)

https://github.com/marmelab/awesome-rest

## documentation
documentation generators for API such as Swagger? http://nordicapis.com/top-specification-formats-for-rest-apis/

### api clients
https://github.com/marmelab/awesome-rest#javascript-clients
https://github.com/marmelab/restful.js

## versioning
[APIs as infrastructure: future-proofing Stripe with versioning](https://stripe.com/blog/api-versioning)

### passing with querystrings
surprisingly, per https://stackoverflow.com/questions/6243051/how-to-pass-an-array-within-a-query-string not standardized - stringifying into JSON is interesting https://www.apharmony.com/software-sagacity/2014/10/node-js-passing-arrays-in-the-querystring/

### marmelab examples
https://github.com/marmelab/admin-on-rest | marmelab/admin-on-rest: A frontend framework for building admin SPAs on top of REST services, using React and Material Design
https://github.com/moonlight-labs/aor-jsonapi-client | moonlight-labs/aor-jsonapi-client: JSON-API compliant restClient for Admin-On-Rest

### graphql


[GraphQL Productivity](https://web.archive.org/save/https://about.sourcegraph.com/graphql/graphql-productivity/)
[GraphQL is for silos](https://antoniogarrote.wordpress.com/2016/11/23/graphql-is-for-silos/)

[Reducing our Redux code with React Apollo](https://dev-blog.apollodata.com/reducing-our-redux-code-with-react-apollo-5091b9de9c2a)

#### graphql link dump 2017-03
https://news.ycombinator.com/item?id=14260764
https://blog.risingstack.com/graphql-overview-getting-started-with-graphql-and-nodejs/
https://github.com/mugli/learning-graphql
https://hackr.io/tutorials/learn-graphql
https://edgecoders.com/graphql-learn-by-doing-part-1-of-3-9b04cadeacfa
https://github.com/chentsulin/koa-graphql