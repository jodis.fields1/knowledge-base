https://github.com/graphile/postgraphile/issues/19 | Any plans to support mysql 5.7? · Issue #19 · graphile/postgraphile
https://news.ycombinator.com/item?id=17565508 | Ask HN: Were you happy moving your API from REST to GraphQL? | Hacker News

## 2018-09 graphql basic
tl;dr: not much on implementation details, co-creator (https://github.com/schrockn) focuses on python but heavily node-based

https://github.com/json-api/json-api/issues/1272 | Tracking Issue for deficiencies vs. GraphQL · Issue #1272 · json-api/json-api
https://www.reddit.com/r/graphql/comments/81ngkj/graphqltypescript_can_be_used_with_typeorm/ | graphql-typescript can be used with typeorm! : graphql
https://medium.freecodecamp.org/rest-apis-are-rest-in-peace-apis-long-live-graphql-d412e559d8e4 | REST APIs are REST-in-Peace APIs. Long Live GraphQL.
https://stackoverflow.com/questions/48664987/what-does-mean-in-javascript | syntax - What does {| ... |} mean in javascript? - Stack Overflow
https://github.com/graphql/graphql-js/blob/master/src/__tests__/starWarsData.js | graphql-js/starWarsData.js at master · graphql/graphql-js
https://github.com/graphql/graphql-js/blob/master/src/__tests__/starWarsValidation-test.js | graphql-js/starWarsValidation-test.js at master · graphql/graphql-js
https://developer.github.com/v4/guides/migrating-from-rest/ | Migrating from REST to GraphQL | GitHub Developer Guide
https://www.robinwieruch.de/graphql-apollo-server-tutorial/ | GraphQL Server Tutorial with Apollo Server and Express - RWieruch


## 2018-07-20 graphql python
https://github.com/Getmrahul/Flask-Graphene-SQLAlchemy | Getmrahul/Flask-Graphene-SQLAlchemy: A demo project for Flask + GraphQL (With Graphene & SQLAlchemy)
https://www.google.com/search?q=apollo+vs+python+flask&hl=en&ei=MPFPW6OvEoGukwWNt4XYDg&start=10&sa=N&biw=1440&bih=781 | apollo vs python flask - Google Search
https://hackernoon.com/playing-with-graphql-python-flask-84ead622461c | Playing With GraphQL + Python Flask – Hacker Noon
https://medium.com/@fasterpancakes/graphql-server-up-and-running-with-50-lines-of-python-85e8ada9f637 | GraphQL server up and running with 50 lines of python
https://graphql.org/ | GraphQL | A query language for your API
https://www.howtographql.com/react-apollo/0-introduction/ | Fullstack Tutorial with GraphQL, React & Apollo
https://github.com/graphql-python/flask-graphql | graphql-python/flask-graphql: Adds GraphQL support to your Flask application.
https://github.com/graphql-python/graphene/issues | Issues · graphql-python/graphene
http://nafiulis.me/graphql-in-the-python-world.html | GraphQL in the Python World | EF
https://pypi.org/project/Flask-GraphQL/ | Flask-GraphQL · PyPI
https://medium.com/@fasterpancakes/graphql-server-up-and-running-with-50-lines-of-python-85e8ada9f637 | GraphQL server up and running with 50 lines of python
