
Hasura - tied to graphQL
* https://mobile.twitter.com/ThePracticalDev/status/1260646494597607424
* https://www.digitalocean.com/blog/learning-graphql-by-doing/?

### 2021
postgraphile is where it's at

* https://subzero.cloud/ - copy of postgraphile?

### prospects
https://github.com/dbohdan/automatic-api

#### flask-restless
[Adding a simple API to your Postgres database](http://jasonrhaas.com/2017/07/17/adding-a-simple-api-to-your-postgres-database.html) -

### maybe
PostgREST - does not handle updating relational data in a single request https://github.com/begriffs/postgrest/issues/667
* https://github.com/subzerocloud/postgrest-starter-kit - openresty
* https://subzero.cloud/

### rejected
https://stackoverflow.com/questions/45703244/is-there-a-postgrest-like-module-in-python-flask-which-can-automatically-provide

Options for automatic CRUD api:
python eve - settling on this for now, major gap is lack of JSON API support

flask-rest-jsonapi - also in the running https://github.com/miLibris/flask-rest-jsonapi


sandman - does not support relational data well https://github.com/jeffknupp/sandman2
postgraphql - many-to-many https://github.com/postgraphql/postgraphql/issues/521, https://github.com/postgraphql/postgraphql/issues/91, https://github.com/graphile/graphile.github.io/issues/4 - also see https://github.com/graphql-python/graphene-sqlalchemy/

#### javascript
* https://github.com/VulcanJS/Vulcan
    * rails for js?
* featherjs
    * https://github.com/feathersjs/express
    * https://github.com/feathersjs-ecosystem/feathers-guide/blob/master/step-by-step/basic-feathers/rest-api-server.md
* fortunejs - https://github.com/fortunejs/fortune

## ruby
[Build a RESTful JSON API With Rails 5 - Part One
](https://scotch.io/tutorials/build-a-restful-json-api-with-rails-5-part-one)
    * not sure if this is "automatic"
