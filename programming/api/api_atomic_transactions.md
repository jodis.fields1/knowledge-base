Difficult to do atomic transactions with pure REST API.

Conclusion: Good case for having both an RPC and a REST API.

## 2018-09 link dump
* kickoff: rest api atomic transactions - Google Search

### close / workarounds / discussion
https://www.reddit.com/r/programming/comments/7s9tm2/a_response_to_rest_is_the_new_soap/dt3vdyf/ | berkes comments on A Response to REST is the new SOAP
https://www.smashingmagazine.com/2016/09/understanding-rest-and-rpc-for-http-apis/ | Understanding RPC Vs REST For HTTP APIs — Smashing Magazine
https://stackoverflow.com/questions/9056009/restful-atomic-update-of-multiple-resources | rest - RESTful atomic update of multiple resources? - Stack Overflow
https://firebase.googleblog.com/2017/07/introducing-conditional-rest-requests.html | The Firebase Blog: Introducing Conditional REST Requests

### distant
https://github.com/json-api/json-api/issues/205#issuecomment-36626149 | Creating two related objects in one POST · Issue #205 · json-api/json-api
https://softwareengineering.stackexchange.com/questions/224174/do-restful-apis-tend-to-encourage-anemic-domain-models | rest - Do RESTful APIs tend to encourage anemic domain models? - Software Engineering Stack Exchange
https://stackoverflow.com/questions/15056878/rest-vs-json-rpc/38474262#38474262 | REST vs JSON-RPC? - Stack Overflow
https://www.jsonrpc.org/specification | JSON-RPC 2.0 Specification

### unrelated
These should prolly be moved...
https://github.com/fossasia/open-event-server/issues/3641 | API documentation implementation for the nextgen API · Issue #3641 · fossasia/open-event-server
https://github.com/apiaryio/api-blueprint | apiaryio/api-blueprint: API Blueprint
https://swagger.io/blog/news/mulesoft-joins-the-openapi-initiative/ | MuleSoft Joins the OpenAPI Initiative: The End of the API Spec Wars | Swagger
https://dzone.com/articles/transactions-for-the-rest-of-us | Transactions for the REST of Us - DZone Integration
https://www.hl7.org/FHIR/http.html#transaction | Http - FHIR v3.0.1
