
== python general: @ deck:python ==

Q: Delete an element from a dictionary @f5p5esusj4Z
- del d[key], d.get('key', 'default'); be careful about key errors and shallow copies

Q: What does if __name__ == “__main__”: do? @59N8Aat8a24
- One reason for doing this is that sometimes you write a module (a .py file) where it can be executed
  directly. Alternatively, it can also be imported and used in another module. -https://stackoverflow.com/questions/419163/what-does-if-name-main-do
  
 Q: What does the == operator actually do on a Python dictionary?
 - deep quality per
   https://stackoverflow.com/questions/17217225/what-does-the-operator-actually-do-on-a-python-dictionary and https://stackoverflow.com/questions/1911273/is-there-a-better-way-to-compare-dictionary-values
