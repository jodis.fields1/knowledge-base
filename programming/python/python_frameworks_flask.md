Flask
===========

https://github.com/humiaozuzu/awesome-flask

See https://gitlab.com/jcrben-not-mine/flask/flask-group

Zappa? https://news.ycombinator.com/item?id=17380293

https://github.com/Robpol86/Flask-Large-Application-Example - I use this


### examples

[How to structure a Flask-RESTPlus web service for production builds](https://medium.freecodecamp.org/structuring-a-flask-restplus-web-service-for-production-builds-c2ec676de563) - 

#### citadel?
not sure why...
* https://github.com/projecteru/citadel/blob/feature/next-gen/citadel/app.py
* https://github.com/projecteru/citadel/blob/23b2b5a89aae60adc9e18f36a1944c6e50a592f7/citadel/app.py#L123

### 2018-03 link dump flask

#### integrations

#### flask server
https://github.com/miguelgrinberg/flasky | miguelgrinberg/flasky: Companion code to my O'Reilly book "Flask Web Development", second edition.
https://www.git-pull.com/code_explorer/django-vs-flask.html | 
https://github.com/tornado-utils/tornado-restless | tornado-utils/tornado-restless: Insprired on flask-restless and sqlalchemy api wrapping for tornado
https://stackoverflow.com/questions/14814201/can-i-serve-multiple-clients-using-just-flask-app-run-as-standalone/14815932#14815932 | python - Can I serve multiple clients using just Flask app.run() as standalone? - Stack Overflow
https://github.com/channelcat/sanic | channelcat/sanic: Async Python 3.5+ web server that's written to go fast

https://stackoverflow.com/questions/26811438/how-do-i-update-a-list-data-relation-in-python-eve | How do I update a list data_relation in Python Eve - Stack Overflow
https://stackoverflow.com/questions/21496055/flask-app-deployment-with-dokku | gunicorn - Flask app deployment with dokku - Stack Overflow
http://flaskappbuilder.pythonanywhere.com/ | F.A.B. Example
http://killtheyak.com/use-postgresql-with-django-flask/ | Kill The Yak | Use PostgreSQL with Flask or Django
https://github.com/JackStouffer/Flask-Foundation | JackStouffer/Flask-Foundation: A solid foundation for your flask app

##### flask restless
[Upgrading to Flask-restless v1.0.0](https://thelaziestprogrammer.com/sharrington/web-development/upgrading-to-flask-restless-v1.0.0-part-1)

TODO: pair with https://marshmallow.readthedocs.io/en/latest/
TODO: create a folder for custom endpoints
    * /bespoke/{writes,reads}
Maybe move over to Flask JSON API https://github.com/miLibris/flask-rest-jsonapi?
this guy patched it https://github.com/therealryanbonham/flask-restless/commit/7f7511e5db94e991fe6d608702e4793f21eb0ba0