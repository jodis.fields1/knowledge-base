list comprehension:
"Just remember the syntax: [ expression for item in list if conditional ]"  per http://www.pythonforbeginners.com/basics/list-comprehensions-in-python

fnmatch - handy globbing

ipython:
for x,y in os.environ.items():
    print('os.environ:',x,y)


2017-11-26:
renaming files:
import os
import shutil
imgfiles = [f for f in os.list('.') if fnmatch.fnmatch(f, '*.jpg')]
for i, f in enumerate(imgfiles):
    os.rename(f, f'2017_10-synthesize-{i}.jpg')