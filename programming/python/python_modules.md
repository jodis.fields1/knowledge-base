TODO move general stuff over

start with https://realpython.com/absolute-vs-relative-python-imports/
* https://realpython.com/python-modules-packages/ | Python Modules and Packages – An Introduction – Real Python

"One might think that python imports are getting more and more complicated with each new version." - https://stackoverflow.com/questions/67631/how-to-import-a-module-given-the-full-path#comment59401304_67692

https://portingguide.readthedocs.io/en/latest/imports.html

https://manikos.github.io/how-pythons-import-machinery-works

[PEP 451 -- A ModuleSpec Type for the Import System](https://www.python.org/dev/peps/pep-0451/) - wish I knew more about this

### use cases for importlib hooks
https://legacy.python.org/dev/peps/pep-0302/#id25 (seen at https://stackoverflow.com/questions/22185888/pythons-loader-what-is-it): "Extending the import mechanism is needed when you want to load modules that are stored in a non-standard way. Examples include modules that are bundled together in an archive; byte code that is not stored in a pyc formatted file; modules that are loaded from a database over a network"

### modules and requiring
https://stackoverflow.com/questions/42033360/how-to-make-a-package-executable-from-the-command-line?noredirect=1&lq=1 | python - How to make a package executable from the command line? - Stack Overflow
https://www.reddit.com/r/Python/comments/2gki9e/python_apps_the_right_way_entry_points_and_scripts/ | Python Apps the Right Way: entry points and scripts : Python

https://stackoverflow.com/questions/1054271/how-to-import-a-python-class-that-is-in-a-directory-above | module - How to import a Python class that is in a directory above? - Stack Overflow
https://stackoverflow.com/questions/17506552/python-os-path-relpath-behavior | Python os.path.relpath behavior - Stack Overflow
https://stackoverflow.com/questions/4684520/fixing-paths-in-python | Fixing '../../' paths in python - Stack Overflow
https://stackoverflow.com/questions/714063/importing-modules-from-parent-folder/33532002#33532002 | python - Importing modules from parent folder - Stack Overflow
https://stackoverflow.com/questions/8419401/python-defaultdict-and-lambda | collections - Python defaultdict and lambda - Stack Overflow
https://github.com/pcattori/require.py/blob/master/require/require.py | require.py/require.py at master · pcattori/require.py
https://stackoverflow.com/questions/40022220/attempted-relative-import-beyond-toplevel-package | python - Attempted relative import beyond toplevel package - Stack Overflow
https://github.com/pcattori/require.py | pcattori/require.py: Relative imports in Python, inspired by Node.js `require`
https://python-reference.readthedocs.io/en/latest/docs/property/fget.html | fget — Python Reference (The Right Way) 0.1 documentation