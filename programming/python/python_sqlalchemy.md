[SQLAthanor: Serialization / Deserialization for SQLAlchemy ORM](https://www.reddit.com/r/Python/comments/90jxnv/sqlathanor_serialization_deserialization_for/)

https://www.pythonsheets.com/notes/python-sqlalchemy.html

https://github.com/dahlia/awesome-sqlalchemy | dahlia/awesome-sqlalchemy: A curated list of awesome tools for SQLAlchemy

### modeling cookbook
[How can I achieve a self-referencing many-to-many relationship on the SQLAlchemy ORM back referencing to the same attribute?](https://stackoverflow.com/questions/9116924/how-can-i-achieve-a-self-referencing-many-to-many-relationship-on-the-sqlalchemy)

## introspection
[How to discover table properties from SQLAlchemy mapped object](https://stackoverflow.com/questions/2441796/how-to-discover-table-properties-from-sqlalchemy-mapped-object)

## testing
https://datamade.us/blog/transactional-testing/ | Transactional Testing with Pytest and Flask-SQLAlchemy - DataMade
http://alexmic.net/flask-sqlalchemy-pytest/ | Delightful testing with pytest and Flask-SQLAlchemy | Alex Michael
http://alextechrants.blogspot.com/2014/01/unit-testing-sqlalchemy-apps-part-2.html?m=1 | Alex's Tech Rants: Unit testing SQLAlchemy apps, part 2: the universal method
http://mattupstate.com/presentations/testing-flask-applications/
* also see https://github.com/SuryaSankar/flask-sqlalchemy-booster

### framework integrations
https://stackoverflow.com/questions/16491564/how-to-make-sqlalchemy-in-tornado-to-be-async/16503103 | python - How to make SQLAlchemy in Tornado to be async? - Stack Overflow
https://stackoverflow.com/questions/42373208/how-to-update-item-relationships-with-eve-sqlalchemy | python - How to update item relationships with eve-sqlalchemy - Stack Overflow
https://github.com/FactoryBoy/factory_boy/issues/305 | Option to make Faker return unique values · Issue #305 · FactoryBoy/factory_boy

### migrations
http://alembic.zzzcomputing.com/en/latest/cookbook.html | Cookbook — Alembic 0.9.9 documentation

### performance
see awesome-sqlalchemy
https://github.com/inconshreveable/sqltap | inconshreveable/sqltap: SQL profiling and introspection for applications using sqlalchemy

### fake data
https://github.com/klen/mixer | klen/mixer: Mixer -- Is a fixtures replacement. Supported Django, Flask, SqlAlchemy and custom python objects.
https://stackoverflow.com/questions/23152337/how-to-update-sqlalchemy-orm-object-by-a-python-dict | How to update sqlalchemy orm object by a python dict - Stack Overflow

### 2018-09-03 revisit:
while messing with flask-restless custom serialization, had trouble getting from an InstrumentedAttribute (articles list) on a mapped class to either a mapped class or at least a mapper; from mapped class (Person) -> articles.property (or prop?).mapper and then class - ultimately it looked like:
`self.model.articles.property.mapper.class_`

flask-rest-jsonapi helped: https://github.com/miLibris/flask-rest-jsonapi/blob/268e94808573033e0232007a887982207f715e9a/flask_rest_jsonapi/data_layers/alchemy.py#L406

### 2018-03 link dump
#### console / interactive
https://bitbucket.org/pyalot/sqlalchemy-console/ | pyalot / sqlalchemy console — Bitbucket
https://gist.github.com/ryansb/3ad9b7ccf225d46a16dc | SQLAlchemy/JSON Notebook - requires Python 3, SQLAlchemy, psycopg2, and Jupyter (formerly IPython Notebook)
http://codeflow.org/entries/2009/jul/31/sqlalchemy-console/ | SQLAlchemy Console - Codeflow
https://github.com/catherinedevlin/ipython-sql | catherinedevlin/ipython-sql: %%sql magic for IPython, hopefully evolving into full SQL client


