
See Anki mostly

https://www.fullstackpython.com - in general a great resource

##  terminal communication
https://snarky.ca/the-many-ways-to-pass-code-to-python-from-the-terminal/

## Books
Functional Programming in Python - in GDrive, mostly read

## quick ref
* [Is there a built-in function to print all the current properties and values of an object?](https://stackoverflow.com/questions/192109/is-there-a-built-in-function-to-print-all-the-current-properties-and-values-of-a)
* [Python: Find in list](https://stackoverflow.com/questions/9542738/python-find-in-list)
  * [Python 2 vs Python 3 - Difference in behavior of filter](https://stackoverflow.com/questions/41666977/python-2-vs-python-3-difference-in-behavior-of-filter)

## utility
https://github.com/Suor/funcy

### autoformatting
[Blog: A comparison of autopep8, black, and yapf - Code formatters for Python](https://www.reddit.com/r/Python/comments/8oqy03/blog_a_comparison_of_autopep8_black_and_yapf_code/)
* sounds like black with minimal config will have the most momentum

### linting
https://www.reddit.com/r/Python/comments/82hgzm/any_advantages_of_flake8_over_pylint/dvai60a/
* points to flake8 plugins
* https://www.slant.co/topics/2692/~python-code-linters - pylint is top

flake8 - Ian Stapleton Cordasco (sigmavirus24) show
pylint - bit of a one-man-show by Claudiu Popa (PCManticore) of Zapier

### Gotchas
[Microservices with Docker, Flask, and React - Version 2 has been released](https://www.reddit.com/r/Python/comments/7hhptz/microservices_with_docker_flask_and_react_version/dqrk84b/) - 

## docs
[the table of contents in epub file is too long](https://bugs.python.org/msg339931)
built in docs on iterables sucks

[Is there a documentation browser for Python as nice as railsapi.com](https://stackoverflow.com/questions/6658146/is-there-a-documentation-browser-for-python-as-nice-as-railsapi-com)

### foundation - language reference
2018-10: read the first 3 chapters https://docs.python.org/3/reference/index.html, data model and lexical analysis are both good!

tips: http://book.pythontips.com/en/latest/index.html

[Python Programming Tips #2: Spreading a Class Over Multiple Files](http://www.qtrac.eu/pyclassmulti.html)

### docstrings
reST is recommended by the PEP 287 per https://stackoverflow.com/questions/3898572/what-is-the-standard-python-docstring-format

### exceptions style
http://scottlobdell.me/2015/06/using-exception-handling-control-flow-python/

### Frameworks
Flask versus Django? 
https://github.com/pyeve/eve - maybe? but Mongo-focused
https://github.com/begriffs/postgrest - automatic, also see https://github.com/nuveo/prest

### ORM
SQLAlchemy versus Django? https://www.fullstackpython.com/sqlalchemy.html

https://github.com/grundic/awesome-python-models

##### Projects
Do fun Python projects: http://www.openbookproject.net/py4fun/ 
http://newcoder.io/
http://buildingskills.itmaybeahack.com/book/oodesign-3.1/html/index.html#

##### Internals
Start with [http://pgbovine.net/cpython-internals.htm](CPython internals: A ten-hour codewalk through the Python interpreter source code)?

[How can I learn more about Python’s internals? [closed]](https://stackoverflow.com/questions/3298464/how-can-i-learn-more-about-python-s-internals) - pointed to https://tech.blog.aknin.name/category/my-projects/pythons-innards/

[Docs for the internals of CPython Implementation](https://stackoverflow.com/questions/574004/docs-for-the-internals-of-cpython-implementation) - didn't say much

http://jessenoller.com/blog/2009/02/01/python-threads-and-the-global-interpreter-lock | Python Threads and the Global Interpreter Lock — jesse noller

