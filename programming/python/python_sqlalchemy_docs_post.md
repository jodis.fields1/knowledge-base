Thank you for the amazing tool! I've been messing around with SQLAlchemy a little bit lately, mostly in the context of projects such as https://github.com/miLibris/flask-rest-jsonapi and https://github.com/jfinkels/flask-restless.

I've noticed that there are a whole lot of properties on the InstrumentedAttribute of a mapped class instance (shown below). It's a bit overwhelming. In my screenshot, I count 20 public properties, altho 8 of them are from the InspectionAttr (http://docs.sqlalchemy.org/en/latest/orm/internals.html#sqlalchemy.orm.base.InspectionAttr). The QueryableAttribute and PropsComparator seem similar as both provide the adapter.  The remaining 11 are: class_, comparator, dispatch, expression, impl, info, key, parent, prop, property, and timetuple.

Some of these have no leading underscore, suggesting that they are not private. I found documentation for some of them over at http://docs.sqlalchemy.org/en/latest/orm/internals.html#sqlalchemy.orm.attributes.InstrumentedAttribute and its base classes, but I couldn't find any for others, including: dispatch, expression, and impl. Also, property is mentioned narratively at the top of the QueryableAttribute not called out the way other properties are, and has this duplicate "prop" - why both?

Also, I noticed that adapt_to_property is shown as public in the docs but in my screenshot seems to be prefixed by an underscore suggesting internal use. 

A search for dispatch (for example) didn't turn up the precise documentation that I might expect from for a public property (http://docs.sqlalchemy.org/en/latest/search.html?q=dispatch&check_keywords=yes&area=default).

If a PR updating the docs is welcome, let me know and I might take a stab (altho I'm a newb at this, so not much).


Screen Shot 2018-09-03 at 7.58.23 PM.png