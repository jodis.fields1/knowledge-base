
### debugging
https://stackoverflow.com/questions/37150910/how-can-i-use-pycharm-to-locally-debug-a-celery-worker | python - How can I use PyCharm to locally debug a Celery worker? - Stack Overflow
https://stackoverflow.com/questions/12698212/how-to-debug-celery-django-tasks-running-locally-in-eclipse | python - How to debug Celery/Django tasks running locally in Eclipse - Stack Overflow
https://stackoverflow.com/questions/4163964/python-is-it-possible-to-attach-a-console-into-a-running-process | python: is it possible to attach a console into a running process - Stack Overflow
https://stackoverflow.com/questions/1637198/method-to-peek-at-a-python-program-running-right-now | debugging - Method to peek at a Python program running right now - Stack Overflow


### general
https://github.com/Robpol86/Flask-Celery-Helper | Robpol86/Flask-Celery-Helper: Celery support for Flask without breaking PyCharm inspections.

### flask
https://citizen-stig.github.io/2016/02/17/using-celery-with-flask-factories.html | Using Celery with Flask Factories
http://shulhi.com/celery-integration-with-flask/ | Celery integration with Flask
https://blog.miguelgrinberg.com/post/using-celery-with-flask | Using Celery With Flask - miguelgrinberg.com

