https://github.com/kayhayen/Nuitka - ??

example commands: 
* `pip install --extra-index-url https://test.pypi.org/simple/ Flask-Restless-jcrben`
  * use --extra-index-url instead of -i to download its dependencies; otherwise you can use `--no-deps`

## history
2018-08-27: hit dependency hell on my user env; pip-autoremove was broken due to conflicting dependencies; lesson: aggressively use venv even for small little scripts
2018-08-23: stumbled around setup.py for ntfy - see my comment https://github.com/dschep/ntfy/issues/171#issuecomment-415627493

## venv and pip
https://github.com/kennethreitz/pipenv is the new hotness
also see https://github.com/fisherman/pipenv for automatic switching, discussion at https://github.com/kennethreitz/pipenv/issues/378

### Virtualenv/venv
NOTE: Python 3 has `pyvenv` (CLI to venv module) to replace `virtualenv`. Basically it seems simple: it adds an env/bin to the project  which has a bunch of Python files to the $PATH and changes the prompt. that's it!
I originally tried to use virtualenvwrapper, but found it awkward and Saurabh Kumar's [Virtualenv vs Virtualenvwrapper](https://saurabh-kumar.com/blog/virtualenv-vs-virtualenvwrapper.html) seems better.

### pip
pip install borgbackup[fuse] is the extra_requires of setup.py
    * not exposed in the UI, proposal noted at https://stackoverflow.com/questions/44507781/what-does-the-notebook-in-pip-install-ipythonnotebook-mean#comment85688290_44508042
        * https://github.com/pypa/pip/issues/4824
leaves equivalent: `pip list --not-required`
remove dependencies: `pip-autoremove somepackage -y` per https://stackoverflow.com/a/27713702/4200039
pip-chill or pipdeptree are cool to get around the child dependencies issue https://stackoverflow.com/questions/9232568/identifying-the-dependency-relationship-for-python-packages-installed-with-pip
For [How to backup python dependencies or modules already installed](http://stackoverflow.com/questions/6223426/how-to-backup-python-dependencies-or-modules-already-installed), use `pip install -r requirements.txt`.

### Packaging
2018-08-27: ran into hiccups uploading to pypi - frequent issue ([Invalid or non-existent authentication information](https://github.com/pypa/setuptools/issues/941)) - turns out I skimmed [the guide](https://packaging.python.org/tutorials/packaging-projects/) too quickly and didn't have an account at test.pypy.org
* https://stackoverflow.com/questions/16584552/how-to-state-in-requirements-txt-a-direct-github-source to install from git repo

## setup.py
start here: 
* https://packaging.python.org/tutorials/packaging-projects/
* see also: https://github.com/kennethreitz/setup.py

setuptools
    * [Replace easy_install with pip install](https://github.com/pypa/setuptools/issues/917)

Distutils is not powerful enough, so Setuptools (nonstandard) expanded its power, as explained at [Differences between distribute, distutils, setuptools and distutils2?](http://stackoverflow.com/questions/6344076/differences-between-distribute-distutils-setuptools-and-distutils2). As of 2016, things are converging but still a mess. There is also [Bento](http://cournape.github.io/Bento/) (WIP) and Anaconda (more niche?)