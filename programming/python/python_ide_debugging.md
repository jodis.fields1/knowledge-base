
## Debugging
how to debug: https://www.reddit.com/r/Python/comments/1jeurc/do_you_debug_python_with_breakpoints/
ipython: https://www.reddit.com/r/Python/comments/1ak2tt/open_ended_question_about_ipython_and_its_usage/
How can I tell where my python script is hanging? suggests python -m trace --trace YOURSCRIPT.py which is pretty cool

https://stackoverflow.com/questions/242485/starting-python-debugger-automatically-on-error - print exceptions, stack trace; also https://stackoverflow.com/questions/132058/showing-the-stack-trace-from-a-running-python-application
[How do you see the return value from a function in the Python debugger, without an intermediate?](https://stackoverflow.com/questions/10902203/how-do-you-see-the-return-value-from-a-function-in-the-python-debugger-without)

## Debuggers
https://github.com/fabioz/PyDev.Debugger - the debugger for pycharm/pydev and probably vscode-python down the line

## profiling
https://jvns.ca/blog/2018/09/08/an-awesome-new-python-profiler--py-spy-/


## pycharm
[View return value in debugger?](https://intellij-support.jetbrains.com/hc/en-us/community/posts/206974255-View-return-value-in-debugger-) - most likely an existing gap
[Ignorred breakpoints while debugging Django Celery](https://youtrack.jetbrains.com/issue/PY-14690) - contains workaround
https://youtrack.jetbrains.com/issue/PY-9801 - workaround for evaluating inside tests

## others

pudb - https://github.com/inducer/pudb note a wiki and code 
Spyder - awesome
Gdb - in some cases may be necessary per https://wiki.python.org/moin/DebuggingWithGdb
to print variables use:
dir () will give you the list of in scope variables:
globals () will give you a dictionary of global variables
locals () will give you a dictionary of local variables

See [IntegratedDevelopmentEnvironments](https://wiki.python.org/moin/IntegratedDevelopmentEnvironments) for the enormous list. It seems that Spyder has the most momentum in terms of activity and innovation, and I hope the community can settle on it and really solidify its position rather than scattering its resources.