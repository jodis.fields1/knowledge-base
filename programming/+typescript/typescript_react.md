[Deciphering TypeScript’s React errors](https://medium.com/innovation-and-technology/deciphering-typescripts-react-errors-8704cc9ef402)

TODO: use https://github.com/sw-yx/react-typescript-cheatsheet
TODO: review the piotr's doc

https://github.com/clausreinke/typescript-tools

https://github.com/ryanatkn/react-typescript-closing-the-loop

### react and typescript
https://stackoverflow.com/questions/40671978/typescript-struggles-with-redux-containers - used this
https://charleslbryant.gitbooks.io/hello-react-and-typescript/content/TypeScriptAndReact.html - hugely helpful
https://github.com/piotrwitek/react-redux-typescript-guide#connected-counter-example---with-own-props - hugely helpful
### core
https://github.com/DefinitelyTyped/DefinitelyTyped/blob/master/types/muicss/react.d.ts
https://github.com/DefinitelyTyped/DefinitelyTyped/blob/master/types/react/index.d.ts#L326
https://github.com/Microsoft/TypeScript/issues/11597#issuecomment-316124318 | Intellisense for object properties defined in multi-line JSDoc comments · Issue #11597 · Microsoft/TypeScript
https://stackoverflow.com/questions/35950385/react-intellisense-in-visual-studio-code | reactjs - React intellisense in Visual Studio Code - Stack Overflow
https://stackoverflow.com/questions/41847565/how-to-do-documentation-in-reactjs | javascript - How to do documentation in ReactJS? - Stack Overflow

## unorganized
https://github.com/Microsoft/TypeScript/issues/7000#issuecomment-182402530 | JSX and Salsa: Intellisense for ReactComponents does not work · Issue #7000 · Microsoft/TypeScript
https://github.com/Microsoft/TypeScript-React-Starter/blob/master/package.json | TypeScript-React-Starter/package.json at master · Microsoft/TypeScript-React-Starter

## stateless functions
https://medium.com/@iktakahiro/react-stateless-functional-component-with-typescript-ce5043466011 | React Stateless Functional Component with TypeScript
https://github.com/dxinteractive/jsdoc-react-proptypes | dxinteractive/jsdoc-react-proptypes: Lets jsdoc parse React propTypes to autocreate prop tags
https://github.com/Microsoft/vscode/issues/39520#issuecomment-349081484 | JSX attribute hint only displays for stateless functional component · Issue #39520 · Microsoft/vscode
https://github.com/Microsoft/TypeScript/issues/15587#issuecomment-299293469 | [JS] React stateless components propTypes error · Issue #15587 · Microsoft/TypeScript
