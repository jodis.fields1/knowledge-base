Troubleshooting: `Tracing module resolution: tsc --traceResolution`

read 
 * http://mherman.org/blog/2016/11/05/developing-a-restful-api-with-node-and-typescript/#.WIjT07YrKRt
 * https://github.com/niutech/typescript-compile and 
 * https://basarat.gitbooks.io/typescript/content/docs/project/tsconfig.html

TypeScript 2.4 discussion: https://news.ycombinator.com/item?id=14647041

## typing paths or sql
"types can make working with SQL an actual pleasure" - https://news.ycombinator.com/item?id=21033991

typed paths: https://stackoverflow.com/questions/37053800/typescript-specific-string-types - check at runtime
* https://glebbahmutov.com/blog/url-type/

[hackage: path: Support for well-typed paths](http://hackage.haskell.org/package/path)

## protips
[How to pass optional parameters in TypeScript while omitting some other optional parameters?](https://github.com/Microsoft/vscode/pull/23058#issuecomment-448354161)

### tuples
TODO: move this to typescript-learn repo
https://codingblast.com/typescript-tuples/ | Tuples in TypeScript - CodingBlast
https://stackoverflow.com/questions/48686849/using-tuples-in-typescript-type-inference | Using Tuples in TypeScript (Type Inference) - Stack Overflow
https://github.com/Microsoft/TypeScript/issues/20899 | Poor tuple support · Issue #20899 · Microsoft/TypeScript
https://github.com/Microsoft/TypeScript/issues/11312 | mapping over an array of tuple types does not give the correct type · Issue #11312 · Microsoft/TypeScript

### getting started (compiling)
need to use tsconfig.json to specify per https://github.com/Microsoft/TypeScript/issues/5858#issuecomment-255185701 

### codebases
[Ask HN: Which TypeScript codebase should I study to get better?](https://news.ycombinator.com/item?id=15662394)

### articles
Reddit - https://redditblog.com/2017/06/30/why-we-chose-typescript/
Lyft - why TypeScript https://eng.lyft.com/typescript-at-lyft-64f0702346ea, also has https://github.com/lyft/react-javascript-to-typescript-transform
Tumblr - https://engineering.tumblr.com/post/165261504692/flow-and-typescript   

### link dump
[Avoid Export Default](https://basarat.gitbooks.io/typescript/docs/tips/defaultIsBad.html)
https://github.com/s-panferov/awesome-typescript-loader/issues/250
https://github.com/Microsoft/TypeScript/issues/11917
[Consider allowing access to UMD globals from modules](https://github.com/Microsoft/TypeScript/issues/10178)

### import and default
epiphany: 
> only works when using babel in the toolchain, since it adds the required default field to react's export (which uses commonjs format) due to some "compatibility" adjustments regarding ES2015 modules. allowSyntheticDefaultImports: true only allows to make the typescript compiler assume the presence of the default export, but it won't be added in case it's absent.
    - https://github.com/wmonk/create-react-app-typescript/issues/214#issuecomment-353870566

https://itnext.io/great-import-schism-typescript-confusion-around-imports-explained-d512fc6769c2

[Importing default module fails when using ES6 syntax](https://github.com/Microsoft/TypeScript/issues/3337) - ?
[ES6 Modules](https://github.com/Microsoft/TypeScript/issues/2242) - ?
https://github.com/Microsoft/TypeScript/issues/10895
https://stackoverflow.com/questions/33793875/migrating-react-es6-to-typescript-import-statements-dont-work

https://github.com/DefinitelyTyped/DefinitelyTyped/issues/5128

## unorganized
TODO: what does `allowSyntheticDefaultImports` do?
* https://github.com/Microsoft/TypeScript/issues/10895#issuecomment-248359239
* "I turn this option on because I prefer import React from 'react' over import * as React from 'react'" per [Getting started with TypeScript and React](https://javascriptplayground.com/react-typescript/)

TODO: use esModuleInterop per https://www.typescriptlang.org/docs/handbook/release-notes/typescript-2-7.html

### IDE 
use `relativeFileDirname` & `fileBasenameNoExtension` as I note at https://github.com/Microsoft/vscode/issues/25786

https://stackoverflow.com/questions/31587949/hide-js-map-files-in-visual-studio-code
https://medium.com/thecodecampus-knowledge/howto-configure-intellij-webstorm-for-typescript-development-5c2388fa6ce2#.6p2nsx56c

#### VSCode
debugging typescript: https://medium.com/@mtiller/debugging-with-typescript-jest-ts-jest-and-visual-studio-code-ef9ca8644132
couple hundred issues vscode tracked: https://github.com/Microsoft/TypeScript/labels/VS%20Code%20Tracked

### Packages
https://github.com/jch254/starter-pack/tree/typescript

## experts
http://blakeembrey.com
http://brianflove.com

### tutorial
Everything by Schulz esp https://mariusschulz.com/blog/series/typescript-evolution
* https://egghead.io/courses/advanced-static-types-in-typescript
* https://mariusschulz.com/blog/tagged-union-types-in-typescript
