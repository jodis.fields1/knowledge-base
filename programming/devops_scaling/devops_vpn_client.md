
Conclusion: 
* cross: maybe Fruho might be the only one per https://github.com/Tunnelblick/Tunnelblick/issues/348#issuecomment-270571451
* macOS: Tunnelblick
* Linux: use network manager applethttps://bokunokeiken.wordpress.com/2015/11/07/kubuntu-connect-to-openvpn-server-with-network-manager-applet/ | Kubuntu: Connect to OpenVPN Server with Network Manager Applet | Pandalon

## the search for cross-platform
https://github.com/SoftEtherVPN/SoftEtherVPN/issues | Issues · SoftEtherVPN/SoftEtherVPN
http://www.freelan.org/ | freelan - A VPN client that loves you !
https://github.com/fruho/fruhoapp | fruho/fruhoapp: Fruho VPN Manager - Universal VPN Client |
https://github.com/freelan-developers/freelan | freelan-developers/freelan: The main freelan repository.
https://www.google.com/search?hl=en&q=freelan%20Go%20and%20edit%20the%20configuration%20file%20at%20%2Fusr%2Flocal%2Fetc%2Ffreelan%2Ffreelan.cfg%20to%20get%20started | freelan Go and edit the configuration file at /usr/local/etc/freelan/freelan.cfg to get started - Google Search
http://www.infradead.org/openconnect/gui.html | OpenConnect VPN client.
https://openconnect.github.io/openconnect-gui/ | Welcome to OpenConnect graphical client pages. | OpenConnect GUI
http://www.infradead.org/openconnect/mail.html | OpenConnect VPN client.
https://github.com/openconnect/openconnect/tree/master/www | openconnect/www at master · openconnect/openconnect
https://askubuntu.com/questions/508250/openvpn-gui-client-for-udp-tcp/508372 | software recommendation - OpenVPN GUI client for UDP/TCP - Ask Ubuntu