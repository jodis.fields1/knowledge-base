
# new
https://www.qovery.com/

# old
https://serversforhackers.com/ - spent a fair bit of time around here

training: https://www.reddit.com/r/devops/comments/chszal/has_anyone_done_udacitys_aws_cloud_devops/

NoOps: the goal https://blog.appdynamics.com/engineering/is-noops-the-end-of-devops-think-again/

Resources:
https://theagileadmin.com/what-is-devops/

dokku tutorial:
https://ritazh.github.io/devopsfun/posts/

## baseline productivity
use tmux (byobu) per https://stackoverflow.com/questions/7210011/amazon-ec2-ssh-timeout-due-inactivity

use 'w':
    * [I usually run 'w' first when troubleshooting unknown machines](https://news.ycombinator.com/item?id=16685452)

* https://github.com/anumsh/Linux-Server-Configuration - suggests glances, ssh port change
* https://github.com/bencam/linux-server-configuration - various software

## perspectives / rants
[DevOps Is Bullshit: Why One Programmer Doesn’t Do It Anymore](https://lionfacelemonface.wordpress.com/2015/03/08/devops-is-bullshit-why-one-programmer-doesnt-do-it-anymore/) - just have all the devs do ops?

## 12-factor 
heroku / dokku
nodejs? https://github.com/snodgrass23/base12

## runtime vs build-time config
is this really as tricky as https://blog.jonrshar.pe/2020/Sep/19/spa-config.html
* https://medium.com/@balint_sera/environment-variables-in-single-page-apps-ce658b131216
* https://burnedikt.com/spa-runtime-configuration-for-multiple-environments-using-webpack/

webpack - defineplugin
