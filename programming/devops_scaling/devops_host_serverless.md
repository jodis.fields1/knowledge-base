Mixing general hosting & serverless concerns for the moment

## 2020-09
AWS Aurora
* doesn't work well w/ Postgres tho
* https://stackoverflow.com/questions/59399320/does-typeorm-work-with-aws-aurora-serverless
  * https://github.com/ArsenyYankovsky/typeorm-aurora-data-api-driver/issues/28 - fixed?
  * timestamp issue: https://forums.aws.amazon.com/thread.jspa?threadID=318094
* https://medium.com/safara-engineering/wiring-up-typeorm-with-serverless-5cc29a18824f

## custom image upload
NEED: upload custom image
    * AWS has it, maybe Vultr?
    * GCE: https://googlecloudplatform.uservoice.com/forums/302595-compute-engine/suggestions/10066821-add-import-export-ova
        * but see https://github.com/hashicorp/packer/issues/3587 | Feature Request: Create Google Compute Engine Images from scratch/import existing images · Issue #3587 · hashicorp/packer

## providers / hosts
AWS GCE Azure DigitalOcean etc

Packethost? 
    * https://twitter.com/grhmc/status/975927112585576448 | Graham Christensen on Twitter: "Looking away from the race-to-the-bottom hosting companies and towards @packethost

## serverless
[From Express.js to AWS Lambda: Migrating Existing Node Apps to Serverless](https://news.ycombinator.com/item?id=16714700)

### link dumps / explorations
#### 2018-05
https://en.m.wikipedia.org/wiki/Serverless_computing | 
https://read.acloud.guru/our-serverless-journey-part-2-908d76d03716 | How we migrated our startup to serverless – A Cloud Guru
https://serverless.com/community/champions/ | Serverless Champions

#### 2018-03
use openFaaS or kubeless?
https://github.com/kubeless/kubeless | kubeless/kubeless: Kubernetes Native Serverless Framework
https://github.com/iron-io/functions/issues/630 | Is this project still active? · Issue #630 · iron-io/functions
https://github.com/openfaas/faas/issues/212 | Document: Kong integration using Kubernetes · Issue #212 · openfaas/faas
https://www.infoworld.com/article/3218086/cloud-computing/serverless-computing-may-kill-google-cloud-platform.html | Serverless computing may kill Google Cloud Platform | InfoWorld
