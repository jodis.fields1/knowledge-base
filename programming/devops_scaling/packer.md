Packer
===

see dotfiles/projects/bencreasy.com

## cloud-init
somewhat popular, looked into this mid 2019:
* "User data scripts are executed via cloud-init, a daemon that can configure instances at creation/boot time independently from what is baked in to the image." per https://stackoverflow.com/questions/51144582/how-can-i-create-a-custom-linux-ami-with-packer-that-accepts-user-data
* "problem arises when cloud-init has not finished fully running on the source AMI by the time that packer starts any provisioning steps" https://packer.io/docs/other/debugging.html#issues-installing-ubuntu-packages mentioned at https://github.com/hashicorp/packer/pull/6930#issuecomment-509079491
* [cloud-init: Building clouds one Linux box at a time](https://www.youtube.com/watch?v=1joQfUZQcPg)
* https://superuser.com/questions/827977/use-cloud-init-with-virtualbox

## Gotchas / bugs
* when putting jenkins under VSC: noticed that uploading a file to a folder has owner of the uploading user (for docker, USER <user>) while uploading the file directly had <roo>
* file permissioner - be careful about permissions [file provisioner permission denied](https://github.com/hashicorp/packer/issues/1551) - file docs bug?
* "when creating Amazon EBS-backed AMIs, the (ssh_username)[https://www.packer.io/docs/builders/amazon-ebs.html#ssh_username] must match the AMI default username" - http://timclifford.io/post/packer-ssh_username/

### TO-INVESTIGATE
[linuxkit/linuxkit](https://github.com/linuxkit/linuxkit)
* [Difference between hashicorp packer vs linuxkit](https://stackoverflow.com/questions/47812633/difference-between-hashicorp-packer-vs-linuxkit)

[Use AWS profiles for credentials?](https://github.com/hashicorp/packer/issues/2471)

### boot command
https://stackoverflow.com/questions/31370750/how-do-i-find-out-the-boot-command-for-packer

### templates
https://github.com/law/minimal-xenial64
https://github.com/kaorimatz/packer-templates/blob/master/http/ubuntu/preseed.cfg
https://github.com/kaorimatz/packer-templates
https://github.com/s12v/xenial/blob/master/template.json
https://github.com/ChiperSoft/Packer-Vagrant-Example/tree/0eca73a859b258059d6af437b63e1585986ca098
https://lobster1234.github.io/2017/04/23/packer-your-AMIs-for-immutable-aws-deployments/

### auth ntp issue
this frustrated me for a good few hours, fix was restarting the computer as noted at:
[AWS authentication from environment variables is not working](https://github.com/hashicorp/packer/issues/2611#issuecomment-323939526)

the recommendation is something like: 
[ntpdate -u $(systemsetup -getnetworktimeserver|awk '{print $4}')](https://arstechnica.com/civis/viewtopic.php?p=1751756&sid=5fe39f3c0744363d624eacf100c3817d#p1751756)

other:
https://stackoverflow.com/questions/27433964/packer-amazon-ebs-authfailure?rq=1
https://stackoverflow.com/questions/41697434/what-is-the-correct-way-to-query-amazon-aws-ami-from-packer
https://github.com/hashicorp/packer/issues/3070
https://github.com/aws/aws-sdk-go/issues/579

## How it works
Pretty sure that if you're running from ISO, it will only use ssh authentication to run the provisioners after that ISO is done...