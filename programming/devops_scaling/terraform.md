https://stackoverflow.com/questions/39803182/terraform-use-case-to-create-multiple-almost-identical-copies-of-infrastructure

answer this: https://stackoverflow.com/questions/39102030/is-it-possible-to-trigger-packers-post-processor-skipping-the-build-step
## cloud terraform
ran into an issue w/ this before... make sure your folder has only text files, tries to upload everything
* https://www.terraform.io/docs/cloud/migrate/index.html
* https://learn.hashicorp.com/tutorials/terraform/cloud-migrate
## unorganized
others:
* https://medium.com/@skeller88/introducing-aws-terraform-bootstrap-a-starter-repo-to-deploy-a-python-app-on-top-of-aws-lambda-c39831fbae99

TO-READ:
https://heap.engineering/terraform-gotchas/
[A Comprehensive Guide to Terraform](https://blog.gruntwork.io/a-comprehensive-guide-to-terraform-b3d32832baca)

READ:
    * [Terraforming 1Password](https://blog.agilebits.com/2018/01/25/terraforming-1password/)
        * large system! 12 copies of a bunch of components
    * [Embracing Immutable Server Pattern Deployment on AWS](https://multithreaded.stitchfix.com/blog/2016/09/08/EmbracingImmutableServerPatternDeploymentonAWS/)
        * not terraform, but a case study

### best practices
https://github.com/hashicorp/best-practices/blob/master/terraform/providers/aws/README.md

### scripting
use json per https://zwischenzugs.com/2017/02/21/terraform-and-dynamic-environments/#comment-1492

### critical
https://charity.wtf/2016/02/23/two-weeks-with-terraform/
