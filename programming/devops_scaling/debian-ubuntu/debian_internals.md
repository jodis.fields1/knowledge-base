https://superuser.com/questions/10997/find-what-package-a-file-belongs-to-in-ubuntu-debian | Find what package a file belongs to in Ubuntu/Debian? - Super User

https://lists.samba.org/archive/samba-technical/2013-June/093501.html | What package do I need to install on Debian to get /usr/include/sys/acl.h?
https://askubuntu.com/questions/481/how-do-i-find-the-package-that-provides-a-file | How do I find the package that provides a file? - Ask Ubuntu

https://www.reddit.com/r/debian/comments/6lnakq/what_are_the_essential_packages/ | What are the Essential Packages? : debian

### debian infrastructure
#### packaging
see ./debian_apt.md

#### hosting
https://lists.debian.org/debian-devel/2017/05/msg00111.html | Re: Moving away from (unsupportable) FusionForge on Alioth?
https://lists.debian.org/debian-devel/2017/05/msg00110.html | Re: Moving away from (unsupportable) FusionForge on Alioth?
https://lists.debian.org/debian-devel/2017/05/msg00274.html | infinite number of Debian workflows (Re: Moving away from (unsupportable) FusionForge on Alioth?)
https://lists.debian.org/debian-devel/ | Debian Mailing Lists -- Index for debian-devel
https://pagure.io/pagure | Overview - pagure - Pagure
https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=829046 | #829046 - ITP: pagure -- A git centered forge - Debian Bug report logs

#### old feedback
https://anonscm.debian.org/cgit/

actually have to dive deep to get to something like https://alioth.debian.org/projects/d-i/
---
First off, I know I can find lots of cool stuff with Google, but that's not the point...

Is https://sources.debian.net/ linked anywhere from https://www.debian.org/, or even from a page linked by https://www.debian.org/? I couldn't even find it on the sitemap https://www.debian.org/sitemap#development

Altho I tried hard, I could not find a way to get to the source code from the https://www.debian.org/ even after navigating into various places (e.g., Developers Corner). Maybe I missed it.

There is the mention of the website source at the bottom ("Web site source code is available") which will eventually lead to https://alioth.debian.org/ buried in between a bunch of CVS instructions.

I'll admit I'm not very up on this stuff... I mainly spend time on Github. Would like to become involved but finding my way seems tricky.