Ubuntu launchpad
===

## launchpad
https://en.wikipedia.org/wiki/Launchpad_(website) | Launchpad (website) - Wikipedia

https://launchpad.net/ubuntu/+source/linux-meta/+bugs | Bugs : linux-meta package : Ubuntu
https://launchpad.net/chromium-browser | Chromium Browser in Launchpad
https://askubuntu.com/questions/979048/where-exactly-is-all-the-source-code-on-launchpad-browseable | packaging - Where exactly is all the source code on Launchpad browseable? - Ask Ubuntu

## staleness
conversations at: https://code.launchpad.net/~cjwatson/launchpad/opt-in-zopeless-immediate-mail/+merge/270383

### UI
[Where exactly is all the source code on Launchpad browseable?](https://askubuntu.com/questions/979048/where-exactly-is-all-the-source-code-on-launchpad-browseable) - ??
[click lp:launchpad from https://launchpad.net/launchpad to go tohttps://code.launchpad.net/~launchpad-pqm/launchpad/devel](https://code.launchpad.net/~launchpad-pqm/launchpad/devel)

#### meta packages and such??
"This page is for a packaging branch for a program in Ubuntu" - what does that really mean? over at
[linux-meta package](https://launchpad.net/ubuntu/+source/linux-meta/+bugs)

### git
https://help.launchpad.net/Code/Git | Code/Git - Launchpad Help

### messy bugs
[Bugs : Launchpad itself](https://bugs.launchpad.net/launchpad/+bugs?field.searchtext=&search=Search+Bug+Reports&field.scope=project&field.scope.target=launchpad) - 
[Bug #702524 “Package branch merges not automatically detected”](https://bugs.launchpad.net/launchpad/+bug/702524)

