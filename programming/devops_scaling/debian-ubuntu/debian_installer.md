[How To: Install Minimal KDE in Stretch](https://lists.debian.org/debian-user/2017/06/msg00854.html)

[ReduceDebian](https://wiki.debian.org/ReduceDebian)

### auto install
https://www.theurbanpenguin.com/auto-installing-ubuntu-16-04/ | Auto-Installing Ubuntu 16.04 - The Urban Penguin
https://wiki.debian.org/DebianInstaller | DebianInstaller - Debian Wiki
https://wiki.debian.org/DebianInstaller/Concepts | DebianInstaller/Concepts - Debian Wiki
https://thornelabs.net/2015/10/14/debian-ubuntu-preseed-documentation-and-working-examples.html | Debian/Ubuntu Preseed Documentation and Working Examples
https://askubuntu.com/questions/806820/how-do-i-create-a-completely-unattended-install-of-ubuntu-desktop-16-04-1-lts | preseed - How do I create a completely unattended install of Ubuntu Desktop 16.04.1 LTS? - Ask Ubuntu

### internals
http://hands.com/d-i/ | "Hands-off" Debian Installation
https://bugs.launchpad.net/ubuntu/+source/debian-installer/+bug/1505839 | Bug #1505839 “Unable to install from text mode interface” : Bugs : debian-installer package : Ubuntu
https://bugs.debian.org/cgi-bin/pkgreport.cgi?maint=debian-boot@lists.debian.org | Bugs in packages maintained by debian-boot@lists.debian.org -- Debian Bug report logs
https://www.debian.org/releases/stable/amd64/apbs03.html.en | B.3. Creating a preconfiguration file
https://alioth.debian.org/projects/d-i/ | Alioth: debian-installer: Project Home

#### core
where is the initial entry point?

* "The Debian installer is highly modular, and modules are key to its design.
Details about modules (udebs) are explained in modules.txt..."
  * per https://salsa.debian.org/installer-team/debian-installer/blob/master/doc/devel/design.txt
* https://salsa.debian.org/installer-team/d-i/tree/master/scripts
* https://salsa.debian.org/installer-team/main-menu
* https://salsa.debian.org/installer-team/base-installer
* https://salsa.debian.org/installer-team/cdebconf
  * per https://salsa.debian.org/installer-team/cdebconf/blob/master/doc/design.txt this is a rewrite of debconf in C?
    * how does this compare to https://salsa.debian.org/pkg-debconf/debconf which is 95% perl