Not impressed by this.

## recipes or tips
https://askubuntu.com/questions/tagged/apt
* [How to list all installed packages](https://askubuntu.com/questions/17823/how-to-list-all-installed-packages)
    * still no clear good answer, I filed this a while back as a bug
* [Why use apt-get upgrade instead of apt-get dist-upgrade?](https://askubuntu.com/questions/194651/why-use-apt-get-upgrade-instead-of-apt-get-dist-upgrade)
    * basically neither are good!? one has higher risk of breakages, other is incorrect

### maintainers
https://blog.jak-linux.org/ - Julian Andres Klode

### example
took a closer look at wine with https://salsa.debian.org/wine-team/wine

seems that the "upstream" is routinely merged into the master branch, which has another "debian" folder
* interesting technique - alternative to how I do submodules

### packaging
* [Quick Debian development guide](https://anarc.at/software/debian-development/)

https://www.debian.org/doc/manuals/debian-faq/ch-pkg_basics.en.html | The Debian GNU/Linux FAQ - Basics of the Debian package management system

https://github.com/bafu/debconf-concepts | bafu/debconf-concepts: Study the concepts behind Debconf.
https://feeding.cloud.geek.nz/posts/manipulating-debconf-settings-on/ | Manipulating debconf settings on the command line
