* see also bash_recipes.md
* see also dotfiles/docker-alias.md

pulling local images from docker: https://github.com/docker/compose/issues/3660#issuecomment-786484180

https://serverfault.com/questions/750868/how-to-decide-between-a-docker-volume-container-and-a-docker-volume

### ssh server
* in jenkins project, ran an ssh server but [~jpetazzo/If you run SSHD in your Docker containers, you're doing it wrong!](https://jpetazzo.github.io/2014/06/23/docker-ssh-considered-evil/) says you should not


### recurring
https://medium.com/@oprearocks/how-to-properly-override-the-entrypoint-using-docker-run-2e081e5feb9d

### multiple processes
* [Can I run multiple programs in a Docker container?](https://stackoverflow.com/questions/19948149/can-i-run-multiple-programs-in-a-docker-container)
    * refers to https://docs.docker.com/config/containers/multi-service_container/
        * somehow I have put "service docker start; service docker start; bash" at the end in my packer wrapper script

#### docker in docker
* https://github.com/jpetazzo/dind
    * found from https://stackoverflow.com/questions/49799563/running-docker-on-ubuntu-docker-image
* switching to vfs worked for me https://stackoverflow.com/questions/30984569/error-error-creating-aufs-mount-to-when-building-dockerfile/45511060#comment91009869_51559230


#### avoiding root
prompted by Cannot connect to the Docker daemon. Is 'docker -d' running on this host?:

`sudo usermod -aG docker $USER` per https://stackoverflow.com/questions/48957195/how-to-fix-docker-got-permission-denied-issue

#### force to stay up
~~sometimes you want to just force the container to keep running -> use `sleep infinity` or `while true; do sleep 1; done` per https://stackoverflow.com/questions/42218957/dockerfile-cmd-instruction-will-exit-the-container-just-after-running-it~~
    * DO NOT DO THIS as it causes 100% cpu
    * use tty instead per https://stackoverflow.com/a/36872226/4200039
    * also consider `tail -f /dev/null` suggestion
        * from http://kimh.github.io/blog/en/docker/gotchas-in-writing-dockerfile-en/

[How to properly override the ENTRYPOINT using docker run](https://medium.com/@oprearocks/how-to-properly-override-the-entrypoint-using-docker-run-2e081e5feb9d)
    * needed this in jenkins project, forget the detail

[Assign static IP to Docker container](https://stackoverflow.com/questions/27937185/assign-static-ip-to-docker-container)

[How to get a Docker container's IP address from the host?](https://stackoverflow.com/questions/17157721/how-to-get-a-docker-containers-ip-address-from-the-host/26984302#26984302)

## random
[What is /usr/sbin/service doing under a Docker container](https://stackoverflow.com/questions/38382388/what-is-usr-sbin-service-doing-under-a-docker-container)
    * just a script looking in /etc/init.d

## docker daemon
Something weird going on with the docker daemon?
    * lots of options at https://docs.docker.com/engine/reference/commandline/dockerd/
    * haven't had to use many of them at all, but keep --debug in mind

https://github.com/moby/moby/issues/27102 | Docker Daemon command `dockerd` not found on latest stable Docker for Mac and Docker Toolbox · Issue #27102 · moby/moby
https://forums.docker.com/t/how-to-fix-the-error-docker-daemon-is-not-supported-on-darwin-please-run-dockerd-directly/19554/5 | How to fix the error "`docker daemon` is not supported on Darwin. Please run `dockerd` directly" - Docker for Mac - Docker Forums
https://github.com/docker/for-mac/issues/2267 | docker daemon - Not able to run in Mac · Issue #2267 · docker/for-mac
https://stackoverflow.com/questions/21871479/docker-cant-connect-to-docker-daemon | linux - Docker can't connect to docker daemon - Stack Overflow
https://docs.docker.com/config/daemon/ | Configure and troubleshoot the Docker daemon | Docker Documentation
https://github.com/moby/moby/issues/20693 | Move .dockercfg to $XDG_CONFIG_HOME (a.k.a. ~/.config) · Issue #20693 · moby/moby
https://github.com/EugenMayer/docker-sync/wiki/2.3-daemon-mode | 2.3 daemon mode · EugenMayer/docker-sync Wiki

## General
Docker Repose
See docker-exploration repo
http://security.stackexchange.com/questions/70827/passing-secret-keys-securely-to-docker-containers

### Backups
docker run --rm --volumes-from brightidea_sqlsrv_1 -v $(pwd):/backup --net brightidea_default -it ubuntu tar cvf /backup/backup.tar /var/opt/mssql/data 
docker cp - brightidea_sqlsrv_1:. <backup2.tar

[Unexpected results from Docker Benchmarking. Need your input.](https://www.reddit.com/r/docker/comments/8wls2a/unexpected_results_from_docker_benchmarking_need/)
    * performance lag...

### hub
#### old tags
OPEN: [Error response from daemon: cannot stop container #254](https://github.com/docker/for-linux/issues/254)
    * ran into this 2018-03

[Docker Official Images](https://github.com/docker-library/official-images) - "removed from a library file do not get removed from the Docker Hub, so that old versions can continue to be available for use"

### networking
Expose and publish
http://stackoverflow.com/questions/22111060/difference-between-expose-and-publish-in-docker

http://stackoverflow.com/questions/30545023/how-to-communicate-between-docker-containers-via-hostname

http://www.dedoimedo.com/computers/docker-networking.html

#### access host from container
Mac workarounds for issues: https://docs.docker.com/docker-for-mac/networking/#per-container-ip-addressing-is-not-possible 

[From inside of a Docker container, how do I connect to the localhost of the machine?](`https://stackoverflow.com/questions/24319662/from-inside-of-a-docker-container-how-do-i-connect-to-the-localhost-of-the-mach`)

there's also https://forums.docker.com/t/using-localhost-for-to-access-running-container/3148/4 which discusses hitting the NAT gateway of the vm at 10.0.2.15

#### Volumes
https://github.com/fntlnz/doenter - enter mac vm
from there volumes are at:
/var/lib/docker/volumes - inspect with tree or exa

#### docker VM
##### Alpine
apk update
apk add --update curl

https://forums.docker.com/t/docker-apk-package-for-alpine-linux-has-an-unresolved-dependency-to-libseccomp/9604/2 - ??
