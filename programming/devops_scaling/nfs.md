2019-01: hit this periodically; most recently when using inthe.am vagrant; culminated in https://github.com/hashicorp/vagrant/issues/10609

## try nfs
See linux/network_share.md

https://medium.com/@sean.handley/how-to-set-up-docker-for-mac-with-native-nfs-145151458adc | Set Up Docker For Mac with Native NFS – Sean Handley – Medium
https://en.m.wikipedia.org/wiki/Network_File_System | Network File System - Wikipedia
https://unix.stackexchange.com/questions/45899/why-is-linux-nfs-server-implemented-in-the-kernel-as-opposed-to-userspace | Why is Linux NFS server implemented in the kernel as opposed to userspace? - Unix & Linux Stack Exchange
https://willhaley.com/blog/mount-nfs-share-on-a-mac/ | Mount an NFS Share on a Mac using the Terminal - Will Haley

### intro
http://www.troubleshooters.com/linux/nfs.htm
https://www.systutorials.com/241630/how-to-allow-root-access-to-nfs/
https://www.ev3dev.org/docs/tutorials/setting-up-an-nfs-file-share/ | Setting Up an NFS File Share
https://www.tecmint.com/how-to-setup-nfs-server-in-linux/ | How to Setup NFS (Network File System)

focused on nfsv3 - for v4 check out something like https://www.linuxjournal.com/content/encrypting-nfsv4-stunnel-tls; on mac [nfs.client.mount.options = vers=4 to /etc/nfs.conf ](https://www.reddit.com/r/applehelp/comments/63429c/nfsv4_osx_mounting_freebsd_nfsv4_share/)

## linux server
#### authoritative
http://nfs.sourceforge.net/ | Linux NFS faq
https://linux.die.net/man/8/exportfs | exportfs(8) - Linux man page

https://serverfault.com/questions/377170/which-ports-do-i-need-to-open-in-the-firewall-to-use-nfs

### linux mounting
mounting from linux, might need a package: `sudo apt install nfs-kernel-server` per https://github.com/hashicorp/vagrant/issues/9666#issuecomment-426069146

unmount: https://askubuntu.com/questions/292043/how-to-unmount-nfs-when-server-is-gone

#### userspace
https://packages.debian.org/sid/nfs-common

### bsd
https://unix.stackexchange.com/questions/49224/how-do-i-export-a-folder-to-a-subnet-using-nfs-in-freebsd-9-0

### mac mounting
mounting from mac: needs resvport and maybe insecure per https://superuser.com/questions/254339/how-to-mount-nfs-export-on-mac-os-x; also see https://apple.stackexchange.com/questions/142697/why-does-mounting-an-nfs-share-from-linux-require-the-use-of-a-privileged-port

might need to open firewall https://serverfault.com/questions/377170/which-ports-do-i-need-to-open-in-the-firewall-to-use-nfs

[Problem with NFS Shared Folder Permissions on Mac OS X (El Capitan)](https://github.com/hashicorp/vagrant/issues/6360)

on mac, "The owner (502) / group (dialout) of files & directories within the synced folders is normal for a vagrant guest on a OS X host" per https://stackoverflow.com/questions/24862708/php-creates-empty-session-in-nfs-shared-directory

on mac, potentially changing the permissions on the host? staff versus wheel groups? per https://github.com/hashicorp/vagrant/issues/8061#issuecomment-267004646

[Vagrant permissions](https://serverfault.com/questions/487862/vagrant-os-x-host-nfs-share-permissions-error-failed-to-set-owner-to-1000#564842)

### dinghy mapping
https://github.com/codekitchen/dinghy/issues/15#issuecomment-86232623 | Files owned by unknown user in container · Issue #15 · codekitchen/dinghy
* he ended up figuring out a workaround


### link dump
https://github.com/hashicorp/vagrant/pull/1029 | Add ability to specify arbitrary NFS mount options per share by dansimau · Pull Request #1029 · hashicorp/vagrant
https://github.com/hashicorp/vagrant/issues/6513 | synced_folder + NFS + using each results in wrong nfs mounts · Issue #6513 · hashicorp/vagrant
https://github.com/hashicorp/vagrant/issues/6360 | Problem with NFS Shared Folder Permissions on Mac OS X (El Capitan) · Issue #6360 · hashicorp/vagrant
https://stackoverflow.com/questions/39045997/cannot-change-file-ownership-from-vagrant-vm | linux - Cannot change file ownership from Vagrant VM - Stack Overflow

### bindfs
https://github.com/gael-ian/vagrant-bindfs

plugin workaround...

https://stackoverflow.com/questions/28755845/npm-install-within-vagrant-box | node.js - npm install within Vagrant box - Stack Overflow
* bindfs options https://github.com/puphpet/puphpet/pull/1403

#### TODO answer stackoverflow questions
https://stackoverflow.com/questions/42021773/permission-issue-with-vagrant-501-dialout