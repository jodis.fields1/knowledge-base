
## 2018-09 docker alternatives
prompted by: https://news.ycombinator.com/item?id=17804916 | Docker cannot be downloaded without logging into Docker Store | Hacker News

https://medium.com/@adriaandejonge/moving-from-docker-to-rkt-310dc9aec938 | Moving from Docker to rkt – Adriaan de Jonge – Medium
http://cri-o.io/ | cri-o
https://news.ycombinator.com/item?id=17909594 | Show HN: Magic Sandbox – learn Kubernetes on real infrastructure | Hacker News
