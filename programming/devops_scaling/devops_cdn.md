
[Static assets in an eventually consistent webapp deployment](https://web.archive.org/web/20170831073359/https://rea.tech/static-assets-in-an-eventually-consistent-webapp-deployment/) - good tips

versioning, cloudfront, etc

Caching issues!

[How to force update Single Page Application (SPA) pages?](https://stackoverflow.com/questions/34388614/how-to-force-update-single-page-application-spa-pages) - must ask user to reload their page to trigger busting cache
    * https://stackoverflow.com/a/34394927/4200039 proper answer, `window.location.reload(true)`


## suggested best practice
"The only fool-proof approach is a "never delete old assets" approach... or at least a "only delete old assets... after some time has passed). This is pretty standard if you're using a CDN - e.g. CloudFront I believe keeps your old assets for 24 hours.

In summary:
A) Best Solution: Don't delete old assets, either by removing cleanupOutputBeforeBuild() or using a CDN that stores your old asset URLs for a little while
B) Great Solution: Use a decent deploy (e.g. the Capistrano philosophy) where you completely finish deploying and building your code before new traffic hits things. You shouldn't see any 404's, except in edge-cases (that Stof mentioned).
C) Not Great Solution If you have a deploy that continually serves traffic and deletes the old assets (e.g. an Rsync solution), your assets will 404 for a little while during deploy. Either make your deploy process better or use a different versioning method (like what Stuf suggested)

There's nothing to do in Encore, but we could add a small note about this in the versioning section."
    * https://github.com/symfony/webpack-encore/issues/6#issuecomment-308120263

## brightidea issue
https://github.com/brightideainc/main/pull/5954#issuecomment-346465233

So the way the the CDN cache works is that we create an endpoint in the application with the CDN's domain (bi.cloudfront.net) and instead of our domain (company.brightidea.com) along with the rest of the path.

Cloudfront servers receive this request and then check their storage for the content of the entire endpoint (/fractal/something?some-number.)

If it exists, then that is served to the browser and the user receives our lovely javascript without hitting our servers.

If it does not exist, cloudfront requests the content from our server from a domain that we configured. So they know that bi.cloudfront.net should get content from na5.brightidea.com. So then we get a request for na5.brightidea.com/fractal/something?some-number. This isn't a customer's subdomain, but it doesn't matter because we're just serving static files and no php script is executed.

When our load balancer receives a request, it selects one web server (of many) to forward the request to. That server finds the file in its local storage and serves it.

So unless we change the filename or path of an endpoint, cloudfront will continue serving their cached version which could be out of date. This is why we append the file modified time to bust the cache.

Now for when this issue starts!

When we deploy, the process loops through all of the web servers sequentially and copies the new files. This could take up to a few minutes per server.

At some point, server 1 could be serving up new code while server 8 is still serving up old code at the same time.

When someone requests a page at this point, it could load the view idea page from server 1. Since this server has the latest code, the file modified time is at the latest, so it request a new version of the javascript from cloudfront, which cloudfront doesn't have yet.

Cloudfront will then make a request to us for this new version of the code and the load balancer will just happen to pick server 8 which returns the old version and cloudfront then caches it. So then all subsequent requests for the new version will result in the old code even after the deploy fully completes.

Sometimes this doesn't matter, sometimes it just results in an older version of a widget loading, and sometimes if the venders.js is mismatched with the widget js, there will be an error and the widget will not load at all.

So our solution to this is two fold. First we updated the deploy process to copy all of the files EXCEPT for /protected/config/release.json which is updated at build time with the SHA1 of the HEAD commit of the deployed branch. When the deploy completes, only this file is then updated in all of the servers.

The second part is to update the paths of all cloudfront endpoints in our code to include this build value. This will force cloudfront to get a new version after the deploy finally completes and all servers are up to date.

Hope this makes sense.