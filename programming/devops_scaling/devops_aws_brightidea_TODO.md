see also config/aws/MY_README files

[AWS interview preparation](https://www.reddit.com/r/devops/comments/8mcm1a/aws_interview_preparation/)

### general
https://news.ycombinator.com/item?id=15919789 | Introducing Amazon Linux 2 | Hacker News
https://github.com/open-guides/og-aws/issues/290 | A bit more on Amazon Linux compatibility etc. · Issue #290 · open-guides/og-aws

### high-resolution monitoring
https://encrypted.google.com/search?hl=en&q=Time%20to%20First%20Byte%20and%20AWS%20response%20time | Time to First Byte and AWS response time - Google Search
https://aws.amazon.com/blogs/aws/new-high-resolution-custom-metrics-and-alarms-for-amazon-cloudwatch/ | New – High-Resolution Custom Metrics and Alarms for Amazon CloudWatch | AWS News Blog
https://github.com/awslabs/collectd-cloudwatch | awslabs/collectd-cloudwatch: A collectd plugin for sending data to Amazon CloudWatch
https://docs.aws.amazon.com/AmazonCloudWatch/latest/monitoring/publishingMetrics.html | Publish Custom Metrics - Amazon CloudWatch
https://aws.amazon.com/about-aws/whats-new/2017/07/amazon-cloudwatch-introduces-high-resolution-custom-metrics-and-alarms/ | Amazon CloudWatch introduces High-Resolution Custom Metrics and Alarms

### memory monitoring
https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/mon-scripts.html | Monitoring Memory and Disk Metrics for Amazon EC2 Linux Instances - Amazon Elastic Compute Cloud
https://docs.aws.amazon.com/AmazonCloudWatch/latest/monitoring/install-CloudWatch-Agent-on-first-instance.html | Getting Started: Installing the CloudWatch Agent on Your First Instance - Amazon CloudWatch

### negative
https://serverfault.com/questions/275736/amazon-linux-vs-ubuntu-for-amazon-ec2 | Amazon Linux vs. Ubuntu for Amazon EC2 - Server Fault

### docs
https://aws.amazon.com/amazon-linux-ami/2017.09-packages/ | Amazon Linux AMI 2017.09 Packages

https://signin.aws.amazon.com/oauth?response_type=code&client_id=arn%3Aaws%3Aiam%3A%3A540806263933%3Auser%2FAWSForums-Auth&redirect_uri=https%3A%2F%2Fforums.aws.amazon.com%2Fthread.jspa%3FthreadID%3D146805%26isauthcode%3Dtrue&forceMobileLayout=0&forceMobileApp=0 | Amazon Web Services Sign-In
https://aws.amazon.com/amazon-linux-ami/faqs/ | FAQs
https://aws.amazon.com/blogs/aws/new-amazon-linux-container-image-for-cloud-and-on-premises-workloads/ | New Amazon Linux Container Image for Cloud and On-Premises Workloads | AWS News Blog
