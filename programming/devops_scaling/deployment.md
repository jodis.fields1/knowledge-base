Deployment
===

https://12factor.net/

### dokku
TODO: move to my dokku-group repo
uses plugn which uses go and basher, looks like these `bashenv` files prolly come from https://github.com/progrium/go-basher/blob/master/basher.go#L192)
-----> Creating http nginx.conf
-----> Running nginx-pre-reload
/tmp/bashenv.411217876: line 142: syntax error near unexpected token `('
/tmp/bashenv.411217876: line 142: `	eval "$1() { '
/home/dokku/.basher/bash: main: command not found

### serverless
[How we built Hamiltix.net for less than $1 a month on AWS](https://news.ycombinator.com/item?id=16497447) - 

## netlify
also see gatsby

2019-08: created account, messed around:
* scanned https://www.netlify.com/docs and https://www.netlify.com/products
* CLI: https://www.netlify.com/docs/cli/#continuous-deployment
* need to do https://www.netlify.com/docs/custom-domains
* functions playground : https://functions.netlify.com 
* forms handling: https://www.netlify.com/blog/2017/09/19/form-handling-with-the-jamstack-and-netlify
* webhooks: https://www.netlify.com/docs/webhooks

### General tools
http://www.skippbox.com/using-your-docker-compose-files-with-kubernetes/
https://github.com/visionmedia/deploy
https://stackoverflow.com/questions/11088892/is-there-a-deployment-tool-similar-to-fabric-written-in-javascript
http://mikeeverhart.net/2016/01/deploy-code-to-remote-servers-with-gulp-js/
https://johnmunsch.com/2015/03/08/shipit-vs-flightplan-for-automated-administration/
https://www.npmjs.com/package/nginx-conf
https://www.blazemeter.com/blog/jenkins-vs-other-open-source-continuous-integration-servers
https://stackoverflow.com/questions/9184959/rewriting-nginx-for-pushstate-urls
https://stackoverflow.com/questions/22944631/how-to-get-the-ip-address-of-the-docker-host-from-inside-a-docker-container
http://www.informationweek.com/cloud/platform-as-a-service/cloud-native-what-it-means-why-it-matters/d/d-id/1321539



### roll your own virtualization
Proxmox - seems that it's mostly a hobbyist thing per https://www.reddit.com/r/sysadmin/comments/3nhhe5/does_anyone_use_proxmox_in_a_serious_production/