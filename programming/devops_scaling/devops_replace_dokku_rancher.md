Conclusion: seems oriented towards GUI

## 2018-07
"Rancher doesn't replace your "native" orchestrator. You can still use Kubernetes to manage your applications from the dashboard or the command line"
    - https://andrewlock.net/home-home-on-the-range-installing-kubernetes-using-rancher-2-0/

Not sure how to add a cluster without GUI: https://medium.com/@jcrben/maybe-i-missed-it-but-how-can-we-avoid-using-a-gui-b5168bdf3645

Poor docs for CLI: https://github.com/rancher/cli#usage

"They are often the most cost-effective way to run Kubernetes because cloud providers typically don’t charge for the resources required to run the Kubernetes management plane. This leads to significant savings for smaller clusters as it takes at least 3 nodes to run a production-grade etcd database and Kubernetes masters."
    - https://rancher.com/what-is-rancher/how-is-rancher-different/

[$15 Production Kubernetes Cluster on DigitalOcean](https://news.ycombinator.com/item?id=13006296)
    * much more complicated than I'd like
    * someone comments about using rancher with OVH https://www.ovh.com/world/vps/vps-ssd.xml

"I thought the same as well, but recently I've been having a decent go at it with RancherOS"
    - https://news.ycombinator.com/item?id=15273857
