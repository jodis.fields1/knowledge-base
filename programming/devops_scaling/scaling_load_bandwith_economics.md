https://www.google.com/search?hl=en&ei=6_6iW6H2D_X19AOQxZOYAw&q=advertising+revenue+per+hour&oq=advertising+revenue+per+hour&gs_l=psy-ab.3..0i22i30.7339.10434..10555...0.0..0.183.1284.10j4......0....1..gws-wiz.......0j0i71.T-jptfxwbhI | advertising revenue per hour - Google Search

## bandwith overview

https://www.nearlyfreespeech.net/services/hosting | Web Sites - NearlyFreeSpeech.NET

https://www.quora.com/What-are-some-typical-ad-revenues-per-person-per-hour-of-television-watched-for-different-categories-of-shows | What are some typical ad revenues per person per hour of television watched for different categories of shows? - Quora

https://www.digitalocean.com/community/questions/i-need-to-upload-10tb-of-data-will-this-cost-me | I need to upload 10TB of data, will this cost me? | DigitalOcean

https://www.digitalocean.com/community/questions/how-much-bandwidth-do-i-get | How much Bandwidth do I get? | DigitalOcean

https://blog.digitalocean.com/bandwidth-pricing-introduced/ | Bandwidth Pricing Introduced

https://digitalocean.uservoice.com/forums/136585-digitalocean/suggestions/32978362-never-charge-for-bandwidth | Never charge for bandwidth – Customer Feedback for DigitalOcean

https://www.lowendtalk.com/discussion/109287/is-digitalocean-charging-for-extra-bandwidth | Is DigitalOcean charging for extra bandwidth? — LowEndTalk

https://www.digitalocean.com/community/questions/questions-about-bandwidth-transfer-limits-and-billing | Questions about bandwidth, transfer limits and billing. | DigitalOcean
