See cheat/mac_user_groups

2018-02: ran into an issue when trying to use jenkins docker image on mac
    * error message: `Can not write to /var/jenkins_home/copy_reference_file.log. Wrong volume permissions?`
    * don't really understand how I fixed it
    * requested tips at https://github.com/jenkinsci/docker/issues/493#issuecomment-406657809

## docker stuff
https://www.jujens.eu/posts/en/2017/Jul/02/docker-userns-remap/ | Use Linux user namespaces to fix permissions in docker volumes
https://denibertovic.com/posts/handling-permissions-with-docker-volumes/ | Deni Bertovic :: Handling Permissions with Docker Volumes
use gosu
https://serversforhackers.com/c/dckr-file-permissions | Docker & File Permissions | Servers for Hackers
https://www.reddit.com/r/docker/comments/46ec3t/volume_permissions_best_practices/ | Volume permissions best practices? : docker

## generic file permissions
https://wiki.archlinux.org/index.php/File_permissions_and_attributes | File permissions and attributes - ArchWiki
https://unix.stackexchange.com/questions/160527/is-there-a-way-to-prevent-git-from-changing-permissions-and-ownership-on-pull | Is there a way to prevent git from changing permissions and ownership on pull? - Unix & Linux Stack Exchange
https://github.com/jenkinsci/docker/pull/225 | Use gosu to fix volume permissions at startup by carlossg · Pull Request #225 · jenkinsci/docker

## generic macos
https://superuser.com/questions/592921/mac-osx-users-vs-dscl-command-to-list-user/621055 | macos - Mac OSX: users vs dscl command to list user - Super User
https://apple.stackexchange.com/questions/29874/how-can-i-list-all-user-accounts-in-the-terminal | macos - How can I list all user accounts in the terminal? - Ask Different
https://superuser.com/questions/202814/what-is-an-equivalent-of-the-adduser-command-on-mac-os-x | macos - What is an equivalent of the adduser command on Mac OS X? - Super User

## jenkins github complaints
https://stackoverflow.com/questions/40007657/os-x-how-to-give-permissions-to-jenkins-user-to-run-docker-command | OS X - how to give permissions to jenkins user to run docker command - Stack Overflow
https://github.com/jenkinsci/docker/issues/154 | Do not use user ID 1000 for jenkins · Issue #154 · jenkinsci/docker

https://github.com/jenkinsci/docker/issues/177#issuecomment-241997721 | Permission denied - /var/jenkins_home/copy_reference_file.log · Issue #177 · jenkinsci/docker
https://github.com/jenkinsci/docker/issues/493 | touch: cannot touch ‘/var/jenkins_home/copy_reference_file.log’: Permission denied · Issue #493 · jenkinsci/docker
https://github.com/jenkinsci/docker/issues/112#issuecomment-227080794 | uid 1000 is a horrible choice for the jenkins user · Issue #112 · jenkinsci/docker
https://github.com/jenkinsci/docker/issues/277 | Another /var/jenkins_home/copy_reference_file.log: Permission denied · Issue #277 · jenkinsci/docker