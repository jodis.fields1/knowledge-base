https://github.com/wg/wrk

[Gracefully Scaling to 10k PostgreSQL Connections for $35/mo, Part Two](https://news.ycombinator.com/item?id=17370062)

### microservices
https://fernandoabcampos.wordpress.com/2016/02/04/microservice-architecture-step-by-step-tutorial/
http://microservices.io/articles/scalecube.html
https://thenewstack.io/microservices-calls-robust-api-management-tools/
http://microservices.io/patterns/microservices.html
http://www.vinaysahni.com/best-practices-for-building-a-microservice-architecture
http://callistaenterprise.se/blogg/teknik/2015/03/25/an-operations-model-for-microservices/