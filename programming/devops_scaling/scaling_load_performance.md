https://en.wikipedia.org/wiki/Web_server_benchmarking

http://highscalability.com/all-time-favorites/

[How do microservice system architectures avoid network bottlenecks?](https://softwareengineering.stackexchange.com/questions/275734/how-do-microservice-system-architectures-avoid-network-bottlenecks)

### Metrics
Time To First Byte (TTFB) - https://www.serverdensity.com/guides/ttfb/

requests per second

### Load testing
https://github.com/wg/wrk

#### Measurement tools
* [Node.js-based alternative to jMeter](https://github.com/nodejs/benchmarking/issues/30)
* [Which gets the measurements right, JMeter or Apache ab?](https://stackoverflow.com/questions/10260526/which-gets-the-measurements-right-jmeter-or-apache-ab)

##### general network performance
[How to check Internet Speed via Terminal?](https://askubuntu.com/questions/104755/how-to-check-internet-speed-via-terminal) - used iperf3 - remember that you have to run iperf3 -s on the server that you hit!!

### Browser performance profiling
https://github.com/paulirish/automated-chrome-profiling

https://stackoverflow.com/questions/21780391/how-to-approach-end-client-performance-testing-on-single-page-web-applicatio

flame graphs: http://www.brendangregg.com/FlameGraphs/cpuflamegraphs.html

#### Read / write performance generally
##### time (unix)
https://stackoverflow.com/questions/556405/what-do-real-user-and-sys-mean-in-the-output-of-time1?rq=1
https://stackoverflow.com/questions/37382402/comparing-two-different-processes-based-on-the-real-user-and-sys-times

#### 2017 web link dump
https://github.com/micmro/performance-bookmarklet
https://developers.google.com/web/tools/chrome-devtools/evaluate-performance/timeline-tool
https://stackoverflow.com/questions/31240207/what-does-chrome-network-timings-really-mean-and-what-does-affects-each-timing-l
https://developers.google.com/web/tools/chrome-devtools/rendering-tools/
https://stackoverflow.com/questions/28580845/how-can-i-reduce-the-waiting-ttfb-time
https://developers.google.com/web/tools/chrome-devtools/network-performance/understanding-resource-timing
https://github.com/kdzwinel/betwixt


### global benchmarks
https://www.akamai.com/us/en/solutions/intelligent-platform/visualizing-akamai/