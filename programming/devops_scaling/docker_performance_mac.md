
Try delegated etc https://docs.docker.com/docker-for-mac/osxfs-caching/



## docker performance
raw only applies inside performance
    [Slow disk performance with `raw` format on 17.12.0-ce-mac46](https://github.com/docker/for-mac/issues/2395)
TODO:
    * http://markshust.com/2018/01/30/performance-tuning-docker-mac | Performance Tuning Docker for Mac | Mark Shust
    * https://medium.com/@TomKeur/how-get-better-disk-performance-in-docker-for-mac-2ba1244b5b70 | How to get a better disk performance in Docker for Mac
    * 
takeaways:
    * use 

### docker-sync exploration
[How can I set container permissions for volumes to non-root on native_osx?](https://github.com/EugenMayer/docker-sync/issues/434)
[Add visual feedback or a message stating that first sync can take some minutes to complete](https://github.com/EugenMayer/docker-sync/issues/568)


### bugs
* [File system performance improvements](https://github.com/docker/for-mac/issues/1592#issuecomment-377012888)
    * main bug
* [Severe Docker 1.12.1 performance regression with DB2 images (~10x slower)](https://github.com/docker/for-mac/issues/668)
    * big bug, not completely fixed?
* [File access in mounted volumes extremely slow](https://github.com/docker/for-mac/issues/77#issuecomment-299510932)
    * old parent issue

### daemon config
~/Library/Containers/com.docker.docker/Data/database/com.docker.driver.amd64-linux/etc/docker/daemon.json
    * per https://github.com/docker/docker.github.io/issues/2643#issue-219739364

* set 

### advice
https://docs.docker.com/docker-for-mac/osxfs-caching/#semantics | Performance tuning for volume mounts (shared filesystems) | Docker Documentation

### docker-sync
TODO: add vendor and node_modules to exclude
use root uid? 0
`find . -type f -exec chmod 666 {} \;`
https://github.com/EugenMayer/docker-sync/issues/39 | Rework configuration reference to explain each option · Issue #39 · EugenMayer/docker-sync

### link dump
https://github.com/docker/for-mac/issues/2395#issuecomment-356653982 | Slow disk performance with `raw` format on 17.12.0-ce-mac46 · Issue #2395 · docker/for-mac
https://github.com/docker/for-mac/issues/77#issuecomment-299510932 | https://github.com/docker/for-mac/issues/77#issuecomment-299510932
https://github.com/docker/for-mac/issues/668 | 
https://github.com/docker/docker.github.io/issues/2992 | Improve docs for `nocopy` behavior for volumes · Issue #2992 · docker/docker.github.io
