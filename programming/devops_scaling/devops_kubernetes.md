https://www.docker.com/blog/simplifying-kubernetes-with-docker-compose-and-friends/

[Kubernetes Is a Surprisingly Affordable Platform for Personal Projects](https://news.ycombinator.com/item?id=18111665)
https://github.com/hobby-kube/guide - just what I need!

[Convergence to Kubernetes](https://medium.com/@pingles/convergence-to-kubernetes-137ffa7ea2bc) 

https://www.reddit.com/r/kubernetes/comments/9ajm1p/kubernetes_cluster_on_aws_for_as_little_as_3_a/ | Kubernetes cluster on AWS for as little as $3 a month : kubernetes
https://www.reddit.com/r/aws/comments/8gsuvi/pointing_a_domain_name_at_an_application_load/ | pointing a domain name at an application load balancer (ALB) : aws
https://www.reddit.com/r/devops/comments/as82lp/ive_built_a_platform_for_trying_kubernetes_for/ | I've built a platform for trying Kubernetes for free, for learning or hobby hosting! Would love feedback! : devops


## replace dokku
https://github.com/hasura/gitkube
or 
rancher 


## single node
https://ahmet.im/blog/minikube-on-gke/
https://github.com/datawire/pib
https://github.com/kelseyhightower/kubeadm-single-node-cluster
    * https://github.com/codefresh-io/k8s-single-node-installer

https://vsupalov.com/getting-started-with-kubernetes/

## high-level
https://www.digitalocean.com/community/tutorials/an-introduction-to-kubernetes
    * still confused by this sometimes - PaaS features versus just orchestration?
        * [What does Kubernetes actually do and why use it?](http://www.developintelligence.com/blog/2017/02/kubernetes-actually-use/) is good

## docs exploration
2018-04-14: went thru [foundational app developer journey](https://kubernetes.io/docs/user-journeys/users/application-developer/foundational/)

read [My Notes about a Production-grade Ruby on Rails Deployment on Google Cloud Kubernetes Engine](http://www.akitaonrails.com/2018/01/09/my-notes-about-a-production-grade-ruby-on-rails-deployment-on-google-cloud-kubernetes-engine)

## high-availability
[Kubernetes High Availability: No Single Point of Failure](https://thenewstack.io/kubernetes-high-availability-no-single-point-of-failure/)
    * sounds like there are plenty of gaps and expenses
    * [Load Balancing in Kubernetes (2016)](http://www.devoperandi.com/load-balancing-in-kubernetes/) is outdated and prolly 

## TODO
https://github.com/kubernetes/charts

## local
https://github.com/kubernetes/minikube - work locally

## openshift
openShift recommended by slant.co https://github.com/openshift/origin
    * porcelain over kubernetes?

## bootstrapping with kops or kubeadm
comparison:
> Kubeadm is a toolkit for bootstrapping a best-practises Kubernetes cluster on existing infrastructure. Kubeadm cannot provision your infrastructure which is one of the main differences to kops. Another differentiator is that Kubeadm can be used not only as an installer but also as a building block.
    * [Kops vs. Kubeadm: What’s the difference?](https://www.weave.works/blog/kops-vs-kubeadm)

### kops
kops - will generate terraform, but I noticed GC issue https://github.com/kubernetes/kops/issues/329
    * to get local networks, you'd have to use unofficial terraform providers for virtualbox/vmware https://github.com/hashicorp/terraform/issues/8518

not seeing anything about generating a local network vagrant or virtualbox which sucks...

### kubeadm
[Playing with kubeadm in Vagrant Machines](https://medium.com/@joatmon08/playing-with-kubeadm-in-vagrant-machines-36598b5e8408)
    * Dec 2017 - seemed rough

## alternatives?
https://www.sylabs.io/ - Singularity is for High-performance computing