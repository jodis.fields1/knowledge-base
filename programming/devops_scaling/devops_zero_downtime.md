
TODO: read this [So You Got Yourself a Loadbalancer](https://serversforhackers.com/c/so-you-got-yourself-a-loadbalancer)

https://en.wikipedia.org/wiki/Load_balancing_(computing) - general principles
    * biggest lesson here is https://en.wikipedia.org/wiki/Load_balancing_(computing)#Persistence
        * not sure if we did that at Brightidea, seems complicated, note that https://devcentral.f5.com/questions/speeding-connection-draining-by-forcibly-closing-http-keep-alive-51930 for F5 points to using reselect to force another pool member
        * more explanation of persistence at https://web.archive.org/web/20171205223948/https://f5.com/resources/white-papers/load-balancing-101-nuts-and-bolts

## TODO steps
[How to use terraform.io to change the image of a stateful server without downtime or data loss?](https://serverfault.com/questions/616778/how-to-use-terraform-io-to-change-the-image-of-a-stateful-server-without-downtim)
    * The basic pattern is to define your data volumes separately and attach these to the instances, so that when the instance is destroyed and a new one created (e.g. from a new AMI built by Packer) the existing volume can be attached to the new instance.
        * aws_volume_attachment,  prevent_destroy = true, and so on...

[Deploying to a single host with zero downtime](https://www.reddit.com/r/docker/comments/6041un/deploying_to_a_single_host_with_zero_downtime/)
    * most recommend minikube https://github.com/kubernetes/minikube
        * see my kubernetes notes


## high-availability / single point of failure
`aws ec2 describe-availability-zones --region region-name` per https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/using-regions-availability-zones.html#using-regions-availability-zones-launching

https://stackoverflow.com/questions/10494431/sticky-and-non-sticky-sessions

"Basically if you do not require long lasting sticky sessions you can configure your DNS servers to return multiple A records (IP addresses) for your website."
    - per https://stackoverflow.com/questions/7943211/web-app-high-availability-how-to-prevent-a-single-point-of-failure


## theory / architecture
[TCP is an underspecified two-node consensus algorithm and what that means for your proxies](https://web.archive.org/web/20180319054814/https://morsmachine.dk/tcp-consensus) - saw this at https://news.ycombinator.com/item?id=16616357 - guy has a point about TCP getting messed up in the middle

## stories
[SmartStack: Service Discovery in the Cloud](https://web.archive.org/web/20180109215555/https://medium.com/airbnb-engineering/smartstack-service-discovery-in-the-cloud-4b8a080de619) - the airbnb story
    * https://github.com/airbnb/smartstack-cookbook
    * https://github.com/airbnb/synapse

[Deploy PHP applications with zero downtime and under (heavy) load](https://web.archive.org/web/20180322043733/http://blog.forrest79.net/?p=537)

[Stack Overflow: How We Do Deployment - 2016 Edition](https://web.archive.org/web/20180317003343/https://nickcraver.com/blog/2016/05/03/stack-overflow-how-we-do-deployment-2016-edition/)

[HARDENING NODE.JS FOR PRODUCTION PART 3: ZERO DOWNTIME DEPLOYMENTS WITH NGINX](https://web.archive.org/web/20170831134911/http://blog.argteam.com/coding/hardening-node-js-for-production-part-3-zero-downtime-deployments-with-nginx/)
    * nodejs story

## process
process (copied from https://blog.codeship.com/zero-downtime-deployment-with-aws-ecs-and-elb/):
1. Create a new Virtual Machine (VM) image with the new code.
2. Start a number of VMs using that image, equal to the number currently running.
3. Verify each of these instances are running correctly and responding to checks.
4. Add the new instances to the ELB while removing the old (with connection draining).
5. Verify that everything is working properly. If not swap old back in for the new and diagnose the problem. If so, delete the old instances.

### redirect domain name to new
https://serverfault.com/questions/371833/changing-servers-redirect-to-new-ip-no-downtime 
    * "Forward the request coming to the old ip to the new server until DNS propagation completes."
        * need to get better with iptables

### sockets
poses special issues per https://github.com/SocketCluster/socketcluster/issues/42#issuecomment-69827820:
"
restart all workers by sending a SIGUSR2 signal to the master process (master PID is logged when you start SC) or you can use the SocketCluster instances' killWorkers()
...
Missing a few realtime messages isn't a huge deal if you're storing the messages in a database anyway (which is usually the case for most apps unless we want truly ethereal messaging) you can make your clients refetch the latest data when socket.on('connect', ...) triggers
...
True zero downtime deploy is difficult to achieve with realtime WebSocket connections because each client is attached to a single server. We could come up with a strategy to keep the old workers up (the ones that have active connections) and spawn some new ones (which use the new code) to handle all new connections and the old workers will be killed only when they have 0 clients left attached to them"

### connection draining
[Advanced Node Draining in HashiCorp Nomad](https://www.hashicorp.com/blog/advanced-node-draining-in-hashicorp-nomad)
https://serverfault.com/questions/512119/what-happens-after-you-deregister-an-ec2-instance-from-elastic-load-balancer
https://serverfault.com/questions/861589/amazon-elb-doesnt-clean-up-keepalive-connections/861592
    * AWS ELB supports draining out of the box
    * also see https://aws.amazon.com/blogs/aws/elb-connection-draining-remove-instances-from-service-with-care/ and https://aws.amazon.com/blogs/aws/new-aws-application-load-balancer/
3 - connection draining - is particularly interesting.

