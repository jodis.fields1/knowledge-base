[Hello, and welcome to DNS](https://news.ycombinator.com/item?id=16714679)

[AdGuard DNS: A Privacy-Oriented DNS Server](https://news.ycombinator.com/item?id=18788410)

## 2017
### wildcard?
[In my /etc/hosts/ file on Linux/OSX, how do I do a wildcard subdomain?](https://serverfault.com/questions/118378/in-my-etc-hosts-file-on-linux-osx-how-do-i-do-a-wildcard-subdomain)

## general
https://tools.ietf.org/html/rfc1034 - precursor to implementation at 1035

https://web.archive.org/web/20170601041754/http://blog.catchpoint.com/2014/07/01/dns-lookup-domain-name-ip-address/

[How can I see Time-To-Live (TTL) for a DNS record?](https://serverfault.com/questions/179630/how-can-i-see-time-to-live-ttl-for-a-dns-record)

### link dump
https://github.com/alex/what-happens-when/issues | Issues · alex/what-happens-when
https://www.petekeen.net/how-and-why-im-not-running-my-own-dns | How and why I'm not running my own DNS | Pete Keen
https://diyisp.org/dokuwiki/doku.php?id=technical:dnsresolver | DNS resolvers [Do-It-Yourself Internet Service Providers]
https://doc.powerdns.com/ | PowerDNS Documentation
https://github.com/alex/what-happens-when/issues/262 | Where is the parsing process of DNS? · Issue #262 · alex/what-happens-when
https://blog.dnsimple.com/2015/02/top-dns-servers/ | The Top DNS Servers And What They Offer - DNSimple Blog
https://github.com/isc-projects/bind9 | isc-projects/bind9: The classic full-featured DNS implementation. This is a mirror of https://source.isc.org; report issues at https://www.isc.org/community/report-bug.
https://slashdot.org/story/03/02/25/1635239/root-server-switches-from-bind-to-nsd | Root-server switches from BIND to NSD - Slashdot
https://www.isc.org/blogs/dnsbind-canards-redux/ | DNS/BIND Canards, Redux | Internet Systems Consortium
http://www.securityweek.com/defense-bind-open-source-dns-software-yields-better-breed-secure-product | In Defense of BIND: Open Source DNS Software Yields a Better Breed of Secure Product | SecurityWeek.Com
https://en.wikipedia.org/wiki/Public_recursive_name_server | Public recursive name server - Wikipedia
https://github.com/samboy/MaraDNS#other-dns-servers | samboy/MaraDNS: MaraDNS: A small open-source DNS server
https://encrypted.google.com/search?hl=en&q=how%20to%20host%20your%20own%20nameserver | how to host your own nameserver - Google Search
https://www.wired.com/2010/02/set_up_a_dns_name_server/ | Set Up a DNS Name Server | WIRED
