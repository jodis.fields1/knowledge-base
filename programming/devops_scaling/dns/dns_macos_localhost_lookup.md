## new
apparently eBPF can help replace dtrace and strace? http://www.brendangregg.com/blog/2019-01-01/learn-ebpf-tracing.html

## 2018-05 dtrace exploration
disabled SIP via recovery mode + csrutil disable
https://github.com/opendtrace/toolkit/blob/master/Guide | toolkit/Guide at master · opendtrace/toolkit
http://dtrace.org/blogs/brendan/2011/10/10/top-10-dtrace-scripts-for-mac-os-x/ | Brendan's blog » Top 10 DTrace scripts for Mac OS X
https://stackoverflow.com/questions/33799894/iosnoop-iotop-opensnoop-execsnoop-opensnoop-dtruss-and-other-dtrace-based-c | osx elcapitan - iosnoop, iotop, opensnoop, execsnoop, opensnoop, dtruss and other dtrace based commands don't work on osx El capitan, macOS Sierra - Stack Overflow

### older
https://apple.stackexchange.com/questions/208762/now-that-el-capitan-is-rootless-is-there-any-way-to-get-dtrace-working | Now that El Capitan is "rootless", is there any way to get dtrace working? - Ask Different
https://8thlight.com/blog/colin-jones/2017/02/02/dtrace-gotchas-on-osx.html | A few DTrace gotchas and workarounds on OS X | 8th Light

## 2018 macos dns lookup issue
.test is causing 1 minute local DNS lookup

need to use dnsmasq to debug??

### dnsmasq
https://www.reddit.com/r/archlinux/comments/8342sh/psa_i_had_slow_steam_download_speeds_i_fixed_it/
https://stackoverflow.com/questions/19579138/osx-mavericks-dnsmasq-stops-working/19651350#19651350
https://stackoverflow.com/questions/22313142/wildcard-subdomains-with-dnsmasq
https://serverfault.com/questions/431605/dont-automatically-include-all-subdomains-in-dnsmasq-address
https://stackoverflow.com/questions/14059916/is-there-a-command-to-list-all-unix-group-names

### link dump
https://stackoverflow.com/questions/3007868/how-can-i-get-dtrace-to-run-the-traced-command-with-non-root-priviledges

http://dtrace.org/guide/chp-intro.html#chp-intro

https://opensourcehacker.com/2011/12/02/osx-strace-equivalent-dtruss-seeing-inside-applications-what-they-do-and-why-they-hang/

https://bryce.is/writing/code/macosx/debugging/udp/sockets/dtruss/dtrace/eaddrinuse/2016/07/30/debugging-using-system-calls.html - first post that got me into dtrace, found his game https://www.prettymuchgames.com/games/takecare interesting

https://apple.stackexchange.com/questions/290745/macos-launchctl-and-dnsmasq-for-localhost-domain-spoofing | launchd - macOS, launchctl and dnsmasq for localhost domain spoofing - Ask Different
https://apple.stackexchange.com/questions/26616/dns-not-resolving-on-mac-os-x | macos - DNS not resolving on Mac OS X - Ask Different
https://askubuntu.com/questions/473613/chrome-stuck-on-resolving-host | networking - Chrome stuck on 'resolving host' - Ask Ubuntu
https://web.archive.org/web/20170216001308/http://internals.exposed/blog/dtrace-vs-sip.html | https://web.archive.org/web/20170216001308/http://internals.exposed/blog/dtrace-vs-sip.html
https://stackoverflow.com/questions/9192176/host-doing-unnecessary-dns-lookup-for-localhost | linux - host doing unnecessary dns lookup for localhost - Stack Overflow
https://www.quora.com/Does-localhost-require-a-DNS-lookup | Does localhost require a DNS lookup? - Quora
https://github.com/code-mancers/invoker/issues/205 | Change the default .dev tld to .test (or other) · Issue #205 · code-mancers/invoker
https://gist.github.com/ogrrd/5831371 | Setup dnsmasq on OS X
https://news.ycombinator.com/item?id=12578908 | Ask HN: What TLD do you use for local development? | Hacker News
