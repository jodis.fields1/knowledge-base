See deployment

[Goodbye Microservices: From 100s of problem children to 1 superstar](https://segment.com/blog/goodbye-microservices/)

https://konghq.com/
https://www.oreilly.com/ideas/microservices-at-scale

[Ask HN: How do you keep track of releases/deployments of dozens micro-services?](https://news.ycombinator.com/item?id=16166645)

### Microservices
#### Frontend
2017:
https://github.com/opencomponents/awesome-oc
https://opencomponents.github.io/

#### Backend
Third pass (2018-02):
http://microservices.io/patterns/data/saga.html - this looks so hard to get right, with a distributed transaction

Second pass:
https://github.com/rancher/rancher
https://thenewstack.io/kubernetes-way-part-one/
https://thenewstack.io/microservices-terribly-named-ambiguously-defined/
https://thenewstack.io/airbnbs-10-takeaways-moving-microservices/
https://tsuru.io/

First pass:
https://fernandoabcampos.wordpress.com/2016/02/04/microservice-architecture-step-by-step-tutorial/
