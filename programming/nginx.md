# nginx
https://gitlab.com/jcrben-play-learn/nginx-play

Reminder: https://nginx.org/en/docs/http/request_processing.html

openresty is tied up with the postgREST https://github.com/subzerocloud/postgrest-starter-kit

https://github.com/agile6v/awesome-nginx

https://github.com/fcambus/nginx-resources

## docker
* https://docs.nginx.com/nginx/admin-guide/installing-nginx/installing-nginx-docker/
* https://hub.docker.com/_/nginx
  * [Running container as unprivileged user not possible anymore](https://github.com/nginxinc/docker-nginx/issues/329#issuecomment-505088245)

## expose 80 w/o root
use nginx redirect per https://serverfault.com/questions/112795/how-to-run-a-server-on-port-80-as-a-normal-user-on-linux

## Flashcards
None! WIP

[All pages showing 'Welcome to nginx!'](https://stackoverflow.com/questions/23757301/all-pages-showing-welcome-to-nginx/23757498)
    * "One of these files will contain a root directive that configures where to serve static files from initially, amongst other standard configuration"

## General
*nginx versus node* - see [Using Node.js only vs. using Node.js with Apache/Nginx](http://stackoverflow.com/questions/16770673/using-node-js-only-vs-using-node-js-with-apache-nginx)

setup defaults using:

/etc/nginx/sites-enabled

and then the content is served from 

/var/www/html


## Tutorials
https://hackr.io/tutorials/learn-nginx
[How To Host Multiple Node.js Applications On a Single VPS with nginx, forever, and crontab](https://www.digitalocean.com/community/tutorials/how-to-host-multiple-node-js-applications-on-a-single-vps-with-nginx-forever-and-crontab)

## Learning history
Spent a long time browsing the official wiki

don't use apache but if we did... https://www.addedbytes.com/articles/for-beginners/url-rewriting-for-beginners/