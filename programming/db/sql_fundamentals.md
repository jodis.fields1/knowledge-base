# SQL

[Medium-hard SQL interview questions (quip.com)](https://news.ycombinator.com/item?id=23053981)

## ref tricks
* sql formatting
  * use Intellij
  * http://www.dpriver.com/pp/sqlformat.htm - actually sucks, secretly changes stuff - see right sidebar

## unorganized
TODO: https://news.ycombinator.com/item?id=17905666&utm_term=comment | Show HN: Select Star SQL, an interactive SQL book | Hacker News

[SQL where is a CASE WHEN valid](https://news.ycombinator.com/item?id=18877121) - anywhere!

epiphany:

could not do: 
`SELECT CONCAT(CAST(1 as CHAR), 'hi') as col1`

duh, need a subquery:
SELECT CONCAT(CAST(1 as CHAR), (SELECT 'hi')) as col1

learned and forgotten this at least a couple times

### unorganized 2019-07
https://hashrocket.com/blog/posts/modeling-polymorphic-associations-in-a-relational-database | Modeling Polymorphic Associations in a Relational Database | Hashrocket
https://www.reddit.com/r/programming/comments/ad1bo5/say_no_to_venn_diagrams_when_explaining_joins/ | Say NO to Venn Diagrams When Explaining JOINs : programming
http://www.unixwiz.net/techtips/sql-injection.html | SQL Injection Attacks by Example
https://dba.stackexchange.com/questions/83951/financial-database-design | Financial database design - Database Administrators Stack Exchange
https://www.timescale.com/blog/time-series-data-why-and-how-to-use-a-relational-database-instead-of-nosql-d0cd6975e87c/ | Time-series data: Why (and how) to use a relational database instead of NoSQL
https://news.ycombinator.com/item?id=19880334 | Ask HN: How does your development team handle database migrations? | Hacker News
https://news.ycombinator.com/item?id=19423539 | Azure Data Studio: An Open-Source GUI Editor for Postgres | Hacker News


### one-off test queries (mysql)
https://stackoverflow.com/questions/5859391/create-a-temporary-table-in-a-select-statement-without-a-separate-create-table
https://stackoverflow.com/a/1264632/4200039

SELECT 'AD' as cou, 1 as intx
returns:
cou | intx
AD  | 1

SELECT cou FROM (SELECT cou, intx FROM (
  SELECT 'AD' as cou, 1 as intx
  UNION ALL SELECT 'AE', 2
) AS x) as y WHERE cou = 'AD' AND 1

### left join
[MySQL Left Join](http://www.mysqltutorial.org/mysql-left-join.aspx)

### learning resources
#### w3resource
lots of really good stuff here, examples include diagrams at:
    * https://www.w3resource.com/sql/aggregate-functions/count-function.php
    * https://www.w3resource.com/sql/aggregate-functions/count-having.php


### indexes / performance
https://use-the-index-luke.com/

#### sqlzoo
have come back to this a couple times over the years
run by Andrew Cumming per https://sqlzoo.net/wiki/User_talk:Andr3w, Edinburgh Napier University, UK - also see https://sqlzoo.net/wiki/SQLZOO:About

### common table expressions
https://www.essentialsql.com/introduction-common-table-expressions-ctes/ | Introduction to Common Table Expressions (CTE's) - Essential SQL

#### 2018-03-17 practice with postgres

##### subqueries
https://www.dofactory.com/sql/subquery

https://pgexercises.com/questions/basic/classify.html answer uses CTE and CASE WHEN:
    * [PostgreSQL: using a calculated column in the same query](https://stackoverflow.com/q/8840228/4200039)
with label as (select name,
			   case when (monthlymaintenance < 100) then 
			   		'cheap'
			   else 
			   		'expensive'
			   end as cost
			   from cd.facilities)
select * from label;

### correlated subqueries
2019-02-10: ran into this with a migration; ended up using a JOIN plus a complex MIN to get the data instead of a correlated subquery; https://www.geeksforgeeks.org/sql-correlated-subqueries/ is a good guide; also https://web.archive.org/web/20170116055214/http://www.akadia.com/services/sqlsrv_subqueries.html

headscratched at https://dba.stackexchange.com/questions/168673/mysql-subquery-in-columns-of-select-statement

watch out for these
* https://en.wikipedia.org/wiki/Correlated_subquery

### string concat and group by
these two things are related and there's some differences by browser...
    * [Postgresql GROUP_CONCAT equivalent?](https://stackoverflow.com/questions/2560946/postgresql-group-concat-equivalent)
        * string_agg, array_agg, maybe listagg later per http://www.postgresql-archive.org/ToDo-listagg-is-in-ANSI-SQL-2016-tt5949385.html#none
        * [STRING AGGREGATION IN POSTGRESQL, SQL SERVER, AND MYSQL](http://www.postgresonline.com/journal/archives/191-String-Aggregation-in-PostgreSQL,-SQL-Server,-and-MySQL.html) (2010) - another discussion on cross-db
    * BigQuery uses string_agg a lot

discussion around standards at prestodb [Implement string aggregation function](https://github.com/prestodb/presto/issues/1976)

### case when
2018-05 ran into a really complex case when (evaluationTab) - keep in mind
that the thing after THEN is the RETURN VALUE... so SELECT <blah> FROM <blach> WHERE x == 'whatever' AND WHERE CASE <expression> THEN 1 ELSE NULL is a way to selectively filter

#### resources
https://asktom.oracle.com/pls/asktom/f?p=100:11:0::::P11_QUESTION_ID:9532075300346073664 | Ask TOM "How to use case statement inside where clause ?"
https://stackoverflow.com/questions/24249323/using-case-inside-where-clause | sql - Using case inside where clause - Stack Overflow
https://community.modeanalytics.com/sql/tutorial/sql-case/ | SQL CASE | SQL Tutorial - Mode Analytics
https://news.ycombinator.com/item?id=16840220 | My Favorite PostgreSQL Queries and Why They Matter | Hacker News

