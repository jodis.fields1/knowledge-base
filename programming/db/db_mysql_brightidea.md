

### mysql privileges
https://stackoverflow.com/questions/20353402/access-denied-for-user-testlocalhost-using-password-yes-except-root-user | mysql - Access denied for user 'test'@'localhost' (using password: YES) except root user - Stack Overflow
https://stackoverflow.com/questions/5016505/mysql-grant-all-privileges-on-database | mariadb - MySQL: Grant **all** privileges on database - Stack Overflow
https://stackoverflow.com/questions/1559955/host-xxx-xx-xxx-xxx-is-not-allowed-to-connect-to-this-mysql-server | Host 'xxx.xx.xxx.xxx' is not allowed to connect to this MySQL server - Stack Overflow
https://stackoverflow.com/questions/8348506/grant-remote-access-of-mysql-database-from-any-ip-address | grant remote access of MySQL database from any IP address - Stack Overflow


### configuration

connect with
`mysql -h 172.18.0.1`
/etc/my.cnf inside ./build container
datadir=/var/lib/mysql
bind-address = 127.0.0.1
#[client]
#bind-address = 127.0.0.1
#socket=/var/lib/mysql/mysql.sock
# Disabling symbolic-links is recommended to prevent assorted security risks
symbolic-links=0
# Settings user and group are ignored when systemd is used.
# If you need to run mysqld under a different user or group,
# customize your systemd unit file for mysqld according to the
# instructions in http://fedoraproject.org/wiki/Systemd

[mysqld_safe]
log-error=/var/log/mysqld.log
pid-file=/var/run/mysqld/mysqld.pid

[client]
protocol=tcp
#bind-address = 127.0.0.1


### performance audit
https://github.com/mcafee/mysql-audit | mcafee/mysql-audit: AUDIT Plugin for MySQL. See wiki and readme for description. If you find the plugin useful, please star us on GitHub. We love stars and it's a great way to show your feedback.
https://dev.mysql.com/doc/workbench/en/wb-tutorial-visual-explain-dbt3.html | MySQL :: MySQL Workbench Manual :: 7.5 Tutorial: Using Explain to Improve Query Performance
https://stackoverflow.


### msyql stuff
com/questions/367711/what-is-the-best-collation-to-use-for-mysql-with-php/25475756#25475756 | What is the best collation to use for MySQL with PHP? - Stack Overflow
https://stackoverflow.com/questions/2989724/how-to-mysqldump-remote-db-from-local-machine | mysql - how to mysqldump remote db from local machine - Stack Overflow
https://linux.die.net/man/1/mk-parallel-dump | mk-parallel-dump(1) - Linux man page
https://linux.die.net/man/1/mk-find | mk-find(1) - Linux man page
https://stackoverflow.com/questions/37319690/enable-the-strict-mode-in-mysql/47694445#47694445 | enable the strict mode in Mysql - Stack Overflow
https://dev.mysql.com/doc/refman/5.7/en/sql-mode.html | MySQL :: MySQL 5.7 Reference Manual :: 5.1.8 Server SQL Modes
https://mysqlworkbench.org/category/modelling/ | Modelling – The MySQL Workbench Developer Central Site
https://www.mysql.com/products/workbench/features.html | MySQL :: MySQL Workbench Features
https://github.com/mysql | MySQL
https://forums.mysql.com/read.php?152,208229,208229#msg-208229 | MySQL :: MySQL Workbench: Propel, Doctrine, SQLAlchemy, ActiveRecord, Hibernate
https://dev.mysql.com/doc/refman/5.7/en/privilege-changes.html | MySQL :: MySQL 5.7 Reference Manual :: 6.2.6 When Privilege Changes Take Effect
https://stackoverflow.com/questions/924729/how-to-best-display-in-terminal-a-mysql-select-returning-too-many-fields | command line - How to best display in Terminal a MySQL SELECT returning too many fields? - Stack Overflow
https://www.w3resource.com/mysql/aggregate-functions-and-grouping/aggregate-functions-and-grouping-group_concat.php | MySQL GROUP_CONCAT() function - w3resource
https://stackoverflow.com/questions/8212192/can-mysql-concatenate-strings-with | concatenation - Can MySQL concatenate strings with || - Stack Overflow
