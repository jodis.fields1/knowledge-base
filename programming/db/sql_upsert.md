SQL Upsert
=========

Common task, awkward syntax - in both mysql and postgres, have to twice new value twice.
* Solution: wrote a function to generate DRY sql
* https://stackoverflow.com/questions/9537710/is-there-a-way-to-use-on-duplicate-key-to-update-all-that-i-wanted-to-insert | mysql - Is there a way to use ON DUPLICATE KEY to Update all that I wanted to insert? - Stack Overflow
* https://stackoverflow.com/questions/36359440/postgresql-insert-on-conflict-update-upsert-use-all-excluded-values | PostgreSQL INSERT ON CONFLICT UPDATE (upsert) use all excluded values - Stack Overflow

## link dump
https://stackoverflow.com/questions/1109061/insert-on-duplicate-update-in-postgresql/30118648#30118648 | sql - Insert, on duplicate update in PostgreSQL? - Stack Overflow
https://postgresql.uservoice.com/search?filter=ideas&query=ON%20CONFLICT | Knowledge Base – Customer Feedback for PostgreSQL


## mysql specific upsert wrapper
Wrote an upsert wrapper function, links below used to help me figure out my mistakes

### errors
https://stackoverflow.com/questions/29069210/insert-select-on-duplicate-key-with-column-aliases-in-select-clause | mysql - INSERT + SELECT + ON DUPLICATE KEY with column aliases in SELECT clause - Stack Overflow
https://dba.stackexchange.com/questions/73323/mysql-insert-into-on-duplicate-key-update/73325#73325 | Mysql INSERT INTO .. ON DUPLICATE KEY UPDATE - Database Administrators Stack Exchange
https://stackoverflow.com/questions/38104072/mysql-insert-error-operand-should-contain-1-column | MySQL INSERT Error Operand should contain 1 column - Stack Overflow

### related stuff viewewd
https://github.com/CSNW/sql-bricks/issues/83 | Upsert queries · Issue #83 · CSNW/sql-bricks