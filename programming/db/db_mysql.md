
**Comparison**
http://troels.arvin.dk/db/rdbms/

## MySQL

### performance
lots of discussion on performance at e.g. https://dev.mysql.com/doc/refman/5.7/en/subquery-optimization.html

also see:
* "mysql does optimize a subquery in the JOIN clause: it runs the subquery and puts the results into a "derived table" (temporary table without any indexes), and joins the derived table like any other" - https://stackoverflow.com/a/28622861/4200039
* [MySQL subquery really slow… Workaround?](https://stackoverflow.com/questions/5997991/mysql-subquery-really-slow-workaround)

Interesting article that I didn't understand as well as I'd like as of 2018-08:
* [How I slashed a SQL query's runtime with two Unix commands](https://news.ycombinator.com/item?id=17690780)

### DML
[MySQL Error 1093 - Can't specify target table for update in FROM clause](https://stackoverflow.com/questions/45494/mysql-error-1093-cant-specify-target-table-for-update-in-from-clause) - found the MySQL feature request, didn't see one for MariaDB altho they are working on the LIMIT over there https://jira.mariadb.org/browse/MDEV-139 
    * "Sometimes I wonder what drugs the MySQL devs are on..." from above


upsert: https://stackoverflow.com/questions/548541/insert-ignore-vs-insert-on-duplicate-key-update and https://dev.mysql.com/doc/refman/5.5/en/insert-on-duplicate.html

### DDL / data types
Use text instead of varchar!

No boolean support! Boo!

[How to Enforce Data Type Constraint](http://www.geeksengine.com/database/design/data-type-constraint.php) - gotchas, unsigned integer constraints return negative numbers return max!

[Go4Expert MySQL Database Tutorials](http://www.go4expert.com/articles/mysql/) - ??

### Indexes
According to [this](http://www.sitepoint.com/mysql-mistakes-php-developers/) indexes are regenerated for every insert and update? I guess that makes sense...

### innodb constraints
I was confused as heck by how the "CONSTRAINT" syntax was optional and what the "symbol" meant over at http://dev.mysql.com/doc/refman/5.0/en/innodb-foreign-key-constraints.html 13.2.4.4. FOREIGN KEY Constraints - someone else was also confused (http://stackoverflow.com/questions/310561/mysql-terminology-constraints-vs-foreign-keys-difference) but I guess the answer is to not worry about it and just ignore the optional term. For example, stocks_ibfk_1 was created as a symbol for a foreign key constraint, and the index name was simply sector_id (same name as the foreign key)

### ancient
**MySQL**

[Do Not Pass This Way Again](http://grimoire.ca/mysql/choose-something-else) - 

start with mysql.server start [per SO](http://stackoverflow.com/questions/4788381/getting-cant-connect-through-socket-tmp-mysql-when-installing-mysql-on-m)

Other general tips (mysql bias, obviously)

updating with join tips at http://blog.ookamikun.com/2008/03/mysql-update-with-join.html and http://forums.mysql.com/read.php?97,57357,57357 sounds pretty straightforward

when you're starting out with some data, you could create one big table with duplicate data (columns) and then use index-match (similar to vlookup) to put in foreign keys, then delete those columns as you fork off that data into its own tables, typically one table will be sort of the focus e.g. in my homeinventory database it is products or in the stock market it would be the stocks; also when researching I heard some stuff about Combination lookup-update at http://wiki.pentaho.com/display/EAI/Combination+lookup-update

interesting tip on joining multiple tables http://www.dbforums.com/microsoft-access/1617402-correct-syntax-inner-join-multiple-tables.html

Note that the mysql monitor is a good way to play around with mysql; I prefer batch mode (use /. [filename] or source to execute files - see http://dev.mysql.com/doc/refman/5.0/en/programs-client.html section 4.5 and particularly http://dev.mysql.com/doc/refman/5.0/en/batch-commands.html for more info on this type of stuff - note that comments are done with

Use mysql to login - e.g. mysql -h iiprocess.db -u impinform -p impindata in other words "mysql -h [hostname] -u [username] -p [password prompted] [database name]

For loading data use \. - see http://dev.mysql.com/doc/refman/5.0/en/loading-tables.html and http://dev.mysql.com/doc/refman/5.0/en/load-data.html - can likely also import using the GUI - http://bytes.com/topic/mysql/answers/520788-loading-lots-data-into-mysql-table points out that load data says it doesn't support text or blob (true) but someone says it works anyway - would be nice to find a more elegant way to handle large blocks of text

Transferring from one table to the next

Learned and tested this from http://dev.mysql.com/doc/refman/5.0/en/insert-select.html with a little inspiration from http://stackoverflow.com/questions/7396938/need-to-populate-one-table-with-data-from-another-table-in-same-database and http://stackoverflow.com/questions/5167605/copy-data-from-one-table-to-another-and-adding-additional-data

Standards

Looked at SQL:2011 http://en.wikipedia.org/wiki/SQL:2011 Par 1 and 2 (see external links) to find out if there's a rule on table names not having hyphens; Part 2 page 50 or so has discussion of tables but not clear they discuss this

Stored procedures and triggers

These aren't as exciting as I thought; Ch18 in docs http://dev.mysql.com/doc/refman/5.0/en/stored-programs-views.html - also looked at http://net.tutsplus.com/tutorials/an-introduction-to-stored-procedures/ (syntax examples), http://www.databasejournal.com/features/mysql/article.php/3525581/MySQL-Stored-Procedures-Part-1.htm (hard to follow), and http://preetul.wordpress.com/2009/06/26/using-mysql-stored-procedures-with-php-mysqlmysqlipdo/ (ok, not clear on applications)