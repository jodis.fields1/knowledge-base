
## 2018-05
[N+1 is a Rails feature](https://web.archive.org/web/20180529015530/https://rossta.net/blog/n-1-is-a-rails-feature.html)
[Conceptual compression means beginners don’t need to know SQL — hallelujah!](https://m.signalvnoise.com/conceptual-compression-means-beginners-dont-need-to-know-sql-hallelujah-661c1eaed983)
    * exaggeration... very early beginners

## old
READ THIS FIRST: [Database abstraction layer](https://en.wikipedia.org/wiki/Database_abstraction_layer)

When I first started programming web applications, I was distantly aware of a controversy around object-relational mappers (ORMs). But I didn't spend much time thinking about them, as I was too busy figuring out the details of the specific implementations that I was working on. I briefly played around with wiring SQLAlchemy into a Flask application, which probably wasn't the most approachable first ORM as compared to something built-in like Rails or Django. Since then I've used Bookshelf.js, Sequelize (Node.js), Rails, and Yii (ActiveRecord pattern).

ORMs are both easier and more difficult. It's easier to write `object->save` then to type out the entire SQL statement. But you have to have at least some understanding how the underlying SQL statement works, so you're juggling more concepts. It's more convenient.

ORMs can also be much more difficult if you try to use them in complex ways that they were not designed to handle. Complex SQL doesn't such as nested subqueries, proprietary SQL features, pivots, and so on don't always fit into the ORM API, and if you try to make them fit you're going to have a hard time. But you always want to be independent from the database implementation. This is where abstracted SQL syntax comes in handy. SQLAlchemy calls their abstraction the SQL Expression Language. Yii calls its the Command Builder. Ruby has [Arel](https://www.viget.com/articles/composable-sql-queries-in-rails-using-arel) and [Sequel](https://github.com/jeremyevans/sequel). These tools may also let you run raw SQL, but it is not clear if this also abstracts implementations or just escapes input (TODO: check into raw SQL abstraction).

A friend of mine once remarked that when working with a backend framework (e.g., Django or Rails) as an API, the framework becomes basically the ORM. 

Surprisingly, ORMs sometimes lag behind in key and basic features, such as batch updating for Rails. TODO: check for this

One interesting topic among ORMs is that their abstraction of data types. Sometimes ORMs have to choose the best types, and this gives you an indication of perhaps the 'best' representation of a type. For example, MYSQL has multiple TEXT values, but other databases such as Postgres have a single TEXT value which is adequate for all purposes [[1](https://github.com/yiisoft/yii2/issues/8950)]. 