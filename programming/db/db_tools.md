NOTE: also in zenkit

settled on https://github.com/schemaspy/schemaspy

https://en.wikipedia.org/wiki/Comparison_of_database_tools

[Synchronizing a MySQL Database with Git and Git Hooks](http://ben.kulbertis.org/2011/10/synchronizing-a-mysql-database-with-git-and-git-hooks/) - sounds cool

### To-try? - ranked by best to worst
SQL Squirrel - open-source http://squirrel-sql.sourceforge.net/ and has update on 2017; 
SQL Power Architect - http://www.sqlpower.ca/page/architect_download_os; bugs at http://trillian.sqlpower.ca/bugzilla/ and code at https://github.com/SQLPower/power-architect but seems like it might be dead

Dbeaver - solid

[Dbvis](http://www.dbvis.com/features/software-screenshots/) - freeware, widely-used
Navicat - one of the most popular
Mysql - MySQL Workbench seems to be most popular so should be prepared to use it, Percona has various solutions

### Docs
https://github.com/schemaspy/schemaspy - now here
schemaSpy (s[ee howto](http://randomactsofcoding.blogspot.com/2009/01/database-documentation-using-schemaspy.html)) wiki-style tool seems decent per [http://stackoverflow.com/questions/9228130/tool-for-creation-and-managing-of-mysql-db-documentation](http://stackoverflow.com/questions/9228130/tool-for-creation-and-managing-of-mysql-db-documentation)

#### Brightidea example

java -jar .\schemaSpy_5.0.0.jar -t mssql-jtds -db OnDemand_Dev -host localhost -port 1443 -u vagrant -o library -connprops=domain\=WIN-OHS0CTBB1TK

java -jar .\schemaSpy_5.0.0.jar -t mssql-jtds -db OnDemand_Dev -host localhost -port 1433 -u sa -p sasa -o library -s dbo

#### Tools
Dataedo - seems to be clear winner in documentation
[http://dbdesc.com/](http://dbdesc.com/) - underwhelming?

[Documenting relational databases (tables, views, functions, triggers)](http://stackoverflow.com/questions/3475998/documenting-relational-databases-tables-views-functions-triggers) - ?

[Good tool to visualise database schema?](http://stackoverflow.com/questions/433071/good-tool-to-visualise-database-schema) - schemaSpy is recommended

[How to document a database](http://stackoverflow.com/questions/369266/how-to-document-a-database) - pointed me to Dataedo

[Doxygen-like for .sql files in a php application](http://stackoverflow.com/questions/4185455/doxygen-like-for-sql-files-in-a-php-application) - there's a sql2doxygen thing

[How should I document a database schema?](http://writers.stackexchange.com/questions/12587/how-should-i-document-a-database-schema) - (oddly in writers), answer suggests Visio and Dia, but another answer says db fields are really hard to change

[Generate table relationship diagram from existing schema (SQL Server) [closed\]](http://stackoverflow.com/questions/168724/generate-table-relationship-diagram-from-existing-schema-sql-server) - DbVis is the most highly-recommended

[http://sualeh.github.io/SchemaCrawler/](http://sualeh.github.io/SchemaCrawler/)