## link dump
https://news.ycombinator.com/item?id=18335723 | Show HN: SQL Trainer – Learn SQL by doing live data exercises | Hacker News
http://www.mysqltutorial.org/mysql-exists/ | The Ultimate Guide to MySQL EXISTS
https://www.w3schools.com/sql/func_mysql_case.asp | MySQL CASE Function
https://www.w3schools.com/sql/trymysql.asp?filename=trysql_case | MySQL Tryit Editor v1.0
http://www.sqltutorial.org/sql-left-join/ | SQL LEFT JOIN: A Comprehensive Guide to LEFT JOIN in SQL
https://stackoverflow.com/questions/1052189/how-to-write-a-simple-database-engine | sql - How to write a simple database engine - Stack Overflow
https://www.vertabelo.com/blog/technical-articles/database-model-for-a-messaging-system | Database Model for a Messaging System
https://stackoverflow.com/questions/4429319/you-cant-specify-target-table-for-update-in-from-clause | sql - You can't specify target table for update in FROM clause - Stack Overflow
https://stackoverflow.com/questions/27532260/database-design-dilemma-on-connecting-users-with-messages | mysql - Database design dilemma on connecting users with messages - Stack Overflow
https://stackoverflow.com/questions/8105927/database-design-for-storing-chat-messages-between-people | mysql - Database Design for storing Chat Messages between people - Stack Overflow
https://news.ycombinator.com/item?id=18557260 | LearnDB: Learn how to build a database | Hacker News
