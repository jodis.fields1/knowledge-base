Services mean that data is organized into services. A flat list of hundreds of tables is hard to grok. Specific discussion: https://www.brentozar.com/archive/2010/05/why-use-schemas/ | Why Use Schemas?

Solutions:
* use schemas - supported by most all dbs except mysql
* in mysql, use dbs - might be less easy to join across dbs but see discussion e.g. https://laracasts.com/discuss/channels/laravel/how-to-connect-2-different-databases-and-join-2-tables-that-are-on-different-servers


## link dump
https://dba.stackexchange.com/questions/25655/does-mysql-5-5-support-shared-database-with-separate-schema | Does MySQL 5.5 support shared database with separate schema? - Database Administrators Stack Exchange
https://stackoverflow.com/questions/4217157/how-can-i-organize-a-glut-of-mysql-tables | How can I organize a glut of mysql tables? - Stack Overflow
https://stackoverflow.com/questions/1972943/how-do-i-construct-a-cross-database-query-in-mysql | sql - How do I construct a cross database query in MySQL? - Stack Overflow
