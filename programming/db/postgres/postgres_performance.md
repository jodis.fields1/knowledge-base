[Gracefully Scaling to 10k PostgreSQL Connections for $35/mo, Part Two](https://news.ycombinator.com/item?id=17370062)

[100x faster Postgres performance by changing 1 line](https://www.datadoghq.com/blog/100x-faster-postgres-performance-by-changing-1-line/) -

"If a query is performing badly when it should be much faster, make sure types are compatible across the board. Some languages are particularly strict with regard data type usage, so consider PGDB among them."
    * [PG Phriday: 10 Ways to Ruin Performance: Cast Away](http://bonesmoses.org/2015/06/05/pg-phriday-10-ways-to-ruin-performance-cast-away/)