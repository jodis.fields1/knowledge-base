Looks like I'm settling on postgres as my db of choice.

See Anki for memories and dotfiles for psql notes

https://stackoverflow.com/questions/24718706/backup-restore-a-dockerized-postgresql-database?rq=1 - might run into this someday

[Pev: Postgres ‘Explain’ Visualizer (2016) (tatiyants.com)](https://news.ycombinator.com/item?id=14834672) - 

## server admin
On debian, just do install & then do service postgresql start
* also see https://serverfault.com/questions/560757/how-do-i-change-the-config-file-location-of-a-postgresql-database-for-service-p
* no desc of --config-file option ... where is it? https://www.postgresql.org/message-id/48721502.6060303@dunslane.net
* https://wiki.debian.org/PostgreSql
  * http://www.debianhelp.co.uk/postgresql.htm
  * https://unix.stackexchange.com/questions/103765/logic-behind-postgres-binary-installation-path-on-debian

https://pgdash.io/blog/pgctl-tips-tricks.html

## diffing
https://github.com/fordfrog/apgdiff

## GUI
https://github.com/pgManage/pgManage - formerly postage, from workflowproducts

## guides / application dev
[Mastering PostgreSQL in Application Development](https://news.ycombinator.com/item?id=15634953)
* expensive, highly-reviewed
* excerpt at https://tapoueh.org/blog/2018/03/database-normalization-and-primary-keys/

## Gotchas
my Docker container comment: https://github.com/docker-library/postgres/issues/176#issuecomment-338515954

## roles (users), groups, etc
[Command to list PostgreSQL user accounts?](https://unix.stackexchange.com/questions/201666/command-to-list-postgresql-user-accounts)
    * \du
    * deu[+] [PATTERN]

https://www.digitalocean.com/community/tutorials/how-to-use-roles-and-manage-grant-permissions-in-postgresql-on-a-vps--2#how-to-remove-permissions-in-postgresql - good overview
https://www.postgresql.org/docs/9.6/static/role-attributes.html
    * https://www.postgresql.org/docs/9.0/static/sql-createrole.html
        * "The INHERIT attribute governs inheritance of grantable privileges (that is, access privileges for database objects and role memberships). It does not apply to the special role attributes set by CREATE ROLE and ALTER ROLE"

### privileges
[How to change owner of PostgreSql database?](https://stackoverflow.com/a/43704694/4200039) - using ALTER TABLE is a gotcha, instead use REASSIGN

https://www.postgresql.org/docs/9.0/static/sql-grant.html
"GRANT ALL PRIVILEGES ON DATABASE grants the CREATE, CONNECT, and TEMPORARY privileges on a database to a role (users are properly referred to as roles). None of those privileges actually permits a role to read data from a table; SELECT privilege"
    - https://serverfault.com/a/198018/309584
    - do not think this is true, but will need to investigate

### Tasks
*Dump and restore db* https://stackoverflow.com/questions/12564777/schema-only-backup-and-restore-in-postgresql

### Tools
#### CLI
createuser - calls CREATE ROLE https://www.postgresql.org/docs/9.0/static/sql-createrole.html
createdb
dropdb
psql
pg_ctl (Debian) - https://frosty-postgres.blogspot.com/2014/04/how-to-start-postgresql-debian-way-with.html

### pain points
bloat? fix in pg12 https://news.ycombinator.com/item?id=16526623

### Connection and configuration

ultimately use libpq

see https://www.postgresql.org/docs/9.1/static/libpq-connect.html for possible connection values
passed through https://github.com/brianc/node-postgres/wiki/Client#new-clientobject-config--client

https://www.postgresql.org/docs/current/static/runtime-config-client.html

#### docker config
[Version postgres:9.4 doesn't create role/password](https://github.com/docker-library/postgres/issues/41#issuecomment-382925263)
https://github.com/docker-library/postgres/issues/266

### Settings
https://dba.stackexchange.com/questions/56023/what-is-the-search-path-for-a-given-database-and-user

### Search path
Can affect what schema your tables get created into per https://stackoverflow.com/questions/9067335/how-does-the-search-path-influence-identifier-resolution-and-the-current-schema

### Source
http://www.interdb.jp/pg/ - recommended by Bradfield

https://github.com/postgres/postgres - official mirror

### Feedback
submitted INSERT INTO ... SET to uservoice
http://sql-info.de/index.html - good articles on Postgres, including on foreign data wrappers

### Tips
[Postgres tips for Rails developers](https://news.ycombinator.com/item?id=14222823) - also check out pgHero

### Community
http://peter.eisentraut.org/ - main guy
https://blog.hagander.net/another-couple-of-steps-on-my-backup-crusade-235/ - working on backups
https://rhaas.blogspot.com/2016/01/postgresql-past-present-and-future.html - seems to be a leader

### Deep end
Clusters, sharding, replication, etc

#### Replication
https://www.opsdash.com/blog/postgresql-streaming-replication-howto.html
https://rhaas.blogspot.com/2016/08/ubers-move-away-from-postgresql.html - seems that Uber wanted logical replication but it doesn't exist yet?

### Crazy extensions
Versioning? https://stackoverflow.com/questions/6365710/database-content-versioning
    * https://github.com/pgMemento/pgMemento
