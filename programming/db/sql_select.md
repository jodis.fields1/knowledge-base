
## filtering or finding duplicates
[Finding duplicate values in a SQL table](https://stackoverflow.com/questions/2594829/finding-duplicate-values-in-a-sql-table/2594855#2594855)
still find using distinct versus GROUP BY a bit confusing

### distinct
[SQL SELECT with DISTINCT on multiple columns](https://www.w3resource.com/sql/select-statement/queries-with-distinct-multiple-columns.php) - 


## aggregation
[Reason for Column is invalid in the select list because it is not contained in either an aggregate function or the GROUP BY clause [duplicate]](https://stackoverflow.com/a/13999903/4200039) - excellent explanatio