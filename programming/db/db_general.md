http://www.sqlstyle.guide/

### Versioning (also see postgres, migrations)
https://dotmesh.com/blog/introducing-dotmesh/ - intriguing

### Browser
Lovefield - ??

### Sharding / scaling
[How Sharding Works](https://medium.com/@jeeyoungk/how-sharding-works-b4dec46b3f6) - read 2018-07 randomly

### Cascades
http://stackoverflow.com/questions/1481476/when-to-use-on-update-cascade - on update cascade is useless if your primary key never changes

### Junction tables**
Often referred back to [Understanding a SQL Junction Table](http://megocode3.wordpress.com/2008/01/04/understanding-a-sql-junction-table/)

### ORMs
See specific languages for details...
[ORMs Under the Hood](http://www.vertabelo.com/blog/technical-articles/orms-under-the-hood) 
General N+1 problem
https://stackoverflow.com/questions/23185319/why-is-loading-sqlalchemy-objects-via-the-orm-5-8x-slower-than-rows-via-a-raw-my

### Tips and learning
[Ask HN: What is the best online resource to learn advanced SQL?](https://news.ycombinator.com/item?id=13417326)
http://sqlzoo.net/ - certainly the best
Remember to sometimes start your query from a junction table to get all the data you want! and don't forget about a union to concat a bunch of data together

Joe Celko's SQL for Smarties, Fourth Edition: Advanced SQL Programming - need to check this out

http://hackmysql.com/case2 ("Table Design and MySQL Index Details")incidentally, that hackmysql.com went down  

#### Hack reactor lecture
Junction/Join table - look at it as a two-dimensional grid with markers where a relationship exist->create a hash table for the join table

#### SQL
SQL - uses an array (fixed size buckets with sequential ids) and then use hash tables for indexing->basically create a new one for each lookup which creates a cost for insertion and updates
MongoDB - like a hash table with references pointing to a spot on the disk, leaves holes in the database when there's a deletion

Sorting - can use a CASE WHEN as described [here](https://www.tutorialspoint.com/sql/sql-sorting-results.htm) which reassigns the number for these purposes..

### Application
#### Populating
This require some programming skill; I used Excel to come up with test data but it was awkward, especially for junction tables.  DigDB might help

### Financial statements / time series
I keep wondering the best way to handle financial statements in a relational database, and I think this book Managing Time in Relational Databases: How to Design, Update and Query Temporal Data (http://my.safaribooksonline.com/book/databases/database-design/9780123750419) might provide some clues.
