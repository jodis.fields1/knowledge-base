
## 2018-07
No conclusion from this, except perhaps - use mysqldump?
Wonder what the story is for postgres

https://serverfault.com/questions/256051/incremental-differential-mysql-backup-using-mysqldump | Incremental/Differential MySQL Backup using mysqldump - Server Fault
https://stackoverflow.com/questions/23880033/mysql-backup-options-using-mysqldump-and-mysqlbackup | MYSQL backup options using mysqldump and mysqlbackup - Stack Overflow
https://dba.stackexchange.com/questions/20/how-can-i-optimize-a-mysqldump-of-a-large-database | mysql - How can I optimize a mysqldump of a large database? - Database Administrators Stack Exchange
https://www.quora.com/What-is-the-fastest-way-to-back-up-a-MySQL-database | What is the fastest way to back up a MySQL database? - Quora
https://dba.stackexchange.com/questions/31865/how-to-increase-fast-backup-and-restore-of-500gb-database-using-mysqldump | mysql - How to increase fast backup and restore of 500GB database using mysqldump? - Database Administrators Stack Exchange
https://stackoverflow.com/questions/15872543/access-mysql-remote-database-from-command-line | Access mysql remote database from command line - Stack Overflow
https://stackoverflow.com/questions/1135245/how-to-get-a-list-of-mysql-user-accounts | mysql5 - How to get a list of MySQL user accounts - Stack Overflow
