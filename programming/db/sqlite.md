Avoid wherever possible.

## Sqlite
https://www2.sqlite.org/cvstrac/wiki is deprecated

[How do I list the tables in a SQLite database file](http://stackoverflow.com/questions/82875/how-to-list-the-tables-in-an-sqlite-database-file-that-was-opened-with-attach) - .schema and .tables do not work when db is attached; [awardspace tutorial](http://sqlite.awardspace.info/syntax/sqlitepg12.htm)

Per [third FAQ dynamically typed](http://www.sqlite.org/faq.html#q3) (constraints are ineffective) but has a "type affinity" where it tries to convert to the declared type 

Foreign key constraint ([official page](http://www.sqlite.org/foreignkeys.html)) requires you to run a command every time ([see SO](http://stackoverflow.com/questions/9937713/does-sqlite3-not-support-foreign-key-constraints)) which will be fixed in 4

Limited ALTER TABLE syntax, no DROP column, etc. Workarounds implemented [in alembic](https://bitbucket.org/zzzeek/alembic/issues/21/column-renames-not-supported-on-sqlite) and sqlalchemy-migrations, also see [How do I rename a column in a SQLite database table?](http://stackoverflow.com/questions/805363/how-do-i-rename-a-column-in-a-sqlite-database-table/805508#805508

*Importing files*
http://www.sqlite.org/cvstrac/wiki?p=Imp"trailing": trueortingFiles is the key to me; however it doesn't mention that when you turn headers on, your "csv" should not have headers; also need to turn on csv mode by .mode csv (default is list); .separator should probably be bars like |  as least likely to get screwed with

*Tools*
SqliteBrowser, sqliteman

### GUI
https://github.com/pvanek/sqliteman