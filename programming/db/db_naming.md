Naming and style
Personal standard: use all lowercase and _ for multiple words ; also use table.id versus table.tableid, at first I was worried this would be confusing but I figure that I'm really only going to use multiple words in the names ("descriptors") of columns

http://justinsomnia.org/2003/04/essential-database-naming-conventions-and-style/

http://blog.sqlauthority.com/2008/09/25/sql-server-guidelines-and-coding-standards/ - don't like this

General
That inextricable problem and controversy. I've looked at:

Database, Table and Column Naming Conventions? http://stackoverflow.com/questions/7662/database-table-and-column-naming-conventions which is from 2008 - pointed me towards ISO/IEC 11179 (http://metadata-standards.org/11179/#A1also see Wikipedia page) which settles on plural for table names, although there's a bunch of disagreement on that. Apparently this somewhat originates from Joe Celko, who has written Joe Celko's SQL programming style - page 6-10 discusses ISO/IEC 11179. This is possibly also related to a http://en.wikipedia.org/wiki/Data_element_name. I read through ISO/IEC 11179 on Feb-6-2012.

Oracle standards also agree that table names should be plural http://ss64.com/ora/syntax-naming.html
    Column names should be singular and should probably have something of the table embedded in them, particularly for the primary key.
    http://www.sqlhacks.com/FAQs/Naming-Conventions has a description of ISO/IEC 11179
    Joe Celko has another comment on his guidelines here - http://bytes.com/topic/sql-server/answers/808553-database-naming-conventions

Other threads include http://stackoverflow.com/questions/4702728/relational-table-naming-convention/4703155#4703155 and http://stackoverflow.com/questions/4819482/why-isnt-there-an-official-naming-convention-for-database-objects\

Couple Microsoft threads illustrate the controversy over the standards: http://social.msdn.microsoft.com/Forums/eu/transactsql/thread/7f9e2793-54cb-4032-9abc-60063d268ab6 (complaining about Joe Celko) and http://social.technet.microsoft.com/Forums/en-US/transactsql/thread/a32b99e6-ab99-41fe-877b-cfb6470280c1 (someone bringing up the same comments as Joe Celko in an inflammatory manner). In the latter thread, Michael Hotek makes the following comments:

Good comments. Brian Tkatch is sort of similar to my instinct, and that shown on sqlzoo.net - not including a table prefix in columns, where Adam Tappis and Phil Brammer come down on the other side. Above technet thread also discusses advantages/disadvantages of underscores versus CamelCase - with Joe Celko noting that underscores are easier to read than CamelCase. As far as writing out words fully rather than abbreviating, Joe Celko notes that this can lead to more typos.