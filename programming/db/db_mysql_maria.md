

## 2018-03 #linkdump !random-postgres-consolidation -- mariadb comparison
https://mariadb.com/kb/en/library/compatibility-differences/ - lots here

### community involvement
"However, the commit log of 5.7 shows that all contributors are @oracle.com. Most commit messages reference issue numbers that are only in an internal tracker at Oracle and thus not open for public discussion. There are no new commits in the latest 3 months because Oracle seems to update the public code repository only in big batches post-release"
    - https://seravo.fi/2015/10-reasons-to-migrate-to-mariadb-if-still-using-mysql

### instrumentation
"In matter of instrumentation, MySQL has implemented performance_schema tables for replication, while MariaDB has only a tiny fraction of the instrumentation available in regular tables"
    - https://www.percona.com/blog/2017/11/02/mysql-vs-mariadb-reality-check/#comment-10968619
