
settling on alembic from sqlalchemy as the best

[How do I test database migrations?](https://stackoverflow.com/questions/2332400/how-do-i-test-database-migrations)
* TODO: answer this, as I'm testing migrations these days... just test it the same as other db-touching ORM code but disable the test after running

### zero-downtime
https://news.ycombinator.com/item?id=16665447
* https://web.archive.org/web/20180325173338/https://samsaffron.com/archive/2018/03/22/managing-db-schema-changes-without-downtime

### alembic / sqlalchemy
TODO: merge python migrations over
http://alembic.zzzcomputing.com/en/latest/cookbook.html


### Patterns and approaches
[flyway Database Downgrade](https://github.com/flyway/flyway/issues/109) - prompted by liquibase and searched throughout for the best migration tool

*automatic migrations* (reflection etc)
Apparently Django generates SQL and migrations from the underlying code, which is what I'm looking for

doctrine seems to [generate automatic migrations](https://web.archive.org/web/20171007190237/https://cameronspear.com/blog/a-different-and-better-approach-to-database-migrations-in-php/); rails generates the schema automatically
for PHP, propel is main competitor but version 2 with Data Mapper pattern is in stasis

Loopback - nodejs framework from Strongloop which has automatic migrations I think; see https://strongloop.com/strongblog/refactoring-loopback-sql-connectors/
