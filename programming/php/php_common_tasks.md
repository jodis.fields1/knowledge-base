
https://stackoverflow.com/questions/12997204/post-vs-http-raw-post-data-vs-file-get-contentsphp-input
    * $_POST is automatically decoded from application/www-url-encoded


## see configs
* protip: php --ini to see where loaded from

## copy array (object.assign equivalent)
array_replace - https://stackoverflow.com/a/49320617/4200039

## find an element inside an associate array?
ex:
$progress_index = array_search($step_id, array_column($progress, 'step_id'),true);

array column pulls all the `step_id`s into an array
also see:
https://stackoverflow.com/questions/14224812/elegant-way-to-search-an-php-array-using-a-user-defined-function

## construction and validating objects
https://stackoverflow.com/questions/4604910/validating-the-construct-making-sure-vars-of-of-correct-type

**Plain objects**
[How to define an empty object in PHP](http://stackoverflow.com/a/34787619/4200039) - (object) [
    'key' => 'value'
]

is main way; cannot initialize properties with stdClass


**PHP todo or exercises**
Write a PHP function which logs out an object only to a certain level
Use SO Count level 3 of an array, Remove first levels of identifier in array and Convert Object To Array With PHP

**Code samples and core functions**

*Higher order functions*

functional programming - [underscore.php](http://brianhaveri.github.o/Underscore.php/) seems to be inactive

[php closure in anonymous function and reference &](http://stackoverflow.com/questions/26227515/php-closure-in-anonymous-function-and-reference) - be sure to use closures with pass by reference
