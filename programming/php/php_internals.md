**Internal architecture**
hack on PHP at least a little https://www.sitepoint.com/contributing-to-php-how-to-fix-bugs-in-the-php-core/

[Extension Writing Part I: Introduction to PHP and Zend](https://devzone.zend.com/303/extension-writing-part-i-introduction-to-php-and-zend/)

[Zend API: Hacking the Core of PHP](https://secure.php.net/manual/en/internals2.ze1.zendapi.php)

found thru https://stackoverflow.com/questions/2479402/calling-c-c-library-function-from-php?lq=1

**Contributors/Community**
https://externals.io/ - read the internals easier
Mostly on Github! Make a pull request!
Larry Garfield - "Director of Developer Experience at @Platformsh", comments on externals
Remi Collet - French Red Hat guy https://github.com/remicollet
Sara Golemon - major PHP PECL contributor over 15 years, C badass, https://github.com/sgolemon / pollita
Fabier Potencier - Symfony guy; see http://fabien.potencier.org/php-is-much-better-than-you-think.html
Jordi Boggiano - the Composer guy https://seld.be/notes/php-versions-stats-2017-1-edition
Brent Shaffer - Google's PHP evangelist
Nikita Popov - fairly hardcore at https://nikic.github.io/
Evert Pot - wrote https://evertpot.com/PHP-Sucks/ and came up with Sabre CalDAV server
Sam Dark - main Yii guy, raising money on Patreon
RFCs: bunch of these on the website, but apparently these are not tied to the internal mailing list? so you have to browse to find comments? horrible!