

TODO: https://github.com/phpstan/phpstan or https://github.com/exakat/php-static-analysis-tools

## ide
* https://youtrack.jetbrains.com/issue/WI-5304

## types
https://stackoverflow.com/questions/43399282/php-type-hinting-array-of-objects | PHP type hinting array of objects - Stack Overflow

https://medium.com/2dotstwice-connecting-the-dots/creating-strictly-typed-arrays-and-collections-in-php-37036718c921 | Creating strictly typed arrays and collections in PHP

https://github.com/swaggest/php-json-schema | swaggest/php-json-schema: High definition PHP structures with JSON-schema based validation

https://thephp.cc/news/2016/02/typed-arrays-
in-php | thePHP.cc - Typed Arrays in PHP

https://codereview.stackexchange.com/questions/149950/php-validate-json-objects-with-a-json-schema | validation - PHP Validate JSON objects with a JSON schema - Code Review Stack Exchange

https://stackoverflow.com/questions/4036708/how-can-i-validate-the-structure-of-my-php-arrays | How can I validate the structure of my PHP arrays? - Stack Overflow

### types - generic proposal
NOTE: all php typechecking is at runtime! ugh, per:
* https://wiki.php.net/rfc/arrayof | PHP: rfc:arrayof
* https://externals.io/message/100946 | RFC - Array Of for PHP 7 - Externals
* https://stitcher.io/blog/what-php-can-be

#### types - static checking dream
* why not build on the hack foundation?
    * "Now if the RFC was a plan for baking a compile-time static analysis engine into PHP itself, that would be interesting. But that is a massive project.-Rasmus" 
    * https://externals.io/message/101477#101563