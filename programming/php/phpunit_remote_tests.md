TODO: need to set this up for python

Being able to run tests on a keyboard shortcut has seriously sped up backend workflow.

The biggest issue I had was not in linking the containers - and it was returning a 255 exit code rather than throwing an error. Ended up just barely stepping in to realize that the db coudln't connect...

## skipping files
ended up just appending skip to the file...

## vscode support
* no support
* 
https://github.com/Microsoft/vscode/issues/29194 | Support for remote development scenarios · Issue #29194 · Microsoft/vscode
https://github.com/Microsoft/vscode/issues/43636 | PHP Remote Interpreter · Issue #43636 · Microsoft/vscode

## official
* probably could use an edit or two

### knowledgebase
https://confluence.jetbrains.com/display/PhpStorm/Running+PHPUnit+tests+over+SSH+on+a+remote+server+with+PhpStorm | Running PHPUnit tests over SSH on a remote server with PhpStorm - PhpStorm - Confluence
https://confluence.jetbrains.com/display/PhpStorm/Running+PHPUnit+tests+over+SSH+on+a+remote+server+with+PhpStorm#RunningPHPUnittestsoverSSHonaremoteserverwithPhpStorm-1.ConfiguringPHPUnitByRemoteInterpreter | Running PHPUnit tests over SSH on a remote server with PhpStorm - PhpStorm - Confluence
https://www.jetbrains.com/help/phpstorm/configuring-remote-interpreters.html | Configuring Remote PHP Interpreters - Help | PhpStorm
https://confluence.jetbrains.com/display/PhpStorm/Working+with+Remote+PHP+Interpreters+in+PhpStorm | Working with Remote PHP Interpreters in PhpStorm - PhpStorm - Confluence

### bugs
https://youtrack.jetbrains.com/issue/WI-13429 | Using phpunit from composer : WI-13429

## stackoverflow
* don't think these were too helpful

https://stackoverflow.com/questions/36412004/phpstorm-process-finished-with-exit-code-255 | php - PHPStorm - Process finished with exit code 255 - Stack Overflow
https://stackoverflow.com/questions/34370848/phpstorm-xdebug-process-finished-with-exit-code-255 | php - Phpstorm + xdebug = Process finished with exit code 255 - Stack Overflow
https://serverfault.com/questions/153634/php-exit-status-255-what-does-it-mean | php5 - PHP exit status 255: what does it mean? - Server Fault
https://stackoverflow.com/questions/18310259/cannot-find-phpunit-in-include-path-phpstorm | php - Cannot find PHPUnit in include path phpstorm - Stack Overflow
https://stackoverflow.com/questions/50611782/using-phpunit-in-phpstorm-using-docker | php - Using PHPUnit in PhpStorm using Docker - Stack Overflow
https://stackoverflow.com/questions/49455767/phpstorm-running-phpunit-database-tests-from-docker-container | PhpStorm: running PHPUnit database tests from Docker container - Stack Overflow
https://local.getflywheel.com/community/t/local-phpunit-phpstorm/6203 | Local + PHPUnit + PhpStorm - Workflow - Local by Flywheel

https://stackoverflow.com/questions/20689273/how-to-make-intellij-prompt-me-for-command-line-arguments | java - How to make IntelliJ prompt me for command line arguments - Stack Overflow

