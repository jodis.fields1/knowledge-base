
### 2018-05 dump

#### vscode
https://marketplace.visualstudio.com/items?itemName=felixfbecker.php-intellisense | PHP IntelliSense - Visual Studio Marketplace
https://github.com/bmewburn/intelephense | bmewburn/intelephense: Intellisense for PHP
https://github.com/neild3r/vscode-php-docblocker | neild3r/vscode-php-docblocker: Simple docblocker for php
https://github.com/HvyIndustries/crane | HvyIndustries/crane: PHP Intellisense/code-completion for VS Code

#### type hint arrays
https://stackoverflow.com/questions/33692299/how-to-document-the-types-of-arrays-in-phpdoc-when-those-arrays-are-maps-aka-h?rq=1 | php - How to document the types of arrays in PHPDoc, when those arrays are maps (aka hash tables)? - Stack Overflow
https://stackoverflow.com/questions/778564/phpdoc-type-hinting-for-array-of-objects/23072048#23072048 | php - PHPDoc type hinting for array of objects? - Stack Overflow
https://stackoverflow.com/questions/32611557/phpstorm-phpdoc-can-i-type-hint-individual-array-element | php - PHPStorm + PHPdoc - can I type hint individual array element? - Stack Overflow

https://tech.fleka.me/getting-rid-of-laravel-models-to-improve-performance-of-the-command-blackfire-io-profiling-53884fa6573e?gi=e2bf504a3b1a | Getting rid of Laravel models to improve performance of the command (Blackfire.io profiling)
https://youtrack.jetbrains.com/oauth?state=%2Fissue%2FWI-17116 | https://youtrack.jetbrains.com/oauth?state=%2Fissue%2FWI-17116
https://softwareengineering.stackexchange.com/questions/186439/is-declaring-fields-on-classes-actually-harmful-in-php | Is declaring fields on classes actually harmful in PHP? - Software Engineering Stack Exchange
https://stackoverflow.com/questions/30108757/phpstorm-tells-me-namespace-name-doesnt-match-psr-0-psr-4-project-structure | PhpStorm tells me: "Namespace name doesn't match PSR 0/PSR 4 project structure" - Stack Overflow
