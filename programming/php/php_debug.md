### debugging
TODO: figure out how to debug workers in PHP and try https://github.com/sidkshatriya/dontbug reverse debugger in PHP!
Also watch [Extend xdebug to support reverse debugging](https://bugs.xdebug.org/view.php?id=888) found thru [Can XDebug debug a PHP CLI script that uses pcntl_fork()](https://stackoverflow.com/questions/35926701/can-xdebug-debug-a-php-cli-script-that-uses-pcntl-fork)?

https://en.wikipedia.org/wiki/DBGp - apparently supported by all major languages per 

#### XDebug
Setup REPL with PHPStorm per http://stackoverflow.com/a/9183249/4200039

For CLI:
[Debug PHP command line script in PHPStorm](http://stackoverflow.com/questions/5315247/debug-php-command-line-script-in-phpstorm) - however [Remote Command-Line debugging with PHPStorm for PHP/Drupal (including drush)](http://randyfay.com/content/remote-command-line-debugging-phpstorm-phpdrupal-including-drush) seems to be the best but also see [New in 4.0: Easier debugging of remote PHP command line scripts](http://blog.jetbrains.com/webide/2012/03/new-in-4-0-easier-debugging-of-remote-php-command-line-scripts/) 
In my setup I ran the following to the web box: `export XDEBUG_CONFIG="idekey=PHPSTORM remote_host=10.0.2.2 remote_port=9000"` and `export PHP_IDE_CONFIG="serverName=clitest"` - may have to drop the debugger into `main/CORE/framework/yiic`

xdebug - has a custom var_dump function, but to dump that into error log have to do http://stackoverflow.com/a/139491/4200039

#### Other debugging and error reporting
*Errors*

[runtime configuration](http://php.net/manual/en/errorfunc.configuration.php) and the various [error handling functions](http://php.net/manual/en/ref.errorfunc.php)

default is: E_ALL & ~E_DEPRECATED & ~E_NOTICE & ~E_WARNING & ~E_STRICT & ~USER_WARNING

experimented with taking out strict, user_warning which gave me HTMLish errors

taking out warning gave me htmlentities(): charset `ASCII' not supported, assuming utf-8

notice gave me Undefined property: Csrf\CsrfToken::$token

i_error_logger.bix - 

check /var/log/php_errors.log

line 471: display_errors off

error_reporting is super important and shows as bitmask ([see constant mapping](http://www.php.net/manual/en/errorfunc.constants.php)) in phpinfo

per [How to get useful error messages in PHP?](http://stackoverflow.com/questions/845021/how-to-get-useful-error-messages-in-php) and phptherightway set /etc/php.ini to "On"

*Debuggers*

phpdbg, xdebug, and zend debugger - phpdbg is built into php 5.6 but it doesn't seem to be graphical, xdebug uses the DBGp protocal (?) so might be related

*REPL* - [Say Hello to Boris: A Better REPL for PHP](http://www.sitepoint.com/say-hello-to-boris-a-better-repl-for-php/)

raveren/kint - haven't tried it

[ccampbell/chromephp](https://github.com/ccampbell/chromephp) - nice, doesn't work with iframes

FirePHP is supposed to be a method, but I've gotten hung up on even installing it. The list of everything is at [http://www.christophdorn.com/Blog/2010/11/29/firephp-1-0-in-5-steps/](http://www.christophdorn.com/Blog/2010/11/29/firephp-1-0-in-5-steps/)

db:

http://us.php.net/manual/en/pdo.errorinfo.php [http://us.php.net/manual/en/pdostatement.errorinfo.php](http://us.php.net/manual/en/pdostatement.errorinfo.php)