
NEEDS: [Documenting associative array key](https://github.com/phpDocumentor/fig-standards/issues/7#issuecomment-361356407) - looks like this will be revived per https://github.com/phpDocumentor/fig-standards/issues/7#issuecomment-361356407
Also see:
https://stackoverflow.com/questions/37254695/type-hinting-for-properties-in-php-7 | attributes - Type hinting for properties in PHP 7? - Stack Overflow
https://stackoverflow.com/questions/2756974/do-php-interfaces-have-properties | oop - Do PHP interfaces have properties? - Stack Overflow
http://devpark.pl/what-changed-in-php-7-2/?lang=en | devpark.pl/what-changed-in-php-7-2/?lang=en

##PHP

[https://github.com/ziadoz/awesome-php](https://github.com/ziadoz/awesome-php)

[http://www.icosaedro.it/phplint/](http://www.icosaedro.it/phplint/) - built into PHP CLI

### linting
[php-cs-fixer versus PHP_CodeSniffer](https://github.com/squizlabs/PHP_CodeSniffer/issues/374#issuecomment-67450299)

### namespaces
[PHP Namespaces Explained](https://web.archive.org/web/20170606170137/https://daylerees.com/php-namespaces-explained/)
    * best so far

2018-09-26: PSR-0 versus PSR-4 https://stackoverflow.com/questions/24868586/what-is-the-difference-between-psr-0-and-psr-4 prompted by https://stackoverflow.com/questions/30108757/phpstorm-tells-me-namespace-name-doesnt-match-psr-0-psr-4-project-structure

### best practcies
* https://phptherightway.com/pages/The-Basics.html
* https://stackoverflow.com/questions/5438060/showing-all-errors-and-warnings

### flaws / warts
Previous Sadness Sadness Index Next Sadness »
Ternary operator associativity - http://phpsadness.com/sad/30

https://stackoverflow.com/questions/26431066/continue-2-and-break-in-switch-statement - continue is complicated

Is PHP a badly designed programming language? (Quora) -

found pointed to [PHP: a fractal of bad design](http://eev.ee/blog/2012/04/09/php-a-fractal-of-bad-design/) and also see [http://phpsadness.com/](http://phpsadness.com/)

print always returns 1

string interpolation is generally doublequotes, but if accessing arrays or something use braces such as {$row['User_ID']} - note not using quotes within [double quotes is another option](http://stackoverflow.com/questions/11821672/string-interpolation-with-an-array), but avoid that complexity

*Frameworks* - SLIM looks kind of like node, author wrote phptherightway.com

*Security*

[http://phpsecurity.readthedocs.org/en/latest/](http://phpsecurity.readthedocs.org/en/latest/) interesting book

[Suhosin comes back from the dead, bringing security to newest PHP versions](https://ckon.wordpress.com/2014/07/19/suhosin-newest-php-security/) (2014) - disabled eval and regex /e tag

### profiling
[xhprof](http://www.sitepoint.com/the-need-for-speed-profiling-with-xhprof-and-xhgui/) - [directions at sitepoint](http://www.sitepoint.com/the-need-for-speed-profiling-with-xhprof-and-xhgui/)