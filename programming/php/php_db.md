SQL, ORM, etc

### SQL
* Culture of strings rather than real data types: see 
  * [PDO, PostgreSQL and bool](https://github.com/zendframework/zf2/issues/7284) - follows up an earlier report at [Boolean type with PostgreSQL](http://www.yiiframework.com/forum/index.php/topic/32334-boolean-type-with-postgresql/) and [Typified FALSE value in PDO (Postgres) bug](https://github.com/yiisoft/yii/issues/779)
  * [Why not PDO_MySQL return integer?](http://stackoverflow.com/questions/16129432/why-not-pdo-mysql-return-integer) - `PDO::ATTR_STRINGIFY_FETCHES` because  'database engines can handle very large numbers (Oracle, for instance, allows 38-digit numbers) and PHP cannot'
 * 

 ### ORM
 Doctrine - is it dying? https://www.tomasvotruba.cz/blog/2017/03/27/why-is-doctrine-dying/