
Official work:
https://wiki.php.net/rfc/consistent_function_names - rejected
[good point on externals.io by Larry Garfield](https://externals.io/message/84102#84202):
"will just add to the list of things people make fun of PHP for instead of reduce it"

### Wrappers (e.g. Underscore.php)
bdelespierre of underscore.php says he uses the Laravel Collections lib

https://github.com/maciejczyzewski/bottomline - looks alive
https://github.com/bdelespierre/underscore.php/ - slightly more active
https://github.com/Anahkiasen/underscore-php/ - clearly dead
https://github.com/tajawal/lodash-php - almost nobody is using it
Laravel helpers
https://github.com/phacility/libphutil - helpers used with Phabricator
https://github.com/brandonwamboldt/utilphp/ - 1000+ stars

### PHP standard lib

*To-be-studied*

[Hacking with PHP](http://www.hackingwithphp.com/) - free

[Functional Programming in PHP](https://www.simonholywell.com/post/2014/08/functional-programming-in-php---the-book/) - 

[Function Handling](http://php.net/manual/en/book.funchand.php) - need to learn about this

[Reflection](http://php.net/manual/en/book.reflection.php) - cool API

[HTTP](http://php.net/manual/en/book.http.php) - ?

[Stream Functions](http://php.net/manual/en/ref.stream.php) - context [option in opendir](http://www.tutorialspoint.com/php/php_function_opendir.htm) is affected by these somehow

*Core*

[Variable handling](http://php.net/manual/en/book.var.php) - 

[Class/Object Information](http://php.net/manual/en/book.classobj.php) - 

[String Functions](http://php.net/manual/en/ref.strings.php) - strings!

[PCRE](http://php.net/manual/en/book.pcre.php) - handy, I guess ereg isn't used much 

[Filesystem Functions](http://php.net/manual/en/ref.filesystem.php) - lots of good stuff

[Directory Functions](http://php.net/manual/en/ref.dir.php) - not sure how this relates

*Misc*

Fileinfo - libmagic method to determine file type

Mail - builtin

[Process Control Extensions](http://php.net/manual/en/refs.fileprocess.process.php) - lots of interesting stuff

[Multibyte String](http://php.net/manual/en/funcref.php) - might be outdated way to handle UTF

[Other Basic Extensions](http://php.net/manual/en/funcref.php) - lots of random stuff, such as the ["Misc"](http://php.net/manual/en/book.misc.php) functions (connection, sleep, define, etc)

[Other Services](http://php.net/manual/en/funcref.php) - sockets, zero-mq, cURL

[Apache Solr](http://php.net/manual/en/book.solr.php) - maybe we use?

[Apache ](http://php.net/manual/en/book.apache.php)- that's our SAPI (Server API)

[Session](http://php.net/manual/en/intro.session.php) - session support, Postgres-specific is much faster