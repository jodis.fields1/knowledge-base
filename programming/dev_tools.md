See terminal.md

Most of these notes are in the dotfiles repo.

## Companies
Sourcerely - Grammarly for code

https://about.sourcegraph.com/

## frontend
* SEE frontend/chrome_devtools.md
* firefox devtools?
* https://stackoverflow.com/questions/41533660/how-to-console-log-this-function - use this `console.log() ||` all the time

### online sandbox
jsbin, jsfiddle, etc - prefer jsbin

StackBlitz, codesandbox - in a different league?
* Stackblitz - developer didn't like hearing about codesandbox https://news.ycombinator.com/item?id=14925957 - open-soure where?
* Codesandbox - Kent Dodds uses it https://codesandbox.io/s/github/kentcdodds/downshift-examples/tree/master/?module=%2Fsrc%2Fother-examples%2Fgmail%2Findex.js&moduleview=1

stackoverflow's product - ??

### API development
Insomnia - ??
Jetbrains Intellij - https://blog.jetbrains.com/phpstorm/2017/09/editor-based-rest-client/
Advanced Rest Controller - seems good, using it
Postman - supposed leader, couldn't get it to work