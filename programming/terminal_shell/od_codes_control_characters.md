
dotfiles (master<>)$ god -t x -
^[b,.'

NOTE: use od for ascii to octal conversion, e.g.
echo 10 | od -a -t x1

NOTE: remember that in certain cases, the decimal looking value can be hex per https://stackoverflow.com/questions/45123987/integer-constants-in-pic-assembly-decimal-vs-hex
## control character representation
^? - delete, ASCII octal 127
^@ - NULL, ASCII octal 0
^H - backspace, ASCII octal 8

NOTE: TEXTUAL REPRESENTATION IS NOT REAL - verify with `od` after geneting the special character with CTRL+V trick:
echo "^?" | od -cd -t x1
"^A through ^Z, pronounced “Ctrl-FOO”, are represented by the bytes with values 1 through 26. So when I say that c_cc[VINTR] is equal to ^C by default, that’s actually just the number 3"
    - per https://web.archive.org/web/20180613173929/https://blog.nelhage.com/2009/12/a-brief-introduction-to-termios-termios3-and-stty/ 
    - also discovered in https://unix.stackexchange.com/questions/217010/search-and-replace-control-characters-m-i-in-vi#comment368468_217011
    - learned about how these are generated from https://en.wikipedia.org/wiki/Control_character#In_ASCII in the link to Backspace - "control code could also be accessed by pressing Control-H, as H is the eighth letter of the Latin alphabet"


according to https://unix.stackexchange.com/questions/217010/search-and-replace-control-characters-m-i-in-vi#comment368468_217011 these control characters aren't really two characters at all...

### universal access
https://superuser.com/a/1301422/457084 demonstrates the trick of sending ^W (CTRL+W) hex code for universal mac / linux - probably relies on termios CTRL+W binding?

## finding codes
cool trick: pipe the codes from infocmp into tput per https://stackoverflow.com/a/7783928/4200039 e.g.
`tput kf12 | od -cb -tx1`

keep referring to https://www.asciitable.com/
the main trick is CTRL+V to get the literal per http://www.ibb.net/~anne/keyboard/keyboard.html#trick
    * this comes from termios tho I think - `lnext = ^V;`; from stty -a

### use od
key is to use the bytes, e.g.:
`od -N 4 -t a -` (for accepting input)

next key is to output multiple lines, e.g.:
echo "Hello    ^C" | od -cba -t x1

outputs several lines with different formats!

on osx can use GNU version with `god`

per https://en.wikipedia.org/wiki/Od_(Unix)
~~perhaps god -t x <<< $(echo "^[") | head~~

### 2018-07
asdfaewaf
0000000 2e2c621b 0a0a0a27 6473610a 77656166
^[[A^[[A^C
dotfiles (master<>)$ god -N 1-t x -
god: invalid suffix in -N argument '1-t'
dotfiles (master<>)$ god -N 1 -t x -
^[b
0000000 0000001b
0000001
dotfiles (master<>)$ b
-bash: b: command not found
dotfiles (master<>)$ god -N 1 -t a -
^[b
0000000 esc
0000001
dotfiles (master<>)$ b
-bash: b: command not found
dotfiles (master<>)$ god -N 2 -t a -
^[b
0000000 esc   b
0000002
dotfiles (master<>)$
dotfiles (master<>)$ god -N 4 -t a -
^[b

0000000 esc   b  nl  nl
0000004
dotfiles (master<>)$ god -N 4 -t a -
 ^[b
0000000  sp esc   b  nl
0000004
dotfiles (master<>)$ god -N 4 -t a -
a


0000000   a  nl  nl  nl
0000004
dotfiles (master<>)$ god -N 4 -t a -
^[b

0000000 esc   b  nl  nl
0000004
dotfiles (master<>)$ god -N 4 -t x -
^[b

0000000 0a0a621b
0000004
dotfiles (master<>)$ god -N 4 -t o -
^[b

0000000 01202461033
0000004
dotfiles (master<>)$ god -N 4 -t x -




0000000 0a0a0a0a
0000004
dotfiles (master<>)$ god -N 4 -t x -
^A


0000000 0a0a0a01
0000004
dotfiles (master<>)$ god -N 4 -t x -
^[a

0000000 0a0a611b
0000004
dotfiles (master<>)$ god -N 4 -t x -
^[aa
0000000 0a61611b
0000004
dotfiles (master<>)$ god -N 2 -t x -
^A
0000000 00000a01
0000002
dotfiles (master<>)$ god -N 1 -t x -
^A
0000000 00000001
0000001
dotfiles (master<>)$
dotfiles (master<>)$ god -N 2 -t x -
^A
0000000 00000a01
0000002
dotfiles (master<>)$ god -N 3 -t x -



0000000 000a0a0a
0000003
dotfiles (master<>)$ god -N 3 -t x -
^A
.
0000000 002e0a01
0000003
dotfiles (master<>)$
dotfiles (master<>)$ god -N 3 -t x -
^[

0000000 000a0a1b
0000003