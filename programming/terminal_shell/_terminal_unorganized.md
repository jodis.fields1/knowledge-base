https://news.ycombinator.com/item?id=18478654 | Building your own shell using Rust | Hacker News
https://github.com/aignas/st | Page not found · GitHub

## questions
why ANSI escape code when ANSI is not an established character set?
https://en.wikipedia.org/wiki/ANSI_escape_code | ANSI escape code - Wikipedia
http://www.aivosto.com/articles/control-characters.html#list_ISO8859 | Control characters in ASCII and Unicode

## utf-8

https://news.ycombinator.com/item?id=11248847 | Xterm(1) now UTF-8 by default on OpenBSD | Hacker News
https://bluesock.org/~willg/dev/ansi.html | ansi codes


## $TERM
https://unix.stackexchange.com/questions/43945/whats-the-difference-between-various-term-variables | terminal - What's the difference between various $TERM variables? - Unix & Linux Stack Exchange

## keyboard shortcuts
https://gitlab.com/gnachman/iterm2/issues/3753#note_3583444 | Request: Make alt-arrow escape sequences default on OS X version of iTerm 2 (#3753) · Issues · George Nachman / iterm2 · GitLab
https://github.com/xtermjs/xterm.js/blob/290c95ad71f8b1264139a7ed6f070c4741a8987e/src/core/input/Keyboard.ts#L135 | xterm.js/Keyboard.ts at 290c95ad71f8b1264139a7ed6f070c4741a8987e · xtermjs/xterm.js
https://apple.stackexchange.com/questions/16135/remap-home-and-end-to-beginning-and-end-of-line | macos - Remap "Home" and "End" to beginning and end of line - Ask Different
https://superuser.com/questions/1233641/linux-chromium-enable-emacs-style-key-bindings | google chrome - Linux Chromium: Enable Emacs Style Key Bindings - Super User
https://unix.stackexchange.com/questions/65434/map-superleftright-to-home-end | keyboard - Map Super+[Left|Right] to Home/End - Unix & Linux Stack Exchange
https://askubuntu.com/questions/43049/set-fn-arrow-to-represent-home-and-end | keyboard - set fn + arrow to represent 'home' and 'end' - Ask Ubuntu
https://superuser.com/questions/428945/defining-keyboard-shortcuts-involving-the-fn-key#comment2026404_439929 | linux - Defining keyboard shortcuts involving the Fn key - Super User
http://www.thinkwiki.org/wiki/How_to_get_special_keys_to_work | How to get special keys to work - ThinkWiki
https://jblevins.org/log/kbd | (Emacs) Keyboard Shortcuts for Editing Text Fields in OS X
https://defkey.com/what-means/ctrl-left | What does the Ctrl + Left arrow keyboard shortcut? ‒ defkey
https://github.com/sorin-ionescu/prezto/issues/278#issuecomment-8399602 | Add ctrl-arrow key bindings · Issue #278 · sorin-ionescu/prezto
https://askubuntu.com/questions/124815/how-do-i-enable-emacs-keybindings-in-apps-such-as-google-chrome/918962#918962 | gtk - How do I enable Emacs keybindings in apps, such as Google Chrome? - Ask Ubuntu

## cursor position
https://github.com/xtermjs/xterm.js/issues/1409 | How do I set the cursor position? · Issue #1409 · xtermjs/xterm.js
https://github.com/sindresorhus/ansi-escapes/issues/1 | Make cursor position a little more cross platform · Issue #1 · sindresorhus/ansi-escapes
https://github.com/xtermjs/xterm.js/issues/1526 | Save/restore cursor should track more properties · Issue #1526 · xtermjs/xterm.js

## unorganized
https://linux.die.net/man/1/xev | xev(1): print contents of X events - Linux man page
https://systemundertest.org/xterm/ | XTerm | System Under Test
https://systemundertest.org/ | System Under Test
https://www.xfree86.org/4.8.0/ctlseqs.html | Xterm Control Sequences
https://askubuntu.com/questions/17299/what-do-the-different-colors-mean-in-ls | command line - What do the different colors mean in ls? - Ask Ubuntu
https://github.com/chjj/tty.js/issues/177#issuecomment-390550291 | is this library still maintained? any better alternative? · Issue #177 · chjj/tty.js
https://github.com/chjj/blessed/issues/346#issuecomment-403161932 | Dead Project? · Issue #346 · chjj/blessed
https://vi.stackexchange.com/questions/16624/use-terminal-to-display-file-with-ansi-escape-codes | neovim - Use :terminal to display file with ansi escape codes - Vi and Vim Stack Exchange
https://asciinema.org/docs/how-it-works | How it works - asciinema
https://unix.stackexchange.com/questions/226412/how-can-i-get-the-raw-formatting-output-of-commands | osx - How can I get the raw formatting output of commands? - Unix & Linux Stack Exchange


## clear terminal
https://superuser.com/questions/555554/putty-clear-scrollback-from-commandline | PuTTY: clear scrollback from commandline - Super User
https://askubuntu.com/questions/25077/how-to-really-clear-the-terminal | command line - How to really clear the terminal? - Ask Ubuntu
https://www.gnu.org/software/termutils/manual/termutils-2.0/html_mono/tput.html | Portable Terminal Control From Scripts

## outdated / deprecated key codes
ask maintainer about usefulness of these old codes and formal deprecation process

https://vt100.net/docs/vt100-ug/chapter3.html#S3.3.3 | Digital VT100 User Guide: Programmer Information
http://www.columbia.edu/kermit/vt320-keyboard.jpg | vt320-keyboard.jpg (1648×844)
https://www.google.com/search?q=VT220+f14&hl=en&source=lnms&tbm=isch&sa=X&ved=0ahUKEwjF78im8c_eAhUjNX0KHVINDBoQ_AUIEygB&biw=1440&bih=767#imgrc=SbFJ-nt-zCK2FM: | VT220 f14 - Google Search
https://www.censoft.com/support/help/ttus_help/ttusVT320VT220_Keyboard_Layout.htm | VT320/VT220 Keyboard Layout
https://cboard.cprogramming.com/c-programming/67435-how-can-clear-screen.html | How can this clear the screen?
https://unix.stackexchange.com/questions/211923/escape-sequences-for-function-keys-f1-f12-especially-f11-and-f12 | terminal - Escape sequences for function keys (F1-F12), especially F11 and F12 - Unix & Linux Stack Exchange