NOTE: `env -i your_command` to run a clean environment for your command

SEE ALSO: bash.md

* per https://unix.stackexchange.com/questions/48994/how-to-run-a-program-in-a-clean-environment-in-bash; REMEMBER THIS

https://thoughtbot.com/upcase/videos/mastering-the-shell-overview
1. fix fasd-plugin for fisœh to use vim 
2. explore fzf https://github.com/junegunn/fzf
3. https://www.reddit.com/r/linux/comments/5pbxiv/the_ultimate_cli_cheat_sheet_that_you_will_ever/


## tldr or bropages tricks protips
* https://tldr.ostera.io/

## global env pet peeve
NOTE: mostly solved by nixos / nix
Well, some of them only matter for CLI commands. For example the Python CLI has 16 environment variables https://docs.python.org/3/using/cmdline.html#envvar-PYTHONSTARTUP

*Apple* <br/>
More generally look into this method: http://apple.stackexchange.com/questions/4447/how-to-listen-for-an-application-launch-event-on-mac-os-x

also see:
http://stackoverflow.com/questions/22693509/how-can-i-launch-an-applescript-application-and-run-shell-scripts-from-another-a
also see:
http://stackoverflow.com/questions/3695213/mac-os-x-monitor-app-launch?rq=1
and unanswered: http://stackoverflow.com/questions/24703082/detecting-agent-app-launch-and-termination-without-polling-on-mac-os-x


## shell
taskwarrior means more focus on the shell...

TODO: continue to set up ntfy
* stopped at https://github.com/dschep/ntfy/issues/171
* note that it integrates with pushover https://github.com/dschep/ntfy/issues/98
* seen at https://apple.stackexchange.com/questions/9412/how-to-get-a-notification-when-my-commands-are-done/238869#238869 and https://superuser.com/questions/345447/how-can-i-trigger-a-notification-when-a-job-process-ends
TODO: fix up w3m or find better one in alternativeto
* fix up w3m for text-editor browsing https://www.reddit.com/r/vim/comments/17dg43/textmode_web_browser_with_vimperatorlike/
* Per Wikipedia talk I found that it's maintained by the debian team now
* sounds like lynx is better than w3m from https://news.ycombinator.com/item?id=17495207; also brow.sh is newer

* I paste text a lot, and 90% of the time I use Cmd+Shift+V to paste plain text in Google Chrome per [Tip: Just the text, please!](http://chrome.blogspot.com/2010/09/tip-just-text-please.html) .
1. z is awesome (note you can easily miss the . before executing its shell script), but you can also try fasd
 * I reviewed [FASD - the Commandline Shortcuts I Always Wanted](http://blog.xargs.io/2012/07/fasd-the-commandline-shortcuts-i-always-wanted/) and it showed me fasd -e [command] - note that I probably need zsh and word completion here.
 * Look into a go command as discussed [Upgrading from Autojump to Fasd](http://blog.patshead.com/2013/02/upgrading-from-autojump-to-fasd.html?r=related)
3. use dot aliases per [this](http://www.thegeekstuff.com/2008/10/6-awesome-linux-cd-command-hacks-productivity-tip3-for-geeks/)
* Use a split-screen! If you're looking at a man page, make sure you're splitting the screen and trying the commands in your other terminal!! If you're staring at code and jumping back and forth, open that split screen!
* Find, grep, and ack all the things: man <manpage> | col -b | grep <term> to avoid the issues with formatting

Use Sublime Text file searcher - CMD+SHIFT+F. Use the exclude filter and do a recursive globbing (**/**) to exclude all the libs folder and *.min.js to remove all the minified ones.

MAYBE: look into https://github.com/eivind88/prm

### practice TODO
TODO
https://www.digitalocean.com/community/tutorials/an-introduction-to-linux-i-o-redirection - best explanation of tee "Tee redirects standard input to both standard output and one or more files"

watch playterm videos? https://linux.slashdot.org/story/11/09/24/0338244/playterm-a-new-way-to-improve-command-line-skills

http://en.wikipedia.org/wiki/Comparison_of_command_shells

https://www.commandlinefu.com/
watch people on playterm: https://linux.slashdot.org/story/11/09/24/0338244/playterm-a-new-way-to-improve-command-line-skills

see comment for nip
