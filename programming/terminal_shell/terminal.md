See productivity/keyboard_shortcuts !!

TODO: work on Hyperterm (or Terminus?) before publishing this
TODO: work with xonsh before publishing too...
* [Build Your Own Shell using Rust](https://www.joshmcguigan.com/blog/build-your-own-shell-rust/)
* [Tutorial - Write a Shell in C](https://brennan.io/2015/01/16/write-a-shell-in-c/)
* [Anatomy of a shell](https://news.ycombinator.com/item?id=18777909)

NOTE: also see the iTerm config in the dotfiles
[Text-Terminal-HOWTO](http://www.tldp.org/HOWTO/Text-Terminal-HOWTO.html)

this was somewhat motivated by the desire for achieving what it turns out that the iTerm "Natural text editing" preset accomplished (per https://gitlab.com/gnachman/iterm2/issues/3753#note_137868890), apparently by setting commands compatible with bash readline defaults.

## terminal
long-term hyper - but lack of scrollback search means that in the meantime...
(reject konsole, alacritty)
* NOTE: easy workaround for lack of scrollback search - copy into a text file - noted suggestion https://github.com/zeit/hyper/issues/140

### GNOME terminator (cross-platform)
* zero commits in all of 2018
* https://terminator-gtk3.readthedocs.io/en/latest/licensing.html - not easily discoverable
* plugins: https://terminator-gtk3.readthedocs.io/en/latest/plugins.html
* themes: https://github.com/EliverLara/terminator-themes
* [How to Select-All in Terminator](https://askubuntu.com/questions/697171/how-to-select-all-in-terminator) -

### complementary apps
#### less
TODO: [Less and Grep: Getting colored results when using a pipe from grep to less](https://superuser.com/questions/36022/less-and-grep-getting-colored-results-when-using-a-pipe-from-grep-to-less)
https://github.com/tcr/rager - rust alternative

**Common terminals**  
e.g. xterm but also the [builtin linux terminal](https://superuser.com/questions/361377/what-is-arch-linuxs-default-terminal)

see what ncurses/terminfo terminals are supported with https://stackoverflow.com/questions/12345675/screen-cannot-find-terminfo-entry-for-xterm-256color#comment77115648_20212351 - as of 10.11.5 there's /usr/lib/libncurses.dylib

**stty**  
apparently controls sending CTRL+C (SIGINT) to the process per [SIGTERM with a keyboard shortcut](https://superuser.com/questions/343031/sigterm-with-a-keyboard-shortcut)?

**Escape sequences**  
Inside your terminal, most importantly, is a shell. These two things can talk to each in some ways.

First, READ:

  * [What type of sequences are escape sequences starting with “\033\]”?](https://askubuntu.com/questions/831971/what-type-of-sequences-are-escape-sequences-starting-with-033/832061)
  * followed by [Where do I find a list of terminal key codes to remap shortcuts in bash?](https://unix.stackexchange.com/questions/76566/where-do-i-find-a-list-of-terminal-key-codes-to-remap-shortcuts-in-bash)

This confused me. What is `Esc+` in iTerm - is that a literal escape? Also, in the readline bind-key which drives built-in Emacs keyboard shortcuts, you might see something like this:
```
bind -P | grep '^backward-kill-word'
backward-kill-word can be found on "\e\C-h", "\e\C-?".
```

If you use the literal escape for `\e`, backward-kill-word will work. (It is a two stage shortcut: tap `Esc`, then lift your finger and tap `CTRL+H`). But if you use an Option key mapped to `Esc+`, it will not work. If you use CTRL+[ to generate the ESC ([per Wikipedia](https://en.wikipedia.org/wiki/Escape_character#ASCII_escape_character)) it will also work!

for echo, need -e - e.g. `echo -e "\e[32mhello"` or `echo -e "\\E[32mhello"`; however, noticed that for printf only \e worked; also may want to use GNU versions on mac (gecho, gprintf, etc)

### standards / terminfo / termcap / ncurses
2019-02-10: glanced at tput (https://github.com/mirror/ncurses/blob/b60a2772d9f149d8e900c1d5a09a53a56a0837a8/progs/tput.c#L281) - code is a bit confusing; https://github.com/wkoszek/ncurses_guide might be a good intro

terminfo (Dickey) would not include kitty in terminfo https://github.com/kovidgoyal/kitty/issues/879#issuecomment-419686348

the Esc[Line;ColumnH stuff at http://ascii-table.com/ansi-escape-sequences.php was/is confusing

"A terminal control code is a special sequence of characters that is printed (like any other text). If the terminal understands the code, it won't display the character-sequence, but will perform some action"
  - http://wiki.bash-hackers.org/scripting/terminalcodes

NOTE: do not hardcode color sequences (vary by terminal?): use tput per [How can I print text in various colors?](https://mywiki.wooledge.org/BashFAQ/037)

ISO/IEC 6429 is the main standard per https://en.wikipedia.org/wiki/ANSI_escape_code#History
  * http://pubs.opengroup.org/onlinepubs/7908799/xcurses/terminfo.html is a sort of standard

"Apparently all these standards are huge and almost unimplementable. It seems nobody ever implements “ANSI” fully or consistently."
  - per http://redgrittybrick.org/standards.html
  - has some good references

#### unstandardized sequences
turns out SHIFT+SPACE is not part of the typical standard so often unsupported
originally discovered from vim (https://stackoverflow.com/questions/279959/how-can-i-make-shiftspacebar-page-up-in-vim), and further commented at https://github.com/zeit/hyper/issues/2602#issuecomment-403312098 referencing a detailed explanation of what's happening https://emacs.stackexchange.com/questions/1020/problems-with-keybindings-when-using-terminal/13957#13957

also see https://gitlab.com/gnachman/iterm2/issues/3519

#### viewing my terminfo
kLFT5=\E[1;5D - per https://unix.stackexchange.com/questions/262129/what-is-the-gnome-terminal-ansi-escape-sequence-for-ctrl-arrow-s this indicates CTRL+leftarrow?
  * terminfo database is not clear on this, altho kLFT4 is documented there...
  * "kDN, kDN5, kDN6, kLFT5, kLFT6, kRIT5, kRIT6, kUP, kUP5, kUP6 ... The uppercase names are made up, since there are no standards that apply..."
  * "The numbers correspond to the modifier parameters documented in Xterm ..." - per https://invisible-island.net/xterm/terminfo.html
    * 2       Shift
    * 3       Alt
    * 4       Shift + Alt
    * 5       Control
    * 6       Shift + Control
    * 7       Alt + Control
    * 8       Shift + Alt + Control
common capabilities: https://wiki.archlinux.org/index.php/Bash/Prompt_customization#Common_capabilities

interesting comments on iTerm at https://invisible-island.net/ncurses/terminfo.ti.html
man terminfo explains some of the parameters (see below)
from https://www.tldp.org/HOWTO/Text-Terminal-HOWTO-16.html learned that ("quick way to inspect") infocmp will dump relevant terminfo - but did not mean anything to me? output:
tried to view /usr/share/terminfo/78/xterm-256color but it seemed to be binary (no comments)
```
usr/share/terminfo/78/xterm-256color
xterm-256color|xterm with 256 colors,
	am, bce, ccc, km, mc5i, mir, msgr, npc, xenl,
...
```
##### a capability
> The am capability tells whether the cursor sticks at the right edge of the screen when text is output, but this does not necessarily apply to a cuf1 from the last column
  - from man terminfo

##### bce capability
The -bce variants support "background color erase", which means that
areas of the screen can be cleared by erasing them rather than filling
them with space characters. This may be faster, and prevents extra
spaces when doing copy-paste from the terminal.
  - from https://gist.github.com/KevinGoodsell/744284


##### cup
TODO: look into using cup to mouse click into input zone
  * NOTE: iTerm can already do this!? Option+click on input
"cup" appears to stand for "cursor position", and is another
terminfo capability used for moving the cursor to an arbitrary location
on the screen. smcup is used to prepare the terminal for cup usage, and
rmcup is used to reset it.
  - [The Trouble With Terminals](https://gist.github.com/KevinGoodsell/744284) by KevinGoodsell
##### history / other
"An older database is termcap(5), while terminfo(5) is a newer database" per https://stackoverflow.com/a/7783928/4200039
  * "Linux console isn't even close to vt220. It's a subset of (the less capable) vt100. Each of the answers to this question has at least one error" from Thomas Dickey https://stackoverflow.com/questions/7767702/what-is-terminal-escape-sequence-for-ctrl-arrow-left-right-in-term-linu#comment79739160_7767702
[Can Terminal.app be made to respect ANSI escape codes?](https://stackoverflow.com/a/25879581/4200039) - ANSI codes aren't that standard apparently relative to terminfo

## Colors
### color standards?
use tput to get colors (and other codes) due to terminals using different codes

"In development of xterm over the past 20 years, it incorporated ANSI (8) colors, adapted the aixterm feature (16) colors, added extensions for 88- and 256-colors. Much of that has been adopted by other developers"
  - https://unix.stackexchange.com/a/269195/108198
[Colors in terminal](http://jafrog.com/2013/11/23/colors-in-terminal.html) - awesome discussion, also see [Colours and formatting in Gnome/Ubuntu's Terminal](http://www.growingwiththeweb.com/2015/05/colours-in-gnome-terminal.html) for Linux-oriented discussion

[TrueColour.md](https://gist.github.com/XVilka/8346728) - interesting discussion of the evolution of a new standard...

https://askubuntu.com/questions/17299/what-do-the-different-colors-mean-in-ls

why are 256 colors called cubes? irssi has a /cubes command, also see https://askubuntu.com/a/828422/457417