the linux pty driver is a major part of the puzzle

it binds a bunch of keyboard shortcuts:
[What are the keyboard shortcuts for the command-line?](https://unix.stackexchange.com/a/255735/108198)


man termios
    * glanced at BSD version

## stty options / commands
gstty to access GNU version on mac

[What the Shell? Getting Started with Unix](https://web.archive.org/web/20180704211654/http://www.peachpit.com/articles/article.aspx?p=659655&seqNum=13)
    * recommends `stty sane`

backspace and delete seem to be ^? or ^H or something...

## 2018-07 dive
3-part series: \
* part 3 https://web.archive.org/web/20180613173927/https://blog.nelhage.com/2010/01/a-brief-introduction-to-termios-signaling-and-job-control/
* part 2 https://web.archive.org/web/20180613173929/https://blog.nelhage.com/2009/12/a-brief-introduction-to-termios-termios3-and-stty/
    * detour about ioctl pointed met to https://web.archive.org/web/20171221031413/https://opensourceforu.com/2011/08/io-control-in-linux/
        * author runs sysplay.in - see [Writing your First Linux driver in the Classroom](https://sysplay.in/blog/2013/03/)
* part 1 - https://web.archive.org/web/20180613173930/https://blog.nelhage.com/2009/12/a-brief-introduction-to-termios/
