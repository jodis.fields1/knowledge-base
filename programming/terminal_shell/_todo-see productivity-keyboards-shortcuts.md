Can't quite merge them because keyboard shortcuts aren't only applicable to the terminal / shell...

TODO: merge keyboard shortcuts for shell specifically here

4 providers:
system
stty
terminal
shell
# terminal
2018-07: replaced my somewhat hacky `^[[1;5D` (the literal code for kLFT5 per `tput kLFT5 | od -cb -tx1`) with Esc+b ...

# shell (bash)
readline issues unsetting - answered https://stackoverflow.com/a/51181899/4200039

you can do conditionals as seen in https://unix.stackexchange.com/a/197016/108198 and https://github.com/sente/dotfiles/blob/master/.inputrc#L117
# stty
`stty werase undef` per https://stackoverflow.com/a/10980808/4200039