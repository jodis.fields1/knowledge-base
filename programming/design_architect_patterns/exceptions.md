Exceptions are surprisingly controversial and misunderstood. For example, the question [Is it good practice to use exceptions and throw/catch exceptions rather than returning 0 or 1?](http://programmers.stackexchange.com/questions/133335/handling-errors-in-php-when-using-mvc) has two highly voted answers with the exact opposite conclusion!

The language Go, for example, famously does not use exceptions. It uses numeric error codes! [link to explanation which discusses trade-off]

Meanwhile, most modern object-oriented languages use exceptions, and they work in largely similar ways.

It's worth exploring how that works exactly to ensure that the details are understood. 

1. [Propagating Exceptions](http://www.csit.parkland.edu/~mbrandyberry/CS1Java/Lessons/Lesson19/PropagatingExceptions.htm) or [what is exception propagation?](http://stackoverflow.com/questions/10633664/what-is-exception-propagation)
2. [What are the best practices for catching and re-throwing exceptions?](http://stackoverflow.com/questions/5551668/what-are-the-best-practices-for-catching-and-re-throwing-exceptions/20189759#20189759) - important tips


Note that a helpful trick is sometimes catching and then rethrowing the exception to continue to propagate it up (exception chaining?).