See blog post?
https://www.quora.com/What-are-the-best-conferences-for-software-architecture

https://qconsf.com/ - looks pretty cool

see [The Clean Architecture (2012)](https://news.ycombinator.com/item?id=14218490)

See frontend_general_overview and atomic design for frontend

For backend, blog post, dependency injection, nestjs and also this:

### link dump
https://www.quora.com/Which-is-better-Laravel-or-Django-for-large-applications-development | (1) Which is better, Laravel or Django, for large applications development? - Quora
https://hackr.io/blog/django-vs-laravel#To_Wrap_up | Django Vs Laravel: Which framework to choose in 2018?

#### laravel architecture
https://www.w3schools.in/laravel-tutorial/application-directory-structure/ | Laravel Application Directory Structure
https://laravel.com/docs/5.7/packages | Package Development - Laravel - The PHP Framework For Web Artisans
https://www.w3schools.in/laravel-tutorial/application-directory-structure/ | Laravel Application Directory Structure

#### flask
https://github.com/pallets/flask/issues/2626#issuecomment-397298677 | Official Guide on how to structure Flask application · Issue #2626 · pallets/flask
https://github.com/gofynd/flask-full | gofynd/flask-full: starter/boilerplate flask application with celery, mongoengine, signals, shell commands, swagger api docs and sphinx docs integration
https://lepture.com/en/2018/structure-of-a-flask-project | Structure of a Flask Project - Just lepture
https://github.com/pallets/flask/issues/2626#issuecomment-397298677 | Official Guide on how to structure Flask application · Issue #2626 · pallets/flask
