NOTE: turns out php-group repo has nothing

Still figuring this out precisely - see my blog post

### Python
http://python-dependency-injector.ets-labs.org/introduction/di_in_python.html - good explanation

### php
https://medium.com/@galopintitouan/understanding-dependency-injection-by-example-with-the-symfony-di-component-part-1-2-15ac4bfd0f81 | Understanding Dependency Injection by example with the Symfony DI component (part 1/2)
https://code.tutsplus.com/tutorials/examples-of-dependency-injection-in-php-with-symfony-components--cms-31293 | Examples of Dependency Injection in PHP With Symfony Components
https://medium.com/manomano-tech/diving-into-symfonys-dependencyinjection-part-2-symbiosis-with-the-config-component-b535c5a2c591 | Diving into Symfony’s DependencyInjection — Part 2: Symbiosis with the Config component
https://symfony.com/doc/current/service_container/tags.html | How to Work with Service Tags (Symfony Docs)
https://symfonycasts.com/screencast/symfony-services/tagging-services | Tagging Services (and having Fun!) > Symfony 3: Level up with Services and the Container | SymfonyCasts
https://symfony.com/doc/current/reference/dic_tags.html | Built-in Symfony Service Tags (Symfony Docs)
