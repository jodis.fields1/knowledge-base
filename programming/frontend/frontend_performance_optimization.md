keywords: scaling performance

NOTE: see frontend_performance_bookmarklet.js next to this file

[Latency Numbers Every Programmer Should Know](https://people.eecs.berkeley.edu/~rcs/research/interactive_latency.html)

## 2018
[self versus total](https://stackoverflow.com/questions/7127671/difference-between-self-and-total-in-chrome-cpu-profile-of-js)

### http caching
came up again in context of graphql:
* [How-to: HTTP Caching for RESTful & Hypermedia APIs](https://web.archive.org/web/20181219015013/https://www.apiacademy.co/articles/2015/12/how-to-http-caching-for-restful-hypermedia-apis)
* [A Web Developer’s Guide to Browser Caching](https://medium.com/@codebyamir/a-web-developers-guide-to-browser-caching-cc41f3b73e7c)
* [HTTP Caching](https://developer.mozilla.org/en-US/docs/Web/HTTP/Caching)
* [IBM Knowledge Center: Caching of GET requests](https://www.ibm.com/support/knowledgecenter/en/SSLKT6_7.6.0.9/com.ibm.mif.doc/gp_intfrmwk/rest_api/c_rest_get_caching.html) - calculating an ETag value can be nontrivial, pessimistic versus optimistic...

Takeaways: for the initial request to the HTML file, you need `Cache-Control: no-cache` to get the initial document (or not, if ETag is the same); once you have that document, a cache busting parameter prevents the browser from having to even request a new copy of subsquent resources (ETag)

### chrome devtools profiling timing
[Finding an infinite (or very large) loop in JavaScript using Chrome Dev Tools](https://stackoverflow.com/questions/16843369/finding-an-infinite-or-very-large-loop-in-javascript-using-chrome-dev-tools)
    * use a flame chart...

https://github.com/paulirish/automated-chrome-profiling | paulirish/automated-chrome-profiling: Node.js recipes for automating javascript profiling in Chrome

#### link dump
https://chrome.google.com/webstore/detail/page-load-time/fploionmjgeclbkemipmkogoaohcdbig?hl=en | Page load time - Chrome Web Store
https://stackoverflow.com/questions/44687143/how-to-use-chrome-developer-tools-performance-or-memory-tab-when-page-freezes | javascript - How to use Chrome Developer Tools Performance or Memory tab when page freezes - Stack Overflow
https://stackoverflow.com/questions/35145777/auto-record-and-save-chrome-developer-tools-profiling-data-on-page-load | javascript - Auto record and save chrome developer tools profiling data on page load? - Stack Overflow
https://encrypted.google.com/search?hl=en&ei=zNHKWsDYKsiGsAW67oeYBg&q=chrome+devtools+profiling+performance+stop+on+load&oq=chrome+devtools+profiling+performance+stop+on+load&gs_l=psy-ab.3...4723.6114.0.6130.11.9.0.0.0.0.217.717.1j3j1.5.0....0...1c.1.64.psy-ab..8.0.0....0.UxcYzFI_oIc | chrome devtools profiling performance stop on load - Google Search
https://github.com/ChromeDevTools/awesome-chrome-devtools#chrome-devtools-protocol | ChromeDevTools/awesome-chrome-devtools: Awesome tooling and resources in the Chrome DevTools & DevTools Protocol ecosystem


### background images
[Why background images are slow to display – and how to make them appear faster](https://www.nccgroup.trust/uk/about-us/newsroom-and-events/blogs/2016/may/why-background-images-are-slow-to-display-and-how-to-make-them-appear-faster/)
    * "CSS background images tend to be loaded and displayed later than images referenced in <img> elements. As a rule, they’re given lower priority by the browser, and the CSS has to finish loading before they can be discovered (making them invisible to the browser preloader)."
        * use rel="preload" or <img>

## 2017
[The Cost Of JavaScript](https://medium.com/dev-channel/the-cost-of-javascript-84009f51e99e) - parse+compile in addition to download

## 2016
[Critical Rendering Path](https://developers.google.com/web/fundamentals/performance/critical-rendering-path/) - ?

[TechEmpower Web Framework Benchmarks](http://www.techempower.com/benchmarks/)

**Speed measurement -**

Use [PageSpeed Insights](https://developers.google.com/speed/pagespeed/insights/?url=http%3A%2F%2Fcreate.pathhero.bencreasy.com%2Fcreate%3Fcreate%3Ddemo&tab=desktop)! GTmetrix is OK too. May have to get past sign-in to see effects of dynamic content
tons of tools, but easiest may just be the Network tab of Chrome Developer Tool; load the page and watch the seconds and also the Loaded in red at the bottom
[Tools to measuring page rendering time](http://sqa.stackexchange.com/a/7678) - StackExchange SoftwareQQ answer says impossible to measure client-side rendering exactly****

**Performance and testing**

websiteoptimization.com
[http://viewlike.us/](http://viewlike.us/) - test websites in any resolution
webutation.com - reviews websites; like McAfee's reputation site

**Backend optimization** - 
https://blog.risingstack.com/measuring-http-timings-node-js/

[http://dtrace.org/blogs/brendan/2011/10/10/top-10-dtrace-scripts-for-mac-os-x/](http://dtrace.org/blogs/brendan/2011/10/10/top-10-dtrace-scripts-for-mac-os-x/)
[Dtrace](https://blog.8thlight.com/colin-jones/2015/12/01/ask-dtrace-why-are-my-tests-so-slow.html)!, ?, check [load with uptime](https://en.wikipedia.org/wiki/Load_(computing)), make sure caching is proper ([db versus memcached/redis](http://stackoverflow.com/questions/3084789/memcached-vs-sql-server-cache)) per [Great SQL Server Debates: Buffer Cache Hit Ratio](https://www.simple-talk.com/sql/database-administration/great-sql-server-debates-buffer-cache-hit-ratio/) (also [learn about a page](http://stackoverflow.com/questions/4401910/mysql-what-is-a-page))

note that you can mimic slow internet - see devtools docs

[sitemonitoring-production](https://github.com/jirkapinkas/sitemonitoring-production) - checks speed and fixes broken links!

****

**Profiling - opensource**

Turns out there aren't a lot of solutions. Devtools profile can be downloaded into an HTTP Archive (HAR) file and analyzed with [HAR Viewer](https://github.com/janodvarko/harviewer) (in PHP), also there is a diff facility per [this](http://www.webpagetest.org/forums/showthread.php?tid=12718) from GTMetrix [available at code.google.com](https://code.google.com/p/harviewer/issues/detail?id=52)

Find the critical path - see [What is a HAR File and what do I use it for?](https://www.neustar.biz/blog/what-is-a-har-file) for a description of that analysis

[How to Test Loading Time or Speed of Website – 5 Free Tools for Testing Website Performance](http://codeboxr.com/blogs/how-to-test-load-time-website-speed-performance-free-online-tools) - covers a bunch

**Controlling flow**

[How to get important images to load early](https://www.nccgroup.trust/uk/about-us/newsroom-and-events/blogs/2015/may/how-to-get-important-images-to-load-early/) (2015) - good techniques

**Background**

Show Slow is an open source tool that aggregates below rankings
Chrome DevTools - console.timer
Google PageSpeed
Yslow - [open-source](https://github.com/marcelduran/yslow/) from Yahoo
New Relic
traceroute
squid-cache -> proxy server company
Concurrent connections per host -> by domain
Wait for everything to load -> 'load' event
Wait for DOM + scripts to load -> DOMContentLoaded
HTTP ain't free
Headers
Cookies have 2k limit -> after that they get chopped off
Batch stuff
gzip is fast but not smallest in size
Sass allows you to create and grab images from spritesheet!
Browser caching
Use cache-control header, set ETag if content will change, and no-cache for dynamic content
HTML5 appcache manifest which lists dependencies

Use a CDN - BootstrapCDN is free from MaxCDN but only hosts minified code