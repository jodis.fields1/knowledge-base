Wants:

* works on mobile app (React Native) and web
* install each component by itself like lodash
* easy to restyle

https://github.com/input-output-hk/react-polymorph

https://chakra-ui.com/

## options
https://www.javascriptstuff.com/react-components/#tag:toolkit
https://www.javascriptstuff.com/react-component-toolkits/
https://hackernoon.com/the-coolest-react-ui-frameworks-for-your-new-react-app-ad699fffd651

http://fela.js.org/ - found thru https://github.com/Semantic-Org/Semantic-UI-React/issues/1000#issuecomment-315673986

### semantic-ui
Wants: react native, not available per https://github.com/Semantic-Org/Semantic-UI-React/issues/1000#issuecomment-370162836
asked about installing specific components at https://gitter.im/Semantic-Org/Semantic-UI-React?at=59c1f798c101bc4e3aea326b and was pointed to https://github.com/Semantic-Org/Semantic-UI-React/issues/1922
tips:
* https://stackoverflow.com/questions/43255151/semantic-ui-react-webpack-size-is-1-74m/43288212#43288212 is prolly best from https://github.com/Semantic-Org/Semantic-UI-React/issues/2175

### react native web
https://www.javascriptstuff.com/react-component-toolkits/#5-react-native-web says it is unique?

### ant design
seems to be opinionated according to https://www.javascriptstuff.com/react-component-toolkits/
[ant-design/ant-design](https://github.com/ant-design/ant-design) - 25k stars, looks good
* some React Native?
* install individual components?
* typescript :)
* server rendering

### react bootstrap
12k stars
biased against bootstrap since it seems hard to restyle
owner has lots of cool projects

### material-ui
[Build as a multi-repository](https://github.com/mui-org/material-ui/issues/7839) - owner is skeptical

### others
https://www.reddit.com/r/reactjs/comments/a6qhbr/checked_21_react_ui_kits_briefly_im_done/

ring-ui - see [JetBrains Web UI components open-sourced](https://news.ycombinator.com/item?id=14929076)

https://github.com/cerner/terra-core - from a real engineering team at Cerner (health information recorsd), with good tests...

https://fluentsite.z22.web.core.windows.net/quick-start - Microsoft sponsored?

#### rejected
pinterest - glanced at it, uses flow (negative) and PropTypes together? also not sure it's easily restylable


https://github.com/grommet/grommet - complaints about tough upgrade path