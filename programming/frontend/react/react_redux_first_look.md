


### old
[The Problem with Redux and How to Fix It](https://news.ycombinator.com/item?id=12187258) - 

[whats the redux idiom for waiting for multiple async calls?](https://github.com/reactjs/redux/issues/723) - 


**React Server**

[https://react-server.io/docs](https://react-server.io/docs)

**Build**

[mikach/requirejs-babel](https://github.com/mikach/requirejs-babel) - 

**Tutorials / Examples**
[Removing User Interface Complexity, or Why React is Awesome](http://jlongster.com/Removing-User-Interface-Complexity,-or-Why-React-is-Awesome)- famous post using Bloop from Firefox Developer Tools guy
[Simple Data Flow in React Apps Using Flux and Backbone: A Tutorial with Examples](https://www.toptal.com/front-end/simple-data-flow-in-react-applications-using-flux-and-backbone)
[React JS Tutorial and Guide to the Gotchas](https://zapier.com/engineering/react-js-tutorial-guide-gotchas/)
[redbooth/backbone-redux](https://github.com/redbooth/backbone-redux) - 
[gaearon/redux-devtools](https://github.com/gaearon/redux-devtools)
[https://github.com/ParsePlatform/parse-dashboard](https://github.com/ParsePlatform/parse-dashboard) - looks pretty decent, also see [https://github.com/ParsePlatform/parse-server](https://github.com/ParsePlatform/parse-server)
[pixyj/feel](https://github.com/pixyj/feel/tree/master/client/app) - Backbone-React app
[http://blog.vjeux.com/2014/javascript/why-does-react-scale-jsconf.html](http://blog.vjeux.com/2014/javascript/why-does-react-scale-jsconf.html)
[https://github.com/clayallsopp/react.backbone](https://github.com/clayallsopp/react.backbone) - someone recommended I don't bother with it?
[TodoMVC React-Backbone](https://github.com/tastejs/todomvc/blob/gh-pages/examples/react-backbone/js/app.jsx) - 
[Why we moved to React](https://tech.instacart.com/why-we-moved-to-react/) (Instacart) - 

*Redux*

[http://teropa.info/blog/2015/09/10/full-stack-redux-tutorial.html](http://teropa.info/blog/2015/09/10/full-stack-redux-tutorial.html) - haven't read it

**To read**

[The SoundCloud Client in React + Redux](http://www.robinwieruch.de/the-soundcloud-client-in-react-redux/)- 

**Advanced**

[Trying to put API calls in the correct place](https://github.com/reactjs/redux/issues/291) - 

[erikras/ducks-modular-redux](https://github.com/erikras/ducks-modular-redux) - A proposal for bundling reducers, action types and actions when using Redux

**Old**

unique keys - good advice [here](https://news.ycombinator.com/item?id=11187598)

[An opinionated guide to React.js best practices and conventions](https://web-design-weekly.com/2015/01/29/opinionated-guide-react-js-best-practices-conventions/) - best so far!

Note: child components mount before parent components - duh! but the rendering happens in the opposite order

Note: if you want to use refs to access inner components, make sure that you set the ref on that inner component and not some other HTML element!!

Note: the return value, if on another line, has to be wrapped in parentheses to avoid ASI screwing with you

Note: setting state on a parent component will not cascade down to the child components, although the props will. To set state in child components contingent upon a parent stage change, you could use WillReceiveProps but that seems to be discouraged