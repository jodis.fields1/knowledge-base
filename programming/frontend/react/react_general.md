TODO: read https://www.gitbook.com/book/vasanthk/react-bits/details for lots of good stuff

TODO: look into https://github.com/jaredpalmer/formik for building forms

## Links
* https://github.com/xgrommx/awesome-redux
* https://github.com/markerikson/redux-ecosystem-links
* https://github.com/markerikson/react-redux-links
    * see e.g. https://github.com/markerikson/react-redux-links/blob/master/react-component-patterns.md

### Internals /Fundamentals
[React, Inline Functions, and Performance](https://cdb.reacttraining.com/react-inline-functions-and-performance-bdff784f5578)
* https://web.archive.org/web/20210512011748/https://blog.webdevsimplified.com/2020-05/memoization-in-react/

[Build Yourself a Redux](https://news.ycombinator.com/item?id=14273549)
[What does « dehydrate » and « rehydrate » stand for in Fluxible?](https://stackoverflow.com/questions/29824908/what-does-dehydrate-and-rehydrate-stand-for-in-fluxible)

[Understanding the React Source Code, Part 1](https://news.ycombinator.com/item?id=16125670) - but react v16 is practically an entire rewrite
    * https://engineering.hexacta.com/didact-fiber-incremental-reconciliation-b2fe028dcaec explains the rewrite

### Tools / Monitoring
* https://github.com/garbles/why-did-you-update
* react-passthrough - http://jamesknelson.com/building-a-property-passthrough-higher-order-component-for-react/

#### Isolation

##### 2018-03 link dump
[7 Reasons to Outlaw React’s Functional Components](https://medium.freecodecamp.org/7-reasons-to-outlaw-reacts-functional-components-ff5b5ae09b7c)

* Luke Davis recommended Cosmos:
    * https://github.com/react-cosmos/react-cosmos | react-cosmos/react-cosmos: Dev tool for creating reusable React components

https://blog.percy.io/turbocharge-your-react-development-with-storybook-and-percy-d9bff19c187e | Turbocharge your React development with Storybook and Percy

* [Fighting for Component Independence](https://medium.com/@skidding/fighting-for-component-independence-2a762ee53272) points to:
* https://github.com/storybooks/storybook
* https://github.com/carteb/carte-blanche - has fuzz testing
* https://github.com/glenjamin/devboard

* https://medium.com/@alex_pustovalov/structor-differs-from-react-storybook-in-that-it-can-6009e4debae7 - looks dead

### Typescript / Flow integration
https://robwise.github.io/blog/using-flow-annotations-in-your-redux-reducers

### File structure
[Rangle starter structure](https://github.com/rangle/rangle-starter/issues/214)
https://github.com/davezuko/react-redux-starter-kit/wiki/Fractal-Project-Structure - found thru https://www.reddit.com/r/reactjs/comments/6al7h2/facebook_has_30000_react_components_how_do_you/


### Pain points?
[Add fragment API to allow returning multiple components from render](https://github.com/facebook/react/issues/2127)

#### Conditions
Not really a big deal, but see https://github.com/facebook/react/issues/690 and https://www.npmjs.com/package/jsx-control-statements

## projects
### PathHero
refactored into individual files on [April 2015](https://github.com/jcrben/pathhero.bencreasy.com/commit/221ecac71d888be2485ba743ed79a2f98414fd67#diff-4cac7e39533538b8e240e7d69031f337)

### others
* F8 React Native - http://makeitopen.com/blog/2017/12/04/blog-post-f82017-open-source.html
* [https://github.com/selfhub/selfhub](https://github.com/selfhub/selfhub)
* [https://github.com/CulturedCheese/thesis-project](https://github.com/CulturedCheese/thesis-project)
