
## background - "Inventing on Principle"
hot reload and time travel is a bit motivated by Bret Victor's famous [Bret Victor - Inventing on Principle](https://vimeo.com/36579366)
    * seems to have never been open-sourced per https://stackoverflow.com/questions/9448215/tools-to-support-live-coding-as-in-bret-victors-inventing-on-principle-talk

## 2017 (late half) - with typescript
main upstream: https://github.com/facebookincubator/create-react-app/pull/2304
https://github.com/wmonk/create-react-app-typescript/issues/118 - workable advice
[React Hot Loader v3. this component is not accepted by Hot Loader](https://stackoverflow.com/questions/46685054/react-hot-loader-v3-this-component-is-not-accepted-by-hot-loader)

## alternatives
https://survivejs.com/webpack/developing/automatic-browser-refresh/
BrowserSync https://browsersync.io/
https://github.com/lepture/python-livereload

## performance
https://github.com/erikras/react-redux-universal-hot-example/issues/70 - lots of people still have issues
https://github.com/erikras/react-redux-universal-hot-example/issues/616 - ?

## react & typescript
[Hello React and TypeScript](https://charleslbryant.gitbooks.io/hello-react-and-typescript/content/)

[Webpack’s HMR & React-Hot-Loader — The Missing Manual](https://medium.com/@rajaraodv/webpacks-hmr-react-hot-loader-the-missing-manual-232336dc0d96)
* [Webpack & The Hot Module Replacement](https://medium.com/@rajaraodv/webpack-hot-module-replacement-hmr-e756a726a07)

## 2017 (early half)
Ultimately managed to get this working but it was far too slow

https://github.com/patrikholcak/hot-loader-demo - nice minimal demo
https://github.com/krasevych/react-redux-styled-hot-universal - fairly advanced

https://github.com/gaearon/react-hot-boilerplate/pull/61#issuecomment-254467020 - main source
* first see commit for alpha
* then see commit for beta

original docs: https://github.com/gaearon/react-hot-loader/tree/master/docs

https://github.com/erikras/react-redux-universal-hot-example/issues/1184 - the original classic, continued at https://github.com/bertho-zero/react-redux-universal-hot-example

[Hot-reloading in 2015:](https://medium.com/@the1mills/hot-reloading-with-react-requirejs-7b2aa6cb06e1#.b2xfv2h1g) - use requirejs
