
## unmount
The router is largely responsible for mounting and unmounting: "Forgetting to call unmountComponentAtNode will cause your app to leak memory. There is no way for us to automatically detect when it is appropriate to do this work. Every system is different" per https://reactjs.org/blog/2015/10/01/react-render-and-top-level-api.html

https://stackoverflow.com/questions/41498756/when-does-a-component-unmount
https://stackoverflow.com/questions/35370044/react-when-do-i-need-to-call-reactdom-unmountcomponentatnode
https://stackoverflow.com/questions/21662153/unmounting-react-js-node
https://developmentarc.gitbooks.io/react-indepth/content/life_cycle/death_unmounting_indepth.html

## general link dump
[You might not need React Router](https://medium.freecodecamp.org/you-might-not-need-react-router-38673620f3d)
[Why I Don't Use React Router](https://news.ycombinator.com/item?id=12517962)

https://github.com/beyond-labs/react-mirror

### redux-little-router
https://github.com/justrossthings/react-redux-boiler

### redux-first-router
https://github.com/faceyspacey/redux-first-router
