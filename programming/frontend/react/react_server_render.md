https://developers.google.com/web/updates/2019/02/rendering-on-the-web

https://github.com/developit/unistore | developit/unistore: 🌶 650b state container with component actions for Preact & React
https://scotch.io/amp/tutorials/build-a-ssr-app-with-preact-unistore-and-preact-router?__twitter_impression=true | Build a SSR App With Preact, Unistore and Preact Router ― Scotch

TODO: https://tylermcginnis.com/react-router-server-rendering/

FIRST STEPS: see historical, run thru mhart's example

https://redux.js.org/recipes/server-rendering
https://hackernoon.com/whats-new-with-server-side-rendering-in-react-16-9b0d78585d67

sidecar process nodejs

## 2018 update react 
https://github.com/tylermcginnis/rrssr | tylermcginnis/rrssr
https://tylermcginnis.com/react-router-server-rendering/ | Server Rendering with React and React Router
https://codeburst.io/react-server-side-rendering-ssr-with-express-and-css-modules-722ef0cc8fa0 | React Server Side Rendering (SSR) with Express and CSS Modules

## php
https://github.com/Limenius/ReactRenderer

## important?
http://andrewhfarmer.com/server-side-render/ - for me, mostly no for now with web apps, echoed by http://jamesknelson.com/universal-react-youre-doing-it-wrong/

## TODO
### Tutorial
https://medium.freecodecamp.org/server-side-rendering-your-react-app-in-three-simple-steps-7a82b95db82e
https://scotch.io/tutorials/react-on-the-server-for-beginners-build-a-universal-react-and-node-app - browsed, seems solid
http://redux.js.org/docs/recipes/ServerRendering.html - obviously...
https://medium.com/@navgarcha7891/react-server-side-rendering-with-simple-redux-store-hydration-9f77ab66900a - ?
https://medium.com/@mikeyGlitz/down-the-universal-rabbit-hole-8b7c6319aa3d - ?

### performance gotchas 
react-dom-stream is one proposed workaround plus redis e.g. 
[React.Js: Achieving 20ms server response time with Server Side Rendering](https://news.ycombinator.com/item?id=12727791)
https://github.com/walmartlabs/react-ssr-optimization
rendering on the server is blocking and takes 200ms per Halt Hammerzeit; discussed in detail at https://medium.com/react-university/4-practical-tips-for-drastically-improved-server-side-rendering-in-react-2df98555a26b

### Libraries
Halt Hammerzeit is a leader
* https://github.com/halt-hammerzeit/universal-webpack
  * example project at https://github.com/halt-hammerzeit/webpack-react-redux-isomorphic-render-example which uses https://github.com/halt-hammerzeit/react-isomorphic-render

### Examples
best practice - next.js from zeit, skimmed thru source
https://hackernoon.com/server-side-rendering-with-create-react-app-1faf5a9d1eff

#### Historical
https://github.com/mhart/react-server-example - one of the original, with no redux
  * https://github.com/mhart/react-server-routing-example
http://eflorenzano.com/blog/2014/04/09/react-part-1-getting-started/ - one of the oldest blog posts