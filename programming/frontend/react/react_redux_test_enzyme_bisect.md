2018-03: decided to attack a bug in enzyme with bisec tand npm run link
    * https://web.archive.org/web/20150830120011/https://medium.com/@rasjani/debugging-3rd-party-npm-module-regression-d10530f2b67
    * https://web.archive.org/web/20180108040315/https://medium.com/trisfera/the-magic-behind-npm-link-d94dcb3a81af
    * also see [Allow checkout of a git repo directly into node_modules](https://github.com/npm/npm/issues/12506)
    * https://github.com/laggingreflex/gfork which lets you fork npm projects

lerna, switching node versions, etc ended up being a real pain

"At the first render, vnode.memoizedProps is the source of truth."
    -  https://github.com/airbnb/enzyme/issues/1163#issuecomment-332438576?

STEPS:
* proper branch -> react 15, v2.9.1

hmm, bisecting something like this is tricky. and now I'm wondering if the repro above passes for 2.9.1 as I had earlier thought (since `npm test` running 8.10.0 is now giving me `TypeError: Super expression must either be null or a function, not undefined`). for anyone else who wants to try, seems that you'd want to run `npm run build` with lerna (globally installed?) -> `npm link` from the new `packages/enzyme` for the newer build with node 8 npm 5, then switch node / npm versions to 6 / 3 for 2.9.1

From all the chatter above, it seems like a fair number of comments are just confusion around the 3 breaking changes? @ljharb to be honest I found that migration guide really hard to follow - it reads more on the implementation detail level - even re-reading it now it's hard for me to pull the message "you now always have to re-find from the root every time if there's been changes" from https://github.com/airbnb/enzyme/blob/master/docs/guides/migration-from-2-to-3.md#element-referential-identity-is-no-longer-preserved

I noticed a repro case https://github.com/sontek/enzyme-react16/tree/react15. Pulling it down and  switching to the react15 branch, it did seem like 3 failed and 2 succeeded, so I decided to give bisecting a try following 

Now it seems like maybe I was wrong and 2 does not fail?