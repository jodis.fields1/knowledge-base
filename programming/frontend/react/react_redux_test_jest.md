https://github.com/kentcdodds/react-testing-library/blob/master/README.md#faq - says enzyme is an antipattern

## gotchas
* async in `describe()`
    * https://github.com/airbnb/enzyme/issues/1558#issuecomment-385111666

## 2021-09 jest spelunking
wanted to run plain jest at the CLI instead of CRA changed (monstrosity)
* code split between node_modules/jest-cli & @jest/core 
  * -> jest.js
  * -> jest/core/cli/index
  * -> jest/core/cli/index -> jest-config/readConfigs()
  * -> runJest.js
  * -> back out of @jest to jest-runner/runTest.js

## 2018-03 jest update
what's the story with babel-jest?
```
 Jest encountered an unexpected token
      This usually means that you are trying to import a file which Jest cannot parse, e.g. it's not plain JavaScript.
      By default, if Jest sees a Babel config, it will use that to transform your files, ignoring "node_modules".
      Here's what you can do:
       • To have some of your "node_modules" files transformed, you can specify a custom "transformIgnorePatterns" in your config.
       • If you need a custom transformation specify a "transform" option in your config.
       • If you simply want to mock your non-JS modules (e.g. binary assets) you can stub them out with the "moduleNameMapper" config option.
      You'll find more details and examples of these config options in the docs:
      https://jestjs.io/docs/en/configuration.htm
```

    * automatically included https://facebook.github.io/jest/docs/en/getting-started.html#using-babel
    * for typescript see https://github.com/kulshekhar/ts-jest

jest.mock - key as it has that optional second parameter
    * https://facebook.github.io/jest/docs/en/jest-object.html#jestmockmodulename-factory-options
    * 4 ways to mock ES6 classes described at https://facebook.github.io/jest/docs/en/es6-class-mocks.html

--- old


*Jest*
Set jest property on package.json for config
Ran into [unmockedModulePathPatterns can not detect module path](https://github.com/facebook/jest/issues/100) (see [SO page](http://stackoverflow.com/questions/24981486/jest-react-example))

## jest
TODO: per [Hide console logging for passing tests and show it for failures](https://github.com/facebook/jest/issues/4156) - see [fingers-crossed.js reporter](https://github.com/mozilla/addons-frontend/blob/e1606743d79e779b1902399685f35a90aa6b9ab9/tests/jest-reporters/fingers-crossed.js)

Important?: Mock defined in __mocks__ directory not being used in submodules - addressed by setMock or https://github.com/facebook/jest/issues/335#issuecomment-309401620 - also note that "Modules that are mocked with jest.mock are mocked only for the file that calls jest.mock"

similarly [How do you manually mock one of your own files in Jest?](https://stackoverflow.com/questions/28514868/how-do-you-manually-mock-one-of-your-own-files-in-jest) points to setMock

#### snapshhots
* [Effective Snapshot Testing](https://blog.kentcdodds.com/effective-snapshot-testing-e0d1a2c28eca)
* [Interactive Snapshot Update mode](https://github.com/facebook/jest/pull/3831)

### mocking
I've had better luck with setMock() and not using the import statement:
* [How can I mock an ES6 module import using Jest?](https://stackoverflow.com/questions/40465047/how-can-i-mock-an-es6-module-import-using-jest)
* [How to mock dependencies for unit tests with ES6 Modules](https://stackoverflow.com/questions/27323031/how-to-mock-dependencies-for-unit-tests-with-es6-modules)

* [jest.fn() All the Things](https://medium.com/@deanslamajr/jest-fn-all-the-things-d26f3b929986)
