My only animation note, so maybe more general

## fancy / cool
https://litepaper.com/resources/intro-to-augur
* progress bar when scrolling
* sidebar pops out

https://leebyron.com/

http://rileyh.com/

## react
https://www.smashingmagazine.com/2013/04/css3-transitions-thank-god-specification/
https://github.com/reactjs/react-transition-group/issues/11
https://www.npmjs.com/package/react-animate-state
https://github.com/chenglou/react-motion/issues/139
http://unitstep.net/blog/2015/03/03/using-react-animations-to-transition-between-ui-states/
https://stackoverflow.com/questions/37092116/reacttransitiongroup-doesnt-works-with-react-redux-connected-component
https://stackoverflow.com/questions/31374033/what-are-the-callbacks-in-reacttransitiongroup-hooks
https://medium.com/@rajaraodv/the-inner-workings-of-virtual-dom-666ee7ad47cf
https://news.ycombinator.com/item?id=14144142