
TODO: rematch

ttps://www.reddit.com/r/reduxjs/comments/a2nvwf/the_secret_to_using_redux_createnamespacereducer/ | The Secret to Using Redux: createNamespaceReducer : reduxjs

https://github.com/beyond-labs/react-mirror

[scalable-frontend-with-elm-or-redux](https://github.com/slorber/scalable-frontend-with-elm-or-redux) - TODO, found at https://github.com/reactjs/redux/issues/1528#issuecomment-198287342

[Reducer Composition with Effects in JavaScript](https://github.com/reactjs/redux/issues/1528) - official discussion circles around react-loop
https://github.com/reactjs/redux/blob/master/docs/recipes/reducers/ReusingReducerLogic.md - another official suggestion

[How to reuse Redux components?](https://medium.com/@MattiaManzati/how-to-reuse-redux-components-8acd5b4d376a)

[Scalable FE with Redux and Elm architecture](https://medium.com/@hunterbmt/scaleable-fe-with-redux-and-elm-architecture-c6812ed0125e) - recommends prism at the end

## TO-REVIEW
https://community.risingstack.com/repatch-the-simplified-redux/
https://github.com/erikras/multireducer - might be more straightforward
https://github.com/redux-loop/redux-loop
https://github.com/gcazaciuc/redux-fractal
https://github.com/salsita/prism - often tied with others?
https://github.com/reactjs/redux/issues/897#issuecomment-263769587 - reduxish?

## Reviewed
https://community.risingstack.com/repatch-the-simplified-redux/ - not sure how much I like dispatching reducers
redux-operations - doesn't solve reusable problem yet it seems, explained at https://medium.com/@matt.krick/introducing-redux-operations-332ab56e468b and [Solving Redux’s shortcoming in 150 LOCs](https://medium.com/@matt.krick/solving-redux-s-shortcoming-in-150-locs-540979ce6cf9)
freactal - https://github.com/FormidableLabs/freactal actually looks quite slick, noticed that it doesn't pass state into effects
redux-tiles - seems pretty basic https://github.com/Bloomca/redux-tiles

## Reviewed, rejected


## Unreviewed, niche
* https://github.com/minedeljkovic/redux-elmish, also see https://github.com/alexeisavca/elmy and https://www.reddit.com/r/reactjs/comments/4pnscw/elmy_elm_architecture_for_react/d4ni0m4/
* https://github.com/DataDog/redux-doghouse - scoped actions and reducers

### Elm architecture
per https://github.com/jarvisaoieong/redux-architecture just use redux-loop

### Extreme reusability
https://fabricatech.wordpress.com/2017/05/15/reusable-redux-packages-an-approach-to-framework-insulation-and-code-reuse/ - share angular and react code with well-defined interfaces