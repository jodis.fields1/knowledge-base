
## ultimate solution
* created a placeholderRow component which takes placeholderShape
  * TODO: update to use css grid for ultimate flexibility, prolly need placeholderGrid

## research links
https://github.com/OGPoyraz/react-fake-component
http://cloudcannon.com/deconstructions/2014/11/15/facebook-content-placeholder-deconstruction.html
https://github.com/buildo/react-placeholder
https://github.com/third/react-content-placeholder
https://github.com/gnandous/react-awesome-loader
http://matthewroach.me/react-placeholder-loading-state/