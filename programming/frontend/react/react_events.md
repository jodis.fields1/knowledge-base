```javascript
$$typeof: Symbol(react.element)
```

## react event handling
### overview
https://medium.com/the-guild/getting-to-know-react-doms-event-handling-system-inside-out-378c44d2a5d0 | Getting to know React DOM’s event handling system inside out
https://levelup.gitconnected.com/how-exactly-does-react-handles-events-71e8b5e359f2 | How exactly does React handle events? – gitconnected.com | Level Up Your Coding
### source
https://github.com/facebook/react/blob/c954efa70f44a44be9c33c60c57f87bea6f40a10/packages/react-dom/src/client/ReactDOM.js | react/ReactDOM.js at c954efa70f44a44be9c33c60c57f87bea6f40a10 · facebook/react
https://github.com/facebook/react/tree/master/packages/react-dom | react/packages/react-dom at master · facebook/react

### docs
https://reactjs.org/docs/handling-events.html | Handling Events – React
https://reactjs.org/docs/refs-and-the-dom.html | Refs and the DOM – React

### questions
https://stackoverflow.com/questions/44659548/why-onclick-in-react-got-bound-to-emptyfunction | javascript - Why 'onClick' in react got bound to emptyFunction? - Stack Overflow
https://stackoverflow.com/questions/47493812/react-cloneelement-returning-an-object-instead-of-function | reactjs - React.CloneElement returning an object instead of function - Stack Overflow
https://stackoverflow.com/questions/27707911/add-event-handler-to-react-dom-element-dynamically | reactjs - Add event handler to React.DOM element dynamically - Stack Overflow
