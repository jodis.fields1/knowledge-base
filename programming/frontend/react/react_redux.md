
See redux_reusable.md

TODO: rematch seems best? 
* good reception at https://www.reddit.com/r/reactjs/comments/7z5l2w/rethinking_redux_in_2018_its_time_for_rematch/
    * like freactal
    * https://medium.com/@joseph0crick/redux-simplifiers-an-overview-46f4aac0908e says most popular


Formidable Labs has lots of awesome stuff

[Blogged Answers: Redux - Not Dead Yet!](http://blog.isquaredsoftware.com/2018/03/redux-not-dead-yet/)

TODO: http://blog.isquaredsoftware.com/2017/05/idiomatic-redux-tao-of-redux-part-2/
    * excessively verbose?


## TODO
https://medium.freecodecamp.org/patterns-for-using-react-with-statechart-based-state-machines-33e6ab754605 | Patterns for using React with Statechart-based state machines

https://hackernoon.com/redesigning-redux-b2baee8b8a38 | Redesigning Redux – Hacker Noon

https://github.com/theKashey/restate | theKashey/restate: A redux fractal state library

### Gotchas
reactjs batches updates so every action does not trigger render. see:
https://twitter.com/dan_abramov/status/712002881742839808
[Should mapStateToProps be called every time an action is dispatched?](https://github.com/reactjs/react-redux/issues/291)
[How to chain synchronous actions?](https://github.com/reactjs/redux/issues/1543)

### Routing
https://github.com/FormidableLabs/redux-little-router
http://jamesknelson.com/created-junctions-js/#more-1118

### Internals
[Implement React Redux from Scratch (Part 2)](https://medium.com/@kj_huang/implementation-of-react-redux-part-2-633441bd3306)

[Do I need connect() from React Redux or should I use store.subscribe()](https://github.com/reactjs/react-redux/issues/281)
[Question: Redux + React with only stateless functions](https://github.com/reactjs/redux/issues/1176)

### Design and encapsulation
[Encapsulation in Redux: the Right Way to Write Reusable Components](https://blog.javascripting.com/2016/02/02/encapsulation-in-redux/)

#### Actions / logic
thunks versus sagas, see [Have you struggled with where to put application logic in a React + Redux app?](https://www.reddit.com/r/reactjs/comments/548wsi/have_you_struggled_with_where_to_put_application/)

#### Sagas
See also redux_reusable.md
[JavaScript Power Tools: Real-World Redux-Saga Patterns](https://news.ycombinator.com/item?id=14515308) - good Hackernews discussion
https://www.reddit.com/r/reactjs/comments/5lsb4j/idiomatic_redux_thoughts_on_thunks_sagas/ - discusses controversy

### Other
https://github.com/tommikaikkonen/redux-orm - not sure why (yet)

### File / Directory organization
https://github.com/erikras/ducks-modular-redux - popular but not that interesting