
TODO: https://github.com/attekei/practical-frontend-testing
    * check out metabase

GOTCHA: [simulate('click') issue with react-router Link](https://github.com/airbnb/enzyme/issues/516)

GOTCHA: props are not the same as the rendered html; see [
Better way to assert on rendered HTML properties?](https://github.com/airbnb/enzyme/issues/634)

PROTIP: simulate clicks with [Setting text value of input ](https://github.com/airbnb/enzyme/issues/76#issuecomment-189606849)

TODO: does unmounting cause memory leaks? according to https://github.com/airbnb/enzyme/issues/1689 yes, but I haven't noticed it

TODO: pair with cosmos https://github.com/react-cosmos/react-cosmos/pull/534

TODO: https://github.com/mlawrie/hereafter for async

TODO: could answer this https://stackoverflow.com/questions/43568570/how-can-i-get-window-location-pathname-on-my-test-file-using-jest/43568680
    * point to https://github.com/facebook/jest/issues/5124

https://devhints.io/enzyme | Enzyme cheatsheet

Common lessons / gotchas:
    * cannot return promise from describe
        * [Allow for `describe` to return a promise](https://github.com/facebook/jest/issues/2235)
    * can use jsdom instead for events since they don't propagate
        * https://github.com/airbnb/enzyme/issues/426#issuecomment-222331257
```jsx
// this test does not use enzyme because it doesn't propagate events
    // render to el istead of body to avoid react warning
    const topChild = document.createElement('div');
    topChild.className = 'top';
    document.body.appendChild(topChild);
        ReactDOM.render(
...
```
        * https://stackoverflow.com/questions/37723238/how-to-check-the-actual-dom-node-using-react-enzyme
    * simulate not recommended "Use .prop('onClick')() instead"
        * https://github.com/airbnb/enzyme/issues/1496#issuecomment-360963840
    * have to manually run <root>.update()
        * https://github.com/airbnb/enzyme/blob/master/docs/guides/migration-from-2-to-3.md
        * https://github.com/airbnb/enzyme/labels/v3%20expected%20difference
    * cheerio gotchas
        * https://github.com/airbnb/enzyme/issues/1162 - innerHTML



## general
[Use Enzyme for Integration Testing (end-to-end testing)](https://github.com/airbnb/enzyme/issues/237)
Is there any point to anything other than using enzyme to fully mount and run
basically end-to-end tests?


### mount versus shallow etc
"shallow wrappers represent what the component renders, but mounted wrappers represent the component itself"
    - https://github.com/airbnb/enzyme/issues/134 | hasClass does not work on root ReactWrapper
https://stackoverflow.com/questions/44082820/enzyme-when-to-use-shallow-render-or-mount | reactjs - Enzyme: When to use shallow, render, or mount? - Stack Overflow
https://stackoverflow.com/questions/38710309/when-should-you-use-render-and-shallow-in-enzyme-react-tests | testing - When should you use render and shallow in Enzyme / React tests? - Stack Overflow
https://stackoverflow.com/questions/47763766/what-is-the-difference-between-enzymes-shallow-render-and-instance-method | reactjs - What is the difference between enzymes shallow render and instance method - Stack Overflow
https://gist.github.com/fokusferit/e4558d384e4e9cab95d04e5f35d4f913 | Difference between Shallow, Mount and render of Enzyme

## tutorials
[Integration Testing React and Redux With Mocha and Enzyme](https://engineering.classdojo.com/blog/2017/01/12/integration-testing-react-redux/) - how is jsdom different?
https://javascriptplayground.com/introduction-to-react-tests-enzyme/ | An introduction to testing React components with Enzyme 3

### 2018-03 link dump
https://github.com/react-cosmos/react-cosmos/pull/534 | Test API by skidding · Pull Request #534 · react-cosmos/react-cosmos
