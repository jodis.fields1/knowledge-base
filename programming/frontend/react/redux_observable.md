See also:
* functional_programming_reactive.md


[Understanding the observable type](http://nick.balestra.ch/2016/Understanding-the-observable-type/)
    * decent intro - producers, observers, and observables! observer just executes a contract!

## rxjs
https://softchris.github.io/books/rxjs/observable-wrapping/
## es-observable
recommended by redux-loop at 
https://github.com/redux-loop/redux-loop/issues/41#issuecomment-210190604 | Add an Effect for dispatching an unknown amount of actions · Issue #41 · redux-loop/redux-loop

https://tc39.github.io/proposal-observable/ | Observable
https://github.com/tc39/proposal-observable | tc39/proposal-observable: Observables for ECMAScript

## 2018-03 link dump
https://stackoverflow.com/questions/37379947/replace-callback-hell-with-observable | javascript - Replace callback hell with observable - Stack Overflow
https://stackoverflow.com/questions/44071518/how-to-avoid-callback-hell-with-ngrx-observables | angular - How to avoid callback hell with ngrx observables - Stack Overflow
https://github.com/redux-observable/redux-observable/issues/402 | Combining Observable ajax requests which depend on the result of the previous · Issue #402 · redux-observable/redux-observable
https://www.reddit.com/r/javascript/comments/6qm3j3/the_great_escape_from_callback_hell_javascript/ | The Great Escape from Callback Hell – JavaScript Teacher – Medium : javascript

## comparison
https://stackoverflow.com/questions/40021344/why-use-redux-observable-over-redux-saga | javascript - Why use Redux-Observable over Redux-Saga? - Stack Overflow
