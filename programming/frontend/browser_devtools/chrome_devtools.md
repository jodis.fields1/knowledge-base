# chrome devtools
See also: 
* self-improvement/software_apps/browsers.md
* nodejs_debugging.md

https://developers.google.com/web/tools/chrome-devtools/shortcuts

https://github.com/pyllyukko/user.js/ - version control firefox config

See also: dotfiles/config/chromium/MY_README.md

TODO: checkout https://github.com/diffsky/chromedotfiles

TODO: https://brookhong.github.io/2016/10/09/debug-chrome-with-gdb.html

TODO: http://webmasters.stackexchange.com/questions/10039/keep-google-chrome-element-inspector-dom-tree-expanded-on-reload

## profiling
[Difference between 'self' and 'total' in Chrome CPU Profile of JS](https://stackoverflow.com/questions/7127671/difference-between-self-and-total-in-chrome-cpu-profile-of-js)

[Finding an infinite (or very large) loop in JavaScript using Chrome Dev Tools](https://stackoverflow.com/questions/16843369/finding-an-infinite-or-very-large-loop-in-javascript-using-chrome-dev-tools)

## performance optimization
[Analyze Runtime Performance](https://developers.google.com/web/tools/chrome-devtools/rendering-tools/)

## troubleshooting
### [VM] file from javascript
https://stackoverflow.com/questions/17367560/chrome-development-tool-vm-file-from-javascript

### arrow function debug tips
Available per [How to set a breakpoint at a lambda call in Google Chrome DevTools?](https://stackoverflow.com/questions/30664151/how-to-set-a-breakpoint-at-a-lambda-call-in-google-chrome-devtools)
* reproduced in /Users/bencreasy/code/assorted/webdesign/webdesign.html
* appears to require columns or something in sourcemap?
* see https://bugs.chromium.org/p/chromium/issues/detail?id=566801 for ticket
    
[Workaround](https://github.com/georgebonnr/bug): const myFunction = () => (()=>{debugger})() || Math.random() * Math.random()

### debug eval
[Chrome Development Tool: [VM] file from javascript](https://stackoverflow.com/questions/17367560/chrome-development-tool-vm-file-from-javascript)
    * set sourcemapping at bottom

## Bug tracker
https://bugs.chromium.org/p/chromium/issues/list

Click the component dropdown -> Show only -> see the list of components

## Dimmed properties
https://stackoverflow.com/questions/29821843/what-is-the-significance-of-faded-properties-when-using-console-dir-in-chrome-de

[Hide inherited Styles in Dev Tools](https://coderwall.com/p/7abp6g/hide-inherited-styles-in-dev-tools)
[What does it mean when a CSS rule is grayed out in Chrome's element inspector?](https://stackoverflow.com/questions/3265555/what-does-it-mean-when-a-css-rule-is-grayed-out-in-chromes-element-inspector)

## General
is there a keyboard shortcut for long resume discussed at https://developers.google.com/web/tools/chrome-devtools/debug/breakpoints/step-code?hl=en

[Down and Dirty with Chrome Dev Tools](http://blittle.github.io/chrome-dev-tools/) - 
http://stackoverflow.com/questions/8243742/chrome-javascript-debugging-how-to-save-break-points-between-page-refresh-or-b - need to try this
Watching: [Debugging with webpack, ES6 and Babel](http://stackover5flow.com/questions/32211649/debugging-with-webpack-es6-and-babel) - see linked Chrome issue, can't type source mapped code into console
TODO: [Firediff](https://addons.mozilla.org/en-US/firefox/addon/firediff/) - does Chrome have this?
TODO: Time-traveling debugger - Edge is going to get it?
TODO: document Force selected state as a cool trick

Unsolved mysteries: how can I prevent chrome breakpoints from disappearing when I reload? Related: [Maintain JS debug code & reloading browser](http://stackoverflow.com/questions/17306783/maintain-js-debug-code-reloading-browser)
[How do I keep the “Elements” (DOM) tree open in the Webkit Inspector?](http://stackoverflow.com/questions/4189949/how-do-i-keep-the-elements-dom-tree-open-in-the-webkit-inspector) - seems this basically requires a hack, need to open an issue with Chrome

**Dev team**<br/>
See http://www.paulirish.com/ and the Slack channel

**Map Local**<br/>
2019-01: 
* https://www.charlesproxy.com/documentation/proxying/ssl-proxying/ | SSL Proxying • Charles Web Debugging Proxy
* https://www.charlesproxy.com/documentation/using-charles/ssl-certificates/ | SSL Certificates • Charles Web Debugging Proxy
* https://stackoverflow.com/questions/3979685/how-to-enable-map-local-over-https-with-charles-proxy#comment22985679_3979685 | configuration - How to enable Map Local over https with Charles Proxy? - Stack Overflow
* https://veerasundar.com/blog/2016/05/charles-proxy-map-local/ | How to use Charles Proxy Map Local


2018-06: there's [Tamper Chrome (extension)](https://github.com/google/tamperchrome) also now
    * TODO: get this or Tamper working

[Tamper](https://dutzi.github.io/tamper/):
    * TODO: fix https://github.com/dutzi/tamper/issues/23#issuecomment-303808488
    * discussion at https://news.ycombinator.com/item?id=16452852 

This doesn't come default. Look at Charles Proxy or if you're daring,  https://dutzi.github.io/tamper/ (Tamper) which is based on mitmproxy. Also Privoxy but doesn't work with SSL.

**General**<br/>
Reduce the text in your bookmarks bar bookmarks to make way for more space. In general, bookmarks are a bad place to store links - use pinboard or something.
Change headers - use Postman, other candidates listed at [View and Set HTTP headers for Safari/Chrome](http://stackoverflow.com/questions/7097502/view-and-set-http-headers-for-safari-chrome)

**Issues**<br/>
When stepping through, debugger does not consistently jump to the event handler unless you set a breakpoint. The list of event handlers misses some event listeners - probably because they are not UI-based - particularly Marionette ones. The major SO question on this, [How to debug JavaScript/jQuery event bindings with Firebug (or similar tool)](http://stackoverflow.com/questions/570960/how-to-debug-javascript-jquery-event-bindings-with-firebug-or-similar-tool?rq=1) has mainly outdated answers, but [one answer](http://stackoverflow.com/a/18602708/4200039) pointed to the [monitorEvents() command](https://developer.chrome.com/devtools/docs/commandline-api#monitoreventsobject-events) in the Devtools CLI. There's also the Visual Events Chrome Extension.


Snippets don't have a "Save as" anymore (find issue)
Keyboard shortcuts can't be customized per [this](http://stackoverflow.com/questions/8192714/customize-chrome-debugger-keyboard-shortcuts)

## Protips
[Possible to save a filter in the Chrome console?](https://stackoverflow.com/questions/40750524/possible-to-save-a-filter-in-the-chrome-console)

Expand Network tab
* second number is latency per https://stackoverflow.com/questions/40851400/in-chrome-network-tab-under-the-size-column-what-do-the-2-numbers-represent

Be sure to turn on blackbox content-scripts; however, they still run unless you're in icognito
Use async checkbox
Put breakpoints on DOM mutation
To copy a variable into the clipboard, [use `copy()`](http://superuser.com/questions/777213/copy-json-from-console-log-in-developer-tool-to-clipboard) in the console
If you run into [Chrome Development Tool: [VM] file from javascript](http://stackoverflow.com/questions/17367560/chrome-development-tool-vm-file-from-javascript), check the `expression` argument to the `evaluate()` function.
[How to increase number of Call Stack entries in Google Chrome Developer Tools (or Firefox)?](http://stackoverflow.com/questions/9931444/how-to-increase-number-of-call-stack-entries-in-google-chrome-developer-tools-o) - CLI switch

**Chrome dock**<br/>
Similar to the OSX dock but for Chrome tabs, so they are hidden: 

* Panel Tabs ([github](https://github.com/lnikkila/chrome-panel-tabs)) is basically what I'm looking for.
* Apparently available for [Chrome OS](http://www.omgchrome.com/chrome-for-mac-hosted-app-integration-spotlight/). 
* [Fluid](http://alternativeto.net/software/fluid/) costs money.
