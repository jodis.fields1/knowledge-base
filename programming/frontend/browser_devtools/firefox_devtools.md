


## 2018-03 general #linkdump 
https://hacks.mozilla.org/2014/02/live-editing-sass-and-less-in-the-firefox-developer-tools/ | Live Editing Sass and Less in the Firefox Developer Tools – Mozilla Hacks – the Web developer blog
https://stackoverflow.com/questions/42504326/what-unique-features-do-the-firefox-devtools-have-that-the-chrome-devtools-dont | What unique features do the Firefox DevTools have that the Chrome DevTools don't have and vice versa? - Stack Overflow
https://bugzilla.mozilla.org/show_bug.cgi?id=1018184 | 1018184 - Fix uncaught promise rejections in DevTools tests
https://bugzilla.mozilla.org/show_bug.cgi?id=1182254 | 1182254 - (dt-contribute) [meta] Reducing DevTools Contribution Friction
https://bugzilla.mozilla.org/show_bug.cgi?id=1389864 | 1389864 - When Developer Tools is open, it drastically slows down page loading