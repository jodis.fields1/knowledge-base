
As of Chrome 69.0.3497.100, it seems like this started working sort of...? But all chromium bug reports are still open


### 2018-09-22
https://stackoverflow.com/questions/52160700/is-it-possible-to-write-directly-to-sass-partials-from-the-dev-tools | css - Is it possible to write directly to sass partials from the dev tools? - Stack Overflow
https://stackoverflow.com/search?tab=newest&q=%5bgoogle-chrome-devtools%5d%20workspace | Newest posts containing 'workspace' - Stack Overflow
https://stackoverflow.com/questions/51326464/chrome-devtools-persisting-changes-via-styles-panel | css - Chrome DevTools persisting changes via Styles panel - Stack Overflow
https://medium.com/@lampt2509/set-up-persistence-scss-from-chrome-devtools-for-angular-220ce0818568 | Set up persistence scss from Chrome DevTools for Angular
https://stackoverflow.com/questions/49573358/is-there-a-way-to-use-chromes-local-overrides-workspace-with-angular-2s-ts/49815094#49815094 | Is there a way to use Chrome's local overrides / workspace with Angular 2+'s ts file? - Stack Overflow
https://developers.google.com/web/tools/chrome-devtools/workspaces/ | Save Changes To Disk With Workspaces  |  Tools for Web Developers  |  Google Developers


[Source maps with newer Chrome DevTools' workspaces #6400](https://github.com/webpack/webpack/issues/6400)

TODO: find wishlist bug report for Firefox per https://stackoverflow.com/questions/28856004/is-there-an-equivalent-to-chromes-devtools-workspaces-in-firefox
    * https://wiki.mozilla.org/DevTools/Features/Workspaces - what is this? not a real workspaces...?

### Random
https://github.com/NV/chrome-devtools-autosave/issues

com/questions/29232701/unable-to-live-edit-javascript-in-chrome-developer-tools
https://stackoverflow.com/questions/30744802/how-can-i-recompile-and-update-javascript-in-chrome-dev-tools

[Show all changes made through Chrome Developer Tools](https://stackoverflow.com/questions/25020526/show-all-changes-made-through-chrome-developer-tools)
    * precursor to workspaces

### workspaces general
https://encrypted.google.com/search?hl=en&q=webpackDevMiddle%20and%20chrome%20devtools%20workspace | webpackDevMiddle and chrome devtools workspace - Google Search

the supposed key:
https://medium.com/@rafaelideleon/webpack-your-chrome-devtools-workspaces-cb9cca8d50da | Webpack your Chrome DevTools Workspaces – Rafael De Leon – Medium

#### tricks nuances
breakpoints - ?? https://encrypted.google.com/search?hl=en&ei=ZDWPWp_wEYStzwLwzZG4Ag&q=google+chrome+workspace+breakpoints&oq=google+chrome+workspace+breakpoints&
reveal in navigator - https://encrypted.google.com/search?hl=en&q=google%20chrome%20reveal%20in%20navigator%20workspace | google chrome reveal in navigator workspace - Google Search

#### issues raised
https://stackoverflow.com/questions/39460637/how-to-configure-webpack-devtools-workspaces | How to configure Webpack DevTools workspaces? - Stack Overflow
https://stackoverflow.com/questions/24801815/map-folder-to-file-system-chrome-dev-tools | Map folder to file system Chrome Dev Tools - Stack Overflow
https://stackoverflow.com/questions/26514025/chrome-dev-tools-mapping-network-resource-to-local-file-not-working | javascript - Chrome Dev Tools - Mapping network resource to local file not working - Stack Overflow
https://github.com/webpack/webpack/issues/6400 | Source maps with newer Chrome DevTools' workspaces · Issue #6400 · webpack/webpack
https://github.com/webpack/webpack/issues/2395 | How to resolve file path from a webpack:/// URI · Issue #2395 · webpack/webpack
https://github.com/webpack/webpack.js.org/pull/763/files | Watch Mode with Chrome DevTools Workspaces by rafde · Pull Request #763 · webpack/webpack.js.org

### official docs / explanations
https://developers.google.com/web/tools/setup/setup-workflow | Set Up Persistence with DevTools Workspaces  |  Tools for Web Developers  |  Google Developers
https://developers.google.com/web/updates/2017/10/devtools-release-notes#workspaces | What's New In DevTools (Chrome 63)  |  Web  |  Google Developers

#### unoffocial 
https://glebbahmutov.com/blog/local-overrides/ | Local overrides | Better world by better software

#### chromium bug reports
[workspace component:Platform>DevTools](https://bugs.chromium.org/p/chromium/issues/list?can=2&q=workspace%20component%3APlatform%3EDevTools)
    * https://bugs.chromium.org/p/chromium/issues/detail?id=809731 | 809731 - Doesn't map automaticly - please bring back manually adding - chromium - Monorail
    * https://bugs.chromium.org/p/chromium/issues/detail?id=807996 | 807996 - Chrome Dev Tools - workspace for SCSS - chromium - Monorail
[component:Platform>DevTools overrides](https://bugs.chromium.org/p/chromium/issues/list?can=2&q=component%3APlatform%3EDevTools+overrides&colspec=ID+Pri+M+Stars+ReleaseBlock+Component+Status+Owner+Summary+OS+Modified&x=m&y=releaseblock&cells=ids)

