https://www.w3.org/WAI/perspectives/ | Web Accessibility Perspectives Videos: Explore the Impact and Benefits for Everyone

## tech / css
https://github.com/rebeccacremona/a11y-games

## legal / social
https://www.searchenginejournal.com/ada-compliant-website/200106/ | ADA Compliance for Websites - A Beginner's Guide
https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA | ARIA - Accessibility | MDN

## motivation - 2018-03 !nav-element-for-admin-quick-nav
prompted by use of <nav> element and https://css-tricks.com/navigation-in-lists-to-be-or-not-to-be/
https://hackernoon.com/ada-compliance-lawsuits-and-your-web-presence-797ef03cdef2 | ADA Compliance and Your Website — Should You Actually Be Worried?

### development helpers
https://github.com/Khan/tota11y - seems to be the best
aXe by Deque Systems - super popular https://chrome.google.com/webstore/detail/axe/lhdoppojpmngadmnindnejefpokejbdd?hl=en

[Is it ok to use h1 and then h3 instead of h1 and then h2?](https://ux.stackexchange.com/questions/49111/is-it-ok-to-use-h1-and-then-h3-instead-of-h1-and-then-h2) - practical example!

### development process
http://blog.jantrid.net/2015/12/woe-aria-aria-describedby-to-report-or.html | Jantrid: Woe-ARIA: aria-describedby: To Report or Not to Report?

#### other (untried)
https://chrome.google.com/webstore/detail/wave-evaluation-tool/jbbplnpkjmmeebjpijfedlgcdilocofh/reviews?hl=en | Chrome Web Store - Extensions
https://chrome.google.com/webstore/detail/axe/lhdoppojpmngadmnindnejefpokejbdd?hl=en | Chrome Web Store - Extensions
https://chrome.google.com/webstore/detail/siteimprove-accessibility/efcfolpjihicnikpmhnmphjhhpiclljc?hl=en | Siteimprove Accessibility Checker - Chrome Web Store
https://github.com/google/automation-inspector | google/automation-inspector

### screen readers
https://en.wikipedia.org/wiki/List_of_screen_readers | List of screen readers - Wikipedia

#### chromevox
only for Chromebook, browser-focused
    * Chromium version no longer updated
https://github.com/cynthia/chromevox | cynthia/chromevox: Chromevox only export of Chrome accessibility suite (https://code.google.com/p/google-axs-chrome/).
https://chromium.googlesource.com/chromium/src/+/lkgr/docs/accessibility/chromevox.md | ChromeVox (for developers)

#### nvda
windows only
https://github.com/nvaccess/nvda | nvaccess/nvda: NVDA, the free and open source Screen Reader for Microsoft Windows
https://github.com/nvaccess/nvda/issues/8103 | NVDA does not read the level in multilevel nested lists · Issue #8103 · nvaccess/nvda

#### orca
linux screenreader
https://alternativeto.net/software/orca/ | Orca Screen Reader Alternatives and Similar Software - AlternativeTo.net

### aria

https://alternativeto.net/software/atbar/ | ATbar Alternatives and Similar Websites and Apps - AlternativeTo.net
https://addons.mozilla.org/en-US/firefox/addon/wave-accessibility-tool/?src=search | WAVE Accessibility Extension – Add-ons for Firefox
https://groups.google.com/forum/#!topic/axs-chrome-discuss/o-5E9IDHiwU | Google Groups
