
### 2019
revisting this briefly - why did I dismiss the transparent overlay approach? mentioned in a comment on below article:
https://css-tricks.com/dangers-stopping-event-propagation/#comments

### 2018
do we need bubbling at all?

    jcrben 8 hours ago
    I dislike relying on propagation - can hardly remember when I've really needed it

    @AnthonyAstige
    AnthonyAstige 8 hours ago
    I find propagation a tool, like many other relatively complex ones, best used only if and when needed. And it probably gets abused (like other complex tools).

    The ClickOutside event listeners are at the document level, which makes sense to me because of the nature of "click on anything". This relies on propagation from lower dom elements to the higher.

    I haven't had too many cases of needing propagation either, but I find neat when a real case comes up.

    @jcrben
    jcrben 8 hours ago
    Relying on propagation to the body does seem like it would make sense in certain situations - but in practice it ends up being quite difficult to apply because:
    (1) you are vulnerable to things in the middle unpredictably stopping propagation, as we've done here (and sometimes happens in library code which is difficult to apply) and
    (2) when something doesn't stop propagation, you've got all sorts of stuff above you that you really don't want to worry about firing

    The alternative to clickoutside and bubbling is that you apply an overlay (could be transparent) on everything outside the focus box and register the handler on that.

    @AnthonyAstige
    AnthonyAstige 7 hours ago
    The alternative to clickoutside and bubbling is that you apply an overlay (could be transparent) on everything outside the focus box and register the handler on that.

    With an overlay is there a clean way to do both:

    Trigger the side effect (for example here: close the slide out panel)
    Trigger the expected user action (for example here: focusing in a table row)
    I'm thinking the event would be captured by the element with the highest stacking over, in this case the overlay. Triggering the side effect, but not the expected user action.

    @jcrben
    jcrben 7 hours ago  • 
    That sounds like the type of thing that could be handled directly by redux actions? The overlay can be tied to actions that do whatever needs doing based upon the state.

    In terms of just general clickoutside, the element that you click outside of would be on top - sort of a lightbox without necessarily having a lightbox.

    When I had to support clickoutside (anywhere) to close a box in the NGA a year or two ago, I had to refactor out stopPropagation from a few places.