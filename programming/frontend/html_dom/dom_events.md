
### 2018
[Monitor Events](https://developers.google.com/web/tools/chrome-devtools/console/events)
see dom_events_bubbling_debate.md for discussion from https://github.com/brightideainc/main/pull/6663#discussion_r175863952
* note that https://github.com/facebook/react/issues/6410 conversation shows that others don't like bubbling either

[Tasks versus microtasks](https://jakearchibald.com/2015/tasks-microtasks-queues-and-schedules/)
    * from 2015 - relevant in 2018? probably
    * https://developer.mozilla.org/en-US/docs/Web/JavaScript/EventLoop

### 2017
[onFocusIn/onFocusOut events](https://github.com/facebook/react/issues/6410#issuecomment-292895495) - onBlur does not bubble and is encapsulated! great, but React doesn't implement it properly; also a bit of a gotcha in event delegation [React prevent event bubbling in nested components on click](https://stackoverflow.com/questions/38619981/react-prevent-event-bubbling-in-nested-components-on-click)

### original
Start with: https://developer.mozilla.org/en-US/docs/Web/Events with all the events

Then see http://www.quirksmode.org/js/events_order.html which explains bubbling versus capture...

BUT jQuery [does not support capturing](https://stackoverflow.com/questions/7163616/why-does-jquery-event-model-does-not-support-event-capture-and-just-supports-eve)!!

http://stackoverflow.com/questions/743876/list-all-javascript-events-wired-up-on-a-page-using-jquery

e.target is what triggered the event (could be bubbling?) up while e.currentTarget is what you bound it to [Difference between e.target and e.currentTarget](https://stackoverflow.com/questions/5921413/difference-between-e-target-and-e-currenttarget)
    * as of 2018, still don't really know this
    * https://stackoverflow.com/a/5921528/4200039 and http://joequery.me/code/event-target-vs-event-currenttarget-30-seconds/ for interactive example

NOTE: keydown and keyup are quite different (same with mousedown and mouseup) - using mousedown you can preventDefault to prevent the focus from changing! (ditto for keydown). can't do that with click. keyup can be helpful to see where you ended up and act accordingly (i.e., go back)

TODO: explore functional reactive programming and RxVision

[How to Access jQuery's Internal Data](http://elijahmanor.com/how-to-access-jquerys-internal-data/) - use this to access the events, also see [How to quickly view what js code is assigned to a given element](https://groups.google.com/forum/#!topic/google-chrome-developer-tools/NTcIS15uigA) and of course the Visual Event plugin

[The Dangers of Stopping Event Propagation](https://css-tricks.com/dangers-stopping-event-propagation/) - interesting but OVERRATED

Events
http://help.dottoro.com/larrqqck.php has a complete list of events