

## Basic inheritance:
Event Handler -> Element -> HTMLElement -> DivElement

## General
It's sometimes casually said that you should learn the DOM API before you learn jQUery. That makes some sense as it is more fundamental, but people use jQuery because the DOM API is awkward and verbose to work with. The best overview I've seen is [You Don't Need jQuery](http://blog.garstasio.com/you-dont-need-jquery/).

With that said, the core of the API is actually smaller than jQuery and quite manageable, as [this inheritance diagram illustrates](http://html5tutorial.com/dom-inheritance-map/).