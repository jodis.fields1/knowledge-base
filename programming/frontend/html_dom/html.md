HTML: The Living Standard - Developer's Edition https://html.spec.whatwg.org/dev/

2018-02:
learned that most events are "supported" for all HTML elements but meaningless:
"While these attributes apply to all elements, they are not useful on all elements. For example, only media elements will ever receive a volumechange event fired by the user agent"
    * debated this on WHATWG IRC channel TODO find that discussion

only place that I could find listing actual relevant elements is W3Schools:
https://www.w3schools.com/jsref/event_onload.asp



## 2018-03 link dump HTMLvalidation
noticed that I was doing non-semantic things and aria wasn't checking it
https://validator.github.io/validator/ | The Nu Html Checker (v.Nu)
https://github.com/validator/validator/issues/573 | Publish Docker Image with Standalone Web Server on Docker Hub · Issue #573 · validator/validator
https://codeburst.io/the-front-end-checklist-8b2292fdda44 | The Front-End Checklist – codeburst

### react
cannot validate react!
https://stackoverflow.com/questions/36650295/w3c-html-validation-for-react-jsx-files | W3C HTML validation for React JSX files - Stack Overflow

#### vscode react
nothing that really tries to do W3C validation for react in vscode...

### vscode
https://stackoverflow.com/questions/44559598/vs-code-html-error-checking-validation | VS Code HTML error checking / validation - Stack Overflow
    * https://github.com/Microsoft/vscode-htmlhint | Microsoft/vscode-htmlhint: VS Code integration of HTMLHint, an HTML linter.

## urls
use root relative URLs:
https://webmasters.stackexchange.com/questions/56840/what-is-the-purpose-of-leading-slash-in-html-urls