## atomic design
[Atomic Design](https://news.ycombinator.com/item?id=17998315)

### 2018 update
#### ARc
architectural style:

https://arc.js.org/ | ARc - Atomic React
https://github.com/diegohaz/arc/issues/294 | Are you using ARc? Please, read this. · Issue #294 · diegohaz/arc
https://github.com/velopert/bitimulate | velopert/bitimulate: Simulated cryptocurrency trading system
https://github.com/velopert/bitimulate/blob/master/bitimulate-frontend/src/components/App.js | bitimulate/App.js at master · velopert/bitimulate

#### generic
https://web.archive.org/web/20170624111907/https://medium.com/@yejodido/atomic-components-managing-dynamic-react-components-using-atomic-design-part-1-5f07451f261f

https://medium.com/@alexmngn/why-react-developers-should-modularize-their-applications-d26d381854c1 | Why React developers should modularize their applications?

https://blog.usejournal.com/thinking-about-react-atomically-608c865d2262 | Thinking About React, Atomically ⚛ – Noteworthy - The Journal Blog

https://codeburst.io/atomic-design-with-react-e7aea8152957 | Atomic Design with React – codeburst

https://medium.com/@alexmngn/why-react-developers-should-modularize-their-applications-d26d381854c1 | Why React developers should modularize their applications?

https://github.com/danilowoz/react-atomic-design | danilowoz/react-atomic-design: Boilerplate with 
the methodology Atomic Design using a few cool things.

#### bit
https://blog.bitsrc.io/structuring-a-react-project-a-definitive-guide-ac9a754df5eb | Structuring a React Project - a Definitive Guide – Bits and Pieces
https://blog.bitsrc.io/simplify-complex-ui-by-implementing-the-atomic-design-in-react-with-bit-f4ad116ec8db | Atomic Design With React And Bit: Simplify a Complex UI
https://github.com/teambit/bit | teambit/bit: Easily share code between projects with your team.

#### react versus angular
https://www.reddit.com/r/Angular2/comments/960sbe/what_does_react_honestly_have_over_angular/ | What does React honestly have over Angular? : Angular2

## checklist
CDN, HTML validation, security, SEO, licensing...
https://web.archive.org/web/20180330214200/https://codeburst.io/the-front-end-checklist-8b2292fdda44?gi=1b63a08b863c

## tutorial sites
https://survivejs.com/
