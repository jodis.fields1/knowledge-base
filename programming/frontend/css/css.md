See anki!

TODO: protips - think about sizing (width and height carefully); 0,0,0,0 for top,right,bottom etc will create a max size block
TODO: https://github.com/typestyle/typestyle

### spec study

[[css-overflow-3] Clarify padding-bottom in overflow content](https://github.com/w3c/csswg-drafts/issues/129#issuecomment-380861187)
    * I filed this; years later, still not resolved...

[CSS Spec: block-level box, block container box and block box](https://stackoverflow.com/questions/30883857/css-spec-block-level-box-block-container-box-and-block-box) - tables are not boxes...
2018-03-24: reviewed spec again, noticed that box-sizing: border-box does not enforce the height / width against excessive padding which makes sense (max-height, max-width) but where in the spec can I find that?
    * similar to https://stackoverflow.com/questions/46688896/padding-overrides-height-of-the-element-with-box-sizing-border-box

### encounters / tips
[Make <body> Take Up 100% of the Browser Height](https://www.kirupa.com/html5/make_body_take_up_full_browser_height.htm) -
    * "In HTML and CSS, some of the greatest mysteries revolve around two things:"
##### Layout and position
NOTE: this is where complexity lies
[CSS Positioned Layout Module Level 3](https://www.w3.org/TR/css-position-3/) - 
[CSS Display Module Level 3](https://www.w3.org/TR/css-display-3/) -

### Standards
https://developer.mozilla.org/en-US/docs/Web/CSS/CSS3
https://drafts.csswg.org/ - 
https://github.com/w3c/csswg-drafts
https://hg.csswg.org/drafts

### frameworks / ui toolkits / philosophies
http://milligram.io/
https://purecss.io/
https://github.com/inuitcss/inuitcss - no themes, just BEM structure - see found thru https://www.sitepoint.com/component-driven-css-frameworks/
Spectre?
[Mini.css](https://news.ycombinator.com/item?id=14264494&utm_term=comment) looks interesting

### grid layout
https://github.com/corysimmons/postcss-ant - ?
https://github.com/stubbornella/oocss/wiki/grids#base-classes - contains a test grid


###### other
margin-colllapsing - 
div collapsing - 
:before pseudoelement
floats

###### stacking context and z-index
So I thought I knew how a stacking context worked, but it turned out I didn't. The [series of MDN articles](https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Positioning/Understanding_z_index/The_stacking_context) finally sunk in. The key point I was missing is that z-index primarily happens among sibling elements.

###### pixels
First off, what is a pixel? See [What's a Pixel?](https://developer.appway.com/screen/ShowSingleRecipe/selectedRecipeId/1429057950522) for an explanation. Also [see this](http://stackoverflow.com/a/609561/4200039).

importantly, aspect ratios can vary! Generally it is width:height but that might be reversed in mobile phones. For user experience, width probably matters the most. In decimal, width to height ratio for Android phones ranges from 1.33 (4:3) to 1.77 (16:9) per [this](http://stackoverflow.com/questions/7199492/what-are-the-aspect-ratios-for-all-android-phone-and-tablet-devices).


CSS pixel != physical pixel
CSS pixel == physical pixel / device pixel ratio. [This howtodesign.com article](http://www.howdesign.com/featured/hardware-css-pixels-retina-display/) refers to a deleted Wikipedia article for ratios, so I'm not sure where to find the ratios.

[Browser compatibility — viewports](http://www.quirksmode.org/mobile/tableViewport.html) is a major source.

[Designing For Device Orientation: From Portrait To Landscape](http://www.smashingmagazine.com/2012/08/10/designing-device-orientation-portrait-landscape/)

[Comment at  Of Device Screen Sizes and Aspect Ratios](http://winsupersite.com/mobile-devices/device-screen-sizes-and-aspect-ratios) observes that 16:9 can feel awkwardly long in portrait or crunched in landscape.

#### Animations
https://github.com/daneden/animate.css

[[css-transitions] Transition to height (or width) "auto"](https://github.com/w3c/csswg-drafts/issues/626#issuecomment-399586189)
    * 