See also: ../frontend/react/react_ui_component_toolkit.md

https://pustelto.com/blog/css-vs-css-in-js-perf/
* pointed me to https://github.com/callstack/linaria/blob/master/docs/HOW_IT_WORKS.md

## 2019-11-20 evaluation
* thought about ant.design
* noticed https://bit.dev/nexxtway/react-rainbow from some Spanish firm
* [Base Web, Uber’s New Design System for Building Websites in React](https://news.ycombinator.com/item?id=19758848) - pointed me to Material-UI which is emphasizing customization, also Reach-UI

CONCLUSION: installing 

## 2018-01-15 link dump:
Conclusion: wait a bit on css modules, use postcss conservatively

good: https://tylergaw.com/articles/sass-to-postcss

https://github.com/w3c/csswg-drafts/issues/1571
http://technotif.com/create-grids-postcss-ant/

### linters
https://github.com/stylelint/stylelint

### css modules
good: http://kevinsuttle.com/posts/css-modules-a-review

https://github.com/css-modules/css-modules/issues/187
https://github.com/css-modules/icss
https://github.com/tivac/modular-css
https://github.com/css-modules/css-modules
https://github.com/css-modules/css-modules/pull/65

#### tooling 
https://www.reddit.com/r/reactjs/comments/6saesk/for_those_who_use_cssmodules_how_do_you_leverage/
https://blog.envylabs.com/webpack-2-postcss-cssnext-fdcd2fd7d0bd
https://github.com/gajus/react-css-modules/issues/57#issuecomment-159255217

### css-in-js
[Re: "‬How does writing CSS in JS make it any more maintainable?"](https://news.ycombinator.com/item?id=18681899)

https://blog.bitsrc.io/9-css-in-js-libraries-you-should-know-in-2018-25afb4025b9b

big list of options at https://github.com/MicheleBertoli/css-in-js

Emotion? - https://mobile.twitter.com/wonderboymusic/status/966732920437268481
https://github.com/jonathantneal/precss
https://github.com/anandthakker/doiuse
https://github.com/FormidableLabs/radium

https://github.com/styled-components/styled-components
https://www.reddit.com/r/reactjs/comments/70o52m/styled_components_glamorous_or_emotion/
https://reactarmory.com/answers/should-i-use-css-in-js

https://www.reddit.com/r/reactjs/comments/6saesk/for_those_who_use_cssmodules_how_do_you_leverage/dlc9kg9/
https://www.reddit.com/r/javascript/comments/3yuudg/tj_holowaychuk/


