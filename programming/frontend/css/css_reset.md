2018-04:

I took a look into this and consulted the following resources:
https://css-tricks.com/reboot-resets-reasoning/ | Reboot, Resets, and Reasoning | CSS-Tricks

## browser defaults
https://stackoverflow.com/questions/6867254/browsers-default-css-for-html-elements | Browsers' default CSS for HTML elements - Stack Overflow
    * https://html.spec.whatwg.org/multipage/rendering.html#non-replaced-elements contains recommendations
## examples
https://github.com/shannonmoeller/reset-css | shannonmoeller/reset-css: An unmodified copy of Eric Meyer's CSS reset. PostCSS, Webpack, Sass, and Less friendly.
https://github.com/necolas/normalize.css | necolas/normalize.css: A modern alternative to CSS resets
https://github.com/sindresorhus/modern-normalize | sindresorhus/modern-normalize: Normalize browsers' default style

## other
https://css-tricks.com/getting-acquainted-with-initial/ | Getting Acquainted with Initial | CSS-Tricks