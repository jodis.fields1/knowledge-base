https://css-tricks.com/snippets/css/a-guide-to-flexbox/ - when reading this, take NOTE of the two columns, left for container and right for items

https://flexboxfroggy.com/


[Gridgarden: A cool game to learn CSS Grid (cssgridgarden.com)](https://news.ycombinator.com/item?id=21050501)

https://www.smashingmagazine.com/2018/08/flexbox-alignment/ - GREAT
* also see https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Box_Alignment/Box_Alignment_in_Flexbox
* https://www.smashingmagazine.com/2018/08/flexbox-display-flex-container/

## devtools
https://hacks.mozilla.org/2019/01/designing-the-flexbox-inspector/

## sticky footer w/ flexbox
https://css-tricks.com/couple-takes-sticky-footer/#article-header-id-3 | Sticky Footer, Five Ways | CSS-Tricks
https://stackoverflow.com/questions/17521178/css-position-relative-stick-to-bottom | html - css position: relative; stick to bottom - Stack Overflow
https://stackoverflow.com/questions/31000885/align-an-element-to-bottom-with-flexbox | html - Align an element to bottom with flexbox - Stack Overflow

## flexbox must-know
* https://teamtreehouse.com/community/do-children-of-children-elements-become-flex-items-as-well | Do children of children elements become flex items as well? - NO

## flex-basis and width
http://gedd.ski/post/the-difference-between-width-and-flex-basis/
> You probably noticed that in all of our illustrations we visualized the size of the flex items before they got put into the flex container. We did that because that’s exactly what flex-basis is: the size of flex items before they are placed into a flex container. It’s the ideal or hypothetical size of the items. But flex-basis is not a guaranteed size!

2018-11: reviewed https://medium.freecodecamp.org/even-more-about-how-flexbox-works-explained-in-big-colorful-animated-gifs-a5a74812b053

Duh, this is core to flexbox - follwed with flex-grow

## IE11
[IE 11, Flexbox, and Absolute Positioning Not Working](https://stackoverflow.com/questions/37995819/ie-11-flexbox-and-absolute-positioning-not-working/37999178#37999178)

https://github.com/philipwalton/flexbugs/issues/157
https://github.com/philipwalton/flexbugs
https://stackoverflow.com/questions/37534254/flex-auto-margin-not-working-in-ie10-11