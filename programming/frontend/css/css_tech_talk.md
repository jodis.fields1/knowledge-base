TODO: sketch out CSS tech talk

Sketch:
* overview of theory; CSS 2.1 and then the split into multiple modules, inherited is generally font and such
* box model and box-sizing; height and width?
* position: relative and child absolute
* centering, especially vertical centering
* flexbox?

### flebox bugs
See ./css_flexbox.md


### css grid
https://learncssgrid.com/

https://css-tricks.com/css-grid-in-ie-css-grid-and-the-new-autoprefixer/

https://css-tricks.com/snippets/css/complete-guide-grid/
https://developers.google.com/web/updates/2017/01/css-grid
https://stackoverflow.com/questions/14772778/display-table-cell-on-new-row
[Ask HN: What should I know before I invest fully in CSS grid?](https://news.ycombinator.com/item?id=15880697) - 

https://stackoverflow.com/questions/43311943/prevent-content-from-expanding-grid-items