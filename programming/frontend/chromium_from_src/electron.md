Electron apps
===

## general
https://jlongster.com/secret-of-good-electron-apps

## debugging
[Can not copy text of devtools by context menu](https://github.com/electron/electron/issues/525#issuecomment-397791455)

## is it slow and heavy?
not sure; according to https://www.reddit.com/r/linuxmasterrace/comments/605af7/firefoxlike_electron/ it's super-stripped down and only 50-60MB per instance; tabs and processes cause lots of memory use