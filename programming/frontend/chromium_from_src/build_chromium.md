TODO: move these huge files to a flash drive

2018-03-03 - 2018-03-04 project

alternative: [Chromium Compiling Setup for DevTools Hackers](https://gist.github.com/paulirish/2d84a6db1b41b4020685)

IDE: https://chromium.googlesource.com/chromium/src/+/master/docs/vscode.md

perhaps start at [Engineering design docs](https://www.chromium.org/developers/design-documents)
    * assumed background: https://www.chromium.org/developers/design-documents/multi-process-architecture

Start with https://chromium.googlesource.com/chromium/src/+/lkcr/docs/mac_build_instructions.md

had to install XCode - also open it to install the components
apparently it requires a sudo permission at some point, but only running Firefox bootstrap
triggered that

Conclusion:
* `./out/content/Content\ Shell.app/Contents/MacOS/Content\ Shell` contains the render engine
* ./out came out to around 100+GB

## updates
https://groups.google.com/a/chromium.org/forum/#!topic/chromium-discuss/HHMbfxPOuEY - suggests `gclient sync`

## build instructions (plain)
[How to build Chromium faster?](https://stackoverflow.com/questions/37134466/how-to-build-chromium-faster)
should build the content shell https://chromium.googlesource.com/chromium/src/+/lkcr/docs/android_build_instructions.md#build-content-shell (Android)

https://chromium.googlesource.com/chromium/src/+/lkcr/docs/linux_build_instructions.md#faster-builds

use depot-tools:
`fetch --no-history chromium` (took an hour or two on home connection)
`gn gen out/Default` - this might be related to later build issues / flakiness?

### issues
https://codereview.chromium.org/?closed=0 - why mostly 2 months ago?
* "Too many open files" - work thru per https://superuser.com/a/443168/457084
* ../../services/network/public/cpp/resource_response_info.h:24:10: fatal error: 
'services/network/public/mojom/fetch_api.mojom-shared.h' file not found, on commit 52e08bd1ea85e5553129c14561b26c99d31211ca
    * seems there are a bunch of generated files which are not generated...
    * docs at https://chromium.googlesource.com/chromium/src/+/master/mojo
    * [The chromium build is flaky around code generators (gn misses checking generated files?)](https://bugs.chromium.org/p/chromium/issues/detail?id=655123)
        * left comment at https://github.com/ninja-build/ninja/pull/1331#issuecomment-370164963

## CI
Build bot: https://luci-milo.appspot.com/p/chromium
for Mac: 
    * success: https://luci-milo.appspot.com/buildbot/chromium/Mac/38850
    * failure: https://luci-milo.appspot.com/buildbot/chromium.fyi/Chromium%20Mac%2010.13/265

### tests
https://chromium.googlesource.com/chromium/src/+/lkcr/docs/testing/
mostly focused on layout tests?

flakiness is an issue: [Flaky test rate is too high](https://groups.google.com/a/chromium.org/forum/#!topic/chromium-dev/F-cxPmzy7e0)

## src in general

### blink DOM rendering
turns out most of the code is in chromium/third_party/WebKit/Source/core/dom/ (
    * third-party misleading since actively hacked on, but noted in https://www.chromium.org/developers/content-module
    * spreadsheet tracks where stuff is being moved

content dir/ has the blink render code, ignore everything else
* chromium/content/browser/browser_main_runner.cc seems to be sort of an entry point
* ->chromium/content/browser/browser_main_loop.cc
* ?->chromium/content/public/common/content_client.cc
* ?->chromium/content/public/browser/web_contents.cc
* ?->chromium/content/public/renderer/content_renderer_client.cc
* searched for initialize_renderer->
    * ->content/browser/web_contents/web_contents_impl.cc
        * ->chrome/browser/android/web_contents_factory.cc (don't care about android)

#### layout (my particular interest)
https://groups.google.com/a/chromium.org/forum/#!forum/layout-dev

fixing all bugs now?
https://bugs.chromium.org/p/chromium/issues/list?can=2&q=component%3ABlink>DOM
https://bugs.chromium.org/p/chromium/issues/list?can=2&q=component%3ABlink>Layout