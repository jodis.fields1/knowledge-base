Random explorations are kind of a bad thing. Starting this (2018-05) and hopefully
I can notice and prevent it...

## ReasonML
https://news.ycombinator.com/item?id=16316004 | Reason ML toolchain | Hacker News
http://2ality.com/2017/11/about-reasonml.html | What is ReasonML?
http://www.akitaonrails.com/2015/10/28/personal-thoughts-on-the-current-functional-programming-bandwagon | Personal Thoughts on the Current Functional Programming Bandwagon | AkitaOnRails.com

## flutter
https://hackernoon.com/why-flutter-uses-dart-dd635a054ebf | Why Flutter Uses Dart – Hacker Noon
https://github.com/dart-lang/sdk/issues/21984 | Structural types · Issue #21984 · dart-lang/sdk
https://top.quora.com/Is-Flutter-likely-to-replace-Java-for-Android-app-development | Is Flutter likely to replace Java for Android app development? - Quora
