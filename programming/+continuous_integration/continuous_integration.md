Related: git

See jenkins-group repo for jenkins stuff

## Quality analysis
[https://github.com/mre/awesome-static-analysis](https://github.com/mre/awesome-static-analysis)

SonarLint (for Intellij plus others) plus SonarQube seems like a cool fit; there's also Scrutinizer, SesioLabsInsight, etc

check out [codeclimate.com](http://codeclimate.com), [Cyclomatic Code Complexity](https://en.wikipedia.org/wiki/Cyclomatic_complexity), [Halstead complexity measures](https://en.wikipedia.org/wiki/Halstead_complexity_measures), ReSharper, and check out call graphs

used [Using code metrics to guide code reviews](http://www.microsoft.com/en-gb/developers/articles/week03jul15/using-code-metrics-to-guide-code-reviews/) (2015) which has other diagrams; wala and doxygen can generate call graphs

## vendors
### scrutinizer
we use this for PHP, it kinda sucks
    * no editor integration
    * UI is clunky, very hard for me to find ignored issues
        * https://github.com/scrutinizer-ci/scrutinizer/issues/68 did not help

#### docs
TODO: apply https://scrutinizer-ci.com/docs/tools/php/scrutinizer-cs-fixer/
https://scrutinizer-ci.com/docs/tools/php/php-scrutinizer/ - entry
https://scrutinizer-ci.com/docs/configuration/build_reference - general