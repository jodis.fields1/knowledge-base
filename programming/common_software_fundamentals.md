Software fundamentals
===

https://github.com/mr-mig/every-programmer-should-know

[Software Engineering principles to make teams better (principles.dev)](https://news.ycombinator.com/item?id=27688612)

Also see c_systems repo

## quick ref 
https://github.com/sdmg15/Best-websites-a-programmer-should-visit#when-you-get-stuck
* see https://github.com/sdmg15/Best-websites-a-programmer-should-visit#when-you-get-stuck

### learn quick
* http://hyperpolyglot.org/
* https://learnxinyminutes.com/
* https://devdocs.io/

### use quick tools protips
See dev_tools.md

* diffnow: https://www.diffnow.com/
* JSON: https://jsoneditoronline.org/
* dependency viewer: http://npm.anvaka.com/#/view/2d/react-router
* Network tools: https://mxtoolbox.com/NetworkTools.aspx
  * Subnet: https://mxtoolbox.com/SubnetCalculator.aspx

## interpreters / compilers
See linux_lore/lexing_parsing.md

2019-08-19: [How to implement a programming language in JavaScript](https://web.archive.org/web/20190716080127/http://lisperator.net/pltut/)

[Let's Build a Simple Interpreter. Part 14: Nested Scopes (2017) ](https://news.ycombinator.com/item?id=20313220) - found https://craftinginterpreters.com/

See Lisp book

## talks
TODO: add the talk about SOLID which I posted in Hackreactor clan

[Show HN: TechYaks – Best of 50k tech talks ranked by confidence intervals](https://news.ycombinator.com/item?id=17988464)

## books
2018-06-26: skimmed Racket repo while glancing at Little Schemer; turns turns out it doesn't run for lots of people
    * [Previous Up Next: The origin of CAR and CDR in LISP](http://www.iwriteiam.nl/HaCAR_CDR.html) speculates `car` means Content Address Register and `cdr` means Copy Decrement Register
2018-06-01: skimmed SICP Pocket, should go with SICP distilled
    * also see Andy Kitson: https://github.com/ajkitson/sicp/blob/master/ch1/1.3.scm

## streams
https://github.com/whatwg/streams
Discussed in every language
the idea is that certain data is either a stream or a block

why streams? epiphany: you can't transfer all the data at once! altho perhaps in Linux filesystems you can sort of do that sometimes

## history
https://twobithistory.org/timeline.html
