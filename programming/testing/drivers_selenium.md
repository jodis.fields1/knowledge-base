
See react_redux_test.md for modern notes

## takeaway?
[Dan Dascalescu's brain-dump](http://wiki.dandascalescu.com/howtos/from_zero_to_automated_web_app_testing_best_practices) recommends ChimpJS and Cucumber.

2018 reflection: ChimpJS is struggling with how it fiberizes webdriverio (per https://github.com/xolvio/chimp/issues/612) - important lesson: focus on a large, sustainable community (!!) and try not to get too abstracted from upstream (the browsers)
https://github.com/dhamaniasad/HeadlessBrowsers

tl;dr - for cross-browser tests, use Webdriver (maybe WebdriverIO) - for quick scraping, maybe CasperJS with SlimerJS or Puppeteer / Chromy (phantomjs replacement) or Electron
there's also https://github.com/vvo/selenium-standalone which I think I used

## 2017 braindump potential article...
[Selenium IDE alternatives for UI regression testing](https://news.ycombinator.com/item?id=13586904) (2017) - recent discussion, see http://screenster.io/a-better-way-to-do-visual-regression-testing/

TODO: Write README for docs thus. Include [ChromeDriver homepage](https://sites.google.com/a/chromium.org/chromedriver/) with other drivers available on IE. Include https://www.tildedave.com/2015/11/11/scaling-out-tilts-nightwatch-tests-with-magellan.html
[WEBDRIVERJS AND PROMISES](http://blog.scottlogic.com/2015/03/04/webdriverjs-and-promises.html) - gotcha when retrieving values

TODO: [upgrade from selenium rc to webdriver](http://stackoverflow.com/questions/5870480/upgrade-from-selenium-rc-to-webdriver?rq=1) and WebDriverBacked to migrate our old tests over


When I went to Hackreactor, someone described Selenium as fit only for masochists. I'm not sure what PhantomJS or unit testing frameworks are in comparison, since they're no walk in the park either. I've found Selenium is actually not so bad once you manage stumble through the maze of outdated documentation and Java snippets. Here's a brain-dump, with a bias on the nodejs bindings and the npm [`selenium-webdriver`](https://www.npmjs.com/package/selenium-webdriver) package:[^n]

The official project is a bit of a mess. Although the above linked package page does manage to run an automated browser instance, it has no instructions on how to get it running for tests, even though that is the primary use. I used [Getting started with Selenium Webdriver for node.js](http://bites.goodeggs.com/posts/selenium-webdriver-nodejs-tutorial/) (2014), but it omits a key line: 

`var test = require('selenium-webdriver-testing')`

Thereafter, all tests should be prefixed with `test` (e.g., `test.describe`, `test.it`, etc).

This cost me several hours over the span of a couple days, including diving deep into the source code and perusing its architecture,[^n] before I found [Selenium With Node.js and Mocha: Automated Testing With JavaScript](http://simpleprogrammer.com/2014/02/03/selenium-with-node-js/) (2014) which provided the necessary detail.

Armed with this detail, I successfully wrote a few simple tests. I [perused the API docs](http://seleniumhq.github.io/selenium/docs/api/javascript/)[^n] and the [elementalselenium.com tips](http://elementalselenium.com/tips/42-one-test-multiple-browsers-parallel) and then [interactively explored the API](https://autumnator.wordpress.com/2012/09/25/how-to-debug-test-and-try-selenium-code-with-interactive-shells/) in a REPL-environment. It seems ` this.driver.manage().timeouts().implicitlyWait(10000);` is handy for making sure the elements are on the page but see [Selenium c# Webdriver: Wait Until Element is Present](http://stackoverflow.com/questions/6992993/selenium-c-sharp-webdriver-wait-until-element-is-present) for why you shouldn't do that - often you expect elements to be missing! Note also there is also [Selenium WebDriver - Test if element is present](http://stackoverflow.com/questions/7991522/selenium-webdriver-test-if-element-is-present?lq=1) also findElements which may not throw an exception.

To do something like `setSleep` from Selenium 1.0 (RC), ([see SO question](http://stackoverflow.com/questions/11154982/is-there-any-method-in-webdriver-with-java-for-controlling-speed-of-browser)) I could probably at a sleep or setTimeout to `webdriver.WebDriver.executeCommand_` in webdriver.js.

I tried the supposedly more "user-friendly" abstraction layer, [WebDriverIO](http://webdriver.io/), and managed to get it to locally run tests after some frustration (key command: `./bin/wdio examples/wdio/wdio.mocha.conf.js` from the root directory, as noted at the very bottom of the [example page at this point in time](https://github.com/webdriverio/webdriverio/tree/bc17fbf9cc35822f4bd08cb409161a7fa8ac4644/examples)).

**Multiple browsers**<br/>
Two major approaches to cross-browser testing[^n] are possible: Selenium Grid or a commercial solution such as Sauce Labs, BrowerStack, or [Spoon](https://spoon.net/).

There may a be simpler approach using [vvo/selenium-standalone](https://github.com/vvo/selenium-standalone) or wdio (see [this](Selenium testing workflow with WebdriverIO)) but I haven't figured it out.

There's also theoretically a quick and dirty way on a single machine with threads or spawning multiple child processes. Most of the online examples are a mixture of old (see [mocha-selenium](https://github.com/Strider-CD/mocha-selenium) - by the author of the [awesome RxVision](http://jaredforsyth.com/2015/03/06/visualizing-reactive-streams-hot-and-cold/) and [browserstack/selenium-webdriver-nodejs](https://github.com/browserstack/selenium-webdriver-nodejs)) and Java-based [[1](http://seleniumeasy.com/selenium-tutorials/testing-in-multiple-browsers)].

[Look! No Hands! - Using Selenium and Node.js for interactive UI testing](http://www.randomjavascript.com/2015/04/look-no-hands-using-selenium-and-nodejs.html) - this might let me run multiple browsers with my tests

[Headless Functional Testing with Selenium and PhantomJS](http://code.tutsplus.com/tutorials/headless-functional-testing-with-selenium-and-phantomjs--net-30545) is also helpful.

I'm not sure if these [Selenium server Arguments](https://myselenium.wordpress.com/2011/07/04/selenium-server-arguments/) still apply.

[^n]: Bindings exist for most major languages; the other one interesting me at the current moment is [php-webdriver](https://github.com/facebook/php-webdriver).
[^n]: See Simon Stewart's [entry in AOSA](http://aosabook.org/en/selenium.html), a [pithy summary](https://rationaleemotions.wordpress.com/2015/04/04/webdriver-as-i-know-it/), and [this thread](https://groups.google.com/forum/#!topic/chromedriver-users/xVMy5OGLcl8). Plus there's the [JsonWireProtocol](https://github.com/SeleniumHQ/selenium/wiki/JsonWireProtocol).
[^n]: After first exploring the outdated but higher page-ranked Google Code API version, and opening the page in Firefox since it was broken in Chrome. Oh, the wonders of open-source.
[^n]: Browser marketshare stats can be seen at [netmarketshare.com](http://www.netmarketshare.com/browser-market-share.aspx?qprid=2&qpcustomd=0) or a 2015 *VentureBeat* narrative overview [Chrome passes 25% market share, IE and Firefox slip](http://venturebeat.com/2015/05/01/chrome-passes-25-market-share-ie-and-firefox-slip/). Old versions of Chrome and Firefox are less significant.