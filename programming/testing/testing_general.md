
https://github.com/testcontainers/testcontainers-node
* from https://www.docker.com/blog/maintainable-integration-tests-with-docker/

**Todo**
Code coverage - Blanket.js (server-side), Instanbul (integrates with karma), Coveralls.io (hosting)
Wallaby.js - look into it

TestDouble - [https://github.com/testdouble](https://github.com/testdouble) has good resources

## Fragility problem
Verifying mocks and stubs! A hardcoded mock isn't guaranteed to match the real object, motivation for automocking - but also just avoid mocks?

If there is a database, user a 'stub' or a 'mock'. or not - an integration test?
[TDD best practices from Enterprise Craftsmanship](http://enterprisecraftsmanship.com/2015/08/03/tdd-best-practices/) - awesome series


[How we test client-side Javascript: Setup and patterns](http://informatics.unep-wcmc.org/post/86302609060/how-we-test-client-side-javascript-setup-and) - 


**Docker and E2E tests**

[http://gregoryszorc.com/blog/2014/10/16/the-rabbit-hole-of-using-docker-in-automated-tests/](http://gregoryszorc.com/blog/2014/10/16/the-rabbit-hole-of-using-docker-in-automated-tests/)

**Todo**

[http://www.peterprovost.org/blog/2012/05/02/kata-the-only-way-to-learn-tdd/](http://www.peterprovost.org/blog/2012/05/02/kata-the-only-way-to-learn-tdd/)

[The Behaviour Specification Handbook](https://devzone.channeladam.com/library/bdd-behaviour-specification-handbook/) - 


**Running relevant tests**

[https://www.npmjs.com/package/test-creep](https://www.npmjs.com/package/test-creep)

[http://blogs.atlassian.com/2008/11/stop_testing_so_much/](http://blogs.atlassian.com/2008/11/stop_testing_so_much/)

**Exploration 2015**

Karma and requirejs: [Lab: A beginners guide to starting a new web app with Karma, Jasmine and RequireJS](http://stephenjamescode.blogspot.com/2014/05/lab-beginners-guide-to-starting-new-web.html) - best guide by far

Tried watching James Shore's [Let's Play TDD](https://www.youtube.com/watch?v=CdCNJYKqmi0) (ended at #4) on Youtube

**General**

[Mocks Aren't Stubs](http://martinfowler.com/articles/mocksArentStubs.html) (2007) - seems to be a classic from Martin Fowler

**General observations**

[Testing your frontend JavaScript code using mocha, chai, and sinon](https://nicolas.perriault.net/code/2013/testing-frontend-javascript-code-using-mocha-chai-and-sinon/) (2013) - probably best I found, by the guy who wrote CasperJS

[Running subsets of Mocha test suites](http://paulsalaets.com/posts/running-subsets-of-mocha-test-suites/) - karma-mocha also has a grep CLI

[Best Practices for Spies, Stubs and Mocks in Sinon.js](https://semaphoreci.com/community/tutorials/best-practices-for-spies-stubs-and-mocks-in-sinon-js) (2015) - 

**E2e**

[End-to-End JavaScript Testing is Easy (the minimal way to do it)](http://blog.strafenet.com/2014/07/03/end-to-end-javascript-testing-is-easy-the-minimal-way-to-do-it/) - best short and

simple overview

[nightwatch (plus page objects)](http://nightwatchjs.org/guide#page-objects) - major up and comer

WebdriverIO - sexy wrapper around Selenium

[Why unit testing is a waste of time](https://news.ycombinator.com/item?id=681503) - agreed that tests should clear out data

*Selenium*

For Java, you need everything in [this answer](http://stackoverflow.com/a/5134972/4200039)

Project has more energy than initially realized: see api at [http://seleniumhq.github.io/selenium/docs/api/javascript/](http://seleniumhq.github.io/selenium/docs/api/javascript/) (ignore the broken googlecode one). 

*Desktop* - Sikuli (see [review](http://teotti.com/sikulix-automated-testing-with-image-recognition/)) seems to be main one

**Casperjs and phantomjs**

[Using A Remote Debugger With CasperJS and PhantomJS](https://drupalize.me/blog/201410/using-remote-debugger-casperjs-and-phantomjs) - sweet!! also [You can see what casperJS is doing with this little hack](https://www.codementor.io/tips/4678317992/you-can-see-what-casperjs-is-doing-with-this-little-hack)

[Front-end Testing for the Lazy Developer with CasperJS](http://www.helpscout.net/blog/functional-testing-casperjs/) - remember to use .then

[Page Object pattern with CasperJS](http://jsebfranck.blogspot.fr/2014/03/page-object-pattern-with-casperjs.html) - page objects! also see [Using Page Objects to Overcome Protractor's Shortcomings](https://www.thoughtworks.com/insights/blog/using-page-objects-overcome-protractors-shortcomings)

Got caught by using var in my before & beforeEach - these were scoped to that block and not available in the it block! Duh!

For asynch, remember to execute the done() function per  [Async function in mocha before() is alway finished before it() spec?](http://stackoverflow.com/questions/24723374/async-function-in-mocha-before-is-alway-finished-before-it-spec)

If you're running mochajs in the browser, remember you have to toss in the mocha.setup('bdd') and mocha.run()! and remember your stylesheet!

[Feature proposal: global setup and teardown](https://github.com/mochajs/mocha/issues/1460) - already exists

[Mocha tests with extra options or parameters](http://stackoverflow.com/questions/16144455/mocha-tests-with-extra-options-or-parameters/16145010#16145010) - arbitrary parameters

**Resources** - read Test-Driven Development by Christian Johansen

**Karma** - first ran into issues with chai not being defined, but you have to list them in the files of your config per [Include dependencies in Karma test file for Angular app?](http://stackoverflow.com/questions/20132823/include-dependencies-in-karma-test-file-for-angular-app) (there's also the karma-chai plugin but don't use that, it seems unmaintained)

****

**Test runners**

[Can Protractor and Karma be used together?](http://stackoverflow.com/questions/17070522/can-protractor-and-karma-be-used-together) - no!

****

**Types of tests**

Supertest - HTTP assertions from Mocha author

Frisby.js - API testing framework based on Jasmine

Mutation testing - [Humbug for PHP](https://github.com/padraic/humbug) and [jimivdw/grunt-mutation-testing](https://github.com/jimivdw/grunt-mutation-testing) for JS

e2e, unit - I've got these handled OK

## Visual Test
TODO: figure this outensures a UI hasn't changed unexpectedly

## A/B testing
This is more of a data science thing?

