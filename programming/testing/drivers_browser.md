<insert history going back to earliest test automation, mention that Linux does not have automated tests, Sikuli and AutoHotKey for desktop stuff>

<insert overview of web automation testing with Selenium, OpenQA, Watir, Mink/Behat (PHP) [abstraction around drivers](http://mink.behat.org/en/latest/guides/drivers.html), and Capybara and Splinter abstractions, Zombie.js/jsdom from 2011, PhantomJS/QtWebkit, Suhu (no waits, ThoughtWorks uses it), Codeception, and curl>

Watir's instructions are to use it interactively http://watirwebdriver.com/