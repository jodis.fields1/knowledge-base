Some patterns I've noticed

## patterns
https://www.patternfly.org/v3/pattern-library/forms-and-controls/inline-edit/index.html

inline-editable:
* https://ux.stackexchange.com/questions/3465/signifying-to-user-that-field-is-editable
* https://webapphuddle.com/inline-edit-design/

login page:
* https://github.com/mazipan/login-page-css
