
Code-free startup - Tilda, Bubble, Code-Free Startup, Stencyl

keywords: no code, low code, low-code, NoCode

TODO: [Utopia, a visual design tool for React, with code as the source of truth](https://news.ycombinator.com/item?id=27516212)

[Ask HN: What are your favorite low-coding apps / tools as a developer?](https://news.ycombinator.com/item?id=22786853)

[The impact of no-code and low-code technology on small businesses](https://www.youtube.com/watch?v=1Vy1_LgaLhE)
* aison.tech

## options
[Launch HN: Optic (YC S18) – Automate Routine Programming] - https://news.ycombinator.com/item?id=17560059

https://twitter.com/framer - TODO

https://www.newco.app/ - courses?

UNTRIED:
RapidUI, Modulz per https://mobile.twitter.com/colmtuite/status/980050864500027392
[Glide 2.0](https://www.producthunt.com/posts/glide-2-0)
also weebly, wix, etc

### [webflow](https://en.wikipedia.org/wiki/Webflow)
saw https://github.com/webflow/leadership/blob/ccc59ef90a2a560a4859fc750ee07ea47853124c/tech_lead.md a while ago
cofounders includes Vlad Magdalin:
> "fan of @worrydream's Inventing on Principle talk ( https://vimeo.com/36579366 ), I have a standing offer to buy you coffee/lunch/beer/caviar/diamonds just so I can get to know you IRL"

cofounders include Bryant Chou, said in 2018:
> Prediction: 60% of SaaS company marketing sites will be on Webflow by 2020

https://webflow.com/
    * came up again at https://twitter.com/webflowapp/status/948637855684808704

### haiku
founded by Zack Brown in 2016, formerly Famo.ous
in typescript: https://github.com/HaikuTeam/core

as of 2018-07, still in developer preview, seemed a bit rough

#### haiku tutorials
https://docs.haiku.ai/using-haiku/figma-to-react.html
[Motion design for the web, iOS & Android with Haiku](https://medium.com/haiku-blog/getting-started-with-animations-for-the-web-ios-android-with-haiku-568184eb31fa)


### pagedraw
founded around 2018 by Jared Pochtar a year out of college; written in Coffeescript per https://github.com/Pagedraw/coffee-react-base

as of 2018-07, OK but ultimately underwhelming - cool stackblitz integration


## backend

plivo - visual voice etc

ubot - see ads from themhttps://m.facebook.com/story.php?story_fbid=1046375035708295&id=178604518847732
