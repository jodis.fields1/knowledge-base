TODO: https://github.com/dali-lab/figma-workshop/blob/master/README.md

https://blog.figma.com/how-kiwi-com-handles-project-structure-versioning-components-in-figma-8258e0acb963

## icons
https://github.com/4y0/figma-fontawesome-icons
https://www.figmacrush.com/font-awesome-5-free-figma-icon-set/
https://www.simonmccade.com/blog/how-developers-and-tech-founders-can-turn-their-ideas-into-ui-design

## log
* 2018-08-08: 2 hours or so messed around with https://www.figma.com/file/adCqDAJHmSMnNwLoMFKClaaN/freeform?node-id=0%3A1 on a whim motivated by [3.0 release](https://blog.figma.com/figma-3-0-217d6c248f85) which is mostly about prototyping
    * skimmed https://blog.usejournal.com/30-days-deep-into-figma-full-review-7fffbb237c27
* 2018-07: perused some docs etc

## intro email
[Figma Help Center](https://help.figma.com/): The answers to most of your questions.
[Figma Tutorial Videos](https://www.youtube.com/channel/UCQsVmhSa4X-G3lHlUtejzLA/videos): Deep dives on key features.
Figma User Forum: Chat with other Figmoids.
[Figma Resources](https://www.figma.com/resources): Sample files, videos and such.

## lessons
https://www.lynda.com/Figma-tutorials/Figma-UX-Design/711832-2.html - by Brian Wood


## vector networks
log: 2018-07
vector network designs?
[The Mighty Pen Tool](https://medium.com/@trenti/the-mighty-pen-tool-6b44ff1c32d)

### dumb newb tips
1. key is to use the pen and just click, don't hold down as that causes a bezier curve!
https://help.figma.com/drawing/vector-networks

2. edit existing object by clicking Edit Object! in menu
![](2018-07-14-09-45-50.png)

3. Put stuff into group with the Menu->Object
![](2018-08-08-20-18-19.png)


## platform
no plugins, but an API

https://blog.figma.com/introducing-figmas-platform-ee681bf861e7
