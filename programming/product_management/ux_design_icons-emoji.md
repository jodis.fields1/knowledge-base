
tl;dr - do not use emoji as they are platform-specific

use font-awesome and so on

https://storytale.io/ - illustrations

### Icons
use font-awesome etc

Ego Icons https://www.producthunt.com/posts/ego-icons-2


### emoji
https://github.com/vector-im/element-web/issues/1031
my issue was ultimately solved by https://askubuntu.com/questions/1029661/18-04-color-emoji-not-showing-up-at-all-in-chrome-only-partially-in-firefox however - needed to install the font on Ubuntu
1F601 -> 128513 (which is the UTF-32BE per https://unicode-table.com/en/1F601/)
U+274C i.e. 10060
String.fromCodePoint(10060)
https://stackoverflow.com/a/39419844/4200039 - pseudoelements required?
use parseInt from https://stackoverflow.com/questions/51660661/string-fromcodepoint-does-not-return-flag-emoji

https://medium.com/@seanmcp/%EF%B8%8F-how-to-use-emojis-in-react-d23bbf608bf7
* but why use emoji?

https://unicode-table.com/en/1F601/

https://unicode.org/emoji/charts/full-emoji-list.html#1f600
