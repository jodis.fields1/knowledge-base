* https://www.draw.io/
  * https://trello.com/c/bVyIlgF3/131-tablet-pen-support-with-automatic-recognition-of-shapes-arrows-etc
  * job seen advertised at https://news.ycombinator.com/item?id=14902706

[30 Best Online Course Websites to Learn UI/UX (Updated)](https://medium.com/@Vincentxia77/30-best-online-course-websites-to-learn-ui-ux-updated-6b104762731a)
* checked 2018, need to do someday

#### icons
https://control.rocks/?ref=producthunt

#### Vector image
Figma - my goto
Sketch - the paradigm shifter
Adobe XD - experience designer
UXPin - been around a while, no free plan

#### emoji 
https://motherboard.vice.com/en_us/article/78kzn9/what-the-emoji-youre-sending-actually-look-like-to-your-friends

##### Wireframing WYSIWYG (prototyping)
Balsamiq Mockups - often recommended, leader [per 2015 list of 20](http://www.creativebloq.com/wireframes/top-wireframing-tools-11121302)
iplotz - free trial shows that it is awesome!
Pencil Project - free open-source and worth looking into
Mockingbird - seems really cool

Use [json-server](https://github.com/typicode/json-server) to skip dealing with a server
graphicdesignforum.com seems to be the main place; also points to mediabistro.com for learning http://www.mediabistro.com/ondemandvideos.html?red=gc

PerfectPixel or pixel-perfect overlays of .psd files, both on github

##### Images
mylio is for personal photos

When linking images, you should use the Compass $assets_path helper per [Where’s Your Assets?](http://blog.grayghostvisuals.com/css/image-url/)


## screen recording / gifs -
windows - https://www.screentogif.com/ is great

[screencast-o-matic.com](http://screencast-o-matic.com) is also good
Jing / screencast.com is the best, possibly check out https://www.getcloudapp.com/blog/cloudapp-the-productivity-swiss-army-knife
used [How-to: Make animated GIFs using only free / open source software](http://oppositelock.kinja.com/how-to-make-animated-gifs-using-only-free-open-sourc-1444654871) but note that I had to adjust the fuzz by a few percent (upwards?); [OS X Screencast to animated GIF](https://gist.github.com/dergachev/4627207) (gist) is the best source; also Licecap, Giphy Capture (from giphy.com), and screencast.com

https://github.com/vvo/gifify - make gifs from mp4s

https://github.com/jmathai/elodie is interesting - also EXIF editors

#### Themes

[http://bootswatch.com/](http://bootswatch.com/)

##### Image and video editing
https://photoeditor.polarr.co/ - cool, started off as a Show HN project https://news.ycombinator.com/item?id=14839459

Lightgallery - the only electron app it seems? 
nomacs is good for basic video viewing, needs homebrew package (see https://github.com/nomacs/nomacs/issues/133)

Pixelr - web, free, OK

###### darktable
2018-10: motivated by simple desire to shrink a JPEG file, read up and watched 1.0 demo video from 2012; noticed https://www.darktable.org/usermanual/en/export_selected.html: "For some export formats like JPEG you can define an output quality ... If the file format supports embedded metadata, like JPEG, JPEG2000 and TIFF, darktable will try to store the history stack as XMP tags within the output file" which is cool; also read discussion about non-compatible way of handling .xmp (with extension versus not) at https://discuss.pixls.us/t/linux-applications-and-their-non-standard-xmp-sidecar-naming-convention/2688/26

###### GIMP
GIMP - better than I expected! First gotcha: [reopen toolbox](http://superuser.com/questions/645532/i-accidentally-closed-my-gimp-toolbox-can-i-get-it-back)\

latest dev beta version not packaged for macOS
* see gimp-latest.rb in my homebrew-tap

dev version: 
filtering:
* blurring some text 2018-03: http://smallbusiness.chron.com/make-things-blurry-using-gimp-77113.html

####### GIMP plugins
https://en.wikibooks.org/wiki/GIMP/Installing_Plugins
2018-03: not much update on plugin manager (https://bugzilla.gnome.org/show_bug.cgi?id=767277), funded Jehan on Liberapay, also see https://www.patreon.com/pippin

https://build.gimp.org/ - Jenkins CI

###### Tricks

Resizing - use Fractal Interpolation to increase size without pixelation [per this](http://www.wpbeginner.com/beginners-guide/how-to-resize-and-make-images-larger-without-losing-quality/)! perfect resize w/ Photoshop

###### Commercial

Pixelmator - lighweight

*Free open-source*
ImageMagick - automatically convert images
image vectorizer - [http://www.autotracer.org/](http://www.autotracer.org/)
CamanJS - image editor in Javascript but its been 2 years since last commit
Photoshop - their PSDs are the standard
MyPaint - great for tablet drawing, came up with OpenRaster as PSD alternative
Pinta - written in C# on Mono, seems to be the best
Shotwell - super simple one written in Vala (like CoffeeScript for C; related to another version Genie)
MyPaint - emerges as a leader on alternativeto but it is really a painting product, works with a lot of tablets
Krita - billed as a Photoshop killer
PAINT.NET
http://advanced.aviary.com/tools
FSViewer - no editing, but good viewer
XnView - great for TIFF files and portable; was a bit tricky to figure out shortcuts with SHIFT+Pgdwn/up for changing pages
Favicon - [favicon-cheat-sheet](https://github.com/audreyr/favicon-cheat-sheet)

[How to have multiple favicon sizes, yet serve only a 16x16 by default?](http://stackoverflow.com/questions/9150691/how-to-have-multiple-favicon-sizes-yet-serve-only-a-16x16-by-default) - good question!

[How do I force a favicon refresh](http://stackoverflow.com/questions/2208933/how-do-i-force-a-favicon-refresh) - toss a querystring in to force a refresh
