


[Portfolio management](https://gitlab.com/gitlab-org/gitlab-ee/issues/2329)

http://www.liberatingstructures.com/ - Liberating Structures

https://www.mindtools.com/pages/article/newHTE_95.htm
* Quick Wins (High Impact, Low Effort)
* Major Projects (High Impact, High Effort)

### Scaled Agile SAFe
Apparently it considers unseen costs and technical debt? that's good...

https://web.archive.org/web/20200915050154/https://www.atlassian.com/dam/jcr:93265840-405e-4ea7-831c-b65eddc1d242/Scaling%20agile%20with%20Atlassian%20and%20SAFe%20white%20paper.pdf

### scrum
* [Myth 11: In Scrum, we spend too much time in meetings](https://web.archive.org/web/20180531021542/http://www.barryovereem.com/myth-11-in-scrum-we-spend-too-much-time-in-meetings/)
* [A Summary of the 10 Scrum Myths](https://web.archive.org/web/20180531021303/http://www.barryovereem.com/a-summary-of-the-10-scrum-myths/)


### estimation
[How to choose the best methods of estimation for planning](https://www.atlassian.com/blog/add-ons/choose-best-methods-estimation-planning)

http://www.jamesshore.com/Agile-Book/

Estimation - https://www.mountaingoatsoftware.com/books/agile-estimating-and-planning

Data: http://agiledata.org/ - Scott W. Ambler has some decent thoughts, reviewed in https://martinfowler.com/books/refactoringDatabases.html

### Kanban etc
https://www.atlassian.com/blog/jira-software/7-steps-to-a-beautiful-and-useful-agile-dashboard
* 2021: Tara.ai
