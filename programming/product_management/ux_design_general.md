Never use black: https://ianstormtaylor.com/design-tip-never-use-black/

[NoDesign.dev – Tools and resources for non-artistic developers (nodesign.dev)](https://news.ycombinator.com/item?id=23454557)

https://www.happyhues.co/palettes/17
* https://news.ycombinator.com/item?id=21780659

[How to become a UX Designer at 40 with no digital or design experience!](https://uxplanet.org/how-to-become-a-ux-designer-at-40-with-no-previous-digital-or-design-experience-5c96af46b73c)

[Nick Babich](https://uxplanet.org/@101)

## afewitems thoughts
InVision, Photoshop/GIMP, Sketch, or something... also think about http://www.lukew.com/

figure out my preferred layout... lostgrid, ant, maybe https://mdo.github.io/table-grid/?

maybe also animations with https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Animations/Using_CSS_animations?

### Tools
Figma seems best, comparison at https://www.slant.co/topics/13/~best-mockup-and-wireframing-tools-for-websites
[7 UX/Usability tools to try in 2017](https://uxplanet.org/7-ux-usability-tools-to-try-in-2017-4d8b94714bf6)
Sketch - the real leader, integrations such as https://github.com/airbnb/react-sketchapp
also see https://balsamiq.com/
https://github.com/evolus/pencil is open-source but crappy
Inkscape - is it used? https://ux.stackexchange.com/q/8467/73829
