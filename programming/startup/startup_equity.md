Startup equity
===

https://mobile.twitter.com/holman/status/1161047132696006656

" two basic types of stock options are non-qualified stock options (NQSOs) and incentive stock options (ISOs)" per https://smartasset.com/taxes/stock-options-tax - best to get ISOs

## Equity - Management
[New Standard Deal](https://news.ycombinator.com/item?id=18095224)

[PROFIT SHARING FOR BOOTSTRAPPED STARTUPS](http://nathanbarry.com/profit-sharing/)

[The Right Way to Grant Equity to Your Employees](http://firstround.com/review/The-Right-Way-to-Grant-Equity-to-Your-Employees/)

## Equity - Employee
* TODO: https://github.com/jlevy/og-equity-compensation
* TODO: [Holloway Guide to Equity Compensation (holloway.com)](https://news.ycombinator.com/item?id=17717727)

http://better-equity.recursion.org/
https://80000hours.org/2015/10/startup-salaries-and-equity-compensation/ - 

[A Guide to Employee Equity](https://news.ycombinator.com/item?id=12219510) - 

### extended exercise
[extended-exercise-windows](https://github.com/holman/extended-exercise-windows) - 

[Former employee sues startup for financials in order to value granted stock](https://news.ycombinator.com/item?id=12320377) - 

### Equity tools
* [TLDR Stock options*](https://tldroptions.io/) - from long-term stock exchange (Eric Ries) folks
* [{SALARY OR EQUITY}](http://salaryorequity.com/)
* [Compensation and Equity Calculator](https://comp.data.frontapp.com/) - code at https://github.com/frontapp/compensation-calculator