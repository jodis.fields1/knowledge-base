## startup ideas quick
See dev_ideas_quick.md

[Tell HN: How to run a startup for $6 a year](https://news.ycombinator.com/item?id=22354060)

From California politics, put https://www.couragescore.org/hall-of-shame/ on a map

Some of these could just improve my personal brand

* write book on Wikipedia editing for gumroad
  * recommended by https://twitter.com/dvassallo/status/1254147759118024704
* create a website called donotoverpay
* website app to build personal profile from public links
* ibdhope.com - dump of IBD data
* hospital scorecard showing basic transparency facts (IT policy, patient advisory council)
* flash cards tying faces to names
* interactive map augmented reality
* bar app designed to help people interact - drinking games on the fly
* Planning poker estimation
* Sell lifehacking service
  * lots of ppl like this on twitter, e.g. 
    * https://mobile.twitter.com/mooremomentum1
    * Cameron Fous
* create an RIA, manage money by putting it in blue chips
* crowdsourced analyst representing shareholders
* create a database listing all companies mentioned in asshole design
* improve personal brand by writing articles https://authorityalchemy.com/become-a-contributor-for-forbes/
* create content on https://mobile.twitter.com/teachable - found via https://mobile.twitter.com/ankurnagpal/status/1229064666690867200

## Promotion / funding / help
[Anyone looking to launch on @ProductHunt in January? ](https://twitter.com/kevinwdavid/status/1216578002756108289)

https://mobile.twitter.com/awilkinson - Tiny Capital

1517

that happyhues guy, Mackenzie Child
* https://mobile.twitter.com/mackenziechild/status/1205529150276227072

Weekend.fund
* https://medium.com/@rrhoover/turn-your-side-project-into-a-company-introducing-weekend-build-c70aebc6d716

* https://jumpcut.com/
  * "like business school, but better"
* https://i2f.launchpeer.com/


### Streaming
Tutorial:
* thoughtcafe

Videos:
* do a video reflecting on past annual Wikipedia contributions 
* create an RIA and add youtube videos showing how to an evaluate brokerage account
* stream about economics on youtube
* add video to my knowledgebase

Tools:
* https://www.youtube.com/watch?v=jrwiSRcn9fU | Best Budget Live Streaming Camera of 2020! - YouTube
* https://www.reddit.com/r/WendoverProductions/comments/8m5kfs/where_does_he_get_all_the_videos_in_his_youtube/
* https://www.reddit.com/r/WendoverProductions/comments/biw6ul/video_production_question/
* https://vimeo.com/create
* https://twitter.com/Promodotcom/status/1237306032747163649