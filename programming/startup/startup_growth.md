Startup growth
===

https://segment.com/blog/the-growth-stacks-of-2019/

"One frequently overlooked incentive for emerging startups in need of cloud infrastructure is the Research and Development (R&D) Tax Credit" per https://www.leyton.com/en/usa/home &  https://www.youtube.com/watch?v=8rMRlKtfbL4

## potential fundraising / liftoff
https://jumpcut.com/ - LIKE BUSINESS SCHOOL, ONLY 100,000X BETTER

https://twitter.com/merci - "DMs are open. Happy to help"

https://www.facebook.com/thejakehare - "I have an idea for an app. Now what?"

## general advice
[I made a database of TL;DR business strategies/case studies](https://news.ycombinator.com/item?id=24957244)

[YC Startup School 2018: a free, 10-week, online course](https://news.ycombinator.com/item?id=17558452)

[Founder to CEO: How to build a great company from the ground up](https://docs.google.com/document/d/1ZJZbv4J6FZ8Dnb0JuMhJxTnwl-dwqx5xl0s65DE3wO8/preview#heading=h.pdmqf3646hgt) By Matt Mochary

Ship by ProductHunt: https://www.producthunt.com/ship/aws
* comes with 7500 in AWS credits

[Share your startup - April 2018](https://www.reddit.com/r/startups/comments/88q3h8/share_your_startup_april_2018/)
    * lots of cool stuff!

https://www.slideshare.net/mobile/dmc500hats/startup-metrics-for-pirates-long-version

## management
https://github.com/webflow/leadership/blob/master/tech_lead.md

## sales
BANT - Budget, Authority, Needs, and TimelineAll Things Sales: Mini-lessons for startup founders

[All Things Sales: Mini-lessons for startup founders](https://news.ycombinator.com/item?id=17899510)

## product-market fit
https://a16z.com/2017/02/18/12-things-about-product-market-fit/

## business models
[Show HN: Tinder Revenue Estimate](https://news.ycombinator.com/item?id=16545152)

### based upon source
https://plan.io/task-management/ - based upon redmine

## founders
[1517 community](https://www.facebook.com/groups/1750280855282658/permalink/2369992499978154/)

https://www.f6s.com/ - social network
cofounderslab.com - I hid my profile 2018-02

## growth hacking
[Lessons from Startups: Leveling Up the Waitlist at Zero Financial](https://finovate.com/friday-fun-leveling-waitlist/)
    * tweet to move up in the line

[Show HN: Checklist of over 100 directories to submit your startup](https://news.ycombinator.com/item?id=17591677)

https://hackernoon.com/what-most-dont-see-in-tbh-an-app-sold-for-100m-and-launched-only-9-weeks-ago-f15edd11505f

https://www.julian.com/learn/growth/intro

[THE BAMF BIBLE](https://www.producthunt.com/posts/the-bamf-bible)

## customer intelligence
Qualtrics

https://sparktoro.com/

## affiliate marketing
https://www.billysadswork.com/opt-in-for-gene-pool-keys-cic-1-0-free19109225?gclid=CjwKCAjw-bLVBRBMEiwAmKSB87J3tagsxHSfmIYhiPAv7xzL4EbFD3dygHu92OIbGWgkgqQA-Cp6oBoCH-IQAvD_BwE
    * infomercial
    
https://www.fastcompany.com/3065928/sleepopolis-casper-bloggers-lawsuits-underside-of-the-mattress-wars?partner=inc&cid=sf01002&sr_share=facebook

### targets of a business
Primary entities: people, things, experiences, knowledge

#### fintech
https://finovate.com/a-look-at-the-top-50-fintech-companies-in-europe

regtech per http://finovate.com/whats-hot-whats-not-u-s-fintech/

### Code free startup
Moved to ux_design_codefree.md


### Promotion places
[Places to promote your software company](https://news.ycombinator.com/item?id=14921695)

### Advice / conference
https://thehustle.co - runs HustleCon for nontechnical founders

### Landing page
https://github.com/dennybritz/neal-react - came up near the top
[Runway – Cash planning tool for startups](https://news.ycombinator.com/item?id=14671523)

**Advice**
Brian Clark (see LinkedIn, bclark8923@gmail.com) gave an amazing talk with comments such as:
medium how to build killer products evolution of landing
email with spelling errors which seemw genuine ( iPad)
snipply - snippets of links

**Apps**
Text Easy to text people?

**Google SEO (including Panda)**

Panda update got a lot of people bothered http://www.seomoz.org/blog/duplicate-content-in-a-post-panda-world

http://theaveragegenius.net/google-reward-quality-original-content-interview-askthebuilder-tim-carter/ advertisement on SEO criticizing Google