Venture capital and startups
===

https://venturehacks.com/ - recommended by https://twitter.com/vcstarterkit

https://www.producthunt.com/posts/hyper-founder-program

## new
Initialized - https://twitter.com/Initialized/status/1047241168164024321

https://en.wikipedia.org/wiki/Founders_Fund

https://twitter.com/cshapiro/status/1317212694529802242
* Ryan Caldbeck complained about Craig Shapiro https://twitter.com/ryan_caldbeck/status/1316730276014247937

people around bay area:
* https://twitter.com/salil

### healthcare
see:
* medical_profession_industry/health_system_general.md - Deerfield Management
* ibd_research_funding.md - ex: https://en.wikipedia.org/wiki/Esther_Dyson

## old

[How to Value a SaaS Business](https://news.ycombinator.com/item?id=11183109) - 

## Startups
[Fuck Your 90 Day Exercise Window](http://zachholman.com/posts/fuck-your-90-day-exercise-window/)

## Digital marketing
Cost per click  
Divide by form completion rate for total cost per customer

Then look at form conversion rate and total lifetime value

[Product Hunt](http://www.producthunt.com/ "http://www.producthunt.com/") - cool website

The Brutal Ageism of Tech: Years of experience, plenty of talent, completely obsolete (2014) - interesting article

https://www.cbinsights.com/blog/startup-failure-post-mortem/

http://www.bloomberg.com/news/articles/2015-09-28/liam-casey-s-pch-international-helps-startups-crack-china
