https://github.com/joyzheng/fyi - nice little app https://joy.fyi/books

[Show HN: A Firefox extension to leave comments on any URL ](https://news.ycombinator.com/item?id=20292638)
* DescribeThis idea

Health resume - https://amp.insider.com/woman-created-health-resume-and-doctors-love-the-idea-2019-6

IBD - track symptoms, automatically display various indexes from automatically pulled in data
* partner with Vivante Health
* ibdhope

See taskwarrior - but that's mostly quick tasks on existing projects.

2. parse these markdown files
    1. see _meta top folder
    2. count and load all links into a database
3. write articles / blog posts - have a unique perspective
    1. jenkins? nix? react-redux?
    2. charge for them?
4. Create stripped down openssh lite
    1. OK, not exactly quick...
5. sell playlists of good music
   1. focusatwill alternative e.g. https://www.reddit.com/r/productivity/comments/807n62/viable_alternative_to_focusatwill_music_to_focus/