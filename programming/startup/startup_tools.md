## meta finding
* [g2crowd](https://www.g2crowd.com)
  * need grid feature
* capterra

## logo
https://hipsterlogogenerator.com/

https://www.brandcrowd.com/maker/logos

https://www.vectorstock.com/royalty-free-vector/tree-money-logo-vector-29948419

## freebies
https://aws.amazon.com/blogs/aws/aws-has-launched-the-activate-founders-package-for-startups-%F0%9F%9A%80/

## payment
https://blog.risingstack.com/stripe-payments-integration-tutorial-javascript/

## funding
* Glassdollar is a tool that helps you find VCs based on your startup
* Investor Hunt is an AI-powered database of over 40K investors
* Angel Database is a huge database of over 13K angel investors

## logistics
https://en.wikipedia.org/wiki/Third-party_logistics (3PL)

https://www.cnbc.com/2018/11/09/amazon-offering-50-percent-cheaper-shipping-than-ups-to-some-sellers.html

## business metrics
https://nathanbarry.com/metrics/

## productivity
see my productivity notes...
http://www.startupdaybook.com/

## hiring
Fiverr – A marketplace to offer or procure digital freelance services. Great place to start making freelance income.
Algorithmia - 
ValuesCoders - wrote this interesting post How Much Does an App Cost: A Massive Review of Pricing and other Budget Considerations

## TOOLS/SOFTWARE (copied from somewhere)
Sumome – A suite of free tools that can be used to grow your website’s email list and traffic. The SumoMe tools are easy to install and work on any website.
Google Search Console – Helps you to diagnose and optimize your Google search visibility.
Google Tag Manager – A tag management system that allows you to quickly and easily update tags and code snippets on your website or mobile app.
Google Trends – Explore Google trending search topics to gage interest and demand.
Wave Accounting – Free software for sending invoices and managing accounting and bookkeeping.
SurveyMonkey – Create and publish online surveys in minutes, and view results graphically and in real time.
Mailchimp – Freemium online email marketing solution to manage subscribers, send emails, and track results. Some alternatives include AWeber, Constant Contact and GetResponse.
    * Mailtrain open-source alternative https://news.ycombinator.com/item?id=16862545 
Shopify – The best ecommerce platform for selling online with little customization.
Built With – Find out what platform any website is built with.
Hootesuite – Social media management software that helps you manage multiple networks and profiles.
BuzzSumo – Analyze what content performs best for any topic or competitor.
Canva – Create designs for Web or print: blog graphics, presentations, Facebook covers, flyers, posters, invitations, etc.
Open Site Explorer – Use this SEO tool to research backlinks, identify top pages, identify link building opportunities and analyze anchor text.
Ahrefs – A toolset for SEO and marketing. Includes tools for backlink research, organic traffic research, keyword research, content marketing & more.

TOOLS/SOFTWARE – AMAZON FBA SPECIFIC
Jungle Scout Pro – Jungle Scout shows you the exact products that can make you money on Amazon. Check sales estimates of competitors, find opportune niches, etc.
CamelCamelCamel – Amazon price tracker, Amazon price history charts, price watches, and price drop alerts.
Cash Cow Pro – Tool suite that includes most of the features you need to run & grow your Amazon business, from automatic messaging for feedback to keyword tracking. Only $29.97/mo.
ECOMMERCE WEBSITES/BLOGS

A Better Lemonade Stand – A Better Lemonade Stand is an online ecommerce incubator, supporting early stage ecommerce entrepreneurs. Learn how to start an online business.
Shopify Blog – A blog about ecommerce marketing, running an online business and updates to Shopify’s ecommerce community.
The Amazing Seller – The ultimate beginner’s blog/podcast for an Amazon FBA business.
SkillShare – An online education platform with classes taught by the world’s best practitioners. Learn the skills you need to make money online.
Neil Patel – All about digital marketing. Patel runs this digital “Marketing School” by offering short, actionable lessons in blog and podcast format. His core focus is SEO.
BUSINESS BOOKS

Six Months to Six Figures
Fail Fast or Win Big
The 4-Hour Work Week
BROWSER EXTENSIONS

Google Tag Assistant -Tag Assistant helps to troubleshoot installation of various Google tags including Google Analytics, Google Tag Manager and more.
ColorZilla – Advanced eyedropper, color picker, gradient generator and other colorful goodies.
WhatFont – The easiest way to identify fonts on web pages.
Lastpass – LastPass, an award-winning password manager, saves your passwords and gives you secure access from every computer and mobile device.
Checker Plus for Gmail – Get desktop notifications, read, listen or delete emails without opening Gmail or Inbox by Gmail & easily manage multiple accounts.
Block Yourself From Analytics – Block your Google Analytics™ activity for the websites you own, so no more false stats.
Word Count Tool – Word Count Tool counts the number of words and characters in your selected text.
Fair AdBlocker by STANDS – Block unwanted ads, popups, tracking and malware (even on Facebook & Youtube), speed up your browsing, and protect your privacy.
QPush – Push text and links from computer to iPhone.
Send Anywhere – Send your files easily, quickly and to virtually anywhere.
Jungle Scout Pro – Amazon and FBA product research made easy. Extract rank, sales volume, estimated revenue and more without entering the product page.
Domain Hunter Plus – Quickly scan the current web page and check for dead links (link building opportunities).
ECOMMERCE/BUSINESS PODCASTS

The Amazing Seller – The ultimate beginner’s podcast for an Amazon FBA business.
ProBlogger – Fantastic podcast for anyone who wants to make money blogging.
Shopify Masters – Provides the knowledge and inspiration you need to build, launch and grow a profitable online store.
The Time Ferriss Show – #1 business podcast on all of iTunes. Tim deconstructs world-class performers from eclectic areas (investing, sports, business, art, etc.) to extract the tactics, tools, and routines you can use.

