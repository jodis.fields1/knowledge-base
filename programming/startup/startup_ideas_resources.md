See Zenkit

http://blog.ycombinator.com/13-startup-ideas/

[Ask HN: I will pay you $2000 and code the MVP for your side-project](https://news.ycombinator.com/item?id=19536156)

APIs:

https://github.com/APIs-guru/openapi-directory

## innovative people
https://www.cbinsights.com/research/report/innovation-frameworks-navigate-disruption

See people_codestars.md

Bret Victor - duh
[Barry O'Reilly](https://barryoreilly.com/)

Someday / maybe:

## Scaled Knowledge Framework - sits above a lot of other ideas
Motivations: 
GTD reference system
use tags at the top of files to determine whether to put my markdown file online
* Itamar's how I lost a pile of knowledge
* tynan - "I always think I'll remember good idea..."

### DescribeThis
Describe anything - Wikidata, then tie into your own knowledge
    * Integrate MusicBrainz metadata about music into DescribeThis

## recipe company
automated food creation company wrapping Instant Pot
start with a prepackaged instant pot recipe company

## aggregate events from multiple calendars
https://www.doing.io/
discussion at http://www.hughmalkin.com/blogwriter/2015/9/23/why-no-one-has-solved-event-discovery
Workaround right now: use red for a mandatory and lightish orange for tentative
events app for aggregating facebook, meetup.com, and personal events
Doing looks cool: https://www.quora.com/Is-there-a-startup-working-on-an-event-aggregator-site/answer/Anastasia-Proehl
TimeTree looks promising: https://timetreeapp.com/?locale=en


## Personal risk management web app
See Find Insurance Faster

## research papers app
integrate with https://acawiki.org/Home
integrate with bibsonomy
integrate with zotero
researchgate competitor
research.describethis.com?

## blockchain
crypto used solely for transparency? match dollars?

## decentralized facebook clone
See MonicaHQ
companion to trakt.tv for friends Contacts app
https://mastodon.social/about seems better than diaspora
motivated a bit by Is social graph portability workable? 
 https://news.ycombinator.com/item?id=14696619

## legal
look at https://free.law/recap/

## evaluate app
evaluate app - explain whether you did the thing (job application, movie, book, investment) or whether you stopped and why