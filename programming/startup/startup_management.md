See also management_law_practice.md

TODO: find article which talks about how corporate cultural values should include trade-offs - "move fast and break things"
* https://hbr.org/2018/02/ban-these-5-words-from-your-corporate-values-statement

Also see jobsearch notes in general - need to consolidate more in there

[Ten Management Secrets You Learn At McKinsey &amp; Co.](https://www.forbes.com/sites/brettarends/2014/06/02/ten-things-i-learned-at-mckinsey-co/#377273435c7f)

Radical Candor - ruinous empathy? learned from Silicon Valley tv show https://www.radicalcandor.com/blog/stop-ruinous-empathy/


## Public roadmap - see pocketsmith and hotjar 
https://www.hotjar.com/blog/2015/11/24/mastering-the-art-of-prioritization-how-hotjar-decides-what-to-work-on-next/

## firing
[Firing someone](https://www.fifteenlinesoffame.com/2018/03/16/displacing-someone/) - emotional and psychological perspective from Ben Patterson (seen via ghprb)