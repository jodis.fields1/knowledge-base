# Startup - mentors

https://twitter.com/SaaSWiz
* https://www.mailerrize.com/
* https://scrapegram.io/
* https://twitter.com/SaaSWiz/status/1400531567399874562
  * 5 types: white label, PaaS (stripe, gumroad), baby PaaS, NoCode, regular
* https://twitter.com/SaaSWiz/status/1394840812404543488
  * put open-source tools online
  * https://twitter.com/SaaSWiz/status/1394795509311803392
    * find small biz using GoDaddy and help them

https://twitter.com/OneJKMolina/status/1422951240674299909
* goot advice, partnered w/ Ed Latimore

## Jeff Bezos
[Amazon CEO Jeff Bezos on The David Rubenstein Show](https://www.youtube.com/watch?v=f3NBQcAqyu4)
* "regret minimization"
* most big decisions are made from intuition
* as top mgmt, make small number of high-quality decisions
