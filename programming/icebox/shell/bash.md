Also see .bashrc
Also see anki!!
see code/open-source/bash-group

https://devhints.io/bash

https://github.com/dylanaraps/pure-bash-bible

## frequent
[http://mywiki.wooledge.org/BashGuide/TestsAndConditionals](http://mywiki.wooledge.org/BashGuide/TestsAndConditionals) - refer to this comprehensive list

## bash replacement / atlernatives
[Ask HN: What to use instead of Bash / Sh for scripting?](https://news.ycombinator.com/item?id=26761195)

[JavaScript for Shell Scripting (github.com/google)](https://news.ycombinator.com/item?id=27072515&utm_term=comment)

See [https://amoffat.github.io/sh/](https://amoffat.github.io/sh/)

https://www.reddit.com/r/oilshell - modern bash??
https://github.com/tdenniston/bish | tdenniston/bish: Bish is a language that compiles to Bash. It's designed to give shell scripting a more comfortable and modern feel.
https://github.com/ergonomica/ergonomica | ergonomica/ergonomica: 🖥️ A cross-platform modern shell.


## Tools
Use BashSupport:
https://www.plugin-dev.com/project/bashsupport/

**Bash commands**

2015-05: careful using echo statements to debug as these apparently can interfere with the program; [various tools in UNIX SE here](http://unix.stackexchange.com/questions/155551/how-to-debug-a-bash-script)

*Tutorials*

[Debugging a script](http://wiki.bash-hackers.org/scripting/debuggingtips) - obviously essential
[How to debug a bash script?](https://stackoverflow.com/questions/951336/how-to-debug-a-bash-script)

## TODO
use xonsh or plumbum instead of bash
If you plan to write a shell script, consider [http://stackoverflow.com/questions/209470/can-i-use-python-as-a-bash-replacement](http://stackoverflow.com/questions/209470/can-i-use-python-as-a-bash-replacement) instead. or ruby with [adamwiggins/rush](https://github.com/adamwiggins/rush) but it isn't maintained

## random
should [rig bash to be case-insensitive](http://unix.stackexchange.com/questions/60162/how-to-make-cd-arguments-case-insensitive) if I use this 

**Defensive bash** - [Writing Robust Bash Shell Scripts](http://www.davidpashley.com/articles/writing-robust-shell-scripts/) see set -u and set -e

interesting features: [https://github.com/guns/haus/blob/master/etc/bashrc](https://github.com/guns/haus/blob/master/etc/bashrc)

**Resources**

[http://m.odul.us/blog/2015/8/12/stronger-shell]()

[http://explainshell.com/](http://explainshell.com/) - similar to manpages

Cheatsheet: [http://www.bigsmoke.us/readline/shortcuts](http://www.bigsmoke.us/readline/shortcuts)

[5 command line tools you should be using](http://zeroturnaround.com/rebellabs/5-command-line-tools-you-should-be-using/) - led me down into learning about sponge and getting back into man pages,

**Looping** 

also split by delimiter with [http://stackoverflow.com/a/918898/4200039](http://stackoverflow.com/a/918898/4200039)

\- from a random day in freenode #bash

geirha    for f in ./*.bak; do [[ -e $f ]] || continue; printf '%s\n' "$f"; done

00:24    izabera    !nullglob

00:24    greybot    Causes unmatched globs to expand to nothing rather than themselves. shopt -s nullglob; files=(*); echo "There are ${#files[@]} files." - see: http://mywiki.wooledge.org/NullGlob

**Linting** - debian's devscripts contains shellcheck, bashate, checkbashisms, etc per [this](https://news.ycombinator.com/item?id=11190623)/
