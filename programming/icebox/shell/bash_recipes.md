
See anki!!

### 2018
[Force “bad” (not 0) return code of terminal command?](https://askubuntu.com/questions/607614/force-bad-not-0-return-code-of-terminal-command)

### 2017

#### docker volume and cut

also if you get a volume which is still used, filter to figure out who is using it: for example docker ps -a --filter volume=1e0f9bb02918dd088360d9ebb001c7415012e6a52815c80180b86a796f9c981d

given output from docker volume ls like this:
<driver>	<name>

how to operate on it?
docker volume ls | xargs echo?? - but how to remove

one solution: cut - e.g. cut -d : -f 2 <<< "stdin:whatever"
but probably have to convert to whitespace first: https://unix.stackexchange.com/a/169309/108198

another per https://stackoverflow.com/a/13930396/4200039:
while read ln; do echo "processing $ln"; done

NOTE: does not work with output that has spaces
finally: echo "local               whatever_sqlsrvdata" | tr -s " " | while read ln; do echo $(cut -d ' ' -f 2 <<< ${ln}); done
docker volume ls | grep -v -E 'brightidea|main' | while read ln; do echo $(tr -s ' ' <<< "$ln") | cut -d ' ' -f2 | while read col; do echo "${col}"; done; done;
docker volume ls | ggrep -v -E 'main|brightidea' | tr -s " " | while read ln; do docker volume rm $(cut -d ' ' -f 2 <<< ${ln}); done


### 2016
Topics to cover: 

1. Communication. Piping, saving to variables, redirecting to stdin.
2. [How to write a bash script that takes optional input arguments?](http://stackoverflow.com/questions/9332802/how-to-write-a-bash-script-that-takes-optional-input-arguments) - easier said than done
3. Use printf and not echo if you're debugging? `echo "-e"` requires an escape: `echo "\-e"` works?
4. Keyboard shortcuts - see my bash_profile and also readline ([Redo typing in OSX bash](http://superuser.com/questions/554908/redo-typing-in-osx-bash)) 

Gotchas: see [Rich’s sh (POSIX shell) tricks](http://www.etalabs.net/sh_tricks.html).

###### startup
[Startup Sequences of Shells](https://news.ycombinator.com/item?id=14972558)

###### Copying and moving files
Surprisingly, `cp -r some_folder/ other_folder` will copy all child files into other_folder, exactly like `cp -r some_folder/* other_folder`, while `cp -r some_folder other_folder` will copy `some_folder` into `other_folder`. Which is surprising because I thought `some_folder/` (with a slash at the end) was just a "literal" folder.

If I'm inside a directory, you might think that `cp -r . /other/dir/` would copy my current directory into `/other/dir`. You would be wrong: it selects all the files in the current directory (apparently that's what the dot glob does?). Use `cp -r ../. /other/dir/` to select the current directory and copy it into `/other/dir/`.



**Man builtin**<br/>
	anonymouscoder1:	what does this mean: "Note that, in the case of csh(1) builtin commands, the command is executed in a subshell if it occurs as any component of a pipeline except the last"
16:39	izabera	it means that you can pipe data to read in csh
16:39	izabera	or whatever the equivalent of read is
16:40	anonymouscoder1:	izabera could you explain that a little more?
16:41	izabera	# echo x | read var; echo "var is: <$var> because in bash both sides run in their own subshell"
16:41	shbot	izabera: var is: <> because in bash both sides run in their own subshell
16:41	izabera	k# echo x | read var; echo "var is: <$var> because in ksh and csh the last side runs in parent shell"
16:41	shbot	izabera: var is: <x> because in ksh and csh the last side runs in parent shell
16:41	anonymouscoder1:	sweet
16:42	Riviera	phy1729: mh, checked the manpages of these three (tar, cpio and pax) on openbsd and didn't find a solution.
16:42	Riviera	phy1729: i might have missed something of course.
16:50	anonymouscoder1:	izabera: so it seems csh is a bit more powerful than bash in that case?
16:51	izabera	bash has that too
16:51	izabera	it's not enabled by default
16:52	izabera	# set -o lastpipe; echo x | read var; echo "var is: <$var>"
16:52	shbot	izabera: bash: set: lastpipe: invalid option name
16:52	shbot	izabera: var is: <>
16:52	izabera	# shopt -s lastpipe; echo x | read var; echo "var is: <$var>"
16:52	shbot	izabera: var is: <>
16:52	izabera	uhm...
16:53	izabera	i don't understand o_o
16:53	izabera	# bash -c 'shopt -s lastpipe; echo x | read var; echo "var is: <$var>"'
16:53	shbot	izabera: var is: <x>
16:53	anonymouscoder1:	ok
16:54	izabera	ah yes, job control
16:54	izabera	# shopt -s lastpipe; set +m; echo x | read var; echo "var is: <$var>"
16:55	shbot	izabera: var is: <x>
16:56	anonymouscoder1:	izabera so when trying to write a portable bash script, I can set options inside those scripts and not affect my portability