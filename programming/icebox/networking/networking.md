Networking
===

See also linux_lore/linux_networking

## overview
https://jvns.ca/blog/2018/03/05/things-ive-learned-networking/

https://unix.stackexchange.com/questions/23383/show-gateway-ip-address-when-performing-ifconfig-command

https://www.slant.co/improve/topics/2330/~resources-to-learn-networking

## tools
https://unix.stackexchange.com/questions/36714/what-can-i-use-to-monitor-and-log-incoming-outgoing-traffic-to-from-remote-hosts

### netcat
multiple versions of this - GNU and BSD?
* http://www.unixfu.ch/use-netcat-instead-of-telnet/
* https://jameshfisher.com/2018/12/31/how-to-make-a-webserver-with-netcat-nc.html

[An introduction to Networking with Netcat and NodeJS](https://coolaj86.com/articles/intro-to-networking-with-netcat-and-nodejs.html) -  

## Basics
* [If two computers in the same local area network try to connect to each other, do they have to go through a router?](https://www.quora.com/If-two-computers-in-the-same-local-area-network-try-to-connect-to-each-other-do-they-have-to-go-through-a-router)
* https://en.wikipedia.org/wiki/Default_gateway - great example

[PHP execution model vs Python web](https://blog.xoxzo.com/2012/05/02/php-execution-model-vs-python-web/) - also see the Ian Bicking article too

## Tools
See dev_tools.md

Hogwatch - uses [nethogs](https://news.ycombinator.com/item?id=11601172), tried it out

[TCPTuner: Congestion Control Your Way](https://news.ycombinator.com/item?id=11659808)

[Is it just me or is networking really hard?](http://gafferongames.com/2015/09/12/is-it-just-me-or-is-networking-really-hard/) - 

[Once Again on TCP vs UDP](http://ithare.com/once-again-on-tcp-vs-udp/) - UDP needs to be self-healing!

ip - replacing ifconfig, good overview at https://www.cyberciti.biz/faq/linux-ip-command-examples-usage-syntax/
lsof - perhaps best
iftop - top for networks
Traceroute -
Dig->host to IP; opposite is host command (host->IP) - part of the ISC BIND distribution, last updated 2000; moved to Github under the Bundy project name; on Linux systems dig is usually part of a common package: bind-tools (Gentoo), bind-utils (Red Hat, Fedora), or dnsutils (Debian); in Arch there may be migration to "drill" [per this](https://www.archlinux.org/todo/dnsutils-to-ldns-migration/). See [this](https://www.petekeen.net/dns-the-good-parts) for a good explanation

Connections can be either ESTABLISHED, NEW, RELATED, or LISTENING (I guess)

*Commands*
ss
lsof
Linux- see http://tille.garrels.be/training/tldp/ch10s06.html for a list
dhclient - not sure what this does but saw it [here](http://unix.stackexchange.com/questions/36705/why-the-default-eth0-interface-is-down-by-default-on-centos)
ifconfig - see [Ifconfig Command - Explained in Detail](http://www.aboutlinux.info/2006/11/ifconfig-dissected-and-demystified.html) which led me to [How a Broadcast Address Works](http://learn-networking.com/network-design/how-a-broadcast-address-works)
nmap - check for open ports
ping
tracerouteip
netstat
telnet
tcpdump - [http://jvns.ca/blog/2016/03/16/tcpdump-is-amazing/](http://jvns.ca/blog/2016/03/16/tcpdump-is-amazing/) not sure how this compares to Wireshark

hostname - [jargon alert and demystification](http://bitflop.com/tutorials/how-to-set-your-hostname-and-domain-name-correctly.html)!


Note that if you ifconfig [interface - e.g. eth0], the LACK of a dhcp lease indicates a static IP address

Windows - ipconfig

**Online tools**

whatismyip has various tools

http://canyouseeme.org/ checks for port scanning


## Books and overviews
[Linux Networking-concepts HOWTO](http://www.netfilter.org/documentation/HOWTO//networking-concepts-HOWTO.html) by Rusty Russell (2001) - short and humurous; also see [http://people.netfilter.org/rusty/unreliable-guides/](http://people.netfilter.org/rusty/unreliable-guides/)
http://www.cs.rpi.edu/~moorthy/Courses/os98/Pgms/socket.html

## Misc learning

See ["What happens when you type google.com into your browser's address box and press enter?"](https://github.com/alex/what-happens-when/blob/master/README.rst) on Github

[Server Admin for Programmers](https://serversforhackers.com) - prolly best resource out there

*INADDR_ANY investigation* - 

[Question about INADDR_ANY](http://stackoverflow.com/questions/6894092/question-about-inaddr-any) - answers my primary question

[UDP binding and port reuse in Linux](http://hacked10bits.blogspot.com/2014/12/udp-binding-and-port-reuse-in-linux.html) - 

## Firewalls
https://askubuntu.com/questions/448836/how-do-i-with-ufw-deny-all-outgoing-ports-excepting-the-ones-i-need - used ufw

port forwarding: iptables is not available in OSX and ipfw was removed, so use pfct or whatever
to access root on the web vagrant vm: su root with password vagrant

## OSX setup
[Mac pfctl Port Forwarding](http://salferrarello.com/mac-pfctl-port-forwarding/)

Try this: [Web Server & File Permissions in Vagrant](https://serversforhackers.com/video/web-server-file-permissions-in-vagrant)

Also if using vagrant setup may need to turn off Firewall per this [https://github.com/adlogix/docker-machine-nfs/issues/22#issuecomment-205745518](https://github.com/adlogix/docker-machine-nfs/issues/22#issuecomment-205745518)****

## old

ifconfig is replaced by iproute2 (ip link, ip addr, etc); see  [A new years resolution: Stop using deprecated CLI utilities](https://bbs.archlinux.org/viewtopic.php?id=155770) with much more detail at  [Deprecated Linux networking commands and their replacements](http://dougvitale.wordpress.com/2011/12/21/deprecated-linux-networking-commands-and-their-replacements/)

**Process managers**
Circus is better than supervisord as it can hook into circuits, uses ZeroMQ
also pm2

**Node and nginx**
See [HARDENING NODE.JS FOR PRODUCTION PART 2: USING NGINX TO AVOID NODE.JS LOAD](http://blog.argteam.com/coding/hardening-node-js-for-production-part-2-using-nginx-to-avoid-node-js-load/) for best overview on combining these

[How do you have one IP address and many websites?](http://serverfault.com/questions/106882/how-do-you-have-one-ip-address-and-many-websites) - see [here for nodejs example](http://stackoverflow.com/questions/8503841/virtual-hosting-with-standalone-node-js-server)

**Loopback and special addresses** - the 10.0.2.* addresses are special [per this](http://stackoverflow.com/a/34732276/4200039)


**Troubleshooting** -  [How to troubleshoot wireless network connectivity problems](http://searchnetworking.techtarget.com/news/945257/Wireless-network-troubleshooting-Connectivity) (2010) seems like a good start

[inSSIDer](http://www.7tutorials.com/layman-guide-solving-wireless-network-interference-problems) might be a good tool to try

**Theory and OSI**

*Overview*

Physical connection->DHPC->TCP/IP->and so on!

Wireless networks broadcast SSID (names) using MAC management frame called beacon frame  [per superuser](http://superuser.com/questions/347435/is-the-ssid-for-wireless-routers-part-of-the-link-layer-in-the-internet-protocol)

*Further*

Physical layer ( [wiki](https://en.wikipedia.org/wiki/Physical_layer)) -> many protocols but typically 802.11 (wireless, see "a technical tutorial on ieee 802" by pablo brenner, uses 2.4Ghz or 5Ghz frequency) or 803.2 (ethernet,  [see tutorial](http://www.radio-electronics.com/info/telecommunications_networks/ethernet/ethernet-ieee-802-3-tutorial.php)) frames, these frames are quite different!

Link layer ( [wiki](https://en.wikipedia.org/wiki/Link_layer)) -> links physical to data link layer? Point-to-Point Protocol (PPP,  [wiki](https://en.wikipedia.org/wiki/Point-to-Point_Protocol)) is an example which seems to talk to ISP and get the dynamic IP address  [per Rusty Russell](http://www.netfilter.org/documentation/HOWTO/networking-concepts-HOWTO-7.html),  [wisegeek has a better explanation](http://www.wisegeek.com/what-is-a-point-to-point-protocol.htm) discussing encapsulation and not sure it is involved with wireless, more information at Aaron Bachus's routeralley.com's  [Point-to-Point Protocol (PPP) pdf](http://routeralley.com/guides/ppp.pdf)

ARP showed up a lot on wireshark, see Brown's Guide to IP Layer Network Administration with Linux  [2.1. Address Resolution Protocol (ARP)](http://linux-ip.net/html/ether-arp.html) for a succinct overview

[Time-to-live TTL](http://searchnetworking.techtarget.com/definition/time-to-live) - number of hops (not really time?) as each router subtracts one, use traceroute to see number of hops it takes

Routing table - see  [7 Linux Route Command Examples (How to Add Route in Linux)](http://www.thegeekstuff.com/2012/04/route-examples/)

[Exercise 24. Networking: interface configuration, ifconfig, netstat, iproute2, ss, route](https://nixsrv.com/llthw/ex24) - detailed steps

**Subnetting**

[Why is my subnet mask 255.255.255.255?](http://superuser.com/questions/239606/why-is-my-subnet-mask-255-255-255-255) - point-to-point connection

Read  [http://serverfault.com/questions/49765/how-does-ipv4-subnetting-work](http://serverfault.com/questions/49765/how-does-ipv4-subnetting-work) again

[http://jodies.de/ipcalc](http://jodies.de/ipcalc) is great for quick visualization

IP subnetting made easy (2009) and Subnet a Class A network with ease (2002) are kinda interesting

[List of Class A  and Class B networks](http://www.aturtschi.com/whois/networks.html)

*Command-line* -
see rsync (best, also [see on trailing slash](http://devblog.virtage.com/2013/01/to-trailing-slash-or-not-to-trailing-slash-to-rsync-path/)) and scp (inferior); I used [Copying files using rsync from remote server to local machine](http://stackoverflow.com/questions/9090817/copying-files-using-rsync-from-remote-server-to-local-machine) but added --progress: sync -chavzP --stats user@remote.host:/path/to/copy /path/to/local/storage, -a is a key switch for preserving

*Tools* - on OSX use "networksetup -listallhardwareports"; on others use iproute2 tools (e.g., ss -tlnp to see listening ports)

BSD netstat is far different from Linux version [per this](http://apple.stackexchange.com/questions/97212/netstat-n-unknown-or-uninstrumented-protocol); annoying! solution is to just go with iproute2 mac

**DHCP**

In Linux, isc-dhcpd https://help.ubuntu.com/11.10/serverguide/dhcp.html and https://help.ubuntu.com/community/dhcp3-server

[What is DHCP and How DHCP Works? (DHCP Fundamentals Explained)](http://www.thegeekstuff.com/2013/03/dhcp-basics/) - 2013 article which is decent

## DNS
http://www.reddit.com/r/programming/comments/qjiue/dns_for_developers_now_theres_no_excuse_not_to/ - however I don't really understand the canonical name thing still - others recommend http://www.digwebinterface.com

### Dynamic DN
Good tutorial at http://www.dyndnsservices.com/tech.htm which has a basic laymen's introduction

## Hosting and NAT and port forwarding
2014-11 update: read great answer at SU  [What is port forwarding and what is it used for?](http://superuser.com/questions/284051/what-is-port-forwarding-and-what-is-it-used-for); note that my router is prolly set up for Faux-DMZ or maybe Universal Plug and Play, also looked at What Is 'Port Forwarding'? How Do I Set My Own Port Forwards?

## Domain viewing
domaintools.com - best
webboar.com - like domaintools but free; no history though
http://www.mydnstools.info/ - another tool; doesn't seem too special

## Site stats
Use pingdown, hyperspin, and http://www.internetseer.com
http://www.statshow.com/ - Quantcast are shown; no Alexa