Networking
===


## project ideas
### penetration test
TODO: use netcat to penetrate a system

TODO: dive into https://www.hackingloops.com/information-gathering-penetration-testing-kali-linux/

[Let's Code a TCP/IP Stack: TCP Retransmission](https://news.ycombinator.com/item?id=14701199&utm_term=comment)
Construct node server and talk between them - did this, see myutils/ and https://stackoverflow.com/questions/6442676/how-to-prevent-node-js-from-exiting-while-waiting-for-a-callback

### arp discovery tools
https://www.npmjs.com/package/local-devices

https://github.com/netdiscover-scanner/netdiscover - truly official version; rejected from homebrew https://github.com/Homebrew/legacy-homebrew/pull/47577
https://github.com/alexxy/netdiscover - code is actually quite similar

ARP Discovery: on the subnet, machines communicate through ARP as noted at [Networking Basics: How ARP Works](https://www.tummy.com/articles/networking-basics-how-arp-works/) and also see [http://superuser.com/questions/675094/how-does-communication-between-2-computers-in-a-single-network-happen](http://superuser.com/questions/675094/how-does-communication-between-2-computers-in-a-single-network-happen) - you can use netdiscover per [Scan All private subnets with NMAP [closed]](http://security.stackexchange.com/questions/54237/scan-all-private-subnets-with-nmap)

### port scanning
[Ping via nmap all possible private IPv4 addresses](http://superuser.com/questions/664827/ping-via-nmap-all-possible-private-ipv4-addresses) (also see [nmap: easily Ping-Scan all addresses in my subnet](http://superuser.com/questions/146174/nmap-easily-ping-scan-all-addresses-in-my-subnet?rq=1)) - picked up a lot of the devices on the local network which netdiscover also found...

## overview
Epiphanies: 0.0.0.0 means something different when listening on a server than routing
so server.listen('0.0.0.0') will pick up anything, e.g. both 192.168.1.0 and 127.0.0.1
curl 0.0.0.0 will route to the default gateway

IMPORTANT: Your NIC will only have an inet (ip4) address after you connect to a network - and clearly a NIC can only connect to one network under that circumstance...

Networking has a mysterious reputation with a wide surface array of concepts: sockets, nodes, a wide array of similar tools, ipv4 versus ipv6, NAT, DHCP, subnets, default gateways, OSI model, and so on. At its heart, it is not quite as difficult as it seems.

DHCP and subnet mask: these two are tied together in the sense that the DHCP server will dole out IP addresses within the given subnet mask? So if the subnet mask is 255.255.255.0 the ip address will be 1 through 254 in that final digit.

Note that [0.0.0.0](https://en.wikipedia.org/wiki/0.0.0.0) means two different things: all IP addresses of a machine (if you are listening) or the "default route" (gateway) (if you are sending).

Most of the work you'll do in learning happens on internal networks in the reserved IP address space of 10.*.*.* and 192.168.*.

Puzzle 1: a home router is available and it shows a couple devices connected that you don't recognize. How do you find them? Well, you can use nmap on the device from your computer to scan the ports which gives you some clues (and maybe it will have ssh, so you can ssh into it). Will it connect directly or go into the home router to resolve it? I'm not sure, but I suspect it won't go to the [default gateway](https://en.wikipedia.org/wiki/Default_gateway) but instead will use ARP since the address is inside the subnet mask (inside the network). And how can you ultimately find the devices? Well, you are best off just looking up the MAC address prefix and hunting around, because there's no guaranteed way!

OSI model: note [Protocol Data Units](https://en.wikipedia.org/wiki/Protocol_data_unit)!