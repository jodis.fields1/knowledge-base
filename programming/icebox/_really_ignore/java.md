In 2017, resolved to never learn java

Java IDEs - would go with Intellij but the other options are Netbeans and Eclipse:

[takari/polyglot-maven](https://github.com/takari/polyglot-maven) - I hate XML

## 2018-06
sadly getting into this due to jenkins :(

Lessons:
    * [Java: What does the colon (:) operator do?](https://stackoverflow.com/questions/2399590/java-what-does-the-colon-operator-do)

Background:
https://medium.com/@robmuh/java-is-dying-78680d34718e | Java is Dying – Rob Muhlestein – Medium
https://medium.com/@elizarov/why-im-not-enthusiastic-about-java-10-b2d789b6d42a | Why I’m not enthusiastic about Java 10 – Roman Elizarov – Medium
http://melix.github.io/blog/2016/05/gradle-kotlin.html | Cédric Champeau's blog: Gradle and Kotlin, a personal perspective

## versions and compatibility
[Is JDK “upward” or “backward” compatible?](https://stackoverflow.com/questions/4692626/is-jdk-upward-or-backward-compatible) - had to read this a few times to get it

### version manager search
motivation: get away from Oracle and its .oracle_jre_usage
conclusion: use https://github.com/shyiko/jabba and either adoptopenjdk or zulu

https://groups.google.com/forum/#!topic/mechanical-sympathy/jQGahuzJKM4 | Google Groups - points to zulu
https://github.com/linux-china/jenv | linux-china/jenv: Java enVironment Manager
https://github.com/gcuisinier/jenv | gcuisinier/jenv: Manage your Java environment

## osx
[important directories per OSX docs](https://developer.apple.com/library/mac/qa/qa1170/_index.html): /Library/Java/JavaVirtualMachines/jdk1.8.0_51.jdk/Contents/Home/jre/lib/ext has the extensions

apparently the plugins are based on the actual version: on OSX this lives in /usr/bin/java but that is reporting a different

https://apple.stackexchange.com/questions/102466/does-java-come-pre-installed-on-mac-os-x-10-7-10-8-and-beyond | macos - Does Java come pre-installed on Mac OS X 10.7/10.8 and beyond? - Ask Different
https://stackoverflow.com/questions/15826202/where-is-java-installed-on-mac-os-x | macos - Where is Java Installed on Mac OS X? - Stack Overflow
https://medium.com/@baz8080/setting-java-home-on-mac-os-acd44e3a6027 | Setting JAVA_HOME on Mac OS – Barry Carroll – Medium
https://stackoverflow.com/questions/1348842/what-should-i-set-java-home-to-on-osx | java - What should I set JAVA_HOME to on OSX - Stack Overflow

#### resources
Effective Java - apparently the Bible of books

Kotlin - Java replacement which is nicer than Scala??

polyglot-maven - no need for XML anymore

[How to print the default java classpath from the command line in windows](http://superuser.com/questions/592768/how-to-print-the-default-java-classpath-from-the-command-line-in-windows) - 

[https://github.com/google/guava](https://github.com/google/guava) - Google utility lib

[Why Java? Tales from a Python Convert](http://sookocheff.com/post/java/why-java/) - good summary


## Eclipse versus Netbeans
Given that I don't like WebStorm, I decided to investigate Eclipse and Netbeans (holding off on Visual Studio for now).
Survey at [Java Tools and Technologies Landscape for 2014](http://zeroturnaround.com/rebellabs/java-tools-and-technologies-landscape-for-2014/) is cool!

I read [What's the big IDE? Comparing Eclipse and NetBeans](http://www.theserverside.com/feature/Whats-the-Big-IDE-Comparing-Eclipse-vs-NetBeans) (2012) and [Eclipse, NetBeans or IntelliJ: Which is the best Java IDE?](https://jaxenter.com/eclipse-netbeans-or-intellij-which-is-the-best-java-ide-107980.html) notes that Eclipse was slow to get Java 8 support but has that whole platform thing. [NetBeans IDE and Intellij IDEA From The Eyes of a Long Time Eclipse User](https://dzone.com/articles/teenage-sex-and-what-the-sunmiscunsafe-misery-teac) (2014) was illuminating.

### Eclipse
The first thing I found out about Eclipse is that it is an enormous, sprawling project. It was confusing to find the main editor, and when I found [its main page](http://www.eclipse.org/eclipse/) I had to go to another step to [a wiki page](http://www.eclipse.org/eclipse/) before I found a plain English explanation:
> The unfortunately named "Eclipse Project" is the project dedicated to producing the Eclipse SDK. This name made sense back when there were only two or three projects at Eclipse, but now it is frequently referred to as the "Eclipse top-level project" to reduce confusion (or just "The Platform" when we're feeling grandiose). This project in turn is composed of five sub-projects: Platform, Java development tools (JDT), Plug-in Development Environment (PDE), e4, and Orion.

Navigating deeper into [the Platform](http://wiki.eclipse.org/Platform), I found that information on the "old component web pages is likely not up to date". Warnings like this are a good sign, but when going deeper into these pages, I found no warnings, which is confusing. This does not feel as welcoming as I'd like.

### Netbeans
See [their comparison](https://netbeans.org/features/platform/compare.html)
Great support for Javascript autocompletion!