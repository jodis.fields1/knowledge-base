Swagger ??

**Documentation / comments / style**

### Code Docs

[https://github.com/senchalabs/jsduck](https://github.com/senchalabs/jsduck) is cool for generating docs

jsdoc - reviewed several sites and all chose it but also see esdoc, but based on Java so has things like interfaces which is kind of odd; also see [https://github.com/documentationjs/documentation](https://github.com/documentationjs/documentation)

[documenting an object literal](http://stackoverflow.com/questions/10683430/jsdoc-how-do-i-document-the-options-object-literal-for-a-parent-class#14820610)

[Ask HN: How does your team write documentation? What tools do you use?](https://news.ycombinator.com/item?id=11164013) - architecture README comments

#### JSDoc
https://stackoverflow.com/questions/30012043/how-to-document-a-function-returned-by-a-function-using-jsdoc
https://stackoverflow.com/questions/24214962/whats-the-proper-way-to-document-callbacks-with-jsdoc?rq=1
https://stackoverflow.com/questions/13403887/how-to-document-callbacks-using-jsdoc?noredirect=1&lq=1
https://stackoverflow.com/questions/3171454/best-way-to-document-anonymous-objects-and-functions-with-jsdoc