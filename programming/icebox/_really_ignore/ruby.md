refer to architecture blog post also

See also Rails article
https://bugs.ruby-lang.org/ - deleted my account

http://humblelittlerubybook.com/book/html/chapter4.html - good book

## essays
http://madhadron.com/posts/2013-02-25-a-criticism-of-ruby.html


### ORM
ActiveRecord - Rails sort of pioneered it

ROM - Ruby Object Mapper, formerly DataMapper, not sure about it

###### IDE
Netbeans RoR seems best: http://plugins.netbeans.org/plugin/38549/ruby-and-rails/
Aptana (Eclipse bundled for web) has a debugger: http://www.aptana.com/products/radrails.html
Most people use Pry, Ruby-debug (?), and IRB per [How to debug ruby code?](http://stackoverflow.com/questions/4124051/how-to-debug-ruby-code) - 
vim-ruby-debugger seems promising

puts debugging - [I am a puts debugger](https://tenderlovemaking.com/2016/02/05/i-am-a-puts-debuggerer.html) handy article

###### Blocks versus callbacks
Essential to understand [yield](https://rubymonk.com/learning/books/4-ruby-primer-ascent/chapters/18-blocks/lessons/54-yield) first
[JavaScript Needs Blocks](http://yehudakatz.com/2012/01/10/javascript-needs-blocks/) (2012) -  after researching and thinking about this, I'm not totally convinced. So blocks return from the outer, but you can always assign the result from the block and return there. Also more completely explained at [Closures in Ruby](https://www.sitepoint.com/closures-ruby/) (2015) with relevant commentary at [Why Python doesn't need blocks](https://news.ycombinator.com/item?id=7965249) (2015)


## configuration
https://stackoverflow.com/questions/9474299/what-are-the-paths-that-require-looks-up-by-default - frequently confuses me, also see https://stackoverflow.com/questions/417179/how-does-ruby-know-where-to-find-a-required-file

