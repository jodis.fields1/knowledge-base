See also linux_orgs_culture

### language popularity
Motivated by https://medium.com/altdotnet/on-the-need-for-a-c-renaissance-634078d4e865 which used https://www.indeed.com/jobtrends/q-python-q-c%23.html

[The 7 Most In-Demand Programming Languages of 2018](https://www.codingdojo.com/blog/7-most-in-demand-programming-languages-of-2018/) - an annual thing

## companies

BlackDuck - runs openhub
gitmemory.com - see contributions

### Standards / professional
See Anki
http://awards.acm.org/list-of-awards - 
* [ACM Software System Award](https://en.wikipedia.org/wiki/ACM_Software_System_Award) - one of the most prestigious

#### Journals
http://programming-journal.org/ - open-access and relatively modern

### Code
RedMonk - Github and Stack Overflow
* [The New Kingmakers: How Developers Conquered the World](https://smile.amazon.com/New-Kingmakers-Developers-Conquered-World-ebook/dp/B0097E4MEU)

[Open Source is the worst](http://www.healthintersections.com.au/?p=2934)

https://octoverse.github.com/ - good metrics on growth!

### Jobs
Indeed - analysis at http://www.codingdojo.com/blog/9-most-in-demand-programming-languages-of-2017/

### Other
StackOverfow - 
Hacker News Who's Hiring Survey - ?
TIOBE - weird data sourcing such as Google Trends

### Open-source
https://ossmetrics.com/leaderboard
https://github.com/sarahsharp/foss-heartbeat