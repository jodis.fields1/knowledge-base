http://lucumr.pocoo.org/2013/12/9/stop-being-clever/ Armin's rant

**Companion libraries**

[http://timschlechter.github.io/bootstrap-tagsinput/examples/](http://timschlechter.github.io/bootstrap-tagsinput/examples/)

****

**Example**

****

​                var engine = new Bloodhound({

​                    local: [{name:'Pig with a house'}, {name: 'Rat'}, {name: 'Stephanie'}],

​                    queryTokenizer: Bloodhound.tokenizers.whitespace,

​                    datumTokenizer: function(d) {

​                        console.log(d)

​                        var tokens = Bloodhound.tokenizers.whitespace(d.name);

​                        console.log(tokens)

​                        return tokens;

​                    }

****

**Twitter typeahead**

twitter bootstrap typeahead ajax example - SO question which includes a [typeaheadjs example](http://stackoverflow.com/a/30340490/4200039)

Basic start: [How to use Typeahead.js 0.10 step-by-step / remote / prefetch / local](http://stackoverflow.com/questions/21710289/how-to-use-typeahead-js-0-10-step-by-step-remote-prefetch-local)

*Important*

use the typeahead display property (formerly displayKey) as a function to reduce the stringified object

note that if transform passes an array, each suggestion will be passed to display and you can dynamically grab the key

when using typeahead events, the events are triggered on tt-input

BUGS: [https://github.com/twitter/typeahead.js/issues/1354](https://github.com/twitter/typeahead.js/issues/1354) seems to be a more significant one

found a migration guide at [https://github.com/twitter/typeahead.js/blob/master/doc/migration/0.10.0.md#tokenization-methods-must-be-provided](https://github.com/twitter/typeahead.js/blob/master/doc/migration/0.10.0.md#tokenization-methods-must-be-provided) which answered some of my questions

datumTokenizer will help you filter local and prefetch I think -> note that it seems you must run the tokens through the function then return them

the tokenizer function expects a string [per this](http://stackoverflow.com/questions/21933532/bootstrap-typeahead-bloodhound-tokenizer-doesnt-work-with-mulitple-values)

NOTE about async: you must use the prepare (transform?) method to properly filter these results down!

identify parameter is mostly worthless

*Tokenization*

[https://github.com/twitter/typeahead.js/blob/master/doc/migration/0.10.0.md#tokenization-methods-must-be-provided](https://github.com/twitter/typeahead.js/blob/master/doc/migration/0.10.0.md#tokenization-methods-must-be-provided) from [https://github.com/twitter/typeahead.js/issues/1055](https://github.com/twitter/typeahead.js/issues/1055)

deltab  cluelesscoder: if someone's typing in e.g. a sentence, if you don't tokenize it you can only complete the sentence as a whole; whereas with it tokenized into words, you could complete each word, regardless of whether the sentence has been seen before

Twitter typeahead turned out to be a nightmare, as the tutorials targeted various different versions.

looks like I missed part of the documentation at [http://twitter.github.io/typeahead.js/examples/](http://twitter.github.io/typeahead.js/examples/)

[http://stackoverflow.com/a/30340490/4200039](http://stackoverflow.com/a/30340490/4200039) turned on a lightbulb

*deprecated* - [this](http://blattchat.com/2013/01/09/bootstrap-typeahead/) was sort of helpful until I realized it was completely outdated