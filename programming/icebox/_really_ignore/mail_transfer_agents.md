
* [How to run your own e-mail server with your own domain, part 1](https://arstechnica.com/information-technology/2014/02/how-to-run-your-own-e-mail-server-with-your-own-domain-part-1/2/)
* [Taking e-mail back, part 2: Arming your server with Postfix and Dovecot](https://arstechnica.com/information-technology/2014/03/taking-e-mail-back-part-2-arming-your-server-with-postfix-dovecot/)
* [Taking e-mail back, part 3: Fortifying your box against spammers](https://arstechnica.com/information-technology/2014/03/taking-e-mail-back-part-3-fortifying-your-box-against-spammers/)



### mail transfer agents
exim - winner

sendmail - owned by a proprietary company, cannot find version-controlled repository, has weird gotchas like https://serverfault.com/questions/399955/is-it-ok-to-rewrite-email-addresses-intended-for-remote-delivery-to-use-cnames

postfix - author does not use git, unofficial repo at https://github.com/vdukhovni/postfix
