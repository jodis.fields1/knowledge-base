https://www.reaktor.com/blog/fear-trust-and-purescript - followup on https://www.reaktor.com/blog/fear-trust-and-javascript/

http://cvlad.info/haskell/-  from https://news.ycombinator.com/item?id=17745927

[A Nonlinear Guide to Haskell](http://locallycompact.gitlab.io/ANLGTH/)

Mostly Adequate Guide by DrBoolean has lessons: https://github.com/MostlyAdequate/mostly-adequate-guide

## unorganized
TODO: the haskell book: http://haskellbook.com/ per http://bitemyapp.com/posts/2014-12-31-functional-education.html
[bitemyapp/learnhaskell](https://github.com/bitemyapp/learnhaskell) - 

## timeline
2018-06: returned to this briefly as nixos community loves haskell plus ran across clanki https://github.com/marcusbuffett/Clanki which I couldn't get to build
2016-03: started http://learnyouahaskell.com/

## ghc configuration
unpleasant initial experience while building Clanki:
https://github.com/haskell/haskell-platform/issues/304


## functors
2018-06 investigation:
    * functors allow one to map over stuff without worrying about losing the structure
        * helps to ensure associativity
    * functor is used (outdated) for first-class functions
    * [What is a functor?](https://web.archive.org/web/20171021213816/https://medium.com/@dtinth/what-is-a-functor-dcf510b098b6)
    * [Making Our Own Types and Typeclasses](http://learnyouahaskell.com/making-our-own-types-and-typeclasses#the-functor-typeclass)

## lenses ??
functors are a precursor, very similar
[Lenses Quick n' Dirty](https://vimeo.com/104807358) - 
    * recommends optics or rambda-lens https://github.com/DrBoolean/lenses/issues/10
    * 2018-06: watched


## control structures
### fewer ifs
USE switch (true) in javascript

Use Haskell to use less ifs:
http://degoes.net/articles/destroy-all-ifs

https://stackoverflow.com/questions/5464362/javascript-using-a-condition-in-switch-case/9055603#9055603
https://en.wikibooks.org/wiki/Haskell/Control_structures
https://stackoverflow.com/questions/4901902/is-there-a-step-through-debugger-for-haskell
https://elixir-lang.org/getting-started/case-cond-and-if.html#expressions-in-guard-clauses
https://stackoverflow.com/questions/1167589/anti-if-campaign