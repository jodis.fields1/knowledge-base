
## vanillajs tips
https://css-tricks.com/now-ever-might-not-need-jquery/

**Should I use regular javascript rather than jQuery**

[Demystifying JavaScript](http://toddmotto.com/talks/fowd-2014/#1)

[You don't need jQuery ](http://toddmotto.com/talks/fowd-2014/#1)- apparently Todd Moto is an expert in this area

[5 Things You Should Stop Doing With jQuery](http://flippinawesome.org/2013/05/06/5-things-you-should-stop-doing-with-jquery/) - random tips

[You might not need jQuery](http://youmightnotneedjquery.com/) - good resource for modern javascript actually, found  [thru reddit](http://www.reddit.com/r/programming/comments/1wl5f3/you_might_not_need_jquery/)

*Manipulating HTML and DOM - vanillaJS in general*
nodeList and childNodes includes text elements so go with node.children instead to get elements
NamedNodeMap is actually a list of attributes (e.g. href, name, src, class, etc)
the difference between outerHTML and innerHTML is that outerHTML contains the enclosing tags

## old
Note: remember to use .eq to get the jQuery object at index and .get to get the DOM node

Note: $('div') versus $('<div></div>') - clearly the latter creates a node. Also $(function() {insert stuff}) will execute the function when ready

**Forms**

[Convert form data to JavaScript object with jQuery](http://stackoverflow.com/questions/1184624/convert-form-data-to-javascript-object-with-jquery) - serializeArray ftw

*Pre-jQuery*
Basic
Data upload
4 alternatives:
What does enctype='multipart/form-data' mean? and Sending multipart/formdata with jQuery.ajax and then check the HTMLFormElement - note reset and clear

