Related: Marionette general

[Javascript inheritance (through Backbone source code), why Surrogate function in Backbone?](http://stackoverflow.com/questions/26221925/javascript-inheritance-through-backbone-source-code-why-surrogate-function-in) - still don't get this

## Tips

### Routing

#### debugging routes
If you are struggling to figure out why your route isn't matching:
Find the routing source code - wrap an if condition around the route inside the 
_routeToRegExp function:

if (route === "action(/:scope)/*") {
    debugger
}

get the result of new RegExp and then use it to check against the route
you're trying to match

inside, you can call functions with 
this.__proto__.constructor.__super__.__proto__._routeToRegExp()

### splat
The docs are a bit confusing about `*splat` - the "splat" part is any arbitrary string,
but no string won't work. Inside the Backbone.Router code:
this.__proto__.constructor.__super__.__proto__._routeToRegExp('action/:scope/*splat').toString() == this.__proto__.constructor.__super__.__proto__._routeToRegExp('action/:scope/*path').toString()


**Form binding**[http://stackoverflow.com/questions/4074636/can-i-bind-form-inputs-to-models-in-backbone-js-without-manually-tracking-blur-e](http://stackoverflow.com/questions/4074636/can-i-bind-form-inputs-to-models-in-backbone-js-without-manually-tracking-blur-e)

also look at stickit and modelbinder [https://www.smashingmagazine.com/2013/08/backbone-js-tips-patterns/](https://www.smashingmagazine.com/2013/08/backbone-js-tips-patterns/)

**Rendering**

[Rendering Views in Backbone.js Isn't Always Simple](http://ianstormtaylor.com/rendering-views-in-backbonejs-isnt-always-simple/) - 

**Epiphanies**

el is just a string - you use that a selector when you do this.$el.html()

**Example apps**

[ccoenraets/backbone-directory](https://github.com/ccoenraets/backbone-directory) - better than ToDoMVC

[backbone-boilerplate/backbone-boilerplate](https://github.com/backbone-boilerplate/backbone-boilerplate) - 

[Backbone.js get and set nested object attribute](http://stackoverflow.com/questions/6351271/backbone-js-get-and-set-nested-object-attribute) - nested library linked here

N

**Random thoughts**

Input->Controller->Model->View->Browser

To get the sync event to fire return an empty json object! this triggers a 'success' on save - see [here](https://groups.google.com/forum/#!topic/backbonejs/EicUn91Jy-E) for similar complaint

Backbone.Model.extend({}) is a function execution (of a constructor function) which creates a constructor function! it has some default properties which get attached to the instance

_.extend, _.defaults, and _.extendOwn are assigned to createAssigner, so get used to seeing that function! it is important!

**Router**

[Push State with Node.js and Express](http://heu.io/posts/push-state-with-nodejs-and-express/) - this was necessary but swapped sendFile for send per [this suggestion](http://stackoverflow.com/a/26615223/4200039)

[Backbone Routes with pushState on are not working when you refresh page](http://stackoverflow.com/questions/19124452/backbone-routes-with-pushstate-on-are-not-working-when-you-refresh-page) - absolute paths for those script tags!

[How to handle a simple click event in Backbone.js?](http://stackoverflow.com/questions/14198959/how-to-handle-a-simple-click-event-in-backbone-js) - add global delegated event handler for pushState, related to various others [Preventing full page reload on Backbone pushState](http://stackoverflow.com/questions/7640362/preventing-full-page-reload-on-backbone-pushstate)

[Backbone Router root duplicates in the url](http://stackoverflow.com/questions/17410633/backbone-router-root-duplicates-in-the-url) - I had this issue but I fixed it by adding a slash before the href (relative to root domain)

**Resources**

[Developing Backbone.js Applications](http://addyosmani.github.io/backbone-fundamentals/#is-the-marionette-implementation-of-the-todo-app-more-maintainable) - Osmani's open-source book

[backbone-contact-manager](https://github.com/dmytroyarmak/backbone-contact-manager) - good example for routing!

[Backbone routes without hashes?](http://stackoverflow.com/questions/7310230/backbone-routes-without-hashes) - good suggestion to use Backbone Boilerplate

Bones - conventions found thru [Reusing backbone views/routes on the server when using Backbone.js pushstate for seo/bookmarking](http://stackoverflow.com/questions/7098130/reusing-backbone-views-routes-on-the-server-when-using-backbone-js-pushstate-for)