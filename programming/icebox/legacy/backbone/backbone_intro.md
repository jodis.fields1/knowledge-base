Although Backbone is among the oldest MVC (Model-View-Controller) frameworks, it doesn't seem to have a truly approachable tutorial system. Veteran users freely admit that it does not support basic functionality which you can get quickly and easily using Angular or Ember, such as parallel views. The source code is annotated and concise, but that doesn't really replace a good overview.[^n]

First off, you should be using the collection methods most of the time. The raw `sync` method, which is actually [not really related to the `sync` *event*](Are both the 'sync' event and Backbone.sync), is advanced and you probably won't need it.[^n]

**Tutorial**<br/>
There are a lot of tutorials, but [Jason's Krol's](https://web.archive.org/web/20161007043400/https://kroltech.com/2013/12/29/boilerplate-web-app-using-backbone-js-expressjs-node-js-mongodb/) (with [github source code](https://github.com/shorttompkins/benm)) is probably the most detailed introduction. The most advanced instruction might require paying up for something like [BackboneRails.com](http://www.backbonerails.com/).

Pragmatic Backbone is a good source of advanced advice, such as the [suggestion to use the Backbone object as a global pubsub](http://pragmatic-backbone.com/using-events-like-a-baws). 

It won't be too long before you're ready to plug in Marionnette. Ben McMormick has a [series on Marionnette](http://benmccormick.org/marionette-explained/) (TODO: read them!).

[^n]: Additional documentation on routing was  provided recently, but you'll have to read the source to even learn about options sometimes.
[^n]: See [issue #2552](https://github.com/jashkenas/backbone/issues/2552) complaining about sync's lack of functionality (TODO: check docs discrepancy) and Stack Overflow's [Backbone.js Sync is not triggering any events on the model](http://stackoverflow.com/questions/10590260/backbone-js-sync-is-not-triggering-any-events-on-the-model) for details.