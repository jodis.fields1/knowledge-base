**Handlebars**

handlebars - [assemble/handlebars-helpers](https://github.com/assemble/handlebars-helpers) and chain with [this trick](https://github.com/wycats/handlebars.js/issues/304) tho not sure it works with [Chain helpers or filter data in handlerbars js](http://stackoverflow.com/questions/12523741/chain-helpers-or-filter-data-in-handlerbars-js); also 

[How to set selected select option in Handlebars template](http://stackoverflow.com/questions/13046401/how-to-set-selected-select-option-in-handlebars-template) - neat trick

inflection - [i package](https://www.npmjs.com/package/i), but best one seems to be [dreamerslab/node.inflection](https://github.com/dreamerslab/node.inflection) 

**

[Registration of templateHelpers for Templating Engines other than Underscore (eg. Handlebars)](https://github.com/marionettejs/backbone.marionette/issues/806) - ??

[Handlebars template helpers in Backbone.Marionette](http://mikefowler.me/2014/02/20/template-helpers-handlebars-backbone-marionette/) - ??

*TO-DOCUMENT*: 

use @root to access the parent per [this](https://github.com/wycats/handlebars.js/issues/196)