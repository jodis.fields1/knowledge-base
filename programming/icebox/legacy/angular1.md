[Zeef AngularJS categorized list by by Gianluca Arbezzano](https://angularjs.zeef.com/gianluca.arbezzano) - great place to grab directives

[Preparing for the future of AngularJS](https://www.airpair.com/angularjs/posts/preparing-for-the-future-of-angularjs)

[Advanced routing and resolves](https://medium.com/opinionated-angularjs/advanced-routing-and-resolves-a2fcbf874a1c)

[The Top 5 Mistakes AngularJS Developers Make Part 2: Abusing $watch](https://csharperimage.jeremylikness.com/2014/11/the-top-5-mistakes-angularjs-developers_28.html)

[Why you should not use AngularJS](https://medium.com/@mnemon1ck/why-you-should-not-use-angularjs-1df5ddf6fc99)


**Gotchas**

[Angular .controller() runs before .run() AngularJS](http://stackoverflow.com/questions/29475825/angular-controller-runs-before-run-angularjs) - watch out for race conditions, there is no queuing

[AngularJs: How to check for changes in file input fields?](http://stackoverflow.com/questions/17922557/angularjs-how-to-check-for-changes-in-file-input-fields) - lots of requests for this on Github!

$scope.apply - you should be wrapping your 'outside of angular' changes inside this function per [AngularJS and scope.$apply](http://jimhoskins.com/2012/12/17/angularjs-and-apply.html)

JohnPapa: Aim for modules that are a hundred lines or less and functions that are 10 lines or less, preferably 5 lines or less!

Why do we need to pass in dependencies as an array? Because minification can break Angular apps if not as the function parameters are minified!

Directives -> contain a link function

When you use the ControllerAs syntax (e.g., MyController as controller) and want to reference a variable, you must use this in the constructor.

**Fundamentals**

[The AngularJS Module System](http://blog.jorgenschaefer.de/2014/04/the-angularjs-module-system.html) (2014) - 

[Studying the Angular JS Injector - loading modules](http://taoofcode.net/studying-the-angular-injector-loading-modules/) - 

note config and run blocks only run once at initialization

**Directives**

*ngClass* - see [ngClass Using Evaluated Expression](https://scotch.io/tutorials/the-many-ways-to-use-ngclass)

*Custom directives*

[How to create Self Closing Alert Message in AngularJS](http://stackoverflow.com/questions/21759898/how-to-create-self-closing-alert-message-in-angularjs) - sort of taught me how to use a custom directive, especially with the plunker

if you use '@', the string in the attribute is passed directly into the template; '=' is for matching;

**Styles**

[Angularjs code/naming conventions [closed\]](http://stackoverflow.com/questions/20802798/angularjs-code-naming-conventions) - there's a few! mgechev, johnpapa, and Google seem to be the main ones

**Angular 2.0** - ng-Europe 2014 presentation on [Angular 2 Core](https://docs.google.com/presentation/d/1XQP0_NTzCUcFweauLlkZpbbhNVYbYy156oD--KLmXsk/preview?forcehl=1&hl=en&sle=true&slide=id.p)

**Cheat sheet** - really good resource! also see [ui-router cheatsheet](https://d2eip9sf3oo6c2.cloudfront.net/pdf/egghead-io-ui-router-cheat-sheet.pdf)