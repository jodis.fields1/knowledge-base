http://stackoverflow.com/questions/7909161/jquery-iframe-file-upload?lq=1

You can use map, forEach, reduce, filter, etc.

There's a few tricks to make writing for IE9 less painful:

* [A fix for window.location.origin in Internet Explorer](http://tosbourn.com/a-fix-for-window-location-origin-in-internet-explorer/)

**Testing**

* see https://dev.modern.ie/tools/vms/mac/
* [JSON.stringify objects to the console](http://stackoverflow.com/questions/2434613/console-log-in-ie-on-an-object-just-outputted-object-object)