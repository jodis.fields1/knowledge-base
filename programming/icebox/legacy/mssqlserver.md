
## SQL Server migration scripts
You can connect to the SQL Server Management Studio by loading it up in Virtualbox by specifying the local machine as the Server name and using the proper password. In Virtualbox, `192.168.50.1` is typically used as an interface to the local (Mac, Linux) machine.
![image](https://cloud.githubusercontent.com/assets/5614134/21158308/a47863fe-c131-11e6-8260-acf5eff9a425.png)


* also see [Restoring a SQL Database Backup Using SQL Server Management Studio](http://www.howtogeek.com/50354/restoring-a-sql-database-backup-using-sql-server-management-studio/) for how to restore with the GUI.
* If you'd prefer, you can use command-line scripts: 
    * [Simple script to backup all SQL Server databases](https://www.mssqltips.com/sqlservertip/1070/simple-script-to-backup-all-sql-server-databases/), 
    * [Auto generate SQL Server restore script from backup files in a directory](https://www.mssqltips.com/sqlservertip/1584/auto-generate-sql-server-restore-script-from-backup-files-in-a-directory/), and 
    * [Auto generate SQL Server restore script from backup files in a directory](https://www.mssqltips.com/sqlservertip/1584/auto-generate-sql-server-restore-script-from-backup-files-in-a-directory/)

#### SQL Server Management GUI
Note that there is a gotcha in the restoration process where it will fail if you try to restore to a "device" (file) when the database exists. Instead, you should delete the database, then right-click Databases in the Object Explorer->Restore Database. The menu may state that it is restoring to the last previous backup which is not the time of your file, but you can ignore that (it appears).

You may also need to check the option "Close existing connections to destination database".
![image](https://cloud.githubusercontent.com/assets/5614134/21158456/1b72f41a-c132-11e6-99f9-1cba0bca5135.png)

It's a good idea to backup your development database before running the migration to allow restoration if it doesn't go as planned:

* To create a backup right click on your database name. Go to "Tasks" -> "Back Up..."

To delete the last migration from the sqlite db so that it will be picked up to run again, you can create a utility like the below and run it from the directory which contains migration.db:
```
#!/usr/bin/env bash
while true; do
    sqlite3 migration.db <<< "SELECT * FROM bi_migration ORDER BY id DESC LIMIT 1;"
    read -p "Do you wish to delete last migration [yn]" yn
    case $yn in
        [Yy]* ) sqlite3 migration.db <<< "DELETE FROM bi_migration ORDER BY id DESC LIMIT 1;"; break;;
        [Nn]* ) exit;;
        * ) echo "Please answer yes or no.";;
    esac
done
echo "Deleted last migration!"
```

## Brightidea SQL server tips and tricks
### Expired password?
You have to use the SQL Server Management Console on Windows. Someday maybe they'll support macOS / Linux (https://dba.stackexchange.com/questions/196335/change-expired-user-password-on-sql-server-from-linux).

### Schema and design 
- views versus tables
      * some of our tables are actually views, such as bi_idea_detail which is bi_idea + a few aggregated    columns like score so you can sort on score easier (from active record)
      * you can run `sp_helptext bi_idea_detail` in sql studio to see the query for the view

- handy commands
      * `sp_helptext` shows you the query for any view, proc, or function
      * `sp_help` [table_name] gives you the schema and meta-data for any table
      * `sp_columns` shows the columns
      * may need to use `exec` e.g. `exec sp_help [table_name]`

- find a table if you know part of the name

        SELECT top 10 TABLE_NAME
        FROM INFORMATION_SCHEMA.TABLES
        WHERE TABLE_TYPE = 'BASE TABLE' and table_name like '%pref%' 


### Common errors
Please document the common cause of SQL errors here.
- Zero width characters: it is very easy to copy zero-width characters from the SQL Server GUI into your IDE, where they can wreak all sorts of inexplicable havoc. 
    * IntelliJ [Zero-width character locator](https://github.com/harley84/ZeroLengthRadar) plugin is recommended. 
    * You can set up PHPStorm [Database integration](https://github.com/brightideainc/main/wiki/Developer-Tools-(PHPStorm)#database-connection) to avoid using SQL Server GUI.
- You cannot reuse the same label for a bound parameter - so you need to use $member_id twice, you should create multiple labels (e.g., `:member_id1`, `:member_id2`). Can crop up with various error messages such as:
    * `Operand type clash: text is incompatible with int`
    * `COUNT field incorrect or syntax error`
- `Conversion failed when converting from a character string to uniqueidentifier`
    * Usually caused when trying to filter by ID but it's blank
    * Solved by wrapping the problem column in `CAST ( column as NVARCHAR(4))` but ideally we would like to avoid casting
    * In general, type conversion can happen automatically per [this chart](https://msdn.microsoft.com/en-us/library/ms187928.aspx) but if the field cannot be converted into a uniqueidentifier it will fail. Work-in-progress in understanding how this crops up.
    * For example, notification.reference_id is an nvarchar which contains three character strings and UUIDs, and seems to need casting.
- `Invalid string or buffer length` - TODO; might also have been caused by trying to reuse a label


### Performance and tuning
 - How to shrink a data file when shrink from the GUI does not work

        DBCC SHRINKFILE (LOGICAL-db-file-name, target_size_in_MB) 
- rebuild all indexes in all tables in all dbs: http://www.mssqltips.com/sqlservertip/1367/sql-server-script-to-rebuild-all-indexes-for-all-tables-and-all-databases/

- check for unused indexes: http://www.handsonsqlserver.com/how-to-identify-and-remove-unused-indexes-only-after-carefully-review-and-verification/

- to check if there is sufficient RAM/memory on a machine

        see # 5 here: http://thomaslarock.com/2012/05/are-you-using-the-right-sql-server-performance-metrics/ 

- If you are debugging high CPU execute this proc that I (Lou) installed on the master DB. It will show currently active queries. Repeatedly execute it so you see if there is a pattern as to which queries are running the most

        exec sp_WhoIsActive 

- To improve performance set "Auto Close" property on a database to false. (sql server express)
- time a query from within MS SQL Server Studio

        SET STATISTICS TIME ON
        -- query here
        SET STATISTICS TIME OFF 
Some good queries to help performance and detect bottlenecks:

    http://sqlserverperformance.wordpress.com/2011/02/04/five-dmv-queries-that-will-make-you-a-superhero-in-2011/ 