2017: https://blogs.technet.microsoft.com/dataplatforminsider/2017/04/03/sql-server-command-line-tools-for-mac-preview-now-available/
    * sqlcmd?

## Version
Program Files\Microsoft SQL Server\MSSQL.n\MSSQL\LOG\ERRORLOG says that we’re running SQL Server 2012 - 11.0.2100.60 (x64) Express Edition (Build 9200:)

## Describe tables
*describe table / \d* - use [exec sp_columns](http://stackoverflow.com/questions/319354/what-is-the-equivalent-of-describe-table-in-sql-server)

## Recover deleted
*Recover deleted -* [see this](http://dba.stackexchange.com/questions/995/how-do-i-get-back-some-deleted-records) and download the stored prc

eventually I added back my username with this: INSERT INTO dbo.bi_member (member_id, affiliate, create_date, email, password_set_date, screen_name) VALUES('49B56D85-3148-4D39-B8F2-6E79CD2365EB', 'OD7', '20150818 10:34:09 AM', 'bcreasy@brightidea.com', '20150818 10:34:09 AM', 'bcreasy');

## Find stuff
find all columns: used [this](http://blog.sqlauthority.com/2008/08/06/sql-server-query-to-find-column-from-all-tables-of-database/) to find role_id column

find a string: used [this](http://stackoverflow.com/a/885112/4200039) successfully