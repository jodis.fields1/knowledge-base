Legacy because lodash replaced it

**Underscore** -

one gotcha: mapping over undefineds returns them (unlike $.map), so use _.compact to trim it down

best overview I've found is  [Simplify your JavaScript with Underscore.js](http://singlebrook.com/blog/simplify-your-javascript-with-underscorejs); noticed that they had similar each and map methods, but apparently per  [Do Underscore.js and jQuery complement each other?](http://stackoverflow.com/questions/8907373/do-underscore-js-and-jquery-complement-each-other) at SO