I never liked these.

Got caught by using var in my before & beforeEach - these were scoped to that block and not available in the it block! Duh!

For asynch, remember to execute the done() function per  [Async function in mocha before() is alway finished before it() spec?](http://stackoverflow.com/questions/24723374/async-function-in-mocha-before-is-alway-finished-before-it-spec)

If you're running mochajs in the browser, remember you have to toss in the mocha.setup('bdd') and mocha.run()! and remember your stylesheet!

## karma
Karma and requirejs: [Lab: A beginners guide to starting a new web app with Karma, Jasmine and RequireJS](http://stephenjamescode.blogspot.com/2014/05/lab-beginners-guide-to-starting-new-web.html) - best guide by far

Karma - first ran into issues with chai not being defined, but you have to list them in the files of your config per [Include dependencies in Karma test file for Angular app?](http://stackoverflow.com/questions/20132823/include-dependencies-in-karma-test-file-for-angular-app) (there's also the karma-chai plugin but don't use that, it seems unmaintained)

## mocha
[Feature proposal: global setup and teardown](https://github.com/mochajs/mocha/issues/1460) - already exists

[Mocha tests with extra options or parameters](http://stackoverflow.com/questions/16144455/mocha-tests-with-extra-options-or-parameters/16145010#16145010) - arbitrary parameters


[Running subsets of Mocha test suites](http://paulsalaets.com/posts/running-subsets-of-mocha-test-suites/) - karma-mocha also has a grep CLI

## sinon
[Testing your frontend JavaScript code using mocha, chai, and sinon](https://nicolas.perriault.net/code/2013/testing-frontend-javascript-code-using-mocha-chai-and-sinon/) (2013) - probably best I found, by the guy who wrote CasperJS

[Best Practices for Spies, Stubs and Mocks in Sinon.js](https://semaphoreci.com/community/tutorials/best-practices-for-spies-stubs-and-mocks-in-sinon-js) (2015) - 