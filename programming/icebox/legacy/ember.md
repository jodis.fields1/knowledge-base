
https://medium.com/@lukedeniston/how-to-write-a-really-really-ambitious-spa-in-2019-60fc38de89db

2019: revisited due to inthe.am
* https://medium.com/front-end-weekly/using-react-components-in-your-ember-app-8f7805d409b0
* https://guides.emberjs.com/release/getting-started/core-concepts/

## Ember
Migration to React / Redux discussion and the mess of state everywhere is interesting

[5 Essential Ember 2.x Concepts You Must Understand](http://emberigniter.com/5-essential-ember-2.0-concepts/) - ??

Generally Data Down Actions Up (DDAU) but sometimes sideways with a service per

[Handling an action triggered by the same component used in multiple routes](http://discuss.emberjs.com/t/handling-an-action-triggered-by-the-same-component-used-in-multiple-routes/8561)

[Why i'm leaving ember](http://discuss.emberjs.com/t/why-im-leaving-ember/6361/132) - pent-up frustration!