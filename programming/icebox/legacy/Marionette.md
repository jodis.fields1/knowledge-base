Related: Backbone introduction

[http://www.backbonerails.com/series](http://www.backbonerails.com/series)

**Transition to React** - see React note

[How We Rebuilt Our App, Part 2: From Rails + Marionette to React](http://web.archive.org/web/20160304012706/http://blog.siftscience.com/blog/2015/from-rails-and-marionette-to-react) - Sift Science seems relatively sophisticated

[Gentle Migration from Marionette to React](http://jgaskins.org/blog/2015/02/06/gentle-migration-from-marionette-to-react)

**Best practices / basic flow** - no DOM manipulation, lots of child views, and render all children within parent layoutView so you can run onAttach and hit them all

Document options, especially non-default ones!

[folder and file structure](https://gist.github.com/apehead/ad4153dcb45746a399b9) - lib for components is essential     

**Non-persistent state**

Per [Things I Learned from Building a Large Scale Backbone + Marionette Application](http://authenticff.com/journal/building-large-scale-backbone-marionette-applications) you can use methods which don't get saved obviously

[https://ampersandjs.com/docs/#ampersand-state](https://ampersandjs.com/docs/#ampersand-state)

[https://www.npmjs.com/package/marionette.state-service](https://www.npmjs.com/package/marionette.state-service)

**Request-response**

[What is a good way to document sub/pub?](http://stackoverflow.com/questions/15825334/what-is-a-good-way-to-document-sub-pub) - good idea to document intermodule comm

Debate:

[http://victorsavkin.com/post/50669505752/marionette-requestresponse-considered-harmful](http://victorsavkin.com/post/50669505752/marionette-requestresponse-considered-harmful)

[https://github.com/jmeas/blog/blob/839d21d600126c6e4c60e8dd6708f7a9a8a88b88/source/_drafts/events-part-1.md](https://github.com/jmeas/blog/blob/839d21d600126c6e4c60e8dd6708f7a9a8a88b88/source/_drafts/events-part-1.md)

Alternatives include Thorax (seems dead), Chaplin (less dead, akre53 recommends it), Layout Manager (?), Exoskeleton (works with Chaplin?), and... (drumroll)

**Backbone-Marionette**

*Behaviors* - [Marionette.js Behaviors, Part 1: The Basics](https://spin.atomicobject.com/2014/09/11/marionette-behaviors-overview/) (see Part 2 on testing) and [Staying DRY with Marionette Behaviors](http://benmccormick.org/2015/03/23/staying-dry-with-marionette-behaviors/)

*General*

Backgrid has some patches

Look at line 2200 or so for the render function!

using version 2.4.2

take note of the lifecycle. for example, with CollectionView and Child ItemViews, Marionette renders (combines together) a document fragment of CollectionView with items and then attaches it to the DOM. lifecycle:

before:render -> render each individual item -> render:collection (after complete) -> attach collection -> attach each item -> show collection -> show each

**

*Understanding* - [delegateEvents in backbone.js](http://stackoverflow.com/questions/11073877/delegateevents-in-backbone-js) explains that

look into [founder's screencast](https://sub.watchmecode.net/pricing-plans/)