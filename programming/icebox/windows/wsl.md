# windows subsystem linux
wslview - https://superuser.com/questions/1160419/how-can-i-open-a-file-from-wsl-with-the-default-application
# projects
## fix bluetooth
https://answers.microsoft.com/en-us/windows/forum/windows_10-networking-winpc/unable-to-remove-bluetooth-device-on-windows-10/ea6da83d-583e-4b80-8714-367510879f07?page=21

## fix keyboard apping
https://github.com/oblitum/Interception | GitHub - oblitum/Interception: The Interception API aims to build a portable programming interface that allows one to intercept and control a range of input devices.
https://superuser.com/questions/381296/mapping-caps-lock-to-escape-and-control-on-windows-7 | keyboard - Mapping Caps Lock to Escape and Control on Windows 7 - Super User
https://github.com/susam/uncap | GitHub - susam/uncap: Map Caps Lock key to Escape key on Windows (with Uncap), Linux (with setxkbmap), and macOS (with System Preferences)
https://twitter.com/EnergyCredit1/status/1172922413732028416 | O&G OG on Twitter: "$MELI just to throw a DCF out there to support view its overvalued to make simple i'll use this as a framework 1/ https://t.co/4YPFifLUP3" / Twitter

## windows export and restore all settings including wsl
Google search: "windows export and restore all settings including wsl"

# unorganized
https://docs.microsoft.com/en-us/windows/wsl/ | An overview on the Windows Subsystem for Linux | Microsoft Docs


https://techcommunity.microsoft.com/t5/windows-dev-appconsult/running-wsl-gui-apps-on-windows-10/ba-p/1493242 | Running WSL GUI Apps on Windows 10 - Microsoft Tech Community

https://superuser.com/questions/435602/shortcut-in-windows-7-to-switch-between-same-applications-windows-like-cmd | Shortcut in Windows 7 to switch between same application's windows, like Cmd + ` in OS X - Super User

# draft stackoverflow answer
It appears that if you install the extensions locally, they also become available remotely. So you just need to install them locally.

I have a version-controlled bash script (similar to this approach https://www.growingwiththeweb.com/2016/06/syncing-vscode-extensions.html). I installed Git for Windows and then in Powershell navigated to https://stackoverflow.com/questions/1098786/run-bash-script-from-windows-powershell


PS: I asked one of the VSCode developers how to approach it also https://twitter.com/jcrben/status/1316261476126978048

# gotchas
Full desktop
* https://www.reddit.com/r/bashonubuntuonwindows/comments/eqwoxy/gnome_and_kde_in_wsl/fsjoj9n/?utm_source=reddit&utm_medium=web2x&context=3 | Gnome and KDE in WSL : bashonubuntuonwindows

Disappearing:
* https://superuser.com/questions/1474559/wsl2-x11-programs-disappear | windows 10 - WSL2 X11 programs "disappear" - Super User

Path issue:
* 

WSL version:
* https://docs.microsoft.com/en-us/windows/wsl/compare-versions | Comparing WSL 2 and WSL 1 | Microsoft Docs

VSCode:
* https://code.visualstudio.com/blogs/2019/09/03/wsl2 | Using WSL 2 with Visual Studio Code
* https://code.visualstudio.com/docs/remote/wsl | Developing in the Windows Subsystem for Linux with Visual Studio Code

Window moving:
* https://www.reddit.com/r/bashonubuntuonwindows/comments/b0b4fx/having_problems_with_vscode_under_wsl_x11_cant/ | Having problems with vscode under WSL (x11) - can't move the window : bashonubuntuonwindows
* https://github.com/vercel/hyper/issues/1683 | How to show native title bar and close icon on Arch Linux / Gnome? · Issue #1683 · vercel/hyper


# setup

# hyper
https://github.com/vercel/hyper/issues/2207#issuecomment-538577097 | Crashes when `wsl.exe` is selected as shell · Issue #2207 · vercel/hyper

# link dump
https://unix.stackexchange.com/questions/16815/what-does-display-0-0-actually-mean | xorg - What does DISPLAY=:0.0 actually mean? - Unix & Linux Stack Exchange

https://www.tenforums.com/tutorials/164318-how-set-linux-distribution-version-wsl-1-wsl-2-windows-10-a.html#option2 | How to Set Linux Distribution version to WSL 1 or WSL 2 in Windows 10 | Tutorials
https://stackoverflow.com/questions/37523980/running-gui-apps-on-docker-container-with-a-macbookpro-host | macos - Running GUI apps on docker container with a MacBookPro host - Stack Overflow

https://gist.github.com/leodutra/a6cebe11db5414cdaedc6e75ad194a00 | Installing Windows Subsystem for Linux 2, Hyper, ZSH and VSCode extensions
https://gist.github.com/mtobeiyf/d7a58eb676a784c57f49afc46ca2f56f | WSL + ZSH + Hyper Setup
https://github.com/microsoft/WSL/issues/1493 | [Question] How to remove Windows pathes from WSL PATH? · Issue #1493 · microsoft/WSL
https://docs.microsoft.com/en-us/windows/wsl/interop#share-environment-variables-between-windows-and-wsl | Windows interoperability with Linux | Microsoft Docs
https://devblogs.microsoft.com/commandline/automatically-configuring-wsl/ | Automatically Configuring WSL | Windows Command Line
