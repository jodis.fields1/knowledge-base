# Portable apps

Apps to install:
* VLCPortable
* Anki
* FSViewer43 - FastStone Image Viewer
* PidginPortable
* PuttyPortable
* WiresharkPortable
* https://github.com/GiacomoLaw/Keylogger

## overview
Been a big fan of portableapps rather than regular programs. Unfortunately Windows 7 isn't really designed around them but oh well.

.paf is PortableAps Format; has some certifications associated with it

PortableFreeWare.com by Andrew Lee and centered around JauntePE and redllar is another community ( [see interview](http://www.mattscuppa.com/2008/03/31/portable-freeware/ "http://www.mattscuppa.com/2008/03/31/portable-freeware/"))

nice to see that Gizmo's Freeware http://www.techsupportalert.com/best-free- portable-programs.htm is also a big supporter

_Integrating with file extensions_
didn't turn up much in a search; http://portableapps.com/support/firefox_portable suggests I can do this with Firefox at least...

**Windows registry**
This tends to get cluttered up; reviewed  [When \- and why - should you store data in the Windows Registry? - Stack Overflow](http://stackoverflow.com/questions/268424/when-and-why-should-you-store-data-in-the-windows-registry "http://stackoverflow.com/questions/268424/when-and-why-should-you-store-data-in-the-windows-registry") for some thoughts 

**Portable apps managers**
see http://superuser.com/questions/2836/whats-the-most-popular- portable-applications-manager and http://lifehacker.com/5389421/five-best- portable-apps-suites

http://www.codyssey.com/products/codysafe.html - for-profit, not open source

LiberKey - French, for-profit

PortableApps Suite - run by the well-known John Haller - probably best option; income from usb deals (http://portableapps.com/blogs/johnhaller/2011-04-27_status_update) but probably not lucrative - ask about buying a flash drive with the suite included; can be found in LinkedIn using portablekeyword

PStart - recommended per http://www.davinciplanet.com/reasons-to-use- portable-applications/

SyncBack - recommended by above

**In Linux**
Qt - see  [Create portable Qt application for linux](http://qt-project.org/forums/viewthread/46043 "http://qt-project.org/forums/viewthread/46043") for tips and also  [portableappslauncher docs](http://portableapps.com/manuals/PortableApps.comLauncher/ "http://portableapps.com/manuals/PortableApps.comLauncher/") have some information

http://en.wikipedia.org/wiki/Portable_application#Portability_on_Linux_and_UNIX-like_systems is a good overview

[appstogo linux page](http://appstogo.mcfadzean.org.uk/linux.html "http://appstogo.mcfadzean.org.uk/linux.html") has a few

http://superuser.com/questions/2844/ portable-apps-for-linux covers a lot

http://portablelinuxapps.org/ seems to be the main thing; uses the AppImageKit by Simon Peter which is dated 2005; also see the  [ELF Statifier](http://statifier.sourceforge.net/ "http://statifier.sourceforge.net/") or Ermine (paid cousin) 

Daniil Kulchenko had an idea on this for Linux but ran into issues per http://portableapps.com/node/16896 - says they have been somewhat addressed per portools.com but this seems to be dead; another person in the thread points to Gentoo Prefix