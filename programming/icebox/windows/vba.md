My days of VBA are thankfully long gone. But I have a bunch of notes from those days, and maybe someone will find something useful here. Note that VB6 is basically the same thing as VBA with a few less limitations. Most of the documentation you'll find is from Microsoft MVPs (see [wiki](https://en.wikipedia.org/wiki/Microsoft_Most_Valuable_Professional)), of which there are fewer than I had thought: maybe around 4000 total last I checked.

If you're going to program Excel, I suggest checking http://www.python-excel.org/ or https://github.com/PHPOffice/PHPExcel. I haven't used them but the experience will likely be more pleasant and skills more transferable.

**Tips**<br/>
[Ch. 5 Using Ranges excerpt](https://msdn.microsoft.com/en-us/library/aa139976(v=office.10).aspx) from Excel 2002 VBA Programmer's Reference. I first discovered this trick using [How to Loop in Cell Range in Excel](https://suite.io/guy-lecky-thompson/6ep21g) which opened my eyes to using cells and looping over columns!
[EXCEL VBA, inserting blank row and shifting cells](http://stackoverflow.com/questions/15816883/excel-vba-inserting-blank-row-and-shifting-cells) is also helpful.

**General**<br/>
VBA in general
Main reference page:  [Microsoft Office 2010](https://msdn.microsoft.com/en-us/library/office/cc313152(v=office.12).aspx)  andtechnical spec at  [[MS-VBAL]: VBA Language Specification](http://msdn.microsoft.com/en-us/library/dd361851.aspx) 
http://www.excel-easy.com/vba.html is great, be sure to look at right for more detail

IsNumeric doesn't work quite right per http://www.mrexcel.com/forum/ excel-questions/17013-isstring-isnumber-visual-basic-applications.html which has alternative function

In Excel, use Personal.xlsb to store your modules and functions

http://blog.mclaughlinsoftware.com/2010/05/26/excel-udf-tutorial/ is a good tutorial on creating user-defined functions

http://www.freevbcode.com/ has a lot of code that can be used

unfortunately no "in" function per http://stackoverflow.com/questions/1505206/imitating-the-in-operator

"Option Compare Text" to do case-insensitive string comparison; you can also do StrComp http://stackoverflow.com/questions/756118/vb-vba-strcomp-or


[Find last row, column or last cell](http://www.rondebruin.nl/win/s9/win005.htm) - ?

[IsLetter](https://techniclee.wordpress.com/2010/07/21/isletter-function-for-vba/) - code could come in handy

Python for Excel - ? seems really cool but hard to understand; other methods include IronSpread, datanitro, openpyxl, xlrd and xlwt, etc

**Access**<br/>
[Commonly used naming conventions](http://access.mvps.org/access/general/gen0012.htm) - may as well follow these

**Annoyances**<br/>
There are too many to say. Note that I owned the *Fixing Access Annoyances* book. It didn't fix the annoyances, but provided workarounds, but it seemed that I fundamentally couldn't work with raw SQL in Access. On a trivial note, small things like [VBE How to collapse worksheets by default](http://stackoverflow.com/questions/16977441/vbe-how-to-collapse-worksheets-by-default) just nag at you.