## wsl
https://chariotsolutions.com/blog/post/running-linux-on-windows-with-wsl-2-and-a-native-kernel/ | Running Linux on Windows with WSL 2 and a Native Kernel — Chariot Solutions
https://github.com/QMonkey/wsl-tutorial/blob/master/README.wsl2.md | wsl-tutorial/README.wsl2.md at master · QMonkey/wsl-tutorial
https://codeyarns.com/tech/2019-05-12-vcxsrv-x-server-for-windows.html | Code Yarns – VcXsrv X server for Windows
https://www.reddit.com/r/bashonubuntuonwindows/comments/6bpyw8/tip_use_hyperjs_as_terminal_emulator_for_bash_on/ | Tip: Use Hyper.js as Terminal Emulator for Bash On Windows : bashonubuntuonwindows
https://conemu.github.io/en/BashOnWindows.html | ConEmu | Bash on Ubuntu on Windows in ConEmu (WSL)
https://askubuntu.com/questions/1245160/cdo-error-while-loading-shared-libraries-libqt5core-so-5-cannot-open-shared-o | windows - cdo: error while loading shared libraries: libQt5Core.so.5: cannot open shared object file: No such file or directory - Ask Ubuntu
https://superuser.com/questions/1110974/how-to-access-linux-ubuntu-files-from-windows-10-wsl | How to access linux/Ubuntu files from Windows 10 WSL? - Super User
https://stackoverflow.com/questions/60239837/how-do-i-get-the-extensions-that-are-installed-remotely-on-visual-studio-code | windows subsystem for linux - How do I get the extensions that are installed remotely on Visual Studio code - Stack Overflow
https://stackoverflow.com/questions/1098786/run-bash-script-from-windows-powershell | Run bash script from Windows PowerShell - Stack Overflow
https://superuser.com/questions/1110974/how-to-access-linux-ubuntu-files-from-windows-10-wsl/1495497#1495497 | How to access linux/Ubuntu files from Windows 10 WSL? - Super User