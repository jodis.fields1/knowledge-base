## serving from local network
NOTE: maybe better to just reverse tunnel from my own digitalocean server so I can get to it from outside...?

use netsh to forward:
* https://stackoverflow.com/a/66485783/4200039
* also see https://stackoverflow.com/questions/24646165/netsh-port-forwarding-from-local-port-to-local-port-not-working

### netsh
note you can do name=all to see all

had to hit wsl ip addr ( 172.25.83.196 ) directly rather than localhost in connectaddress; then connect to 192.168.1.217
netsh interface portproxy add v4tov4 listenaddress=0.0.0.0 listenport=3000 connectaddress=172.25.83.196 connectport=3000

```powershell
netsh advfirewall set allprofiles state off
netsh advfirewall set allprofiles state on

netsh advfirewall firewall add rule name="Allowing LAN connections" dir=in action=allow protocol=TCP localport=3000
netsh advfirewall firewall show rule name="Allowing LAN connections"
netsh advfirewall firewall delete name="Allowing LAN connections"

netsh interface portproxy add v4tov4 listenaddress=0.0.0.0 listenport=3000 connectaddress=localhost connectport=3000
netsh interface portproxy delete v4tov4 listenport=3000 listenaddress=0.0.0.0

```

## SSHing in
Make sure to install ssh-agent / keychain https://www.funtoo.org/OpenSSH_Key_Management,_Part_2

Gotcha: need to have a local account, not the weird cloud account

https://www.hanselman.com/blog/the-easy-way-how-to-ssh-into-bash-and-wsl2-on-windows-10-from-an-external-machine
* the BEST
* also https://www.hanselman.com/blog/visual-studio-code-remote-development-may-change-everything
* also started at https://stackoverflow.com/a/63781351/4200039

https://superuser.com/questions/1407020/logging-into-windows-10-openssh-server-with-administrator-account-and-public-key

https://superuser.com/questions/1445976/windows-ssh-server-refuses-key-based-authentication-from-client

https://superuser.com/questions/1569241/password-authentication-is-not-working-in-ssh-on-windows/1631635

https://docs.microsoft.com/en-us/windows-server/administration/openssh/openssh_server_configuration?WT.mc_id=-blog-scottha

https://docs.microsoft.com/en-us/windows-server/administration/openssh/openssh_keymanagement
