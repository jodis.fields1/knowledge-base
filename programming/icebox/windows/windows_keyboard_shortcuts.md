https://github.com/bblanchon/disable-windows-keys | GitHub - bblanchon/disable-windows-keys: A tiny application that disables the Windows keys of your keyboard. Very useful in games!
https://superuser.com/questions/670731/how-do-i-disable-windows-k-shortcut-key-on-windows-8 | hotkeys - How do I disable Windows + K shortcut key on Windows 8? - Super User
https://code.visualstudio.com/api/references/when-clause-contexts#available-contexts | when clause contexts | Visual Studio Code Extension API

## disable via registry etc
https://windowsreport.com/disable-keyboard-shortcuts-windows-8/ | How to turn off keyboard shortcuts in Windows 10
https://windowsreport.com/disable-windows-key/ | How to disable the Windows Key in Windows 10 [FULL GUIDE]

## autohotkey
https://www.autohotkey.com/docs/misc/Override.htm | Overriding or Disabling Hotkeys | AutoHotkey
https://www.autohotkey.com/docs/Tutorial.htm#s21 | Beginner Tutorial | AutoHotkey
https://www.autohotkey.com/boards/viewtopic.php?t=63030 | Disable windows button and keep WIN+ combinatios work (common methods doesn't work) - AutoHotkey Community
