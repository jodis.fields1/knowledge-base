Windows general
===

https://marketplace.visualstudio.com/items?itemName=ms-vscode-remote.vscode-remote-extensionpack
* my main interface

### windows remote desktop
see remote desktop


### share files
https://support.apple.com/guide/mac-help/connect-to-a-windows-computer-from-a-mac-mchlp1660/11.0/mac/11.0 | Connect to a Windows computer from a Mac - Apple Support
https://support.apple.com/guide/mac-help/set-up-windows-to-share-files-with-mac-users-mchlp1659/mac | Set up Windows to share files with Mac users - Apple Support
https://support.apple.com/guide/mac-help/share-mac-files-with-windows-users-mchlp1657/mac | Share Mac files with Windows users - Apple Support

### Codes
Microsoft Office Professional 2010: C8WP3FHCX8W2FKH3VHWJJ3Q64

Confirmation number: 2003075857

```
Windows 7:
Product Part No.: X15-39644
Installed from 'Full Packaged Product' media.
Product ID: 00426-292-7202532-85507  match to CD Key data
CD Key: 6K3R2-QQMHW-RH993-8C9KW-2QXBT
```

Case 1011473105

Windows XP: must be US-OFN309-53590-7BO-1989

## Batch / Shell

[Preventing a MS-DOS window from automatically closing in Windows](http://www.computerhope.com/issues/ch000738.htm) - call cmd.exe /K from the command-line or insert a pause statement

[Batch file with changing drive letters ](http://superuser.com/questions/581744/batch-file-with-changing-drive-letters)

[What does %~dp0 mean, and how does it work?](http://stackoverflow.com/questions/5034076/what-does-dp0-mean-and-how-does-it-work) - re-read this in 2014 a few years later

[Unblock CMD at school and become the administrator!](http://www.instructables.com/id/Unblock-CMD-at-school-and-become-the-administrator/)

[http://ss64.com](http://ss64.com) - has lots of different stuff

[Table testing](http://888b6538-c870-49c9-b529-ca28716e0428) and 

[PowerShell script in a .bat file](http://dmitrysotnikov.wordpress.com/2008/06/27/powershell-script-in-a-bat-file/) (2008) - basically it is not super-easy
