A few days ago I added a new exception (error message) to the `homebrew-cask` project as a step towards clarifying the status of one of their "chief bugs". Dynamic, non-compiled languages such as Ruby, Python, and Javascript make it relatively easy to contribute compared to the common C++ programs of the Linux world.[^n] However, the path towards doing something like this is still not really clear to beginners, so let me share my (admittedly crude) methods. 

###### Check for advice
Most open-source projects will have some documentation. I actually forgot to check this when I started, and wish I had. In general, however, the advice you'll find is minimal: I rarely see an architectural overview.[^n] In the case of homebrew-cask's [hacking.md](https://github.com/caskroom/homebrew-cask/blob/master/doc/hacking.md), the advice just tells you how to set up a development version of cask to hack on without messing with the version you're relying on. Basic, but helpful. Luckily, the locally installed version is a git repo and I did a quick `git checkout *` to roll back my changes.

###### Find the entry point
First off, it helps to figure out the program's entry point. In this case, it was a command-line argument: brew cask. This starts with brew. So let's type `which brew` and find the program. In this case it was a shell (bash) script located at `/usr/local/bin/brew`. So we check that out using our favorite text editor, in my case Sublime Text: `subl /usr/local/bin/brew`.

###### Log out interesting data
At this point, you might be confused by the code. My most important piece of advice: *learn to **log out data** to your console or whatever it is you're working with*. In bash, you can use `echo`, and add double quotes and a dollar sign to expand variables: `echo "$BREW_LIBRARY_DIRECTORY"` for example. Ruby has `puts`,[^n] Javascript has `console.log`, and Python has `print`. Beginners rarely seem to hear that much of the work of programming gets done with something as basic as typing out print commands.[^n]

Code is a bunch of functions calling other functions, and if you can follow the steps of those functions, you can figure out the program. So throw logging statements in the functions as you go and see where it takes you.

In this case, after I found myself in the bash file `brew-cask`, I echoed out some of the variables and found that it was ultimately calling `/usr/local/Cellar/brew-cask/0.54.1/rubylib/brew-cask-cmd.rb`.

###### Figure out how module loading works
At this point, all you see is something about `$LOAD_PATH` and some `require` statements followed by a single function call. Require statements are another piece of the puzzle: you need to understand how modules are located. In Ruby, a require statement means to search the `$LOAD_PATH` and include the matching code. In Python, usually modules are looked for in directories listed in `sys.path`, including the current directory and `PYTHONPATH` (note: quite complex, see [Traps for the Unwary in Python’s Import System](http://python-notes.curiousefficiency.org/en/latest/python_concepts/import_traps.html)). In the Javascript CommonJS module system, npm searches `node_modules` upwards from the current directory, or an absolute path is specified directly.

In this case, the `$LOAD_PATH.unshift(File.expand_path('..', Pathname.new(__FILE__).realpath))` adds the directory above the current command to the `$LOAD_PATH`. I look in that directory (`ls /usr/local/Cellar/brew-cask/0.54.1/rubylib/`), find the file `hbc/cli.rb`, and add a `put` to confirm that it is entered where I think it is from. From there, I continued to dive in with `put` statements similar to how I'd done before.

###### Experiment with syntax
When I dived into homebrew-cask, it had been a couple years since I'd really looked at Ruby, as I went down the Python route due to some maintenance issues I found in  some of Ruby's main websites. So while I was doing this I experimented with adding code based on the existing syntax but I also had to check Ruby docs for various syntax basics. It just takes time, persistence, and fiddling.

###### Be fast
These things all take patience and time, but significantly less time if you know your keyboard shortcuts. Over time, you should be able to skip to the beginning of a line, skip words (both including slashes and not), copy a line, move lines, and so on. I will try to write a post about the tricks I use.
[^n]: I spent a nontrivial amount of time exploring programs in Linux and fighting things like cmake, qmake, and kbuildsycoca4. 
[^n]: I started [a repo](https://github.com/jcrben/code-summaries) where I try to provide these but didn't get too far.
[^n]: Ruby has also *print* but puts adds a newline.
[^n]: You can also use a graphical program which shows you all the values known as an integrated development environment (IDE), but it's not always easy to get these to work right, and in particular, although there's the expensive IntelliJ RubyMine, I've never gotten a free Ruby IDE to work. 