How to learn new codebases / architecture?

There's a hackernews thread somewhere?

2017 - [Ask HN: How to understand the large codebase of an open-source project?](https://news.ycombinator.com/item?id=16299125)

Look for verbose logging options.
Step through critical path.
Make sure you can jump to definition, etc quickly (see https://www.gnu.org/software/global/links.html)

Take a glance at Architecture of Open Source Applications.