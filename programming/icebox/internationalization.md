over time this is becoming more and more solved

[Launch HN: Lang (YC S19) – Internationalization Built for Devs](https://news.ycombinator.com/item?id=20627137)

Tamas recommands https://messageformat.github.io/Jed/ and gettext

TODO: [mirego/accent](https://github.com/mirego/accent)
TODO: https://www.i18next.com/overview/configuration-options

## javascript
https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/toLocaleUpperCase

## edge cases
Turkish I-problem - see my Wikipedia edit to Turkish alphabet

## case folding
tl;dir does not matter, need unicode normalization
https://stackoverflow.com/a/26895380/4200039
https://www.w3.org/International/wiki/Case_folding


## discussion / exploration
[Ask HN: Do you internationalize/localize your apps?](https://news.ycombinator.com/item?id=15625675) - ??

https://github.com/alibaba/react-intl-universal - suggested by below

[Internationalization in React](https://medium.freecodecamp.org/internationalization-in-react-7264738274a0) - by Preethi
[Why has nobody built a front-end internationalization platform — or has someone?](https://www.quora.com/Why-has-nobody-built-a-front-end-internationalization-platform-%E2%80%94-or-has-someone)


**Internationalization**

[http://userguide.icu-project.org/](http://userguide.icu-project.org/)
Globalize
comparison: [http://rxaviers.github.io/javascript-globalization/](http://rxaviers.github.io/javascript-globalization/)

can't get name of currency without kind of a hack: [https://github.com/globalizejs/globalize/issues/605](https://github.com/globalizejs/globalize/issues/605)

JavaScript Globalization Overview - [couple years old](http://rxaviers.github.io/javascript-globalization/)

## old-school
* https://en.wikipedia.org/wiki/Gettext from GNU:
* https://phraseapp.com/blog/posts/learn-gettext-tools-internationalization/