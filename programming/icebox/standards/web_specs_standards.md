
## standards bodies
http://en.tldp.org/HOWTO/Unix-and-Internet-Fundamentals-HOWTO/ - foundational; read the first bit
### ecma tc39 javascript
[https://esdiscuss.org/](https://esdiscuss.org/) - seems more maintained

[http://tc39wiki.calculist.org/about/people/](http://tc39wiki.calculist.org/about/people/)

[http://es5.github.com/](http://es5.github.com/) (annotated version) is not much practical help; also see http://wiki.whatwg.org/wiki/Web_ECMAScript

http://wiki.ecmascript.org/doku.php has an ongoing Harmony project

Guide to reading specifications http://www.alistapart.com/articles/readspec

## web standards
whatwg and w3c overlap and disagree, see [their W3C complaints](https://wiki.whatwg.org/wiki/W3C)

### whatwg
### w3c
[REVISING W3C PROCESS COMMUNITY GROUP](https://www.w3.org/community/w3process/)

[Web Platform Publication Status (PubStatus)](https://www.w3.org/WebPlatform/WG/PubStatus)
[World Wide Web Consortium Process Document](https://www.w3.org/2018/Process-20180201/)
Advisory Board https://www.w3.org/2002/ab/ - met Tantek, how much influence does this have?

### DOM Specifications
WHATWG is authoritative; differences: 
* [HTML5: W3C vs WHATWG. Which gives the most authoritative spec?](https://stackoverflow.com/questions/6825713/html5-w3c-vs-whatwg-which-gives-the-most-authoritative-spec)
* [W3C vs. WHATWG HTML5 Specs – The Differences Documented](https://developer.telerik.com/featured/w3c-vs-whatwg-html5-specs-differences-documented/)
* [W3C vs. WHATWG HTML5 Specs - The Differences Documented](https://dzone.com/articles/w3c-vs-whatwg-html5-specs)
* [Differences between the W3C HTML 5.2 specification and the WHATWG LS](https://www.w3.org/wiki/HTML/W3C-WHATWG-Differences)

[https://dom.spec.whatwg.org/](https://dom.spec.whatwg.org/) - has a github page!

MDN's DOM page does not include a link to [https://developer.mozilla.org/en/Mozilla_DOM_Hacking_Guide](https://developer.mozilla.org/en/Mozilla_DOM_Hacking_Guide)

**ECMAScript standard (harmonization)**

*Notes*

slightly better idea of what eval() means based on http://es5.github.com/#x10 page 50 of ECMA-262 5.1 Edition / June 2011

function stuff around page 120 or http://es5.github.com/#x15.3.4 is sorta interesting

The ECMAScript spec draws on formal language theory, context-free grammars, and that sort of stuff - impossible to understand, for example, Section 5 without understanding some of the jargon related to terminal and nonterminal symbols and such. Unfortunately, even the Wikipedia pages on these topics are somewhat impenetrable - for example, the example on http://en.wikipedia.org/wiki/Terminal_and_nonterminal_symbols raises the question of why single digits are terminal when they can obviously be added to other numbers to create new numbers... - see http://www.antlr.org/wiki/display/ANTLR3/Quick+Starter+on+Parser+Grammars+-+No+Past+Experience+Required for good related intro and also took a look at

Lexical and Syntactic Analysis at http://www.nku.edu/~foxr/CSC407/NOTES/ch4.ppt

**HTTP**

http://tools.ietf.org/html/rfc2616 is the authoritative document on HTTP
https://tools.ietf.org/html/rfc3986#section-3.2.2 - URLs

Per [Why trailing slashes on URIs are important](https://cdivilly.wordpress.com/2014/03/11/why-trailing-slashes-on-uris-are-important/) points to [RFC 3986](http://tools.ietf.org/html/rfc3986#section-5.2.3)