**External APIs**

[http://cloud-elements.com/pricing/](http://cloud-elements.com/pricing/) -

Maybe MuleSoft?

**Segment**

Primary abstraction layer

[http://stackoverflow.com/questions/tagged/segment-io](http://stackoverflow.com/questions/tagged/segment-io)

**Intercom**

[http://stackoverflow.com/questions/tagged/intercom](http://stackoverflow.com/questions/tagged/intercom)

[https://developers.intercom.com/docs/intercom-javascript](https://developers.intercom.com/docs/intercom-javascript)

[https://developers.intercom.com/reference](https://developers.intercom.com/reference)

[https://docs.intercom.com/configure-intercom-for-your-product-or-site/customize-the-intercom-messenger/customize-the-intercom-messenger-technical](https://docs.intercom.com/configure-intercom-for-your-product-or-site/customize-the-intercom-messenger/customize-the-intercom-messenger-technical) -

*Segment* - [https://segment.com/docs/integrations/intercom/](https://segment.com/docs/integrations/intercom/)

*Companies* - need to use this feature [https://docs.intercom.com/configure-intercom-for-your-product-or-site/customize-intercom-to-be-about-your-users/group-your-users-by-company](https://docs.intercom.com/configure-intercom-for-your-product-or-site/customize-intercom-to-be-about-your-users/group-your-users-by-company)
