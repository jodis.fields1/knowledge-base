# RSS
Major long-term interest but haven't dove into it:

Pipedream https://twitter.com/tod/status/1252326197322645504

[It's time to head back to RSS?](https://news.ycombinator.com/item?id=16721690)
* [Atom vs. RSS](http://nullprogram.com/blog/2013/09/23/)
    * best elaboration on benefits

## lack thereof
* page2rss
* kill-the-newsletter

### 2018-03-15 RSS general / tracking subscribers / auth #linkdump

#### principles
https://en.wikipedia.org/wiki/RSS | RSS - Wikipedia
https://github.com/TryGhost/Ghost/issues/2263 | RSS feed structure audit · Issue #2263 · TryGhost/Ghost
https://tools.ietf.org/html/rfc5005#section-3 | RFC 5005 - Feed Paging and Archiving

#### authentication
https://wordpress.stackexchange.com/questions/19427/options-for-authenticated-feeds | authentication - Options for authenticated feeds - WordPress Development Stack Exchange
https://github.com/adamfranco/private-feed-keys | adamfranco/private-feed-keys: This WordPress plugin allows subscription to RSS feeds on private blogs that require authentication. Works with "More Privacy Options" on multi-site installs.
https://github.com/TryGhost/Ghost/issues/9001 | RSS feeds for private blogs · Issue #9001 · TryGhost/Ghost

#### track subscribers
https://stackoverflow.com/questions/3327677/standard-and-reliable-way-to-track-rss-subscribers | Standard and reliable way to track RSS subscribers? - Stack Overflow

https://www.quora.com/Is-it-possible-to-extract-a-RSS-feed-from-a-Flipboard-magazine/answer/Tom-Dings | Tom Dings's answer to Is it possible to extract a RSS feed from a Flipboard magazine? - Quora