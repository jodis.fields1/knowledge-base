TODO: merge with interactive_notebooks.md

https://informationisbeautiful.net/2020/learn-to-do-dataviz-in-your-pyjamas

https://en.wikipedia.org/wiki/Information_visualization
* note in particular https://en.wikipedia.org/wiki/Treemapping

https://github.com/nowthis/sankeymatic - http://sankeymatic.com/ flow diagrams

metabase.com/ - was thinking about doing this at work

http://sankeymatic.com/build/ - someone at Hackreactor used this


https://flowingdata.com/membership/  

also has the cool http://your.flowingdata.com/faq/  


https://github.com/getredash/redash

## principles
http://dataviz-literacy.wmflabs.org/ - from https://flowingdata.com/2018/02/23/beginners-guide-to-visualization-literacy/ (Luke Davis)

## jupyter version control
TODO: start using jupyter notebook and see [Notebook normalization for version control](https://github.com/jupyter/help/issues/174)

[How to Version Control Jupyter Notebooks](https://news.ycombinator.com/item?id=18740197)