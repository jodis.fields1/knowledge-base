
## libs

### lightweight charts
https://github.com/tradingview/lightweight-charts/blob/master/src/api/ichart-api.ts


### plotly
https://plotly.com/javascript/is-plotly-free/ | Is Plotly.js Free? | JavaScript | Plotly
https://en.wikipedia.org/wiki/Comparison_of_JavaScript_charting_libraries | Comparison of JavaScript charting libraries - Wikipedia
https://github.com/zingchart/awesome-charting | zingchart/awesome-charting: A curated list of the best charting and dataviz resources that developers may find useful, including the best JavaScript charting libraries
https://plotly.com/javascript/candlestick-charts/ | Candlestick Charts | JavaScript | Plotly
https://github.com/topics/tradingview-charting-library | tradingview-charting-library · GitHub Topics

## old
[Plottable.js – Flexible, interactive charts for the web ](https://news.ycombinator.com/item?id=12429959)

## highcharts
Highcharts:

### API version
in the API, see the version number at the top right - e.g. "Since 4.1.0"

There's options, namespaces (just one - Chart), and Classes

see https://forum.highcharts.com/highcharts-usage/older-version-api-t39739/ for how to access the older API

Highcharts.charts has a bunch of undefineds - https://github.com/highcharts/highcharts/issues/2431

### concepts
too much!!

#### axes
ticks are just the freakin little markers - but sometimes they can be finicky about positioning
tickPositioner?
