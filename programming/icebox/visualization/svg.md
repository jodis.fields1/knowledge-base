## quick SVG cheatsheet
Nate Murray from newline had a good intro:
![](2019-10-15-12-17-35.png)


Which is drawn by this path element:

<path d="M 0 0 L 100 0 L 100 100 L 0 50 Z" />
So... WTF is going on in that d attribute?

The d (for data, clever) attribute is a list of commands:

M 0 0 will Move to the (x,y) point (0,0)
L 100 0 will draw a Line to (100, 0)
L 100 100 connects the Line to (100, 100)
L 0 50 connects to (0, 50)
Z connects line back to the first point

## SVG, with a side of XML

*Update to discuss HTML5 and SVG integration*

This is more of a theoretical, rambling aside on SVG and XML, so if you're looking to conjure you may just want to skip along. This will also be review for most designers and many developers. 

I'm guessing that many modern web developers start getting into SVG when they encounter D3, as I did. D3 makes SVG amazingly easy to work with, but underneath is a complex XML-based vector graphics specification that I wanted to understand.

SVG elements are contained inside of an XML namespace. The best introduction I've found is [Namespaces Crash Course](https://developer.mozilla.org/en-US/docs/Web/SVG/Namespaces_Crash_Course) on MDN, which is mostly unchanged from its original 2006 draft. Summarized briefly, SVG needs to live inside a root tag which defines a namespace. For example, `<svg xmlns="http://www.w3.org/2000/svg">` is the root, and all descendants are then in that namespace and interpreted according to the rules of SVG.[^n] This feels weird and hearks back to when XML and XHTML were the rage.[^n]

Back to SVG: once you start using the native DOM interface, you'll find that it differs in strange ways. Instead of `createElement`, you must use `createElementNS`, passing in the namespace URI (see above example) as the first parameter. But to set and get attributes which don't have a namespace prefix (typically everything but the `xlink:href` attribute), you pass in `null`. More likely you'll let D3 do the heavy lifting, though.

SVG itself stands for scalable vector graphics. Vector graphics are drawn with mathematical formulas which can be viewed in plaintext, as opposed to bitmaps like Canvas (similar to JPEG photo files) which are pure binary. SVG's reach extends out from the web: Inkscape is an open-source vector graphics editor based on SVG, and Sketch is also a vector graphics editor which can export to SVG. Vector graphics ain't going away, and the scalable part makes them quite handy for responsiveness (I'm eyeing [caniuse SVG favicons](http://caniuse.com/#feat=link-icon-svg)).

At the heart of SVG is its handling of graphics: shapes, colors, animations, and so on. Filters, strokes, and gradients are not the typical developer's specialty. This may have to wait for another post.

[^n]: The 'why' of XML namespacing is deeply explained in a 2003 book chapter [A Ten-Minute Guide to XML Namespaces](http://books.xmlschemata.org/relaxng/relax-CHP-11-SECT-1.html), but basically it prevents global variable collision.
[^n]: XML feels arcane and mysterious, and I've spent more time than I'd like to admit puzzling over complex specifications such as XBRL. [This thread](http://programmers.stackexchange.com/questions/61198/if-xml-is-so-bad-why-do-so-many-people-use-it) has some good practical perspectives. Note that XML is still a thing even if HTML5 is not XML: for example, ReactJS JSX is XML-like, meaning that you have to type &lt;input/&gt;, not &lt;input&gt;. XML config files are ubiquitous, the U.S. SEC's open data format uses the related XBRL, and the de facto web standards body WHATWG has a page [HTML vs. XHTML](https://wiki.whatwg.org/wiki/HTML_vs._XHTML) which discusses serving up "polyglot" documents which meet both HTML5 and XML standards. See also [Is XHTML5 dead or is it just an synonym of HTML5?](http://programmers.stackexchange.com/questions/149839/is-xhtml5-dead-or-is-it-just-an-synonym-of-html5). In addition, W3C released an update to the [Resource Description Framework](https://en.wikipedia.org/wiki/Resource_Description_Framework) (RDF) specification in 2014. I met a Stanford data scientist a few weeks ago who is working with medical data sets stored as graphs in the RDF XML format, although according to Wikipedia RDF can be stored in non-XML formats, particularly with the 2014 update.