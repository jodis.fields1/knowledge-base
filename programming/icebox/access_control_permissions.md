Packages:

* [RailsApps/rails-devise-roles](https://github.com/RailsApps/rails-devise-roles) - 
* [Role-based authorization in Rails](https://blog.chaps.io/2015/11/13/role-based-authorization-in-rails.html) - 
* [Yii Authentication and Authorization](http://www.yiiframework.com/doc/guide/1.1/en/topics.auth) - 
* [Entrust Role-based Permissions for Laravel 5](https://github.com/Zizaco/entrust) - 
* [kristianmandrup/cantango](https://github.com/kristianmandrup/cantango) - CanCan extension with role oriented permission management, rules caching and much more
* [Rails 4 user roles and permissions](http://stackoverflow.com/questions/25272911/rails-4-user-roles-and-permissions) - 
* [How to make Role-Based Access Control in PHP](https://sevvlor.com/post/2014/10/14/how-to-make-role-based-access-control-in-php/) - 