TODO: copy over Google doc

https://github.com/kubaue/React-TDD - largely my approach

## testing css
Resolution: test for class names existence nonexistence

https://github.com/facebook/jest/issues/3094 | Jest doesn't works with JSX which imports CSS · Issue #3094 · facebook/jest
https://github.com/justinsisley/Jest-CSS-Modules | justinsisley/Jest-CSS-Modules: Jest processor that prevents CSS module parse errors
https://stackoverflow.com/questions/39641068/jest-trying-to-parse-css | javascript - jest trying to parse .css - Stack Overflow
http://ianmcnally.me/blog/2017/8/2/a-better-solution-for-mocking-css-modules-in-jest | A better solution for mocking CSS modules in jest — Ian McNally
https://stackoverflow.com/questions/10318330/how-do-you-ad-stylesheets-to-jsdom | css - How do you ad stylesheets to JSDOM - Stack Overflow


## axios / nock flushing
https://github.com/ctimmerm/axios-mock-adapter

https://github.com/facebook/jest/issues/2157 | Provide an API to flush the Promise resolution queue · Issue #2157 · facebook/jest
https://github.com/node-nock/nock/issues/950 | How to know when all intercepters have been called · Issue #950 · node-nock/nock
https://github.com/axios/axios/issues/1180 | detect jest and use http adapter instead of XMLhttpRequest · Issue #1180 · axios/axios
https://github.com/request/request-promise/issues/147 | Error: read ECONNRESET · Issue #147 · request/request-promise
https://stackoverflow.com/questions/16995184/nodejs-what-does-socket-hang-up-actually-mean | node.js - NodeJS - What does "socket hang up" actually mean? - Stack Overflow
https://github.com/facebook/jest/issues/5737 | Unmocking axios? I get 'the string did not match the expected pattern' · Issue #5737 · facebook/jest
https://github.com/node-nock/nock/issues/699 | Network Error when using nock with axios · Issue #699 · node-nock/nock
https://stackoverflow.com/questions/42677387/jest-returns-network-error-when-doing-an-authenticated-request-with-axios | javascript - Jest returns "Network Error" when doing an authenticated request with axios - Stack Overflow
