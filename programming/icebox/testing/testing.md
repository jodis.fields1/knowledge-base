## 2018
puppeteer is leading:
* [Why Puppeteer is Better than Selenium](https://www.lucidchart.com/techblog/2018/08/08/why-puppeteer-is-better-than-selenium/)
* puppeteer-browser for cross-browser https://github.com/GoogleChrome/puppeteer/issues/1667#issuecomment-397319888

## 2017
[Is Functional Programming a viable alternative to dependency injection patterns?](https://softwareengineering.stackexchange.com/questions/275891/is-functional-programming-a-viable-alternative-to-dependency-injection-patterns)

[How to mock the imports of an ES6 module?](https://stackoverflow.com/questions/35240469/how-to-mock-the-imports-of-an-es6-module) - general non-jest mocking
[Improve ability to stub/mock exports of ES2015 modules transpiled by Babel (T6748)](https://github.com/babel/babel/issues/3811) - some plugins at the bottom

## 2016
[http://blog.trevoke.net/blog/2016/04/26/dependency-injection/](http://blog.trevoke.net/blog/2016/04/26/dependency-injection/) - by my old IRC acquaintance, mentions verifying doubles - *Bring up with Chris this pattern*

[How we test client-side Javascript: Setup and patterns](http://informatics.unep-wcmc.org/post/86302609060/how-we-test-client-side-javascript-setup-and) - 

If there is a database, user a 'stub' or a 'mock'

## Docker and E2E tests

[http://gregoryszorc.com/blog/2014/10/16/the-rabbit-hole-of-using-docker-in-automated-tests/](http://gregoryszorc.com/blog/2014/10/16/the-rabbit-hole-of-using-docker-in-automated-tests/)

## Todo

[http://www.peterprovost.org/blog/2012/05/02/kata-the-only-way-to-learn-tdd/](http://www.peterprovost.org/blog/2012/05/02/kata-the-only-way-to-learn-tdd/)

[The Behaviour Specification Handbook](https://devzone.channeladam.com/library/bdd-behaviour-specification-handbook/) - 

[TDD best practices from Enterprise Craftsmanship](http://enterprisecraftsmanship.com/2015/08/03/tdd-best-practices/) - awesome series

## Running relevant tests

[https://www.npmjs.com/package/test-creep](https://www.npmjs.com/package/test-creep)

[http://blogs.atlassian.com/2008/11/stop_testing_so_much/](http://blogs.atlassian.com/2008/11/stop_testing_so_much/)

## Exploration 2015

Tried watching James Shore's [Let's Play TDD](https://www.youtube.com/watch?v=CdCNJYKqmi0) (ended at #4) on Youtube

## Todo

Wallaby.js - look into it

TestDouble - [https://github.com/testdouble](https://github.com/testdouble) has good resources

## General

[Mocks Aren't Stubs](http://martinfowler.com/articles/mocksArentStubs.html) (2007) - seems to be a classic from Martin Fowler

## E2e

[End-to-End JavaScript Testing is Easy (the minimal way to do it)](http://blog.strafenet.com/2014/07/03/end-to-end-javascript-testing-is-easy-the-minimal-way-to-do-it/) - best short and

simple overview

[nightwatch (plus page objects)](http://nightwatchjs.org/guide#page-objects) - major up and comer

WebdriverIO - sexy wrapper around Selenium

[Why unit testing is a waste of time](https://news.ycombinator.com/item?id=681503) - agreed that tests should clear out data

*Selenium*

For Java, you need everything in [this answer](http://stackoverflow.com/a/5134972/4200039)

Project has more energy than initially realized: see api at [http://seleniumhq.github.io/selenium/docs/api/javascript/](http://seleniumhq.github.io/selenium/docs/api/javascript/) (ignore the broken googlecode one). 

*Desktop* - Sikuli (see [review](http://teotti.com/sikulix-automated-testing-with-image-recognition/)) seems to be main one

## Casperjs and phantomjs

[Using A Remote Debugger With CasperJS and PhantomJS](https://drupalize.me/blog/201410/using-remote-debugger-casperjs-and-phantomjs) - sweet!! also [You can see what casperJS is doing with this little hack](https://www.codementor.io/tips/4678317992/you-can-see-what-casperjs-is-doing-with-this-little-hack)

[Front-end Testing for the Lazy Developer with CasperJS](http://www.helpscout.net/blog/functional-testing-casperjs/) - remember to use .then

[Page Object pattern with CasperJS](http://jsebfranck.blogspot.fr/2014/03/page-object-pattern-with-casperjs.html) - page objects! also see [Using Page Objects to Overcome Protractor's Shortcomings](https://www.thoughtworks.com/insights/blog/using-page-objects-overcome-protractors-shortcomings)

## Resources
read Test-Driven Development by Christian Johansen

## Test runners
[Can Protractor and Karma be used together?](http://stackoverflow.com/questions/17070522/can-protractor-and-karma-be-used-together) - no!

## Types of tests
Supertest - HTTP assertions from Mocha author

Frisby.js - API testing framework based on Jasmine

Mutation testing - [Humbug for PHP](https://github.com/padraic/humbug) and [jimivdw/grunt-mutation-testing](https://github.com/jimivdw/grunt-mutation-testing) for JS

Code coverage - Blanket.js (server-side), Instanbul (integrates with karma), Coveralls.io (hosting)