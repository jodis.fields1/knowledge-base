https://dmitripavlutin.com/what-every-javascript-developer-should-know-about-unicode/

[The Python Unicode Mess](http://changelog.complete.org/archives/9938-the-python-unicode-mess)

### 2017
https://chardet.readthedocs.io/en/latest/usage.html - solid Python lib
mass convert with find and vim (see `cheat -e find`)

**2016** (C investgation)

Reading graphemes is what you want, and that's difficult in any language as noted in 

[Why does C++ seem to pretend Unicode doesn't exist?](https://m.reddit.com/r/cpp/comments/1y3n33/why_does_c_seem_to_pretend_unicode_doesnt_exist/)

Note that Java supports counting graphemes explicitly using BreakIterator as noted in [The 7 Ways of Counting Characters](http://developers.linecorp.com/blog/?p=3473)

[C programming: How to program for Unicode?](http://stackoverflow.com/questions/526430/c-programming-how-to-program-for-unicode) -

good advice to wrap ICU

[How well is Unicode supported in C++11?](http://stackoverflow.com/questions/17103925/how-well-is-unicode-supported-in-c11) - terribly

**2015**

http://nedbatchelder.com/text/unipain.html - watch this video first; seems to contradict [http://lucumr.pocoo.org/2011/12/7/thoughts-on-python3/](http://lucumr.pocoo.org/2011/12/7/thoughts-on-python3/)

[What is the difference between encode/decode?](http://stackoverflow.com/questions/447107/what-is-the-difference-between-encode-decode/448383#448383) says encoded means the raw numbers; see also [What Every Programmer Absolutely, Positively Needs To Know About Encodings And Character Sets To Work With Text](http://kunststube.net/encoding/) for PHP encoding: strings are handled as bytes so string[0] could cut a character in half if you don't use the mb_* lib

2014-11: confused by this again while reading yergler's  [Bytes, Characters, Codecs, and Strings](http://yergler.net/2012/bytes-chars/) and finally got it drilled into my brain on IRC that Unicode is the codepoints and not the bytes while a byte "string" is just literal bytes! also explains why encoding results in bytes while decoding results in unicode! also looked at  [Processing Text Files in Python 3](http://python-notes.curiousefficiency.org/en/latest/python3/text_file_processing.html) from Coghlan and  [Unicode for Dummies](http://pythonconquerstheuniverse.wordpress.com/2012/02/01/unicode-for-dummies-encoding/)

[Everything you did not want to know about Unicode in Python 3](http://lucumr.pocoo.org/2014/5/12/everything-about-unicode/) - sorta wondered what would happen to 8-bit characters based on the statement "Any 8 bit byte based content can pass through just fine, but you are following the contract with the operating system that any character processing will be limited to the first 7 bit" but it appears they will be left as blank spaces or maybe question marks per above. Started me on a unicode in Linux blitz from which I found  [Introduction to Unicode — Using Unicode in Linux](http://michal.kosmulski.org/computing/articles/linux-unicode.html) (2004) and  [The Unicode HOWT](http://www.tldp.org/HOWTO/Unicode-HOWTO.html) (2001) by Haible