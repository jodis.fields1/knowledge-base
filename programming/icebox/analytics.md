# analytics

## new
Launch HN: PostHog (YC W20) – open-source product analytics
* Segment competitor

## old
TODO: snowplow analytics https://github.com/snowplow/snowplow

**Analytics** -

TODO: heap analytics, amplitude or mixpanel free versions to understand how users flow

Identifying and Filtering Internal Traffic from Google Analytics (2015) Internalize Chrome extension as well as Google's official [Google Analytics Opt-out Browser Add-on](https://tools.google.com/dlpage/gaoptout)

*Tag manager* -

Had to add the Google Tag Manager to track outbound clicks! See intro at [Google Tag Manager for Web Tracking](https://developers.google.com/tag-manager/quickstart); to really learn do the [Analytics Academy](https://analyticsacademy.withgoogle.com/)

[Google Analytics Events for v2](https://support.google.com/tagmanager/answer/6106716?hl=en) helped me

[Stop spam from free-social-buttons, floating-share-buttons.com](http://www.ohow.co/block-social-buttons-simple-share-buttons-referral/) - apparently these domains spam you so that you'll click on them, odd

[Getting To Grips With Google Tag Manager](https://www.distilled.net/blog/getting-started-with-google-tag-manager/) (2014) - seems to be a reasonable start, but macros are deprecated!!

[Variable Guide For Google Tag Manager](http://www.simoahava.com/analytics/variable-guide-google-tag-manager/) (2015) - covers the new one
