# mobile development
[What's the smallest cell phone width for responsive design?](https://www.reddit.com/r/web_design/comments/3s5hnp/whats_the_smallest_cell_phone_width_for/)
    * 320px is smallest width phone

## html / css gotchas
[CSS3 100vh not constant in mobile browser](https://stackoverflow.com/questions/37112218/css3-100vh-not-constant-in-mobile-browser)
    * scrollbar gets in the way

## android dev
`brew cask info caskroom/cask/android-sdk` - install this

## 2018-08 AMP pages
these were annoying me on vacation, still can't figure out how to disable them effectively

https://www.androidpolice.com/2018/05/23/opinion-google-amp-still-confusing-not-getting-better/

## react native 2018-07-01
https://news.ycombinator.com/item?id=17315893 | My problem with React Native is lack of proper ES6 support on Android. The issue... | Hacker News
https://github.com/facebook/react-native/issues/11232#issuecomment-375426983 | the Proxy can not use on Android but iOS is ok · Issue #11232 · facebook/react-native
https://news.ycombinator.com/item?id=17317799 | React Native is hot garbage. I work on a commercial project and it's unbelievabl... | Hacker News
https://www.google.com/search?hl=en&q=progressive%20web%20app%20instead%20of%20react%20native | progressive web app instead of react native - Google Search
https://www.progress.com/blogs/choose-between-progressive-web-apps-react-native-nativescript-2018 | Choosing Mobility Options in 2018
https://medium.com/snapp-mobile/why-starting-a-new-android-project-with-java-is-a-bad-idea-359bffe0bbd6 | Why starting a new Android project with Java is a bad idea


## 2018-05
custom domains reuqire a proxy to sync the mobile devtools, sadly...
https://stackoverflow.com/questions/18571213/how-can-i-force-a-hard-reload-in-chrome-for-android | How can I force a hard reload in Chrome for Android - Stack Overflow
https://stackoverflow.com/questions/5794984/how-to-debug-web-sites-on-mobile-devices | debugging - How to debug web sites on mobile devices? - Stack Overflow
https://developers.google.com/web/tools/chrome-devtools/remote-debugging/local-server | Access Local Servers  |  Tools for Web Developers  |  Google Developers


## old

https://mydevice.io/devices/

https://onsen.io/ ?