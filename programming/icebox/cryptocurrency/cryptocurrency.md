[Ask HN: A good primer on cryptocurrencies?](https://news.ycombinator.com/item?id=14659775)

## people
https://twitter.com/CryptoKaleo

https://twitter.com/pierre_rochard - muted

### filecoin / ipfs
Seems they are doing good work!

https://github.com/ipfs | IPFS
https://discuss.ipfs.io/t/filecoins-for-the-little-guys/806/4 | Filecoins for the little guys? - Ecosystem - discuss.ipfs.io
https://www.reddit.com/r/filecoin/comments/8t01hh/price_of_filecoin/ | Price of Filecoin : filecoin
https://filecoin.io/ | https://filecoin.io


### Random
https://en.wikipedia.org/wiki/Blockstream - Rusty Russell works there

https://github.com/hyperledger/hyperledger - from Intel

https://copay.io/ - ??

#### dock.io
sort of sounded promising, but now not seeming so great:
https://www.reddit.com/r/dockio/comments/8b8x2s/how_is_dockio_different_from_linkedin/

### Types
https://techcrunch.com/2017/11/19/100-cryptocurrencies-described-in-4-words-or-less/
https://xmr.to/ - monero anonymous 
https://www.antshares.org/ - strong backing in China


#### Ethereum
according to https://www.reddit.com/r/ethereum/comments/5izcf5/lets_talk_about_the_projected_coin_supply_over/ it will cap out eventually

eventually plan to switch to https://github.com/ethereum/wiki/wiki/Proof-of-Stake-FAQ
