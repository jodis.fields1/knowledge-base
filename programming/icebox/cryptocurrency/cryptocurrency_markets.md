https://medium.com/@nic__carter/a-glimpse-into-the-dark-underbelly-of-cryptocurrency-markets-d1690b761eaf | A glimpse into the dark underbelly of cryptocurrency markets - makes sense

https://techcrunch.com/2018/09/02/the-collapse-of-eth-is-inevitable/ - a bit too much inside baseball for me to follow

## stocks
https://twitter.com/KushMansPicks/status/1380696641590726656 - big list

## shorting
https://mobile.twitter.com/iTradeContracts/status/1394006225331707905 - KOT4x then link mt4

apparently Kraken altho it sounds like the fees are absurdly high (1% of trade??) per https://mobile.twitter.com/shaolintrader88/status/1416386431648518148
* https://bitcoin.stackexchange.com/questions/93812/opening-a-short-position-on-kraken-requires-btc-to-begin-with

## Website tools
* https://messari.io/
* https://dune.xyz/home
* https://www.skew.com/

https://www.reddit.com/r/Python/comments/7m18d0/opensource_cryptocurrency_portfolio_manager/ | Open-source cryptocurrency portfolio manager written in Flask! : Python
https://www.reddit.com/r/BitcoinMarkets/comments/7m1xqo/my_new_tool_to_track_your_portfolio_and_keep_you/ | My new tool to track your portfolio and keep you up at night 🤑 : BitcoinMarkets
https://www.reddit.com/r/investing/comments/7mzono/just_started_investing_made_a_little_opensource/ | Just started investing, made a little (opensource) program to analyze australian stock exchange. : investing

## buying traditionally?
https://www.bitcoinbulls.net/buying-bitcoin-through-a-brokerage-account.html | Buying Bitcoin Through a Brokerage Account

## strategies
https://news.ycombinator.com/item?id=15932492 | Financial Modeling for Cryptocurrency: The spreadsheet that got my first 1,000% | Hacker News

## news / discussion
https://www.coindesk.com/whos-buying-bitcoin-demand-persists-amid-fork-fears/ | Who's Buying Bitcoin? Demand Persists Amid Fork Fears - CoinDesk

## communities / discussion
https://www.reddit.com/r/BitcoinMarkets/ | Sharing of ideas, tips, and strategies for increasing your Bitcoin trading profits

## market size / overview
https://coinmarketcap.com/currencies/ripple/
https://www.reddit.com/r/BitcoinMarkets/comments/7i1kky/bitcoin_tech_rant_december_6th_2017/ | Bitcoin Tech Rant December 6th 2017 : BitcoinMarkets
https://www.cryptocompare.com/exchanges/poloniex/overview | Poloniex Exchange Reviews, Live Markets, Guides, Bitcoin charts
https://bitinfocharts.com/top-100-richest-bitcoin-addresses.html | Top 100 Richest Bitcoin Addresses and Bitcoin distribution
https://data.bitcoinity.org/markets/volume/30d?c=e&t=b | Bitcoinity.org
https://news.ycombinator.com/item?id=15877838 | The Bitcoin Whales: 1,000 People Who Own 40 Percent of the Market | Hacker News

## brokers / exchanges
https://en.bitcoin.it/wiki/Comparison_of_exchanges | Comparison of exchanges - Bitcoin Wiki
https://en.wikipedia.org/wiki/Digital_currency_exchange | Cryptocurrency exchange - Wikipedia
https://bravenewcoin.com/news/the-compelling-case-for-decentralized-crypto-exchanges/ | The compelling case for decentralized crypto exchanges » Brave New Coin

### poloniex
https://cointelegraph.com/news/suddenly-bitcoin-exchange-poloniex-changes-terms-of-use-triggers-alarm | Suddenly, Bitcoin Exchange Poloniex Changes Terms of U... | News | Cointelegraph
https://en.bitcoin.it/wiki/Poloniex | Poloniex - Bitcoin Wiki
https://www.coinstaker.com/poloniex-may-known-trouble-horizon/ | Poloniex May Have Known Trouble Was on the Horizon
https://poloniex.com/press-releases/2015.05.19-Open-Letter | An Open Letter from Tristan D'Agosta
https://www.ccn.com/cryptocurrency-exchange-poloniex-insecure-security-review-claims/ | Cryptocurrency Exchange Poloniex is Insecure, Security Review Claims
https://www.reddit.com/r/CryptoCurrency/comments/57q9gf/poloniex_is_secure_were_good/ | Poloniex is Secure. We're Good. : CryptoCurrency
https://steemit.com/bitcoinscamfighters/@bitcoinfighters/bitcoin-scam-fighters-fact-sheet-poloniex-and-chief-executive-officers | BITCOIN SCAM FIGHTERS -- FACT SHEET POLONIEX AND CHIEF EXECUTIVE OFFICERS — Steemit
https://poloniex.com/ | Poloniex - Bitcoin/Digital Asset Exchange
https://www.reddit.com/r/BitcoinMarkets/comments/4hmthy/people_are_too_lenient_with_poloniex_this_could/ | People are too lenient with Poloniex, this could end badly. : BitcoinMarkets

### bittrex
https://bittrex.com/ | Just a moment...
https://www.forexbrokerz.com/bitstamp-vs-bittrex-forex-broker-comparison | Bitstamp vs. Bittrex Bitcoin Exchange Comparison
https://en.m.wikipedia.org/wiki/Bitstamp | Bitstamp - Wikipedia



### kraken
seems to be a complete basket case
https://www.kraken.com/ | Kraken | Buy, Sell and Margin Trade Bitcoin (BTC) and Ethereum (ETH) - Buy, Sell, & Trade Bitcoin
