If you read the spec, you'll see how much is implementor-specific!

pain in my ass - see why Contenteditable is terrible

[Contenteditable div cursor outside of div](http://stackoverflow.com/questions/29462773/contenteditable-div-cursor-outside-of-div) - this appeared to happen when there was no empty text nodes in the div - also, I fixed it by using the W3C range to move the cursor back inside

[[Bug 13011\] Encourage authors to use "white-space: pre-wrap" for contenteditable/designMode](https://lists.w3.org/Archives/Public/public-html-bugzilla/2011Jun/0922.html) - not sure how helpful

[http://stackoverflow.com/questions/6023307/dealing-with-line-breaks-on-contenteditable-div](http://stackoverflow.com/questions/6023307/dealing-with-line-breaks-on-contenteditable-div) - seems Firefox/Safari add <br>s

[http://stackoverflow.com/questions/34588187/cursor-size-is-different-for-different-line](http://stackoverflow.com/questions/34588187/cursor-size-is-different-for-different-line) - cursor different size

**Selection API and ranges**

Tim Down author of Rangy (and log4javascript!) is the resident expert

per [Range.selectNodeContents()](https://developer.mozilla.org/en-US/docs/Web/API/Range/selectNodeContents)

"The startOffset is 0, and the endOffset is the number of child Nodes or number of characters contained in the reference node"

So the number in the offset is usually the number of nodes, including text nodes, 1-indexed!

Might use a toString to get the number of characters... [Range object: differences between Webkit and Mozilla based browsers](http://stackoverflow.com/questions/7486509/range-object-differences-between-webkit-and-mozilla-based-browsers)

Therefore, if both startContainer and endContainer are the same, you can find the node at the end with:

range.startContainer[range.endOffset-1]

and beginning at:

range.endContainer[range.startOffset-1]

[What is anchorNode , baseNode , extentNode and focusNode in the object returned by document.getSelection?](http://stackoverflow.com/questions/27241281/what-is-anchornode-basenode-extentnode-and-focusnode-in-the-object-returned) - baseNode and extentNode are synonyms