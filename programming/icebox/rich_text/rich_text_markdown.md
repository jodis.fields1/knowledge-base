tagged with markdown for now for lack of a better place and since there's a trend to switch back and forth
    also stuff on markdown in anki?

https://github.com/yabwe/medium-editor
https://github.com/StackExchange/pagedown
https://github.com/bergie/hallo
https://github.com/iDoRecall/comparisons/blob/master/JavaScript-WYSIWYG-editors.md
https://sofish.github.io/pen/

## markdown
vscode uses markdown-it and I think Ghost does too, which implements GFM (Github); Gitlab uses https://github.com/vmg/redcarpet which Github used to use

### tables
* use alt+shift+f per https://github.com/neilsustc/vscode-markdown/issues/100#issuecomment-367369089
discussed using multiple columns / rows at https://stackoverflow.com/a/49212652/4200039
![](2018-03-10-10-44-56.png )

#### 2018-03-12 link dump - editing markdown tables for Confluence
https://community.atlassian.com/t5/Confluence-questions/Using-markdown-tables/qaq-p/578215 | Using markdown tables
http://truben.no/table/ | Untitled - Table Editor
https://www.tablesgenerator.com/markdown_tables# | Markdown Tables generator - TablesGenerator.com

## general
Typora (closed-source) is best for mixing markdown and view
    * http://support.typora.io/Draw-Diagrams-With-Markdown/
### CKEditor
http://ckeditor.com/addon/markdown

CKEditor - supposed to be awesome?
https://stackoverflow.com/questions/22252875/ckeditor-have-it-return-markdown-syntax-instead-of-html

### diagrams
mermaid? 

### contenteditable
see separate note
the bane of this, but a guy who worked at Medium wrote a big article and
extensible hooks were being created...

### redactor
work library...