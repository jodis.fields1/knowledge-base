

## 2018
User Agent headers: https://fetch.spec.whatwg.org/#forbidden-header-name
went from https://fetch.spec.whatwg.org/#cors-safelisted-request-header to https://developer.mozilla.org/en-US/docs/Web/HTTP/CORS

## 2015
### How it works

[http://www.rfc-editor.org/rfcxx00.html](http://www.rfc-editor.org/rfcxx00.html) lists all the internet standards

TCP/IP Guide is free online; 1,600 pages - [tcpipguide.com HTTP overview](http://www.tcpipguide.com/free/t_TCPIPWorldWideWebandHypertextOverviewandConcepts.htm), [tcipguide.com history section](http://www.tcpipguide.com/free/t_HTTPOverviewHistoryVersionsandStandards.htm)

List of HTTP header fields ( [wiki](http://en.wikipedia.org/wiki/List_of_HTTP_header_fields)) - need to understand how most of these

[RFC2616](http://tools.ietf.org/html/rfc2616) is fundamental

2014 update with RFC 723X described at  [Bye bye RFC2616, Welcome RFC 723X ](http://www.lennybacon.com/post/2014/06/10/bye-bye-rfc2616-welcome-rfc-723x)and  [HTTP/1.1 just got a major update](https://news.ycombinator.com/item?id=7861152) (ycombinator)

[http://www.w3.org/standards/techs/http#w3c_all](http://www.w3.org/standards/techs/http#w3c_all) for the specifications

[http://www.w3.org/Protocols/](http://www.w3.org/Protocols/) - overview of HTTP

[http://www.faqs.org/rfcs/index.html](http://www.faqs.org/rfcs/index.html) - has list of most popular 

 has links to the RFC which update

RFC 2109, later revised in RFC 2965 (HTTP State Management Mechanism) - cookies and stuff