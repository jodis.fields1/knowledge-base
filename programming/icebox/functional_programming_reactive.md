[What's the difference between Currying and Partial Application?](http://raganwald.com/2013/03/07/currying-and-partial-application.html) - best article on the topic I found, by author of JavaScript Allonge     

Netflix engineer originally recommended Microsoft's ReactiveExtensions and this tutorial [http://reactive-extensions.github.io/learnrx/](http://reactive-extensions.github.io/learnrx/) was helpful - however it's very similar to underbar!

**Functional reactive**

[RxJS is great. So why have I moved on?](https://news.ycombinator.com/item?id=10746533) - interesting discussion     

[The introduction to Reactive Programming](https://gist.github.com/staltz/868e7e9bc2a7b8c1f754)

[you've been missing](https://gist.github.com/staltz/868e7e9bc2a7b8c1f754) -