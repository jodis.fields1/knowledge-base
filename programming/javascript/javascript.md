Javascript
===

## authentication / passwords
https://blog.bitsrc.io/6-javascript-user-authentication-libraries-for-2019-6c7c45fbe458

https://github.com/simov/grant - prolly the most maintained

passportjs - apparently still the major game in town, but rather neglected, forked at https://github.com/jaredhanson/passport/issues/536#issuecomment-467375004

## frameworks / libs / etc
use libhunter?

https://risingstars.js.org/2019/en/#section-nodejs-framework

## 2018-02 update
[Memory Terminology](https://developers.google.com/web/tools/chrome-devtools/memory-problems/memory-101) - excellent

### date time libs
momentjs is the leader, but mutates stuff a lot
Successor: Luxon from moment team
See also: date-fns https://github.com/date-fns/date-fns
https://moment.github.io/luxon/docs/manual/why.html
https://www.reddit.com/r/javascript/comments/79trcf/luxon_an_immutable_date_wrapper_similar_to_moment/dp4wap4/

## sockets
https://meta.stackexchange.com/questions/189350/websockets-stuck-in-pending-on-stack-overflow

### rounding toFixed
[Why toFixed() rounding is so strange [duplicate]](https://stackoverflow.com/questions/18374940/why-tofixed-rounding-is-so-strange) - TODO reread this

### backslash
"when there’s no special meaning: like \d or \z, then the backslash is simply removed..." - per https://javascript.info/regexp-escaping#new-regexp

### language spec w/ ternary
from https://github.com/brightideainc/main/pull/7872 - in retrospect makes some sense, but why didn't it return true in second? because it needs to evaluate ternary I guess

```
kinda weird logic error:
var x = true || console.log('a')
output: nothing
x === true

var x = true || true ? console.log('a') : console.log('b')
output: 'a'
x === undefined
```

http://www.ecma-international.org/ecma-262/6.0/#sec-conditional-operator | ECMAScript 2015 Language Specification – ECMA-262 6th Edition
https://github.com/getify/You-Dont-Know-JS/blob/master/types%20%26%20grammar/ch5.md | You-Dont-Know-JS/ch5.md at master · getify/You-Dont-Know-JS
https://stackoverflow.com/questions/5097346/how-is-the-ternary-operator-evaluated-in-javascript | How is the ternary operator evaluated in JavaScript? - Stack Overflow
https://stackoverflow.com/questions/39359713/extra-parenthesis-required-for-ternary-statement | javascript - Extra parenthesis required for ternary statement - Stack Overflow


### Newbie intro
https://davidshariff.com/blog/javascript-inheritance-patterns/ - Pseudoclassical, Functional pattern, Prototypal pattern

[JS Visualizer – Visualize Context, Hoisting, Closures, and Scopes in JavaScript](https://news.ycombinator.com/item?id=18177755)

I'm pruning my notes of things that I've internalized, but that doesn't mean that other people can't share. This is targeted to my younger brother.

First off, the best guide is the [MDN Javascript Guide](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Introduction), and it should be read in its entirety at some point in this process. But I hesitate to recommend it first. I would recommend Codeschool's Javascript adventure first.

**Gotchas**<br/>
Hoisting -  [JavaScript Scoping and Hoisting](http://www.adequatelygood.com/JavaScript-Scoping-and-Hoisting.html) (2010) is perhaps the original use of the term "hoisting", tho he denies inventing it. Also see [When and Where should we use hoisting in Javascript](http://programmers.stackexchange.com/questions/233500/when-and-where-should-we-use-hoisting-in-javascript) which says never. But [Note 4. Two words about "hoisting"](http://dmitrysoshnikov.com/notes/note-4-two-words-about-hoisting/) points out specifically the mutual recursion aspect and the issue of file concatenation with function declarations.
[Check if a variable is an object in javascript](http://stackoverflow.com/questions/8511281/check-if-a-variable-is-an-object-in-javascript/25715455#25715455) - typeof doesn't work as it includes null and functions, plus arrays
For loops
[Why is using “for…in” with array iteration such a bad idea?](http://stackoverflow.com/questions/500504/why-is-using-for-in-with-array-iteration-such-a-bad-idea) - don't risk it, classic discussion