Nodejs debugging
===
See c_systems/_notes/debuggers also

https://github.com/nearform/node-clinic-doctor


TODO: [Ndb – An improved debugging experience for Node.js](https://news.ycombinator.com/item?id=17581521)

## Chrome debuggin
chrome://inspect/#devices

#### event loop
[Event Loop context in the debugger](https://github.com/nodejs/help/issues/1621)
* [found from feature request: a way to inspect what's in the event loop](https://github.com/nodejs/node/issues/1128)

See nodejs_internals#Event_loop

#### remote
https://www.reddit.com/r/programming/comments/6fm6y0/debugging_remote_nodejs_application_running_in_a/

#### postmortem
see https://blog.risingstack.com/post-mortem-diagnostics-debugging-node-js-at-scale/#disqus_thread

**error codes and names** - see[http://man7.org/linux/man-pages/man3/errno.3.html](http://man7.org/linux/man-pages/man3/errno.3.html)
[http://stackoverflow.com/questions/23683911/node-js-error-code-meanings-specifically-fs](http://stackoverflow.com/questions/23683911/node-js-error-code-meanings-specifically-fs) with related err-codes npm module

exit codes: [https://nodejs.org/api/process.html#process_exit_codes](https://nodejs.org/api/process.html#process_exit_codes)

#### Debugging
[Debugging Node.js with Google Chrome](https://medium.com/the-node-js-collection/debugging-node-js-with-google-chrome-4965b5f910f4) - key is to about:inspect

https://github.com/jaridmargolin/inspect-process/issues/5
https://github.com/ChromeDevTools
https://news.ycombinator.com/item?id=14152257#14153442
https://github.com/nodejs/node/issues/8690
https://github.com/Microsoft/nodejstools/issues/197
https://visualstudio.uservoice.com/forums/121579-visual-studio-ide/suggestions/3549376-attach-child-process-to-debugger-automatically
https://github.com/nodejs/node/issues/8369
https://github.com/june07/NIM/ - motivation at https://june07.com/inspect-broke-my-workflow/

##### 2018-03 attach to running process
https://stackoverflow.com/questions/13052548/node-js-how-to-attach-to-a-running-process-and-to-debug-the-server-with-a-conso | Node.js: How to attach to a running process and to debug the server with a console? - Stack Overflow
https://stackoverflow.com/questions/30181274/how-to-attach-a-running-node-js-process-and-debug-without-a-gui | debugging - how to attach a running node.js process and debug WITHOUT a GUI? - Stack Overflow

#### network requests
https://stackoverflow.com/a/29835774/4200039 - global-request-logger or wireshark - maybe mitm / tamper proxy?
https://github.com/nodejs/diagnostics - esp. https://github.com/nodejs/diagnostics/issues/75 for network inspection
https://github.com/buggerjs/bugger - deprecated, this guy is driving network inspection in --inspect

##### Auto trace or instrumentation
* 2018-08-12: revisited Theseus from a Slant convo w/ Cappucino. See dismissed tools

#### Dismissed tools
* spyjs and tracegl - unmaintained per https://github.com/traceglMPL/tracegl/issues/7 
* theseus for brackets looks super cool, uses node-theseus/fondue to instrument the entire program; "[instrumentation](https://en.wikipedia.org/wiki/Instrumentation_(computer_programming))" library `fondue.js`. Brackets also has a couple loosely-maintained traditional debuggers which I didn't try. Surprisingly, Theseus does not seem popular, and it can seem confusing to use as this [How to debug JavaScript in brackets](https://groups.google.com/forum/#!topic/brackets-dev/L8eZS3NTFgo) (2014) thread and [this Stackoverflow question](http://stackoverflow.com/questions/22102783/how-to-debug-an-angularjs-app-using-theseus) demonstrates. Raymond Camden covers some of Brackets other cool features in [Deeper In the Brackets Editor](http://code.tutsplus.com/tutorials/deeper-in-the-brackets-editor--net-35527).
<trimmed>