Nodejs frameworks
===

#### express
classic tried and true

`req.host` and `req.path` are both wrong and will be deprecated
  * should've matched node
  * `req.path` -> `req.pathname`
  * `req.host` will be back

#### Koa
  koa:
  [https://www.smashingmagazine.com/2016/08/getting-started-koa-2-async-functions/](https://www.smashingmagazine.com/2016/08/getting-started-koa-2-async-functions/)
  - tutorialspoint - [detailed, bit naive](https://www.tutorialspoint.com/koajs/koajs_restful_apis.htm)
  derby: seems to be focused on sockets
  nodal: seems popular?
  lux: JSON API compliant out of the box

https://github.com/ethanresnick/json-api/issues/37
https://github.com/rkusa/koa-passport