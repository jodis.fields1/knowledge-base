Nodejs general
===

https://node.green/

## unorganized 2021
https://blog.soulserv.net/exiting-node-js-asynchronously/
* use async-kit

## unorganized 2019-07
https://github.com/direnv/direnv/issues/284 | Add support for .env files · Issue #284 · direnv/direnv
https://www.npmjs.com/package/dotenv | dotenv - npm
https://codeburst.io/how-to-easily-set-up-node-environment-variables-in-your-js-application-d06740f9b9bd | How to (easily) set up Node environment variables in your JS application
https://news.ycombinator.com/item?id=18903196 | A summary and curation of the top-ranked content on Node.js best practices | Hacker News
https://www.reddit.com/r/node/comments/ahj1gp/polydev_faster_routecentric_development_for/ | polydev - Faster, route-centric development for Node.js apps with built-in Hot Module Replacement. : node


## Event loop
[feature request: a way to inspect what's in the event loop](https://github.com/nodejs/node/issues/1128)

[Node: synchronous code runs faster than asynchronous code](https://medium.com/@adamhooper/node-synchronous-code-runs-faster-than-asynchronous-code-b0553d5cf54e) - ?

https://github.com/RisingStack/risingstack-bootcamp

[What you should know to really understand the Node.js Event Loop](https://web.archive.org/web/20170930165624/https://medium.com/the-node-js-collection/what-you-should-know-to-really-understand-the-node-js-event-loop-and-its-metrics-c4907b19da4c)

**parallelization** - [http://stackoverflow.com/questions/19120213/parallelizing-tasks-in-node-js](http://stackoverflow.com/questions/19120213/parallelizing-tasks-in-node-js)

**Exports** - HackReactor used their former student's article [Understanding module.exports and exports in Node.js](http://www.sitepoint.com/understanding-module-exports-exports-node-js/) but see [module.exports vs exports in Node.js](http://stackoverflow.com/questions/7137397/module-exports-vs-exports-in-node-js) for more details and remember module.exports wins!
exports = module.exports = {};

**Resources**
Check out [http://howtonode.org/](http://howtonode.org/) and nodeschool
 
[Using node.js, how to serve content without using a framework like express?](http://programmers.stackexchange.com/questions/197806/using-node-js-how-to-serve-content-without-using-a-framework-like-express) - points to node beginner book, but basically you can use the http module to start

#### Theoretical / Scaling (not yet relevant)
**Complaints**[http://stackoverflow.com/questions/24584427/tj-holowaychuks-criticisms-of-nodejs](http://stackoverflow.com/questions/24584427/tj-holowaychuks-criticisms-of-nodejs)

