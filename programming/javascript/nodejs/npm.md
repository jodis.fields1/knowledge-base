

### hacking on node_modules package
[Installing a local module using npm?](https://stackoverflow.com/questions/8088795/installing-a-local-module-using-npm)
* https://stackoverflow.com/a/59766644/4200039

#### force dependencies
https://stackoverflow.com/questions/15806152/how-do-i-override-nested-npm-dependency-versions

#### npm link and such
https://docs.npmjs.com/cli/v7/commands/npm-link

see react_redux_test_enzyme_bisect
[Anyone using npm link/yarn link successfully for developing local libraries?](https://www.reddit.com/r/node/comments/71q8pa/anyone_using_npm_linkyarn_link_successfully_for/)
    * https://www.npmjs.com/package/yalc is recommended

### common error
[[crash] npm ERR! cb() never called!](https://npm.community/t/crash-npm-err-cb-never-called/858/60)
* dreaded, ubiquitous error
* increase ulimit per https://npm.community/t/crash-npm-err-cb-never-called/858/61?

### timeline
2019: yarn2 requires prolog https://yarnpkg.com/features/constraints as criticized here https://twitter.com/seldo/status/1221464311115108353 and defended at https://twitter.com/rauschma/status/1221135190740869127

2018-06: yarn or npm decision?
    * pnpm is open to package.yaml https://github.com/pnpm/pnpm/issues/1100
        * but has lots of compatibility issues e.g. gatsby
    * +1 for yarn: team is more open to something like package.json5 https://github.com/yarnpkg/yarn/issues/178
    * yarn again; note that the `npm ci` code is at https://github.com/zkat/cipm
        * added in https://github.com/npm/npm/pull/19542/files with no comments
    * vscode team is still using yarn https://www.reddit.com/r/javascript/comments/8bozsl/2018_go_back_to_npm_vs_yarn/dx92ckt/
        * suggests that [@types packages are not installed as expected](https://github.com/yarnpkg/yarn/issues/4489#issuecomment-360812460 is not so bad
    * [make `--pure-lockfile` default for `install`](https://github.com/yarnpkg/yarn/issues/570#issuecomment-274638907)
        * per this comment, the lock is keyed specifically to entries in package.json

### lock and shrinkwrap
https://github.com/npm/npm/issues/6298 | Shrinkwrap and dev dependencies · Issue #6298 · npm/npm
    * interestingly, led me down the path of how yarn treats its dependencies lockfiles (i.e. ignores them)
        * [Lockfiles should be committed on all projects](https://yarnpkg.com/blog/2016/11/24/lockfiles-for-all/) from https://medium.com/@boennemann/avoid-yarn-for-packages-and-fully-enjoy-its-benefits-for-application-development-8bdd4deb33cf
https://github.com/npm/npm/issues/17761 | A way to run "npm install" without modifying the lockfile · Issue #17761 · npm/npm
https://github.com/peerigon/updtr | peerigon/updtr: Update outdated npm modules with zero pain™

### dependencies and peerDependencies
use https://www.npmjs.com/package/npm-install-peers per https://github.com/npm/npm/issues/11213
[Understanding the npm dependency model](https://lexi-lambda.github.io/blog/2016/08/24/understanding-the-npm-dependency-model/) - best explanation by far followed by https://stackoverflow.com/questions/26737819/why-use-peer-dependencies-in-npm-for-plugins whereas https://nodejs.org/en/blog/npm/peer-dependencies/ is terrible, https://docs.npmjs.com/files/package.json#peerdependencies is sort of OK

Note: peerDependencies throws a warning in npm 3 per https://codingwithspike.wordpress.com/2016/01/21/dealing-with-the-deprecation-of-peerdependencies-in-npm-3/

### npm
package naming, see [On "Reserving" Package Names](https://groups.google.com/forum/#!topic/npm-/odEwfBoxwy4) (npm forum is deprecated; try node forum)

package.json: see [https://docs.npmjs.com/files/package.json](https://docs.npmjs.com/files/package.json)

bin property symlinks into ./node_modules/.bin if the file is installed locally

use npm bin to access per [How to use package installed locally in node_modules?](http://stackoverflow.com/questions/9679932/how-to-use-package-installed-locally-in-node-modules)

scripts property: [task automation with npm run](http://substack.net/task_automation_with_npm_run) (2010) and later

[How to Use npm as a Build Tool](http://blog.keithcirkel.co.uk/how-to-use-npm-as-a-build-tool/) - "npm test" is shorthand for "npm run test" and more generalizable; to more generally execute these you can always do export PATH=./node_modules/.bin:$PATH per [fake nvm instructions](https://www.npmjs.com/package/nvm); adding arguments to these scripts was a major requested change per [issue 3494](https://github.com/npm/npm/issues/3494)

custom config properties: 'jest' is a major example, not sure of others
