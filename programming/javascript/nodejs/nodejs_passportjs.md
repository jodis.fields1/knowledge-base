# passportjs
PREREQUISITE: understand connect middleware and nodejs; various things get added to the incomingRequest (e.g., req._passport)

The [docs suck](https://github.com/jaredhanson/passport/issues/592) and lib unmaintained - see 
* https://github.com/jwalton/passport-api-docs
* https://github.com/passport-next
* https://github.com/passport/express-4.x-local-example/blob/master/server.js
* Visual flow: https://stackoverflow.com/questions/27637609/understanding-passport-serialize-deserialize?answertab=votes#tab-top - thanks https://hackernoon.com/passportjs-the-confusing-parts-explained-edca874ebead

See also:
*  https://github.com/jaredhanson/passport/issues/208#issuecomment-113445331

TL;DR w/ passport-local

* [passport-local Strategy.authenticate](https://github.com/jaredhanson/passport-local/blob/2bf3939ca369e08a47a28585c2ccfb3cecffeb9c/lib/strategy.js#L69)
  * called by [passport.authenticate](https://github.com/jaredhanson/passport/blob/1c8ede35a334d672024e14234f023a87bdccaac2/lib/middleware/authenticate.js#L361)
  * expects username & password field on req.body
  * passes these to the `verify()` callback passed into the original `passport.use(new LocalStrategy)`
  * `verify()` passes user into `verified()` which calls `self.success()`
  * by `self.success()`, login success / failure should be clear - but you can use a custom callback
  * `self.success()` calls `req.login()` which calls [req.logIn in lib/http](https://github.com/jaredhanson/passport/blob/2327a36e7c005ccc7134ad157b2f258b57aa0912/lib/http/request.js#L50) which calls  [serializeUser](https://github.com/jaredhanson/passport/blob/2327a36e7c005ccc7134ad157b2f258b57aa0912/lib/sessionmanager.js#L14)

  ## dependencies
https://github.com/expressjs/session | expressjs/session: Simple session middleware for Express
https://github.com/expressjs/cookie-session | expressjs/cookie-session: Simple cookie-based session middleware

## resources

