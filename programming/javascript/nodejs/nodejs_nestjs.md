Nodejs - Nestjs
===

cookie_id: "wx2ljSpPaBaP44sY3gAWCZIbcg8ny9Mq"


job queue using bull:
* https://github.com/cdiaz/nest-job-queue

global modules decorator:
* https://github.com/nestjs/nest/blob/master/packages/common/decorators/modules/global.decorator.ts

BrunnerLivio - https://github.com/Roche/lxdhub and https://dev.to/nestjs/advanced-nestjs-dynamic-providers-1ee
* https://github.com/Roche/lxdhub/tree/master/packages/api is his app

my-thai-star from capgemini https://github.com/devonfw/my-thai-star/blob/2b1f2ef132b21e8e43e57036223378422cddeff6/node/src/app/core/core.module.ts
* also has interesting ConfigurationModule https://github.com/devonfw/my-thai-star/blob/2b1f2ef132b21e8e43e57036223378422cddeff6/node/src/config/default.ts

https://github.com/jmcdo29/testing-nestjs
* https://github.com/BrunnerLivio/nestjs-integration-test-db-example

Mai's ecommerce app - https://github.com/kelvin-mai/react-commerce and uses https://github.com/palantir/blueprint

Ben Awad has a bunch of typescript stuff: https://www.youtube.com/user/99baddawg/search?query=typeorm

[Ideas App](https://www.youtube.com/playlist?list=PLBeQxJQNprbiJm55q7nTAfhMmzIC8MWxc)

[Learn Nest.js from Scratch by building an API](https://www.youtube.com/watch?v=F_oOtaxb0L8)

presentations: https://github.com/nestjs/nest/issues/1058
* in particular https://speakerdeck.com/kamilmysliwiec/rethinking-enterprise-architectures-with-node-dot-js

## nestjs/crud
https://hackernoon.com/quick-and-easy-crud-with-nestjs-nestjsxcrud-and-testmace-t9cn313h
* dropped it due to some annoying dependency hell w/ classTransformer

## auth

authentication - see authentication.md:
* https://github.com/cdiaz/nestjs-auth0/blob/master/src/app.module.ts
* [Add "Security" section #109](https://github.com/nestjs/docs.nestjs.com/issues/109)
* [NestJS Authenticated sessions documentation has major gaps and is seemingly wrong #237](https://github.com/nestjs/docs.nestjs.com/issues/237)

## control flow
backend/node_modules/@nestjs/core/nest-factory.js 
* create() gets ball rolling
    * note that NestContainer is exposed as this.container per https://github.com/nestjs/nest/blob/9a0efc19d8f94783b6636180aa4f44e241eea8ae/packages/core/nest-application-context.ts#L40
* initialize()
    * createInstancesOfDependencies() kicks off the injector.js stuff

## nestjs
Summary: basically ngModule applied to the backend?

## console
[Possibility to write "Console Commands" #1117](https://github.com/nestjs/nest/issues/1117)

### realworld
https://github.com/lujakob/nestjs-realworld-example-app | lujakob/nestjs-realworld-example-app: Exemplary real world backend API built with NestJS + TypeORM
https://github.com/gothinkster/realworld#frontends | gothinkster/realworld: "The mother of all demo apps" — Exemplary fullstack Medium.com clone powered by React, Angular, Node, Django, and many more

### link dump
https://github.com/nestjs/nest/issues/363#issuecomment-383343388 | Unable to run tests because Nest can't resolve dependencies of a service · Issue #363 · nestjs/nest

https://github.com/scalio/nest-workshop-backend/commit/be315597632b01a8abf06569bab39dfdf8ba3118#diff-d4b33aff3b53f78f1f3f9497666e916f | Chapter 5 · scalio/nest-workshop-backend@be31559

https://github.com/nestjs/nest/issues/918 | How to map entities to DTOs · Issue #918 · nestjs/nest

### career / jobs
Dynatrace - position in Detroit (ugh)
Cigna - Portland

### react compatibility
ngModule use with react - Google Search


## angular modules
nestjs is modeled after these:

https://angular.io/guide/feature-modules | Angular - Feature Modules
https://angular.io/guide/ngmodules | Angular - NgModules
https://angular.io/guide/module-types | Angular - Types of Feature Modules
https://medium.com/@palmer_todd/ngmodules-angular-modules-simplified-52d2075f8c8e | NgModules: Angular Modules Simplified – Todd Palmer – Medium
