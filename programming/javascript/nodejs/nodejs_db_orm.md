Nodejs orm
===
See my describethis / afewitems projects...

TODO: consider switching to mikro-orm
* https://github.com/dario1985/nestjs-mikro-orm but later see https://github.com/nestjs/docs.nestjs.com/pull/1821

## typeorm
[Allow changing type of entity attribute per database configuration](https://github.com/typeorm/typeorm/issues/1776) - no common cross-db types

[How to get raw sql query string with all parameters escaped?](https://stackoverflow.com/questions/59563190/how-to-get-raw-sql-query-string-with-all-parameters-escaped)

### typeorm bugs
see stock-analysis/backend/notes

## old
https://www.reddit.com/r/node/comments/8bf4gb/what_are_your_experiences_with_the_sequelize_orm/ | What are your experiences with the sequelize ORM? : node
https://www.reddit.com/r/node/comments/8sugu6/express_sequelize_vs_nestjs_typeorm/ | express + sequelize vs nestjs + typeorm : node
https://www.reddit.com/r/javascript/comments/7u796b/objectionjs_with_knexjs_vs_typeorm_which_one_do/ | Objection.js with Knex.js vs TypeORM - which one do you prefer and why? : javascript

[Seeding your Database with Thousands of Users using Knex.js and Faker.js](https://www.reddit.com/r/node/comments/a2pz4q/seeding_your_database_with_thousands_of_users/) - ??
