Nodejs internals
===

See also c_systems/notes/debuggers.md

## timeline
2019-08: 
* found a bug in readdirSync which listed a folder which wasn't actually a folder (`.git/branches`); dunno how `internalbinding('fs')` works but maybe something at https://github.com/nodejs/node/tree/master/lib/internal/fs; found UV_DIRENT_DIR as an eum (2) in deps/uv/include/uv.h
* mentorship program https://medium.com/the-node-js-collection/node-js-mentorship-working-group-573dac18d8ff is interesting


2018-06:
* https://github.com/alibaba/AliOS-nodejs/wiki/Workers-in-Node.js-based-on-multithreaded-V8
* convo with Eric, clear neither of us really knew

## codebase explorations
### vscode's nodejs version
VSCode runs its extensions on the internal electron nodejs version - which, per [Custom node version to run VSCode extensions](https://stackoverflow.com/questions/45062881/custom-node-version-to-run-vscode-extensions), cannot be changed. Diving in to figure out how that is figured out:
* developer tools-> process.versions to see node version
* navigate to version of electron, notice https://github.com/electron/electron/tree/master/vendor which has node
* in Makefile, note https://github.com/electron/node/blob/dc8fe9d390b1deb32ae2601357a90679cbcc757e/Makefile#L531
* in tools/getnodeversion.py, pointer to ../src/node_version.h
* #define at top https://github.com/electron/node/blob/d969dd20b689b14e7bbda6be03b361638809ef55/src/node_version.h#L25

Per [How to find out which node vscode is using](https://github.com/Microsoft/vscode/issues/11660#issuecomment-246265598) I discovered you can use process.versions to find out 

## Contribution
https://medium.com/fhinkel/debug-v8-in-node-js-core-with-gdb-cc753f1f32
https://blog.risingstack.com/contributing-to-the-node-js-core/

## Native module and bugs? 
https://stackoverflow.com/questions/23228868/how-to-debug-binary-module-of-nodejs
https://github.com/workshopper/goingnative
https://blog.risingstack.com/how-to-use-rust-with-node-when-performance-matters/
https://github.com/nodejs/node-gyp/issues/489

## Architecture
[Getting Started in Node.js Internals Program](https://github.com/nodejs/inclusivity/issues/96) - I pointed them towards a couple things

### Event loop
[Understanding the Node.js Event Loop](https://nodesource.com/blog/understanding-the-nodejs-event-loop) (2015) -

http://blog.mixu.net/2011/02/01/understanding-the-node-js-event-loop/ - classic article
http://www.journaldev.com/7462/node-js-architecture-single-threaded-event-loop - not classic but somewhat clear

https://developer.mozilla.org/en-US/docs/Web/JavaScript/EventLoop

### C interface and libuv
See also the repo c_systems/notes/debuggers.md and my nodejs fork on Gitlab
Also see https://github.com/bodokaiser/libuv-internals/blob/master/README.md
Best overview at: [How does NodeJS work?](https://blog.ghaiklor.com/how-nodejs-works-bfe09efc80ca) (2015)
Left a comment pointing to some source code at https://stackoverflow.com/questions/8575442/internals-of-node-js-how-does-it-actually-work#comment75968921_11765545

Redundant content at Architecture of Node.js’ Internal Codebase but noted this gem: "libuv maintains a thread pool of four worker threads, although the number can be altered to add more threads. If a request is file-system I/O and DNS-related, then it will be assigned to the thread pool for processing; otherwise, for other requests such as networking, platform-specific mechanisms will be deployed to handle such requests" - in the libuv overview, it notes that "Unlike network I/O, there are no platform-specific file I/O primitives libuv could rely on, so the current approach is to run blocking file I/O operations in a thread pool" with further details at https://web.archive.org/web/20171208015818/https://blog.libtorrent.org/2012/10/asynchronous-disk-io/

Diving deeper into libuv, uv_spawn calls:
* [uv__handle_init](https://github.com/libuv/libuv/blob/v1.12.0/src/uv-common.h#L211) which has that funny do { } while(0) construct (see [here](https://stackoverflow.com/questions/257418/do-while-0-what-is-it-good-for)) which calls a couple things
    * QUEUE_INSERT_TAIL - basic queue
    * uv__handle_platform_init - not important
* uv__malloc
* uv__process_init_stdio which assigns the fds
    * calls uv__stream_fd which seems important but is actually not
* uv__make_pipe
* ... others
* uv__process_child_init
    * ... a bunch of stuff
    * ultimately execvp

## Community
https://en.wikipedia.org/wiki/Bryan_Cantrill - hilarious badass
http://nodeguide.com/community.html
https://nodeschool.io/
https://hueniverse.com/2015/05/10/the-best-kept-secret-in-the-node-community/ - NodeConf at Walker Creek Ranch (from Hapi creator)