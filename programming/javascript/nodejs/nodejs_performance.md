Nodejs performance
===
'[DEBUGGING NODE.JS MEMORY LEAKS](http://blog.flowdock.com/2015/06/22/debugging-node-js-memory-leaks/) (2015) 

[An example of how Node.js is faster than PHP](http://blog.appdynamics.com/nodejs/an-example-of-how-node-js-is-faster-than-php/) - dramatic!

But [A Non-Blocking Benchmark](http://techblog.bozho.net/non-blocking-benchmark/) shows different results, which a commenter explains is derived from Little's Law; skimmed [Little’s Law, Scalability And Fault Tolerance: The OS Is Your Bottleneck. What You Can Do?](http://highscalability.com/blog/2014/2/5/littles-law-scalability-and-fault-tolerance-the-os-is-your-b.html)

**Profiling**
https://www.joyent.com/node-js/production/debug

[Gotchas From Two Years With Node](https://segment.com/blog/gotchas-from-two-years-of-node/) (2015) - TJ Holowaychuck works at Segment

Node-Webkit-Agent - less buggy
Memwatch - ??
Flame Graphs - ??
Apache Bench - necessary to simulate load and gather stats