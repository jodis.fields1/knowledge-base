*Note*: see webpack repositories such as:
* https://gitlab.com/jcrben-play-learn/js-modules-play
* webpack-react-redux-minimal

For source maps, use https://github.com/webpack/webpack/issues/2145#issuecomment-294361203

https://stackoverflow.com/questions/25509471/webpack-multiple-requires-resolving-to-same-file-but-being-imported-twice

TODO: update the devtool docs as requested by https://github.com/webpack/webpack/issues/2145#issuecomment-294368814

## caching
[Long-term caching of static assets with Webpack, React, and Typescript
](https://web.archive.org/web/20181218211237/https://dradetsky.github.io/) - from my friend Daniel Radetsky

https://blog.cloudboost.io/webpack-3-dynamic-imports-code-splitting-and-long-term-caching-made-easy-1892981e0ae7

## epiphanies
* "Loaders are always called from right to left" per https://webpack.js.org/api/loaders/ - a bit counterintuitive
  * learned while analyzing style-loader
    * incidentally, style-loader creates new Blobs (blob:<url>) which is interesting
 

### TODO upgrade to 3 or 4?
3 has memory leak? https://github.com/webpack/webpack/issues/5120
    * FATAL ERROR: CALL_AND_RETRY_LAST Allocation failed - JavaScript heap out of memory

### TODO
what is content base?
https://stackoverflow.com/questions/47327542/mapping-of-webpack-source-maps-for-production-in-chrome - straightforward with Charles proxy?

### tips
* [Import whole directory in Webpack(er)](https://www.neontsunami.com/posts/import-whole-directory-in-webpacker) - has various tool tips

### fundamentals
[What are module, chunk and bundle in webpack?](https://stackoverflow.com/questions/42523436/what-are-module-chunk-and-bundle-in-webpack)

### Env variables
DefinePlugin - [Passing Environment Variables Into Your Code With Webpack](https://medium.com/@justintulk/passing-environment-variables-into-your-code-with-webpack-cab09d8974b0)

### Performance
https://github.com/webpack/webpack/issues/168#issuecomment-34864696 - advice from sokra in 2014 - use eval with pathinfo = true

Comparison at http://cheng.logdown.com/posts/2016/03/25/679045 as well: "For development, use cheap-module-eval-source-map. For production, use cheap-module-source-map."

set up DLL and code splittinge.g. 

DLL: 
* https://github.com/webpack/webpack/tree/master/examples/dll
* https://medium.com/@soederpop/webpack-plugins-been-we-been-keepin-on-the-dll-cdfdd6cb8cd7
* [Minimal Webpack DllPlugin example](https://gist.github.com/robertknight/058a194f45e77ff95fcd)

#### profiling
[Too long build](https://github.com/webpack/webpack/issues/4550#issuecomment-306750677)
[Too long build](https://github.com/webpack/webpack/issues/4550#issuecomment-288849167)

### Hot Module Replacement
See react_hot_reload.md


### Speed
https://github.com/amireh/happypack

### Alternatives
[With latest TypeScript, you may not need Webpack
](https://medium.com/@vivainio/with-latest-typescript-you-may-not-need-webpack-417d2ef0e773)

[Show HN: JohnnyDepp – A tiny dependency manager for modern browsers in 862 bytes](https://news.ycombinator.com/item?id=18111961) - does not handle the bundling

### Boilerplate / starters
https://stanko.github.io/webpack-2-react-redux/

### proxy
        proxy: {
            '/ct': {
                target: 'http://www.brightideadev.com:80',
                changeOrigin: true,
                xfwd: true,
                secure: false
            },
            '/_app': {
                target: 'http://www.brightideadev.com:80',
                changeOrigin: true,
                xfwd: true,
                secure: false
            }
        }