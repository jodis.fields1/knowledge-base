

## options
xhr - most widely available, but otherwise blah; used by redux-resource-request

### axios
axios - best but owner Matt Zabriskie isn't too active these days, lacks high-level integration tests
    * issues with [Content-Type application/x-www-form-urlencoded](https://github.com/axios/axios/issues/362)

### request
pretty much a node-only thing per [Does "request" have browser support?](https://github.com/request/request/issues/2090)
https://github.com/request/request - suffers maintenance issues, see e.g. https://github.com/request/request/issues/1502

### fetch
isomorphic-fetch - uses whatwg-fetch and node-fetch under the hood
[Why I won’t be using Fetch API in my apps](https://medium.com/@shahata/why-i-wont-be-using-fetch-api-in-my-apps-6900e6c6fe78)