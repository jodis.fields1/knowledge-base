keywords: **deployment**
See project repos for latest patterns

## outdated
Customize index.html for development and production -> use [preprocess](https://github.com/jsoverson/preprocess)!! see [grunt-preprocess](https://github.com/jsoverson/grunt-preprocess) and gulp-preprocess. Jumped off from [Have Grunt generate index.html for different setups](http://stackoverflow.com/questions/12401998/have-grunt-generate-index-html-for-different-setups)

**RequireJS and AMD**

[Ditching RequireJS for Webpack: the reasons and lessons learned.](http://blog.player.me/ditching-requirejs-webpack-reasons-lessons-learned/) - 

drop a comment near the enable function logging out the map.id to see the ordering

dependency management which might too heavyweight for me right now

[Intro to Grunt with Install, Configure and Build Javascript Projects](http://www.thegeekstuff.com/2014/08/grunt-introduction/) (2014) and  [Gulp + Browserify: The Everything Post](http://viget.com/extend/gulp-browserify-starter-faq)

[Understanding RequireJS for Effective JavaScript Module Loading](http://www.sitepoint.com/understanding-requirejs-for-effective-javascript-module-loading/) (2013) is also a decent intro

**uglify minify and so on** - 

PurifyCSS to strip those out

**Gulp** -
NOTE: gulp is prolly dead as of 2018
use [gulp-task-listing](https://www.npmjs.com/package/gulp-task-listing) to set the default to show the tasks

got a bit into this; for debugging pipe to grunt-debug but watch is not a stream so try gulp-watch to allow for piping; see npm for how to do incremental building; also see [gulp: The vision, history, and future of the project](https://medium.com/@contrahacks/gulp-3828e8126466)

Brunch - incremental builder which is focused on convention over configuration, making it much faster and less verbose

Broccoli - used under the hood of ember-cli