technically not javascript-specific...

## json schema
http://json-schema.org/understanding-json-schema/index.html | Understanding JSON Schema — Understanding JSON Schema 7.0 documentation
https://www.npmjs.com/package/json-schema-to-typescript | json-schema-to-typescript - npm
https://www.npmjs.com/package/typescript-json-schema | typescript-json-schema - npm
https://cswr.github.io/JsonSchema/spec/multiple_types/ | Multiple Types - JSON Schema
http://json-schema.org/understanding-json-schema/reference/generic.html#enumerated-values | Generic keywords — Understanding JSON Schema 7.0 documentation
