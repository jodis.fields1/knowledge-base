# Javascript future
https://github.com/nodejs/node/pull/18392 - alternative to the .mjs extension

https://github.com/tc39/proposals

https://kangax.github.io/compat-table/es6/

[ES6: The Bad Parts](https://benmccormick.org/2018/06/05/es6-the-bad-parts/)
* agree with some of these, esp. default exports

## symbols
"If you don’t have the reference for the Symbol, you just can’t use it. This also means two symbols will never equal the same value, even if they have the same description." per https://www.keithcirkel.co.uk/metaprogramming-in-es6-symbols/ except 'Symbol.for(). This method creates a Symbol in a “global Symbol registry”'

## polyfill
first off: don't for personal projects - but also see https://www.smashingmagazine.com/2018/10/smart-bundling-legacy-code-browsers/

## decorators
https://www.npmjs.com/package/core-decorators | core-decorators - npm
https://www.sitepoint.com/javascript-decorators-what-they-are/ | JavaScript Decorators: What They Are and When to Use Them — SitePoint
https://babeljs.io/docs/en/babel-plugin-proposal-decorators | @babel/plugin-proposal-decorators · Babel

## table of possibilities / compatibility
https://kangax.github.io/compat-table/es6/
    * see https://node.green/ also

## classes
TODO: try https://github.com/stampit-org/stampit as alternative to ES6 classes

## maps - new Map()
* note - cannot use methods like `reduce()` on these (https://github.com/tc39/proposal-collection-methods); ended up using tuples instead; seee

motivation: retaining order
    * Axel says they are guaranteed per ES6 https://medium.com/@rauschma/another-problem-with-using-object-literals-is-property-key-orders-are-not-guaranteed-3e42872fe8d

[What You Should Know About ES6 Maps](https://hackernoon.com/what-you-should-know-about-es6-maps-dc66af6b9a1e)
    * no mention of shorthand notation...

[Maps in ES6 - A Quick Guide](https://dev.to/mildrenben/maps-in-es6---a-quick-guide-35pk)
    * prolly best

[Is there a shorthand way to create a Map?](https://stackoverflow.com/questions/46043849/is-there-a-shorthand-way-to-create-a-map)
use entries to get the key, value pair: http://exploringjs.com/es6/ch_iteration.html#_iterable-computed-data
for (const pair of arr.entries()) {
    console.log(pair);
}

```
new Map([
    ['Neo4J', Neo4J.getDriver()],
    ['MongoDB', MongoDB.getDB()]
])
```

## modules
https://pencilflip.medium.com/using-es-modules-with-commonjs-modules-in-node-js-1015786dab03
* great matrix
* https://redfin.engineering/node-modules-at-war-why-commonjs-and-es-modules-cant-get-along-9617135eeca1
  * not quite as good

apparently allowSyntheticDefaultExports (from TS) was driven by babel? and babel made a big change from 5 to 6?
* https://stackoverflow.com/questions/33505992/babel-6-changes-how-it-exports-default
### async / await
[Mastering Async Await in Node.js](https://blog.risingstack.com/mastering-async-await-in-nodejs/) - author believes that these are imperatively styled, don't fit well with functional per the filter example

https://stackoverflow.com/questions/37576685/using-async-await-with-a-foreach-loop | javascript - Using async/await with a forEach loop - Stack Overflow
https://jonbellah.com/articles/async-await/ | A Look at Async/Await in ES2017 | JonBellah.com
https://stackoverflow.com/questions/44090197/what-is-the-correct-way-to-handle-nested-async-await-calls-in-node | javascript - What is the correct way to handle nested async await calls in Node? - Stack Overflow

### template literals
* [Template Strings ES6 prevent line breaks](https://stackoverflow.com/questions/38000659/template-strings-es6-prevent-line-breaks) - should try the \ option
* http://2ality.com/2016/05/template-literal-whitespace.html - discussion of common-tags lib

### 2016
var { foo, bar } = { foo: "lorem", bar: "ipsum" }; is "helpful syntactical shortcut for when the property and variable names are the same" per [this](https://hacks.mozilla.org/2015/05/es6-in-depth-destructuring/)

[Use cases for ES6 proxies](https://news.ycombinator.com/item?id=12124197) - ??

[https://esdiscuss.org/topic/how-to-fix-the-class-keyword-brendan-eich ](https://esdiscuss.org/topic/how-to-fix-the-class-keyword-brendan-eich)- suggests that there will be a default (non-new) function call

[https://esdiscuss.org/topic/how-to-fix-the-class-keyword](https://esdiscuss.org/topic/how-to-fix-the-class-keyword) - reception was extremely negative

**2015**
Generators: I read both [2ality](http://www.2ality.com/2015/03/es6-generators.html) and [MDN's in-depth](https://hacks.mozilla.org/2015/05/es6-in-depth-generators/) guides but didn't fully get them

[Getting started with ECMAScript 6](http://www.2ality.com/2015/08/getting-started-es6.html) (2015) - 

[Does async/await solve a real problem?](https://esdiscuss.org/topic/does-async-await-solve-a-real-problem) - 

[jeffmo/es-class-fields-and-static-properties](https://github.com/jeffmo/es-class-fields-and-static-properties) - intriguing
