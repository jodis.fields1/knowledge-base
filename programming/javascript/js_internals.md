See also: software_apps/browsers.md

### from scratchish
Sprocket - https://web.archive.org/web/20160314144031/http://browser.sed.hu/blog/20150714/sprocket-experimental-multiplatform-browser-based-content-api

### firefox
Gecko:
https://github.com/mozilla/gecko-dev/tree/RELEASE_BASE_20140602/js/src/builtin


### chromium / chrome browser
https://web.archive.org/web/20171222203512/https://szeged.github.io/sprocket/architecture_overview.html

Chromium / V8:
https://github.com/v8/v8/tree/6.1.164/src/js
https://www.chromium.org/developers/how-tos/getting-around-the-chrome-source-code

Mojo - IPC interface https://chromium.googlesource.com/chromium/src/+/master/mojo/README.md
also see the Service Manager https://chromium.googlesource.com/chromium/src/+/master/services/service_manager/README.md

#### team
Brett Wilson (brettw@chromium.org) - should have provided more context about not decoupling gn https://github.com/nodejs/node/issues/6089#issuecomment-206638182