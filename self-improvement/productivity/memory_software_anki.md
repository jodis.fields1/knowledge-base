[How To Remember Everything You Learn](https://www.youtube.com/watch?v=V-UvSKe8jW4&feature=youtu.be) - recall attempt, feynman attempt, spaced repetition

## TODO: wrap the repository
https://www.juliensobczak.com/tell/2016/12/26/anki-scripting.html | Anki Scripting 101: Automate your flashcards
https://apps.ankiweb.net/docs/addons.html | Writing Anki 2.0.x Add-ons


https://github.com/FooSoft/anki-connect | FooSoft/anki-connect: Anki plugin to expose a remote API for creating flash cards.
https://docs.python.org/3/using/cmdline.html | 1. Command line and environment — Python 3.7.0 documentation
https://ankiweb.net/shared/info/2055492159 | AnkiConnect - AnkiWeb
https://github.com/dae/anki/pull/58 | Allow a saner, cleaner and standard default linux conf file storage location by jaseg · Pull Request #58 · dae/anki


TODO: keep an eye on https://github.com/TheBrainFamily/TheBrain2.0 and https://github.com/julien-sobczak/memorio


TODO: 
deep dive into the manual per https://www.reddit.com/r/Anki/comments/2gdukp/how_to_enable_typing_in_answers_on_anki/

TODO: make anki notes searchable

### cli
Settled on Anki, but need a CLI client - Clanki? https://github.com/marcusbuffett/Clanki/issues/5

#### anki link dump 2018-06
https://github.com/joedel/spaced-repetition | joedel/spaced-repetition: Remember everything with spaced repetition
https://www.linkedin.com/in/henryhutcheson/ | Henry Hutcheson | LinkedIn
https://github.com/itsdawomb/ebblite | itsdawomb/ebblite: command line flashcard program. uses ebbinghaus retention model.
https://github.com/theq629/fulgurate | theq629/fulgurate: Simple spaced repetition flashcard software.
https://github.com/darius/spaced-out | darius/spaced-out: Vocab drill using parallel corpora, plus classic spaced-repetition drill
https://askubuntu.com/questions/157147/does-there-exist-a-cli-spaced-repetition-program | software recommendation - Does there exist a CLI spaced repetition program? - Ask Ubuntu
https://github.com/hnykda/Saurus | hnykda/Saurus: This is repository to collect ideas about developing new Space Repetition Software, similar to ANKI
https://mnemosyne-proj.org/whats-new | What's new | The Mnemosyne Project
https://github.com/jotron/StudyMD | jotron/StudyMD: Flashcards from Markdown. Built with React and Electron
https://github.com/topics/flashcard-application | Topic: flashcard-application


### reflections / tips
[The use of spaced repetition memory systems has changed my life...](https://twitter.com/michael_nielsen/status/957763229454774272?lang=en)
    * followed by http://augmentingcognition.com/ltm.html

## old
https://hundredminutehack.blogspot.com/2016/07/flashmark-what-it-is-and-why-i-made-it.html?m=1

Enki - my favorite; open-sourced at https://github.com/enkidevs/curriculum
CodeCards - Anki for code discovered thru https://news.ycombinator.com/item?id=14155074
Anki - configured to be portable per   [http://ankisrs.net/docs/manual.html#running-from-a-flash-drive](http://ankisrs.net/docs/manual.html#running-from-a-flash-drive), also tried [runki](https://github.com/seletskiy/runki) but kind of pointless as it just adds cards

Mnemosyne Portable-   [buggy](http://portableapps.com/node/37095)! per 