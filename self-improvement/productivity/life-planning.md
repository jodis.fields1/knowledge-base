* [Does Living a Remarkable Life Require Courage or Effort?](http://calnewport.com/blog/2009/07/22/does-living-a-remarkable-life-require-courage-or-effort/) - 


* [How to Make a Big Decision](https://www.nytimes.com/2018/09/01/opinion/sunday/how-make-big-decision.html) (2018-09) - diverse advice, premortem; see https://news.ycombinator.com/item?id=17914959

TODO: find my prior notes on this topic?
    * self-help.md?
1
## motivational
[[Advice] How and Why I developed a habit of productivity in my free time](https://www.reddit.com/r/getdisciplined/comments/6q4ur4/advice_how_and_why_i_developed_a_habit_of/)

[How the Founder of Game CoLab Leveled Up in Life and Business](http://successlabaz.com/how-the-founder-of-game-colab-leveled-up-in-life-and-business/)

[Great Talks Most People Have Never Heard](https://jamesclear.com/great-speeches?__s=2cgp7sv5wssqvdjop5ir)

https://en.wikipedia.org/wiki/David_Goggins - recommended by everyone

## life skills
* http://bewelltuned.com/ -
  * [Tune Your Emotional Processing](http://bewelltuned.com/)
      * http://bewelltuned.com/gendlins_focusing - I do have lingering, nagging emotions in the back of my mind


### productivity
Keys to productivity - is this correct?


[Ask HN: How do you organize everything you want to do?](https://news.ycombinator.com/item?id=18891069) - day, week, month, year approach?

1. - work on the right thing?
2. - work smart
3. - work fast - shortcuts, automation

https://www.scotthyoung.com/blog/2019/09/10/indistractable/

[Cultivating a Deep Life](https://www.calnewport.com/blog/2020/04/20/cultivating-a-deep-life/)
* Newport's grid

## decision versus information-gathering
* see Tynan on consolidation
* * [How Do You Manage Your Intake Of Information?](https://www.reddit.com/r/slatestarcodex/comments/a3unag/how_do_you_manage_your_intake_of_information/)

## apps - idealweek planning / time mangement 
TODO: where are those time-tracking apps?

[How To Stop Wasting Time On The Internet](https://www.bakadesuyo.com/2018/11/wasting-time-on-the-internet/)
* office hours?

https://radreads.co/time-audit/

LiquidTime - I like it a lot
Ideal Week Planner - https://idealweekplanner.com/ Chrome Extension for $9
https://michaelnichols.org/create-ideal-week/

### background
https://michaelhyatt.com/ideal-week/

### inspirations
The Big Deal - saw this from Lorie Marrero's https://www.clutterdiet.com/

https://sivers.org - Anything You Want

[13. How do you become a fearless programmer?](http://v25media.s3.amazonaws.com/edw519_mod.html#chapter_13)

See https://www.reddit.com/r/getdisciplined/comments/1ugskx/from_someone_who_has_tried_failed_and_succeeded_3/ which recommends pics from placeboeffect.com 

Pieter Levels - https://wip.chat/@levelsio
* https://wip.chat/@levelsio

General advice: http://jamesclear.com/productivity
https://www.reddit.com/r/getdisciplined/comments/6q4ur4/advice_how_and_why_i_developed_a_habit_of/ - cool spreadsheet

## decision advice
https://news.ycombinator.com/item?id=17840275 | If you’re unsure whether to quit your job or break up, you probably should | Hacker News
https://www.amazon.co.uk/Decisive-How-Make-Better-Decisions/dp/1847940862 | Decisive: How to Make Better Decisions: Amazon.co.uk: Chip Heath, Dan Heath: 9781847940865: Books

[How do you manage / plan your life?](https://news.ycombinator.com/item?id=15626233)
    * Semantic Synchrony: https://github.com/synchrony/smsn/wiki is intriguing
[How Successful People Make Decisions Differently](https://www.fastcompany.com/40444808/how-successful-people-make-decisions-differently)

## day by day
[Effective Engineer - Notes](https://gist.github.com/rondy/af1dee1d28c02e9a225ae55da2674a6f)
* http://www.effectiveengineer.com/