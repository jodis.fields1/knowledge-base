WANTED: tab-history from back https://github.com/qutebrowser/qutebrowser/issues/1561#issuecomment-224810978
* similar want for vimium

http://qutebrowser.org/ | qutebrowser | qutebrowser
* see vimium_qtebrowser.md

https://github.com/qutebrowser/qutebrowser/issues/1504 | Using the developer tools with a keyboard · Issue #1504 · qutebrowser/qutebrowser
https://github.com/qutebrowser/qutebrowser/issues/3101 | macOs How to enable webinspector · Issue #3101 · qutebrowser/qutebrowser

## shortcuts
* https://www.shortcutfoo.com/app/dojos/qutebrowser - recommended by quickstart
* https://www.shortcutfoo.com/app/dojos/qutebrowser/cheatsheet

## quick start
http://qutebrowser.org/doc/quickstart.html
* quite confusing if you do not read this