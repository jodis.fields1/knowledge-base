# Firefox & Chromium shortcuts
https://developer.mozilla.org/en-US/docs/Web/API/KeyboardEvent/key/Key_Values

## firefox
problems in customization

* https://superuser.com/questions/1271147/change-key-bindings-keyboard-shortcuts-in-firefox-quantum
    * https://bugzilla.mozilla.org/show_bug.cgi?id=1303384
* https://www.reddit.com/r/firefox/comments/97k26s/remappable_keyboard_shortcuts_when/
* [Add global keyboard shortcut support to commands API](https://bugzilla.mozilla.org/show_bug.cgi?id=1411795) - actually more of a niche type of key
* https://superuser.com/questions/1272551/disable-single-shortcut-in-firefox-quantum - nope

## chromium
These are a major problem... cannot customize??
https://superuser.com/questions/tagged/google-chrome+keyboard-shortcuts

https://superuser.com/questions/497526/how-to-customize-google-chrome-keyboard-shortcuts | How to customize Google Chrome keyboard shortcuts? - Super User
https://productforums.google.com/forum/#!topic/chrome/O7-7ILcLYeE | Google Groups
https://superuser.com/questions/497526/how-to-customize-google-chrome-keyboard-shortcuts/1149685#1149685 | How to customize Google Chrome keyboard shortcuts? - Super User
https://www.reddit.com/r/chrome/comments/51e8dx/help_how_to_disable_chromes_default_keyboard/ | [Help] How to disable Chrome's default keyboard shortcuts? : chrome
https://stackoverflow.com/questions/45347403/assign-command-keyboard-shortcut-from-popup-or-options?rq=1 | google chrome extension - Assign command keyboard shortcut from popup or options - Stack Overflow
https://unix.stackexchange.com/questions/292868/how-to-customise-keyboard-mappings-with-wayland | How to customise keyboard mappings with Wayland - Unix & Linux Stack Exchange
https://superuser.com/questions/347359/any-keyboard-shortcut-for-back-in-chromium-browser | google chrome - Any keyboard shortcut for "Back" in Chromium browser? - Super User
https://superuser.com/questions/809994/disable-chrome-task-manager-alternatively-prevent-extensions-from-being-killed | windows 7 - disable chrome task manager? alternatively: prevent extensions from being killed - Super User
https://bugs.chromium.org/p/chromium/issues/detail?id=317276 | 317276 - Shift-Esc to open Task Manager is not working in certain websites - chromium - Monorail
