NOTE: you may need to turn word wrap off!!



| Shortcut                        | Code          | Jetbrains    | Atom        | Sublime          | My Choice |
|---------------------------------|---------------|--------------|-------------|------------------|-----------|
| Add snippet/template            | Alt+Shift+S   | Cmd+J        | Alt+Shift+S | Tab (implicit)   | ?         |
| Code fold all                   | centered      | Numpad mess  | ?           | CMD+K, CMD+[num] | ?         |
| Code fold subsection            | right-aligned |              |             |                  | ?         |
| Select block-> multiple cursors | ?             | ? see plugin |             | CMD+SHIFT+L      | ?         |
| Move pane left                  |               |              |             |                  |           |
| Switch tab                      |               |              |             | CMD+SHIFT+[      | CTRL+[    |

## sublime
the original...
* [Sublime Text Shortcuts for Writing](http://www.macdrifter.com/2012/08/sublime-text-shortcuts-for-writing.html)
* 
* [Sublime Keyboard Shortcuts - OSX](http://docs.sublimetext.info/en/latest/reference/keyboard_shortcuts_osx.html)
    * sane defaults
    * http://robdodson.me/sublime-text-2-tips-and-shortcuts/

## jetbrains
* [10 WebStorm Shortcuts You Need to Know](https://blog.jetbrains.com/webstorm/2015/06/10-webstorm-shortcuts-you-need-to-know/)

## atom
* https://github.com/nwinkler/atom-keyboard-shortcuts

