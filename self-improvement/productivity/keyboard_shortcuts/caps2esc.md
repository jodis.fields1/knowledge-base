
## linux
ultimately interception tools is best, available as nixos module

## mac
karabiner

## 2018-05 link dump
https://github.com/tekezo/Karabiner-Elements/issues/18 | Control_L to Control_L (+ when you type Control_L only, send Escape) · Issue #18 · tekezo/Karabiner-Elements
https://bugs.chromium.org/p/chromium/issues/detail?id=587777 | 587777 - Chrome has no option to disable the ctrl-q shortcut - chromium - Monorail
https://apple.stackexchange.com/questions/132564/how-can-i-remap-caps-lock-to-both-escape-and-control | macos - How can I remap Caps lock to both Escape and Control? - Ask Different
https://unix.stackexchange.com/questions/392596/remapping-caps-lock-to-left-control-and-left-control-to-super-key-using-xmodmap | keyboard - Remapping Caps Lock to left control, and left control to Super key using xmodmap - Unix & Linux Stack Exchange
https://github.com/oblitum/caps2esc | oblitum/caps2esc: Transforming the most useless key ever in the most useful one
https://github.com/tekezo/Karabiner-Elements | tekezo/Karabiner-Elements: Karabiner-Elements is a powerful utility for keyboard customization on macOS Sierra (10.12) or later.
https://unix.stackexchange.com/questions/377600/in-nixos-how-to-remap-caps-lock-to-control | In nixos, how to remap caps lock to control? - Unix & Linux Stack Exchange
https://github.com/tekezo/Karabiner-Elements/issues/1320 | Exchange command+arrows with option+arrows doesn't seem complete · Issue #1320 · tekezo/Karabiner-Elements