keywords: keyboard shorcuts

TODO: publish KDE shortcut "themes"

inspired by https://github.com/Microsoft/vscode/wiki/Keybinding-Issues
* use https://github.com/Microsoft/node-native-keymap? or electron will handle it anyhow

## learning
https://github.com/halirutan/IntelliJ-Key-Promoter-X

## Kinesis Advantage
Delete -> Cmd \
\| -> +/= \
Caps Lock -> Ctrl \

* [How I Work](https://codequizzes.wordpress.com/2013/12/29/how-i-work/)
* [Optimized Layout for Kinesis Advantage](https://stackoverflow.com/questions/4198331/optimized-layout-for-kinesis-advantage)


## hardware
varieties

### scancodes
2020:
* https://wiki.archlinux.org/index.php/Keyboard_input
* [How to convert USB keyboard packet or usage id/code to ASCII or other convenient format?](https://stackoverflow.com/questions/43830339/how-to-convert-usb-keyboard-packet-or-usage-id-code-to-ascii-or-other-convenient)
  * 1e is 4; also 0x070004 is the HID Usage ID
  * from chromium source linked above, found HID Usage table at https://web.archive.org/web/20141127044412/http://www.usb.org/developers/hidpage/Hut1_12v2.pdf
  * table: http://kbd-project.org/docs/scancodes/scancodes-10.html

#### showkey
NOTE: scancode showkey is not ESR's showkey (https://stackoverflow.com/questions/3463995/showkey-equivalent-on-mac-os-x), which I compiled and ran https://gitlab.com/esr/showkey/-/blob/master/Makefile but does not show scancodes

* Linux kernel is off by a byte from xev
  * per https://unix.stackexchange.com/questions/120589/why-does-showkey-show-a-different-keycode-compared-to-xevdoes


set 1, 2, 3 of scancodes
* Linux might be using 3 by now?
* per https://en.wikipedia.org/wiki/Scancode and https://en.wikipedia.org/wiki/Keyboard_layout
https://homepages.cwi.nl/~aeb/linux/kbd/scancodes-7.html | Keyboard scancodes: Keyboard-internal scancodes


## 2018-04 link dump
https://wiki.archlinux.org/index.php/Extra_keyboard_keys#Scancodes | Extra keyboard keys - ArchWiki
https://stackoverflow.com/questions/45774113/how-to-translate-x11-keycode-back-to-scancode-or-hid-usage-id-reliably | linux - How to translate X11 keycode back to scancode or hid usage id reliably - Stack Overflow
https://www.win.tue.nl/~aeb/linux/kbd/scancodes-10.html | Keyboard scancodes: Keyboard-internal scancodes
http://www.quadibloc.com/comp/scan.htm | Scan Codes Demystified
https://www.linuxjournal.com/article/1080 | The Linux keyboard driver | Linux Journal
https://unix.stackexchange.com/questions/116629/how-do-keyboard-input-and-text-output-work/116630 | x11 - How do keyboard input and text output work? - Unix & Linux Stack Exchange
https://en.wikipedia.org/wiki/X_Window_System_core_protocol#Mappings | X Window System core protocol - Wikipedia
http://www.linuxintro.org/wiki/Understand_how_keyboards_work | Understand how keyboards work - LinuxIntro

https://defkey.com/
http://www.shortcutworld.com/