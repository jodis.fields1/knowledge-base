https://en.wikipedia.org/wiki/Template:Keyboard_keys

See hyper-group for how to figure out codes

Ctrl+Shift+E has some issues potentially on Linux per https://github.com/microsoft/vscode/issues/48480 and https://unix.stackexchange.com/questions/394655/ctrlshifte-causes-beeping/394767#394767

## 2018-05 dump (giving up)
https://superuser.com/questions/1300552/use-meta-super-for-all-keyboard-shortcuts-in-kde-at-least-for-kde-apps | linux - Use Meta (Super) for all keyboard shortcuts in KDE (at least for KDE apps)? - Super User
https://unix.stackexchange.com/questions/28993/what-is-bashs-meta-key | keyboard shortcuts - What is bash's meta key? - Unix & Linux Stack Exchange
https://github.com/keepassxreboot/keepassxc/issues/826 | cmd-shift-[ / cmd-shift-] / cmd-{ / cmd-} should switch tabs · Issue #826 · keepassxreboot/keepassxc
https://www.pelicanhpc.org/tutorial/base.lst.pdf | https://www.pelicanhpc.org/tutorial/base.lst.pdf
https://stackoverflow.com/questions/41364833/karabiner-for-linux | macos - Karabiner for Linux? - Stack Overflow
https://github.com/simos/keyboardlayouteditor/issues | Issues · simos/keyboardlayouteditor
https://stackoverflow.com/questions/6036829/clear-scrollback-buffer-of-linux-virtual-console-terminals | clear scrollback buffer of Linux Virtual Console Terminals - Stack Overflow
https://blog.cwrichardkim.com/keyboard-shortcuts-on-steroids-90e24bf475e7 | 
https://superuser.com/questions/309772/what-is-the-commandk-in-mac-equivalent-in-ubuntu | what is the command+k ( in mac ) equivalent in ubuntu - Super User
https://www.google.com/search?q=vim+ctrl%2Bw+use+ctrl%2Bk+instead&oq=vim+ctrl%2Bw+use+ctrl%2Bk+instead&aqs=chrome..69i57.13336j0j7&client=ms-android-om-lge&sourceid=chrome-mobile&ie=UTF-8 | vim ctrl+w use ctrl+k instead - Google Search
https://vi.stackexchange.com/questions/3699/is-there-a-command-to-enter-visual-block-mode | key bindings - Is there a command to enter Visual Block mode? - Vi and Vim Stack Exchange
https://superuser.com/questions/379152/looking-for-non-conflicting-cross-platform-keybinding-consistency | keyboard shortcuts - Looking for non-conflicting cross-platform keybinding consistency - Super User
https://www.google.com/search?hl=en&q=disable%20ctrl%2Bshift%2Bc%20for%20copy%20in%20konsole | disable ctrl+shift+c for copy in konsole - Google Search
https://www.google.com/search?hl=en&ei=BRADW5ryJIK45gKIsbg4&q=emacs+ctrl%2Be+end+of+line&oq=emacs+ctrl%2Be+end+of+line&gs_l=psy-ab.3..33i160k1.17653.18730.0.19352.12.8.0.0.0.0.376.788.0j1j1j1.3.0....0...1c.1.64.psy-ab..9.2.646...33i21k1.0.F76KAZcDLew | emacs ctrl+e end of line - Google Search
