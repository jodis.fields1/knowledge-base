vimium is clear leader with 9k github stars

WANTED: https://github.com/philc/vimium/issues/1496 | Add an option for displaying the tab history · Issue #1496 · philc/vimium

## debugging links showing up
* noticed some links I coded weren't showing up and noticed that the React onclick listeners were bound to emptyFunction.js but that was unrelated: they needed to be wrapped in <button>

## pain points
TODO: fix new tab page (prolly use tabliss https://github.com/joelshepherd/tabliss/issues/88)
* https://github.com/philc/vimium/issues/531#issuecomment-60481641
* https://github.com/philc/vimium/issues/2845#issuecomment-349904212 - this only works for the "t" keyboard shortcut

does not work on chrome:// pages:
* [enable on chrome:// URLs](https://github.com/philc/vimium/issues/2090)
* [Can you access chrome:// pages from an extension?](https://stackoverflow.com/questions/19042857/can-you-access-chrome-pages-from-an-extension)

## vimium general 2018-5 link dump
https://github.com/philc/vimium/pull/2871 | Detect click listeners for link hints. by smblott-github · Pull Request #2871 · philc/vimium
https://superuser.com/questions/1236864/how-to-use-vimium-to-select-text-from-a-page | google chrome - How to use vimium to select text from a page - Super User
https://fzheng.me/2018/03/17/practical-vim-modes/ | Practical Vim: Modes
https://lifehacker.com/how-to-open-chrome-links-with-your-keyboard-1822942962 | How to Open Chrome Links With Your Keyboard
https://www.slant.co/topics/11931/~web-browsers-or-browser-extensions-allowing-keyboard-navigation | 5 Best web browsers or browser extensions allowing keyboard navigation as of 2018 - Slant
https://vimium.github.io/ | Vimium - the hacker's browser
https://www.google.com/search?hl=en&q=vim%20chrome%20devtools | vim chrome devtools - Google Search
https://stackoverflow.com/questions/32599201/vim-keybinding-inside-chrome-dev | vi - Vim keybinding inside Chrome dev - Stack Overflow
https://news.ycombinator.com/item?id=10418124 | Chrome text editor supports vim and emacs mode, but is not configurable | Hacker News
https://www.reddit.com/r/vim/comments/6hjl68/what_are_my_options_for_a_modern_future_proof_vim/ | What are my options for a modern & future proof "vim browsing experience"? : vim
https://stackoverflow.com/questions/40177659/fixing-focus-out-on-pressing-a-esc-key | javascript - fixing focus out on pressing a "esc" key - Stack Overflow


## vimium alternatives
Surfingkeys is second with active maintainence, 1k stars

### rejected
### saka keys
got around gmail stealing focus, but needs a maintainer
* [https://github.com/lusakasa/saka-key/issues/213](Saka stopped working) - broke randomly
* https://github.com/lusakasa/saka-key/issues/197 - breaks some pages


#### chromium-vim (cvim)
* could not figure out how to map switch tab over in insert mode at a quick glance, seems not very well-maintained?
    * [map (keybind) | tab-navigation | K, J vs. <C-n>, <C-p>](https://github.com/1995eaton/chromium-vim/issues/304)

### maybe
#### qutebrowser
See dedicated note