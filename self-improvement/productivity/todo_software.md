See zenkit scheduled task as well

http://www.orgzly.com/ - mobile focused

tasks.org - open-source
* https://github.com/tasks
* https://play.google.com/store/apps/details?id=org.tasks


## TO-INVESTIGATE
I already know I'll go with open-source, but check these for cool UX:
create tasks from conversations slack competitor?

* clickup.com - found from https://community.bitwarden.com/t/publish-roadmap/95/6
  * saw bug (permissions undefined),  open bugs at https://* clickup.canny.io/bugs?sort=top; https://clickup.canny.io/ for roadmap
  * lots of cool integrations tho;
* notion.so -
 * cannot find open bug tracker 
 * no official linux support; basically airtable (see other discussion on it)
 * https://www.reddit.com/r/productivity/comments/9clzw1/anyone_extensively_use_notion/ for inspiration
* dynalist - wins versus notion in slant https://www.slant.co/versus/15546/15790/~dynalist_vs_notion
  * open bug tracker! https://talk.dynalist.io/c/bugs/l/latest?order=views
* moo.do - stuff seems broken
* germ.io seems mostly dead
* https://coda.io/ - interesting notion competitor


## apps

### open-source trello clone
https://github.com/ajtoo/vscode-org-mode | ajtoo/vscode-org-mode: Emacs Org Mode for Visual Studio Code (In Progress, contributions are welcome)
https://github.com/kiswa/TaskBoard/tree/re-write | kiswa/TaskBoard at re-write
https://github.com/wekan/wekan/issues/299 | Switching to React · Issue #299 · wekan/wekan
https://github.com/bigardone/phoenix-trello | bigardone/phoenix-trello: Trello tribute done in Elixir, Phoenix Framework, React and Redux.
https://github.com/wekan/wekan | wekan/wekan: The open-source Trello-like kanban (built with Meteor)

### zenkit
https://medium.com/@MuratDikmen/i-tried-to-switch-from-my-trello-todoist-setup-to-zenkit-before-zenkit-2-0-1f211ea31e9d captures my frustration - quick adding does not work
more of a big project thing for now

### taskwarrior
TODO: use https://wingtask.com/
TODO: only show certain tasks at certain times of day
TODO: http://cyborginstitute.org/projects/taskn/
https://github.com/coddingtonbear/inthe.am/issues - looks awesome, but too many bugs / not enough active dev

taskwarrior - recommended by Tynan, relatively strong ecosystem
* how does this compare to todo.txt? or others at https://alternativeto.net/software/taskwarrior/?license=opensource
    * see #other_open-source_rejected
* C++ is unfortunate
* Android app: https://bitbucket.org/kvorobyev/taskwarriorandroid/wiki/Home
    * https://github.com/beatgammit/task-warrior-am
* notebook system: http://cyborginstitute.org/projects/taskn/
    * also see https://github.com/ValiValpas/taskopen for annotations

#### retrospectives
[Return to taskwarrior](https://cs-syd.eu/posts/2016-02-21-return-to-taskwarrior)
[Taskwarrior – intelligent TODO list](https://news.ycombinator.com/item?id=11601784)
* "great idea for simplicity, but it just breaks down quickly"

### other (rejected?)
* gtdnext.com - not open-source, but otherwise great
* doit.im - [Hello anybody home?](http://help.doit.im/group/topic/182) - no response
    don't like Ubuntu Launchpad as a tracker (no priority for UX)
    * as of 2018-04 still developed, but no web client, source on sourceforge and using mercurial
* workflowy - dynalist is better
* zenkit - jira clone
* matterlist.com - just a mobile app

#### other open-source rejected
* [Task Coach](http://www.taskcoach.org/)
* todo.txt
    * community-owned per [Community and The Future of Todo.txt](http://blog.todotxt.org/post/164525181522/community-and-the-future-of-todotxt)
    * main developer of android app switched to taskwarrior per https://mpcjanssen.nl/post/free-tasks/
        * "This works fine, but a format like todo.txt has one big disadvantage: it doesn’t have a concept of task identity so any syncing solution is flaky"
    * https://github.com/mpcjanssen/simpletask-android/issues/457 -> https://github.com/mpcjanssen/simpletask-taskwarrior

##  todo software search
ALL REPLACED BY TASKWARRIOR!!

### 2018
https://monday.com/features/ - pretty cool

### 2017
https://github.com/opf/openproject -

GTD Agile tools---
2017:
roadmunk - seems like the winner?
productplan - good but too expensive
Jira - oh well
targetprocess - found thru zenhub, looks awesome
* very software team focused
Breeze.pm - my favorite thus far
Taigi - leading open-source tool
kanbanize - has burndown charts
Favro - good roadmap at https://favro.com/organization/54f72cf77fb4776c31d006ee/670116871a17b43c37a3fa20
http://www.pipefy.com/ - automate business processes?
proofhub - seems pretty good
fluxday - Objectives and Key Resuilts
HelloFocus - lots of words about science, but few screenshots
KanbanFlow - another one of those, no public bug tracker

### 2015
http://www.degconsulting.net/2013/07/review-evernote-gtd-guide.html solid summary
http://heydave.org/post/26770221775/my-productivity-system
Todoist - eh, OK
Omnifocus - notably limited to only one context per action, see discussion where people list contexts
Workflowy - tested based on reddit, seems nice but not enough to switch esp. with lack of RTF
Zendone - noticed there were lots of bugs, closed their forum
nachapp - https://nachapp.com/ is cool, run by James Isaac https://twitter.com/dotajames
