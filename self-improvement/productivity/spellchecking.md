This is a bit of CI / editor thing...
Search: open-source dictionaries

https://github.com/wordset/wordset-dictionary | wordset/wordset-dictionary: The Open Source Dictionary
https://www.jetbrains.com/help/phpstorm/spellchecking.html | Spellchecking - Help | PhpStorm
https://github.com/spell-checker/dictionary-en | spell-checker/dictionary-en: English dictionary for spell-checker and PhpStorm/InteliJ
https://github.com/GNUAspell/aspell | GNUAspell/aspell
https://english.stackexchange.com/questions/8233/largest-open-source-dictionary-w-brief-definitions-not-wiktionary | dictionaries - Largest open-source dictionary w/ brief definitions (not wiktionary) - English Language & Usage Stack Exchange
