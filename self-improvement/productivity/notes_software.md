commonplace book (hypomemna)
==========================
Status: **Mostly resolved** via VSCode markdown files \

https://en.wikipedia.org/wiki/Open-notebook_science

https://foambubble.github.io/
* open-source copy of Roam

[Trilium Notes: Markdown autoformat personal knowledge base](https://news.ycombinator.com/item?id=23335759)

## vscode csv
* https://github.com/janisdd/vscode-edit-csv/issues/13
* https://marketplace.visualstudio.com/items?itemName=RandomFractalsInc.vscode-data-preview

https://stenci.la/blog/introducing-sheets/

[Show HN: Grid.js – Advanced table library that works everywhere](https://news.ycombinator.com/item?id=23420091)


## 2018
* [Show HN: Everything I Know Wiki](https://news.ycombinator.com/item?id=19468993)
* TODO: [Superfolder](https://github.com/galfarragem/superfolder)
* [Ask HN: How do you take notes?](https://news.ycombinator.com/item?id=17799232)

### commercial
* https://notejoy.com/

[Show HN: WorldBrain – full text, local search of your browsing history](https://news.ycombinator.com/item?id=17743352)

### experiments / half-baked
https://usetangle.com/blog
* founded by Myles McGinley, Cameron Porter @Cam39Porter
* https://news.ycombinator.com/user?id=curo - Hakeema is an app to leverage your knowledge online?

### open-source
* TODO: [Micropad](https://news.ycombinator.com/item?id=17580909)
* https://github.com/laurent22/joplin
* MindForger - https://www.reddit.com/r/linux/comments/8qznev/check_mindforger_open_source_markdown_ide/

### dicussions
* [Ask HN: Favorite note-taking software?](https://news.ycombinator.com/item?id=17532094) \
* [Ask HN: Why do you keep a personal knowledge base?](https://news.ycombinator.com/item?id=17530498)

Most of these apps are Mac only, such as market leader DEVONThink
[http://en.wikipedia.org/wiki/Comparison_of_notetaking_software](http://en.wikipedia.org/wiki/Comparison_of_notetaking_software)

## examples of people
[Keeping a plaintext “did” file](https://news.ycombinator.com/item?id=17542961) - splits of vim
https://github.com/MorganGeek/bookmarks

## command-line based
taskwarrior
https://github.com/scottnonnenberg/thoughts-system

##### Paper alternative
https://inkandvolt.com/product/volt-planner/

##### 2017
Simplenote - open-source react / redux desktop
Leanote - Chinese app
Inkdrop - programmer focused
Boostnote - programmer focused
Standard Note - open-source "standard"
https://gingkoapp.com - minimal tree of markdown text, exports, updated blog http://blog.gingkoapp.com/

##### 2016
DEPRECATED evernote and began migration... eventually found [Exporting Evernote to Markdown](https://diego.org/2016/08/31/exporting-evernote-to-markdown/)

Happen Apps Quiver - markdown, code etc support
[Kajero](http://www.joelotter.com/kajero/) - like a Jupyter notebook for js
[https://github.com/levlaz/braindump](https://github.com/levlaz/braindump) - dropped in favor of standard notes
milanote - kinda cool, notes for designers

#### 2015 
[Andrew's Dream Word Processor](http://3x84xo.axshare.com/home.html) - HackReactor guy's idea

#### 2014 and earlier
OneNote - lacking [Way to easily archive old notebooks. In non-propietary format for long-term archival](https://onenote.uservoice.com/forums/362784-education/suggestions/17289824-way-to-easily-archive-old-notebooks-in-non-propie)
