
LibreOffice - keeps a list of [Most Annoying Bugs](https://wiki.documentfoundation.org/QA/Most_Annoying_Bugs)

[Using WikiData in Spreadsheets (Calc)](https://www.reddit.com/r/libreoffice/comments/cy94l7/using_wikidata_in_spreadsheets_calc/)

NOTE:
* Pain point: deselecting text in cell after typing "=" and selecing w/ mouse
  * https://ask.libreoffice.org/en/question/39770/how-to-deselect-cells-in-calc/
  * solution: use CTRL+key


## alternatives
See software_office.md

Why not Google Office suite? Churn risk. But I end up using it mostly anyhow. And used Excel / VBA for years, no...

https://developers.google.com/apps-script/reference/spreadsheet/ | Spreadsheet Service  |  Apps Script  |  Google Developers

## scripting
https://stackoverflow.com/questions/51143831/how-can-i-programmatically-create-an-odt-document | node.js - How can I programmatically create an ODT document? - Stack Overflow
https://github.com/brightideainc/commonwealth/blob/master/AnthonyAstige/biAudit/index.js | commonwealth/index.js at master · brightideainc/commonwealth

### python
https://www.google.com/search?hl=en&q=libreoffice%20PyUNO | libreoffice PyUNO - Google Search
https://stackoverflow.com/questions/7784438/how-do-you-install-or-activate-pyuno-in-libreoffice | python - How do you install or activate PyUno in LibreOffice? - Stack Overflow
http://christopher5106.github.io/office/2015/12/06/openoffice-libreoffice-automate-your-office-tasks-with-python-macros.html | Interface-oriented programming in OpenOffice / LibreOffice : automate your office tasks with Python Macros
https://stackoverflow.com/questions/11638170/what-language-do-i-need-to-write-macros-in-libre-office-calc | python - what language do I need to write macros in LIbre Office Calc? - Stack Overflow
https://stackoverflow.com/questions/9205590/can-you-use-google-apps-script-with-python | Can you use Google Apps Script with Python? - Stack Overflow

### javascript
third-class citizen, don't use it
https://github.com/loveencounterflow/coffeelibre | loveencounterflow/coffeelibre: Scripting Libre(Open/Neo)Office with CoffeeScript
https://ask.libreoffice.org/en/question/98257/javascript-macro-reference/ | JavaScript macro reference - Ask LibreOffice
https://api.libreoffice.org/docs/idl/ref/servicecom_1_1sun_1_1star_1_1script_1_1JavaScript.html | LibreOffice: JavaScript Service Reference
https://www.google.com/search?hl=en&q=libreoffice%20uno%20javascript | libreoffice uno javascript - Google Search


## version controlling
https://stackoverflow.com/questions/38571244/which-output-format-of-libre-office-can-i-use-to-track-the-history-of-my-files | git - Which output format of Libre Office can I use to track the history of my files? - Stack Overflow
https://ask.libreoffice.org/en/question/148892/attempt-to-preserve-fodt-format-names/ | Attempt to Preserve .fodt Format Names - Ask LibreOffice

https://superuser.com/questions/1264379/libreoffice-how-to-save-in-git-friendly-format | LibreOffice: How to save in Git friendly format? - Super User
CONCLUSION: save your files as flat XML (fodt) per [Is there a way in which I can tell LibreOffice to save in a Git friendly format?](https://superuser.com/questions/1264379/libreoffice-how-to-save-in-git-friendly-format)

https://askubuntu.com/questions/774221/is-there-a-vcs-like-system-on-libreoffice-documents | version control - Is There A VCS-Like System on Libreoffice Documents - Ask Ubuntu
https://ask.libreoffice.org/en/question/61946/git-version-control-in-libreoffice/ | 
https://github.com/Nicola17/ODT-Git-helper | Nicola17/ODT-Git-helper: this python script gives you the possibility to extract/compress all files needed to keep track of your ODT files modifications into a Git repository
https://git.wiki.kernel.org/index.php/GitTips#How_to_use_git_to_track_OpenDocument_.28OpenOffice.2C_Koffice.29_files.3F | GitTips - Git SCM Wiki
https://stackoverflow.com/questions/30728630/what-should-i-do-if-i-put-ms-office-e-g-docx-or-openoffice-e-g-odt-docum | github - What should I do if I put MS Office (e.g. .docx) or OpenOffice( e.g. .odt) document into a git repository? - Stack Overflow
https://ask.libreoffice.org/en/question/39512/disable-mouse-zoom-in-osx/ | Disable mouse zoom in osx [closed] - Ask LibreOffice
https://stackoverflow.com/questions/17836774/intellij-idea-on-mac-how-to-disable-pinch-gesture-for-zooming | macos - Intellij IDEA on mac: how to disable pinch gesture for zooming - Stack Overflow

