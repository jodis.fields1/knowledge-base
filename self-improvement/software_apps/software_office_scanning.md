Scanning
===

See also ./_software_apps.md

No paper, ever!

Found that some of my old notes had faded pencil which was hard to OCR; firm pen seems better

## services
http://www.mementopress.com/products_services/scanning/#shoebox | Scanning - Premium & Value, Not Outsourced | Memento Press
http://www.digitalrootsstudio.com/photo-scanning-service.html | Photo Scanning to Digital - SF Bay Area
http://1dollarscan.com/work.php | 1DollarScan - How it Works
https://www.yelp.com/biz/digital-roots-studio-oakland?hrid=K16ZdP4vF6Rf0eaOucanmw&osq=photo+scanning | Digital Roots Studio - 31 Photos & 58 Reviews - Video/Film Production - 4187 Piedmont Ave, Piedmont Ave, Oakland, CA - Phone Number - Yelp
https://www.camscanner.com/ | CamScanner | Turn your phone and tablet into scanner for intelligent document management.

## software apps
https://github.com/ctodobom/OpenNoteScanner/issues/94 | Add option to group scans and export them to PDF · Issue #94 · ctodobom/OpenNoteScanner
https://github.com/dawnlabs/alchemy/releases | Releases · dawnlabs/alchemy
https://github.com/ctodobom/OpenNoteScanner | ctodobom/OpenNoteScanner: Android application for scanning and manipulating handwritten notes and documents.
https://alternativeto.net/software/camscanner/ | CamScanner Alternatives and Similar Apps - AlternativeTo.net

### tesseract
* `tesseract ./IMG_20190914_171911.jpg stdout --dpi 380 --psm 6` 
  *  calculate dpi per https://www.calculatorsoup.com/calculators/technology/ppi-calculator.php
  *  however, images have no inherent size (per https://photo.stackexchange.com/questions/69075/how-do-i-set-up-my-camera-to-get-a-high-enough-resolution-for-printing-at-300dpi &  https://discussions.apple.com/thread/4233045) so dpi is odd 

https://github.com/tesseract-ocr/tesseract/wiki/ImproveQuality | ImproveQuality · tesseract-ocr/tesseract Wiki - see https://github.com/tesseract-ocr/tesseract/wiki/Command-Line-Usage