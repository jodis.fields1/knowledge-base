Some other notes may have open-source solutions but this is a general note

[Any other great apps on F-Droid?](https://news.ycombinator.com/item?id=19430580)

[List of open-source alternatives](https://www.reddit.com/r/androidapps/comments/6362a7/open_source_alternative_for_airdroid/dfs5gx3/)

## 2019-09 link dump
successfully flashed to ROM my Asus Transformer TF300T...

https://forum.xda-developers.com/eee-pad-transformer | Asus Eee Pad Transformer TF101
https://developer.android.com/studio/command-line/adb | Android Debug Bridge (adb)  |  Android Developers
https://www.vogella.com/tutorials/AndroidCommandLine/article.html | Using the Android Debug Bridge (adb) - Tutorial
https://forum.xda-developers.com/transformer-tf300t | Asus Transformer TF300T
https://twrp.me/ | TeamWin - TWRP
https://twrp.me/faq/officialota.html | Official OTA Updates and TWRP
https://en.wikipedia.org/wiki/Sideloading | Sideloading - Wikipedia
http://adbshell.com/commands/adb-install | adb install - Android ADB Shell Commands Manual
https://github.com/djibe/Android-tutorials | djibe/Android-tutorials: Tutorials to upgrade Android to 7.1 or higher or above on many devices
http://www.lineageosdownloads.com/lineage-os-supported-devices/ | Lineage OS Supported Devices [Complete List] – Lineage OS Downloads
https://android.gadgethacks.com/how-to/android-basics-install-adb-fastboot-mac-linux-windows-0164225/ | Android Basics: How to Install ADB & Fastboot on Mac, Linux & Windows « Android :: Gadget Hacks
https://forum.xda-developers.com/transformer-tf300t/general/guide-asus-transformer-pad-tf300t-stock-t3554744 | [GUIDE/TUTORIAL] Asus Transformer Pad TF300T… | Asus Transformer TF300T
https://android.stackexchange.com/questions/89182/bricked-my-tf300t-how-to-re-partition-internal-storage-to-get-it-back | unbricking - Bricked my TF300T: How to re-partition internal storage to get it back? - Android Enthusiasts Stack Exchange
https://www.reddit.com/r/transformerprime/comments/4c0756/how_can_i_get_a_tf201_back_to_stock/ | How can I get a tf201 back to stock? : transformerprime
https://stackoverflow.com/questions/14654718/how-to-use-adb-shell-when-multiple-devices-are-connected-fails-with-error-mor | android - How to use ADB Shell when Multiple Devices are connected? Fails with "error: more than one device and emulator" - Stack Overflow
https://www.youtube.com/watch?v=3wMlCucwGvE | ADB Tutorial: How to use ADB - YouTube
https://www.raywenderlich.com/621437-android-debug-bridge-adb-beyond-the-basics#toc-anchor-001 | Android Debug Bridge (ADB): Beyond the Basics | raywenderlich.com
https://tldr.ostera.io/adb | tldr | simplified, community driven man pages
https://stackoverflow.com/questions/17901692/set-up-adb-on-mac-os-x | android - Set up adb on Mac OS X - Stack Overflow
https://stackoverflow.com/questions/35919984/missing-platform-tools-when-installing-android-studio-on-mac-osx | macos - Missing platform-tools when installing Android Studio on Mac OSx - Stack Overflow
https://android.gadgethacks.com/how-to/ultimate-guide-using-twrp-only-custom-recovery-youll-ever-need-0156006/ | The Ultimate Guide to Using TWRP: The Only Custom Recovery You'll Ever Need « Android :: Gadget Hacks
https://opengapps.org/ | The Open GApps Project
https://github.com/timduru/android_device_asus_tf300t | timduru/android_device_asus_tf300t: TF300T Device Tree
https://public.timduru.org/Android/KatKiss/ | KatKiss-MarshMallow ROM - Android 6.0 - Asus Transformer


## 2018-03-02 link dump
https://github.com/TeamAmaze/AmazeFileManager/issues/959 | Use automated tests to verify behaviour? · Issue #959 · TeamAmaze/AmazeFileManager
https://f-droid.org/en/packages/com.simplemobiletools.filemanager/ | File Manager | F-Droid - Free and Open Source Android App Repository
https://forum.fairphone.com/t/fairphone-s-approach-to-root-on-the-fairphone-2/11893 | Fairphone’s approach to root on the Fairphone 2 - Road Map - Fairphone Forum
https://www.androidauthority.com/lg-v20-update-785383/ | LG V20 update tracker
https://www.androidauthority.com/community/threads/android-authority-owner.832/ | Android Authority owner? | Android Authority Forums
https://forums.oneplus.net/threads/forum-bug-with-advanced-search.796951/ | Other - Forum bug with advanced search? - OnePlus Forums
https://github.com/SimpleMobileTools/Simple-File-Manager | SimpleMobileTools/Simple-File-Manager: A simple file manager for browsing and editing files and directories.
https://github.com/SimpleMobileTools | Simple Mobile Tools