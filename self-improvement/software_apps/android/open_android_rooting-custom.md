
## 2018-10 note
https://www.androidcentral.com/root

## 2018-03 note
Cyanogemnod was discontinued; LineageOS is the successor

### rooting
https://www.androidcentral.com/root

airmirror - much easier if rooted
see my comment at https://en.wikipedia.org/wiki/Talk:Rooting_(Android_OS)/Archive_1#Key_open-source_libraries

## archive
Lollipop was released [insert date], and pushed to my Samsung S4 [around date]. It has been a bit of a disaster, as others have experienced. It appears that either Google or Samsung are not practicing effective test-driven development given some of the basic software engineering issues across numerous devices, such as dropped WiFi, but my bigger problem has been with the UI lacking a basic button. And there is no apparent way to give that feedback to Google: code.google.com is planned for destruction with a horribly unfriendly interface. The Android project has literally tens of thousands of open issues [insert link]. It  seems pointless to send in an issue, and clearly not Google does not encourage it.

The solution is to shift to an open community where issues are prioritized and tackled. Cyanogenmod is the only alternative, so I explored how they were set up.

After I dove in, I found that finding the "getting started" and community overview was challenging. For example, I wanted to find my device in the issue tracker to get an idea for how its issues are handled. Cyanogenmod uses the JIRA tracker where there is no clear way to filter issues by device. After trawling their wiki for a while, I eventually ended up on the IRC channel. [share device name and its mapping]

In addition, I found this issue [Phone Reports: No SIM card - No service](https://jira.cyanogenmod.org/browse/NIGHTLIES-1459?jql=Model%20%3D%20jactivelte) in the nightly bug reports for my phone. It was intriguing because it illustrates the risk and the responsiveness of the open-source: a single line of code broke service entirely, and it was rolled back within the day. Ideally, the developer would have a good set of reference phones and run automated tests against it so nightly testers would haven't to report these regressions, but we don't (yet) live in an ideal world. So someone has to regress [back to the future with software](http://bencreasy.com/back-to-the-future-software/). 

Right now, however, that someone will not be me.