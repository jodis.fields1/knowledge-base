CONCLUSION: gave up after found that it can't be unlocked, did not go thru w/ [[ROOT] DirtySanta comes for the H990](https://forum.xda-developers.com/v20/development/dirtysanta-h990-t3624296/page207)

TODO: try out custom ROM SuperV20          
* https://www.reddit.com/r/lgv20/comments/8qyrs4/upgrading_to_superv20_v31_has_tripled_my_idle/
TODO: get a better battery?

## oreo update
https://www.reddit.com/r/lgv20/comments/9h4zwo/att_v20_oreo_update_apps_keep_hiding_the_home/
* TODO: disable the dot

## specs
name: LG-H990ds

IMEI: 352163083672535

LGH990e9dd4cfe

./fastboot oem device-id:

(bootloader) Device-ID
(bootloader) D86D8F7FAE4CFBC4BA230BF331BCBBC9
(bootloader) 3B12C7FBB7BEE3AF8E9C1137709D8759
(bootloader) ----------------------------------
OKAY [  0.051s]
Finished. Total time: 0.052s

## autofill forms
NOTE: Oreo 8.0 can get me a better autofill
* [Android P finally enables Autofill in Chrome and other browsers](https://www.xda-developers.com/android-p-autofill-browsers/) - P 9.0 is even better
* if it doesn't ship, use Lineage 15.1 https://www.reddit.com/r/lgv20/comments/8b5fh0/its_not_an_april_fools_this_time_lineageos_151/

## frustrations
[Chrome "not responding"](https://forums.androidcentral.com/samsung-galaxy-tab-s3/856603-chrome-not-responding.html)
* keep an eye on this

### attempts to fix
TOOD: greenify to prevent background task pileup
* seems there are no open-source alternatives to Advanced Task Killer
Turn on Developer Options->kill apps

## customizations
Avoid pocket activation:
* custom lock screen - needs to lock automatically with power button, to avoid accidentally triggering in pocket
* disable second screen
* 1 bluetooth connection at a time

## cpu throttling thermal issue
https://www.reddit.com/r/lgv20/comments/8ffn4b/lets_collect_facts_on_cpu_throttling_report_in/ | Let's collect facts on CPU throttling (report in) : lgv20
https://www.makeuseof.com/tag/got-a-hot-nexus-4-heres-a-hack-that-will-cool-it-down/ | Cool Off Your Hot Nexus 4 With a Cheap and Easy Hack
https://www.reddit.com/r/Android/comments/78fkyd/so_every_single_flagship_lg_has_made_since_the_g4/ | 
https://www.xda-developers.com/lg-root-checker-tool-slow-performance/ | LG is Including a Root Checker Tool on Some of Their Devices
https://www.reddit.com/r/lgv20/comments/59jp6u/turn_off_double_tap_notification_screen_to_turn_on/ | Turn off double tap notification screen to turn on : lgv20
