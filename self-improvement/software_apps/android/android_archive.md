## AT&T and Samsung Galaxy S4 Active (SGH-1537)
[http://bgr.com/2013/05/29/galaxy-s4-autocorrect-fix-jelly-bean-keyboard/](http://bgr.com/2013/05/29/galaxy-s4-autocorrect-fix-jelly-bean-keyboard/) - lack of autocorrect is really annoying

### Network compatibility
https://www.frequencycheck.com/compatibility/EL6Ilv/samsung-sgh-i537-galaxy-s-4-active-samsung-fortius/united-states

Xposed-G-Touchwiz will allow the battery cover reminder to be turned off

IMEI: 357365050416334

Android 4 is a bit of a confusing mess - for example, a bunch of the settings are in those boxes, while related settings are app-specific; got confused by contacts sync which I had to find in accounts rather than Gmail or Contacts