# android pixel

## random
[Bring back the headphone jack: Why USB-C audio still doesn't work](https://www.pcworld.com/article/3284186/mobile/bring-back-the-headphone-jack-why-usb-c-audio-still-doesnt-work.html)

## eSIM
https://www.androidcentral.com/using-project-fi-pixel-2s-esim-amazing | The Pixel 2's eSIM makes it dead simple to ditch your carrier for Project Fi | Android Central
https://www.reddit.com/r/GooglePixel/comments/90zxzc/if_google_pixel_2_uses_esim_why_isnt_it_dualsim/ | If Google Pixel 2 uses eSIM, why isn't it dual-sim (or triple, quad-sim, etc) : GooglePixel

## launcher app
open-source:
* https://9to5google.com/2018/06/11/lawnchair-pixel-launcher-replacement-v1-release/ | Popular Pixel Launcher replacement 'Lawnchair' gets finalized V1 release, V2 beta incoming - 9to5Google

https://www.computerworld.com/article/3269497/android/at-a-glance-widget-android.html | A better 'At a Glance' widget for Android | Computerworld


