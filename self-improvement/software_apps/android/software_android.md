[My favorite apps on F-Droid](https://news.ycombinator.com/item?id=17485893)

See apps email with label personal/data for a list of apps

Downloaded from the apps app... 

### android apps tracking
I should have a list of ones I've tried? See installed_android_apps.zip

* App Usage - Manage/Track Usage

#### uninstalled
* parking apps - see life-log/..icebox/auto.md (hidden from search)
* GIThrive by Vivante Health

### multiple phones
Piece ([Indiegogo](https://www.indiegogo.com/projects/piece--2#/)) allows you to have two phone numbers

## To-review
[Can I have different volumes settings for notifications and ringtones with Nexus 4/Android 4.2?](http://android.stackexchange.com/questions/37242/can-i-have-different-volumes-settings-for-notifications-and-ringtones-with-nexus) - apparently not

## Current issues
### Calling 
Unable to turn on screen during calls - tweaked this setting

### Directory structure organization
[Is it possible to reorganize the SD card? Because it is a mess in there](http://android.stackexchange.com/questions/19377/is-it-possible-to-reorganize-the-sd-card-because-it-is-a-mess-in-there) on SE points to Google's recommendations which it seems are seldom followed

### File explorer apps
ES File Explorer - UPDATE: adds annoying adware, switched per https://www.lifehacker.com.au/2016/10/android-users-its-time-to-stop-using-es-file-explorer/ ~~major and works pretty well for adding .nomedia files~~

### Music/audiobooks
Use .nomedia in audiobook folders to ignore; only solution per SE thread  [How to exclude certain folders from getting scanned by music player?](http://android.stackexchange.com/questions/5261/how-to-exclude-certain-folders-from-getting-scanned-by-music-player?rq=1)

Also use a file manager to play audiobooks, look into Akimbo for audiobooks to pick up where you left off and Astro Player

## Development
enable developer mode by tapping 7 times on the "build number"

http://anantshri.info/andro/file_system.html describes the filesystem hierarchy

## Apps
[http://thedroidlawyer.com/2013/03/everything-you-need-to-know-about-android-apps-for-lawyers-well-almost/](http://thedroidlawyer.com/2013/03/everything-you-need-to-know-about-android-apps-for-lawyers-well-almost/)- some good suggestions, most significantly SwiftKey