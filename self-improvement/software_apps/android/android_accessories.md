**Accessories**

Zolo - indiegogo set of protections

*USB* - use the ones reviewed by [this Google engineer](https://news.ycombinator.com/item?id=10508494)

*Car docking* - Breffo Spiderpodium seems to be the best

Most excited about  [Radmo](https://www.indiegogo.com/projects/radmo-the-perfect-mobile-phone-mount-for-your-car/x/2086002) altho it is not available yet. iOttie seems perhaps the best; in Cali can only be used "in a five-inch square in the lower corner of the windshield nearest to the driver and outside of an airbag deployment zone"; think I tried the imagnet previously; had the steelie in my list for a while.

*Case*

OnePocket for iPhone - stores cards and maybe cash; hope it comes for Android