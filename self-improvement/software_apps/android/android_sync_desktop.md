## sync end edit markdown files
Syncthing

Process: searched on F-Droid for editor and https://github.com/gsantner/markor seemed most popular / active

originally tried https://github.com/vmihalachi/turbo-editor

## general
KDE Connect / Soduto on macOS
airdroid

other: Feem v4 (not open-source)

## text messages (mightytext)
**Conclusion**: after years with MightyText, going with Pulse SMS from klinker-apps per open-source and open bug reports
https://github.com/klinker-apps/messenger-issues | klinker-apps/messenger-issues: Roadmap (and issue tracker) for our Pulse SMS app/platform
https://www.reddit.com/r/androidapps/comments/6qomfc/anyone_using_pulse_sms_these_days_google_has_the/ | Anyone using Pulse SMS these days?

Signal Desktop sounds cool but will never sync SMS per [Sending/receiving SMS through Signal Desktop](https://github.com/signalapp/Signal-Desktop/issues/1645)

https://joaoapps.com/join/ | Join – joaoapps
https://github.com/Pushjet/Pushjet-Server-Api/issues/26 | Can somebody explain the complete concept of Pushjet server · Issue #26 · Pushjet/Pushjet-Server-Api
https://alternativeto.net/software/mightytext/ | Mightytext Alternatives and Similar Software - AlternativeTo.net

