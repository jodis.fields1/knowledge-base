List at bottom:

TODO: move these into relevant places

Kept:
NooBoss - my extension manager!
OneTab - awesome for getting rid of bookmarks
Shortcuts (shortkeys) - TODO customize these to save me time... https://github.com/mikecrittenden/shortkeys

### bookmarks
pinboard - not really supported - dev never responded to my questions
* bookmarklets at https://pinboard.in/howto/
    * TODO: try https://github.com/joelcarranza/particular-pinboard
* TODO: try https://pinboard.in/resources/ official browser extension
Goomarks - sucks
buku - also sucks

### security
[A third of all Chrome extensions request access to user data on any site](https://www.zdnet.com/article/a-third-of-all-chrome-extensions-request-access-to-user-data-on-any-site/)
* https://stackoverflow.com/questions/51860036/access-of-third-party-extensions-to-my-cookies
* https://stackoverflow.com/questions/40064016/access-to-cookies-from-chrome-extensions
* https://security.stackexchange.com/questions/166606/what-are-the-effects-of-the-chrome-web-developer-extension-hijack

### monetization
TL;DR advertising mostly - mikerogers.io has a success story

https://softwareengineering.stackexchange.com/questions/55768/charging-for-chrome-extension | Charging for chrome extension? - Software Engineering Stack Exchange
https://groups.google.com/a/chromium.org/forum/#!topic/chromium-apps/kDccQN9DA4w | What to do to monetize a chrome extension? - Google Groups
https://mikerogers.io/2016/10/29/lessons-learnt-from-monetising-my-chrome-extension.html | Lessons learnt from monetising my Chrome Extension
https://www.quora.com/How-do-Chrome-add-ons-make-money | How do Chrome add-ons make money? - Quora
 
### Extensions
use --disable-extensions when coding per https://superuser.com/questions/822429/how-to-start-chrome-without-plugins

#### Extension management
* NooBoss - https://github.com/AInoob/NooBoss
* Chrome Extension Automation https://github.com/moarsel/chrome-extension-automation

https://gitlab.com/jcrben-not-mine/extensions-manager-aka-switcher
* emailed author, email is dead, no license

Chrome extension source viewer (CRX Viewer) - https://github.com/Rob--W/crxviewer

#### Coding
Long-run: these should be loaded only for development sessions as needed...
camsong/chrome-github-mate - makes ignore whitespace easier to see https://github.com/camsong/chrome-github-mate/issues/5

Web Scraper - https://chrome.google.com/webstore/detail/web-scraper/jnhgnonknehpejjnehehllkliplmbmhn

##### Frontend
Visual Event - replaced with https://github.com/DataTables/VisualEvent - CSP could be fixed by downloading entire script https://github.com/DataTables/VisualEvent/issues/57
ColorPick Eyedropper - https://chrome.google.com/webstore/detail/colorpick-eyedropper/ohcpnigalekghcmgcdcenkpelffpdolg

##### Backend
chromelogger https://github.com/ccampbell/chromelogger - log server stuff to Chrome console

#### Landing pages
winner? - https://github.com/joelshepherd/tabliss

Rejected:
HumbleNewTabPage - minimal code https://github.com/quodroc/HumbleNewTabPage
Leoh - not open-source, loosely maintained
* cool landing page with discussion at https://www.reddit.com/r/LeohTab/ 
* dropped it due to worry about errors in console
* alternative:
    * https://github.com/chingu-coders/voyage1-turtles-team-38
    * Be Limitless by Anup Gosavi, https://chrome.google.com/webstore/detail/be-limitless/jdpnljppdhjpafeaokemhcggofohekbp

#### Productivity
Boomerang for Gmail https://chrome.google.com/webstore/detail/boomerang-for-gmail/mdanidgdpmkimeiiojknlnekblgmpdll
Gyroscope for Chrome

#### Other
##### Web analytics (in another note, I'm sure):
SimilarWeb https://www.similarweb.com - https://chrome.google.com/webstore/detail/similarweb-traffic-rank-w/hoklmmgfnpapgjgcpechhaamimifchmp?hl=en
    * dropped, wasn't using it

## list
[Get a list of installed chrome extensions](https://superuser.com/questions/1164152/get-a-list-of-installed-chrome-extensions)
* current method: use Extensions Manager (aka Switcher) to turn them all on, then chrome::system  

### as of 2018-03
#### enabled
hdhinadidafjejdhmfkjgnolgimiaplp : Read Aloud: A Text to Speech Voice Reader
aalppolilappfakpmdfdkpppdnhpgifn : React Sight
*  https://github.com/React-Sight/React-Sight
ahfgeienlihckogmohjhadlkjgocpleb : Web Store : version 0_2
chphlpgkkbolifaimnlloiipkdnihall : OneTab : version 1_18
eadndfjplgieldjbigjakmdgkmoaaaoc : Xdebug helper : version 1_6_1
eemcgdkfndhakfknompkggombfjjjeno : Bookmark Manager : version 0_1
fmkadmapgofadopljbjfkapdkoienihi : React Developer Tools : version 3_2_1
gfdkimpbcpahaombhbimeihdjnejgicl : Feedback : version 1_0
ghbmnnjooekpmoecnnnilnnbdlolhkhi : Google Docs Offline : version 1_4
hdokiejnpimakedhajhdlcegeplioahd : LastPass: Free Password Manager : version 4_9_0_37
iohcojnlgnfbmjfjfkbhahhmppcggdog : EverSync - Sync bookmarks, backup favorites : version 16_2_1
jifpbeccnghkjeaalbbjmodiffmgedin : Chrome extension source viewer : version 1_6_2
jjbjdleccaiklfpcblgekgmjadjdclkp : Pulse Extension : version 1_9_4
jlhmfgmfgeifomenelglieieghnjghma : Cisco WebEx Extension : version 1_0_12
joadogiaiabhmggdifljlpkclnpfncmj : Grid Ruler : version 0_2_1
kfmlkgchpffnaphmlmjnimonlldbcpnh : TimeYourWeb Time Tracker : version 1_0_18
kmendfapggjehodndflmmgagdbamhnfd : CryptoTokenExtension : version 0_9_73
llaficoajjainaijghjlofdfmbjpebpa : Speed Dial [FVD] - New Tab Page, 3D, Sync... : version 65_1_1
lmhkpmbekcpmknklioeibfkpmmfibljd : Redux DevTools : version 2_15_2
mfehgcgbbipciphmccgaenjidiccnmng : Cloud Print : version 0_1
mhjfbmdgcfjbbpaeojofohoefgiehjai : Chrome PDF Viewer : version 1
neajdppkdcdipfabeoofebfddakdcjhd : Google Network Speech : version 1_0
nkeimhogjdpnpccoofpliimaahmaaome : Google Hangouts : version 1_3_7
nlpnopimcnncncmmjdijebhlmkfandpl : Tempo : version 1_1_16
nmmhkkegccagdldgiimedpiccmgmieda : Chrome Web Store Payments : version 1_0_0_3
pkedcjkdefgpdelpbcmbmeomcjbeemfm : Chrome Media Router : version 6518_129_0_1

#### all
lpleipinonnoibneeejgjnoeekmbopbc : Extensions Manager (aka Switcher) : version 0_2_1_2
    * 2018-06-01: started freaking out
aajodjghehmlpahhboidcpfjcncmcklf : NooBoss : version 0_1_7_0
ahfgeienlihckogmohjhadlkjgocpleb : Web Store : version 0_2
chklaanhfefbnpoihckbnefhakgolnmc : JSONView : version 0_0_32_3
chphlpgkkbolifaimnlloiipkdnihall : OneTab : version 1_18
dgjhfomjieaadpoljlnidmbgkdffpack : Sourcegraph for GitHub : version 1_6_99
dhdgffkkebhmkfjojejmpbldmpobfkfo : Tampermonkey : version 4_5
eemcgdkfndhakfknompkggombfjjjeno : Bookmark Manager : version 0_1
elifhakcjgalahccnjkneoccemfahfoa : Markdown Here : version 2_12_0
fgnjcebdincofoebkahonlphjoiinglo : GitSense : version 1_2

fllaojicojecljbmefodhfapmkghcbnh : Google Analytics Opt-out Add-on (by Google) : version 1_1
fmkadmapgofadopljbjfkapdkoienihi : React Developer Tools : version 3_2_1
fngmhnnpilhplaeedifhccceomclgfbg : EditThisCookie : version 1_4_3
gcbommkclmclpchllfjekcdonpmejbdp : HTTPS Everywhere : version 2018_3_13
gfdkimpbcpahaombhbimeihdjnejgicl : Feedback : version 1_0
ghbmnnjooekpmoecnnnilnnbdlolhkhi : Google Docs Offline : version 1_4
gppongmhjkpfnbhagpmjfkannfbllamg : Wappalyzer : version 5_4_11
hdokiejnpimakedhajhdlcegeplioahd : LastPass: Free Password Manager : version 4_9_0_37
hhinaapppaileiechjoiifaancjggfjm : Web Scrobbler : version 2_4_3
iohcojnlgnfbmjfjfkbhahhmppcggdog : EverSync - Sync bookmarks, backup favorites : version 16_2_1
jgpmhnmjbhgkhpbgelalfpplebgfjmbf : Smile Always : version 0_93
jifpbeccnghkjeaalbbjmodiffmgedin : Chrome extension source viewer : version 1_6_2
jjbjdleccaiklfpcblgekgmjadjdclkp : Pulse Extension : version 1_9_4
jlhmfgmfgeifomenelglieieghnjghma : Cisco WebEx Extension : version 1_0_12
joadogiaiabhmggdifljlpkclnpfncmj : Grid Ruler : version 0_2_1
kfmlkgchpffnaphmlmjnimonlldbcpnh : TimeYourWeb Time Tracker : version 1_0_18
kmendfapggjehodndflmmgagdbamhnfd : CryptoTokenExtension : version 0_9_73
laankejkbhbdhmipfmgcngdelahlfoji : StayFocusd : version 1_5_10
lcfdefmogcogicollfebhgjiiakbjdje : Disable Extensions Temporarily : version 1_0
ldgiafaliifpknmgofiifianlnbgflgj : Ugly Email : version 2_1_0
llaficoajjainaijghjlofdfmbjpebpa : Speed Dial [FVD] - New Tab Page, 3D, Sync... : version 65_1_1
lmhkpmbekcpmknklioeibfkpmmfibljd : Redux DevTools : version 2_15_2
logpjaacgmcbpdkdchjiaagddngobkck : Shortkeys (Custom Keyboard Shortcuts) : version 2_2_7
lpleipinonnoibneeejgjnoeekmbopbc : Extensions Manager (aka Switcher) : version 0_2_1_2
mabhojhgigkmnkppkncbkblecnnanfmd : Tamper : version 0_24_8
mdnleldcmiljblolnjhpnblkcekpdkpa : Requestly: Redirect Url, Modify Headers etc : version 5_2_0
mfehgcgbbipciphmccgaenjidiccnmng : Cloud Print : version 0_1
mhjfbmdgcfjbbpaeojofohoefgiehjai : Chrome PDF Viewer : version 1
mkkcgjeddgdnikkeoinjgbocghokolck : Copy as plain text - amaz.in/g : version 1_0
neajdppkdcdipfabeoofebfddakdcjhd : Google Network Speech : version 1_0
nkeimhogjdpnpccoofpliimaahmaaome : Google Hangouts : version 1_3_7
nlpnopimcnncncmmjdijebhlmkfandpl : Tempo : version 1_1_16
nmkinhboiljjkhaknpaeaicmdjhagpep : F.B.(FluffBusting)Purity : version 23_4_15_0
nmmhkkegccagdldgiimedpiccmgmieda : Chrome Web Store Payments : version 1_0_0_3
phgahogocbnfilkegjdpohgkkjgahjgk : Code Climate : version 669
pkedcjkdefgpdelpbcmbmeomcjbeemfm : Chrome Media Router : version 6518_129_0_1

##### deleted for now
aalppolilappfakpmdfdkpppdnhpgifn : React-Sight 1.0.6
phgahogocbnfilkegjdpohgkkjgahjgk : Code Climate 679
nmkinhboiljjkhaknpaeaicmdjhagpep : F.B.(FluffBusting)Purity 26.1.2.0
jnhgnonknehpejjnehehllkliplmbmhn : Web Scraper 0.3.8
nlpnopimcnncncmmjdijebhlmkfandpl : Tempo 1.1.19
    * should replace with bugwarrior
fkjjjamhihnjmihibcmdnianbcbccpnn : GitHub SLOC : version 1_0_8
ihlenndgcmojhcghmfjfneahoeklbjjh : cVim
ldgiafaliifpknmgofiifianlnbgflgj : Ugly Email 3.0.3
    * too niche

##### deleted permanently
hhhpdkekipnbloiiiiaokibebpdpakdp : Saka Key 
    * no longer maintained? see vimium.md
hhinaapppaileiechjoiifaancjggfjm : Web Scrobbler 2.9.0
    * using Spotify
ghniladkapjacfajiooekgkfopkjblpn : Bukubrow 2.3.0
    * really poor UX
oknllbfkmapccinochelgmcdjhlgfnik : Goomarks (Browser for Google Bookmarks) : version 1_0_3_2
oboonakemofpalcgghocfoadofidjkkk : KeePassXC-Browser
dkfhfaphfkopdgpbfkebjfcblcafcmpi : MightyText - SMS from PC & Text from Computer : version 22_3
