# Browsers Firefox

https://github.com/nilcons/firefox-hacks

### wants
permissions around extensions: https://www.reddit.com/r/firefox/comments/cutny7/vivaldi_has_the_ability_to_sleep_extensions_until/ey0jn7c/ and https://bugzilla.mozilla.org/show_bug.cgi?id=1497075

### bugzilla
Sponsored by Mozilla, but written in Perl and not really worked on
* https://wiki.mozilla.org/BMO
* https://github.com/bugzilla/bugzilla-ux/wiki/Bugzilla-6-Roadmap


### 2018-03 firefox migration
TODO: fix to firefox
1. sync bookmarks
    1. found EverSync to sync bookmarks
2. sync browser extensions?
3. configure firefox with https://github.com/pyllyukko/user.js/

use https://github.com/pyllyukko/user.js/blob/relaxed/user.js? decided to start my own from scratch
* no XDG support still :(
* use profiles instead?
  * https://support.mozilla.org/en-US/kb/profile-manager-create-and-remove-firefox-profiles
  * https://support.mozilla.org/en-US/questions/963032
  * http://forums.mozillazine.org/viewtopic.php?f=38&t=607383

Issues:
* scrolling is so slow!
   * TODO: https://www.maketecheasier.com/change-the-scrolling-speed-in-firefox/ and  https://wiki.mozilla.org/Gecko:Mouse_Wheel_Scrolling

### Firefox
**Tweaks**

[switching off browser.search.showOneOffButtons](https://support.mozilla.org/en-US/questions/1033365)

2014-10: had issue with "As a security precaution, Firefox does not automatically re-request sensitive documents" messages (see  [this thread](https://support.mozilla.org/en-US/questions/1018237)), followed advice there "Options- Advanced->Check->Automatically Override Cache Management" which seems to work

2014-09: had issues getting my greasemonkey scripts to sync and reviewed  [Synchronisation of Scripts and Preferences Extensions](https://support.mozilla.org/en-US/questions/989056) but just ended up manually installed them (right-click->Open with->firefox); also figured out that since Firefox is 32-bit I need to install 32-bit Java to run the plugin per Mozilla's page  [Use the Java plugin to view interactive content on websites](https://support.mozilla.org/en-US/kb/use-java-plugin-to-view-interactive-content#w_java-plugin-does-not-appear-in-the-add-ons-manager)

2014-04: fixed wide bookmark properties with  [Bookmark Properties box is too long. How to fix?](https://support.mozilla.org/en-US/questions/980709)
2014-01:  [Dont track me Google greasemonkey script](http://userscripts.org/scripts/show/121923) fixes their mangled URLs; redirect popup fixed per  [thi](https://support.mozilla.org/en-US/questions/969705)

**Mozilla**
*Add-ons* - use the SDK and stuff per this blog post  [How to develop a Firefox extension](https://blog.mozilla.org/addons/2014/06/05/how-to-develop-firefox-extension/) and  [SO question](http://stackoverflow.com/questions/20409349/what-is-the-easiest-way-to-develop-firefox-extension)
*Tips*
about:memory to investigate memory leaks, but not so helpful really

*Caches*
http://superuser.com/questions/81182/how-to-force-internet-explorer-ie-to-really-reload-the-page has an interesting discussion about issues getting IE to reload without cache
also see https://bugzilla.mozilla.org/describecomponents.cgi which has a Core section
*Bug*

When searching on bugzilla, initially it seemed like there were tons of unfixed bugs, but the key is to search on the version numbers 

**Source Code**
*April 2012*

https://developer.mozilla.org/en/Viewing_and_searching_Mozilla_source_code_online and also https://developer.mozilla.org/en/Mozilla_Source_Code_Directory_Structure although these both seem a bit unclear - also https://developer.mozilla.org/en/Source_code_directories_overview