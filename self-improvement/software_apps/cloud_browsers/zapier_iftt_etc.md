
## alternatives
https://github.com/muesli/beehive
zapier
IFTT
bip.io - dead

## draft blog post
I've used Yahoo Pipes for years, mostly to transform subreddit posts into an RSS feeds based on the post hitting a specific threshold number. But it is shutting down soon, and I [along with others](http://www.precisement.org/blog/Any-Yahoo-Pipes-true-substitute-out-there.html) need an alternative. I tried IFTTT and Zapier, but their Reddit recipes only allow for "hot" or "top" post feeds. Enter Bip.io, a node.js alternative.[^n]

There are no blog posts explaining how to use Bip.io and although the interface is slick, it is not at all intuitive, and the documentation, split between [a Uservoice knowledgebase](https://bip.uservoice.com/knowledgebase/topics/74330-bips) and a Github wiki (insert link) is not great.

I relied mostly on two videos: [Twitter Search > RSS](https://vimeo.com/119888216) and its companion [Making a private RSS feed public](https://vimeo.com/119888217) 

[^n]: Huginn, which is Ruby-based, has been around longer, with way more stars, but I won't be focusing on it.