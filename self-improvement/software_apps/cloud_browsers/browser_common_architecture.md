See Anki

## General

[https://github.com/w3c/web-platform-tests](https://github.com/w3c/web-platform-tests)

**Underlying control flow**

[lambda squared λ^2 How browsers work](http://domenicodefelice.blogspot.com/?m=1) (2015) - more detailed
[How browsers work: Behind the scenes of modern web browsers](http://taligarsiel.com/Projects/howbrowserswork1.htm)
    * [How Browsers Work: Behind the scenes of modern web browsers](https://www.html5rocks.com/en/tutorials/internals/howbrowserswork/) - possibly more polished

CSS is used to createa a render tree per:
https://stackoverflow.com/questions/3527800/how-do-browsers-read-and-interpret-css
[http://www.whatwg.org/specs/web-apps/current-work/multipage/parsing.html](http://www.whatwg.org/specs/web-apps/current-work/multipage/parsing.html) 