See also: describethis repo

Chrome and Firefox bookmarks have irritating platform-specific quirks and churn.

TODO: commoncog https://news.ycombinator.com/item?id=17998315

## 2018-12 frustration
due to [bookmarks duplicate after change](https://bugs.chromium.org/p/chromium/issues/detail?id=901606) this has increased in urgency - bunch of my bookmarks got duplicated!

## pocket
possible basis for my own app... "pocket measure scroll detect reading"

https://github.com/jlord/sheetsee-pocket/issues/4

http://myreadinghabits.com/welcome.php

https://www.quora.com/Can-I-see-how-many-articles-I-have-saved-in-my-Pocket-list

## open-source


## syncing
Eversync is great to sync native bookmarks.


## managers
options: https://alternativeto.net/software/google-bookmarks/

### open-source
* https://github.com/kanishka-linux/reminiscence - seen at https://news.ycombinator.com/item?id=17942032
* https://github.com/sienori/Tab-Session-Manager/issues/73 - firefox only, no sync
* http://tabmixplus.org/ - per https://bitbucket.org/onemen/tabmixplus/downloads/ still active
* https://github.com/antonycourtney/tabli/issues

### others
WINNER: raindrop.io - best by far as far UX, tweeted my request for https://raindropio.canny.io/feature-requests/p/save-all-windows-tabs-links-and-open-all-selected-links at https://twitter.com/jcrben/status/1074906009171222528

others:
qlearly - eh

http://www.lasso.net/go/ - did not evaluate

https://www.dropmark.com/ - did not evaluate

https://acornbookmarks.com/ - watching
* delayed until 2021 per email from Brandon Bayer <heybrandon@acornbookmarks.com>

diigo - hard to select all
bookmarks sidebar - just a skin over Google Bookmarks
"Google Bookmark Manager" - an extension version of the manager https://chrome.google.com/webstore/detail/bookmark-manager/gmlllbghnfkpflemihljekbapjopfjik
* deprecated 2018-08 https://productforums.google.com/forum/#!topic/chrome/c85R7GWVtCQ
* published at https://github.com/rtm516/Bookmark-Manager

## read / share counts
https://www.sharedcount.com/ | SharedCount: Social URL Analytics
https://marketingland.com/twitter-share-numbers-159379 | Now That Twitter Has Killed Counts, Other Ways To Find Your Shares - Marketing Land
https://github.com/jaredatch/Shared-Counts/issues/62 | New 3rd-party Twitter API support · Issue #62 · jaredatch/Shared-Counts


### rejected
Bookmark Manager Plus - https://chrome.google.com/webstore/detail/bookmark-manager-plus/pfbeenngglcojppheoegjjjomfkejibg?hl=en
tagpacker - seems to suck, still actively developed as of 2018
kozmos - in private beta as of 2018, not promising