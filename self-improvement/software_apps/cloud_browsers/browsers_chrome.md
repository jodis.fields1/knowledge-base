# Google Cheome
[How to remove one or all auto-complete text entries in Chrome? [duplicate]](https://superuser.com/questions/175250/how-to-remove-one-or-all-auto-complete-text-entries-in-chrome)

See productivity/keyboard_shortcuts.md for a major frustration

https://github.com/everyonesdesign/disable-google-search-text-highlights
* from https://support.google.com/chrome/thread/53700706?msgid=55036220

[Turn notifications on or off](https://support.google.com/chrome/answer/3220216?co=GENIE.Platform%3DDesktop&hl=en)

[Synchronous XMLHttpRequest causes a tab to consume 100% CPU until it's closed](https://bugs.chromium.org/p/chromium/issues/detail?id=848210)
* momentous bug

[How to determine what is causing Chrome to show the “Aw, Snap” dialogue](https://superuser.com/questions/607563/how-to-determine-what-is-causing-chrome-to-show-the-aw-snap-dialogue)

[Remove auto-complete field entries in Google Chrome](https://superuser.com/questions/171198/remove-auto-complete-field-entries-in-google-chrome#comment1247535_171226)
* https://superuser.com/a/631479/457084

## reference links
* https://peter.sh/experiments/chromium-command-line-switches/
* https://fossbytes.com/complete-list-of-secret-chrome-urls-uses/
* chrome://flags/
* chrome://net-internals/#dns

## sync bookmarks issue 2018-11
https://superuser.com/questions/129410/force-chrome-to-sync-bookmarks
* chrome://sync/
* https://chrome.google.com/sync?hl=en-US
  * RESET SYNC
      * log in, log out
  * chrome://settings/syncSetup
      * Managed synced data...

## new bookmarks manager 2018-08
https://productforums.google.com/forum/#!msg/chrome/ZnwwRDo4E3U/Wk6jQfDoBgAJ | Bookmarks manager changed! - Google Product Forums
https://bugs.chromium.org/p/chromium/issues/list?q=label:Proj-MaterialDesign-WebUI | Issues - chromium - An open-source project to help move the web forward. - Monorail
https://bugs.chromium.org/p/chromium/issues/list?can=2&q=label:Proj-MaterialDesign-WebUI%20%20component:UI%3EBrowser%3EBookmarks&colspec=ID%20Pri%20M%20Stars%20ReleaseBlock%20Component%20Status%20Owner%20Summary%20OS%20Modified | Issues - chromium - An open-source project to help move the web forward. - Monorail
https://bugs.chromium.org/p/chromium/issues/detail?id=834205 | 834205 - Remove non-MD bookmark manager - chromium - Monorail
https://productforums.google.com/forum/#!topic/chrome/7uyLxeEI5aw;context-place=topicsearchin/chrome/bookmarks | Bring back the bookmark manager from chrome 62 with all its features - Google Product Forums
https://productforums.google.com/forum/#!msg/chrome/7uyLxeEI5aw/-fsVZ1tWCQAJ | Bring back the bookmark manager from chrome 62 with all its features - Google Product Forums


## people
Evan Martin - http://neugierig.org/software/blog/2015/07/sabbatical-2.html also has celiac disease

## config & privacy
hacked / private: https://github.com/Eloston/ungoogled-chromium/blob/master/resources/patches/ungoogled-chromium/add-flag-for-search-engine-collection.patch

## updates stream
[Chrome Platform Status](https://www.chromestatus.com/features/schedule) - 

## autoplay
* [Bad Audio/Video Example with autoplay and loop](http://pauljadam.com/demos/badav.html) - test the option
* [Autoplay Policy Changes](https://developers.google.com/web/updates/2017/09/autoplay-policy-changes)
    * chrome://flags/#autoplay-policy - set to Document user activation

## Distraction-free
[Limiting the tab asterisk in Slack](https://webapps.stackexchange.com/questions/75666/limiting-the-tab-asterisk-in-slack) - ??
    * had to switch to the desktop client because of this, which really sucks

[kyo-ago/enable-right-click](https://github.com/kyo-ago/enable-right-click) - ?

## Chrome developers portal
Bradfield pointed me to [Chrome University 2018](https://www.youtube.com/playlist?list=PL9ioqAuyl6UIFAdsM5KU6P-hRJdh-BPmm)
http://dev.chromium.org/developers

[Technical Discussion Groups](http://dev.chromium.org/developers/technical-discussion-groups) - these are active


## general

found in the IRC channel:

**Tabs as apps**
[Fluid](http://alternativeto.net/software/fluid/)

**User scripts**
Learning: wiki.greasespot.com, Dive Into Greasemonkey, etc
Hosting: greasyfork.org and openuserjs.org are both hosted on github 
Running: TamperMonkey on Chrome (don't bother with native) and Greasemonkey on Firefox

**Chrome**

*Customizations*: click to play for flash plugins but no simple solution for HTML5 per [How to Stop Auto-Playing Flash and HTML5 Videos in Chrome](http://www.makeuseof.com/tag/stop-auto-playing-flash-html5-videos-chrome/)! also the audio mute botton is in chrome://flags

[Where to find extensions installed folder for Google Chrome on Mac?](http://stackoverflow.com/questions/17377337/where-to-find-extensions-installed-folder-for-google-chrome-on-mac) - ~/Library/Application Support/Google/Chrome/Default 

Official suggestions at https://productforums.google.com/forum/?fromgroups=#!categories/chrome/give-feature-feedback-and-suggestions
