See borgbackup in dotfiles
* vorta versus https://cyborgbackup.readthedocs.io/en/latest/user/install.html

[My 10 year old G Suite account was completely deleted today.](https://www.reddit.com/r/devops/comments/c9miod/my_10_year_old_g_suite_account_was_completely/) (2019)
* historically I used backupify but it got expensive?

convo: [Ask HN: What's your backup setup?](https://news.ycombinator.com/item?id=13694079) - 2017-02
TODO: 3-2-1 method for backups
    * [Backblaze B2 backup setup](https://www.loganmarchione.com/2017/07/backblaze-b2-backup-setup/)

Touches:
2018-03: restic for commercial, borg for personal, https://github.com/gilbertchen/duplicacy looks promising
    * Backblaze is cheapest
        * best price comparison at https://github.com/gilbertchen/cloud-storage-comparison
    * keep an eye on restic's comparison table below

## comparisons
[Borg or Restic?](https://blog.stickleback.dk/borg-or-restic/) - very fair
https://github.com/restic/others
    * [Convert the list to a csv table](https://github.com/restic/others/pull/28)

## saving to cloud
restic can do it, borg cannot really
    * https://libcloud.readthedocs.io/ - abstract interface to cloud

borg: 
* TODO: check out https://github.com/borgbackup/borg/issues/102#issuecomment-328813481
    * https://github.com/luispabon/borg-s3-home-backup
    * also workarounds like https://opensource.com/article/17/10/backing-your-machines-borg
* [Borg backup to Amazon S3 on FUSE?](https://github.com/borgbackup/borg/issues/102)
* [suggest remote backup storage options?](https://github.com/borgbackup/borg/issues/2177)

### duplicacy
biggest issue

### restic
encryption required :(
    * https://github.com/restic/restic/issues/1018

### borg
TODO: work on security
https://github.com/borgbackup/borg/pull/1719 | Clean env in 1.0 by ThomasWaldmann · Pull Request #1719 · borgbackup/borg
https://github.com/borgbackup/borg/commit/b8d954e60a50c8e432c657a3b2d0ce250213ebfb | use XDG_CONFIG_HOME for borg keys instead of ~/.borg, fixes #515 · borgbackup/borg@b8d954e
https://github.com/borgbackup/borg/issues/2935 | Specifying a global config file · Issue #2935 · borgbackup/borg


#### tips / gotchas
[Deduplication efficiency or correct application](https://github.com/borgbackup/borg/issues/3610)
    * use small chunks?


#### setups
https://github.com/witten/borgmatic
https://github.com/borgbackup/community

#### borg SECURITY
TODO: don't really understand exactly how this works
inspired by seeing my keys in dotfiles/config/borg and not knowing how they are used
    * apparently need to export? https://borgbackup.readthedocs.io/en/stable/usage/key.html#borg-key-export
        * noticed at https://wiki.hetzner.de/index.php/BorgBackup/en "In repokey mode (default), the repo key is located in the repo config, i.e. on the storage box. It is recommended that you save a backup of the key"

[barebones recovery with encrypted repositories](https://github.com/borgbackup/borg/issues/3585)
    * how do you backup/sync the password manager? https://github.com/borgbackup/borg/issues/3585

public key encryption?
    * no, see https://github.com/borgbackup/borg/issues/3585
        "it would be nice if a new symmetric key was somehow for each archive, and then encrypted using the public key"


## general
see reddit r/datahoarders

### sqlite
https://stackoverflow.com/questions/25675314/how-to-backup-sqlite-database

### 2018-01-28 s3 compatible
did a search looking for s3 compatible backups solutions; restic stands out

https://github.com/s3git/s3git | s3git/s3git: s3git: git for Cloud Storage. Distributed Version Control for Data. Create decentralized and versioned repos that scale infinitely to 100s of millions of files. Clone huge PB-scale repos on your local SSD to make changes, commit and push back. Oh yeah, it dedupes too and offers directory versioning.

https://github.com/restic/restic | restic/restic: Fast, secure, efficient backup program
https://github.com/krasimir/navigo | krasimir/navigo: A simple vanilla JavaScript router with a fallback for older browsers
https://github.com/zertrin/duplicity-backup.sh | zertrin/duplicity-backup.sh: Bash wrapper script for automated backups with duplicity supporting Amazon's S3 online storage as well as other storage destinations (ftp, rsync, sftp, local storage...).
https://mail.python.org/pipermail/borgbackup/2017q3/000779.html | [Borgbackup] Test : Borg vs Restic