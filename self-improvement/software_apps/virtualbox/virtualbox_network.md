https://stackoverflow.com/questions/23497855/unable-to-connect-to-vagrant-private-network-from-host
* also: https://code-maven.com/virtualbox-host-only-network-ssh-to-remote-machine

### gotchas
2019-01: issues with doing this for inthe.am vagrant on linux host; eventually determined it was mostly a VT-x/AMD-V BIOS flag that needed to be flipped per [How do I run a 64-bit guest in VirtualBox?](https://askubuntu.com/questions/41550/how-do-i-run-a-64-bit-guest-in-virtualbox)

### ssh
[Vagrant ssh authentication failure](https://stackoverflow.com/questions/22922891/vagrant-ssh-authentication-failure) - use `vagrant ssh-config`

[Trying to SSH to local VM Ubuntu with Putty](https://unix.stackexchange.com/questions/145997/trying-to-ssh-to-local-vm-ubuntu-with-putty)


### General
[VirtualBox Networking: an overview](http://bertvv.github.io/notes-to-self/2015/09/29/virtualbox-networking-an-overview/) - 
[Why are my two virtual machines getting the same IP address?](http://unix.stackexchange.com/questions/29999/why-are-my-two-virtual-machines-getting-the-same-ip-address) - if you don't set up an internal network, they're all isolated

### Setup
* [Network & sharing in VirtualBox - Full tutorial](http://www.dedoimedo.com/computers/virtualbox-network-sharing.html)
* [In VirtualBox, how do I set up host-only virtual machines that can access the Internet](http://askubuntu.com/questions/293816/in-virtualbox-how-do-i-set-up-host-only-virtual-machines-that-can-access-the-in)?

#### Lab
http://www.brianlinkletter.com/how-to-use-virtualbox-to-emulate-a-network/
https://www.pythian.com/blog/test-lab-using-virtualbox-nat-networking/
[(Fall 2001) Computer Science 15-441: Computer Networks](https://www.cs.cmu.edu/~srini/15-441/F01.full/www/) - one of the projects is a virtual network
[http://www.brianlinkletter.com/open-source-network-simulators/](http://www.brianlinkletter.com/open-source-network-simulators/) altho Marionnet is down

#### NAT
Basically the default gateway is set to the 10.0.2.2 which hits the host's localhost which then hits the default gateway of the host... use traceroute <somewebsite> to see
https://askubuntu.com/a/293817/457417 demonstrates how NAT seems to work:
```
# NAT interface
auto eth2
iface eth2 inet dhcp
```

### Random commands
#### Cloning
Cloning: `VBoxManage clonehd <old> <new> --format VDI` per http://www.dedoimedo.com/computers/virtualbox-clone.html