TODO: [Multiple monitors full screen mode (ticket #16829)](https://forums.virtualbox.org/viewtopic.php?f=8&t=82004)

https://unix.stackexchange.com/questions/38379/any-reason-not-to-run-linux-in-a-vm-all-the-time

[How can I import an existing .vbox virtual machine in VirtualBox?](https://superuser.com/questions/745844/how-can-i-import-an-existing-vbox-virtual-machine-in-virtualbox) - interesting trick

### debugging
https://stackoverflow.com/questions/2045234/tool-to-debug-guest-os-in-virtual-box - suggests quemu instead

### Performance
EUREKA: need to increase the size of memory allocated - drop stuff if it looks like the host is dipping into the swap file


### limitations
[Nested virtualization - is it possible? [closed]](https://serverfault.com/questions/761233/nested-virtualization-is-it-possible/761234#761234) - open feature request

### feedback loop
[fgrehm/vagrant-cachier](https://github.com/fgrehm/vagrant-cachier/issues/143) - similar to docker caching

#### Keyboard
First off, be sure to turn off the Auto Capture of the keyboard: from inside the box, Input->Keyboard->Keyboard Settings, and arrive in Preferences/Input/Virtual Box Manager where you uncheck Auto Capture Keyboard. [Per this](https://www.virtualbox.org/ticket/8383).

Check out the [Virtualbox keyboard shortcuts](http://kbmode.com/windows/virtualbox-keyboard-shortcuts/), especially [screen-capturing](http://superuser.com/questions/211606/capturing-a-window-image-or-taking-a-screen-shot-in-a-windows-xp-virtual-machine).


### Mac
Linux guests can get seamless mode however...
Mac OSX cannot install Guest Additions on guests per https://apple.stackexchange.com/q/238836/136365 which means no seamless mode there

### Snapshots / change management
[What disk image should I use with VirtualBox, VDI, VMDK, VHD or HDD?](https://superuser.com/questions/360517/what-disk-image-should-i-use-with-virtualbox-vdi-vmdk-vhd-or-hdd#comment787979_440384) - someone said that vmdk is better but it's not clear why, ZFS might be a better idea...
["Delete snapshot" is actually "merge snapshot"](https://www.virtualbox.org/ticket/6601)
[How to backup a vm incrementally](https://forums.virtualbox.org/viewtopic.php?f=1&t=39362)
https://serverfault.com/questions/171213/how-to-manage-changes-to-set-of-cloned-virtual-machines
https://superuser.com/questions/590968/quickest-way-to-merge-snapshots-in-virtualbox

[What is the difference between Virtualbox .SAV and .VDI files?](https://superuser.com/questions/878011/what-is-the-difference-between-virtualbox-sav-and-vdi-files)

### Vagrant

may need to connect an adapter? https://stackoverflow.com/a/44639272/4200039
#### env variables
https://stackoverflow.com/questions/36122348/how-to-use-config-ssh-forward-env-in-vagrant - need to configure sshd_config for SendEnv

https://stackoverflow.com/questions/40270391/shell-environment-variables-in-vagrant-files-are-only-passed-on-first-up
* solution: append to .bashrc, do not rely on .env

#### old

My thoughts:
Vagrant stores things in $HOME/.vagrand.d, including a boxes folder. The boxes folder contains the a clean copy of the box, with no added data, whereas the working copy of the box will be in the application's folder, e.g. $HOME/Virtualbox VMs. If you `vagrant destroy box`, it will destroy the working box and when you do `vagrant up` it will use the cached clean copy from the `boxes` folder. See also [How to export a Vagrant virtual machine to transfer it](http://stackoverflow.com/questions/20679054/how-to-export-a-vagrant-virtual-machine-to-transfer-it) which does not answer my primary question about the db

Look into `vagrant up --parallel` - but not available on Virtualbox.

[boxcutter](https://github.com/boxcutter) is a cool community-driven place to look for boxes; [boxcutter/centos66](https://atlas.hashicorp.com/boxcutter/boxes/centos66) seems reasonably vanilla

See [What does vagrant up do, where does the box get downloaded to?](http://www.andrew-kirkpatrick.com/2013/04/where-does-vagrant-box-get-downloaded-to/)

[Setting up a dev. environment with Vagrant](http://blog.versioneye.com/2015/05/05/setting-up-a-dev-environment-with-vagrant/) - describes how to set GUI = true and such
[Re-associate vagrant with vm](https://github.com/mitchellh/vagrant/issues/1755) - may have a clue as to how it works
`vagrant global-status --prune` can be handy, but in general check <command> --help
Source code: lib/vagrant/plugin/v2/provisioner.rb seems to be the start of a lot of magic
lib/vagrant/machine.rb seems also important
lib/vagrant/environment.rb might have the provisioner path but I'm not seeing it
lib/vagrant/util/env.rb may deal with it too

Backup database with vagrant-triggers: [VAGRANT: BACKUP DATABASE DATA BEFORE VAGRANT DESTROY](http://manuelpuchta.de/blog/vagrant-backup-database-data-before-vagrant-destroy/)

### jargon
.vbox - settings file for virtualbox  per fileinfo.com
https://spin.atomicobject.com/2013/06/03/ovf-virtual-machine/ - overview of file types

### archive
Veewee is dead -[Veewee, Packer and Kickstarting VMs Into Gear](https://blog.kintoandar.com/2015/01/veewee-packer-kickstarting-vms-into-gear.html) - Veewee lets you automate the creation of base boxes

