

### performance
vboxsf (shared folders) is famously terrible, see https://stackoverflow.com/questions/37382402/comparing-two-different-processes-based-on-the-real-user-and-sys-times

however might be fixed per [Making VirtualBox’s File System 10x Faster - Working with “find”](https://web.archive.org/save/http://techblog.en.klab-blogs.com/archives/11851752.html)

compare to NFS: https://web.archive.org/web/20170722174608/https://github.com/winnfsd/vagrant-winnfsd/issues/46

### Debugging
[[vbox-dev] Question about debugging VirtualBox](https://www.virtualbox.org/pipermail/vbox-dev/2017-June/014496.html)

### Source
https://fossies.org/linux/misc/VirtualBox-5.2.6.tar.bz2/

[Chapter 10. Technical background](https://www.virtualbox.org/manual/ch10.html)
https://www.virtualbox.org/wiki/Source_code_organization
https://github.com/jbremer/virtualbox
https://github.com/mdaniel/virtualbox-org-svn-vbox-trunk