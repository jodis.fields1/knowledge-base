TODO:
answer https://superuser.com/questions/1268741/virtualbox-ubuntu-guest-additions-not-installing-modprobe-vboxsf-failed
and
https://askubuntu.com/questions/954376/removing-default-virtualbox-guest-additions

## automation links
https://download.virtualbox.org/virtualbox/

## linux-headers-virtual linux-image-virtual
these packages are just optimized for VMs, kinda old, not really necessary

## TODO long-term
### switch to vagrant
[guest additions fails with virtualbox 5.1.22](https://github.com/dotless-de/vagrant-vbguest/issues/251) - ??

### virtualbox-ext-pack
[command to accept VirtualBox PUEL (for virtualbox-ext-pack installation)?](https://askubuntu.com/questions/811488/command-to-accept-virtualbox-puel-for-virtualbox-ext-pack-installation) - figure out how to do this unattended - also see https://unix.stackexchange.com/questions/289685/how-to-install-virtualbox-extension-pack-to-virtualbox-latest-version-on-linux

## global config
### auto capture keyboard
[Getting host to capture certain key presses in VirtualBox](https://superuser.com/a/725058/457084) - ??
### notifications suppression
https://askubuntu.com/questions/763107/how-do-i-permanently-disable-notifications-about-auto-capture-keyboard-and-mouse/1005271

## guest additions
[How can I tell if the VirtualBox guest additions were installed on an Ubuntu VM?](https://askubuntu.com/questions/169024/how-can-i-tell-if-the-virtualbox-guest-additions-were-installed-on-an-ubuntu-vm) - popular one, but see https://askubuntu.com/questions/tagged/virtualbox

[HOWTO: Install Linux Guest Additions + Xorg config (2009)](https://forums.virtualbox.org/viewtopic.php?f=29&t=15679) - from 2009, wish this was a wiki

### compiling / installing / uninstalling modules
[How can I temporarily disable a kernel module?](https://askubuntu.com/questions/317230/how-can-i-temporarily-disable-a-kernel-module) - ??
[How to blacklist kernel modules?](https://askubuntu.com/questions/110341/how-to-blacklist-kernel-modules?noredirect=1&lq=1) - ??
dkms without reboot discussion with `rcvboxdrv setup`: https://forums.virtualbox.org/viewtopic.php?f=7&t=86480&p=412450&hilit=dkms+necessary#p412449
script over at https://forums.virtualbox.org/viewtopic.php?f=3&t=86144&p=412599&hilit=dkms+necessary#p412428

### VBoxClient enable disable
On Linux, guest additions need some work to enable - basically need to rerun VBoxClient --clipboard:
https://askubuntu.com/questions/63420/how-to-fix-virtualboxs-copy-and-paste-to-host-machine - 
https://askubuntu.com/questions/692619/how-to-make-vboxclient-load-with-the-clipboard-option-when-launched - 
Shared Clipboard: install the Guest Additions, then if it still doesn't work, go to Devices->Shared Clipboard->Bidirectional

### bugs 
3D acceleration - ?
[MacOS Sierra host, CentOS 7 guest, VBoxClient clipboard breaks when copying rich text in guest](https://www.virtualbox.org/ticket/16242) - ??

### fedora
https://forums.fedoraforum.org/showthread.php?285253-virtual-box-and-kernel-headers

### screen display
change resolution:
[Make VirtualBox use full screen size in Windows 8](https://superuser.com/questions/495670/make-virtualbox-use-full-screen-size-in-windows-8)

