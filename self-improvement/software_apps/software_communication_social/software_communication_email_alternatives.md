Email clients
===

## winners / resolution
Mailsping - WINNER cause it's react
* maybe nextcloud for cloud situations?
  * originally Nylas Mail
Nextcloud Mail or Rainloop or RoundCube
TODO: try all 3

##  mail clients (updated 2018-02)
https://inboxzero.easi.net/about
https://github.com/nextcloud/mail - maybe the winner
https://github.com/RainLoop/rainloop-webmail - possible winning candidate
* not well maintained per https://github.com/RainLoop/rainloop-webmail/issues/1640
https://www.slant.co/topics/1460/~self-hosted-webmail-clients

open-source ProtonMail (angular, ugh, with PHP backend) and Tutanota, are racing to win
open-source kolab with roundcube (https://github.com/roundcube/roundcubemail) is the big guy but probably more legacy

Rejected:
  * neomutt - see https://web.archive.org/web/20180528211314/https://demu.red/blog/2017/11/neomutt-macro-opening-texthtml-attachment-from-the-index-view/
  * Zoho - proprietary plus lots of bug reports in forum plus buggy forum!
  * Axigen - self-hosted and proprietary (too much work)

### nextcloud mail
https://blog.wuc.me/2016/05/29/owncloud-mail-0-5.html
* cool notes about finding dates, compare to Gmail https://webapps.stackexchange.com/questions/80030/how-do-i-get-gmail-to-recognize-times-dates-for-google-calendar-creation

### Fastmail
Prolly the best but costs, not as open about feedback

Codebase: various open-source libraries, e.g. https://github.com/fastmail/overture (lots of NIH, no React and hardly any other libs either)
* main focus is https://github.com/jmapio/jmap JMAP protocol
Feedback: user community only at https://www.reddit.com/r/fastmail/
* [Is FastMail development in stagnation? My opinion after 3 years of using it and few suggestions.](https://www.reddit.com/r/fastmail/comments/5xswpt/is_fastmail_development_in_stagnation_my_opinion/) - 2018-02
* [Fastmail cutting loose longtime customers. Forcing everybody to go to expensive premium plans with monthly fees.](https://www.reddit.com/r/fastmail/comments/5rea18/fastmail_cutting_loose_longtime_customers_forcing/) 2017-02

### Tutanota
2018 look:
Codebase: https://github.com/tutao/tutanota - not sure why I thought this was PHP?
* more professional version locking

Feedback: https://tutanota.uservoice.com/forums/237921-general
Gotchas:
* Show stopper - [Conversation View](https://tutanota.uservoice.com/forums/237921-general/suggestions/6820765-conversation-view)

### ProtonMail
2018 look:

Codebase: https://github.com/ProtonMail/WebClient loose versions & no package-lock or yarn, but some decent e2e tests
Feedback: https://protonmail.uservoice.com/forums/284483-feedback - 
Gotchas:
* [Download of Email Archive](https://protonmail.uservoice.com/forums/284483-feedback/suggestions/9326262-download-of-email-archive) - ?
* [3-month account deletion](https://protonmail.uservoice.com/forums/284483-feedback/suggestions/19263325--accounts-that-are-inactive-for-over-3-months-may)

Cool ideas / existing features:
* filtering discussed in [Add option to reject emails based on sender](https://protonmail.uservoice.com/forums/284483-feedback/suggestions/9815199-add-option-to-reject-emails-based-on-sender)
* Planned: [Carddav Server For Contacts (Synching Contacts)](https://protonmail.uservoice.com/forums/284483-feedback/suggestions/7158338-carddav-server-for-contacts-synching-contacts)
* [Contact Groups](https://protonmail.uservoice.com/forums/284483-feedback/suggestions/7695942-contact-groups) - ??
* [Open API / Tools](https://protonmail.uservoice.com/forums/284483-feedback/suggestions/7179569-open-api-tools) - ??

### Outlook
Forced by work
Sidestep frustrations by using a third-party mail client which speaks IMAP!
[Move Trash Icon](https://outlook.uservoice.com/forums/313228--outlook-on-the-web-office-365/suggestions/14830029-move-trash-icon)
* vimium does not work!
