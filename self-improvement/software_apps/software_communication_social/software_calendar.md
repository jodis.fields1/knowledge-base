This should prolly be integrated with email...

https://x.ai - automated scheduling, seen in Andrew Yang's War on Normal People

## remind
cli calendar events:
* ps://www.baty.net/2018/remind-gets-an-update-after-3-years/
* https://github.com/jeaye/remind-warrior


## TODO: investigate alternatives
2018-04 browsed https://github.com/topics/calendar
* https://github.com/pimutils/khal (CLI calendar)
* https://github.com/itchyny/calendar.vim)

Nextcloud
* finishing up free/busy https://github.com/nextcloud/calendar/issues/39

## ics files
https://github.com/topics/ics | Topic: ics
https://github.com/jrenslin/calendar-viewer/blob/master/ics_parser.php | calendar-viewer/ics_parser.php at master · jrenslin/calendar-viewer
https://github.com/u01jmg3/ics-parser | u01jmg3/ics-parser: Parser for iCalendar Events • written in PHP 5 (≥ 5.3.9)
https://github.com/markuspoerschke/iCal | markuspoerschke/iCal: iCal-creator for PHP
https://github.com/adamgibbons/ics | adamgibbons/ics: iCalendar (ics) file generator for node.js
https://github.com/C4ptainCrunch/ics.py | C4ptainCrunch/ics.py: Pythonic and easy iCalendar library (rfc5545)
https://en.wikipedia.org/wiki/List_of_applications_with_iCalendar_support | List of applications with iCalendar support - Wikipedia

## Google
NOTE: turned on autoregistering events from email in Gmail

### calendar
pet peeve: showing optional versus non-optional meetings

https://thenextweb.com/basics/2019/04/01/how-to-add-facebook-events-to-your-google-calendar/ - these instructions were slightly off

Google Calendar - biggest problem is that syncing is delayed and integrating with Outlook is weird
https://www.quora.com/How-can-I-view-a-Microsoft-Exchange-calendar-in-Google-Calendar-I-dont-have-access-to-the-exchange-server-for-plugins-or-the-ability-to-set-up-any-kind-of-proxying-server-I-just-want-the-same-functionality-I-get-on-my-phone-in-Google-Calendar
[Google Calendar not updating published calendar added by URL](https://productforums.google.com/forum/#!topic/calendar/ixQnzHmWDSk)
[Feature Request: Manual Refresh of external calendar URL / .ics / iCal feeds](https://productforums.google.com/forum/#!topic/calendar/iXp8fZfgU2E)

### Calendly
competitors include YouCanBookMe.com and freebusy.io
how to make it figure out that I'm free personally but busy at work?
    * [Combining work and personal calendars to provided a blended view of "busy".](https://productforums.google.com/forum/#!topic/calendar/cnlMK0fIiH4)
        * similar challenge
    * Outlook allows you to mark yourself as Away?! that's good. "By default, Calendly will only consider you unavailable during Busy events, and those times will be removed from your scheduling page" per https://help.calendly.com/hc/en-us/articles/223194228-Office-365-or-Outlook-com#ooo
        * Google has only Free/Busy - how does it work when imported?
