Email
===
All about email, including my search for alternatives

#### email coding
[HEML: an open source markup language for building responsive email](https://news.ycombinator.com/item?id=15573899)
[Can I Use in HTML Emails](https://caniuse.email/)

https://www.caniemail.com/

## Gmail

Current. Frustration due to lack of developer communication and broken filter.
* turns out Important may have overriden my filter? we'll see
* can export filters? cool

### filters
[Gmailctl – Declarative Configuration for Gmail Filters (github.com)](https://news.ycombinator.com/item?id=22715982)
[How to specify “OR” conditions for Gmail filter](https://webapps.stackexchange.com/questions/686/how-to-specify-or-conditions-for-gmail-filter) - 
* this wasn't enough for me for Subject - used https://productforums.google.com/forum/#!category-topic/gmail/managing-settings-and-mail/mZDPKnR-_7A which worked - put the phrases in quotes

### Delivery Status Notification (Failure)
2019-03: started getting all these messages directed at contact@bencreasy.com and triaging ways to address it:
* [can't login or get through account recovery](https://support.google.com/mail/thread/2582784?hl=en&dark=1) - contact account recovery specialist https://support.google.com/accounts/contact/forum_ar_escalations?dark=1
* maybe open a forum thread https://support.google.com/mail/thread/new?hl=en

### gmail backup
TODO: https://github.com/jay0lee/got-your-back/wiki to backup; it includes attachments

maybe [gaubert/gmvault](https://github.com/gaubert/gmvault) with http://gmvault.org/in_depth.html#indepth Custom sync mode: `gmvault sync --type custom --imap-req 'Since 1-Nov-2011 Before 10-Nov-2011' 'foo.bar@gmail.com' -c no`

2013-11: Google finally allowing downloads of mbox data; also use  [Got Your Back to upload](http://justingale.com/2013/04/tech-tip-got-your-back-gyb-gmail-backup-restoring-gmail-to-another-account/) which will keep labels if you use -resume tag

### mail merge setup
Turns out Yet Another Mail Merge is simplest which after you create a spreadsheet is in Tools->Script Gallery->Search for it

Lots of templates but http://www.googlegooru.com/how-to-create-a-mail-merge-using-google-apps-script-google-forms/ refers to http://googleappsdeveloper.blogspot.com/2011/10/4-ways-to-do-mail-merge-using-google.html which shows how to create a mail merge template

### GSuite bring-your-own-domain
Be careful about [Bulk Sender Guidelines]
used directions at http://support.google.com/a/bin/answer.py?hl=en&answer=54719 to set up MX records

(http://support.google.com/mail/bin/answer.py?hl=en&answer=81126&topic=1669057&ctx=topic). Had to change my MX at NFSN; sent out a slightly bulk-email and it was rejected; disabled SPF (see http://www.openspf.org/) and now I'm waiting for that to propogate before testing again.


## general other
### Inbox zero
Bad: tried Unroll.me, Swizzle Sweeper, Scoop

Good: Sanebox

### Threading
with regard to threading see http://en.wikipedia.org/wiki/Conversation_threading; manual threading at http://superuser.com/questions/126356/manual-threading-in-thunderbird and http://superuser.com/questions/371592/grouping-related-e-mails-together-tags-custom-manual-threads; also http://sococare.com/conversation-threading-for-social-care/ discusses how to design these;

### Configuration

[NFS SPF records](https://members.nearlyfreespeech.net/forums/viewtopic.php?t=6365&start=0&postdays=0&postorder=asc&highlight=google+apps) [https://members.nearlyfreespeech.net/forums/viewtopic.php?t=6365](https://members.nearlyfreespeech.net/forums/viewtopic.php?t=6365) - suggests these can be edited

[http://support.google.com/a/bin/answer.py?hl=en&answer=178723](http://support.google.com/a/bin/answer.py?hl=en&answer=178723) Create SPF recordstells how to do it:

Create a TXT record containing this text: **v=spf1 include:_spf.google.com ~all**

### Understanding
[http://aplawrence.com/Blog/B961.html](http://aplawrence.com/Blog/B961.html) - Basic DNS: PTR records and why you care

[http://serverfault.com/questions/333689/how-should-i-set-up-my-ptr-record]()

