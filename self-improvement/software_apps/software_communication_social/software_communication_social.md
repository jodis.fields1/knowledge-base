Look into Mailpile and Mailtrain

DGHRW8S4ozx8

## crm
https://github.com/tdfischer/organizer

Salesforce alternatives:
* https://github.com/salesagility/SuiteCRM
  * forked from SugarCRM

https://en.wikipedia.org/wiki/OnlyOffice - has a CRM module

## social / friends management
[CommuniqAI](https://news.ycombinator.com/item?id=27642977)

https://github.com/JacobEvelyn/friends- CLI

open-book.org - https://news.ycombinator.com/item?id=17548198
dock.io - ?, value of my personal information
MonicaHQ - Personal Relationship Manager
[Socialhome: a federated personal profile with social networking functionality](https://news.ycombinator.com/item?id=16531789) - ??

Facebook - TODO download all data
* [Apps and scripts that make exported Facebook data easier to handle and organise](https://news.ycombinator.com/item?id=16743975)
* created jcrben@gmail.com associated Facebook account, which FB shut down as potentially fraudulent

CLI-based: list at https://neomutt.org/contrib/useful-programs#address-books, ppl looks good; gmail sync considered at https://github.com/hnrysmth/ppl/issues/5 - looks like it might be possible with https://github.com/pimutils/vdirsyncer/ but unclear

### analyze people
https://www.crystalknows.com/ - deleted my account

## Powerpoint alternatives

### remark
https://github.com/gnab/remark

https://github.com/kt3k/remarker - remark CLI
[Multiple Slideshows](https://github.com/gnab/remark/issues/150#issuecomment-59424315)
[Table display](https://github.com/gnab/remark/issues/128#issuecomment-46707392)
[Center table](https://github.com/gnab/remark/issues/462#issuecomment-354456887)
[Font size](https://github.com/gnab/remark/issues/104)

#### remark themes
[Official Presentation -> link back to the .md](https://github.com/gnab/remark/issues/448)
### others
[Ask HN: What alternatives to Powerpoint/Prezi are there?](https://news.ycombinator.com/item?id=15575363)
https://github.com/hakimel/reveal.js - seems to be the most popular, but too heavyweight
https://github.com/gitpitch/gitpitch - seems good, Java
https://github.com/yhatt/marp/issues/174 - Electron / CoffeeScript, might be dead
https://github.com/thorstenb/odpdown - conversion tool, seems dead

DeckSetApp: proprietary


### broadcast (Twitter, Mastodon)
#### Twitter
https://who.unfollowed.me/

http://fllwrs.com/
  
https://www.twitteraudit.com/

https://twitter.com/dvassallo/status/1333605302248251392
* did not work, need to figure out how to make notifications easier

[Engagement is the New Cocaine: The Art and Science of Writing Awesomely Addictive Tweets](https://gumroad.com/l/kegd)

[How Twitter's New Reply System Will Work](https://www.theatlantic.com/technology/archive/2016/05/how-twitters-new-reply-system-will-work/484211/) - crufty rules
Repliy to random people per https://blog.bufferapp.com/twitter-mistake

### Chatting

Signal versus Riot
    * [Messages through the OpenMarket SMS gateway don't make it through after the first one](https://github.com/vector-im/riot-web/issues/3552)
2018-03: seems like Signal is the future, but having growing pains with no backup
* couple backup apps identified at https://whispersystems.discoursehosting.net/t/decrypting-a-backup-file-outside-of-signal/2963/2

### SMS
Current choice due to backup.

[What is RCS messaging? Here’s all you need to know about the successor to SMS](https://www.digitaltrends.com/mobile/what-is-rcs-messaging/) - no standards on emoticons per https://www.nowsms.com/emoticons and https://help.simpletexting.com/en/articles/1090512-can-i-send-emojis-via-sms

#### XMPP
https://movim.eu/

Web client: [Kaiwa](http://getkaiwa.com/) - mostly dead, fork at https://github.com/digicoop/kaiwa/issues/96
Conversations - [My take on "SMS/texting support"](https://github.com/siacs/Conversations/issues/1906#issuecomment-366757751)
https://developer.pidgin.im/wiki/WhatIsLibpurple - most cross-platform
https://github.com/digicoop/kaiwa - XMPP is the protocol

#### group / team chatting online
[http://www.chitchats.co/](http://www.chitchats.co/) - 

##### slack
TODO: slack snooze still shows color in favicon
note that company can download private chats?
look into [Slack Commands](https://brightidea.slack.com/services/B0J22H29H?added=1) 

alternatives: Zulip https://news.ycombinator.com/item?id=16863675


## discord
Linux update issue; `sudo snap refresh discord --edge` per https://support.discord.com/hc/en-us/community/posts/360057789311-Discord-won-t-open-on-Linux-when-an-update-is-available

Discords:
* left Scott Santens https://discord.gg/QKF5mGF
* see subs in life-log for more

#### Listserv
Gmane - was the best, but shut down (?), go to mail-archive instead?

#### IRC and stackexchange
http://www.irchelp.org/ has list of IRC servers, but appears rather outdated

https://ircv3.net/irc/
* see revised version of standard at https://modern.ircdocs.horse/

##### servers
https://ircv3.net/software/servers

https://en.wikichip.org/wiki/mirc/commands/disconnect

https://www.ircstats.org/servers
* https://www.unrealircd.org/ has most market share

##### clients

###### the lounge
[[Discussion] Towards better settings](https://github.com/thelounge/thelounge/issues/85)
    * no per-channel settings it seems
need to use SASL https://github.com/thelounge/thelounge/issues/1026#issuecomment-294096312
number one is [the Lounge](https://news.ycombinator.com/item?id=12063689), fix it with Natifier),
* chat.freenode.net 6697
* nick: jcrben

###### others
nodejs: kiwiirc

##### infastracture / neteworks
[https://botbot.me/freenode/](https://botbot.me/freenode/) - all the logs!

[http://irclog.whitequark.org/](http://irclog.whitequark.org/) - more logs! ruby stuff esp

*freenode - happening place for software!* - 
https://github.com/charybdis-ircd/charybdis - reference implementation for IRCv3
https://github.com/atheme/atheme has alis
