## Office

## faxing
HelloFax - 

### Libreoffice
see dedicated note

### Libreoffice alternatives
[AirTable](https://airtable.com/) - kind of cool
* https://www.reddit.com/r/selfhosted/comments/g6jl1i/seatable_extensible_selfhosted_airtable/

Notion - a lot like Airtable https://www.producthunt.com/posts/notion-2-0

Excel -
* [Mesh Spreadsheet](https://news.ycombinator.com/item?id=21113176)
* Nice design: https://web.archive.org/web/20191218164744/https://adamwathan.me/img/training-spreadsheet.png
* [You Suck at Excel with Joel Spolsky](https://www.youtube.com/watch?v=0nbkaYsR94c)
  * seen from https://news.ycombinator.com/item?id=18151722
  * https://en.wikipedia.org/wiki/Pivot_table

Word - Google Docs is held out as a replacement, but as noted in  [Google Track Changes](https://productforums.google.com/forum/#!topic/docs/rKpgrqfEUXo) it doesn't work;  [Accepting and Rejecting Changes in Microsoft Word](http://perfectpageedit.com/2013/06/12/accepting-and-rejecting-changes-in-microsoft-word/) has advice on how to accept or reject all

[https://github.com/ONLYOFFICE/DocumentServer](https://github.com/ONLYOFFICE/DocumentServer) - large Backbone app!

WebODF is an intriguing web-based plugin

2015-06: trolled through its keyboard shortcuts to find CMD+OPTION+SHIFT+V for pasting unformatted, switch that to CMD+SHIFT+V!

Customizing its keyboard shortcuts is mighty unintuitive - click modify! Added macro to CMD+OPTION+S per [How do I set a keyboard shortcut to saving a version?](http://ask.libreoffice.org/en/question/2065/how-do-i-set-a-keyboard-shortcut-to-saving-a-version/)

Etherpad-lite - text editor in javascript with version control, but weird dependency on AbiWord

pandoc - one guy uses it to format docs in Sublime

## system process utilities

SysSpec - superior, portable; hardware and programs

Run msconfig.exe, then go to Tools tab. Windows also has Process Explorer. Also run services.msc or look at Administrative Tools folder

TCPView is pretty cool for seeing what ports are connected.

BartPE - PEBuilder allows computer to be boot an OS from CD or flash drive even if the regular OS is disabled

WinPatrol - standby monitor; paid for it

SubInACL -  a command-line tool that enables administrators to obtain security information about files, registry keys, and services

Wireshark - 

2014: found out to get the portable version running I need to run as admin

2013: monitors packets and network data; was trying to get the Portable version to work but no dice; note that WinPcap has to be installed for it to work; I also had to check the "capture all in promiscuous mode" because I was using a wireless router; note Riverbed Technology develops WinPcap and is big in this area - I  read some tutorials at http://chrissanders.org/2006/06/packet-school-101-part-1/ and will take a look at the Practical Packet Analysis book; I've been wanting to see how to actually look at packets and page 132 has an example of actually looking at text; 136 has a more detailed example