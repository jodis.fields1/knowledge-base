
## accessories / extensions
[Toast Cover for Lenovo X1 Carbon - 13" 3rd Generation - Real Wood - Ebony](https://www.amazon.com/dp/B01LPLFA48)

[UL Listed 65W 45W AC Charger Fit for Lenovo ThinkPad](https://www.amazon.com/dp/B07CSNTW4Z)

## specs
[Right-side view - ThinkPad T460s](https://support.lenovo.com/us/en/solutions/PD103959)