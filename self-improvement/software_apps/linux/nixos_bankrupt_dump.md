
## possible todos / open questions

## general tutorials
http://docplayer.net/50954194-A-deep-dive-into-nixos-from-configuration-to-boot-cs5250-advanced-operating-systems.html | A Deep Dive into NixOS: From Configuration To Boot CS5250: Advanced Operating Systems - PDF
https://adelbertc.github.io/posts/2017-04-08-first-nix-derivation.html | Finally Reified - My first Nix derivation
https://cdagostino.io/posts/2017-07-28-learning-to-love-nix.html | Carlos D. - Learning to love Nix (even though I previously badmouthed it), feat. Lisp and Python
https://qfpl.io/posts/nix/building-things-with-nix/ | Queensland FP Lab - Building things with Nix

## i3
https://www.reddit.com/r/NixOS/comments/5hd8ok/how_to_handle_nixos_and_i3_config_files/ | How to handle NixOS and I3 config files : NixOS
https://faq.i3wm.org/question/1773/how-can-i-configure-i3wm-to-make-alttab-action-just-like-in-windows/%3C/p%3E.html | How can I configure i3wm to make Alt+Tab action just like in Windows? - i3 FAQ

## nix for devs / nix-shell
https://github.com/uniphil/nix-for-devs | uniphil/nix-for-devs: nix-shell recipes

## overlays
https://www.google.com/search?hl=en&q=presentation%20from%20nixcon%202017%20by%20pierron%20about%20overlays | presentation from nixcon 2017 by pierron about overlays - Google Search
https://www.reddit.com/r/NixOS/comments/8flyrx/can_i_nixshell_p_apkgfrommyoverlay/ | Can I "nix-shell -p aPkgFromMyOverlay" ? : NixOS
https://nixos.org/nix-dev/2016-December/022386.html | [Nix-dev] Introducing Nixpkgs Overlays
https://blog.flyingcircus.io/2017/11/07/nixos-the-dos-and-donts-of-nixpkgs-overlays/ | NixOS: The DOs and DON’Ts of nixpkgs overlays | Flying Circus
https://github.com/NixOS/nixpkgs/issues/28558 | No documentation in manuals on using overlays for nixos · Issue #28558 · NixOS/nixpkgs


## 2018-05 no time to triage these...
https://github.com/NixOS/nixpkgs/issues/1750 | Declarative management of user environments and config files · Issue #1750 · NixOS/nixpkgs
http://ivanbrennan.nyc/2018-05-09/vim-on-nixos | Vim on NixOS · glob
https://askubuntu.com/questions/950797/how-to-take-screenshots-using-keyboard-combo-in-kubuntu-16-04-3 | how to take screenshots using keyboard combo in Kubuntu 16.04.3? - Ask Ubuntu
https://stackoverflow.com/questions/49711277/node2nix-override-wrapprogram-command-not-found | nix - node2nix override; wrapProgram: command not found - Stack Overflow
https://github.com/NixOS/nixpkgs/blob/56904d7c423f2b13b37fbd29f39bbb4b52bc7824/pkgs/stdenv/generic/setup.sh#L852-L858 | nixpkgs/setup.sh at 56904d7c423f2b13b37fbd29f39bbb4b52bc7824 · NixOS/nixpkgs
https://github.com/NixOS/nixpkgs/issues/23099 | Can we make it easier to make a derivation with no source? · Issue #23099 · NixOS/nixpkgs
https://nixos.wiki/wiki/Documentation_Gaps | Documentation Gaps - NixOS Wiki
https://github.com/craigmbooth/nix-visualize | craigmbooth/nix-visualize: Uses the Nix package manager to visualize the dependencies of a given package
