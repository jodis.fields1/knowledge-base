Nixpkgs / nixos
===

https://michael.stapelberg.ch/posts/2019-08-17-introducing-distri/ - alternative

[How to list all installed packages on NixOS?](https://www.reddit.com/r/NixOS/comments/fsummx/how_to_list_all_installed_packages_on_nixos/)
* cool trick!

BLOCKER: libinput - [How do I get a newer libinput?](https://discourse.nixos.org/t/how-do-i-get-a-newer-libinput/393)

WANTS: https://github.com/NixOS/nix/issues/896#issuecomment-325367617 | A simple protocol for derivations to report progress · Issue #896 · NixOS/nix

[See changes between two generations](https://discourse.nixos.org/t/see-changes-between-two-generations/2469)

[edolstra/nix-lang.md](https://gist.github.com/edolstra/29ce9d8ea399b703a7023073b0dbc00d) - ambitious future plans

TODO: version switcher for nix, maybe with curl https://nixos.org/releases/nix/nix-2.0.4/install | sh

TODO: [Share scripts that have dependencies with Nix](https://compiletoi.net/share-scripts-that-have-dependencies-with-nix/)

https://nixos.org/wiki/Cheatsheet
https://nixos.wiki/wiki/Resources#Wiki

[nix-shell: how to specify a custom environment variable?](https://stackoverflow.com/questions/27713707/nix-shell-how-to-specify-a-custom-environment-variable) - seems like a wrapper question, Nix Pills talks about Firefox wrapped

[NixOS is not feeling worthwhile on the desktop, since most config is still mutable...](https://www.reddit.com/r/NixOS/comments/7cgvk7/nixos_is_not_feeling_worthwhile_on_the_desktop/) - use makeWrapper

TODO: look at utilities
    * https://github.com/madjar/nox
    * kiberpipa/nix-rehash (from 2014)

TODO: use home-manager to manage dotfiles https://github.com/rycee/home-manager#graphical-services
    * [Using home-manager on Ubuntu](https://github.com/rycee/home-manager/issues/201) trickiness
    * also try https://nixos.wiki/wiki/Install_Nix_in_multi-user_mode_on_non-NixOS
    * will transition to NixUP someday

[Functional DevOps in a Dysfunctional World](https://www.youtube.com/watch?v=RsSNEkBGmj0&t=1864s) - said something intereting about how symlinks are atomic and one of the few things that are?

#### pinning packages
PRIORITY: fixing pinning - [Better package development with nix-pin and nix-update-source](https://discourse.nixos.org/t/better-package-development-with-nix-pin-and-nix-update-source/257)

https://github.com/NixOS/nixpkgs/issues/9682
https://ocharles.org.uk/blog/posts/2014-02-04-how-i-develop-with-nixos.html - from https://encrypted.google.com/search?hl=en&q=nix-shell%20default.nix

### fundamental understanding
* [How to find the name of a Nix package to install it in configuration.nix?](https://unix.stackexchange.com/questions/250263/how-to-find-the-name-of-a-nix-package-to-install-it-in-configuration-nix)

* [`~/.nix-defexpr` documentation](https://github.com/NixOS/nix/issues/487) - no idea what this does

#### intro tutorials
[Intro talks / slides on nix](https://discourse.nixos.org/t/intro-talks-slides-on-nix/353)
https://nixcloud.io/tour/?id=1

[Servers with Haskell and NixOS](https://chris-martin.github.io/talks/2018-monadic-party/)

### Deployment
Either use AWS VM import or [HOWTO install NixOS on Vultr](https://zimbatm.com/journal/2015/08/04/howto-install-nixos-on-vultr/)

### Nix the package manager
2.0: [edolstra/nix-ui.md](https://gist.github.com/edolstra/efcadfd240c2d8906348) - bunch of new commands like nix upgrade, nix use, etc
BLOCKER: `nix search` does not work
    * https://github.com/NixOS/nix/issues/1892
BLOCKER on non-nixOS: no services with systemd, 
    * solutions? https://github.com/rycee/home-manager 
    * [How does Nix manage SystemD modules on a non-NixOS?](https://unix.stackexchange.com/questions/349199/how-does-nix-manage-systemd-modules-on-a-non-nixos)
    * [How to configure a Nix environment, outside of NixOS?](https://unix.stackexchange.com/questions/369234/how-to-configure-a-nix-environment-outside-of-nixos/369510#369510)


#### maintenance
Skimmed repo:
    * old untriaged issues e.g. [https://github.com/NixOS/nix/issues/1390](Error when building coreutils on armhf) suggest overwhelmed

### NixOS
enabling services can be tricky, per [Docker service is absent from nixpkgs?](https://github.com/NixOS/nixpkgs/issues/16427) best off just searching https://nixos.org/nixos/options.html

[Sys Admin Pocket Survival Guide - NixOS](https://tin6150.github.io/psg/nixos.html)

#### link dump
https://github.com/jeaye/nixos-in-place
https://news.ycombinator.com/item?id=11041240
http://funops.co/nix-cookbook/nixops-by-example/
https://nixos.org/nixops/

## 2018-02 notes
[Linux 4.14 breakage tracking issue](https://github.com/NixOS/nixpkgs/issues/31640) - an eye into the upgrade process

[Switching to Nixos from Arch Linux](https://ramsdenj.com/2017/06/19/switching-to-nixos-from-arch-linux.html)
[The state of Nix on OS X](https://github.com/NixOS/nixpkgs/issues/18506) - ??
[Virtualization in NixOS](https://nixos.wiki/wiki/Virtualization_in_NixOS) - ??
[AppImage(s) on NixOS (doesn't work)](https://github.com/AppImage/AppImageKit/issues/472) - not great

### packer templates
https://github.com/NixOS/nixpkgs/issues/13292#issuecomment-457125269

[cstrahan/nix-packer](https://github.com/cstrahan/nix-packer) (2014) - seems to be most advanced?
[nrolland/nixos-packer](https://github.com/nrolland/nixos-packer) (2016) - most recent
[raskchanky/packer-nixos](https://github.com/raskchanky/packer-nixos) (2014) - ??

### adoption in debian
[Debian+Nix package manager: any experience or advice?](https://www.reddit.com/r/debian/comments/68al81/debiannix_package_manager_any_experience_or_advice/dgxv4og/) - Lamby, DPL, making a push

### alternatives
[Bringing OSTree to real-world desktops](https://lwn.net/Articles/697975/) - Project Atomic, Fedora Workstation, etc

### retrospectives
[Developing on NixOS](http://chriswarbo.net/projects/nixos/developing_on_nixos.html) - 

## 2017
Started using NixOS into installation issues [fix them]. Motivated by https://blog.errright.com/switching-from-homebrew-to-nix/

> so I figured out the issue, in case anyone is curious (since it probably puts a bit of a damper on newbies): I get the error "unable to check ‘https://nixos.org/channels/nixpkgs-unstable" which blocks the rest of the install script from happening; there's an FAQ on it https://nixos.org/wiki/FAQ#Error:_unable_to_check_.60https:.2F.2Fnixos.org.2Fchannels.2Fnixpkgs-unstable.60

> line in the script is https://github.com/NixOS/nix/blob/master/scripts/install-nix-from-closure.sh#L82-L84

> also see https://nixos.org/wiki/Error_Messages and the bottom one for how I ended up getting this to work with http://

##### Fisher tutorial
https://medium.com/@MrJamesFisher/nix-by-example-a0063a1a4c55
https://zef.me/setting-up-development-environments-with-nix-6ca13a5f4c57

## kernel hacking
[doc: Explain how to hack on kernel](https://github.com/NixOS/nixpkgs/commit/eb7e0d42dbe44a452dcbf9afea979ac9591c2fd1)

## old
These articles originally got me interested way back when:

[Why I Use NixOS](http://funloop.org/post/2015-08-01-why-i-use-nixos.html) - good retrospective on the problems in Arch

[Deploying NPM packages with the Nix package manager](http://sandervanderburg.blogspot.com/2014/10/deploying-npm-packages-with-nix-package.html) - interesting hack

NixOS - Zef Hemel is a major expert (see his [2013 guide](http://zef.me/blog/5966/setting-up-development-environments-with-nix) altho nix-docker is broken); in theory cool, altho there is [this complaint](https://news.ycombinator.com/item?id=8836282) about optional dependencies. Sent an email to Charles Strahan per [this comment](http://devblog.avdi.org/2015/08/11/what-its-like-to-come-back-to-a-ruby-project-after-6-months/#comment-31993)