TODO: turn this into a webapp with yaml files


## databases
https://github.com/linuxhw/HWInfo | linuxhw/HWInfo: A project to collect hwinfo reports and find devices with poor Linux-compatibility
https://linux-hardware.org/?probe=324601bf34 | HW probe of Lenovo ThinkPad T460s 20F9C ... #324601bf34
https://www.igel.com/linux-3rd-party-hardware-database/ | Linux Third Party Hardware Database - IGEL

## link dump
https://forum.manjaro.org/t/touchpad-syna3602-0911-5288-not-working-at-all/36269 | Touchpad SYNA3602 (0911:5288) not working at all - Technical Issues and Assistance / Hardware - Manjaro Linux Forum
https://serverfault.com/questions/112542/how-can-i-get-processor-ram-disk-specs-from-the-linux-command-line | How can I get processor/RAM/disk specs from the Linux command Line? - Server Fault

## system76
* https://system76.com/laptops/darter - looks pretty cool
* Jeremy Soller, creator of Redox OS, works there 9 cool Rust stuff https://changelog.com/podcast/280

## razer
Razer - has a decent discord channel Linux Blade https://discord.gg/v6gbqre
* https://blogs.gnome.org/hughsie/2018/02/11/razer-doesnt-care-about-linux/
* touchpad: https://bugzilla.kernel.org/show_bug.cgi?id=199911 followed by https://www.bountysource.com/issues/62633122-touchpad-on-tong-fang-gk5cn5z is interesting
* https://insider.razer.com/index.php?threads/so-razer-dropped-support-for-linux.44342/

## dell
https://www.dell.com/support/article/us/en/04/sln308258/precision-xps-ubuntu-general-touchpad-mouse-issue-fix?lang=en | Precision / XPS: Ubuntu General Touchpad / Mouse Issue Fix | Dell US
https://www.dell.com/community/Linux-General/Dell-XPS-9370-Developer-Edition-is-any-of-currently-available/td-p/6064008?ref=lithium_acptsoln | Solved: Dell XPS 9370 Developer Edition - is any of currently available docks is 100% compatible with it? - Dell Community
https://www.dell.com/community/Linux-Developer-Systems/New-Kaby-Lake-XPS-13-coil-whine/m-p/5088431#M7219 | Solved: RE: New Kaby Lake XPS 13: coil whine - Page 29 - Dell Community


## asus
https://www.reddit.com/r/Ubuntu/comments/8h51d3/best_zenbook_for_ubuntu/ | Best zenbook for Ubuntu? : Ubuntu
https://www.asus.com/zentalk/thread-236583-1-1.html | Asus Zenbook UX550VE Issues,bad experience-UX series
https://www.asus.com/us/support/FAQ/1032786/ | Endless OS - Introduction | Official Support | ASUS USA
https://www.reddit.com/r/linuxhardware/comments/91iord/asus_zenbook_ux550ve_issues_bad_experience/ | Asus Zenbook UX550VE Issues, bad experience : linuxhardware
https://www.asus.com/zentalk/forum.php | ASUS ZenTalk Forum
https://www.reddit.com/r/linuxhardware/comments/70mwti/review_asus_zenbook_ux430ua_linux_review/ | [Review] Asus Zenbook UX430UA Linux Review : linuxhardware

### Zenbook

Incredibly confused by the models and website has little help; for example what's diff between Asus Zenbook UX31A DB52 versus XB72 or why is the older model called ASUS Zenbook UX31E-DH52 compared to the newer ASUS Zenbook Prime UX31A-DB51

2014-03: found out that UX32VD is the only one that allows for upgrading the RAM and perhaps the HDD (7mm can be upgraded to SSD) per Youtube video Asus Zenbook Prime: SSD & RAM Upgrade!; discrete video card hurts battery but good for games and such; thinking about gettig the used one

## purism librem
[PureOS is too stable - should be able to include specific packages from Debian unstable](https://tracker.pureos.net/T366)
    * seems misguided

## candidates
been testing T460s for a while
See ThinkPad T470s or Lenovo X1 Carbon 5th or ThinkPad P52s
Rep ID 290072161 and opalma@lenovo.com

## General laptop notes

*Touchpad*

use [https://major.io/2013/08/24/get-a-rock-solid-linux-touchpad-configuration-for-the-lenovo-x1-carbon/](https://major.io/2013/08/24/get-a-rock-solid-linux-touchpad-configuration-for-the-lenovo-x1-carbon/)

use [https://github.com/BlueDragonX/xf86-input-mtrack](https://github.com/BlueDragonX/xf86-input-mtrack) or [https://github.com/JoseExposito/touchegg](https://github.com/JoseExposito/touchegg)



## official forums
* Thinkpad
*  Dell
*  Asus? https://www.phoronix.com/forums/forum/software/mobile-linux/1041349-asus-begins-offering-linux-based-endless-os-on-select-laptops

**Laptop**

2015-10: Lenovo ThinkPad X1 Carbon (Gen 3) per ArchWiki works out of the box

2015-06: Novena and Librem 15; note you can get XPS 13 with matte screen [as noted here](https://www.reddit.com/r/SuggestALaptop/comments/39tb1a/is_the_dell_xps_13_nontouch_screen_matte_or_glossy/), librem might be from entroware.com

Strangely enough, most laptops are designed (ODM) in addition to manufactured (OEM) by an outsourced Taiwan company per  [wiki list of laptop brands and manufacturers](http://en.wikipedia.org/wiki/List_of_laptop_brands_and_manufacturers). Apple is basically an Original Equipment Designer. ZaReason may use Sager or Clevo; unclear. To buy ODM computers without an intermediary use http://www.agearnotebooks.com/

originally found out this thru http://askubuntu.com/questions/32152/who-makes-laptops-for-ubuntu#comment36101_32152 and [http://www.mopo.ca/real-laptop-manufacturers.html](http://www.mopo.ca/real-laptop-manufacturers.html)

