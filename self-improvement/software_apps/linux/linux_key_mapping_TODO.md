TODO: swap keys
    * https://www.ifixit.com/Guide/Lenovo+Yoga+2+13-Inch+Keyboard+Keys+Replacement/39513
        * buy iFixit Jimmy?

One of the last projects in my Linux desktop project...

Conclusion:
Lenovo does support this since 2009; official https://support.lenovo.com/us/en/solutions/ht074187
    * detailed instructions http://www.hardstaff.com/how-to-change-lenovo-x1-carbon-keyboard-defaults/
    * 

## 2018-03 link dump
https://wiki.archlinux.org/index.php/Xmodmap | xmodmap - ArchWiki
https://wiki.archlinux.org/index.php/Xbindkeys | Xbindkeys - ArchWiki
https://askubuntu.com/questions/137172/how-to-remap-superleft-key-to-control-key | keyboard layout - How to remap Super(left) key to control key - Ask Ubuntu
https://www.google.com/search?q=fix+keyboard+shortcuts&oq=fix+keyboard+shortcuts&aqs=chrome..69i57.11212j0j7&client=ms-android-om-lge&sourceid=chrome-mobile&ie=UTF-8 | fix keyboard shortcuts - Google Search
https://www.dell.com/community/Linux-Developer-Systems/Swap-Ctrl-and-Fn-keys-on-Dell-XPS/td-p/4750978 | Swap Ctrl and Fn keys on Dell XPS - Dell Community
https://askubuntu.com/questions/249775/how-to-swap-ctrl-and-fn-on-a-lenovo-thinkpad-keyboard | xmodmap - How to swap Ctrl and Fn on a Lenovo Thinkpad keyboard? - Ask Ubuntu
https://encrypted.google.com/search?q=lenovo+keyboard+layout&hl=en&source=lnms&tbm=isch&sa=X&ved=0ahUKEwiSq_Lx6OLZAhVP0mMKHbrcBlwQ_AUICigB&biw=1435&bih=781#imgrc=M3gP6kL8Pj1BUM: | lenovo keyboard layout - Google Search
