WANTS:
* [A simple protocol for derivations to report progress](https://github.com/NixOS/nix/issues/896)


[Just moved to multi-user nix install on macOS Big Sur (nix, nix-darwin, home-manager)](https://www.reddit.com/r/NixOS/comments/k7tewl/just_moved_to_multiuser_nix_install_on_macos_big/)
* do this someday

[Setting up a new Mac from scratch using nix-darwin](https://github.com/jwiegley/nix-config/blob/master/install.org)


### errors encountered

### errors / struggles
2018-05-06: infinite recursion encountered, at undefined position error: ...
    * I had a random package (myvim.nix) in my overlays folder

### practice
TODO: this practice is cross-platform, may move it into more general file

nix-env --install -vvvv hello
    * works

### nix-shell
ultimately might be merged with nixos-containers? https://github.com/NixOS/nix/issues/903#issuecomment-323941224

some open issues from experienced people, e.g. [Running builds from inside a nix-shell breaks them](https://github.com/NixOS/nix/issues/1802#issue-289388595)

#### right
nix-shell '<nixpkgs>' --pure -A hello --run 'bash --norc --noprofile'
* very pure, sort of works but cannot find hello
* watch out for TERM https://github.com/NixOS/nix/issues/1056
    
nix-shell '<nixpkgs>' -A hello
* works: `hello` echos Hello, World!

#### wrong
nix-shell '<nixpkgs>' --run hello-2.10
* does not work, got the message from https://github.com/NixOS/nixpkgs/issues/31884#issue-275568377

nix-shell '<nixpkgs>' --pure -A hello
* did not work, lots of trouble finding things, messages about pyenv etc
* cannot find hello

nix-shell '<nixpkgs>' --pure -A hello --command 'bash --norc --noprofile'
* per https://github.com/NixOS/nixpkgs/issues/16907 and https://github.com/NixOS/nix/issues/903
* sort of works, but still inherited a bunch of stuff

#### macOS
Firefox is nixOS only:
https://github.com/NixOS/nixpkgs/blob/940fab424a9859945e8c48315d9287075d79fcf4/pkgs/applications/networking/browsers/firefox/packages.nix#L35
* https://github.com/NixOS/nixpkgs/issues/30285

single-user mode might be better:
* updating channels is tricky in macOS
  * https://github.com/NixOS/nix/issues/1889#issuecomment-367886373
* https://github.com/LnL7/nix-darwin/issues/53

apparently issues
* [How to Upgrade to Nix 2.0](https://github.com/NixOS/nix/issues/1900) - still in flux after release announcement?
* noted install / upgrade procedure seems better in 2.0 at https://github.com/NixOS/nix/issues/1759


shout-user93: ls -l ~/.nix-profile/bin/nix
12:17	shout-user93	~/.nix-profile is an empty file
12:17	clever	shout-user93: which nix
12:17	clever	and then ls -l tat
12:17	clever	that*
12:17	shout-user93	hmm
12:17	shout-user93	looks like /nix/var/nix/profiles/default/bin/nix
12:18	clever	yeah, thats roots profile
12:18	clever	ls -l that path
12:18	shout-user93	looks like this: lrwxr-xr-x 1 root wheel 14 Mar 18 12:11 /nix/var/nix/profiles/default ->
12:19	shout-user93	whoops: -> default-3-link
12:19	clever	ls -lh /nix/var/nix/profiles/default/bin/nix
12:19	zzamboni	quit
12:19	shout-user93	ah, and then I did that, and I'm seeing something...
12:19	shout-user93	so /nix/store/xmi4fylvx4qc79ji9v5q3zfy9vfdy4sv-nix-2.0/bin
12:19	pikajude	i cannot build any haskell packages on macos with a recent commit :/
12:19	pikajude	not sure which
12:20	shout-user93	can I map that hash over xmi4fylvx4qc79ji9v5q3zfy9vfdy4sv over to a commit?
12:20	clever	shout-user93: not easily
12:20	clever	normally there is a hash after the 2.0
12:20	pikajude	no, the hash in the nix store is just a hash of all the build inputs
12:21	clever	shout-user93: nix-store --query --deriver /nix/store/xmi4fylvx4qc79ji9v5q3zfy9vfdy4sv-nix-2.0
12:21	pikajude	nothing to do with the actual source rev
12:21	clever	shout-user93: what does this say?
12:21	shout-user93	unknown-deriver
12:21	clever	ah, its been GC'd, or maybe its the initial one you installed
12:22	shout-user93	probably the initial one
12:22	reinzelmann	join
12:22	{^_^}	[nixpkgs] @vcunat pushed 2 commits to release-18.03: https://git.io/vx30Q
12:22	{^_^}	→ 54804435 by @taku0: nss: 3.34.1 -> 3.35; cacert.certdata2pem: 20160104 -> 20170717
12:22	{^_^}	→ ac2378d8 by @taku0: firefox-esr: 52.6.0esr -> 52.7.2esr
12:22	shout-user93	I didn't exactly do a clean install here
12:22	clever	you could just upgrade it, then youll know what version it is
12:22	clever	sudo -i
12:22	shout-user93	I had 1.1 and followed the instructions
12:22	katona	join
12:22	clever	nix-env -iA nixpkgs.nixStable2
12:22	clever	i think
12:22	shout-user93	ah, I just did nix nix-upgrade
12:23	shout-user93	perhaps that caused an issue
12:23	{^_^}	[nixpkgs] @jtojnar pushed 44 commits to gnome-3.28: https://git.io/vx30A
12:23	{^_^}	→ 22398326 by @hedning: gvfs: fix build
12:23	{^_^}	→ 21c9d97d by @hedning: gnome3.gnome-characters: fix build
12:23	{^_^}	→ e28cceff by @jtojnar: gnome3.gucharmap: clean up
12:23	shout-user93	nix-env -iA nixpkgs.nixStable (without the 2) is running
12:23	clever	what version did it say it was installing?
12:24	nD5Xjz	join
12:24	mudri	join
12:24	shout-user93	replacing old 'nix-2.0' \n installing 'nix-2.0'
12:24	clever	ah, it should at least have the deriver this time
12:25	{^_^}	[nixpkgs] @ryantm opened pull request #37340 → unbound: 1.6.8 -> 1.7.0 → https://git.io/vx3EJ
12:25	shout-user93	then later /nix/store/isrf97h8jqyx9p51frjdmz2609a88252-nix-2.0 - but still says unknown-deriver...
12:25	clever	but we know your running whats in nixpkgs, so thats simpler
12:25	clever	one sec
12:25	clever	nix-instantiate --eval '<nixpkgs>' -A nixStable.src.urls
12:25	clever	shout-user93: that gives you the url to the exact source you just installed
12:25	shout-user93	awesome, thanks
