
[AUR Issues, Discussion & PKGBUILD Requests» Consider 'pull request' github model in AUR](https://bbs.archlinux.org/viewtopic.php?id=209593)

### 2017
https://lukeluo.blogspot.com/2015/11/how-to-debug-system-package-under.html

### 2016 attempt
First gotcha: could not connect to the internet without running dhcpcd. Need to run `ip link set enp0s3  up` and then `dhcpcd enp0s3` ([per this](https://bbs.archlinux.org/viewtopic.php?id=159435)) but I ran into an issue where the device wouldn't go up so also try `ifconfig enp0s3 up`.
Second gotcha: use pkgfile to find packages (first need to install it, pacman -Sy pkgfile)

First glance this one on Atlas (vagrant) seems good: [terrywang/archlinux](https://atlas.hashicorp.com/terrywang/boxes/archlinux)

## Dual-booting Windows
Uses the grub bootloader http://askubuntu.com/questions/148095/how-do-i-set-the-grub-timeout-and-the-grub-default-boot-entry explains how to tweak
https://www.maketecheasier.com/differences-between-mbr-and-gpt/
http://unix.stackexchange.com/questions/49165/can-grub2-share-the-efi-system-partition-with-windows

## Partition and drives
https://help.ubuntu.com/community/RenameUSBDrive - may need to rename
### Separate partitions
An annoying thing to consider when using something like Arch...
[Partitioning for Linux](http://aplawrence.com/Linux/linux_partitioning.html) 

[Put /usr/local/ and /home on separate partition](http://www.linuxquestions.org/questions/debian-26/purpose-of-putting-usr-on-separate-partition-552928/)
http://www.control-escape.com/linux/lx-partition.html is more introductory; http://www.tldp.org/HOWTO/Partition/index.html is more detailed
http://www.tldp.org/HOWTO/Partition-Mass-Storage-Definitions-Naming-HOWTO/x99.html
http://askubuntu.com/questions/56929/what-is-the-linux-drive-naming-scheme

### Swap drive
Still confused about the swapping; Understanding The Linux Virtual Memory Manager covers this topic well; I was particularly confused about what happens if you're out of memory and have no swap - see chapter 13 and page 194 for details basically it kills processes
Looked at http://unix.stackexchange.com/questions/2658/why-use-swap-when-there-is-more-than-enough-ram and https://help.ubuntu.com/community/SwapFaq

## Linux package management
Consider LinuxBrew...
Not really about Arch, but what the hell... might be outdated due to Flatpak and snapd

Fedora - use https://apps.fedoraproject.org/packages/ to find packages online
### Debian
http://askubuntu.com/a/486634/457417 - use apt-clone, end of story, also saves repository info
apt-cache rdepends can show you why a package is installed
http://askubuntu.com/questions/187888/what-is-the-correct-way-to-completely-remove-an-application/187891#187891

### Cascading? Not really sure what this is
Refers to pacman - which interestingly enough includes -u for unneeded as well as cascading as noted at http://www.archlinux.org/pacman/pacman.8.html

### Metapackages
Arch has metapackages as well - see [How do I fully remove the kde meta package?](https://bbs.archlinux.org/viewtopic.php?pid=1300472)

See https://help.ubuntu.com/community/MetaPackages. These can be tricky to uninstall see http://askubuntu.com/questions/166803/how-remove-all-programs-from-a-meta-package

### List manually installed for backup
Basically like Brew bundle dump of leaves... see http://askubuntu.com/questions/2389/generating-list-of-manually-installed-packages-and-querying-individual-packages
http://unix.stackexchange.com/questions/3595/list-explicitly-installed-packages - Gentoo can do it
How to uninstall all but the default Ubuntu packages? - not too helpful
How to view a list of packages that were manually installed without their dependancies - again just OK
Is it possible to tell what packages I've installed that aren't in the vanilla install? - again just OK

apt-mark should work but apparently by design it doesn't include default packages; this info is saved in /var/lib/apt/extended_states; to show manual with aptitude, use aptitude search ‘~i !~M’ per http://ask.debian.net/questions/how-to-get-a-list-of-manually-installed-packages
dpkg stores its information separately in /var/lib/dpkg/status and per 1.2 of the dpkg technical manual it looks like Status: Want Flag Status
