TODO: merge with dotfiles/config/nixpkgs notes

## internals / source
https://github.com/NixOS/nixpkgs/tree/master/pkgs/stdenv/generic | nixpkgs/pkgs/stdenv/generic at master · NixOS/nixpkgs

https://github.com/NixOS/nixpkgs/issues/35543 | buildPackages should not know about the top-level targetPlatform · Issue #35543 · NixOS/nixpkgs

### high-level overview
* initialization magic happens at "/nix/var/nix/profiles/system/etc/profile"
* `/run/current-system/sw` - https://github.com/NixOS/nixpkgs/blob/master/nixos/modules/config/system-path.nix
* `/etc` - https://github.com/NixOS/nixpkgs/blob/e8e4e9c2481f79c059d4f8400c5e707f636b5320/nixos/modules/system/etc/etc.nix
* environment - https://github.com/NixOS/nixpkgs/blob/master/nixos/modules/programs/environment.nix#L3

#### nixos-rebuild switch
what happens when you do this? how does the filesystem actually change?

Example: how is this evdev file written? See runCommand around there
https://github.com/NixOS/nixpkgs/blob/5de85172926ef93060086b204270bfa263b4895c/nixos/modules/services/x11/xserver.nix#L611

### garbage collection roots gcroots profiles
"Turns out, that /nix/var/nix/gcroots/profiles is a symlink to /nix/var/nix/profiles. That is very handy. It means any profile and its generations are GC roots." - https://nixos.org/nixos/nix-pills/garbage-collector.html

Not so on my mac (/nix/var/nix/gcroots/profiles does not exist) - so where are the roots?
dove into the source:
* `nix-store --gc --print-live` runs [opGC](https://github.com/NixOS/nix/blob/aa8bbbf69dfb7a1cd02237fee65c2fce39d27556/src/nix-store/nix-store.cc#L564)
* `[store->findRoots()](https://github.com/NixOS/nix/blob/2e244fb68fc222dbca99572f5cfdea5619225a21/src/libstore/gc.cc#L264)` is a bit confusing
