Software apps
===
Remember to use AlternativeTo.com or cloudsurfing.com to find alternative apps; http://www.appappeal.com;

slant.co is cool

http://www.portablefreeware.com/ is an awesome place to just for the most unobtrusive portable apps

## Code information tools
Stackoverflow - use the brackets to narrow down: [tag][tag] and then add a search within that filtered list.

issue trackers: - use tags to filter. For Chrome Devtools, it is a bit unusual: status=Assigned pri=4 for example. With nearly 60,000 issues narrowing down is essential.

## Web software

### Frontpage
2018-09: emailed tabliss about multicolumn widgets and API

See browsers_extensions#landing_pages also
    * leaning towards https://github.com/joelshepherd/tabliss
        * https://start.me is also good
        * https://freeboard.io/

https://www.reddit.com/r/opensource/comments/5pvonv/open_source_selfhosted_alternative_to_igoogle/
First see http://alternativeto.net/software/netvibes/
http://en.wikipedia.org/wiki/Template:Widget_engine

#### Checked
WINNER: Tabliss
https://start.me - pretty good! but leaning towards tabliss instead
Netvibes-
* 2018-04: Back-to-School Potions: Use Netvibes to Organize and Automate looks like an IFTTT/Zapier functionality
* old: does not filter out their apps for ones which should be removed, got rid of its forum but check email to see fractalVisionz who programmed a decent stock quotes widget
AllMyFaves - does not have widgets like Netvibes
igHome - not open-source, developer not shipping fast, breaking stuff
HeroPanel - rejected in 2018, seems on the path to death
gyroscope - rejected in 2018, closed source

## Desktop File browser
after frustration with OSX Finder (resolved by XtraFinder) I tried out a few
* https://fman.io/ - have not tried; author is cool
* https://github.com/jarun/nnn - 

## images
see ux_design_tools.md

mylio is nice

## Commander style file managers
Such as Midnight Commander (still old, weird getting it to open files correctly, but has built-in creation of text files), muCommander (somewhat abandoned), and Double Commander (prolly the best but did not try). File associations seems to be a problem.

## Music / Audiobooks
QuodLibet - probably the best??
InfraRecorder - open-source CD/DVD writer.
IsoBuster - recover data from CD/DVD
CDex - rip music
Audible - use downpour instead, activation code from https://github.com/inAudible-NG/audible-activator is f9924108 to use https://github.com/KrumpetPirate/AAXtoMP3

## video
Kdenlive - best editor per https://www.slant.co/topics/2073/viewpoints/4/~video-editors-for-linux

## viewing video
VLC - nuff said

## Tracking stuff
HomeZada - need to build my own solution
books etc - librarything mostly
* TODO: move to looked at inventaire.io
* looked at goodreads, libib and https://github.com/curiositry/ReaDB but nothing seems great; discussion around open-source replacement at https://joeyh.name/blog/entry/Goodreads_vs_LibraryThing_vs_Free_software/
* there's something out there written in coffeescript?
* calibre is a desktop app, but not really the same...

## memory
See ../../productivity/memory_software_anki.md

## desktop clipboard
Try Flycut [https://github.com/TermiT/Flycut](https://github.com/TermiT/Flycut)

CopyQ - alternative to Ditto ?? hard to use

Productivity/File Explorer - complete list at http://en.wikipedia.org/wiki/Template:File_managers

Fences is nice http://alternativeto.net/software/fences/? also see http://alternativeto.net/tag/organize-desktop/

## PDF Reader or PDF Editor
Preview & Mac signature: https://apple.stackexchange.com/questions/74950/can-i-export-my-signature-from-preview-on-one-mac-and-import-it-on-another

Ebooks:  calibre; see backup at https://manual.calibre-ebook.com/faq.html#id49 - also note CMD+F11 to show/hide controls; see https://superuser.com/questions/597803/how-do-you-toggle-the-sidebar-and-topbar-in-calibre-to-create-a-clutter-free-vie

See ./software_office_scanning.md for dawnlabs/alchemy

Okular: good bet but on OSX it requires extra setup https://github.com/KDE-mac/homebrew-kde
https://github.com/sindresorhus/awesome-electron/issues/94
qvPDF for print to PDF rather than XChange printer; note you must install GhostScript 32-bit version first!!

PDF-shuffler: per Ohloh PDF-shuffler has 1 (founder logari81 is Konstantinos Poulios and has a G+ page; home is sourceforge;

PDFsam: pdfsam no maintainers per Ohloh but has website where the developer communicates at http://www.pdfsam.org/; he's got a cool online at [http://www.sejda.com/](http://www.sejda.com/)

pdftk of PDF Labs is an open-source editor 

## rejected
Sumatra PDF - open-source! but windows only
