See quantified_health separately

[Show HN: Interface for all digital aspects of my life (beepb00p.xyz)](https://news.ycombinator.com/item?id=23101869)

## social
tried digi.me - underwhelming
exist.io is better

## time
mostly taskwarrior
install selfspy https://github.com/gurgeh/selfspy/issues/151
    * queued in task
    * also look at https://github.com/temporaer/selfspy-vis

http://www.atimelogger.com/
https://github.com/sourcegraph/thyme - covered at https://text.sourcegraph.com/thyme-a-simple-cli-to-measure-human-time-and-focus-577b87337b9c#.r0k39ukfv
I really like Yast
tried RescueTime, didn't get into it