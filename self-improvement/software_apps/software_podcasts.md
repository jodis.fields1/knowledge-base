https://en.wikipedia.org/wiki/List_of_podcatchers
best guide: https://www.tomsguide.com/us/pictures-story/555-best-podcast-apps.html#s12

## turning text into audio
[YouTube to Mp3](https://ytmp3.cc/youtube-to-mp3/)

[Listen to any web article in your podcast player w/ Audiblogs](https://www.producthunt.com/posts/audiblogs)

## video (e.g. youtube) to podcast
use youtube-dl to get the mp4, then move it onto the drive

### apps
Podcast Addict (https://podcastaddict.uservoice.com/) and Google Play are my goto
Maybe SoundCloud as well

top open-source options:
https://github.com/AntennaPod/AntennaPod
gpodder.net - management tool which is pretty weak
https://github.com/podlove - ??
https://gpoddernet.readthedocs.io/en/latest/user/clients.html#web - lists integrated with gPodder

others:
PocketCast is most popular, very minimal web app
Cloud Caster - seemed interesting, but not as good as I thought
Podcast Republic - Android, could try
BeyondPod - web+android, has customer forum http://www.beyondpod.com/forum/forumdisplay.php?17-BeyondPod-4-x-Beta-Versions
Tung - social
Satchel - local focus

#### rejected
DoggCatcher - ?

### industry leaders
https://www.blubrry.com/ - publisher found thru http://schoolofpodcasting.com/is-soundcloud-a-legitimate-option-for-podcasting-8-podcast-media-hosting-companies-compared/

#### metrics
?
