## 2021 pet peeves
drop things from option+tab app switcher
* https://apple.stackexchange.com/questions/92004/is-there-a-way-to-hide-certain-apps-from-the-cmdtab-menu
  * been trying Ghosttile
    * to activate Ghosttile & swipe to switch: open Ghosttile & Remote Desktop; then close Remote Desktop - use spotlight to navigate there (if app icon in top bar, Window->Close or CMD+W). Then restart remote desktop

## random
for docker had to run https://github.com/docker/for-mac/issues/2131#issuecomment-343712703

`softwareupdate --history` shows silent updates per https://news.ycombinator.com/item?id=20409071

TODO: [How to disable typing special characters when pressing option key in Mac OS X? [closed]](https://stackoverflow.com/questions/11876485/how-to-disable-typing-special-characters-when-pressing-option-key-in-mac-os-x)
* also: [Why have option-<key> shortcuts in text fields started inserting special characters?](https://apple.stackexchange.com/questions/205312/why-have-option-key-shortcuts-in-text-fields-started-inserting-special-charact)

TODO: [Prevent auto-pairing for certain devices (Bluetooth)](https://apple.stackexchange.com/questions/159548/prevent-auto-pairing-for-certain-devices-bluetooth)


[Xcode keeps asking for password to use the System Keychain](https://stackoverflow.com/questions/10068566/xcode-keeps-asking-for-password-to-use-the-system-keychain)
    * bit me 2018-05

TODO: [Task Switcher moves to non-primary display on Mavericks](https://apple.stackexchange.com/questions/107419/task-switcher-moves-to-non-primary-display-on-mavericks)


## security
https://mothersruin.com/software/SuspiciousPackage/

## KDE
TODO: KDE apps https://github.com/KDE-mac/homebrew-kde
## communities
* https://www.reddit.com/r/MacOS
* https://www.reddit.com/r/osx

## Must-have apps
* Apptivate - set shortcuts to pull up various programs

## TODO: automount case-sensitive filesystem
https://gist.github.com/dixson3/8360571
https://coderwall.com/p/mgi8ja/case-sensitive-git-in-mac-os-x-like-a-pro
https://apple.stackexchange.com/questions/285262/mounting-via-etc-fstab-on-os-x-but-umount-deletes-mount-point
https://www.google.com/amp/s/amp.reddit.com/r/osx/comments/2w3ybh/automount_nfs_shares/

[Ask HN: Is the new MacBook Pro worth its value for a developer?](https://news.ycombinator.com/item?id=17894252) (2018) - lots of negative experiences

###  hardware monitoring & heat & fans
https://apple.stackexchange.com/questions/59226/should-i-be-worried-about-98c-cpu-temperature-on-macbookair
    * some cool tools

https://apple.stackexchange.com/questions/204431/how-to-monitor-and-control-thermal-cpu-throttling-in-os-x
    * installed intel power gadget

### vagrant / vms
https://github.com/rgl/macos-vagrant - good for CI potentially

### 2018-03-01 link dump: set default app to open with
INSTALL duti per https://apple.stackexchange.com/a/88061/136365
See also:
    https://apple.stackexchange.com/questions/780/problem-with-always-open-with-in-os-x?noredirect=1&lq=1 | macos - Problem with "Always Open With" in OS X - Ask Different
    https://apple.stackexchange.com/questions/6387/how-to-setup-a-default-program-opening-an-unknown-file-extension?noredirect=1&lq=1 | filesystem - How to setup a default program opening an unknown file extension? - Ask Different
    https://apple.stackexchange.com/questions/287730/make-textedit-the-default-app-for-new-file-types | applications - make textedit the default app for new file types - Ask Different
    https://apple.stackexchange.com/questions/276408/terminal-command-to-list-application-defaults | Terminal command to list application defaults? - Ask Different

### General
Negative:
* [authd/taskgated error -67050/-67062 on MacOS High Sierra](https://apple.stackexchange.com/questions/312299/authd-taskgated-error-67050-67062-on-macos-high-sierra)
    * originally 2016 and again in 2018
* [Can not copy and paste within Chrome from time to time](https://apple.stackexchange.com/questions/191624/can-not-copy-and-paste-within-chrome-from-time-to-time)
    * noticed 2018-07-01
* [We need to document macOS](https://news.ycombinator.com/item?id=15001959)
* Upgrades often break shit, e.g. [macOS Sierra upgrade from a developer's perspective](https://www.reddit.com/r/programming/comments/54v7kr/macos_sierra_upgrade_from_a_developers_perspective/)
* can't upgrade to specific release easily per [How to update to Sierra NOT High sierra?](https://forums.macrumors.com/threads/how-to-update-to-sierra-not-high-sierra.2073277/)
* the [Unix utilities are aged](http://robservatory.com/behind-os-xs-modern-face-lies-an-aging-collection-of-unix-tools/). `Bash 3`, which ships with OSX Yosemite, is seven years old. You can upgrade to [`bash 4` without trouble](http://apple.stackexchange.com/questions/24632/is-it-safe-to-upgrade-bash-via-homebrew), but maybe you should try `zsh` or `fish` instead?
* [Does Apple provide a web site with content of `man` pages for the command-line commands bundled with Mac OS X?](https://apple.stackexchange.com/questions/239484/does-apple-provide-a-web-site-with-content-of-man-pages-for-the-command-line-c) - why I need to get away!

Plus:
* common, upgrades less painful as applications bundle (statically link) their library code

### Staying updated
Keep an eye on: https://developer.apple.com/library/content/navigation/

TECHNICAL NOTES: https://developer.apple.com/library/content/navigation/#section=Resource%20Types&topic=Technical%20Notes

### old
For compiling C stuff, be sure to have the Xcode CLT: https://github.com/Homebrew/homebrew-php/issues/1753#issuecomment-104285762

Scripted modifications: https://github.com/mathiasbynens/dotfiles/blob/master/.osx

TODO: [In Mac Chrome, how can I return focus from the address bar to the page?](http://superuser.com/questions/214653/in-mac-chrome-how-can-i-return-focus-from-the-address-bar-to-the-page)

## troubleshooting
https://apple.stackexchange.com/questions/59917/how-do-you-get-system-diagnostic-files-from-os-x/87982#87982

### reporting issues
Coming from the Linux world, I missed being able to see the issues that everyone was having and how long they'd been left there. Reporting via a private email or support ticket feels like sending the problem into the void. Fortunately, there is a sharing venue, even though extremely basic: [Open Radar](http://www.openradar.me/page/1). Better than nothing.

## Daemons and agents
Also see my `~/dotfiles/config/cronjobs`
List them with `sudo launchctl list`; I like `sudo launchctl list | grep -v com.apple` to see only third-party ones
Daemons: run without a user and may not access GUI; in `/Library/LaunchDaemons` or `~/Library/LaunchDaemons`.

## Setting environment variables
You can always set environment variables for command-line apps in your `.bash_profile`, but sometimes you want them to be recognized on startup and applicable to graphical apps. This is unnecessarily complex &mdash; as of Yosemite, Apple has disabled a popular methods using `/etc/launchd.conf` and `~/.MacOSX/environment.plist`. I follow the recommendations of [this Stackoverflow answer](http://stackoverflow.com/questions/25385934/setting-environment-variables-via-launchd-conf-no-longer-works-in-os-x-yosemite/26477515#26477515).

## Install modern tools
Homebrew allows you to [install Linux rather than BSD tools](How to replace Mac OS X utilities with GNU core utilities?), which you should get used to because eventually you'll end up on a Linux server where these are what's available. Plus, they have more features and bug fixes. You can use them side-by-side with a `g` prefix, e.g. `gfind` instead of `find`.

One sort of interesting sidenote is on how tweaking remove and figuring out backup. `rm` bypasses `Trash` (`Recycle Bin` in Windows), so someone wrote a wrapper called `rmtrash` as described [here](http://apple.stackexchange.com/a/17637), but it doesn't support `rm -rf`. For myself, I use Backblaze to mitigate the risk.

To deal with the variety of archive formats, you can install unar or p7zip per [this](http://superuser.com/questions/190053/universal-command-line-unarchiving-tool-on-a-mac).

Also, if you're feeling super-ambitious, you can edit and recompile the command-line tools, as described in [Making ls aware of “hidden” file flag](http://superuser.com/questions/290666/making-ls-aware-of-hidden-file-flag).

## Deficient File Manager (Finder)
Use `Shift+Cmd+G` to navigate into places OSX makes difficult to access.
Bottom Line Upfront (BLUF): Install Xtrafinder using `homebrew cask`. It addresses a lot of issues. KDE's Dolphin is super-powerful and elegant. Windows. In comparison, Finder is lacking basic functionality like creating new files, and further it broke the ability to even copy a path to a file,[^n] just in case you might want to, say, reference it in a source file or something. 

## Confusing symbols
Get used to seeing sometimes confusing symbols representing keys, as described at [here](https://support.apple.com/kb/PH7334?locale=en_US) (which is missing the baffling square bracket symbol <img style="width:10px; height:10px" src="https://www.dropbox.com/s/fay6zt8hw6n07wj/bracket_icon.png?raw=1"/>I saw in iTerm), instead of straightforward actual symbol or names.

## Miscellaneous
Disable annoying beeps per [this](http://superuser.com/questions/98868/how-do-i-disable-the-beep-in-osx-leopard-when-i-perform-an-invalid-action). If you need to make a window stay on top, install Afloat per [this](http://apple.stackexchange.com/questions/17293/set-always-on-top-for-a-window/197632#197632).

If using multiple external screens and headphones, [fix per this](http://apple.stackexchange.com/questions/82249/sound-always-comes-from-apple-thunderbolt-display-even-with-headphones-connected).

Disable annoying autoformatting per [this](https://discussion.evernote.com/topic/51156-em-dash-autoformatting/) at Apple System Preferences -> Language and Region -> Keyboard Preferences -> Text -> uncheck "Use smart quotes and dashes"


[Add the trash folder to the Finder favorites list](http://apple.stackexchange.com/questions/22274/how-can-i-add-trash-icon-to-the-sidebar-in-mac-os-x-lion).

## Keyboard Shortcuts
Make the function keys work as standard keys per [this](Switch your Mac function keys to work as standard function keys).

Sometimes take advantage of the App Shortcuts feature in Keyboard->Shortcuts. Changed "Search Notes..." to CMD+L but I have no idea how I figured how the name of the command.

Some of the OSX keyboard shortcuts conflict with other applications. I typically disable them. I also disable Mission Control shortcuts for the same reason. Mapped Mission Control itself to F10.

## Wish list
[using terminal to copy a file to clipboard](http://apple.stackexchange.com/questions/15318/using-terminal-to-copy-a-file-to-clipboard) - looking into using CopyQ or maybe even writing my own using nodejs

[^n]: See [Right-click, create a new text file. How?](http://apple.stackexchange.com/questions/94755/right-click-create-a-new-text-file-how) and [Copying the current directory's path to the clipboard](http://apple.stackexchange.com/questions/47216/copying-the-current-directorys-path-to-the-clipboard) for other hacks.
