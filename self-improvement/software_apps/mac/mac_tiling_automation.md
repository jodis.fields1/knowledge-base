

[Show HN: Phoenix 2.6 – a macOS window and app manager scriptable with JavaScript](https://news.ycombinator.com/item?id=16787836)
    * not tiling, not exciting

## tiling

### hammerspoon
TODO: save a layout or two

built-in spoons: https://github.com/ashfinal/awesome-hammerspoon/wiki/The-built-in-Spoons

Hammerspoon - under consideration
* [ashfinal/awesome-hammerspoon](https://github.com/ashfinal/awesome-hammerspoon)
* [Hammerspoon – Powerful automation of OS X with Lua](https://news.ycombinator.com/item?id=12097303) - ??
* [[OSX] Hammerspoon never die](https://www.reddit.com/r/unixporn/comments/4105pg/osx_hammerspoon_never_die/) - example configs

Someday:
* https://github.com/ashfinal/awesome-hammerspoon/wiki/Run-aria2-with-rpc-enabled
* wire up aria2c https://aria2.github.io/manual/en/html/aria2c.html#synopsis

### rejected
kwm - not sure why I found this interesting https://github.com/a-morales/kwm

chunkwm - does not restore after [Serialize desktop state to file],(https://github.com/koekeishiya/chunkwm/issues/190), also XDG config is tricky (see e.g. https://github.com/koekeishiya/khd/issues/56)
amethyst - does not persist state https://github.com/ianyh/Amethyst/issues/151
slate - unmaintained, from way back
