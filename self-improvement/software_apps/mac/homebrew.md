
## cask
TODO: https://github.com/Homebrew/brew/issues/6315

cask token conventions:
    * https://github.com/caskroom/homebrew-cask/blob/master/doc/cask_language_reference/token_reference.md#finding-the-simplified-name-of-the-vendors-distribution

multiple versions - no for Cask per https://github.com/caskroom/homebrew-cask/issues/30962#issuecomment-287084214 :(
    * revived this over at https://github.com/caskroom/homebrew-versions/issues/5035#issuecomment-365029127

homebrew-cask cu - [Always updating "latest" apps.](https://github.com/buo/homebrew-cask-upgrade/issues/74)
    * started trying to fix this

### appcast checkpoints?
was a bit baffled by https://github.com/caskroom/homebrew-cask/blob/master/doc/cask_language_reference/stanzas/appcast.md
    * not super useful in some cases due to false positives, removed in those cases https://github.com/caskroom/homebrew-cask/issues/43605#issuecomment-362836340

## general
OSX has a crippled install system involving drag&drog of .dmg files. Cleaning it up requires third-party tools (often paid). Use homebrew.

There's a weird $PATH issue with homebrew. It installs things into its own directory and symlinks them into `usr/local/bin`, which is not in the $PATH by default. You can address in a few ways, such: `export $PATH=usr/local/bin:$PATH`, or adding `usr/local/bin` to `/etc/paths`. It is fairly messy: as noted in [this](http://superuser.com/a/324617/457084) Stack Exchange discussion and in [this FAQ item](https://github.com/Homebrew/homebrew/blob/master/share/doc/homebrew/FAQ.md#my-mac-apps-dont-find-usrlocalbin-utilities), homebrew has contrary recommendations for graphical apps and command-line apps, with the former appending `/usr/local/bin/`. I modify `etc/paths` and haven't run into any issues yet. 

With `homebrew cask`, these issues don't seem to exist: the apps are installed into an `/opt/homebrew-cask` subdirectory and symlinked to `~/Applications` (though at some point moving to `/Applications`), and no further modifications are necessary.

When you uninstall a package, you can remove all the dependencies it pulled in with [this trick](http://stackoverflow.com/a/7333289/4200039).

#### linux transition
https://github.com/Linuxbrew/brew/issues/6 - ??

#### custom prefix
This should be moved to dotfiles...

Installing homebrew to a custom prefix
also look at http://askubuntu.com/questions/210210/pkg-config-path-environment-variable

18:50 cluelesscoder been looking at https://news.ycombinator.com/item?id=10309576 and the lengthy discussion on /usr/local - anyone familiar with why using /usr/local is so necessary? 
18:51 cluelesscoder I'm sort of tempted to try an alternative prefix even tho it is not recommended per this https://github.com/Homebrew/homebrew/blob/master/share/doc/homebrew/Installation.md#untar-anywhere 
18:53 tdsmith alternative prefixes actually work well in practice; we really oughta edit that document 
18:54 tdsmith /usr/local is our default because /usr/local/include and /usr/local/lib are in the default compiler and linker search paths, which means you can build things outside of homebrew without having to teach them where to find things 
18:54 tdsmith where "things" are things which you had installed with homebrew 
18:55 tdsmith the disadvantage of an alternative install location is that we build our binary packages (bottles) for /usr/local and some, but not all, of our binary packages can be automatically relocated to a different prefix 
18:55 tdsmith so you'll have to compile more things in alternative prefixes 
18:56 cluelesscoder will brew handle that compilation for me, or will I have to know the compilation commands? doing make installs or whatever is not a typical thing for me 
18:57 tdsmith it could matter if you're installing ruby gems or something that want to build against something you've installed with homebrew 
18:58 tdsmith but if you're just using homebrew it'll be transparent to you 
18:59 cluelesscoder you can build [things you installed with homebrew] outside of homebrew without having to teach them where to find thing 
18:59 tdsmith lol, sorry; replace the last instance of "thing", not the first 
18:59 tdsmith i speak real clear 
19:00 cluelesscoder it's still a little hard to understand - why would I want to build things twice? 
19:00 cluelesscoder err 
19:00 cluelesscoder I guess I'm conflating building and installing? 
19:00 tdsmith say you want to use a ruby gem "garbage" which depends on libtrash 
19:01 tdsmith you try gem install garbage and it tells you to install libtrash first 
19:01 cluelesscoder ok 
19:01 tdsmith so you brew install libtrash 
19:01 tdsmith if your prefix is /usr/local, now you can just do gem install garbage again and it should work 
19:01 cluelesscoder ah yeah 
19:02 tdsmith otherwise, you probably have to do something like CFLAGS=-I~/.homebrew/include LDFLAGS=-L~/.homebrew/lib gem install garbage 
19:02 tdsmith which is not the end of the world 
19:02 cluelesscoder ah 
19:02 cluelesscoder so gem isn't going to check my $PATH 
19:03 tdsmith it may or may not depending on whether the library installs a trash-config script or something; if it uses pkg-config, you'd have to set PKGCONFIGPATH too 
19:06 cluelesscoder I'm sort surprised that gem install involve compilation - ruby compiles files similar to the way that python generates .pyc files? 
19:06 cluelesscoder or I guess they are being compiled into an executable 
19:07 cluelesscoder eh, I'm just a web programmer, gonna save this conversation but it might be too complex for me 
19:07 tdsmith oh, gem doesn't always compile things! 
19:08 cluelesscoder most of the gems I've installed were just ruby scripts 
19:08 tdsmith but, like most interpreted languages, python and ruby have mechanisms for interfacing with libraries written in C 
19:09 cluelesscoder ah 
19:09 cluelesscoder I also thought this was interesting: "the disadvantage of an alternative install location is that we build our binary packages (bottles) for /usr/local" 
19:09 cluelesscoder I use brew cask for binaries - regular brew has binaries? 
19:11 tdsmith the formulas (almost) always describe how to build from source, but we have bots that build the formulas and produce "bottles" from the result 
19:13 cluelesscoder not sure I understand the distinction 
19:14 cluelesscoder I see that there is a --build-from-source flag tho 
19:15 tdsmith yeah, you get bottles by default if they exist and can be installed to the prefix you're using 
19:15 cluelesscoder so things would take a little longer as I'd be compiling my packages 
19:15 tdsmith the difference from just using the upstream binaries is that things are built with homebrew's versions of things and are configured to respect homebrew's directory tree 
19:15 tdsmith pretty much 
19:18 cluelesscoder as far as tweaking the compiler, do I mostly need to worry about clang? 
19:19 tdsmith if you're building third-party stuff you'll rarely be interacting with clang directly but you'll pass flags to influence its behavior, yeah