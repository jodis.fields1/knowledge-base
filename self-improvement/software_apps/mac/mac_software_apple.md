[https://developer.apple.com/library/prerelease/content/navigation/#section=Resource%20Types&topic=Guides](https://developer.apple.com/library/prerelease/content/navigation/#section=Resource%20Types&topic=Guides) - sort by type to see the various articles on topics

Keywords: mac, apple, macbook, os x, etc

Use openradar.appspot.com (powered by https://github.com/timburks/openradar) to report bugs
    * see them at https://github.com/lionheart/openradar-mirror

Apple osascript - use display dialog to debug! do shell script might also sort of work

2014-11: bought a Macbook Pro and so I'm getting used to OS X; read  [From Linux to OSX: Meet Your New Apps](http://batsov.com/articles/2012/12/09/from-linux-to-osx-meet-your-new-apps/)

[Create an emergency Mac OS X boot disk from a USB flash drive](http://www.macworld.co.uk/how-to/mac-software/create-mac-flash-recovery-drive-for-emergencies-3499802/) -  on the todo list

#### Debugging
See [Technical Note TN2124 Mac OS X Debugging Magic](https://developer.apple.com/library/content/technotes/tn2124/_index.html) per [Debugging Chromium on Mac OS X](https://www.chromium.org/developers/how-tos/debugging-on-os-x)

**Mods**

[http://ianlunn.co.uk/articles/quickly-showhide-hidden-files-mac-os-x-mavericks/](http://ianlunn.co.uk/articles/quickly-showhide-hidden-files-mac-os-x-mavericks/) and related
[http://apple.stackexchange.com/questions/34871/how-to-view-root-directory-and-subdirectories-in-finder](http://apple.stackexchange.com/questions/34871/how-to-view-root-directory-and-subdirectories-in-finder)

**Bluetooth accessories** - [http://macs.about.com/od/Troubleshooting/fl/How-to-Fix-OS-X-Bluetooth-Wireless-Problems.htm](http://macs.about.com/od/Troubleshooting/fl/How-to-Fix-OS-X-Bluetooth-Wireless-Problems.htm)

**Sysadminning -** 5 ways to be a member of a group per [http://superuser.com/questions/279891/list-all-members-of-a-group-mac-os-x](http://superuser.com/questions/279891/list-all-members-of-a-group-mac-os-x); use OSX-specific dscl

check /var/log/system.log

**Filesystem**

[FileSystemOverview](https://developer.apple.com/library/content/documentation/filemanagement/conceptual/filesystemprogrammingguide/fileSystemOverview/FileSystemOverview.html)

**Environment variables** -
Put into /Library/LaunchDaemons to run as root per https://superuser.com/a/36173/457084
[http://www.grivet-tools.com/blog/2014/launchdaemons-vs-launchagents/](http://www.grivet-tools.com/blog/2014/launchdaemons-vs-launchagents/)

**Homebrew** -

[https://github.com/beeftornado/homebrew-rmtree](https://github.com/beeftornado/homebrew-rmtree) - doesn't seem to work

remove all dependencies - see this [http://stackoverflow.com/a/30004725/4200039](http://stackoverflow.com/a/30004725/4200039)

disadvantages: see [El Capitan and Homebrew](https://news.ycombinator.com/item?id=10309576) for lengthy discussion on why using /usr/local is bad, also [Can pkgsrc, Homebrew, Fink, and MacPorts peacefully coexist?](http://superuser.com/questions/186615/can-pkgsrc-homebrew-fink-and-macports-peacefully-coexist/187804) says the samething

see this

for brew cask, tried editing to change versions with Pinta but ran into issues (.exe behind the .zip), note that you can do sha :no-check and version :latest to get around those issues

note that homebrew has no list of explicit list of installed packages and no database; it uses the directory structure to output brew list -> also see brew bundle to create a backup Brewfile

use the homebrew/versions tap to get the dev versions (see [issue 142](https://github.com/caskroom/homebrew-cask/issues/142))

use [http://braumeister.org/](http://braumeister.org/) to browse

purpose of /usr/local/opt symlink according to mistym: links to link to the latest version without needing to care whether it's publicly linked, homebrew only deletes old versions when you run `brew cleanup`

**Preferences **-

enable repeating characters - defaults write -g ApplePressAndHoldEnabled 0

enable gesture for mission control, and disable autocorrect spelling under Keyboard!

The settings locations are kind of a mess per
* [Is there any way to save Mac OS X preferences into a shell file?](http://apple.stackexchange.com/questions/118482/is-there-any-way-to-save-mac-os-x-preferences-into-a-shell-file) although there are some developer details at the  

* [Preferences and Settings Programming Guide](https://developer.apple.com/library/mac/documentation/Cocoa/Conceptual/UserDefaults/AccessingPreferenceValues/AccessingPreferenceValues.html#//apple_ref/doc/uid/10000059i-CH3-108766)

*View hidden* - downloaded Funter per [comment on this alternative](http://ianlunn.co.uk/articles/quickly-showhide-hidden-files-mac-os-x-mavericks/)

**Menu bar** - see [Customizing the OS X Menu Bar](http://computers.tutsplus.com/tutorials/customizing-the-os-x-menu-bar--mac-49391); for third-party apps you have to do it individually or use Bartender

**Software** - XtraFinder is awesome

**Uninstalling** -  [Uninstalling Applications in Mac OS X](http://guides.macrumors.com/Uninstalling_Applications_in_Mac_OS_X) at the cool wiki macrumors.com

**Progression** -  [From OSX to Arch Linux](http://shapeshed.com/from-osx-to-arch-linux/) (2013) sounds good and he ends up at suckless! should also look at his his "teach yourself node.js in 24 hours" book
