**Literature**

[Improvements in Executive Function Correlate with Enhanced Performance and Functioning and Health-Related Quality of Life](https://postgradmed.org/doi/10.3810/pgm.2010.09.2200) - seems fairly positive

Pharmacological enhancement of memory or cognition in normal subjects (2014) - focuses a bit on ampakine CX516 study in rats from 1998; racetams have weak ampakine activity

See http://www.mindnutrition.com/nootropic-guide-stacks.html for some half-lives

Overview at Nutrients and Botanicals for Treatment of Stress: Adrenal Fatigue, Neurotransmitter Imbalance, Anxiety, and Restless Sleep - Stress.pdf

**Staples** - alpha-GPC (see [GlyceroPhosphoCholineGPCforhealthyagingbrainDrParisMKidd.pdf](resources/AD42C03D107EAF973AD9E64570AE356B.pdf)), Bacopa, Gotu Kola (may reduce testosterone), Rhodiola Rosea (gave me excess salivation), Bacognize (from leader [Verdure Sciences](http://www.vs-corp.com/)

**Pills mixing**

[noob creating pills with nootropics](http://www.reddit.com/r/Nootropics/comments/1r8j4g/noob_creating_pills_with_nootropics/ "http://www.reddit.com/r/Nootropics/comments/1r8j4g/noob_creating_pills_with_nootropics/") - reddit thread

[How to use capsule filling machine with smaller doses?](http://www.reddit.com/r/Nootropics/comments/1cz0y8/how_to_use_capsule_filling_machine_with_smaller/ "http://www.reddit.com/r/Nootropics/comments/1cz0y8/how_to_use_capsule_filling_machine_with_smaller/") - another thread

**Self-testing**

Gwern [notes how to blind](http://www.reddit.com/r/Nootropics/comments/1d40a5/prl853/cbx8hub "http://www.reddit.com/r/Nootropics/comments/1d40a5/prl853/cbx8hub"): put a couple pills in a jar, shake, and take one with your eyes closed

**Websites**

Examine.com - clearly best

longecity.com - lots of thoughts

limitlessmindset.com - newcomer, not sure it's wortwhile

**Combinations**

truBrain - Andrew Hill PhD was nice on reddit

**Vendors** - other vendors in prescription only

Jarrow - recommended by silverhydra, also TrueNutrition

Nootropics Depot - run by MisterYouAreSoDumb

**Racetams**
Unifiram- relative of sunifiram, risky

*Coularacetam*- [someone on reddit](http://www.reddit.com/r/Nootropics/comments/1kzlyo/coluracetam_for_impulse_control/ "http://www.reddit.com/r/Nootropics/comments/1kzlyo/coluracetam_for_impulse_control/") said it helps with impulse control; 

*Phenylpiracetam*- supposed to be piracetam with a phenyl group to cross the blood-brain barrier easier, really enjoy this per 2014-01-17 experience; [Bam. Review of phenylpiracetam and update on my daily racetam stack](http://www.reddit.com/r/Nootropics/comments/1kifha/bam_review_of_phenylpiracetam_and_update_on_my/) says it is not the most powerful; also see [this super-geeky discussion ](http://www.reddit.com/r/Nootropics/comments/17t7tl/investigation_into_stereoselective/) on its chemistry

Effect of Oxiracetam and Piracetam on Central Cholinergic Mechanisms and Active-Avoidance Acquisition

Racetam rat studies:

 may make one focus on what was learned prior, per "Piracetam inhibits Pavlovian extinction and reversal learning in a spatial task for rats" (http://dx.doi.org/10.1016/S0028-3908(98)00064-1).

 Ameloriates learned helplessness ("Effects of piracetam on learned helplessness in rats").

 Effect of Chronic Treatment with Piracetam and Tacrine on Some Changes Caused by Thymectomy in the Rat Brain - what is tacrine? Restored functions in thymectized rats.

 Effect of Oxiracetam and Piracetam on Central Cholinergic Mechanisms and Active-Avoidance Acquisition (1986) - shows how choline was affected; oxiracetam had a longer-lasting effect

 Profound effects of combining choline and piracetam on memory enhancement and cholinergic function in aged rats (1981) - very positive effects, very highly cited

 Effect of combined or separate administration of piracetam and choline on learning and memory in the rat (1987) - no effect or negative effect, less highly cited

 Cited by Recent development in 2-pyrrolidinone-containing nootropics(1989); not freely-available.

 Author also published Do rats really express neophobia towards novel objects? Experimental evidence from exposure to novelty and to an object recognition task in an open space and an enclosed space (2009)

 Habituation of exploratory activity in mice: Effects of combinations of piracetam and choline on memory processes (1984) - supportive

 Effect of piracetam plus choline treatment on hippocampalorhythmic slow activity (RSA) and behavior in rabbits (1984) - not sure what this means, need to research RSA

 Piracetam facilitates long‐term memory for a passive avoidance task in chicks through a mechanism that requires a brain corticosteroid action (1998) - not sure

 The prominent role of stimulus processing: cholinergic function and dysfunction in cognition (2011)

*Negative evidence*

There are also some worrying studies - see, for example, this Akhundian paper (n=25)showing increased ADHD as well as another (n=30), although this Zavadenko paper (n=80) had the opposite finding. Maybe piracetam can cause some downregulation of acetycholine receptors. I dunno.

[Piracetam Accelerates Cognitive Decline?!](http://www.longecity.org/forum/topic/68051-piracetam-accelerates-cognitive-decline/ "http://www.longecity.org/forum/topic/68051-piracetam-accelerates-cognitive-decline/") - study in PAQUID cohort as summarized in [Ginkgo Biloba Extract and Long-Term Cognitive Decline: A 20-Year Follow-Up Population-Based Study](http://www.ncbi.nlm.nih.gov/pmc/articles/PMC3543404/ "http://www.ncbi.nlm.nih.gov/pmc/articles/PMC3543404/")

[Why I Don’t Supplement With Piracetam](http://selfhacked.com/2013/07/17/why-i-dont-supplement-with-piracetam/ "http://selfhacked.com/2013/07/17/why-i-dont-supplement-with-piracetam/") - negative anecdotes

**Rhodiola**

Medicine Hunter has a write-up: http://www.medicinehunter.com/rhodiola-botanical-sheet

[Panossian2010\_RhodiolaRosea.pdf](resources/963417AA311D14D23CE69DEF86DD7D1E.pdf) See attached Panossian article - page 6 discusses clinical trials and page 9 has a table (one study group had patients noticing hypersalivation); does not show effect sizes of cognitive improvement. See http://www.erowid.org/experiences/subs/exp\_Rhodiola\_rosea.shtml for experiences.

*Adverse effects*

After taking this, I've had trouble with excessive saliva. This forum thread (http://www.sjwinfo.org/forum/showthread.php?t=1992) discusses people with similar, albeit temporary, experiences. Unfortunately mine has been chronic. Gonna have to stop taking rhodiola. Note that saliva is supposed to have excessive amounts of cortisol in it. Note that a couple weeks later, I'm still having issues with excessive saliva. However, I'm wondering it if could be caused by rikkunshito.

**Vinpocetine**

Comments about noticing more color (http://www.bluelight.ru/vb/threads/124196-The-Big-and-Dandy- Vinpocetine-thread). Negative comments at longecity.com. Note examine.com mentions an immune system disorder case report; I also perused [Erowid](http://www.erowid.org/smarts/vinpocetine/ "http://www.erowid.org/smarts/vinpocetine/") but not much there 

[Role of vinpocetine in cerebrovascular diseases](http://www.ncbi.nlm.nih.gov/pubmed/21857073 "http://www.ncbi.nlm.nih.gov/pubmed/21857073") (2011) - pretty good review, full-text available

**Speculative**

[My single sentence reviews of some common (and uncommon) nootropic agents](http://www.reddit.com/r/Nootropics/comments/1hl3k1/my_single_sentence_reviews_of_some_common_and/ "http://www.reddit.com/r/Nootropics/comments/1hl3k1/my_single_sentence_reviews_of_some_common_and/") - agree with this

http://www.med.nyu.edu/content?ChunkIID=21716 He Shou Wu - not sure why I got into that one tho http://www.dragonherbs.com/prodinfo.asp?number=542 was in my bookmarks

http://examine.com/supplements/Boswellia+Serrata/

Melissa officinalis - also known as Lemon Balm; good for relaxation per http://examine.com/supplements/Melissa+officinalis/

*tyrosine*- a godsend if stressed

China's great four herbal tonics (Fo-Ti, Angelica, Lycium, and Panax Ginseng) Others rank Fo-Ti among the five major tonic herbs (Fo-Ti, Panax Ginseng, Elutheroccus senticosus, Angelica and Glycyrrhiza galabra per http://therootofthematter.net/cgi-bin/itsmy/go.exe?page=28&domain=12&webdir=therootofthematter

*Uridine*- see http://www.longecity.org/forum/topic/51802-gpc-choline-uridine-dha/ for original motivation; http://examine.com/supplements/Uridine/ is incomplete

*Citicholine (CDP-Choline)*- may be better in long-run than a-GPC given CDP-choline\#Mechanism\_of\_action which talks about preservation; http://www.reddit.com/r/ Nootropics/comments/12skmf/choline\_loading\_vs\_ache\_inhibition/c6y6axg is a discussion on downregulation

*Phenibut*- GABA which crosses the blood-brain barrier, might be good for growth hormone too

*Galantamine & Huperzine A*- acetylcholinesterase inhibitors at pharmaceutical level, used for lucid dreaming (see dreamviews.com), avoid

*Noopept*- similar to racetams but different, does not sound very good altho hyped

*Agmantine*- pain relief which silverhydra really likes

*Valproate*- also called valproic acid, see [Epilepsy drug turns out to help adults acquire perfect pitch and learn language like kids](http://www.rawstory.com/rs/2014/01/06/epilepsy-drug-turns-out-to-help-adults-acquire-perfect-pitch-and-learn-language-like-kids/ "http://www.rawstory.com/rs/2014/01/06/epilepsy-drug-turns-out-to-help-adults-acquire-perfect-pitch-and-learn-language-like-kids/") but has severe side effects

http://en.wikipedia.org/wiki/GLYX-13 - lots of buzz but advertised as an antidepressant; see http://www.naurex.com/html/glyx13.html

Ginseng - Antifatigue Effects of Panax ginseng C.A. Meyer: A Randomised, Double-Blind, Placebo-Controlled Trial http://www.ncbi.nlm.nih.gov/pmc/articles/PMC3629193/ is remarkable

Aphanizomenon flosaquae or blue green algae - found out in Jensen's 2014 review of Brain Toniq

Pycnogenol - typically used for testosterone and erectile dysfunction; cognitive enhancement in college kids [per EurekAlert](http://www.eurekalert.org/pub_releases/2011-12/mg-pft120611.php "http://www.eurekalert.org/pub_releases/2011-12/mg-pft120611.php") clinical trial by Belcaro at Pescara University published in Panminerva Medica

*NSI-189* - 20% growth in hippocampus

*Cerebrolysin*- another weird spec drug [discussed at longecity](http://www.longecity.org/forum/topic/28171-cerebrolysin/page-14 "http://www.longecity.org/forum/topic/28171-cerebrolysin/page-14")

*Picamilon* ( [wiki](https://en.wikipedia.org/wiki/Picamilon "https://en.wikipedia.org/wiki/Picamilon")) - apparently good for anxiety

*Semax* - [SEMAX Megadose](http://www.reddit.com/r/Nootropics/comments/210s14/semax_megadose/ "http://www.reddit.com/r/Nootropics/comments/210s14/semax_megadose/") has a good report
