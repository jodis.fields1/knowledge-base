## Structure

[http://en.wikipedia.org/wiki/ Dorsolateral_prefrontal_cortex](http://en.wikipedia.org/wiki/Dorsolateral_prefrontal_cortex)

[Negative correlation between right prefrontal activity during response inhibition and impulsiveness: A fMRI study](http://www.springerlink.com/index/up0wb1c9ap1wpxj2.pdf)