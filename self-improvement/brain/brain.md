# Brain
Note that gwern is the best expert on these types of things that I'm aware, see his  [dual n-back faq](http://www.gwern.net/DNB%20FAQ) for example

## General

"over-expressing the gene – a nuclear receptor called TLX – resulted in smart, faster learners that retained information better and longer" - from City of Hope by Yanhong Shi (2014)

## neurogenesis
[Adult neurogenesis](https://en.wikipedia.org/wiki/Adult_neurogenesis)
* https://www.scientificamerican.com/article/does-the-adult-brain-really-grow-new-neurons/
* https://www.researchgate.net/post/If_cancer_occurs_only_in_dividing_cells_how_can_the_brain_develop_cancer_eventhough_all_brain_cells_are_differentiated_and_dont_divide
  * "a fraction of brain tumors are of neuronal origin, usually arising in embryonic or foetal stages of the children or occasionally in neonatal life"

## Memory

Art of memory - see http://en.wikipedia.org/wiki/Art_of_memory; also Henry Lorayne is the modern guy with a ton of books; added The Memory Book (1996 but 2012 for digital edition) to my Google Play list; also Amazon has an entire category of memory self-help books http://www.amazon.com/gp/bestsellers/books/4743/ref=pd_zg_hrsr_b_1_3_last; found A Kid's Review of Higbee's Your Memory to be helpful, with one commenter pointing to Learning on Steroids as a better alternative http://www.scotthyoung.com/blog/tag/learning-on-steroids/;

## Meditation
is worth it

## Dementia and Alzheimer's

Note APOE4, also see above which discusses exercise

[People who are underweight in middle-age – or even on the low side of normal weight – run a significantly higher risk of dementia as they get older, according to new research that contradicts current thinking.](http://www.reddit.com/r/science/comments/323gb7/people_who_are_underweight_in_middleage_or_even/)

## Environmental factors

Toxins and perhaps sleep; time spent in nature: Creativity in the Wild: Improving Creative Reasoning through Immersion in Natural Settings(http://www.plosone.org/article/info%3Adoi%2F10.1371%2Fjournal.pone.0051474)

[Freeing Up Intelligence](http://www.nature.com/scientificamericanmind/journal/v25/n1/full/scientificamericanmind0114-58.html) - distraction is really significant, particularly emphasis on problems like money scarcity

See Is CO2 an Indoor Pollutant? Direct Effects of Low-to-Moderate CO2 Concentrations on Human Decision-Making Performance from Environmental Health Perspectives

[A Virus Found In Lakes May Be Literally Changing The Way People Think](http://www.reddit.com/r/science/comments/2ksln5/a_virus_found_in_lakes_may_be_literally_changing/) - wildcard in the form of ATCV-1, a chlorovirus! negatively impacts IQ apparently

## Genetics

[Genetics and intelligence differences: five special findings](http://www.nature.com/mp/journal/vaop/ncurrent/full/mp2014105a.html) (2014) 

Yale team finds protein essential for cognition - Yale team including Arnsten highlight alpha7 nicotinic receptors    

KLOTHO - people who have a variant of a longevity gene, called KLOTHO, have improved brain skills such as thinking, learning and memory regardless of their age, sex, or whether they have a genetic risk factor for Alzheimer's disease

Did a Copying Mistake Build Man's Brain? See May 11 issue of the journal Cell and http://www.livescience.com/20102-copying-mistake-build-man-brain.html

Allen Institute For BrainScience is creating the "Allen Atlas" of the brain, which sounds amazing

Note that I'm genetically predisposed to Alzheimer's; see epithilone D as an anti-tau medication

http://www.cell.com/trends/genetics/abstract/S0168-9525%2812%2900158-8 for Crabtree's speculation that we may be getting stupider

## Toxins

The Amazon reviewer C. Qu that I discovered through a 1-star review of Sleep: A Comprehensive Handbook