## Training software
Ended up with a Muse

### tests
https://mensa.dk/iqtest/ - actual Raven's progressive matrices?

digit span - 
* 2018: did this again at http://cognitivefun.net/test/7; started at 3, stopped at 6
    * prompted by noopept investigation  https://www.reddit.com/r/Nootropics/comments/56rwge/noopept_10day_experience/d8m12ce/


### tools
Dual N-Back software

Quantified Mind - http://www.quantified-mind.com
    * looks abandoned

CogMed

Cognifit - tried this and didn't like the game much

NeuroRacer - per How to Rebuild an Attention Span works

Luminosity - biggest probably; study is 500,000 person lumosity study examines optimizing cognitive training tasks to accelerate learning

### studies
Eye Blood Vessels Linked to Cognitive Function, IQ - tied to retinal venular caliber

Benefits of cognitive training can last 10 years in older adults - Advanced Cognitive Training for Independent and Vital Elderly, or ACTIVE by Unverzagt

### IQ testing

http://iqtest.dk/main.swf - I did not score well on this


