**Exercise**

Healthy Lifestyles Reduce the Incidence of Chronic Diseases and Dementia: Evidence from the Caerphilly Cohort Study - read summary  [thru reddit](http://www.reddit.com/tb/1sjzl1) and it shows a -60% for dementia

[Physical inactivity and cognitive functioning: results from bed rest studies](http://www.springerlink.com/content/n4r707714j8t7512/)

http://well.blogs.nytimes.com/2013/04/10/how-exercise-may-boost-the- brain/ suggests that resistance and aerobic have different effects, runners’ brains showed increased levels of a protein known as BDNF

Noticed this article [In-Your-Face Fitness: Dumbbells can make you brainy](http://www.latimes.com/health/la-he-fitness-mind-20120213,0,7344477.story) - while I've heard this before, particularly interesting was that "In 2000, Dutch researchers published a study of 347 men, some of whom were genetically prone to Alzheimer'sdue to a certain gene variant. Adjusting for a number of confounding factors such as smoking, drinking and education, the researchers found that the inactive couch potatoes with the brain-wasting gene variant were four times more likely to develop Alzheimer'sthan the workout warriors who carried the trait" - that could be me! Scary. 2013 update:  [Meta-analysis of 74,046 individuals identifies 11 new susceptibility loci for Alzheimers disease](http://www.nature.com/ng/journal/vaop/ncurrent/full/ng.2802.html) found more SNPs; also see [http://well.blogs.nytimes.com/2014/07/02/can-exercise-reduce-alzheimers-risk/](http://well.blogs.nytimes.com/2014/07/02/can-exercise-reduce-alzheimers-risk/)

Aerobic vesus anaerobic: see Aerobic Exercise or Weight Training to Boost BrainFunction? which suggests weight-training might be better; http://healthland.time.com/2012/07/16/mind-those-reps-exercise-especially-weight-lifting-helps-keep-your- brain-sharp/ says weight-training prevents Alzheimer's; http://well.blogs.nytimes.com/2011/01/19/phys-ed-brains-and-brawn/ discusses rats and BDNF

Exercise and the brain: something to chew on(http://www.ncbi.nlm.nih.gov/pmc/articles/PMC2680508/) - also covers nutrition

Genetic component to exercise desire per study by Theodore Garland, Jr. in Proceedings of the Royal Society B, in 1993 with 224 mice