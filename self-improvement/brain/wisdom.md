See also productivity and life-planning.md

https://www.goodreads.com/genres/entrepreneurship | Entrepreneurship Books
https://www.librarything.com/author/druckerpeterf | Peter F. Drucker | LibraryThing

## Stoicism
[Can someone explain this algorithm in detail? ELI5, preferably : Stoicism](https://www.reddit.com/r/Stoicism/comments/73n2wn/can_someone_explain_this_algorithm_in_detail_eli5/)
https://www.goodreads.com/work/quotes/20739-aphorismen-zur-lebensweisheit | Essays and Aphorisms Quotes by Arthur Schopenhauer
https://www.goodreads.com/author/quotes/17212.Marcus_Aurelius | Marcus Aurelius Quotes (Author of Meditations)
https://www.producthunt.com/newsletter/459?utm_campaign=459_2017-10-29&utm_medium=email&utm_source=Product+Hunt | Brain-enhancing music for work 🎧 - Product Hunt
https://www.reddit.com/r/Meditation/comments/7kuh17/barry_long_start_meditating_now/ | Barry Long | Start Meditating NOW : Meditation
