https://hackr.io - voting up best

[Academind](https://www.youtube.com/channel/UCSJbGtTlrDami-tDGPUV9-w)

[The Best MOOC Provider: A Review of Coursera, Udacity and Edx - SkilledUp.com](http://www.skilledup.com/articles/the-best-mooc-provider-a-review-of-coursera-udacity-and-edx)

"How to Learn Anything Quickly : An Accelerated Program for Rapid Learning" Ricki Linksman, 1999

Minimalist theory of education: http://www.instructionaldesign.org/theories/minimalism.html

## courses
Bradfield CS - ask Rohan Pethiyagoda about a referral

## teaching
BeOnDeck - https://twitter.com/beondeck/status/1309171336011542547

## Computer science roadmap
https://teachyourselfcs.com/
[In 2017, learn every language](https://news.ycombinator.com/item?id=13291593)
https://www.bottomupcs.com/index.xhtml
https://news.ycombinator.com/item?id=14518938

### Learning general
https://www.farnamstreetblog.com/2017/02/naval-ravikant-reading-decision-making/
[https://medium.com/learning-new-stuff/a-simple-technique-to-learn-hard-stuff-ffaa7879bf7c](https://medium.com/learning-new-stuff/a-simple-technique-to-learn-hard-stuff-ffaa7879bf7c)

On learning difficult things by So8res (2013) - lesswrong thread that has a bunch of great advice, esp pairing up to avoid difficult mistakes as I can empathize with that

Seeking Wisdom - good blog by Jana Vembunarayanan which I found thru  [Tales from the Genome – 1](http://janav.wordpress.com/2014/07/04/tales-from-the-genome-1/); see his  [list of books to read](http://janav.wordpress.com/2014/03/12/books-i-wish-i-had-read-before-20/)

[noexcuselist.com](http://www.noexcuselist.com/) - cool conglomeration

### Audiobooks
http://www.openculture.com/freeaudiobooks

http://www.booksshouldbefree.com/book/the-art-of-war-by-sun-tzu