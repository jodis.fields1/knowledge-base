# Electricity theory

Copied from Science general   

**Danger**

[Re: What is dangerous to a person, amps or volts?](http://www.madsci.org/posts/archives/1998-08/899698536.Eg.r.html "http://www.madsci.org/posts/archives/1998-08/899698536.Eg.r.html") - depends! energy from voltage can cause burns but amps can cause fibrillation stopping the heart

**Wiring**
_Three prongs and ground_

Started by reading  [WHY THREE PRONGS? Electrical ground, stray voltage](http://amasci.com/amateur/whygnd.html "http://amasci.com/amateur/whygnd.html"), also note at the bottom the multiple meanings of ground (and yes, ground rods do exist solely for safety altho not always effective if soil is dry per 2002  [In Southeast, Ground Rods May Not Protect Homes Against Lightning](http://news.ufl.edu/2002/08/15/lightningrod/ "http://news.ufl.edu/2002/08/15/lightningrod/")); if current flows thru ground there's a "ground fault" or "leakage current". Basically my understanding this should only happen when the insulation around a hot wire is frayed and it touches the case or something else rather than flowing through intended path. Thus ground fault circuit interrupters (GFCI)

Current flows in a loop, so there's hot (colored black) and neutral (colored white). Third prong ("ground" but not literally to the ground rod but rather same as neutral) is typically bare and connected to metal case and flows back to neutral to allow electricity to escape back (see  [What is the difference between two- and three-pronged plugs?](http://electronics.howstuffworks.com/everyday-tech/question110.htm "http://electronics.howstuffworks.com/everyday-tech/question110.htm")). The ground is low resistive (see  [hyperphysics household wiring](http://hyperphysics.phy-astr.gsu.edu/hbase/electric/hsehld.html "http://hyperphysics.phy-astr.gsu.edu/hbase/electric/hsehld.html") page)

Also see  [ Understanding 220 or 240 volt Electrical Circuits](http://www.nojolt.com/Understanding_240_volt_circuits.shtml "http://www.nojolt.com/Understanding_240_volt_circuits.shtml") - note that for 240v, the "out of phase" existence allows the positive and neutral to both flow electricity and complete the circuit

**Electricity** \- note electricity & magnetism (E&M) is typically the hardest undergrad physics course

William J. Beaty seems to be incredibly knowledgeable about electricity http://www.eskimo.com/~billb/miscon/whatis.html and also  [ "ELECTRICITY" MISCONCEPTIONS IN K-6 TEXTBOOKS](http://amasci.com/miscon/eleca.html "http://amasci.com/miscon/eleca.html") - - -William J. Beaty

http://www.allaboutcircuits.com/

http://galileo.phys.virginia.edu/classes/252/home.html

Safety: still don't understand grounding; also safety is covered at

Electricity and Magnetism by Purcell is the classic text

2014-02: motivated by Pauling's General Chemistry (Ch.3), I read up a bunch on this; prior edition of Pauling's   [College Chemistry](https://archive.org/details/CollegeChemistry "https://archive.org/details/CollegeChemistry") is available online

Detection of a Magnetic Field - at School for Champions Ron Kurtus has a pretty good overview

Lorentz Force -   [per Ron Kurtus](http://www.school-for-champions.com/science/magnetism_lorentz.htm#.UvkSAbTYO-U "http://www.school-for-champions.com/science/magnetism_lorentz.htm#.UvkSAbTYO-U") moving electrons are repulsed by 

Electromagnetic induction - transforms kinetic energy into electricity; first "dynamo" (generator) was a Homopolar generator (also called a Faraday disc); see wiki and note Maxwell's laws

Triboelectric effect is key to understanding this as it displays voltage, also it can be played with using an   [ Electrostatic generator](https://en.wikipedia.org/wiki/Electrostatic_generator "https://en.wikipedia.org/wiki/Electrostatic_generator"), also see nifty   [little gadget by Poupyrev about a paper generator](http://makezine.com/2013/10/10/new-technology-paper-generators-harvest-static-electricity/ "http://makezine.com/2013/10/10/new-technology-paper-generators-harvest-static-electricity/"); 

[ Electrostatic Experiments at Rice University](http://outreach.phys.uh.edu/electexperiments.htm "http://outreach.phys.uh.edu/electexperiments.htm") the Bending Water with a Balloon is particularly cool

The Feynman Lectures on Physics, Vol. II: The New Millennium Edition: Mainly Electromagnetism and Matter (Volume 2) - someday perhaps

_Voltage_ -

6 sources of voltage but by far most common is magnetism per Ch.3 voltage of Introduction to Electronics (2011) by Earl Gates

really like the difference in height analogy from electronics.stackexchange  [What exactly is voltage?](http://electronics.stackexchange.com/questions/50976/what-exactly-is-voltage "http://electronics.stackexchange.com/questions/50976/what-exactly-is-voltage"). Confused by difference from battery voltage and capacitance so see  [What is the difference between a battery and a charged capacitor?](http://physics.stackexchange.com/questions/32391/what-is-the-difference-between-a-battery-and-a-charged-capacitor "http://physics.stackexchange.com/questions/32391/what-is-the-difference-between-a-battery-and-a-charged-capacitor")

_Changing voltage_

Mystified by how to change voltage, but learned more at wiki   [ Transformer#Induction_law](https://en.wikipedia.org/wiki/Transformer#Induction_law "https://en.wikipedia.org/wiki/Transformer#Induction_law") - and finally learned how at physics.stackexchange.com   [How do transformers work?](http://physics.stackexchange.com/questions/51936/how-do-transformers-work "http://physics.stackexchange.com/questions/51936/how-do-transformers-work") which uses inductors (coils of wire around a core of either plastic or a ferromagnet); 

Watched   [E&M at Khan Academy](https://www.khanacademy.org/science/physics/electricity-and-magnetism "https://www.khanacademy.org/science/physics/electricity-and-magnetism") - the key http://electronics.stackexchange.com/questions/50976/what-exactly-is-voltageon uniform infinite charged plates is that each point charge (of which there are infiinite) projects lines radially - so not straight up but all over

Electric Potential Difference -   [ physicsclassroom article](http://www.physicsclassroom.com/class/circuits/u9l1c "http://www.physicsclassroom.com/class/circuits/u9l1c") seems decent

**Transformers and taps**

Based on   [How do utility companies treat voltage deviations in distribution networks?](http://electronics.stackexchange.com/questions/25964/how-do-utility-companies-treat-voltage-deviations-in-distribution-networks "http://electronics.stackexchange.com/questions/25964/how-do-utility-companies-treat-voltage-deviations-in-distribution-networks") I went into transformer wiki and found tap, center tap (  [wiki](https://en.wikipedia.org/wiki/Center_tap "https://en.wikipedia.org/wiki/Center_tap")), and   [ Winding configurations](http://www.allaboutcircuits.com/vol_2/chpt_9/5.html "http://www.allaboutcircuits.com/vol_2/chpt_9/5.html") from allaboutcircuits; electrical-engineering-portal has   [transformers category](http://electrical-engineering-portal.com/category/transformers-2 "http://electrical-engineering-portal.com/category/transformers-2"); also see   [model transformer](http://www.nuffieldfoundation.org/practical-physics/model-transformer "http://www.nuffieldfoundation.org/practical-physics/model-transformer"); 

See   [How Transformers Work](https://www.youtube.com/watch?v=ZjwzpoCiF8A "https://www.youtube.com/watch?v=ZjwzpoCiF8A") for a video that I watched, not really clear at all
