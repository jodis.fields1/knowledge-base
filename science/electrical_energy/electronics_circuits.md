# Electronics and circuits
**Integrated circuits**

Back in the day hobbyists played with circuits and built computers; read Whatever Happened to Computer Hobbyists? (May, 1982);

The Integrated Circuit Hobbyist's Handbook (negative review on Amazon); TTL Cookbook sounds like it is the best

Make Magazine is probably best for looking into this; see Make: Electronics

**Books**

The Art of Electronics - old book republished in 1989

TTL Cookbook - old book on TTL (transistor-transistor logic)

**Electricity draw**
P3 International Kill-A-Watt is the standard; however, there's also Belkin Conserve Insight at http://www.amazon.com/Belkin-Conserve-Insight-Energy-Use-Monitor/dp/B003P2UMP8/ and a list of other products such as http://www.amazon.com/Watts-Pro-Electricity-Consumption-Meter/dp/B000CSWW92/

http://www.amazon.com/Energy-Saving-Monitor-Complete-Transmitter/dp/B002J9IDSG/ref=pd_sxp_grid_pt_2_0 is a more comprehensive item it seems like

**Power management and electric engineering**
Reading on Grano.la's blog about static and dynamic powerhttp://grano.la/blog/faq/#how_are_the_savings_numbers_computed, I ran across http://www.edaboard.com/thread67491.html What is static powerdissipation and dynamic powerdissipation? - note that EDA is probably electronic design automation

Hibernate versus suspend (sleep) - hibernate should consume zero power but was disabled in Kubuntu  [per forum thread](http://www.kubuntuforums.net/showthread.php?58931-How-to-enable-Hibernate-on-%28Kubuntu-system-menu-gt-Leave%29 "http://www.kubuntuforums.net/showthread.php?58931-How-to-enable-Hibernate-on-%28Kubuntu-system-menu-gt-Leave%29") which points to  [a Ubuntu page](https://help.ubuntu.com/12.04/ubuntu-help/power-hibernate.html "https://help.ubuntu.com/12.04/ubuntu-help/power-hibernate.html") on it

upower -d is a quick tool

**Multimeter**

2014-07: got into this again with dying car battery;  [EVERYONE Needs a Multi-Meter](http://www.instructables.com/id/EVERYONE-Needs-a-Multi-Meter/?ALLSTEPS "http://www.instructables.com/id/EVERYONE-Needs-a-Multi-Meter/?ALLSTEPS") from Instructables seems like the best

Bought a multimeter to measure the draw of my computer on/off Grano.la; however turns out I have to split the wire (or shave off the insulation if using a clamp). Don't really want to do that.

http://www.youtube.com/watch?v=bF3OyQ3HwfU (THE BEST MULTIMETER TUTORIAL) was helpful

**Universal AC adapter** \-  

<https://en.wikipedia.org/wiki/AC_adapter#Substitutes>

http://dannythecomputerguy.com/cons-of-universal- power-adapters.html cynical assessment

AC, which is drawn from the "mains", is drawn at different rates through this http://en.wikipedia.org/wiki/AC_adapter - as the article mentions, universal adapters exist (voltage regulation is not universal; not sure what the impact is); Green Plug was proposed http://www.greenplug.us by former VESA cofounder, but he found that first they needed to solve the cost problem of mixed-signal integrated circuits (IC), which was done in late 2011 per http://altenergymag.com/emagazine.php?art_id=1879.

  


Toms Hardware review of 3 types at http://www.tomshardware.com/reviews/laptop-universal- power-adapter,2852-4.html

_Innergie_

Innergie mCube - partnered with Green Plug, but it is still based on tips

PURE radios partnered with Green Plug in 2009 per http://www.businesswire.com/news/home/20091112005370/en/Green-Plug-Announces-Adopter-Smart- Power-Adapter

  


**Wireless power**

Hype at http://www.forbes.com/sites/davidferris/2012/07/24/how-wireless-charging-will-make-life-simpler-and-greener/

No discussion of health effects that I can see
