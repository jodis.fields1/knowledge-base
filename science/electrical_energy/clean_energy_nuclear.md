# Clean energy - nuclear

## general
2013-10: splurged on reading about this particularly motivated by a Quora question about solar in Germany and how it decommissioned the nuclear due to their lack of load-following abilities (instead they do baseload power); note that nuclear is the solution to  [intermittent energy](http://Intermittent energy source "http://Intermittent energy source")

Bill Gates stops chasing nuclear ‘wave’, pursues variety of reactors - TerraPower is not working out quite right I guess

Is This the Bill Gates of Energy?: Meet Nuclear Entrepreneur Taylor Wilson (18) - genius kid funded by Peter Thiel

First new nuclear reactors OK'd in over 30 years - Westinghouse (Toshiba sub) design AP1000 at cost of $14b for 2200 MW (enough for 1 million homes); Greg Jaczko (chair) dissented and is pretty controversial

Why baseload power is doomed - somewhat contrarian, good overview of grid

Can renewables provide baseload power? - says they can

Nuclear Plant Load Following Capabilities - Independent EnergyProducers Association out of Cali Lynn Walters ppt

EnergyCosts Will Rise ‘Viciously’ - Motley Fool thread where people have some insight, for example waterfell says "One problem with nuclear plants and load following is in the build-up and burn-out of xenon inside the fuel. Xenon is a huge neutron poison that builds up when power is decreased..."


## waste
https://www.hcn.org/issues/52.1/nuclear-energy-nuclear-power-is-emissions-free-but-at-what-cost-waste

## Fusion

A Star in a Bottle (2014-03) - New Yorker article on ITER tokamak; also led me to read The MFTF-B story – Fusion or conFusion? (2013) which was shut down without even getting turned on

_Small modular reactor_

NuScale’s SMR Chosen by Western Initiative for Nuclear \- Fluor subsidiary out of OSU

First "Small Modular" Nuclear Reactors Planned for Tennessee \- 180MW Babcock & Wilcox mPower

Can Nuclear Power Be Flexible? - research article on load-following abilities

Advanced Nuclear Power Reactors - World Nuclear Association notes that Areva's EPR can "maintain its output at 25% and then ramp up to full output at a rate of 2.5% of rated power per minute up to 60% output and at 5% of rated output per minute up to full rated power"

Future May Be Bright For Modular Nuclear Reactors - Lynch is an expert