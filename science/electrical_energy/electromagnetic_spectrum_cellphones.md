Electromagnetic spectrum and cellphones
===

See science general and chemistry

## Elecromagnetics
Added something to ankiweb about electromagnetics, but given how much reading I've been doing I should probably record more. Motivated by KINY radio and their frequency (800 AM), I looked into http://en.wikipedia.org/wiki/Radio_spectrum - as I suspected, all cellphones use the ultra high frequency (technically microwaves) described in more detail at http://en.wikipedia.org/wiki/Cellular_frequencies

Per Does AM or FM Radio Have Longer Range (Distance)? http://answers.yahoo.com/question/index?qid=20100326103858AA4J6FV the lower frequency AM waves 535 - 1605 kHz can refract off the ionosphere during the night so have a range across the Earth; higher frequency (88 - 108 MHz) go right through the ionosphere

### Amateur Ham Radio
http://www.arrl.org/getting-licensed is the best place to start learning

Seems like a great way to learn this stuff; watched Ham Nation http://twit.tv/show/ham-nation/69 in Oct 2012 (noticed old age); found http://kb9mwr.blogspot.com/ which notes old and age points to Linux-based radio UDR56K-4 Universal Digital Radio; could also build one with a kit http://www.hamradiosecrets.com/ham-radio-kits.html; also looked at http://www.arrl.org/what-rig-should-i-buy which is the association's advice

See Anchorage Amateur RadioClub (http://www.kl7aa.net/VEC/vecmain.html) for Alaska - there's a list of Alaska volunteers; http://wireless.fcc.gov/services/index.htm?job=licensing_5&id=amateur shows all the volunteer coordinators

http://en.wikipedia.org/wiki/Packet_radio discusses how to do an internet-type network with ham radio

## Background
http://en.wikipedia.org/wiki/Backhaul_(telecommunications) is backend networking; transferring that to wired from wireless is freeing up spectrum

cellular base stations include http://en.wikipedia.org/wiki/Femtocell

## Interoperability and intercompability
4G Confusion Reigns: Apple in Hot Water Over iPad Labeling Abroad says: "The iPad is compatible with the 700MHz and 2.1GHz LTE bands, which works with North American carriers, as well as HSPA+ bands, which are commonplace internationally and in the states. European countries, however, are largely utilizing the 800MHz and 2.6GHz bands for LTE — where it’s even available" - attributes this to the front-end band which can't be hacked; looked at Introduction to RF Front-End Design by Dabrowski; also glanced at http://www.navipedia.net/index.php/Front_End and http://en.wikipedia.org/wiki/Radio-frequency_identification

http://www.fcc.gov/topic/700-mhz has some background on the 700mhz spectrum (freed up by the DTV transition)

http://www.wirelessweek.com/News/2012/06/clearwires-flavor-of-lte-going-mainstream-by-2016/ is optimistic about TD-LTE (as opposed to FD-LTE)

http://news.cnet.com/8301-1035_3-57422611-94/will-at-t-and-verizon-4g-lte-ever-be-compatible/ Will AT&T and Verizon 4G LTE ever be compatible?

Found Promoting Interoperability in the 700 MHz Band http://www.fcc.gov/rulemaking/12-69 by the FCC on this topic; AT&T's rebuttal points to multi-band chipsets (e.g., Band 17 700MHz, Band 4 AWS, Band 5 PCS)

Qualcomm announced a highly multi-band chipset at http://www.fiercewireless.com/story/qualcomm-work-chip-support-multiple-700-mhz-bands/2012-06-05 however the iPhone has 3 different versions and no 7-band chipset http://www.slashgear.com/the-4g-migraine-iphone-5-highlights-scrappy-lte-13247451/

Qualcomm: Multi-Band Chips Will Take LTE Global makes it sound like Apple intentionally doesn't allow its chips to switch bands

Qualcomm: main competitor is Broadcom; also Nokia and TI possibly; Intel seems to be getting into it http://hexus.net/tech/news/cpu/45221-10-years-making-intel-cto-showcases-digital-radio-chip/

Nvidia and Intel have LTE chips; Broadcom announced their first LTE chip in 2013 - way behind! http://www.trefis.com/stock/brcm/articles/168514/broadcom-achieves-lte-compatibility-but-qualcomm-hardly-threatened/2013-02-13

## FCC
2019-08: responded to Sonic and dug into FCC website; WC prolly stands for Wireline Competition Bureau

### old
Obviously the best resource; http://www.fcc.gov/

http://reboot.fcc.gov/spectrumdashboard/searchSpectrum.seam is awesome

http://www.fcc.gov/wireless-telecommunications-bureau is the place I'm most interested in