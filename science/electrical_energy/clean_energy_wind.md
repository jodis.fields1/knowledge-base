# Clean energy - wind
## Wind
Optiwind is one company but Tangarie Alternative Power sucks and defaulted in 2012 per Plant manufacturer defaults on lease (2012)

[Farming the Wind: Wind Powerand Agriculture](http://www.ucsusa.org/clean_energy/smart-energy-solutions/increase-renewables/farming-the-wind-wind-power.html "http://www.ucsusa.org/clean_energy/smart-energy-solutions/increase-renewables/farming-the-wind-wind-power.html") is cool, Iowa gets 25% from wind 

Inspired by  [Dodgy wind? Why "innovative" turbines are often anything but](http://www.gizmag.com/dodgy-wind-turbines/27876/ "http://www.gizmag.com/dodgy-wind-turbines/27876/") by Mike Barnard I reviewed the literature:

> [A review of energystorage technologies for wind powerapplications](http://www.sciencedirect.com/science/article/pii/S1364032112000305 "http://www.sciencedirect.com/science/article/pii/S1364032112000305") (2012) is cited by a lot of articles;    
>  [GE Adds EnergyStorage to Its Brilliant Wind EnergyTurbine](http://theenergycollective.com/hermantrabish/221471/ge-energy-storage-wind-turbine "http://theenergycollective.com/hermantrabish/221471/ge-energy-storage-wind-turbine") puts 50kwh battery inside     
>  VAWTs are a big thing,  [TWN criticizes them](http://twnwindpower.com/2013/01/the-truth-about-vertical-axis-wind-turbines/ "http://twnwindpower.com/2013/01/the-truth-about-vertical-axis-wind-turbines/") and sells its own little turbines   
>  Wind turbine nightmare at remote Alaska refuge may cost millions (2014) - bird-friendly by Tangarie Alternative Power didn't turn out well due to wind damage; note that the Wikipedia page says these were also installed in Alaskan village of Igiugig

> [Innovative wind turbine design triples output](http://www.businessweek.com/investing/green_business/archives/2009/10/innovative_wind.html "http://www.businessweek.com/investing/green_business/archives/2009/10/innovative_wind.html") (2009) is more credulous from Bloomberg

_Materials -_ in reading EPA won't stop with Pebble if we allow it to reinterpret the law (2013) about Pebble, McGroarty says a single industrial wind turbine takes 3 tons of copper which may be an exaggeration.