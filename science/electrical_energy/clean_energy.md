# Clean energy

## sources
GreenTechMedia - by far the best source of news on clean energy
http://www.altenergystocks.com/

## Plan and laws
See Obama's 2013 blueprint announcement http://www.whitehouse.gov/the-press-office/2013/03/15/fact-sheet-president-obama-s-blueprint-clean-and-secure-energy-future;

[Californias energy policies have ripple effects across the West](http://www.hcn.org/issues/46.5/californias-energy-policies-have-ripple-effects-across-the-west) (2014) - makes me slightly more optimistic than I was

## Utilities regulator (Regulatory Commission of Alaskaetc)
read Electricity Regulation in the US: A Guide by RAP

Lessons Learned in State Electric Utility Regulation by Costello and Jones

## Industry

ElectricPower Research Institute - funded by the industry

Bloomberg NewEnergy Finance - good research

_Deregulation_ \- created competitive "merchant power" business

Glanced at The mad, mad world of merchant power, through the histories of NRG and GenOn (2012)

Enron scandal of course

Consumer effects? Utility Deregulation Can Save You Money (2010) says yes (mom blog) versus A look at the impact of energy deregulation on consumers, and its consequent results on the market (2012) from AARP which is more skeptical

Mysterious why say North Carolina never deregulated even tho Study Commission on the Future of Electric Service in North Carolina (2001?) recommended it

Michigan Energy Choice Now is a great example of a citizen movement to deregulate, and it is alive and kicking as of 2014

## Nonprofits

GENI - Global EnergyNetwork Institute

## Articles

[Florida Blasts Away Old Power Plant to Make Way for New](http://news.nationalgeographic.com/news/energy/2013/07/130716-florida-power-plant-demolition/ "http://news.nationalgeographic.com/news/energy/2013/07/130716-florida-power-plant-demolition/") - NatGeo decom article

See warranties section; First Solar cadmium telluride technology had lifespan concerns but these seem diminished per The Real Lifespan of Solar Panels (2013) which cites Photovoltaic Degradation Rates — An Analytical Review NRE

 [Solar Power Fight Raging in GOP | New Republic](http://www.newrepublic.com/article/115582/solar-power-fight-raging-gop)

Alion Energy- robots cleaning the panels

Solar Glass - dark

http://www.earthtechling.com/2012/07/readyset- solar-kit-poised-to-take-u-s-by-storm/ - ReadySet kit looks cool

2014-02: went on a binge on utility-scale after Ivanpah (by BrightSource, on California border with Nevada), a solar power tower (see helioscp.com for special news) rather than parabolic trough, went active; controversy over birds (tortoises were moved) canceled Palen project (hopefully not permanently per  [pending bird data](http://www.helioscsp.com/noticia.php?id_not=2212 "http://www.helioscsp.com/noticia.php?id_not=2212") and see  [ESolar Has Answers to Questions About the BrightSource Solar Power Tower](https://www.greentechmedia.com/articles/read/esolar-has-answers-to-questions-about-the-brightsource-solar-power-tower "https://www.greentechmedia.com/articles/read/esolar-has-answers-to-questions-about-the-brightsource-solar-power-tower") for best response to these controversies); no molten solar but Abengoa's Solana nearby has molten salt (first in nation, SolarReserve Crossroads will also have it); uses limited water due to "dry-cooling technology"; 

[First Solars New Mexico Project: The Parity and the Pain](http://www.altenergystocks.com/archives/2013/02/first_solars_new_mexico_project_the_parity_and_the_pain.html "http://www.altenergystocks.com/archives/2013/02/first_solars_new_mexico_project_the_parity_and_the_pain.html") - oddly low pricing 

**Biofuel**
See stock industry notes, but Lux Research reviews 156 companies and 29 algae ones in Feb 2013 report at Amyris, Poet, Gevo Leaders in Alternative Fuels, Report Says; doesn't look that great for these guys

**Coal**
Carbon storage - turn CO2 into bricks? (2013)

## Hydro
Note rivers in environment note and gov history on the laws recently passed

Most run-of-river B.C. hydro projects can harm fish (2013) - study shows 43/44 impact salmon funded by Moore

## Storage
See battery discussion in auto note

### History
Aquion declared bankruptcy; Envia crashed and burned

titanium dioxide for a supercapicitor per  [Common chemical offers energystorage hope](http://www.abc.net.au/science/articles/2013/07/01/3792075.htm) (2013);

## Tax incentives
Obama targeting abolishment of oil&gas tax incentives which are summarized in  [CRS R42374 ](https://opencrs.com/document/R42374/ "https://opencrs.com/document/R42374/")but  [appears that MLPs are not on the table](http://www.naptp.org/News/LawRegulationDevelopments/ObamaBudget.htm "http://www.naptp.org/News/LawRegulationDevelopments/ObamaBudget.htm"), however at least there's a parity act S. 795 which Begich is not cosponsoring (!)

## Warranties & risk management
PowerGuard Specialty Insurance Services is supposedly the main company backing these warranties for customers' benefit, since they can bankrupt the solar producer. See Solar Insurers Turn Kingmakers Over Panel Survival Doubts (2012) 

## Breakthroughs
https://planetsave.com/2012/05/24/solar-power-breakthrough-new-inexpensive-environmentally-friendly-solar-cell/ 2012 Gratzel cell breakthrough

http://www.eurekalert.org/pub_releases/2012-07/uotf-bbu072712.php \- colloidal quantum dot solarcells

See User talk:J.M.Pearce - Appropedia: The sustainability wiki