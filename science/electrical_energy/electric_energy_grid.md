Energy and grid
===
[Duke Energy's monopoly power faces challenges in Southern states](https://www.facingsouth.org/2019/03/duke-energys-monopoly-power-faces-challenges-southern-states)

See stock market notes 

[How is electricity used in U.S. homes?](https://www.eia.gov/tools/faqs/faq.php?id=96&t=3) - space cooling is a big part
* per [table 7.6 of Monthly Energy Review](https://www.eia.gov/totalenergy/data/browser/index.php?tbl=T07.06#/?f=M&start=200001) residential & commercial are evenly split; this also applies to total consumption per table 2.1
* https://www.eia.gov/energyexplained/us-energy-facts/

## Companies
this duplicates notes in my "insurance data" spreadsheet

Detailed analysis by Roger S. Conrad at  [FirstEnergy’s Cut Unlikely To Repeat](https://conradsutilityinvestor.com/firstenergys-cut-unlikely-to-repeat/ "https://conradsutilityinvestor.com/firstenergys-cut-unlikely-to-repeat/") (2014)  

Exelon - combination of wholesale and regulated

_Wholesale merchant power_

Calpine Corp & NRG - came out of banktruptcy

Dynegy - casualty of the wholesale market

_Regulated_
Duke - positioned in the regulated only (see  [Whats Powering Duke Energys Earnings Growth?](http://www.trefis.com/stock/duk/articles/235262/whats-powering-duke-energys-earnings-growth/2014-04-25?from=email%3Anotd) from 2014)

**Consumption**
DOE  [Clean Energy in My State](http://apps1.eere.energy.gov/states/) is kinda cool   

## History and analysis
[Ambitious Clean Line Energy 'wrapping up'](https://www.windpowermonthly.com/article/1523646/ambitious-clean-line-energy-wrapping-up)

[A Smarter Power Grid for U.S. Utilities](http://www.businessweek.com/articles/2014-07-10/u-dot-s-dot-utilities-start-to-invest-in-smarter-power-grid) (2014) - good overview of maintenance

Richard J. Pierce Jr. - one of the major people behind the "restructuring", see  [Realizing the Promise of Restructuring the Electricity Market](http://papers.ssrn.com/sol3/papers.cfm?abstract_id=585706) (2005) where he talks about the decline/lack of transmission capacity, also see followup in  [The Past, Present, and Future of Energy Regulation](http://epubs.utah.edu/index.php/jlrel/article/viewFile/516/381) (2011) as well as someone following him with  [Preempting Parochialism and Protectionism in Power](http://works.bepress.com/sandeep_vaheesan/6/) (2012)

_Consumer impact_
Utilities like Duke don't like competition, which suggests that it is good for consumers. But in 2007 USA Today published  [Electric deregulation fails to live up to promises as bills soar](http://usatoday30.usatoday.com/money/industries/energy/2007-04-21-electricity_N.htm) so this is debatable

## Deregulation
[The Only Correct Deregulated States Map](http://energytariffexperts.com/blog/2013/5/29/the-only-correct-deregulated-states-map) (2013) - more detailed than most and way better than EIA's 2010 map