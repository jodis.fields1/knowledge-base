bibsonomy !!

*Note* - the rise of SciHub and LibGen somewhat diminishes this in priority - problem is being fixed by piracy and market forces

https://www.readcube.com/home

## 2020
Scholastic - seems to the be leader, not open-source

## 2018 tools
2018-10: Open Journal Systems (OJS) - perused their sprawling PHP monolith https://github.com/pkp/ojs (powered by https://github.com/pkp/pkp-lib) - not my cup of tea. alternatives?
* PubPub - https://github.com/pubpub/pubpub looks good, react and nodejs, but early and may lose momentum
* Flockademic - https://gitlab.com/Flockademic/Flockademic is TypeScript + React and also good, avoid self-hosting per https://medium.com/flockademic/i-want-to-create-alternatives-to-traditional-publishers-what-platform-do-i-use-ce1c275b05d but PKP does that for OJS
  * https://medium.com/flockademic/why-open-source-and-open-access-go-great-together-75e31b630efc
* Janeway - 
* Ambra - powers PLoS

Chrome Google Scholar button https://chrome.google.com/webstore/detail/google-scholar-button/ldipcbpaocekfooobnbcddclnhejkcpn
Zotero!

## General
[From bibliometrics to altmetrics: A changing scholarly landscape](http://crln.acrl.org/content/73/10/596.full)

http://www.fsd.it/fonts/pragmatapro.htm

Inspired by LibraryThing, which I was had a wonderful UI        for looking at my review, comment, and private comment on        books! Can't find that in this space.        
Went through the web-based ones on        http://en.wikipedia.org/wiki/Template:Reference_management_software

Link archiving
archive.is - one-man show, not open-source?
perma.cc - cool, HR acquaintance works there   
webcitation - seems dead
[Show HN: Tesoro – Personal internet archive](https://news.ycombinator.com/item?id=14644441)

See http://www.archiveteam.org/index.php?title=Main_Page for a loose coalition

Internet Archive respects robots.txt but it announced that it stopped!