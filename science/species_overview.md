# Species overview

See environment general for some info as well

  


**Overview**

In 2005 there was a major reclassification of eukaryotes which created Chromalveolata which is mainly autotroph algae (previously split among protists and plants)

  


_Chordata_  ( [ wiki](https://en.wikipedia.org/wiki/Chordata#Classification "https://en.wikipedia.org/wiki/Chordata#Classification"), classification section has # of species)

Tetrapods are the common ancestor and superclass

  


_Fish_

Went through fish, which of course include Animalia->Chordata->Osteichtheyes where it branches into Acinopterygii (99% of fish and dominant vertebrates) and Sarcopterygii (more rare with fleshy lobes as well as fins)

  


_Algae_

Brown algae are supposedly most common in ocean, with yellow-green a relative

Red algae supposedly live deep in the ocean (?), role in coral reefs, produce agar, 

Green algae more common near land and on surface

  


Phytoplankton - always autotrophs (energy from sun), include diatoms, cyanobacteria, and dinoflagellates

  


Diatoms - mostly unicellular algae, not sure which color, include autotrophs and heterotrophs, produce much of the oxygen

Cyanobacteria are prokaryotic but similar and use photosynthesis

  


**International trade** \- forked off from international investing

Note the dolphin war which was ongoing as of 2014

CITES (Convention on International Trade in Endangered Species of Wild Fauna and Flora) is relevant

  


**Species conservation**

I recall that the riverdolphin in China went extinct http://en.wikipedia.org/wiki/Baiji and now it looks like the http://en.wikipedia.org/wiki/Finless_porpoise is in trouble - note that these both live in the Yangtze which had the Three Gorges dam

Skimmed through http://en.wikipedia.org/wiki/List_of_endangered_and_protected_species_of_China

See Association of Zoos and Aquariums (Aza) which has a http://en.wikipedia.org/wiki/Species_Survival_Plan; however the wiki article for zoos indicates they probably don't add much to conservation; accreditation of 2,400 zoos or so is probably 10%

Visited Chicago's Lincoln Park Zoo (Brookfield is much more impressive) and the other free Saint Louis Zoo

  


_Gene bank_  - http://en.wikipedia.org/wiki/Gene_bank

Banco de Germoplasma y Tejidos de Especies Amenazadas - Spanish gene bank found through   [The role of a germplasm and tissue bank in the conservation of endangered species](http://www.fgcsic.es/lychnos/en_EN/articles/germplasm_and_tissue_bank_in_the_conservation "http://www.fgcsic.es/lychnos/en_EN/articles/germplasm_and_tissue_bank_in_the_conservation"); as a I recall there was a Brookings Institution guy calling for gene banking

  


**People**

Carl Woese ( [wiki](https://en.wikipedia.org/wiki/Carl_Woese "https://en.wikipedia.org/wiki/Carl_Woese")) - famously split out Archaea; rejected as a "crank" early on

Thomas Cavalier-Smith ( [ wiki](https://en.wikipedia.org/wiki/Thomas_Cavalier-Smith "https://en.wikipedia.org/wiki/Thomas_Cavalier-Smith")) - majorly responsible for modern revisions 

  


**Endangered**

As I discovered in reading Awakening the Slumbering Giant: How Horizontal Drilling Technology Brought the Endangered Species Act to Bear on Hydraulic Fracturing (2013), the landmark case Tennessee Valley Authority v. Hill has a big influence on this
