Science general
===

https://www.explainthatstuff.com

http://www.thenakedscientists.com/ looks really cool

## Resources and websites

http://www.vectorsite.net/ -  by greg goebel - oddly detailed!

## Laws
Per  [Science and Technology Issues in the 113 the Congress](http://www.fas.org/sgp/crs/misc/R43114.pdf) in CRS 7-5700 total funding is about $150b; 2010 America COMPETES Act did the "doubling path" of basic science at NSF and DOE; intellectual property acts affect this: R&D are tax deductible under Section 174 of tax code, tax credit under Section 41 (expiring 2013), capital gains exclusion in Section 1202; open access; 105-252 STEM education at 13-15 agencies with $3b; agricultural research at $2.5b with $70m for organic and specialty ag and considering New Animal Drug Application (NADA) regulation; NIH is $31b with 80% in extramural supporting 300,000 scientists, down 16% from 2003 peak; defense research is $68b with about 20% towards basic research rather than new and improved systems; NASA budget is undisclosed; nanotechnology started with the National Nanotechnology Initiative in 2001 with $16b to nanotech R&D with $1.7b in 2013; Climate change science shows clean energy at $8b with $2.7b for Global Change Research as a coordinator, note that 2013 will see 2 new reports; water science estimated at $700m; $6b for carbon capture; 

## Education
The Science of Cleaning Products by Steve Spangler turned me onto all sorts of science experiments

Project Lead the Way http://www.pltw.org/ is cool

"The Saturday Thing for kids" in Juneau - also on third Thursdays for adults http://stemak.org/saturday-thing

## Notable publications / books
http://www.plosmedicine.org/article/info:doi/10.1371/journal.pmed.0020124 \- Why Most Published Research Findings Are False

jrank - great source at http://science.jrank.org/

Robert Hazen and James Trefis are the best intro to science authors out there; also I really liked The People's History of Science

The Ten Things All Future Mathematicians and Scientists Must Know (But are Rarely Taught) - potentially good

A Short History of Nearly Everything by Byson - famous pop-science book

Connections by James Burke - sort of a history of inventions and surprising connections

The Canon: A Whirligig Tour of the Beautiful Basics of Science - too many flowery words according to reviewers

## Invention and origin
Edison was particularly prominent; read up http://en.wikipedia.org/wiki/Thomas_Edison e.g. phonograph; amusingly, Edison also called for fractional-interest banking reform

Directory of National Historic Chemical Landmarks from http://portal.acs.org/portal/acs/corg/content?_nfpb=true&_pageLabel=PP_SUPERARTICLE&node_id=468 is a great overview

## Making plus shops and laboratories

In terms of inventions, see http://en.wikipedia.org/wiki/List_of_prolific_inventors

[Real garage science - Man builds scanning electron microscope from scratch](http://www.reddit.com/r/science/comments/g8igo/real_garage_science_man_builds_scanning_electron/) - amazing example of what you can do

http://en.wikipedia.org/wiki/TechShop is an example of a http://en.wikipedia.org/wiki/Hackerspace

Read about a subscription shop sponsored by Make magazine somewhere but now can't find it

Instructables.com is a good website

http://www.nanotech-now.com/columns/?article=182 discusses nanotech labs for rent (not really my thing)

## Equipment
Parts have gotten cheaper; Amazon purchased smallparts.com

https://www.inventables.com/ is another vendor

http://www.makershed.com/ has lots of stuff

MicrOptix i-LAB Hand Held Visible Analyzing Spectrophotometer, with Samplette at  [amzn.com/B00440CZ00](http://amzn.com/B00440CZ00 "http://amzn.com/B00440CZ00") is cool but expensive at $2,500!

UCLA researchers' smartphone 'microscope' can detect a single virus, nanoparticles (2013) - created by Aydogan Ozcan

Curious about controlling atmosphere to test food degradation or remoisturize marijuana; see http://www.coylab.com/ for neat products and http://www.labconco.com/category/controlled-atmosphere-glove-boxes looks more hardcore; http://www.clubsnap.com/forums/newbies-corner/680764-cheap-dry-cabinet.html \- these guys are hobbyists who know dry boxes but don't have too many options

http://en.wikipedia.org/wiki/Van_de_Graaff_generator - recommend to understand voltage per http://amasci.com/miscon/voltage.html

## Life sciences and biology
Get petri dishes at http://www.hometrainingtools.com/microscopic-agar-petri-dishes-science-teaching-tip/a/1060/ either disposable or glass, but the glass needs to be autoclaved prolly; the reason for that is explained at http://scienceforums.com/topic/17725-q-do-microwaves-kill-bacteria-all-how-long-is-needed-to-kill/ "many pathogens can survive boiling water at typical atmospheric pressure (expecially if you’re well above sea level, where the boiling point of water is well under 100 C), for a long time, so the rule-of-thumb is that the boiling water must excede 120 C for 15 minutes for effective sterilization. To get water this hot, the container in which it’s boiled must be pressurized to about 1 atmosphere (100 kPa or 15 PSI)."

Is that bacteria dead yet? - matchbox-sized tool for testing per Dietler

## Crowdfunding
Gifted in Science(http://the-scientist.com/2012/09/01/gifted-in-science/) discusses the different companies available, e.g. petridish.org or fundageek.com

## Paradigm shift and philosophy of science
I have a notepad somewhere listing all these things; http://eprints.ucl.ac.uk/464/1/1.pdf Hempelian and Kuhnian Approaches in the Philosophy of Medicine: the Semmelweis Case discusses medicine

Declining effect sizes: [The Truth Wears Off](http://www.newyorker.com/reporting/2010/12/13/101213fa_fact_lehrer) in New Yorker is intriguing even if Lehrer has some problems

## Education
Downloaded all the lesson plans off http://chemreview.com/ before it becomes charged; noticed that there are books such as Chemistry: Concepts and Problems: A Self-Teaching Guide on Amazon and even a guide to teaching chemistry through "multiple intelligence" at sing-smart.com/guide.html 336-page pdf Teaching Manual - Teach Chemistry Amazingly Well ...

Cheat sheet at http://www.premed411.com/pdfs/CHM1.pdf

http://library.thinkquest.org/C004970/ - great explanations

http://www.chem1.com/acad/webtext/virtualtextbook.html - virtual textbook

## Physics
note electricity was moved to a new section

http://www.staff.science.uu.nl/~hooft101/theorist.html#pmathematics

http://physics.info/ is good

## Fringe/frontier science

Trimmed Laboratory Life: The Construction of Scientific Facts out of my Amazon watchlist

Always been fascinated by spectacular claims; found in medicine that these are often quite promising

Notes on the Nature of Fringe Science (1982) - http://eric.ed.gov/ERICWebPortal/search/detailmini.jsp?_nfpb=true&_&ERICExtSearch_SearchValue_0=EJ260409&ERICExtSearch_SearchType_0=no&accno=EJ260409

Medical science - see all the notes in the Health-Medicine tag

Mapping Mainstream and Fringe Medicine on the Internet - http://scx.sagepub.com/content/22/3/292.abstract

Note there is a medical journal on medicine and sociology - can't remember the name but really focuses on this

Note Sociology of Scientific Knowledge really focuses on this

Wilk v. AMA http://www.answers.com/topic/wilk-v-american-medical-association -  AMA lost against chiropractors in antitrust