Chemistry
===
See plastics under environment

compounds composed of carbon and hydrogen are divided into two classes: aromatic compounds, which contain benzene or similarly aromatic rings of atoms, and aliphatic compounds

## Tutoring self

2014-10: re-reviewed this as I contemplated tossing chemistry textbooks, read up on cathode/anode which are positive/negative depending upon whether it's a galvanic or electrolytic cell (confusing!), found  [How do the stored chemicals in batteries drive electronic current from their terminal to outside devices?](http://chemistry.stackexchange.com/questions/5491/how-do-the-stored-chemicals-in-batteries-drive-electronic-current-from-their-ter/5492#5492) at SE which directed me to Jim Clark

2014-08: see  [Electronic Configurations](http://chemwiki.ucdavis.edu/Inorganic_Chemistry/Electronic_Configurations) for best explanation of orbitals and shells including spdf notation; found from  [Covalent Bonds](http://chemwiki.ucdavis.edu/Theoretical_Chemistry/Chemical_Bonding/General_Principles/Covalent_Bonds) which has a reminder about the octet rule. Ionic bond example: potassium bonds with halogens (F, Cl, etc) by losing one electron which gets them into the octet position (where noble gases are).

[Chemguide](http://www.chemguide.co.uk/index.html) - Jim Clark page which seems quite excellent! Check out Physical Chemistry->redox equilibria for necessary insight into batteries

[ Chem1 virtual textbook](http://www.chem1.com/acad/webtext/virtualtextbook.html) - by Stephen Lower, who also has a great debunking of alkalized/ionized water

[ChemistryUnderstood.com](http://www.chemistryunderstood.com) - seems quite good

## Apps

[Basic Chemistry](https://play.google.com/store/apps/details?id=com.zayan.chemistry.app&hl=en) - haven't tested

[Chem Pro](https://play.google.com/store/apps/details?id=com.ihelpnyc.chempro&hl=en) - the most detailed I think 

[Learn Chemistry](https://play.google.com/store/apps/details?id=com.andromo.dev130378.app231548&hl=en) - collection of mnenomics 

[The Golden Rules of Organic Chemistry](http://iverson.cm.utexas.edu/courses/310N/MainPagesSp06/GoldenRules.html) - tossed in dropbox (Documents->Articles) as a backup

## Bonding

Covalent: keep in mind the valence (outer) shell; for example, carbon has 2 electrons in its inner shell and 4 in its outer, so it can bond to 4 hydrogen, therefore CH4 is methane!

## Molarity and moles
[How Was Avogadro’s Number Determined?](https://www.scientificamerican.com/article/how-was-avogadros-number/)

Confusing - see  [What is Avogadro’s number?](http://science.howstuffworks.com/avogadros-number1.htm) for a reasonable good explanation, also read  [Some Notes on Avogadros Number, 6.023 x 1023](http://iweb.tntech.edu/chem281-tf/avogadro.htm)

## pH

[The pH Scale](http://chemwiki.ucdavis.edu/Physical_Chemistry/Acids_and_Bases/Aqueous_Solutions/The_pH_Scale "http://chemwiki.ucdavis.edu/Physical_Chemistry/Acids_and_Bases/Aqueous_Solutions/The_pH_Scale") at chemwiki - not sure if this applies only to aqueous solutions (?)

## General resources

[Chemogenesis web book](http://www.meta-synthesis.com/webbook.html) - 

[Webelements.com](http://www.webelements.com/) - stuff

Periodic table project - found this by  [reactivity and electronegativity page](http://ericaandchristinagroup13period7.weebly.com/reactivity-and-electronegativity.html)

## Reactivity and electronegativity

[What is the relationship between electronegativity and the reactivity of nonmetals?](http://answers.yahoo.com/question/index?qid=20111210145613AA7eC66) - I really like this statement that "more electronegative a non-metal is, the more reactive it is. Non-metals (i.e. group 6 & 7) want to gain 1 or 2 electrons to gain a noble gas configuration" and "metal (e.g. alkali and alkali earth) it is almost opposite. These metals want lose 1 or 2 electrons to gain a noble gas configuration"

[How does reactivity vary as you move across the table?](http://learning.covcollege.ac.uk/content/Jorum/CHB_Electronic-config-and_periodicity_LM-1.2/page61.htm) - good summary which notes two highly reactive substances: Caesium has an electronegativity of 0.79 and Chlorine on the other hand has an electronegativity of 3.16; phosphorus with an electronegativity of 2.19 is an extremely reactive element

## Relative humidity
Always been interested in learning chemistry but it's so hard to get around to it. Looked at http://en.wikipedia.org/wiki/Relative_humidity one day (2012-12-15) which led me to partial pressure, ideal gases, vapor pressure, condensation, and finally to http://en.wikipedia.org/wiki/Triple_point - what a ride!

## History

[Crucibles The Story Of Chemistry](https://archive.org/details/cruciblesthestor002077mbp) (1957) - free online!

[Elements of Chemistry](https://ebooks.adelaide.edu.au/l/lavoisier/antoine_laurent/elements/contents.html) by Antoine Lavoisier - available online!

[Modern Inorganic Chemistry](http://books.google.com/books?id=1iQ7AQAAMAAJ) (1917) - 900+ page book available free; I liked p9 which discussed Lavoisier's discovery of mercury oxide thru heating air and testing calcination

Caveman Chemistry - decent website and book, found thru  [ potash page](http://cavemanchemistry.com/oldcave/projects/potash/)

Boyle ( [wiki](https://en.wikipedia.org/wiki/Robert_Boyle)) was a major founder as was Lavoisier

## General chemistry

_Polarity_ : see Dipole moments at UCDavis chemwiki which notes that as a result of the negative versus positive ends "water are polar molecules, the interaction between water molecules are so strong that it takes a lot of energy to break the bond between the water molecules. Therefore, the boiling point of polar substances are higher than those of nonpolar substance due to stronger intermolecular force among polar molecules"

Two ways to measure polarity: dielectric constant/permitivity and measuring dielectric constant  [per masterochem website](http://www.masterorganicchemistry.com/2012/04/27/polar-protic-polar-aprotic-nonpolar-all-about-solvents/")

Polarity also gets into the solvent ( [wiki](https://en.wikipedia.org/wiki/Solvent)) discussion, as noted at packaging acetone and alcohol both dissolve polar and nonpolar substances, see  [Why is methanol generally used as a first solvent for extraction purpose to look for bioactives in medicinal plants?](https://www.researchgate.net/post/Why_is_methanol_generally_used_as_a_first_solvent_for_extraction_purpose_to_look_for_bioactives_in_medicinal_plants) (2014) for a related discussion

## Mineral oils

Technically ochem, but somewhat different. Focused around alkanes and alkenes.

[ HowStuffWorks How Gasoline Works, Where does gasoline come from?](http://science.howstuffworks.com/gasoline2.htm "http://science.howstuffworks.com/gasoline2.htm") is clearest explanation I could find on gasoline, which is apparently not really all that standardized apart from octane ratings and maximums of sulfur, benzene etc. 

**Organic chemistry** \- key points, note it is not exactly biochemistry

[Organic chemistry (Google eBook)](http://books.google.com/books?id=e0RRDC4Qg1ICa "http://books.google.com/books?id=e0RRDC4Qg1ICa") (1917) - wow this is old!

Also read up on difference between oil and lipid (basically oil is defined by its lack of miscibility with water due to its nonpolarity and includes petroleum products!)

2014-08: inspired by soap, particularly sodium stearate ( [wiki](https://en.wikipedia.org/wiki/Sodium_stearate "https://en.wikipedia.org/wiki/Sodium_stearate")), and saponification ( [wiki](https://en.wikipedia.org/wiki/Saponification "https://en.wikipedia.org/wiki/Saponification")), I read up again on the constituents of soap (fatty acids), which led me down the road to glycerol (and from there alcohol), carbonyl groups ( [wiki](https://en.wikipedia.org/wiki/Carbonyl "https://en.wikipedia.org/wiki/Carbonyl")) include aldehyde, ketones, and majorly important carboxyl (which is part of amino acids and fatty acids!), starting to get this COOH down!; also went again to bases ( [ wiki](https://en.wikipedia.org/wiki/Base_%28chemistry%29 "https://en.wikipedia.org/wiki/Base_%28chemistry%29")), however this time after reading the wiki article it made more sense to me, note that bases will even saponify and potentially emulsify the lipids on the skin! note that the major alkalis ( [wiki](https://en.wikipedia.org/wiki/Alkali "https://en.wikipedia.org/wiki/Alkali")) are the alkali earth metals - potassium and sodium

2014-07: read up on receptors in biochemistry ( [wiki](https://en.wikipedia.org/wiki/Receptor_%28biochemistry%29 "https://en.wikipedia.org/wiki/Receptor_%28biochemistry%29")) which reminded me of ligands (which fit into receptors), which is different from the ligand in inorganic chemistry

2014-07: re-read up on this with alkylation  [ wiki](https://en.wikipedia.org/wiki/Alkylation "https://en.wikipedia.org/wiki/Alkylation") ("the alkyl group is probably the most common group encountered in organic molecules", methylation is most common),  ochem wiki article, functional groups, etc; R group is called a side chain and it is tied with peptide bond to amino group and carboxyl group; also read biomolecule ( [wiki](https://en.wikipedia.org/wiki/Biomolecule "https://en.wikipedia.org/wiki/Biomolecule")) and small molecule ( [wiki](https://en.wikipedia.org/wiki/Small_molecule "https://en.wikipedia.org/wiki/Small_molecule")) which was eye-opening; protein, with estimates for number of proteins (see proteomics wiki) range from 50,000 (broad groups) to 2 million; and List of human hormones ( [ wiki](https://en.wikipedia.org/wiki/List_of_human_hormones)) and neurotransmitters ( [wiki](https://en.wikipedia.org/wiki/Neurotransmitter)) versus hormones, which led me down the path of glutamate (responsible for 90%?), glutamate receptor NMDA (N-methyl-D-aspartate receptor), and excitatory amino-acid transporters

compounds composed of carbon and hydrogen are divided into two classes: aromatic compounds, which contain benzene or similarly aromatic rings of atoms, and aliphatic compounds which means fat or oil; also there are alicyclic which contain non-aromatic rings; aliphatic includes alkanes, alkenes (gases notably ethylene and propene which is not propane), and alkynes which are often petrochemicals; all hydrocarbons are nonpolar, benzene are slightly more polar than hexane per p.670 of A Microscale Approach to Organic Laboratory Techniques

Drug metabolism: http://en.wikipedia.org/wiki/Drug_metabolism#Oxidation lists all 6 major metabolizing ways; notably http://en.wikipedia.org/wiki/Drug_metabolism#Permeability_barriers_and_detoxification says that cell membranes are hydrophobic and don't allow polar substances across; xenobiotics are often nonpolar which would allow them to cross, but in Phase I enzymes with low specificity turn them polar; Phase II discusses glutathione which is rate-limited by cysteine typically (remember NAC); ratio of reduced glutathione to oxidized glutathione within cells is often used as a measure of cellular toxicity;

## Gold

[The Chemistry of Gold](http://www.researchgate.net/publication/228039844_The_Chemistry_of_Gold/file/60b7d51992dc7c86db.pdf) - wow too much information!

More reactive metals actually are better electrical conductors  [per this](http://www.madsci.org/posts/archives/1999-03/920956179.Ch.r.html)