Plastic
===

TODO: document BPA alternatives on plastics and endocrine disruptor page

See also reform note, toxin (EPA focus) note, and probably others

BPA-free != estrogenic activity (EA) free 

## biodegradation
https://pubs.acs.org/doi/pdf/10.1021/acs.est.7b04051 | Biodegradability of Plastics: Challenges and Misconceptions
https://oceans.taraexpeditions.org/en/m/science/news/bacterial-degradation-of-synthetic-plastics/ | Bacterial degradation of synthetic plastics | Explore to understand, share to bring about change
https://pubs.acs.org/doi/full/10.1021/tx500444e | Survey of Human Oxidoreductases and Cytochrome P450 Enzymes Involved in the Metabolism of Xenobiotic and Natural Chemicals | Chemical Research in Toxicology
https://scholar.google.com/scholar?hl=en&um=1&ie=UTF-8&lr&cites=10281516979594384203 | Muhonja: Biodegradability of polyethylene by bacteria... - Google Scholar
https://www.ncbi.nlm.nih.gov/pmc/articles/PMC6132502/ | Degradation of plastics and plastic-degrading bacteria in cold marine habitats
https://scholar.google.com/scholar?cites=5146424114092908038&as_sdt=2005&sciodt=0,5&hl=en | Urbanek: Degradation of plastics and plastic-degrading... - Google Scholar

## Plastics overview

Cyclic olefin copolymer (COC) - materials in PlastiPure products certified as estrogenic activity (EA) free; by Topas per http://www.pmpnews.com/news/plastipure-approves-safety-topas-coc-materials heat distortion temp of 130 degrees Celsius per http://www.foodproductiondaily.com/Packaging/New-resin-know-how-boosts-COC-material-performance-company also a technical article at www.polyplastics.com/en/product/lines/topas/TOPAS.pdf

BPA - supposedly safe stuff is not so safe per http://www.jsonline.com/watchdog/watchdogreports/34532034.html BPA leaches from 'safe' products

Polyethylene terephthalate (PETE) - can leach antimony so it seems bad; also PET or PET-P; PETG is a modified PET plus glycol-modified

Polyester - includes Tritan

Tritan - Eastman Chemicals; overview at http://www.eastman.com/Brands/Eastman_Tritan/Pages/Overview.aspx

http://www.plasticstoday.com/articles/new-tritan-copolyester-solves-crazing-problem

dishwasher-safe; most are PC or SAN (styrene acrylonitrile) which is brittle and breaks down slowly

Acrylonitrile butadiene styrene (ABS) - http://en.wikipedia.org/wiki/Acrylonitrile_butadiene_styrene manufactured by Keller Products

## Safety and toxicity
PlastiPure: partnered with Reliabrand (maybe) and Topas per http://www.pmpnews.com/news/plastipure-approves-safety-topas-coc-materials details on method at http://www.plasticstoday.com/articles/estrogenic-activity-test disclose breast cancer cell line isolated in the 1970s MCF-7; EA survey of more than 1000 commercial resins and plastic products, including some that advertise themselves as being "EA free, "PlastiPure researchers found that 92% were actually "estrogenic."  Topas - see above; partnered with PlastiPure; JV of Polyplastics (Japanese) and Daicel (another Japanese company) publicly-traded at http://www.reuters.com/finance/stocks/companyProfile?symbol=4202.T

Causes - modifiers or additives could be the issue. Specifically, the researchers looked at antioxidants, impact modifiers (styrenic and elastomer), vulcanizates, olefinic block copolymers, and cyclic olefin copolymer (latter seems to have ruled out per Topas agreement)