Global warming
===

## State-based
[As Oysters Die, Climate Policy Goes on the Stump](http://www.nytimes.com/2014/08/04/us/as-oysters-die-climate-policy-goes-on-stump.html) (2014)

Many states, most notably northeastern states in the Regional Greenhouse Gas

Initiative (RGGI) and western states in the Western Climate Initiative (WCI) either already restrict

GHG emissions from in‐state sources or will soon do so

Removing CO2 or climate engineering or short-lived pollutants

### California
[Berkeley study finds California cap-and-trade program falling short of goals](https://www.greencarcongress.com/2019/05/20190512-capandtrade.html)
* [Researchers Press California to Strengthen Landmark Climate Law](https://www.kqed.org/science/1946804/researchers-press-california-to-strengthen-landmark-climate-law)
* https://climatetrust.org/efforts-to-discredit-offsets-are-often-without-merit-and-worse-yet-if-successful-eliminate-a-valuable-tool-in-the-urgent-fight-against-climate-change/
* https://www.technologyreview.com/s/614216/whoops-californias-carbon-offsets-program-could-extend-the-life-of-coal-mines/
* https://cal.streetsblog.org/2019/05/22/latest-cap-and-trade-auction-shows-strong-results/

[California’s pollution enforcers would like to save tropical forests. But at what cost?](https://calmatters.org/environment/climate-change/2019/07/californias-pollution-enforcers-would-like-to-save-tropical-forests-but-at-what-cost/)

See Stanford's Global Climate and Energy Project (GCEP) by Chris Field and Jennifer Milne http://gcep.stanford.edu/news/goingnegative.html


## R&D investments
the key is nuclear

[Chart: The Global Leaders in R&D Spending, by Country and Company](https://www.visualcapitalist.com/global-leaders-r-d-spending/)

## Periodicals
[Required skimming: climate change](http://www.cjr.org/the_observatory/required_skimming_climate_chan.php) lists:
* InsideClimateNews
* The Daily Climate
* Skeptical Science
* Real Climate
* Climate Central

## Weather
Polar vortex struck U.S. hard in 2013 for very cold weather

## Summaries
[What We Know](http://whatweknow.aaas.org/wp-content/uploads/2014/03/AAAS-What-We-Know.pdf) (2014)

## Impacts
[IPCC dispute simmers over economic costs of climate change](http://www.theage.com.au/environment/climate-change/ipcc-dispute-simmers-over-economic-costs-of-climate-change-20140327-35jho.html) (2014) - Tol, lead author, says chapter 10 is alarmist (?)

### Public opinion
See  [wiki public opinion on climate change](https://en.wikipedia.org/wiki/Public_opinion_on_climate_change)

Yale Project on Climate Change Communication - leader in this area; read Americans’ Knowledge of Climate Change (2010)

Final Report - CLAMER (Climate change and marine ecosystem research results) - EU large opinion poll; 88% support addressing but low awareness of ocean acidification

[Walking the public opinion tightrope](http://www.cjr.org/the_observatory/walking_the_public_opinion_tig.php) (2014) - Breakthrough Institute has a different take than others

## Local advocacy
The Green Tea Party

[Small independent energy producers trying to elbow in on big Alaska utilities](http://www.alaskadispatch.com/article/20130911/small-independent-energy-producers-trying-elbow-big-alaska-utilities) - similar in Alaska

Wind power producers ask regulators for rate certainty - coverage of same situation

No bids in first state timber sale for Tok power (2013) - unfortunate

## Persuasion and strategy
[Changing minds about climate change policy can be done -- sometimes](http://www.eurekalert.org/pub_releases/2013-06/osu-cma062413.php) reports on a 2013 Journal of Communication study by Erik Nisbet

[Study suggests how to win over deniers](http://www.abc.net.au/science/articles/2012/06/18/3525363.htm) (2012) - led by Paul Bain, suggests focusing on positive economics

[Convincing the Climate-Change Skeptic: Nitty-gritty](https://alumni.stanford.edu/get/page/magazine/article/?article_id=43061) - heavy!

[Global temperature 2013](http://www.realclimate.org/index.php/archives/2014/01/global-temperature-2013/) - good overview with fodder for countering the "pause" debate, also comments on Der Spiegl misleading at the end

[Americans care deeply about global warming – but not climate change](http://www.theguardian.com/environment/2014/may/27/americans-climate-change-global-warming-yale-report) (2014) - use the words global warming

## Deniers
[Actual Climate Scientists Who Are Sceptical of AGW](http://denierlist.wordpress.com/326-2/) - actually small, refers to desmosblog

Richard Smith (not to be confused with University of Cambridge economist) from Institute for Policy Research & Development (IPRD) in Capitalism and the destruction of life on Earth (2013) has an extreme view   

Lomborg and Shermer are interesting - don't think it is a problem; emphasizing precautionary principle did not work ( [debating Michael Shermer](http://Debating Michael Shermer \(and Bjorn Lomborg\) on Climate Risks | DeSmogBlog "http://Debating Michael Shermer \(and Bjorn Lomborg\) on Climate Risks | DeSmogBlog"))

## Regulations
EPA Standards for Greenhouse Gas Emissions from Power Plants: Many Questions, Some Answers - good CRS overview

See Docket No. EPA‐HQ‐OAR‐2011‐0660  RIN 2060-AQ91 for the proposed regulation which is getting a lot of press - none of the news articles mentioned the actual name or linked to it, of course - gotta find one I can trust! Based on http://en.wikipedia.org/wiki/New_Source_Performance_Standard; litigation in U.S. Court of Appeals for the District of Columbia; Illinois and Montana, already require carbon capture and storage technology for new coal plants

 Submit written comments per http://epa.gov/carbonpollutionstandard/actions.html

Read more here: http://www.kansascity.com/2012/03/27/3517918/epa-announces-historic-rule-to.html#storylink=cpydiscussed in http://www.kansascity.com/2012/03/27/3517918/epa-announces-historic-rule-to.html

http://thenewamerican.com/tech-mainmenu-30/environment/8700-epa-regulations-to-shut-down-coal-plants-and-raise-energy-prices describes a various anti-coal regulations which were being floated around August 2011

## Nonprofits and orgs
see nonprofits for some lists of these

EPA watchdog group is the http://www.environmentalintegrity.org/ - Eric Schaeffer, an EPA official monitoring the Clean Air Act under the Bush Administration and now the head of EPA watchdog group - however, no financials on the website

Climate Counts - my favorite reviews businesses

http://en.wikipedia.org/wiki/Environmental_Investment_Organisation - like Climate Counts but based in UK

## Science
See http://www.bloomberg.com/news/2013-02-14/the-most-influential-climate-study-few-people-know-about.html which discusses Nature article Greenhouse-Gas Emission Targets for Limiting Global Warming to 2C by Meinhausen

Indonesian climate shift linked to glacial cycle (2014)

Watched a great documentary on hulu plus about sea water rise

https://en.wikipedia.org/wiki/Great_Oxygenation_Event - found thru cyanobacteria, happened about 2.4b years ago

## Sea ice debate
there was a dead cat bounce around 2012 or 2013 but its way down; hilariously out of context; see  [Arctic sea ice delusions strike the Mail on Sunday and Telegraph](http://www.theguardian.com/environment/climate-consensus-97-per-cent/2013/sep/09/climate-change-arctic-sea-ice-delusions) (2013)

## Ocean acidification
Summer 2012 Special Report: Public Awareness of Ocean Acidification - very little knowledge! by The Ocean Project

Read http://www.hcn.org/issues/44.21/can-the-oyster-industry-survive- ocean- acidification/article_view?b_start:int=1 and found it eye-opening, particularly the comments by ilma Sixthirty about the alkalinity of the ocean

[Ocean Acidification: Chicken Little of the Sea Strikes Again](http://wattsupwiththat.com/2011/01/10/ocean-acidification-chicken-of-the-sea-little-strikes-again/ "http://wattsupwiththat.com/2011/01/10/ocean-acidification-chicken-of-the-sea-little-strikes-again/") (2011) - comments by From Peru rebutting were enlightening esp about Ries et al 2009; evolution of corals in acid appears bullshit and saw IPSO report 2013 says oceans more acidic than anytime in 300 million years

Read  [CRS Report R40143 on Ocean Acidification](http://www.fas.org/sgp/crs/misc/R40143.pdf) and noticed it mentioned pteropods, in researching I noticed that the  [sea angel](http://en.wikipedia.org/wiki/Sea_angel) (carnivous preying on  [sea butterflies](http://en.wikipedia.org/wiki/Sea_butterfly)) article discusses survival threats citing IPCC 2007 which I found  [specifically in Ch. 15.2](http://www.ipcc.ch/publications_and_data/ar4/wg2/en/ch15s15-2.html) and these holoplankton are major foods for certain zooplankton (tho not the autotrophic phytoplankton, which are  [in decline](http://en.wikipedia.org/wiki/Phytoplankton#Environmental_controversy)) and fish; newer update is  [Animals are already dissolving in Southern Ocean](http://www.newscientist.com/article/dn22531-animals-are-already-dissolving-in-southern-ocean.html#.Ufq3R6xzz7o) from 2012;  note plankton are organism that doesn't swim in the water

http://apps.seattletimes.com/reports/sea-change/2013/sep/11/pacific-ocean-perilous-turn-overview/ was a series on climate change

## Global warming strategy
Four reasons why the fight against climate change is likely to fail (2014) - Mufson details the losing war

J.H. Adler espouses a fairly reasonable view at A Conservative's Approach to Combating Climate Change (2012)

Global tracking framework - UN/World Bank Sustainable Energy for All (SE4ALL) report

From RFF article below:

The settlement agreement reveals a major piece of EPA’s regulatory strategy for GHGs under the
CAA, comparable in magnitude to the 2010 announcements of new fuel economy standards for
vehicles and permitting requirements for new construction. These three regulatory programs will
cover almost all GHG sources that can be regulated under the Clean Air Act, and a substantial
majority of U.S. emissions new stationary targets include various industrial sources like cement plants, and the aforementioned turbine EGUs.

## Money

Climate Policy Initiative publishes annually:
* Global Climate Finance: An Updated View 2018 - $455b
* 2012: Global Climate Spending Falling Further Behind Target: Report - $359b

See Institutional Investors Group on Climate Change (IIGCC) for a $100B in UN dollars (??)

http://www.carbontax.org/issues/border-adjustments/ - border tax adjustments for carbon taxation

Tom Steyer of Farallon Capital is entering big in 2013 per BBW's The Wrath of a Billionaire


## Politics and laws
2014-03:  [Climate Change Legislation, and More from CRS](http://blogs.fas.org/secrecy/2014/03/crs-climate/) (2014) - notes only one law which imposes a tax on greenhouse gas emissions

See http://www.govtrack.us/congress/votes/113-2013/s58 for a list of the senators who are anti-climate change legislation; includes Baucus, (surprisingly Begich voted in favor); Pryor, Heitkamp, McCaskill, Johnson, both West Virginia Democrats (including Rockefeller, good he is retiring I guess tho it's a coal-state)

In terms of legislation, I skimmed the committee report of the American Clean Energy and Security Act (ACES) Waxman-Markey 2009 bill and noticed that it spoke in terms of hitting 7,400 facilities only but it does target 83% cut by 2050 with a summary at http://www.terrapass.com/politics/seriously-waxman-markey-is-good/ and a negative view at http://www.greens.org/s-r/51/51-02.html;

## Grandfathering and New Source Review
http://en.wikipedia.org/wiki/New_Source_Review is not related to the New Source Performance Standard above

New Source Review and “routine maintenance, repair, and replacement” or the “RMRR exclusion” has led to significant controversy; Bush Administration created bright-line test that generally allows modifications to be characterized as routine if the modification costs less than 20 percent of the original plant construction cost

Section 111(d) of CAA sets new source performance but also existing source guidelines for states with implementation plan approved by the EPA; applies where other sections do not apply

Section 112 includes national ambient air qualitystandards (NAAQS) and air toxics regulation; not really stationary

New York v. EPA, 443 F. 3d 880 - Court of Appeals, Dist. of Columbia Circuit 2006(http://scholar.google.com/scholar_case?case=10889526225470032186) was a lawsuit over above issue which the EPA lost; commentary at [EPA Settlement Holds Potential for Great Effect on GHG Emissions](http://www.ballardspahr.com/alertspublications/legalalerts/2010-12-27_epasettlementholdspotentialforgreat%20effectonghgemissions.aspx) summarizes what happened OK

2012-03 [A.G. Schneiderman Commends EPA For Taking Action To Limit Greenhouse Gas Pollution From New Power Plants](http://www.ag.ny.gov/press-release/ag-schneiderman-commends-epa-taking-action-limit-greenhouse-gas-pollution-new-power) - comments on proposed new regulations which says new plants have 50% less but what about existing?

2011-02 EPA Greenhouse Gas Performance Standards: What the Settlement Agreement Means - good comment on below proposed settlement; says Obama EPA made efforts in 2010 which apply to existing polluters (existing source performance standards or ESPS); says the settlement applies to existing fossil fuel power plants and petroleum refineries which account for 40% of GHG emissions but "also leaves out a wide variety of GHG sources"; example includes "turbine generators fueled by natural gas or other fossil fuels" as not regulated but combined‐cycle plants are regulated; Turbine generators are a significant electricity source—they make up about 19 percent of fossil fuel electricity generation and 14 percent of overall electric generation (in 2010); largely due to procedural issues so possibly no biggie

2010-12 [Proposed Settlement Agreement, Clean Air Act Citizen Suit](https://www.federalregister.gov/articles/2010/12/30/2010-32935/proposed-settlement-agreement-clean-air-act-citizen-suit) \- Document Citation 75 FR 82392; intended to resolve threatened litigation over the EPA's failure to respond to United States Court of Appeals for the District of Columbia Circuit's remand in State of New York, et al. v. EPA, No. 06-1322

2010 U.S. Supreme Court’s 2007 decision in Massachusetts v. EPA, the Court of Appeals remanded the issue to EPA for reconsideration in light of that decision

2007 Sometime around here the Supreme Court sided with the District of Columbia against the Fourth Circuit in a Duke Energy case

2006 What’s Old Is New: The Problem with New Source Review - Cato Institute article which got me started on reading up about this