Green consumer
===
Green stores - list at http://cleantechnica.com/2008/05/11/green-resource-online-top-25-shopping-sites/
* http://www.responsiblepurchasing.org/index.php
* http://www.ecomall.com
* http://www.pristineplanet.com/default.asp
* http://www.comparethebrands.com/category/green-products
* http://www.metaefficient.com
* http://www.greenamerica.org/

Also see http://cleantechnica.com/2008/05/11/green-resource-online-top-25-shopping-sites/ for ways to shop green.

_Watchdog organizations_

http://www.goodguide.com - apparently a "B Corporation"

http://en.wikipedia.org/wiki/Environmental_audit has no discussion of consumer-based environmental auditors

Greenwash: Corporate Environmental Disclosure under Threat of Audit is a model; in 2013 FTC moved in (first time?) per FTC Cracks Down on Misleading and Unsubstantiated Environmental Marketing Claims

Climate Counts - joined this as a member; creates a climate change metric for companies

_Home building_

http://greenhomeguide.com/ and http://www.greenhomebuilding.com/

_Electronics_

Electronic Product Environmental Assessment Tool - certifies electronics

Notable companies

Interface (NYSE:IFSIA)'s Ray Anderson is a pioneer

Steelcase (NYSE:SCS) was first to use mushroom for packaging

Dell came after Steelcase

Sealed Air (SEE) came after all these

_Renewable Energy Credits (RECs) also called certificates_

Got into this when I noticed Endurance International Group (EIG) was saying on all there hosting companies that they use green power; allegations of greenwashing and scamming, plus few details on audits at ipage.com led to investigation. However after looking closer at their banner it does say Community Energy Inc (see below).

EPA website recommends "buy green power products that are independently certified and verified through an audit". REC tracking organizations http://www.epa.gov/greenpower/gpmarket/tracking.htm help to prevent double-counting. However this says "Tracking systems are not substitutes for product certification and verification, as tracking systems only monitor wholesale transactions — individual retail green power customers do not generally hold accounts in tracking systems unless they make very large purchases". http://www.epa.gov/greenpower/buygp/claims.htm

Making Environmental Claims has "claims" recommendations; main page is probably http://www.epa.gov/greenpower/gpmarket/rec.htm

_REC marketeters or sellers_

http://apps3.eere.energy.gov/greenpower/markets/certificates.shtml?page=2 - large list with different subcategories

Community Energy Inc - retail and wholesale REC marketer which Bill McKibben purchases from

http://en.wikipedia.org/wiki/Green_Mountain_Energy_Company - very negative reviews at http://www.boycottgreenmountain.com/

As of 2012 almost none of the active retail marketers have a wiki page

Stopped at Green Mountain on list
