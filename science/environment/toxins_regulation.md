Toxins (EPA focus)
===

## personal testing
https://www.millionmarker.com
* aflatoxin testing?

## pfas
Electrochemical & plasma approaches discussed at https://cen.acs.org/environment/persistent-pollutants/Forever-chemicals-technologies-aim-destroy/97/i12

[Microbe chews through PFAS and other tough contaminants](https://phys.org/news/2019-09-microbe-pfas-tough-contaminants.amp)

http://sci-hub.tw/10.1002/rem.21553 | Sci-Hub | A review of emerging technologies for remediation of PFASs. Remediation Journal, 28(2), 101–126 | 10.1002/rem.21553
https://cen.acs.org/business/specialty-chemicals/Chemourss-Paul-Kirsch-company-needs/97/i5 | Chemours’s Paul Kirsch on why his company needs to change and how it will do it
https://advancedtextilessource.com/2015/12/04/safety-first-a-closer-look-at-environmentally-preferred-dwrs/ | Safety first: A closer look at environmentally-preferred DWRs – Advanced Textiles Source
https://cen.acs.org/environment/persistent-pollutants/Forever-chemicals-technologies-aim-destroy/97/i12 | ‘Forever chemicals’ no more? These technologies aim to destroy PFAS in water

## overview
See agriculture for pesticide notes and toxicology

[Toxipedia](https://www.healthandenvironment.org/environmental-health/explore-further/toxipedia)

Get involved through http://en.wikipedia.org/wiki/Wikipedia:WikiProject_Medicine/Toxicology_task_force 

## Solvents paint thinners, degreasers etc
[Brain may never fully recover from exposure to paint, glue, degreasers](http://www.eurekalert.org/pub_releases/2014-05/aaon-bmn050814.php "http://www.eurekalert.org/pub_releases/2014-05/aaon-bmn050814.php") (2014) - sounds awfully rough 

Neurotoxicity: Grandjean/Landrigan are 'scaremongers' who want more research and precaution when it comes to neurotoxins

## Long-term storage of mercury
Happy to see that the UNEP is on this area even if it's not super-promising, see e.g. Options Analysis and Feasibility Study for the Long Term Storage of Mercury in Latin America and the Caribbean (2010) by UNEP, note that only very recently has the export been banned even tho the top 3 countries were huge artisinal miners per Kulp's mercenary musings   

## Endocrine
BPA is the biggie but also fire retardants and phthalates; also these can rise from the dead (Hormone disruptors rise from the dead)

### Flame retardants
bad ones include tri(chloropropyl) phosphate (TCPP) or some polybrominated diphenyl ethers (PBDE); less toxic ones developed per Non-toxic flame retardants by FoamPartner

### Quaternary ammonium compound
newly discovered in part by BPA researcher Hunt and independently by Hrubec; read about  [in 2014](http://www.eurekalert.org/pub_releases/2014-08/vt-chc081114.php "http://www.eurekalert.org/pub_releases/2014-08/vt-chc081114.php") but news actually came around 2013

## Random
titanium dioxide used in sunscreen - can cause potentially toxic effects when exposed to ultraviolet light; study by Turci shows that one of the two most commonly used crystalline forms of TiO2, called rutile, easily washes off and has little effect on skin. Anatase, the other commonly used form, however, was difficult to wash off and damaged the outermost layer of skin — even in low ultraviolet light. It appears to do so via "free radicals," which are associated with skin aging

## Cadmium
EWG notes that cadmiun is not banned in the US and discusses the history at Why do toys contain toxic cadmium? - the Lautenberg Safer Chemicals Act impact?

## Lead
[New Report Shows Only 15 States and D.C. Have Laws for Testing Lead in School Water](https://www.usgbc.org/articles/new-report-shows-only-15-states-and-dc-have-laws-testing-lead-school-water)

See avgas-political-rant.md
Avgas (aviation fuel) has this; http://oecdwatch.org/cases/Case_242/@@casesearchview?type=Issue&search=en_Health%20risks%20related%20to%20leaded%20gasoline notes that Innospec, Xstrata, and Tetraboost do this; also see http://www.generalaviationnews.com/2013/02/innospec-news-a-shot-across-the-bow/ about concern that avgas could go away

Lead in homes in the United States: not much funding for this unfortunately (got into it through Reddit book review); http://portal.hud.gov/hudportal/HUD?src=/program_offices/healthy_homes is the main place;  [American Healthy Homes Survey: Leadand Arsenic Findings](http://www.hud.gov/offices/lead/NHHC/presentations/R-15_Findings_from_AHH_survey.pdf) (from 2005-6) says about 1/3 have leadpaint www.delta-institute.org/sites/default/files/1-DeltaREDI_CreativeFundingStrategiesForRemediationOfLead.pdf Creative Funding Strategies for Remediation of Lead- discusses the various dozen or so federal programs, none of them seem all that well-funded; 

Global lead Paint elimination by 2020 - IPEN report with an ambitious goal

Abatement: can either remove or encapsulate as discussed in Lead Paint Removal: Options and Costs

also A. Russell Flegal gave a talk in 2013 at AAAS saying it could take centuries

[House GOP Cuts Funding for LeadRemoval in Half](http://www.motherjones.com/kevin-drum/2013/07/house-gop-cuts-funding-lead-removal-half) - ?

[After Flicking Away Lawsuits, LeadIndustry Goes for a Final Knockout](http://www.fairwarning.org/2013/08/after-flicking-away-lawsuits-lead-industry-goes-for-a-final-knockout/ "http://www.fairwarning.org/2013/08/after-flicking-away-lawsuits-lead-industry-goes-for-a-final-knockout/") - five companies fighting including DuPont, Atlantic Richfield (ARCO/Tesoro), WP Fuller (ConAgra) etc per  http://legalnewsline.com/issues/lead-paint/243037-calif-lead-paint-judge-urges-sides-to-settle-this-week; notes advertisement of “This famous Dutch Boy Lead of mine can make this playroom fairly shine" after 1912 annual report noted danger of dust

Industrial uses of lead - at the EoE and really a detailed overview of MA reporting under Massachusetts Toxics Use Reduction Program

## Companies
Doe Run is the biggie and won a Missouri battle in 2013 (Doe Run Settles Lead Liability Cases in Missouri) which led me to McArdle's infuriating article Getting the Liability Out of Lead Paint; 

National Lead (now NL with NYSE:NL) - biggie from back in the 1900s per http://www.allgov.com/news/controversies/lead-industry-on-verge-of-victory-over-final-lawsuit-130812?news=850839 and now does locks with CompX International and Kronos Worldwide for titanium dioxide plus EWI RE for insurance

Sherwin Williams - ditto going way back

[Judge orders companies to pay $1.1B in lead paint ruling ](http://www.bloomberg.com/news/2013-12-16/sherwin-nl-conagra-lose-1-1-billion-lead-paint-verdict-1-.html)  - great news **

## Industry
Lead Liability Insurance: Going . . . Going . . . Gone? (1997) - discusses changes and standards in some states

Understanding Lead Pigment Litigation - http://www.leadlawsuits.com/index.php?s=687 says lawsuits started in 1987

 Lead Settlement Review 1999-2000 - overview of settlements altho not sure where they got the information, variety of defendants including property management, contractors, governments, 1 ceramic paint manufacturer, etc

## Lead paint for artists
making a last stand; altho banned in EU per  [The Great Lead White Shortage](http://paintingperceptions.com/sounding-technical/the-great-lead-white-shortage) (2012), Natural Pigments makes it per  [Lead White Oil Paint](http://www.naturalpigments.com/art-supply-education/lead-white-oil-paint) (2013) 

## MSDS
Required by http://en.wikipedia.org/wiki/Emergency_Planning_and_Community_Right-to-Know_Act (1986) per http://en.wikipedia.org/wiki/Material_safety_data_sheet; I guess http://en.wikipedia.org/wiki/Resource_Conservation_and_Recovery_Act wasn't involved in that (1976)

## Organizations
http://www.healthystuff.org/

Environmental Working Group EWG

Environmental Integrity Project - former EPA scientists make a watchdog group

The Ecology Center - tests the new car smell http://theweek.com/article/index/224612/is-that-new-car-smell- toxic-to-your-bodynbsp

Environmental Council of the States (ECOS) - created http://www.chemicalright2know.org/learn-more-about-the-site/; like NAIC for environment

Safer Chemicals, Healthy Families - http://www.saferchemicals.org/about/who.html coalition of a bunch of groups; wrote up A Platform for Reform of the ToxicSubstances Control Act which is referenced in the committee report for S. 847

Chemicals Policy & Science Initiative CPSI - http://www.chemicalspolicy.org/about.cpsi.php "Since 2001 ... has grown into the most prominent academic chemicals policy program"

Blacksmith Institute - http://www.blacksmithinstitute.org/ good group highlighting specific areas through http://www.worstpolluted.org/;

## Research
Look at humanexposomeproject.com, motivated by abstract of The Blood Exposome and Its Role in Discovering Causes of Disease (2014)

Tox21 project ( [ wiki](https://en.wikipedia.org/wiki/In_vitro_toxicology)) is a major push at the NIH; also read Toxicology for the twenty-first century (2009).

## EPA review
[Why 28 years have passed since the EPA’s last chemical risk review](http://america.aljazeera.com/articles/2014/6/27/epa-chemical-regulation.html) (2014) - ridiculous

Way back when I saw pdf documents which showed PhDs reviewing and commenting on chemicals. Authority is mainly in the The ToxicSubstances Control Act (TSCA); new law has been submitted a few times

EPA toxics organization chart at http://www.epa.gov/aboutepa/orgchart_ocspp.html

Overall organization at http://www.epa.gov/aboutepa/organization.html as well as http://www.epa.gov/aboutepa/index.html

In 2009, EPA halted its ChAMP program, which was criticized by industry http://www.rsc.org/chemistryworld/News/2009/June/23060901.asp

## Food toxins
Solanine in potatoes

Arsenic in rice; originally found thru Rice contains 'worrisome' arsenic levels, says Consumer Reports; CR at Arsenic in your food: Our findings show a real need for federal standards for this toxin(http://www.consumerreports.org/cro/magazine/2012/11/arsenic-in-your-food/index.htm); read thru the wiki page at http://en.wikipedia.org/wiki/Arsenic and found it is still used as pesticides (Monosodium methyl arsenate, Disodium methyl arsonate) and for treating wood

## Persistent and bioaccumulative
See http://en.wikipedia.org/wiki/Regulation_of_chemicals

113th Congress: Chemical Safety Improvement Act by Lautenberg  [described by Thomas Kean in Politico](http://www.politico.com/story/2013/07/frank-lautenberg-chemical-regulation-94133.html), note that there seems to be a regulation in the works to address a bunch of chemicals too (see Unified Agenda discussion)

112 Congress: Safer Chemicals Act in United States see http://www.govtrack.us/congress/bills/112/s847 with the House analog of http://www.govtrack.us/congress/bills/112/s847 ToxicChemicals Safety Act of 2010 (H.R. 5820) in 111th

There's also http://www.govtrack.us/congress/bills/112/hr2521 Endocrine-Disrupting Chemicals Exposure Elimination Act of 2011 and http://www.govtrack.us/congress/bills/110/hr6100 Kid-Safe ChemicalAct of 2008

PBT (persistent, bioaccumulative, and toxic) is a word used in the industry

Global usage of selected persistent organochlorines (1999) by Voldner & Li - these are banned but still used

Persistent toxicchemicals in the US food supply (2002) by Schafer & Kegley

PFOA is a worrisome one

Major Corporations Take Key Steps to Put Consumer Health First - Walmart, P&G, and Johnson  & Johnson agreed to remove a lot of toxins from their products

## Mycotoxins
see separate mold/fungi health note

Aflatoxin is considered to be the worst; can cause severe liver damage and cancer; found in a lot of milk thistle/silymarin and speculative connection to kava toxicity