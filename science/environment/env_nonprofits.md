Env nonprofits
===
## General
In the past I started getting a ton of spam from environmental groups. Could not pin down who was selling my info

Conservation's Friends in High Places: Neoliberalism, Networks, and the Transnational Conservation Elite - 2011 intriguing article; as I review the boards of some of these groups I can really see this

Financial Giving of Foundations and Businesses to Environmental NGOs: The Role of Grantee’s Legitimacy - very intriguing article

Looked into the Environmental Grantmaking Association study: http://www.issuelab.org/resource/tracking_the_field_volume_3_exploring_environmental_grantmaking

http://www.ncrp.org/files/publications/Cultivating_the_grassroots_final_lowres.pdf - author is Sarah Hansen

Foundation funding of grassroots organizations - http://www.citizenshandbook.org/foundation_funding.html  

 ## Climate change
[Nonprofits examine climate-change roles](http://seattletimes.com/html/localnews/2014670744_climate03.html) points to Climate Policy Institute and Climate Works, which aims to influence policy in countries such as China and India

http://www.carbonfund.org/ - haven't really used it but looks OK

http://climatechangeeducation.org/about/index.html - education group

Climatecounts.org - one of favorite groups; focuses on oversight of corporate climate initiatives

Looked at SolarElectric Light Fund but their high executive compensation is a big turn-off; similar to SolarAid which I had concerns about (see below)

SolarAid - intriguing, but no financials on the website. Ralph Greenland (mark assistant Ralph@ solar-aid.org) responded with overview (Programme costs: 90.77% / Fundraising costs: 7.22% / Governance costs: 2.01%); however, I asked for audited financials online; no response initially but after pressing Ralph Greenland responded; related to SunnyMoney and Eight19 per http://www.enterprise.cam.ac.uk/news/2012/1/eight19-launches-new-fund-bring-affordable- solar-p/

For-profit companies are doing this instead per Bringing Pay-as-You-Go Power to the Masses in BBW, which mentions Angaza Design, Powerhive, and Azuri Technologies

 ## Other
Gobal Greengrants Fund was marked as similar to SolarElectric Light Fund, but their grants are not typically for actual energy creation - for advocacy campaigns mostly

Nature Conservancy - $1b or so in revenue (!?!); actually diversified with 2012 numbers showing 30% in foundations, 6% corporate, 28% individual, 20% bequests, 17% "other"; land sales, government grants, and so on form part of revenue

Environmental Working Group - only $6m in revenue or so; like the work; no email on website and autoresponse to form had no copy; don't think they responded

Environmental Defense Fund - about $100m in revenue; big group

Union of Concerned Scientists - $22m annual revenue

Sierra Club - $51m annual revenue; member in 2010/2011, got a backpack for a nominal donation plus I was inspired by table of coal-power plant battles. Shared my address (per Greenpeace email). Richly-printed magazine, not available electronically, turned me off. Then they offered a free duffel bag with renewal. Never responded to my questions. Local group at www.alaska.sierraclub.org/; person from there (Pamela Brodie pbrodie@gci.net) did not respond about how to meet up with Juneau Group. Took natural gas money until recently.

Greenpeace - $27m in annual revenue; does not keep a list of names that receive a onetime mailing; however, very good response from Kris. However, got another mail so they added me to the "Do Not Mail" list.

Natural Resources Defense Council - $97m in annual revenue;

Audobon - $67m contributions and $96m in total revenue in 2012; $432m in assets with $47m in liabilities

League of Conservation Voters - Scott A. Nathan is on the board and is Chief Risk Officer at Baupost;

Ocean Conservancy - took me off mailing list; said it was acquired in "blind exchange" so even though I gave them a form number, they could not tell me who received it.

National Parks Conservation Association - email available; good response on dropping me from junk mail.

National Wildlife Federation - no autocopy for form (no email); never responded to my email about junk mail.

World Wildlife Fund - similar to Ocean Conservancy

Turning the Tides - no email on the website when I sent them http://www.eurekalert.org/pub_releases/2011-10/acs-hwm101911.php; no response

The Nature Conservancy - the local Alaska branch has people from Conocco and various other high-profile corporate groups; see http://www.akrdc.org/