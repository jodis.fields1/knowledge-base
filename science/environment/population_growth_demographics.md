Population growth and demographics
===
See Project Prevention
  
## Environmental angle
http://en.wikipedia.org/wiki/Michael_E._Arth has advocated for a birth credit
* "Each person would be issued half of a birth credit, which he or she can combine with a partner to have one child, or a person can sell his or her (half) credit at the going market rate. Each additional child costs one more credit" https://www.vice.com/en_us/article/jpp7q7/birth-credits-population-control

Climate ethics and population policy (2012) by Cafaro -  out of the philosophy department and arguing that this is  ignored  
  
[Overpopulation: The  transparent elephant in the room causing crucial modern  crises](http://www.eurekalert.org/pub_releases/2014-03/uoha-ott031114.php) - Camilo Mora saying the same thing  
  
Conceiving the Impact: Connecting PopulationGrowth and  Environmental Sustainability (2011) - dead link...  

## Future generation
What percent of kids are with single mothers?  24% in  2010 www.prb.org/pdf10/single-motherfamilies.pdf  

But that hasn't changed much lately    http://www.healthymarriageinfo.org/research-and-policy/marriage-facts/download.aspx?id=263  
    
Narcissus at the Gene Pool - ?? don't have access  
    
## Dysgenics
see  [wiki fertility and intelligence](https://en.wikipedia.org/wiki/Fertility_and_intelligence)

Recent Changes In Fertility Rates In The United States:    What Do They Tell Us About Americans' Changing Families? (2008)    \- really rich families have more kids, but middle-class having    less  
  
## Welfare and child protection
Rethinking the Procreative Right (2007) by Carter J. Dillard  \- provocative article on restricting the right to procreation;  discusses Skinner v. Oklahoma where certain criminals were  sterilized but the list of crimes was arbitrary and held  unconstitutional on equal protection grounds but there's also  Buck v. Bell which upheld sterilization, also note Gerber v.  Hickman where the Ninth Circuit reviewed Skinner and found it  does not generally protect procreation; also see State v. Oakley  where Supreme Court of Wisconsin looked at the state's  prohibition on Oakley having more cyhildren  

Child Welfare and Future Persons (2009) by Carter J. Dillard - another good overview  

Putting Children Last: How Washington  Has Failed    to Protect the Dependent Child's Best Interest  in Visitation (2009) - abusive visitation by parents  

Chemical Castration of Sex Offenders: A Shot in the Arm  towards Rehabilitation (1997) - don't have access, and similar  is Mandatory Chemical Castration for Perpetrators of Sex  Offenses against Children: Following California's Lead  

Judge Imposes Birth Control To Prevent Michigan Woman From  Having More Children (2003) - Family Independence Center v.  Renee Gamez in Michigan; ACLU defended woman after judge  ordered her to have verifiable birth control  

Has the State Gone too Far - Testing the Constitutionality  of Probation Conditions That Limit a Probationer's Right to  Procreate (2001) - similar to above  

The constitutionality of the use of the Norplant  contraceptive device as a condition of probation (1992) - what  happened here?  

## Euthanasia
The other side of the coin: "A Merciful End" tells the early  story; Euthanasia Society of America went through successors and  eventually was taken over by the National Hospice and Palliative  Care Organization (NHPCO) according to hospicepatients.org which  has a great book on it at  http://www.hospicepatients.org/this-thing-called-hospice.html  called Stealth Euthanasia: Health Care Tyranny in America  

Note that most elderly will end up in Medicaid long-term  care program; should have a separate note on elder care  

## Future demographics and elder care

After reading Ch. 16 (exclusions) of the Medicare policy  manual and custodial care in section 110, I wanted to find out  where poor elderly people get cared for if they need custodial  care - and the answer is Medicaid and skilled nursing facilities  or home-based care. Turns out this is 1/3 of Medicaid and  exploding - see NYTimes 2012 With Medicaid, Long-Term Care of  Elderly Looms as a Rising Cost; also see  http://www.kaiserhealthnews.org/Stories/2013/February/08/florida-medicaid-managed-care-long-term-care.aspx;  Kaiser Commission on Medicaid and the Uninsured wrote a 2004  report on long-term care which shows $130b in 2002 for long-term  care with $100b of that for nursing homes; 72% female, 45% 85+;  75% memory loss; home-based and community care is based on a  federal waiver, which is the big new trend in 2013; 40% of  nursing home residents are on Medicaid in 2000; I recall the 2011  or so scandal in nursing homes but it discusses a 1998 scandal  (lol?);  

## Medicaid long-term care and gifts

http://www.nolo.com/legal-encyclopedia/are-revocable-irrevocable-living-trusts-useful-qualifying-medicaid.html  seems to be a good article and conclusive about avoiding  bankrupting yourself  
  
myths about medicaid notes that you can have a lot of equity  in a house and still qualify  http://www.elderlawanswers.com/five-myths-about-medicaids-long-term-care-coverage-12114;  also there are rules on transferring assets and the $14,000 for  the gift tax still could count; however per  http://www.wikihow.com/Avoid-Paying-Gift-Tax there is a $5m  lifetime gift tax exemption  

there's talk about the rules being more "bark than bite" but  one article says don't depend on that  http://www.longtermcarelink.net/article-2006-11-8.htm;  
  
look-back period is based on amount transferred divided by  $136 per day per  http://www.foulston.com/downloads/Medicaid%2520and%2520Longterm%2520Planning.pdf  
  
See  http://www.nolo.com/legal-encyclopedia/how-can-i-safely-transfer-my-assets-get-medicaid-pay-long-term-care.html  for a list of exceptions  

http://www.cutner.com/special_trusts_pooled_supplemental.html  discusses Special Pooled Income Trusts  

two types of states: 209(b) states and SSI states per  http://www.lifemanagement.com/nsa17.2.1417/; "rule in SSI states  is that to the extent that the pension funds are not available to  the nursing home spouse, those assets will not be deemed to be  countable for Medicaid purposes"; other sources discuss "payout  status"  
  
## General
http://timsviews.com/birthlicense.aspx states the  obvious  
  
http://thebreakthrough.org/index.php/programs/conservation-and-development/  population-bomb-so-wrong/ makes the provocative argument for the  role of cable television in declining fertility  

## Birth license or credit
  
See http://en.wikipedia.org/wiki/Birth_credit;  
  
Michael E. Arth advocates for birth credit or license; see  Does a Transferable Birth License Scheme Work as an Alternative  to One-Child Policy? (2012) for an article;  http://forum.bodybuilding.com/showthread.php?t=131561163&page=1  shows a very negative perspective; also see  http://www.prescottaz.com/main.asp?SectionID=36&SubsectionID=73&ArticleID=106865  
  
Children Requiring a Caring Kommunity (CRACK) Project  Prevention - pays drug abusers to not have children  
  
Royal Society - 2012 report calls for populationcontrol per  http://www.guardian.co.uk/environment/2012/apr/26/earth-  population-consumption-disasters and  http://www.newscientist.com/article/dn21745-how-to-defuse-subsaharan-africas-  population-bomb.html?page=2  
  
## Devices
http://www.patient.co.uk/health/intrauterine-contraceptive-device  or the "the coil" does not affect periods making it easy to hide  from husbands; concerns also about using Depo-Provera  http://www.care2.com/causes/will-world-  population-day-open-the-gates-to-coercive-contraception.html  which the Gates foundation is using;  http://quod.lib.umich.edu/j/jii/4750978.0008.205?rgn=main;view=fulltext  talks about the cultural issue of IUDs in India and heavy  bleeding side-effects;  http://en.wikipedia.org/wiki/DKT_International is a nonprofit  focusing on IUDs per  http://www.impatientoptimists.org/Posts/2013/02/The-IUD-A-Tiny-Device-With-a-Huge-Impact  which also argues that they've gotten better;  http://blogs.bmj.com/jfprhc/2012/05/11/melinda-gates-new-crusade-confirmation-that-iud-is-most-effective-for-ec/  mentions that clinicians are not offering IUDs, especially  same-day insertion; Side effects from the copper IUD: do they  decrease over time? (2009) is a review which says some go away  and some say; Mirena is a second-generation IUD with  levonorgestrel but it has lawsuits with criticism from  conservative PRI at  http://www.lifesitenews.com/news/the-mirena-iud-is-becoming-more-popular-and-the-lawsuits-are-piling-up/;  only 2 versions in the US (ParaGuard and Mirena) while outside  there are more options including GyneFix which is smaller than  ParaGuard in Canada per  http://www.womenshealthmag.com/health/iud-birth-control; good  overview at  http://www.contrel.be/GYNEFIX%20SPECIALISTS/gynefix.htm  

https://en.wikipedia.org/wiki/Norplant - the original  implant  

http://sites.path.org/rh/recent-reproductive-health-projects/sayanapress/  \- Pfizer product distributed by Becton, Dickinson (BD) needles  (not to be confused with Reckitt Benckiser) - both  publicly-traded, implanted with Uniject needles;   

_Side effects_

Depo-Provera - may cause irreversible bone loss  per Depo-Provera#Black_box_warning  

Mirena - uses similar mechanism to depo but different  ingredient which suggests that it may not cause bone loss since  it doesn't cause hypoestrogenism per    [IUD  with progestogen wiki article](https://en.wikipedia.org/wiki/IUD_with_progestogen#Bone_density); also see this    [discussion at  treato](http://treato.com/Mirena,Bone+Loss/?a=s) (compare to discussion about dep-provera)  

[UMM    Birth control options for women](http://umm.edu/health/medical/reports/articles/birth-control-options-for-women) \- hard to read this    article I think  

Progestin - synthetic progestogen;  [wiki](https://en.wikipedia.org/wiki/Progestins#Examples) history has the history and the examples lists    the generations  

## Application

* Look into federal U.S. money going into population

Looking into Africa, I found in 2012 Goodluck at  Nigeria is calling for mandatory birth control (but strangely not  partnering with Gates?); Sayana Press by Path & Gates is one  strategy with a 3-month injectable   

http://knowledge.allianz.com/demography/health/?232/aids-impacts-africas-  population(2010) which has some estimates and the much more  detailed Ch. 6 of Disease and Mortality in Sub-Saharan titled  Populationand Mortality after AIDS; also noticed Robert Zubrin's  stupid history http://www.thenewatlantis.com/publications/the-  population-control-holocaust;

## Nonprofits
r/overpopulation has 7k interested parties  
  
The Guardian in 2012 (Counting the cost of family  planning) said that $8.1b is needed to meet current need  versus the $4b spent  
  
2013-05: I noticed that Bill & Melinda got into this in  a big way with the 2012 Summit on Family Planning  http://www.londonfamilyplanningsummit.co.uk/; Economist links to  some articles including a series issue of the Lancet  http://www.economist.com/blogs/feastandfamine/2012/07/family-planning;  also  http://dotearth.blogs.nytimes.com/2012/07/11/a-gates-summit-aims-to-fill-a-family-planning-gap/  and from there I went to  http://blogs.worldbank.org/impactevaluations/is-there-an-unmet-need-for-birth-control-0  for a lively debate on the demand or need (yes, there is good  evidence that it works);  

Population Services International - vary large with ~$700  million budget; not on Charity Navigator (!?); Givewell  evaluated; put money to IUDs hopefully, also have Tracking  Results Continuously (TRaC) studies;  

Population Council - biggie with $45 million budget; leader  is way overpaid; no Givewell analysis  

United Nations PopulationFund (UNFP) - big influence  

Population Connection - United States-based education  group  

Population Action International - ??  

Population Research Institute - more people! conservative  group  

The Population Institute - tiny and under Charity Navigator  radar  

Population Media Center  

PopulationReference Bureau  - http://www.prb.org/Home.aspx statistics  

Family Health International - diversified recently  

DTK International - provides a lot of CYP which stands for  Couple Years Protection  

Path.org - funding Sayana Press Uniject contraception, lots  of innovations  

Population Foundation of India - can't seem to donate to  it  

## Male

Vasalgel - reversible male contraception, also  see http://www.newmalecontraception.org for overview of  different methods and    [Who wants a male pill?](http://www.theguardian.com/society/2014/feb/01/who-wants-male-contraceptive-pill-chauvinism) for reddit/guardian  discussion  

Found one through Warren Buffett Ends Support for  Population Control Programs | Population Research  Institute 