Recycling
===

## mattress
https://www.abedderworld.com/san-francisco-ca/

## reduce
https://wellearthgoods.com/ - order plastic-free goods

https://www.ecoproductsstore.com/compostable_liners_and_bags.html
* found from CEH

https://fulfilld.co/ - waste-free store

https://zerogrocery.com/shop/sfo
* noticed Frankie Nicoletti is head of engineering

https://thewallyshop.co/
* shut down 2021

Humankind - https://byhumankind.com - 
* also see [Georganics Natural Mouthwash Tablets for Fresh Breath](https://www.amazon.com/gp/product/B0752RLDXC)
* [Eco-Friendly Shampoo](https://www.amazon.com/gp/product/B01LXZZIXL)

Loop - https://loopstore.com/products
* https://www.terracycle.com/en-US/collection-programs
* https://hbr.org/2020/01/the-new-business-of-garbage

CleanCult

https://www.how2recycle.info/labels

https://earthhero.com/products/home/terracycle-terracycle-all-in-one-zero-waste-box/

## new
[Foam packaging that dissolves in water? Yes, as Perdue launches online orders](https://www.usatoday.com/story/money/2020/01/09/perdue-launches-online-orders-packaging-dissolves-water/4420163002/)

[16 companies rethinking packaging](https://www.greenbiz.com/article/16-companies-rethinking-packaging)
* Terracycle fundraising https://www.startengine.com/terracycle

https://www.bepakt.com/ - list of waste-free stores

https://wastelandrebel.com/en/my-quest-to-find-the-most-sustainable-toothbrush/

### news
Ellen MacArthur Foundation pledge https://www.newplasticseconomy.org/news/first-annual-new-plastics-economy-global-commitment-progress-report-published

Trucircle - https://www.plasticsnews.com/news/sabic-commits-closing-plastics-loop-2020

Sabic - https://www.plasticsnews.com/news/sabic-commits-closing-plastics-loop-2020
* relying upon https://twitter.com/plasticenergy

### ethical consumer
TODO: merge with green_consumer.md

https://rankabrand.org/sustainable-outdoor-clothing/Patagonia
https://www.2ndvote.com/business-entity/10465/

### recycling plastic bags
[Glad Introduces Recyclable Food Bags Through TerraCycle's Loop Program](https://finance.yahoo.com/news/glad-introduces-recyclable-food-bags-150300767.html)

## old
[ReLoop: What is Mixed Waste Processing or “All in One/Dirty MRF” Recycling? | GreenBlue](http://greenblue.org/reloop-what-is-mixed-waste-processing-or-all-in-onedirty-mrf-recycling/)

2015-02 update: looked into San Fran where recycling also kinda sucks, gets sent over to China altho they are rejecting some due to a new green wall

Huge interest of mine for a long time but only now starting a note 2014-02! Went on a spree and gonna record my notes.

**Overview**

[Recycling: Can It Be Wrong, When It Feels So Right?](http://www.cato-unbound.org/2013/06/03/michael-c-munger/recycling-can-it-be-wrong-when-it-feels-so-right "http://www.cato-unbound.org/2013/06/03/michael-c-munger/recycling-can-it-be-wrong-when-it-feels-so-right") (2013) - critique from Cato

**[The truth about recycling](http://www.economist.com/node/9249262 "http://www.economist.com/node/9249262") (2007) - the economist**

**[Waste Prevention, Recycling, and Composting Options: Lessons from 30 US Communities](http://www.epa.gov/osw/conserve/downloads/recy-com/ "http://www.epa.gov/osw/conserve/downloads/recy-com/") (1995) - really old EPA book  

**Landfilling**

**[‘Recycled’ at the landfill: Much of the glass from local curbside bins ends up as landfill material, not new bottles](http://www.registerguard.com/rg/news/local/30722192-75/glass-curbside-landfills-recycling-bottles.html.csp "http://www.registerguard.com/rg/news/local/30722192-75/glass-curbside-landfills-recycling-bottles.html.csp") (2013) - excellent article on Eugene's glass market**

_Anchorage_
[Anchorage residents may finally get to recycle their glass again](http://www.alaskadispatch.com/article/anchorage-residents-may-finally-get-recycle-their-glass-again "http://www.alaskadispatch.com/article/anchorage-residents-may-finally-get-recycle-their-glass-again") (2012) - clearly difficult

**Reforms**
11 states have bottle deposit payments

California has been a leader with Californians Against Waste leading the charge (see  [summary of laws](http://www.cawrecycles.org/facts_and_stats/california_recycling_laws "http://www.cawrecycles.org/facts_and_stats/california_recycling_laws")), particularly starting in 1989 with AB 939 (which requires Countywide Integrated Waste Plan) which went for 50% and upped to 75% by AB341 (part of AB32 for global warming)

3 arrows symbol -  [according to Green Girl Recycling](http://www.greengirlrecycling.com/faqs.html "http://www.greengirlrecycling.com/faqs.html") no laws on it

**Supply**

As WM notes in  [What Can I Recycle](http://www.wm.com/thinkgreen/what-can-i-recycle.jsp "http://www.wm.com/thinkgreen/what-can-i-recycle.jsp"), 100 million steel cans (tin-coated) per day, huge recycling 

Make sure the plastic iis clean!

**Industry and prices**

Waste Management - largest recycling coordinator I believe

California publishes Beverage Container Refund Values (see  [2012 example](http://www.calrecycle.ca.gov/BevContainer/Notices/2012/2012ComRates.htm "http://www.calrecycle.ca.gov/BevContainer/Notices/2012/2012ComRates.htm"))

[Scrap Metal Recycling in the US Industry Market Research Report Now Available from IBISWorld](http://www.prweb.com/releases/2013/10/prweb11279880.htm "http://www.prweb.com/releases/2013/10/prweb11279880.htm") (2013) - expensive report by Andy Brennan, asaa of 2013, the four largest firms (including Sims Metal Management Ltd., Commercial Metals Co., Schnitzer Steel Industries Inc. and Metalico Inc.) generated a little over 10.0% of total industry revenue

**News sources**

Earth911 - has a  [nice search website](http://search.earth911.com/ "http://search.earth911.com/?what=CFLs%2C+desktop+computers%2C+cell+phones%2C+etc...&where=%2C+%2C99801&list_filter=all&max_distance=25&family_id=&latitude=&longitude=&country=&province=&city="); owned by  [Quest Resource Holding (QRCH)](http://www.otcmarkets.com/stock/QRHC/company-info "http://www.otcmarkets.com/stock/QRHC/company-info"); 

WasteDive

Waste360

WasteBusinessJournal - detailed data on pricing for a price

Biocycle - produces The State of Garbage In America every couple years
