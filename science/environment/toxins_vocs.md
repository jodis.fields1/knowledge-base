Toxins - volatile organic compounds
===

Vulcanized rubber and VOCs? rubber in the Dream Number and Sleep Number beds is probably synthetic reviewed at http://www.nyc.gov/html/doh/downloads/pdf/eode/turf_report_05-08.pdf and www.dec.ny.gov/docs/materials_minerals_pdf/crumbfacts.pdf A REVIEW OF THE POTENTIAL HEALTH AND SAFETY RISKS FROM SYNTHETIC TURF FIELDS CONTAINING CRUMB RUBBER INFILL

PlushBeds uses urethane http://www.plushbeds.com/popup2.html 
SleepEZ, Custom Sleep Design? http://www.themattressunderground.com/mattress-forum/search.html?q=plushbeds&exactname=1&childforums=1&limit=20

## Avogadro's number
https://www.khanacademy.org/science/healthcare-and-medicine/lab-values/v/the-mole-and-avogadro-s-number helped me understand along with https://www.google.com/search?q=what+is+1+mole+of+hydrogen and the top answer at http://answers.yahoo.com/question/index?qid=20071013222257AALHotf - basically, since atomic weight is a relative measure of individual atoms, if you multiply by the constant you'll get the real weight
dependent upon the definition of a gram which is unstable as discussed at http://www.americanscientist.org/issues/pub/2007/2/an-exact-value-for-avogadros-number 

## Basics
VOCs have a vapor pressure between 1 and 760 torr (??), meaning that they don't become gas at normal temperatures - but others contradict that, and generally they just seem to become vaporous earlier; diagram at http://en.wikipedia.org/wiki/Vapor_pressure helps to understand

I used http://www.rsc.org/helperApps/quickEBookChapter.asp?chapterDOI=9781847552310-00001 Sources, Distributions, and Fates of VOCs in the Atmosphere which has the really confusing definition and listed at http://pubs.rsc.org/en/content/chapter/bk9780854042159-00001/978-0-85404-215-9

Ran a Google Books search on "VOC persistent" in 21st century and there's not many books

Remediation Engineering: Design Concepts http://books.google.com/books?id=Rd2QIcNKl1UC&pg=PA12&lpg=PA12#v=onepage&q&f=false discusses density and mentions DNAPL needs density  over 1.01 

Environmental Biotechnology: A Biosystems Approach http://books.google.com/books?id=udVKUlznYpYC&pg=PT146& has some half-life figures
Urban Watersheds: Geology, Contamination, and Sustainable Development http://books.google.com/books?id=4OeFMYR72EsC&pg=PA301 notes several of the most persistent
Circular, Volumes 1155-1159 http://books.google.com/books?id=MP0kAQAAIAAJ&pg=PA15#v=onepage&q&f=false discusses MBTE
U.S. Geological Survey Professional Paper http://books.google.com/books?id=uVLwAAAAMAAJ&pg=PA139Characterization and Control of Odours and VOC in the Process Industries http://books.google.com/books?id=_InU5xHsoKcC&pg=PA19

Beyond CO2: Measuring VOCs for Healthy Indoor Air Quality by AppliedSensor might be useful

http://www.inventivedesignfirm.com/community-resources/understanding-vocs-life-cycle-assessment-lca-and-more/ is a good list

http://www.ncms.org/wp-content/NCMS_files/sustainability/LCA-VOC_Final_Report.pdf Life Cycle Assessment of
Volatile Organic Compounds (LCA-VOC) in Paints & Coatings -  doesn't seem so great; NCMS Project No. 170420 and the
U.S. Environmental Protection Agency (EPA) EPA Cooperative Agreement EM-83325701-1

## Examples
also see http://www.dwellsmart.com/News-Information/Things-to-Avoid/About-VOCs-Volatile-Organic-Compounds 

## Acetone example and rising or falling
Was wondering if VOCs tend to rise or fall which is dependent upon their vapor density; acetone is the example used in Wikipedia http://en.wikipedia.org/wiki/Vapour_density and explains that this term is often relative to air, and technically this is probably http://en.wikipedia.org/wiki/Specific_gravity relative to air; acetone has a vapor density of 2 so it falls
Also acetone boils at 55 degrees Celsius or so, but at lower temperatures I guess it offgases a little bit; not sure about the calculations for that

http://www.worldofmolecules.com/solvents/acetone.htm is naturally occurring

### Density mass and weight
Calculating density uses atomic mass unit (amu) divided by air's 28 or so; turns out this was equivalent to Molar mass 58.08 g mol^-1 so I guess these are the same; did more digging and found that the unified mass unit http://en.wikipedia.org/wiki/Atomic_mass_unit or dalton is defined as one twelfth of the mass of an unbound neutral atom of carbon-12 in its nuclear and electronic ground state; however now ther is talk of redefining the kilogram http://en.wikipedia.org/wiki/New_SI_definitions which might change that
The "atomic weight" typically used is http://en.wikipedia.org/wiki/Relative_atomic_mass which is controversial; it is the ratio 
in practice, molar mass is used http://en.wikipedia.org/wiki/Molar_mass and it uses a molar mass constant of 1g/m

### Vinyl chloride
Looked at www.epa.gov/ogwdw/pdfs/factsheets/voc/tech/vinylchl.pdf by EPA Technical Factsheet on Vinyl Chloride

### Quizzes and learning
http://quizlet.com/11659654/hazwoper-flash-cards/
http://quizlet.com/11230327/hazmat-chemical-and-physical-properties-flash-cards/
http://www.cal-iaq.org/separator/voc/about-vocs 

### Fumes and vapor pressure - if something smells, it is turning into a gas right?
http://castboolits.gunloads.com/showthread.php?121103-Lead-Fumes-let-s-settle-this

http://www.av8n.com/physics/vapor.htm - vapor and gas

http://books.google.com/books?id=b1EToH2dH_YC&pg=PA147&lpg=PA147 - says aerols don't become airborne?