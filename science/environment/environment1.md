# Environment

https://github.com/tmrowco/northapp-contrib
* https://www.electricitymap.org/?page=country&solar=false&remote=true&wind=false&countryCode=US-CA

## EPA general
[GUEST COMMENTARY: Regulatory Fairness Act sets timeframe for EPA vetoes](http://www.alaskajournal.com/Alaska-Journal-of-Commerce/May-Issue-2-2014/GUEST-COMMENTARY-Regulatory-Fairness-Act-sets-timeframe-for-EPA-vetoes/) - Murkowski criticizes 404(c) veto right

## General
Charles Evanstein - Sacred Economics recommended by Neil

Wikipedia template [Environmental sidebar](https://en.wikipedia.org/wiki/Template:Environment_sidebar) leads to a ton of stuff

Excellent list at http://telstar.ote.cmu.edu/environ/m3/s7/us_laws.shtml

The Consumer's Guide to Effective Environmental Choices: Practical Advice from The Union of Concerned Scientists - quite dated as it is from 1999

Jonathan H. Adler (former Competitive Enterprise Institute guy) has written essays on free-market environmentalism e.g. Ecology, Liberty and Property: A Free-Market Environmental Reader

## Public opinion
2020-01-24: [Support for Making Fossil Fuel Companies Pay for Climate Damages](https://climatecommunication.yale.edu/visualizations-data/fossilfuel-lawsuits/?est=damageresp&type=value&geo=county)

2012-04-02 review: Generally, more good than bad (see http://www.pollingreport.com/enviro.htm for a very good summary). In the meantime ran across http://www.andykerr.net/environmentalists-must-diversi/ which made the interesting point that letters are much better than demonstrations. More anecdotal discussions include http://priuschat.com/forums/gen-ii-prius-main-forum/49762-typecast-driving-prius.html and http://southcoastenergychallenge.org/blog/switch-stigmas which says there's been a switch. http://grist.org/cities/dicum4/ discusses how environmentalists need to recruit more members - so does http://tenthmil.com/campaigns/restore/environmentalism_has_been_stigmatized_-_its_time_for_an_overhaul and also noticed an academic article The effects of being categorised: The interplay between internal and external social identitieshttp://www.tandfonline.com/doi/abs/10.1080/10463280340000045

2013-05: read an article called the death of environmentalism or something; need to reframe global warming and other things as human issues

2013-09: Study: Everyone hates environmentalists and feminists was kinda sad.

## Recycling
Found out about http://www.biggreenbox.com/faq.php

Sent my stuff to Total Reclaim once

### Glass
See https://www.alaskadispatch.com/article/anchorage-gets-glass-recycling-again for an overview of attempts to recycle glass

### Books
Cradle to Cradle: Remaking the Way We Make Things - the classic in this area, but as

http://www.amazon.com/review/R1LOSHYY1YQFW5/ref=cm_cr_pr_viewpnt points out not quantitative enough

## Rivers

Pacific Northwest rivers shown at https://commons.wikimedia.org/wiki/File:Pacific_Northwest_River_System.png also note that the dams along the Columbia created a booming aluminum industry for a while; modern concerns about reduced flow due to droughts

Elwha river dams including Glines Canyon were destroyed to restore salmon habitat which took out around 40MW of electricity compared to 12,000 in Bonneville Power Administration grid per Power down: Elwha dams' turbines silenced after decades, controvery per Neighbors have mixed emotions about removal of Elwha Dam (where loss of electricity is noted); fish pass was considered but they wanted the sediment for the estuary; reservoirs created by the dam such as Lake Miller and Lake Aldwell are gone

Reviewed Major upcoming dam removals in the Pacific Northwest which notes 650 total removed

Also How Much Dam Energy Can We Get? which notes a lower capacity factor than expected

## China

2013-06-23: read up on tree planting motivated by the erosion on the Yangtze Riveraffecting the baiji (glanced at Larson's  [Downstream Environmental Impacts](http://www.threegorgesprobe.org/pi/documents/three_gorges/damming3g/ch06.html)) starting with  [2012 Yale analysis by Luoma](http://e360.yale.edu/feature/chinas_reforestation_programs_big_success_or_just_an_illusion/2484/) which references research articles that I looked at such as Greening China Naturally (2011) and China's new forests aren't as green as they seem (2011); Chinese agency is  [Department of Afforestation and Greening (Office of the National Afforestation and Greening Committee)](http://english.forestry.gov.cn/web/article.do?action=readnew&id=201003150914530000) ; there was a $1.5b campaign  [announced for Chongqing in late 2010 for protecting the Yangtze River](http://news.xinhuanet.com/english2010/china/2010-10/08/c_13547874.htm) from erosion; however I'm hearing  [about transplanting of large expensive ginkgo plants from The Atlantic](http://www.theatlantic.com/international/archive/2011/05/gingko-fever-in-chongqing-the-billion-dollar-trees-of-central-china/238885/); 2013 discussion of  [celebration of Intl Day of Forests](http://english.cqnews.net/html/2013-03/22/content_25250830.htm); 2013 article from Xinhua about  [citizens worrying about publicity stunt](http://news.xinhuanet.com/english/china/2013-03/12/c_132228421.htm); 