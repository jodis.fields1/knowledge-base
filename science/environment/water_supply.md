Water supply
===


## Groundwater

A new report says we're draining our aquifers faster than ever (2013) is eye-opening about the realities

[California Fuel Leak Cleanup Rules Leave Lingering Spills to Nature](http://blogs.kqed.org/newsfix/2014/03/06/california-fuel-tank-cleanup-rules-leave-lingering-spills-to-nature/ "http://blogs.kqed.org/newsfix/2014/03/06/california-fuel-tank-cleanup-rules-leave-lingering-spills-to-nature/") (2014) - in 2012 CA dropped its guard...

_ [New Mexico's groundwater protections may take a hit](https://www.hcn.org/issues/45.17/new-mexicos-groundwater-protections-may-take-a-hit) - sucks _

## Drinking water

See separate note about watersupply for more on drinking water

Safe Drinking Water Act of 1974 is key for U.S.

Treating Water With Chlorine Does Not Reduce Diarrhea Cases - report by PLoS Med by Boisson S et al

Chlorination - linked to food allergies particularly dichlorophenol per Chlorine in tap waterlinked to increase in number of people developing food allergies

Noticed this article about plastic cross-linked polyethylene (PEX) pipes used to bring waterin http://www.fhi.no/eway/default.aspx?pid=238&trg=MainLeft_5895&MainArea_5811=5895:0:15,2828:1:0:0:::0:0&MainLeft_5895=5825:92703::1:5896:1:::0:0 \- the fact that these could cause odor and bad taste seems like a pretty bad sign.

## Purification

### 2019-05 look
https://www.quora.com/Is-this-claim-by-Clearly-Filtered-the-water-filter-manufacturer-scientifically-valid | (2) Is this claim by Clearly Filtered (the water filter manufacturer) scientifically valid? - Quora
https://arkansasjake.com/best-water-filter-pitcher-top-5/ | The Best Water Filter Pitcher? Here are the Top 5 - Arkansas Jake
https://reactual.com/home-and-garden/kitchen-products-2/best-countertop-water-filter.html | The Best Water Filters Of 2019 - Reactual
https://www.amazon.com/Clearly-Filtered-Pitcher-Lifetime-Warranty/dp/B076B6FXT5 | Amazon.com: Clearly Filtered Water Pitcher - Guaranteed to remove chemicals such as Fluoride, Lead, PFOA/PFAS, Glyphosate & even Pharmaceutical drugs: Kitchen & Dining

### old
Zerowater is unique and the best; has a few patents and founded by Rajan Rajan; see http://www.bizjournals.com/philadelphia/stories/2007/11/05/story12.html?page=all for a good overview and http://foodbeverage.about.com/od/Food_Entreprenur_Spotlight/a/Starting-A-Small-Company-Learn-How-To-Gain-Shelf-Space-In-Major-Retail-Stores.htm for another somewhat detailed one

Also could test with Watersafe WS425B DrinkingWaterTest Kit

## Tools

Hanna Instruments HI 98129 Waterproof pH/Conductivity/TDS Tester at  [amzn.com/HI98129](http://amzn.com/HI98129 "http://amzn.com/HI98129") - pH is kinda cool but poor reviews

Assessing Exposure and Health Consequences of Chemicals in Drinking Water: Current State of Knowledge and Research Needs (2014) - EHP article which I skimmed